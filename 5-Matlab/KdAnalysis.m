A = [Motor1Speedrads.^2];
b = [(DutyCyclePercentage./100).*SupplyVoltageV./(Rm*Kq)-Motor1Speedrads./(Rm*Kq*Kv)-If/Kq];

Kd = inv(A'*A)*A'*b
