function [correction] = PID_Loop(current, currentTime, target, err, previousCorrection, pid)
    % check if there is new data
    if( (target - current) == err.err)
        correction.total = previousCorrection.total;
    else
        % Calculate timeslice in seconds (given in milliseconds)
        timeslice = (currentTime - err.mark) / 1000;
        
        % Move all current to previous
        err.mark = currentTime;
        err.preverr = err.err;
        
        % Caluclate new error and filtered error
        err.err = target - current;

        % Calculate p term
        correction.p = pid.Kp * err.err;

        % Calculate i term
        correction.i = ( pid.Ki * timeslice * err.err ) + err.prevI;
        err.prevI = inte;

        % Calculate d term
        correction.d = ( ( pid.Kd * ( err.err - err.preverr ) ) + (pid.Kf * err.prevD) ) / (pid.Kf + timeslice);
        err.prevD = d;


        err.correction = ( p + inte + d);

        correction.total = err.correction;
    end
end