[length, width] = size(data);

pid.kp = ;
pid.ki = ;
pid.kd = ;
pid.kf = ;

for i=1:1:length
    % Run the PID loop
    correction[i] = PID_Loop(data(i,6) *(pi/180),... % The current position
                             data(i,1),...          %The timestamp
                             0,...               % The desired position
                             err[i-1],...
                             correction[i-1],...
                             pid);
end