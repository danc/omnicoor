close all;
fricCoeff = 0.9373;

%% Get the recorded pitch data
pitch = data(:,6);

smallPitch = pitch(203:470);


%% Create the sample system
A = [0, 1; (lr*mr*9.81+2*lr*mm*9.81)/(Jr+Jm), -fricCoeff/(Jr+Jm)];
B = [0; 2*lr/(Jr+Jm)];
C = [1,0];
D = [0];
system = ss(A, B, C, D);

[pitchLength, gar] = size(smallPitch);
t = 0:0.005:(0.005*(pitchLength-1));
simData = 180/pi*lsim(system, zeros(pitchLength,1), t, [smallPitch(1)*pi/180, 0]);
plot(t, simData, t, smallPitch);

figure;
plot(t, log(simData), t, log(smallPitch));
title('Log plot');


%% Compute the slope of the logarithmic graphs
linearAnalyticalPitch = log(smallPitch(100:200));
linearSimPitch = log(simData(100:200));
[dataLength, gar] = size(linearAnalyticalPitch);

tzoom = 0:0.005:(0.005*(dataLength-1));
tzoom = tzoom';

[analyticalSlope, analyticalIntercept] = leastSquares(tzoom, linearAnalyticalPitch)
[simSlope, simIntercept] = leastSquares(tzoom, linearSimPitch)