function [slope, intercept] = leastSquares(indep, dep)
%LEASTSQUARES Perform a least squares regression analysis on data
% 
% LEASTSQUARES(indep, dep) Performs a least squares regression analysis
% using principles from linear algebra. It determines the slope and
% intercept for the best fit line for the data set with points (indep,dep)
%
% Arguements:
%  indep - The independant (x-axis) variable of the dataset
%  dep - The dependant (y-axis) variable of the dataset
%
% Output Variables:
%  slope - The slope of the best fit line
%  intercept - The intercept of the best fit line
%
%
% Created by: Ian McInerney
% Created on: May 23, 2013
% Version: 1.0
% Last Modified: July 5, 2013

% Create the initial arrays
[len ~] = size(indep);
A = [ones(len,1), indep];
b = dep;

% Perform the computation x = inverse(transpose(A)*A)*(transpose(A)*b)
x = (A'*A)\(A'*b);

intercept = x(1);
slope = x(2);
end