This folder contains MATLAB scripts used to calculate the friction coefficient for the 1-D pendulum system using zero-input response measurements collected from the logging system. The best script to use is the "IterativeFrictionModeling.m" one, and it will generate results similar to those in the included graphs when run.

The instructions for running that script are located in its header.

AUTHOR: Ian McInerney
DATE: December 2, 2014
