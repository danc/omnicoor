% This script is designed to calculate the coefficient of friction that
% provide the best correlation between a measured system zero-input
% response and a simulated zero-input response for that system.
%
% Note: To use this script you must have already imported the measurement
% log file onto the workspace (with the pitch data inside column 6 of the
% variable called "data"). Additionally, the constants for the system must
% be on the workspace as well. This can be accmplished by running the
% "Constants.m" script in the pendulum system's folder.
%
% Author: Ian McInerney
% Date: December 1, 2014
%
% Revision History:
%  12/1/2014 - Initial script

close all;

%% The constants necessary for this script

% The tolerance to find for the friction coefficient
frictionTolerance = 0.00000005;

% The tolerance to find the intercept match
interceptTolerance = 0.005;

% The minimum number of points to require for the pitch sample used in the
% comparison
minimumPoints = 50;

% The angle at which to stop the pendulum system comparison (it compares
% for angles less than this one).
lowerSwingBound = 40;       % Degrees


%% Get the recorded pitch data
pitch = data(:,6);

fricCoeff = 0.5;    % Initial coefficient
startPoint = 1;   % Initial start point

% Find the point at which the pendulum first crosses the lower swing bound
stopPointFind = find(pitch > lowerSwingBound);
stopPoint = min(stopPointFind);
smallPitch = pitch(startPoint:stopPoint);

% Variables needed for the below loops
stillSearchingSlope = true;
stillSearchingIntercept = true;
interceptCount = 0;
slopeCount = 0;

% Array to hold the end results of each loop
arrayLength = stopPoint - minimumPoints;
loopResults = zeros(arrayLength, 4);

% Iterate over the allowed intercepts to find the one that matches the
% closest to the collected data
while stillSearchingIntercept
    disp(num2str(startPoint));
    interceptCount = interceptCount + 1;    % The number of intercepts explored
    smallPitch = pitch(startPoint:stopPoint);   % Copy the data to be used for the comparison
    
    % Iterate over the friction coefficients to find the best slope match
    stillSearchingSlope = true;
    while stillSearchingSlope
        slopeCount = slopeCount+1;

        %% Create the sample system to simulate the behavior with this coefficient
        A = [0, 1; (lr*mr*9.81+2*lr*mm*9.81)/(Jr+Jm), -fricCoeff/(Jr+Jm)];
        B = [0; 2*lr/(Jr+Jm)];
        C = [1,0];
        D = [0];
        system = ss(A, B, C, D);

        [pitchLength, gar] = size(smallPitch);
        t = 0:0.005:(0.005*(pitchLength-1));
        simData = 180/pi*lsim(system, zeros(pitchLength,1), t, [smallPitch(1)*pi/180, 0]);


        %% Compute the slope of the logarithmic graphs
        linearAnalyticalPitch = log(smallPitch); %(100:200)
        linearSimPitch = log(simData); %(100:200)
        [dataLength, gar] = size(linearAnalyticalPitch);

        tzoom = 0:0.005:(0.005*(dataLength-1));
        tzoom = tzoom';

        [analyticalSlope, analyticalIntercept] = leastSquares(tzoom, linearAnalyticalPitch);
        [simSlope, simIntercept] = leastSquares(tzoom, linearSimPitch);

        difference = simSlope - analyticalSlope;
        if (abs(difference) < frictionTolerance)
            stillSearchingSlope = false;
        else
            % Increment the friction coefficient to its next value
            fricCoeff = fricCoeff + difference;
        end
    end % End of the slope searching loop
    
    % Store the results for possible analysis later
    loopResults(startPoint, 1) = startPoint;
    loopResults(startPoint, 2) = fricCoeff;
    loopResults(startPoint, 3) = simSlope;
    loopResults(startPoint, 4) = simIntercept;
    loopResults(startPoint, 5) = analyticalSlope;
    loopResults(startPoint, 6) = analyticalIntercept;
    
    % Increment the start point and make sure it doesn't drop below the
    % number of points required for comparison
    startPoint = startPoint + 1;
    if (stopPoint - startPoint) < minimumPoints
        stillSearchingIntercept = false;
    end
    
    % See if the intercept is within the tolerance of zero
    if (abs(simIntercept-analyticalIntercept) < interceptTolerance)
        stillSearchingIntercept = false;
    end
end % End of the intercept searching loop


%% Display the results
disp(['Found friction coefficient=' num2str(fricCoeff) ' after ' num2str(slopeCount) ' iterations']);
plot(t, simData, t, smallPitch);
title(['Zero-input system response with \beta=' num2str(fricCoeff)]);
xlabel('Time (s)');
ylabel('Pitch (degrees)');

figure;
plot(t, log(simData), t, log(smallPitch));
title(['Log plot for zero-input system response (\beta=' num2str(fricCoeff) ')']);