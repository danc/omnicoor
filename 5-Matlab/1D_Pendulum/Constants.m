%% These are the constants required for the Simulink model for the 1D-pendulum
% Change log:
%  9/30/2014 - Initial file containing constants from Matt Rich's thesis
%  10/26/2014 - Added constants for the new mechanical system
%  11/2/2014 - Removed constants for the unused wooden frame system
%  11/14/2014 - Changed constants to be for the new pendulum system
%  12/4//2014 - Added new constants for the DJI motors
%  12/10/2014 - Added Kt and Kd for the DJI propellers
%  12/16/2014 - Fixed Kd constant (Discovered calculation error)
%  12/30/2014 - Fixed Kt constants (Discovered calculation error)

%% Constants for the rod
fullRodLength = 1.73;      % The rod length from pivot point to the top (meters)
lr = fullRodLength/2;      % The rod length divided by 2 (meters)
%mr = 0.244;      % This is the total mass of the metal rod used
mr = 0.2270;      % The mass of the rod above the pivot point (kg)
Jr = (mr*(lr*2)^2)/3;      % The moment of inertia for the rod about the pivot point

%% Constants for the motor system
mm = 0.626;         % The mass of the motor system (kg)
beta = 0;           % The angle between the motor shaft and the x-axis in the motor system (Radians)
Jmbody = 6.64E-3;   % The moment of inertia relative to the motor system body
Jm = Jmbody + mm*(fullRodLength)^2;      % The moment of inertia for the motor system about the pivot point


%% Constant for the 8-inch DJI propellers
Kt = 7.9008E-6;         % Thrust constant
Jmr = 1.4602E-5;        % Moment of inertia for the rotor & propeller (8-inch propeller)
Kd = 2.3765E-7;         % Rotor drag constant


%% Propeller thrust constant for the 10-inch DJI propellers
Kt = 1.1325E-5;         % Thrust constant
Jmr = 2.9203E-5;        % Moment of inertia for the rotor & propeller (10-inch propeller)
Kd = 3.5388E-7;         % Rotor drag constant


%% Propeller thrust constant for the Gaui propellers
Kt = 7.3956E-6;


%% Constants for the DJI motors and ESC system
Vb = 11.3;
Rm = 0.243;             % Motor winding resistance (Ohms) (Measured across two terminals)
Kv = 920*(2*pi)/60;     % Motor Back-EMF Constant (Provided in RPM/V, need rad/v sec)
If = 0.422;             % Motor no-load current (Amps) (Measured at full-speed with no propeller)
Kq = Kv;                % Motor torque constant (Nm/A) (defined equal to Kv)


%% Constants for the Motors and ESC system (Old Gaui Motors)
Vb = 11.3;  % Nominal battery voltage (Volts)
Rm = 0.19;  % Motor winding resistance (Ohms)
Kq = 110;  % Motor torque constant (Nm/A)
If = 0.39;  % Motor No-load current (Amps)
Kv = 110;  % Motor Back-EMF Constant (rad/v sec)
Kd = 5.194E-7;  % Motor drag constant (due to propeller)
Jmr = 1.376E-5; % Moment of inertia for the rotor & propellers
