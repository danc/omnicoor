clear all;
close all;
clc;

%% Create a PD controller for the pendulum system
Wgcd = 0.25;
Mp = 5;
systemDelay = 0.15;

%% System definitions
num = [448.638994270635];
den = [1, 67.0935847991990, -5.95743369682484, -399.705582923511];
system = tf(num, den);
s = tf('s');

if (systemDelay == 0)
    delay = 1;
else
    delay = exp(-systemDelay*s);
end

%% Calculate delayed parameters
Wgcdelay = Wgcd*systemDelay;

PMd = 70 - Mp;
PMwgc = PMd*pi/180 + Wgcdelay;
% Make sure it meets the criteria
if PMwgc > pi/2
    error('Unable to meet the stability criterion');
end
if PMwgc < 0
    error('Unable to meet the stability criterion');
end

%% Create the controller
K1 = Wgcdelay*sin(PMwgc);
K2 = Wgcdelay^2*cos(PMwgc);

% Create the transfer functions
% *(4/600)
OL = 10000*(K2+K1*s)*delay*system;
CL = minreal(OL/(1+OL));

%% Plot the controller characteristics
figure;
margin(OL);
grid;

figure;
info = stepinfo(CL);
step(CL);
title(['Step response - Overshoot=' num2str(info.Overshoot) ' Settling Time=' num2str(info.SettlingTime)]);
grid;

disp(['Kp= ' num2str(Kp) ' Kd= ' num2str(Kd)]);
disp(['Zero at ' num2str(Kp/Kd)]);