clear all;
close all;
clc;

%% Create a PD controller for the pendulum system
Wgcd = 1;
Mp = 25;
systemDelay = 0;

% System definitions
num = [448.638994270635];
den = [1, 67.0935847991990, -5.95743369682484, -399.705582923511];
system = tf(num, den);
s = tf('s');

if (systemDelay == 0)
    delay = 1;
else
    delay = exp(-systemDelay*s);
end

% Calculate the necessary phase change
PMd = 70 - Mp;
% (4/600)
[mag, PM] = bode(delay*system, Wgcd);
deltaPM = PMd - (180 + PM) + 3;

% Calculate the zero time constant
Td = tand(deltaPM)/Wgcd;

% (4/600)
% Calculate the required gain to get the crossover
[mag, phase] = bode((1+Td*s)*delay*system, Wgcd);
Kp = 1/mag+5;

% Calculate the Kd
Kd = Kp*Td;

% Create the transfer functions
% *(4/600)
OL = (Kp+Kd*s)*delay*system;
CL = minreal(OL/(1+OL));

%% Plot the controller characteristics
figure;
margin(OL);
grid;

figure;
info = stepinfo(CL);
step(CL);
title(['Step response - Overshoot=' num2str(info.Overshoot) ' Settling Time=' num2str(info.SettlingTime)]);
grid;

disp(['Kp= ' num2str(Kp) ' Kd= ' num2str(Kd)]);
disp(['Zero at ' num2str(Kp/Kd)]);