-- Only XST supports RAM inference
-- Infers Dual Port Block Ram 
library IEEE;
use IEEE.STD_LOGIC_1164.all;  -- defines std_logic types
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity dpblockram256x16 is 
 port (
 		clk  : in std_logic; 
		we   : in std_logic; 
		adda : in std_logic_vector(7 downto 0); 
 		addb : in std_logic_vector(7 downto 0); 
 		din  : in std_logic_vector(15 downto 0); 
 		outa : out std_logic_vector(15 downto 0); 
 		outb : out std_logic_vector(15 downto 0));
end dpblockram256x16; 
 
 architecture syn of dpblockram256x16 is -- 256 x 16 dpram one input port
 
 type ram_type is array (255 downto 0) of std_logic_vector (15 downto 0); 
 signal RAM : ram_type; 
 signal read_a : std_logic_vector(7 downto 0); 
 signal read_b : std_logic_vector(7 downto 0); 
 
 begin 
 process (clk) 
 begin 
 	if (clk'event and clk = '1') then  
 		if (we = '1') then 
 			RAM(conv_integer(adda)) <= din; 
 		end if; 
 		read_a <= adda; 
 		read_b <= addb; 
 	end if; 
	outa <= RAM(conv_integer(read_a)); 
 	outb <= RAM(conv_integer(read_b)); 
end process; 
 
 end syn;
 

