library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity RFIFO256 is
    Port ( clk : in std_logic;
           ibus : in std_logic_vector(15 downto 0);
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic;
           selwadd : in std_logic;
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : out std_logic_vector(15 downto 0);
			  hostread	: in std_logic;
			  hostreadte	: in std_logic;
			  empty : out std_logic;
			  half_full : out std_logic
			  );
end RFIFO256;

architecture Behavioral of RFIFO256 is

component dpblockram256x16 is 
 	port (
 	clk  : in std_logic; 
	we   : in std_logic; 
	adda : in std_logic_vector(7 downto 0); 
 	addb : in std_logic_vector(7 downto 0); 
	din  : in std_logic_vector(15 downto 0); 
 	outa : out std_logic_vector(15 downto 0); 
 	outb : out std_logic_vector(15 downto 0));
end component dpblockram256x16; 
	
	signal pushadd: std_logic_vector(7 downto 0);
	signal pushdata: std_logic_vector(15 downto 0);
	signal popadd: std_logic_vector(7 downto 0);
	signal popdata: std_logic_vector(15 downto 0);
	signal datacounter: std_logic_vector(8 downto 0);
	alias  datacounterMSB: std_logic is datacounter(8);
	alias  datacounterNMSB: std_logic is datacounter(7);
	signal push: std_logic;  -- the write enable for our dual ported RAM
	signal pop: std_logic;  
	signal clear: std_logic; 
	signal fifoempty: std_logic; 
			
begin

	fiforam: dpblockram256x16 port map
	(
 	clk =>  clk,
	we =>   push,
	adda => pushadd,
 	addb => popadd,
 	din =>  ibus,
 	outa => pushdata,
 	outb => popdata
	);	

	atracebuffer: process (clk,selwadd,writesel,selradd,readsel,
				    		pushadd,popadd,popdata,pushdata,datacounter)
	begin
		if clk'event and clk = '1' then

			if push = '1' and pop = '0' then		-- a push			 			
				datacounter <= datacounter + 1;
				pushadd <= pushadd + 1;				
			end if;				
			
			if  pop = '1' and push = '0' then	-- a pop						
				datacounter <= datacounter -1;
				popadd <= popadd + 1;
			end if;

			if  pop = '1' and push = '1' then	-- a push and a pop						
				popadd <= popadd + 1;
				pushadd <= pushadd + 1;
			end if;

-- if neither push nor pop, do nothing... --
	  
			if clear = '1' then 
				pushadd <= (others => '0');
				popadd  <= (others => '0');
				datacounter <= (others => '0');
			end if;	
	

		end if; -- clk rise
		
		if writesel = '1' and selwadd = '0' and datacounter /= 256 then
			push <= '1';
		else
			push <= '0';
		end if;

		if  hostreadte = '1'  and datacounter /= 0 then
			pop <= '1';
		else			
			pop <= '0';
		end if;

		if writesel = '1' and selwadd = '1' then 				 
			clear <= '1';
		else			
			clear <= '0';
		end if;
					 
		if	readsel = '1' and selradd  = '1' then
 			obus(8 downto 0) <= datacounter;
			obus(15 downto 9) <= (others => '0'); -- read data counter	
 		else 
			obus <= (others => 'Z'); 
		end if;

		if datacounter /= 0 then
			fifoempty <= '0';
		else
		   fifoempty <= '1';
		end if;
		
		if hostread = '1' then
			hostdata <= popdata;
		else
			hostdata <= (others => 'Z'); 
		end if;
				
		empty <= fifoempty;
		half_full <= (datacounterNMSB or datacounterMSB);
	end process;
end Behavioral;
