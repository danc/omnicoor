library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hoststat is
    Port (
           obus : out std_logic_vector(15 downto 0);
           read : in std_logic;
           hostread : in std_logic;
           hostdata : out  std_logic_vector(15 downto 0);
           fifostat : in std_logic_vector(7 downto 0);
			  istatus : in std_logic_vector(7 downto 0));
end hoststat;

architecture Behavioral of hoststat is


begin
	astatreg: process (read,hostread,fifostat)
	begin	
		obus <= (others => 'Z');
		if read = '1' then
			obus(7 downto 0) <= istatus;
			obus(15 downto 8) <= fifostat;
		end if;
		hostdata <= (others => 'Z');
		if hostread = '1' then
			hostdata(7 downto 0) <= istatus;
			hostdata(15 downto 8) <= fifostat;
		end if;
	end process;	
		

end Behavioral;
