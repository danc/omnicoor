library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity timer is
   port ( 
	obus: out STD_LOGIC_VECTOR (15 downto 0);
	timerreadcmd: in STD_LOGIC;
	timerlatchcmd: in STD_LOGIC;
	timerclearcmd: in STD_LOGIC;
	clk: in STD_LOGIC
	);
end timer;

architecture behavioral of timer is

signal timercount: STD_LOGIC_VECTOR (15 downto 0);
signal timerlatch: STD_LOGIC_VECTOR (15 downto 0);

signal divisor: STD_LOGIC;
signal delayedclearcmd: STD_LOGIC;
signal dolatch: STD_LOGIC;
signal doclear: STD_LOGIC;

begin
	atimer: process 	  (clk, 
	 							timercount,
								timerlatch,
								timerreadcmd,
								timerlatchcmd,
								dolatch,
								timerclearcmd,
								doclear,
								delayedclearcmd,
								divisor
								)

	begin
		if clk'event and clk = '1' then -- per clk stuff
			delayedclearcmd <= timerclearcmd;
			if timerclearcmd = '1' and delayedclearcmd = '0' then
				doclear <= '1';
			end if;			
			divisor <= not divisor;
			if timerlatchcmd = '1' then	
				dolatch <='1';
			end if;
			if divisor = '1' then -- divided by 2 stuff
				timercount <= timercount + 1;
				if dolatch = '1' then
					timerlatch <= timercount;
					dolatch <= '0';
				end if;
				if doclear = '1' then
					timercount <= x"0000";
					doclear <= '0';
				end if;
			end if; -- divisor = 1
		end if; -- clk

		obus <= "ZZZZZZZZZZZZZZZZ";		
		if timerreadcmd = '1' then
		 	obus <= timerlatch(15 downto 0);
		end if;		
	end process;
end behavioral;
