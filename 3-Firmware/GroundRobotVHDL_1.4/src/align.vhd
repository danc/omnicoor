library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity alignbox is
   Port ( 
	din: in STD_LOGIC_VECTOR (15 downto 0);
	dout: out STD_LOGIC_VECTOR (15 downto 0);
	writes: in STD_LOGIC;
	reads: in STD_LOGIC;
	selradd: in STD_LOGIC_VECTOR (1 downto 0);
	selwadd: in STD_LOGIC_VECTOR (1 downto 0);
	clk: in STD_LOGIC
	 );
end alignbox;


architecture behavioral of alignbox is

signal adata: STD_LOGIC_VECTOR (63 downto 0);
signal srclow: STD_LOGIC_VECTOR (15 downto 0);
signal srchigh: STD_LOGIC_VECTOR (15 downto 0);

begin
	ananlignbox: 
	process  
							( clk,
							  reads,
							  selradd
							)
	begin
		if clk'event and clk = '1' then
			if writes = '1' then
				case selwadd is
					when b"00" => srclow <= din;
					when b"01" => srchigh <= din;
					when others => null;
				end case;
			end if;

		end if; -- clk
		adata( 7 downto  0) <= x"00";
 		adata(15 downto  8) <=  srclow( 7 downto 0);
 		adata(23 downto 16) <=  srclow(15 downto 8); 			
		adata(31 downto 24) <= srchigh( 7 downto 0);
		adata(39 downto 32) <= srchigh(15 downto 8);
		if srchigh(15) = '1' then
			adata(63 downto 40) <= x"FFFFFF";
		else
			adata(63 downto 40) <= x"000000";
		end if;		dout <= "ZZZZZZZZZZZZZZZZ";

		if reads = '1' then
			case selradd is
				when b"00" => dout <= adata(15 downto  0);
				when b"01" => dout <= adata(31 downto 16);
				when b"10" => dout <= adata(47 downto 32);
				when b"11" => dout <= adata(63 downto 48);
				when others => null;
			end case;
		else dout <= "ZZZZZZZZZZZZZZZZ";
		end if;		
	end process;

end behavioral;

