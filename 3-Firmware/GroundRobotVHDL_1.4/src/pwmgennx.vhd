library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity pwmgennx is
	port (
	clk: in STD_LOGIC;
	refcount: in STD_LOGIC_VECTOR (7 downto 0);
	ibus: in STD_LOGIC_VECTOR (7 downto 0);
	loadpwmval: in STD_LOGIC;
	pwmout: out STD_LOGIC

	);
end pwmgennx;

architecture behavioral of pwmgennx is

signal pwmval: STD_LOGIC_VECTOR (7 downto 0);
signal pwm: STD_LOGIC;


begin
	apwmgen: process  (clk)
							
	begin
		if clk'event and clk = '1' then
			if (UNSIGNED(refcount) < UNSIGNED(pwmval)) then 
				pwm <= '1'; 
			else 
				pwm <= '0';
			end if;
			
			if (loadpwmval = '1') then 
 		   	pwmval <= ibus;
			end if;	
		end if;
		pwmout <= pwm;
	end process;
end behavioral;

