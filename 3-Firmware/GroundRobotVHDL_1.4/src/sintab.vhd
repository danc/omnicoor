library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity sinetab is
	port (
	addra: in std_logic_vector(8 downto 0);
	addrb: in std_logic_vector(8 downto 0);
	clk: in std_logic;
	dina: in std_logic_vector(7 downto 0);
	douta: out std_logic_vector(7 downto 0);
	doutb: out std_logic_vector(7 downto 0);
	wea: in std_logic);
end sinetab;

architecture syn of sinetab is
   type ram_type is array (0 to 511) of std_logic_vector(7 downto 0);
   signal RAM : ram_type := 
   (
   x"00", x"02", x"03", x"05", x"06", x"08", x"09", x"0B", x"0D", x"0E", x"10", x"11", x"13", x"14", x"16", x"17",
   x"19", x"1B", x"1C", x"1E", x"1F", x"21", x"22", x"24", x"25", x"27", x"29", x"2A", x"2C", x"2D", x"2F", x"30",
   x"32", x"33", x"35", x"36", x"38", x"39", x"3B", x"3C", x"3E", x"3F", x"41", x"43", x"44", x"46", x"47", x"49",
   x"4A", x"4C", x"4D", x"4F", x"50", x"51", x"53", x"54", x"56", x"57", x"59", x"5A", x"5C", x"5D", x"5F", x"60",
   x"62", x"63", x"64", x"66", x"67", x"69", x"6A", x"6C", x"6D", x"6E", x"70", x"71", x"73", x"74", x"75", x"77",
   x"78", x"7A", x"7B", x"7C", x"7E", x"7F", x"80", x"82", x"83", x"84", x"86", x"87", x"88", x"8A", x"8B", x"8C",
   x"8E", x"8F", x"90", x"92", x"93", x"94", x"95", x"97", x"98", x"99", x"9A", x"9C", x"9D", x"9E", x"9F", x"A1",
   x"A2", x"A3", x"A4", x"A5", x"A7", x"A8", x"A9", x"AA", x"AB", x"AC", x"AE", x"AF", x"B0", x"B1", x"B2", x"B3",
   x"B4", x"B5", x"B7", x"B8", x"B9", x"BA", x"BB", x"BC", x"BD", x"BE", x"BF", x"C0", x"C1", x"C2", x"C3", x"C4",
   x"C5", x"C6", x"C7", x"C8", x"C9", x"CA", x"CB", x"CC", x"CD", x"CE", x"CF", x"D0", x"D0", x"D1", x"D2", x"D3",
   x"D4", x"D5", x"D6", x"D7", x"D7", x"D8", x"D9", x"DA", x"DB", x"DC", x"DC", x"DD", x"DE", x"DF", x"DF", x"E0",
   x"E1", x"E2", x"E2", x"E3", x"E4", x"E4", x"E5", x"E6", x"E7", x"E7", x"E8", x"E8", x"E9", x"EA", x"EA", x"EB",
   x"EC", x"EC", x"ED", x"ED", x"EE", x"EE", x"EF", x"F0", x"F0", x"F1", x"F1", x"F2", x"F2", x"F3", x"F3", x"F4",
   x"F4", x"F4", x"F5", x"F5", x"F6", x"F6", x"F7", x"F7", x"F7", x"F8", x"F8", x"F8", x"F9", x"F9", x"F9", x"FA",
   x"FA", x"FA", x"FB", x"FB", x"FB", x"FC", x"FC", x"FC", x"FC", x"FC", x"FD", x"FD", x"FD", x"FD", x"FD", x"FE",
   x"FE", x"FE", x"FE", x"FE", x"FE", x"FE", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
   x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FE", x"FE", x"FE", x"FE", x"FE",
   x"FE", x"FE", x"FD", x"FD", x"FD", x"FD", x"FD", x"FC", x"FC", x"FC", x"FC", x"FC", x"FB", x"FB", x"FB", x"FA",
   x"FA", x"FA", x"F9", x"F9", x"F9", x"F8", x"F8", x"F8", x"F7", x"F7", x"F7", x"F6", x"F6", x"F5", x"F5", x"F4",
   x"F4", x"F4", x"F3", x"F3", x"F2", x"F2", x"F1", x"F1", x"F0", x"F0", x"EF", x"EE", x"EE", x"ED", x"ED", x"EC",
   x"EC", x"EB", x"EA", x"EA", x"E9", x"E8", x"E8", x"E7", x"E7", x"E6", x"E5", x"E4", x"E4", x"E3", x"E2", x"E2",
   x"E1", x"E0", x"DF", x"DF", x"DE", x"DD", x"DC", x"DC", x"DB", x"DA", x"D9", x"D8", x"D7", x"D7", x"D6", x"D5",
   x"D4", x"D3", x"D2", x"D1", x"D0", x"D0", x"CF", x"CE", x"CD", x"CC", x"CB", x"CA", x"C9", x"C8", x"C7", x"C6",
   x"C5", x"C4", x"C3", x"C2", x"C1", x"C0", x"BF", x"BE", x"BD", x"BC", x"BB", x"BA", x"B9", x"B8", x"B7", x"B5",
   x"B4", x"B3", x"B2", x"B1", x"B0", x"AF", x"AE", x"AC", x"AB", x"AA", x"A9", x"A8", x"A7", x"A5", x"A4", x"A3",
   x"A2", x"A1", x"9F", x"9E", x"9D", x"9C", x"9A", x"99", x"98", x"97", x"95", x"94", x"93", x"92", x"90", x"8F",
   x"8E", x"8C", x"8B", x"8A", x"88", x"87", x"86", x"84", x"83", x"82", x"80", x"7F", x"7E", x"7C", x"7B", x"7A",
   x"78", x"77", x"75", x"74", x"73", x"71", x"70", x"6E", x"6D", x"6C", x"6A", x"69", x"67", x"66", x"64", x"63",
   x"62", x"60", x"5F", x"5D", x"5C", x"5A", x"59", x"57", x"56", x"54", x"53", x"51", x"50", x"4F", x"4D", x"4C",
   x"4A", x"49", x"47", x"46", x"44", x"43", x"41", x"3F", x"3E", x"3C", x"3B", x"39", x"38", x"36", x"35", x"33",
   x"32", x"30", x"2F", x"2D", x"2C", x"2A", x"29", x"27", x"25", x"24", x"22", x"21", x"1F", x"1E", x"1C", x"1B",
   x"19", x"17", x"16", x"14", x"13", x"11", x"10", x"0E", x"0D", x"0B", x"09", x"08", x"06", x"05", x"03", x"02");

signal daddra: std_logic_vector(8 downto 0);
signal daddrb: std_logic_vector(8 downto 0);

begin
   asintab: process (clk)
   begin
      if (clk'event and clk = '1') then
         if (wea = '1') then
            RAM(conv_integer(addra)) <= dina;
         end if;
         daddra <= addra;
         daddrb <= addrb;
      end if; -- clk 
   end process;

   douta <= RAM(conv_integer(daddra));
   doutb <= RAM(conv_integer(daddrb));
end;
