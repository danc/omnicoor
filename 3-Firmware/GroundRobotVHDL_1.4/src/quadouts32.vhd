library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity quadouts32 is
   port ( 
	obus: out STD_LOGIC_VECTOR (15 downto 0);
	ibus: in STD_LOGIC_VECTOR (15 downto 0);
	ReadCmd: in STD_LOGIC;
	WriteCmd: in STD_LOGIC;
	ReadAddr: in STD_LOGIC_VECTOR(2 downto 0);
	WriteAddr: in STD_LOGIC_VECTOR(2 downto 0);
	A: out STD_LOGIC;
	B: out STD_LOGIC;
	clk: in STD_LOGIC
	);
end QuadOuts32;

architecture behavioral of QuadOuts32 is

signal ratecount: STD_LOGIC_VECTOR (15 downto 0);
signal qbits: STD_LOGIC_VECTOR (1 downto 0);
signal ratelatch: STD_LOGIC_VECTOR (15 downto 0);
signal pulsecount: STD_LOGIC_VECTOR (31 downto 0);
signal pulsecountlowlatch: STD_LOGIC_VECTOR (15 downto 0);
alias  pulsecountlow:  STD_LOGIC_VECTOR (15 downto 0) is pulsecount(15 downto 0);
alias  pulsecounthigh: STD_LOGIC_VECTOR (15 downto 0) is pulsecount(31 downto 16);
signal Modelatch: STD_LOGIC_VECTOR (1 downto 0);
signal abit: STD_LOGIC;
signal bbit: STD_LOGIC;
signal oldratecountmsb: STD_LOGIC;

begin
	aquadout: process(clk,ratelatch,modelatch,pulsecount,abit,bbit,readcmd,readaddr)


	begin
		if clk'event and clk = '1' then -- per clk stuff
			oldratecountmsb <= ratecount(15);

			if (pulsecount /= x"00000000") then
				ratecount <= ratecount + ratelatch;	
			end if;
			if ratecount(15) /= oldratecountmsb then
				if pulsecount(31) = '0' then
					if modelatch(1) = '0' then
						pulsecount <= pulsecount -1;
					end if;
					qbits <= qbits +1; 
				else
					if modelatch(1) = '0' then
						pulsecount <= pulsecount +1;
					end if;
					qbits <= qbits -1; 
				end if;
			end if;
			if WriteCmd = '1' then
				case WriteAddr is
					when b"000" => ratelatch <= ibus;
					when b"001" => pulsecountlowlatch <= ibus;
					when b"010" => 
										pulsecounthigh <= ibus;
										pulsecountlow <= pulsecountlowlatch;
					when b"011" => modelatch <= ibus(1 downto 0);
					when b"100" => qbits <= ibus(1 downto 0);
					when others => null;
				end case;
		   end if;

			if qbits = "01"  or qbits = "10" then 
				abit <= '1';
			else
				abit <= '0';
			end if;
 			if qbits = "10"  or qbits = "11" then 
				bbit <= '1';
			else
				bbit <= '0';
			end if;
		end if; -- clk	
		
		if Modelatch(0) = '1' then
			A <= abit;
			B <= bbit;
		else
			A <= 'Z';
			B <= 'Z';
		end if;
						
		if ReadCmd = '1' then
			case ReadAddr is						
					when b"000" => obus <= ratelatch ;
					when b"001" => obus <= pulsecountlow ;
					when b"010" => obus <= pulsecounthigh ;
					when b"011" => 
						obus(1 downto 0) <= modelatch ;
						obus(15 downto 2) <= "00000000000000";
					when b"100" => 
						obus(1 downto 0) <= qbits ;
						obus(15 downto 2) <= "00000000000000";
					when others => obus <= "ZZZZZZZZZZZZZZZZ";
			end case;
		else
			obus <= "ZZZZZZZZZZZZZZZZ";
		end if;
	end process;
end behavioral;
