library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity twelveiorb is
    Port (			
	 		obus: out STD_LOGIC_VECTOR (11 downto 0);
			readport: in STD_LOGIC;
			portdata: in STD_LOGIC_VECTOR (11 downto 0) );
end twelveiorb;

architecture behavioral of twelveiorb is

begin
	atwelveiorb: process (portdata,readport)
	begin
		if readport = '1' then
			obus <= portdata;
 	   else
			obus <= "ZZZZZZZZZZZZ";
		end if;
	end process;

end behavioral;
