library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity irqdrive is
    Port (		
	   	clear: in STD_LOGIC;
			clk: in STD_LOGIC;
			ibus: in STD_LOGIC_VECTOR (7 downto 0);
			obus: out STD_LOGIC_VECTOR (7 downto 0);
			hostibus: in STD_LOGIC_VECTOR (7 downto 0);
			loadport: in STD_LOGIC;
			readport: in STD_LOGIC;
			setirq: in STD_LOGIC;
			clrirq: in STD_LOGIC;
			istatus: out STD_LOGIC_VECTOR (7 downto 0);
			irqs: out STD_LOGIC_VECTOR (15 downto 3)
 			);
end irqdrive;

architecture behavioral of irqdrive is

signal irqselreg: STD_LOGIC_VECTOR (6 downto 0);
signal istatusreg: STD_LOGIC_VECTOR (7 downto 0);
signal irqts: STD_LOGIC_VECTOR (15 downto 3);
signal irqgen: STD_LOGIC;

begin
	airqdrive: process (
								clk,
								ibus,
								readport,
								irqselreg
								)
	begin
		if clk'event and clk = '1' then
			if loadport = '1' then
				irqselreg <= ibus(6 downto 0);
			end if; 
			
			if setirq = '1' and clrirq = '0' then 				
				istatusreg <= istatusreg or ibus;
			end if;
			if setirq = '0' and clrirq = '1' then
				istatusreg <= istatusreg and (not hostibus);			
			end if;
			if setirq = '1' and clrirq = '1' then 
				istatusreg <= (istatusreg and (not hostibus)) or ibus;			
			end if;
			
			if istatusreg = x"00" then
				irqgen <= '0';
			else
				irqgen <= '1';
			end if;
										
			if clear = '1' then
				irqselreg <= "0000000";
				istatusreg <= (others => '0');
          end if;
		end if; -- clk
		-- irqsel reg bit definitions:
		-- 0 --> 3 IRQselect - determine which host interrupt
		-- 4 --> irq ts enable - high to enable driver
		-- 5 --> unused
 	   	-- 6 --> irq mask - high to enable irq
		-- 7 --> irq (read only)
		for i in 3 to 15 loop 	-- note: IRQ 8 and 13 ar no-connects 
										-- so are forced to be tri-state
			if (i /= 8) and (i /= 13) and (irqselreg(3 downto 0) = i) and (irqselreg(4) = '1') then
            irqts(i) <= (irqgen and irqselreg(6));
			else
				irqts(i) <= 'Z';
			end if;
		end loop;
		
		irqs <= irqts;
		istatus <= istatusreg;
		
		if readport = '1' then
			obus(6 downto 0) <= irqselreg;
			obus(7) <= irqgen;
		else
			obus <= (others => 'Z');
		end if;

	end process;
end behavioral;
