library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- new pwmref with 16 bit phase accum and 8 bit and post scale
-- 2 decodes - 16 bit data bus
-- added sync logic so proc can catch up to missed samples

entity pwmref is
  generic (
  	 initialprescale : std_logic_vector(15 downto 0);
	 initialpostScale	: std_logic_vector(7 downto 0));
  				
  port (
    clk            : in  std_logic;
    refcount       : out std_logic_vector (7 downto 0);
    samplegen      : out std_logic;
    ibus           : in  std_logic_vector (15 downto 0);
    prescaleload   : in  std_logic;
    postscaleload  : in  std_logic;
    clearsamplegen : in  std_logic
    );
end pwmref;

architecture behavioral of pwmref is

  signal count          : std_logic_vector (8 downto 0);
  alias pwmrefcount     : std_logic_vector(7 downto 0) is count(7 downto 0); 
  alias toggle          : std_logic is count(8);   
  signal prescale       : std_logic_vector(16 downto 0);
  alias prescalemsb     : std_logic is prescale(16);
  signal oldprescalemsb : std_logic;
  signal postscale      : std_logic_vector(7 downto 0);
  signal prescalelat    : std_logic_vector(15 downto 0) := InitialPrescale;
  signal postscalelat   : std_logic_vector(7 downto 0) := InitialPostScale;
  signal sample         : std_logic;
  signal inc            : std_logic;
  signal dec            : std_logic;
  signal samplesync     : std_logic_vector(3 downto 0);
  signal countedge1     : std_logic;
  signal countedge2     : std_logic;
  signal symmode        : std_logic;

begin
  apwmref : process (clk, samplesync)
  begin
    if clk'event and clk = '1' then
      countedge2 <= countedge1;
      if clearsamplegen = '1' then
        dec <= '1';
      else
        dec <= '0';
      end if;

      if countedge1 = '0' and countedge2 = '1' then
        inc <= '1';
      else
        inc <= '0';
      end if;

      if inc = '0' and dec = '1' then -- decrement case
        if samplesync /= x"0" then
          samplesync <= samplesync -1;
        end if;
      end if;

      if inc = '1' and dec = '0' then -- increment case
        if samplesync /= x"F" then
          samplesync <= samplesync +1;
        end if;
      end if;

      prescale       <= prescale + prescalelat;
      oldprescalemsb <= prescalemsb;
      if oldprescalemsb /= prescalemsb then
        countedge1 <= '0';
        count <= count + 1;     
		  if count(7 downto 0) = 0 then	 -- possible 180 bug
          if postscale /= 1 then
            postscale <= postscale -1;
          end if;
          if postscale = 1 then
            countedge1 <= '1';
            postscale  <= postscalelat;
          end if;
        end if;
      end if;
      if prescaleload = '1' then
        prescalelat <= ibus;
      end if;
      if postscaleload = '1' then
        postscalelat <= ibus(7 downto 0);
        postscale    <= ibus(7 downto 0);
		  symmode <= ibus(15);
      end if;
    end if;  -- clk
 
    if samplesync = x"0" then
      sample <= '0';
    else
      sample <= '1';
    end if;

    if symmode = '0' then
		refcount  <= pwmrefcount;
    else
  		if toggle = '0' then
			refcount <= pwmrefcount;
		else
			refcount <= not pwmrefcount;
		end if;
    end if;			 

    samplegen <= sample; 	


  end process; 
end behavioral;

