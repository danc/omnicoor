library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Lookuptable is
    Port ( clk : in std_logic;
           ibus : in std_logic_vector(15 downto 0);
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic_vector(1 downto 0);
           selwadd : in std_logic;
		     readsel : in std_logic;
		     writesel : in std_logic);
end LookupTable;

architecture Behavioral of LookupTable is

component sinetab is
	port (
	addra: in std_logic_vector(8 downto 0);
	addrb: in std_logic_vector(8 downto 0);
	clk: in std_logic;
	dina: in std_logic_vector(7 downto 0);
	douta: out std_logic_vector(7 downto 0);
	doutb: out std_logic_vector(7 downto 0);
	wea: in std_logic);
end component sinetab;
	
	signal add: std_logic_vector(9 downto 0); 
 	signal data: std_logic_vector(7 downto 0);
	signal negdata: std_logic_vector(8 downto 0);
	signal write: std_logic;

begin




ltableram: sinetab  port map
	(
	addra => add(8 downto 0),
	addrb => "000000000",
	clk =>  clk,
 	dina =>  ibus(7 downto 0),
 	douta => data,
	wea =>   write

	);	

	alookuptable: process (clk,readsel,writesel,selradd,selwadd,add,data)
	begin
		if clk'event and clk = '1' then		
			if writesel = '1' and selwadd = '0' then 
				add <= ibus(9 downto 0);
			end if;		
		end if; -- clk		

		if writesel = '1' and selwadd = '1' then
			write <= '1';
		else
			write <= '0';
		end if;	
					
		negdata <= "100000000" - ("00" & data(7 downto 1));
		
		if	readsel = '1' then
			case selradd is
				when "00" =>
					obus(9 downto 0) <= add;
					obus(15 downto 10) <= "000000";
				when "01" =>
					obus(15 downto 8) <= data;
					obus(7 downto 0) <= "00000000";
				when "10" =>
					if add(9) = '0' then
						obus(15 downto 8) <= '0'& data(7 downto 1);						
					else
						obus(15 downto 8) <= negdata(7 downto 0);
					end if;						
					obus(7 downto 0) <= "00000000";
									
				when others => obus <= "ZZZZZZZZZZZZZZZZ";						
			end case;
		else
			obus <= "ZZZZZZZZZZZZZZZZ";
		end if;
	end process;
end Behavioral;
