library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hoststatb is
    Port ( clk : in std_logic;
           ibus: in std_logic_vector(15 downto 0);
		     obus : out std_logic_vector(15 downto 0);
           read : in std_logic;
			  write : in std_logic;
           hostread : in std_logic;
           hostdata : out  std_logic_vector(15 downto 0));
          
end hoststatb;

architecture Behavioral of hoststatb is
signal status : std_logic_vector(15 downto 0);

begin
	astatreg: process (clk,read,hostread,status)
	begin	
		if clk'event and clk = '1' then
			if write =  '1' then
				status <= ibus;
			end if;		
		end if;
		obus <= (others => 'Z');
		if read = '1' then
			obus <= status;
		end if;
		hostdata <= (others => 'Z');
		if hostread = '1' then
			hostdata <= status;
		end if;
	end process;	
		

end Behavioral;
