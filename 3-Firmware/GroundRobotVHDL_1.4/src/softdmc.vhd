		library IEEE;
use IEEE.std_logic_1164.all;            -- defines std_logic types
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;				-- needed for SP3 DCM
-- hardware rev 30 with 4 index CPU, mul64 div64 and sqrt64, 
-- constant generator, 3 PWMs per Axis, SineTable
-- FIFOed interface, support for Brush, Stepper, 3 Phase Brushless

     entity softdmc is
  		generic(
--------------------------4I34 4 axis 3 Phase 7I39------------------------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	  		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	  		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := true;
--			PIC7I60: boolean := false;
--			PCI: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;	
--			OnePinIRQ : boolean := false;
--			constant IRQPolarity : std_logic := '1';
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := true; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := true;
--			SevenI39_0SCount: boolean := true;	 -- no port AB	so sec counters on hall
--			SevenI39_1: boolean := true;
--			SevenI39_1SCount: boolean := true;
--			PortAB: boolean := false;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := false;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"    
---------------------------------------------------------------------

--------------------------4I34 3Phase KLUDGE (3phase on 7I30) -------
--			Naxis: integer := 2;
--			PagedMemMSA : integer := 9;  -- 2 axis needs 512B but Im lazy so we use 1K
--			Naxisrb: std_logic_vector := x"0002";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--  		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--  		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := true;
--			PIC7I60: boolean := false;
--			PCI: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;	
--			OnePinIRQ : boolean := false;
--			constant IRQPolarity : std_logic := '1';
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := true; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := true;
--			ENAKludge: boolean := true; -- needed on 7I32 and ThreePK
--			SevenI39_0:: boolean := false;
--			SevenI39_1: boolean := false;
--			PortAB: boolean := false;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"    

---------------------------------------------------------------------

--------------------------4I34 Brush motor -- 4 axis ----------------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	 		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	  		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := true;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := false;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			FourLEDS: boolean := false;
--			TwoLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"
    
---------------------------------------------------------------------

--------------------------4I34 Brush motor -- 8 axis ----------------
--			Naxis: integer := 8;
--			PagedMemMSA : integer := 10;  -- 8 axis needs 2K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0008";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := true;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	  		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			PC104: boolean := true;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := false;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := false;    -- no free pins for IO on 8 axis Brush motor
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := false;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"    

---------------------------------------------------------------------

--------------------------4I34 Stepper motor -- 4 axis --------------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	 		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	  		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := true;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := false;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := true;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := true;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := true; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := false;	 -- no free IO ports with 4 axis stepper
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := false;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"ffff";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"    

---------------------------------------------------------------------

--------------------------5I20/4I65 Brush motor -- 4 axis -----------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	  		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010"; 		
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := true;
--			PCI9054: boolean := false;
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '0';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := true;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			FourLEDS: boolean := false;
--			TwoLEDs: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"    

---------------------------------------------------------

--------------------------5I20/4I65 Brush motor -- 8 axis -----------
--			Naxis: integer := 8;
--			PagedMemMSA : integer := 10;  -- 8 axis needs 2K Paged axis memory
--			Naxisrb: std_logic_vector := x"0008";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := true;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := false;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--	  		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	  		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := true;
--			PCI9054: boolean := false;
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '0';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"    

-------------------------------------------------------------------

--------------------------5I20/4I65 4 axis 2 Phase stepper 7I32-----------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	  		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010"; 		
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := true;
--			PCI9054: boolean := false;
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '0';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := true;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := true;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := true; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"    
----------------------------------------------------------------

--------------------------5I20/4I65 4 axis 3 Phase 7I39-----------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	  		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010"; 		
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := true;
--			PCI9054: boolean := false;
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '0';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := true; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := true;
--			SevenI39_0SCount: boolean := false; -- we have sec counters on AB port
--			SevenI39_1: boolean := true;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"    

---------------------------------------------------------------------

--------------------------4I68/5I23 Brush motor -- 4 axis ----------------

			Naxis: integer := 4;
			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
			Naxisrb: std_logic_vector := x"0004";
			BigFIFOs: boolean := true;
			TinyFIFOs: boolean := false;
			HasDivider: boolean := true;
			HasSqrRoot: boolean := true;
			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"044A"; -- 72 MHz
			constant SynClkLow   : std_logic_vector(15 downto 0) := x"A200";
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"05B8"; -- 96 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"D800";
			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
			constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
			PC104: boolean := false;
			PIC7I60: boolean := false;
			PCI9030: boolean := false;
			PCI9054: boolean := true;
			KS8695: boolean := false;
			PPC5200: boolean := false;
			OnePinIRQ : boolean := true;
			constant IRQPolarity : std_logic := '0';
			FourI68: boolean := true;
			FourC81: boolean := false;
			DirectClock: boolean := false;
			BrushMotor: boolean := true;
			TwoPhase: boolean := false;
			ThreePhase: boolean := false; 
			SevenI32: boolean := false;
			ThreePK: boolean := false;
			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
			SevenI39_0: boolean := false;
			SevenI39_0SCount: boolean := false;
			SevenI39_1: boolean := false;
			SevenI39_1SCount: boolean := false;
			PortAB: boolean := true;
			PortCD: boolean := true;
			PortEF: boolean := false;
			ABSCount: boolean := true;
			Spartan2: boolean := false;
			Spartan3: boolean := true;
			OneLED: boolean := false;
			TwoLEDS: boolean := false;		--	5I23 has 2 leds
			FourLEDS: boolean := true;		-- 4I68 has 4 leds
			EightLEDS: boolean := false;
			Quadout: boolean := false;
			InitialPreScale: std_logic_vector(15 downto 0) := x"1234"; -- 20 KHz @72 MHz
			InitialPostscale: std_logic_vector(7 downto 0):= x"01";    
			DualPWM: boolean := true

--------------------------4I68/5I23 Brush motor -- 8 axis ----------------

--			Naxis: integer := 8;
--			PagedMemMSA : integer := 10;  -- 8 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0008";
--			BigFIFOs: boolean := true;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"044A"; -- 72 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"A200";
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"05B8"; -- 96 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"D800";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := true;
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '0';
--			FourI68: boolean := true;
--			FourC81: boolean := false;
--			DirectClock: boolean := false;
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := false;
--			Spartan3: boolean := true;
--			OneLED: boolean := false;
--			TwoLeds: boolean := true;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := false;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1234"; -- 20 KHz @72 MHz
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"    

--------------------------4I68 Stepper motor -- 4 axis ----------------

--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := true;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"044A"; -- 72 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"A200";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := true;
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '0';
--			FourI68: boolean := true;
--			FourC81: boolean := false;
--			DirectClock: boolean := false;
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := true;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := true;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := true; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := false;
--			Spartan3: boolean := true;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := true;
--			FourLEDS: boolean := true;
--			EightLEDS: boolean := false;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"DA74";  -- 240 KHz
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"      -- 30 KHz

--------------------------4I68 3 phase 7I39 -- 4 axis ----------------

--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := true;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"044A"; -- 72 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"A200";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := true;
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '0';
--			FourI68: boolean := true;
--			FourC81: boolean := false;
--			DirectClock: boolean := false;
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := true; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := true;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := true;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := false;
--			Spartan3: boolean := true;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := true;
--			EightLEDS: boolean := false;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1234";	-- 20 KHz
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"      -- 20 KHz


------------------------4C81 Brush motor -- 4 axis -----------------
--
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := true;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"03B9"; -- 62.5 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"ACA0";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;
--			KS8695: boolean := true;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := true;
--			DirectClock: boolean := false;
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := false;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := false;
--			Spartan2: boolean := false;
--			Spartan3: boolean := true;
--			OneLED: boolean := true;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := false;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"    


--------------------------PPC5200 Brush motor  --------------------------
--
--			Naxis: integer := 8;
--			PagedMemMSA : integer := 10;  -- 8 axis needs 2K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0008";
--			BigFIFOs: boolean := true;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"03EF"; -- 66.0 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"1480";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0400";
--			PC104: boolean := false;
--			PIC7I60: boolean := false;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;
--			KS8695: boolean := false;
--			PPC5200: boolean := true;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '0';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := false;
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := false;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := false;
--			Spartan2: boolean := false;
--			Spartan3: boolean := true;
--			OneLED: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := false;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"    

---------------------------------------------------------------------

-------------------------- 7I60 Brush motor - 4 axis  ---------------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	 		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := false;
--			PIC7I60: boolean := true;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;						
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := true;
--			PortEF: boolean := true;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			FourLEDS: boolean := false;
--			TwoLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"    

------------------------------------------------------------------------------

-------------------------- 7I60 Brush motor - 8 axis -------------------------
--			Naxis: integer := 8;
--			PagedMemMSA : integer := 10;  -- 8 axis needs 2K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0008";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := true;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := false;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			PC104: boolean := false;
--			PIC7I60: boolean := true;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;						
--			BrushMotor: boolean := true;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := true;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"01"    

-------------------------------------------------------------------------------

-------------------------- 7I60 Stepper motor - 8 axis -------------------------
--			Naxis: integer := 8;
--			PagedMemMSA : integer := 10;  -- 8 axis needs 2K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0008";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := true;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := false;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			PC104: boolean := false;
--			PIC7I60: boolean := true;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;						
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := true;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := true;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := true; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := false;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := false;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			FourLEDS: boolean := false;
--			TwoLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"ffff";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"    
--------------------------------------------------------------------------------

-------------------------- 7I60 Step motor - 4 axis -------------------------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	  		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := false;
--			PIC7I60: boolean := true;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;						
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := true;
--			ThreePhase: boolean := false; 
--			SevenI32: boolean := true;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := true; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := false;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := false;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := true; -- should be true
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"FBA9"; -- 192 KHz
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"     -- 24 KHz

--------------------------------------------------------------------------------

-------------------------- 7I60 3 phase BLDC - 8 axis -------------------------
--			Naxis: integer := 8;
--			PagedMemMSA : integer := 10;  -- 8 axis needs 2K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0008";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := true;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := false;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--			PC104: boolean := false;
--			PIC7I60: boolean := true;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;						
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := true; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := true;
--			SevenI39_1: boolean := true;
--			SevenI39_2: boolean := true;
--			SevenI39_3: boolean := true;
--			PortAB: boolean := false;
--			PortCD: boolean := false;
--			PortEF: boolean := false;
--			ABSCount: boolean := false;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"    


-------------------------- 7I60 3 phase BLDC - 4 axis -------------------------
--			Naxis: integer := 4;
--			PagedMemMSA : integer := 9;  -- 4 axis needs 1K Paged	axis memory
--			Naxisrb: std_logic_vector := x"0004";
--			BigFIFOs: boolean := false;
--			TinyFIFOs: boolean := false;
--			HasDivider: boolean := true;
--			HasSqrRoot: boolean := true;
--			constant SynClkHigh  : std_logic_vector(15 downto 0) := x"02FA"; -- 50 MHz
--			constant SynClkLow   : std_logic_vector(15 downto 0) := x"F080";
--			constant ICDFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			constant QCDFIFOSize  : std_logic_vector(15 downto 0) := x"0200";
--	 		constant IRBFIFOSize  : std_logic_vector(15 downto 0) := x"0010";
--	 		constant QRBFIFOSize  : std_logic_vector(15 downto 0) := x"0100";
--			PC104: boolean := false;
--			PIC7I60: boolean := true;
--			PCI9030: boolean := false;
--			PCI9054: boolean := false;			
--			KS8695: boolean := false;
--			PPC5200: boolean := false;
--			OnePinIRQ : boolean := true;
--			constant IRQPolarity : std_logic := '1';
--			FourI68: boolean := false;
--			FourC81: boolean := false;
--			DirectClock: boolean := true;						
--			BrushMotor: boolean := false;
--			TwoPhase: boolean := false;
--			ThreePhase: boolean := true; 
--			SevenI32: boolean := false;
--			ThreePK: boolean := false;
--			ENAKludge: boolean := false; -- needed on 7I32 and ThreePK
--			SevenI39_0: boolean := true;
--			SevenI39_0SCount: boolean := false;
--			SevenI39_1: boolean := true;
--			SevenI39_1SCount: boolean := false;
--			PortAB: boolean := true;
--			PortCD: boolean := true;
--			PortEF: boolean := false;
--			ABSCount: boolean := true;
--			Spartan2: boolean := true;
--			Spartan3: boolean := false;
--			OneLED: boolean := false;
--			TwoLEDS: boolean := false;
--			FourLEDS: boolean := false;
--			EightLEDS: boolean := true;
--			Quadout: boolean := false;
--			InitialPreScale: std_logic_vector(15 downto 0) := x"1a37";
--			InitialPostscale: std_logic_vector(7 downto 0):= x"08"    


		);
		port
    (
      -- PC104 bus interface signals --
      BALE      : in    std_logic;
      TC        : in    std_logic;
      BOSC      : in    std_logic;
      RSTDRV    : in    std_logic;
      REFRES    : in    std_logic;
      SBHE      : in    std_logic;
      MASTER    : in    std_logic;
      AEN       : in    std_logic;
      SMEMR     : in    std_logic;
      SMEMW     : in    std_logic;
      IORD      : in    std_logic;
      IOWR      : in    std_logic;
--      IOCHRDY: inout std_logic; 
--      MEMCS16: inout std_logic; 
      IOCS16    : inout std_logic;
      SA        : in    std_logic_vector (19 downto 0);  -- address bus
      SD        : inout std_logic_vector (15 downto 0);  -- data bus
      IRQ       : out   std_logic_vector (15 downto 3);  -- interrupts
      DACK      : in    std_logic_vector (7 downto 6);   -- dma acknowledges
      DRQ       : out   std_logic_vector (7 downto 6);   -- dma requests
      LBENA     : out   std_logic;
      LBDIR     : out   std_logic;
		CONFIGADDR	 : in    std_logic_vector (1 downto 0);

		-- Bridged PCI interface signals --
      LRD   : in  std_logic; -- 9030 only
      LWR   : in  std_logic; -- 9030 only
      LW_R  : in  std_logic;
      ALE   : in  std_logic; -- 9030 only
      ADS   : in  std_logic;
      BLAST : in  std_logic;
      WAITO : in  std_logic;
      LOCKO : in  std_logic;
      CS0   : in  std_logic;
      CS1   : in  std_logic;
      READY : out std_logic;
      INT   : out std_logic; -- also on other one pin IRQ designs
		DEN   : in std_logic;  -- 9054 only
		DREQ  : out std_logic;  -- 9054 only
		HOLD  : in std_logic;  -- 9054 only
		HOLDA : inout std_logic;  -- 9054 only
      LAD : inout std_logic_vector (31 downto 0);  -- data/address bus
      LA  : in    std_logic_vector (8 downto 2);   -- non-muxed address bus
      LBE : in    std_logic_vector (3 downto 0);   -- byte enables
      LCLK: in std_logic;

		-- KS8695 interface signals
		MD : inout std_logic_vector(31 downto 0);
      MA : in std_logic_vector(10 downto 2);
      EROEN : in std_logic;
      ERWEN : in std_logic;
      ECSN : in std_logic;
      LED : out std_logic;

		-- PPC5200 signals

		FPGA_LBDATA: inout std_logic_vector(15 downto 0);
		FPGA_LBADDR: in std_logic_vector(9 downto 0);
		FPGA_CS1: in std_logic;
		FPGA_RW: in std_logic;

     -- PIC /7I60 interface signals --
   	RW: in std_logic; 
	   ICLK: in std_logic; 
	   BUSY: out std_logic; 
		BEEP: out std_logic;
	   PICDATA: inout std_logic_vector (7 downto 0);
		CMCE: out std_logic;
		REB1: out std_logic;
		REB2: out std_logic;
		RTS1: out std_logic;
		RTS2: out std_logic;
		SPICLK: out std_logic;
		SPSO: out std_logic;
		TEB1: out std_logic;
		TEB2: out std_logic;
		TXD1: out std_logic;
		TXD2: out std_logic;
		TXDB1: out std_logic;
		TXDB2: out std_logic;
-- MOTION CONTROL AND MISC I/O

      SYNCLKIN  : in    std_logic;
      -- I/O Port bits
      PORTADATA : inout std_logic_vector (11 downto 0);
      PORTBDATA : inout std_logic_vector (11 downto 0);
      PORTCDATA : inout std_logic_vector (11 downto 0);
      PORTDDATA : inout std_logic_vector (11 downto 0);
      PORTEDATA : inout std_logic_vector (11 downto 0);
      PORTFDATA : inout std_logic_vector (11 downto 0);
		      -- on card debug LEDS
      LEDS      : out   std_logic_vector(7 downto 0);
      -- configuration decode disable
      DIS       : out   std_logic;
      -- motor control stuff
      A         : in    std_logic_vector (Naxis -1 downto 0);
      B         : in    std_logic_vector (Naxis -1 downto 0);
      IDX       : in    std_logic_vector (Naxis -1 downto 0);
      PWMA       : inout std_logic_vector (Naxis -1 downto 0);
      DIRA       : inout std_logic_vector (Naxis -1 downto 0);
      PWMB       : inout std_logic_vector (Naxis -1 downto 0);
      DIRB       : inout std_logic_vector (Naxis -1 downto 0);
      PWMC       : inout std_logic_vector (Naxis -1 downto 0);
      DIRC       : inout std_logic_vector (Naxis -1 downto 0);
		ENA        : out   std_logic_vector (Naxis -1 downto 0);
-- 7I32/3PK signals only
		ENAA       : out   std_logic_vector (Naxis -1 downto 0);
		ENAB       : out   std_logic_vector (Naxis -1 downto 0);
		ENAC       : out   std_logic_vector (Naxis -1 downto 0);
		CCA        : out   std_logic_vector (Naxis -1 downto 0);
		CCB        : out   std_logic_vector (Naxis -1 downto 0);
		ALTIDX     : in   std_logic_vector (Naxis -1 downto 0);

-- 7I39 signals only
		HALL0     : inout   std_logic_vector (4 downto 0);
		HALL1     : inout   std_logic_vector (4 downto 0);
		HALL2     : inout   std_logic_vector (4 downto 0);
		HALL3     : inout   std_logic_vector (4 downto 0);
		HALL4     : inout   std_logic_vector (4 downto 0);
		HALL5     : inout   std_logic_vector (4 downto 0);
		HALL6     : inout   std_logic_vector (4 downto 0);
		HALL7     : inout   std_logic_vector (4 downto 0)
      );
end softdmc;

--#################################################################
--#################################################################
--#################################################################
--#################################################################

 	


architecture dataflow of softdmc is

--  constant Naxisrb      : std_logic_vector(15 downto 0) := Naxis;


-- constant Revision     : std_logic_vector(15 downto 0) := x"B21F"; 	-- clear once option added to counters
																								-- clr countcleared option
																								-- initialized constants in axisram
																								-- signed 32x32 ==> 64 multiply
																								-- synchronize processor reset on PCI versions
  constant SignedMul64 : boolean := true;

  constant Revision     : std_logic_vector(15 downto 0) := x"B21F"; 	-- if signed multiply is true

--      constant decode : std_logic_vector (5 downto 0) := "100001";  -- 0x210 hex
	constant decodepc104_0 : std_logic_vector (5 downto 0) := "100010";  -- 0x220 hex
	constant decodePC104_1 : std_logic_vector (5 downto 0) := "100011";  -- 0x230 hex       
   constant decodePC104_2 : std_logic_vector (5 downto 0) := "100100";  -- 0x240 hex
   constant decodePC104_3 : std_logic_vector (5 downto 0) := "100101";  -- 0x250 hex       


-- PC/104 /misc interface related signals
  signal IOSelectR    : std_logic;     -- real time IO select decode (for read)
  signal IOSelectD    : std_logic;     -- delayed IO select decode	(for write)


  signal DecodePC104 : std_logic_vector (9 downto 4);                                        
  signal WordAccess    : std_logic;

  signal PreSALatch    : std_logic_vector (9 downto 2);     -- latched address      
  signal SALatch       : std_logic_vector (9 downto 2);     -- latched address
  signal PreAENLatch   : std_logic;
  signal AENLatch      : std_logic;  

  signal WriteStrobe  : std_logic;
  signal HostWriteD1   : std_logic;
  signal HostWriteD2   : std_logic;
  signal HostReadTE  : std_logic;
  signal HostReadD1   : std_logic;
  signal HostReadD2   : std_logic;

-- 7I60 related signals
  signal ICLKDel1  : std_logic;
  signal ICLKDel2  : std_logic;
  signal RWDel1  : std_logic;
  signal RWDel2  : std_logic;
  signal ICLKFall  : std_logic;
  signal ICLKRise  : std_logic;
  signal RWRise  : std_logic;
  signal Alatch  : std_logic_vector(7 downto 0);
  signal LowByteLatch  : std_logic_vector(7 downto 0);
  signal IOSelect  : std_logic;
  signal ReadStrobe  : std_logic;
  signal LowLatchRBSel : std_logic;

-- BridgedPCI interface related signals

  signal Add         : std_logic_vector (3 downto 0);
  signal PreFastRead : std_logic;
  signal FastRead    : std_logic;
  signal ReadReq     : std_logic;
  signal WriteReq    : std_logic;
  signal ClrIRQd1    : std_logic;
  signal ClrIRQd2    : std_logic;
  signal ClrIRQReq   : std_logic; 
-- KS8695/generic 16 bit interface signals
 
  signal PreWriteData   : std_logic_vector (15 downto 0);  -- internal write data first latch
  signal WriteData  	   : std_logic_vector (15 downto 0);  -- internal write data second latch
  signal PreADDRLatch   : std_logic_vector (15 downto 0);
  signal ADDRLatch      : std_logic_vector (15 downto 0);
  signal DBus           : std_logic_vector (15 downto 0);  -- internal read data bus
  signal FPGA_CS1d1     : std_logic;
  signal FPGA_CS1d2     : std_logic;
  signal PreAddr			: std_logic_vector(9 downto 0);
  signal LAddr			: std_logic_vector(9 downto 0);

-- general signals
  signal SYNCLK  : std_logic;
  signal CLK0  : std_logic;
  signal CLKFX  : std_logic;
  signal SysReset  : std_logic;
  signal HostReadIFIFO   : std_logic;
  signal HostReadQFIFO   : std_logic;
  signal HostWriteIFIFO   : std_logic;
  signal HostWriteQFIFO   : std_logic;
  signal HostReadIFIFOTE   : std_logic;
  signal HostReadQFIFOTE   : std_logic;	
  signal HostStatusBRead  : std_logic;

  signal WriteROMAddr  : std_logic;
  signal WriteROMData  : std_logic;
  signal ReadROMData  : std_logic;
  signal HostStatusRead : std_logic;
  signal HostROMAddr   : std_logic_vector (10 downto 0);
  signal HostROMData   : std_logic_vector (15 downto 0);
  signal PreDBReset    : std_logic;
  signal DBReset       : std_logic;
  signal EnableZ       : std_logic := '0';

-- internal CPU related signals
  signal LEDLatch      : std_logic_vector (7 downto 0);
  signal ia            : std_logic_vector(11 downto 0);
  signal id            : std_logic_vector(15 downto 0);
  signal mra           : std_logic_vector(10 downto 0);
  signal mwa           : std_logic_vector(10 downto 0);

  signal mi                  : std_logic_vector(15 downto 0);
  signal mo                  : std_logic_vector(15 downto 0);
  signal mw                  : std_logic;
  signal mipram              : std_logic_vector(15 downto 0);
  signal migram              : std_logic_vector(15 downto 0);
  signal pwrite              : std_logic;
  signal gwrite              : std_logic;
  signal dbcarry             : std_logic;
  signal ioradd              : std_logic_vector(9 downto 0);
  alias  iowsel               : std_logic is mwa(9);
  alias  iowdec               : std_logic_vector(7 downto 0) is mwa (7 downto 0);
  alias  iorsel               : std_logic is ioradd(9);
  alias  iordec               : std_logic_vector(7 downto 0) is ioradd (7 downto 0);
  signal PagedRAMRead     : std_logic;
  signal PagedRAMWrite    : std_logic;
  signal GlobalRAMRead    : std_logic;
  signal GlobalRAMWrite   : std_logic;
-- 4.X
  signal ICDFIFORead  : std_logic;
  signal QCDFIFORead   : std_logic;
  signal IRBFIFORead   : std_logic;
  signal QRBFIFORead   : std_logic;
  signal ICDFIFOWrite   : std_logic;
  signal QCDFIFOWrite   : std_logic;
  signal IRBFIFOWrite   : std_logic;
  signal QRBFIFOWrite   : std_logic;

--
  signal HStatusRegWriteSel  : std_logic;
  signal HStatusRegReadSel   : std_logic;
  signal StatusRegBWriteSel  : std_logic;
  signal StatusRegBReadSel   : std_logic;
  signal HostFIFOStatus		  : std_logic_vector (7 downto 0);
  signal PageRegReadSel      : std_logic;
  signal PageRegWriteSel     : std_logic;

  signal AxisRegRead      : std_logic;
  signal AxisRegWrite     : std_logic;
  signal AxisReg             : std_logic_vector (3 downto 0);
  signal AxisRegRB           : std_logic_vector (3 downto 0);
  signal PagedWriteAddress   : std_logic_vector (PagedMemMSA downto 0);
  signal PagedReadAddress    : std_logic_vector (PagedMemMSA downto 0);
  signal StatusRegReadSel    : std_logic;
  signal StatusRegWriteSel   : std_logic;
  signal MRegWriteSel        : std_logic;
  signal MRegReadSel         : std_logic;
  signal AlignReadSel        : std_logic;
  signal AlignWriteSel       : std_logic;

-- 4.X
  signal LMulRegWriteSel     : std_logic;
  signal LMulRegReadSel      : std_logic;
  signal LDivRegWriteSel     : std_logic;
  signal LDivRegReadSel      : std_logic;
  signal LSqrRegWriteSel     : std_logic;
  signal LSqrRegReadSel      : std_logic;

  signal PWMASel           : std_logic;
  signal DirASel           : std_logic;
  signal PWMBSel           : std_logic;
  signal DirBSel           : std_logic;
  signal PWMCSel           : std_logic;
  signal DirCSel           : std_logic;
  signal CCASel       : std_logic;
  signal CCBSel       : std_logic;
  signal AltIDXSel     : std_logic;
--
  signal Counter0ReadSel  : std_logic;
  signal Counter0WriteSel : std_logic;
  signal CCR0WriteSel     : std_logic;
  signal CCR0ReadSel      : std_logic;
  signal Counter0ClearSel : std_logic;
  signal Counter0CountClrdSel : std_logic;
  signal Counter1ReadSel  : std_logic;
  signal Counter1WriteSel : std_logic;
  signal CCR1WriteSel     : std_logic;
  signal CCR1ReadSel      : std_logic;
  signal Counter1ClearSel : std_logic;
  signal Counter1CountClrdSel : std_logic;
  signal QuadOutWriteSel  : std_logic;
  signal QuadOutReadSel   : std_logic;
  signal HallWriteSel	  : std_logic;
  signal HallWriteDDRSel  : std_logic;
  signal HallReadDDRSel   : std_logic;
  signal HallReadSel      : std_logic;
  signal ENASel           : std_logic;


--4.X
  signal PWMASelects           : std_logic_vector (naxis -1 downto 0);
  signal DirASelects           : std_logic_vector (naxis -1 downto 0);
  signal PWMBSelects           : std_logic_vector (naxis -1 downto 0);
  signal DirBSelects           : std_logic_vector (naxis -1 downto 0);
  signal PWMCSelects           : std_logic_vector (naxis -1 downto 0);
  signal DirCSelects           : std_logic_vector (naxis -1 downto 0);
  signal CCASelects       : std_logic_vector (naxis -1 downto 0);
  signal CCBSelects       : std_logic_vector (naxis -1 downto 0);
  signal AltIDXSelects     : std_logic_vector (naxis -1 downto 0);
  signal Counter0ReadSelects   : std_logic_vector (naxis -1 downto 0);
  signal Counter0WriteSelects  : std_logic_vector (naxis -1 downto 0);
  signal CCR0WriteSelects      : std_logic_vector (naxis -1 downto 0);
  signal CCR0ReadSelects       : std_logic_vector (naxis -1 downto 0);
  signal Counter0ClearSelects: std_logic_vector (naxis -1 downto 0);
  signal Counter0CountClrdSelects: std_logic_vector (naxis -1 downto 0);
  signal Counter1ReadSelects   : std_logic_vector (naxis -1 downto 0);
  signal Counter1WriteSelects  : std_logic_vector (naxis -1 downto 0);
  signal CCR1WriteSelects      : std_logic_vector (naxis -1 downto 0);
  signal CCR1ReadSelects       : std_logic_vector (naxis -1 downto 0);
  signal Counter1ClearSelects: std_logic_vector (naxis -1 downto 0);
  signal Counter1CountClrdSelects: std_logic_vector (naxis -1 downto 0);
  signal QuadOutWriteSelects   : std_logic_vector (naxis -1 downto 0);
  signal QuadOutReadSelects    : std_logic_vector (naxis -1 downto 0);
  signal ENASelects            : std_logic_vector (naxis -1 downto 0);
  signal LEna                  : std_logic_vector (naxis -1 downto 0);
  signal HallWriteSelects	       : std_logic_vector (naxis -1 downto 0);
  signal HallWriteDDRSelects	    : std_logic_vector (naxis -1 downto 0);
  signal HallReadDDRSelects       : std_logic_vector (naxis -1 downto 0);
  signal HallReadSelects       : std_logic_vector (naxis -1 downto 0);
  signal LookupReadSel  : std_logic;
  signal LookupWriteSel : std_logic;
  signal IrqDriveWriteSel : std_logic;
  signal IrqDriveReadSel  : std_logic;
  signal IRQSetSel        : std_logic;
  signal HostIRQClrSel    : std_logic;
  signal IRQStatus : std_logic_vector (7 downto 0); 

  signal TimerReadSel  : std_logic;
  signal TimerWriteSel : std_logic;

  signal BitConstSel  : std_logic;

  signal PortAWriteSel    : std_logic;
  signal PortAReadSel     : std_logic;
  signal PortAWriteDDRSel : std_logic;
  signal PortAReadDDRSel  : std_logic;

  signal PortBWriteSel    : std_logic;
  signal PortBReadSel     : std_logic;
  signal PortBWriteDDRSel : std_logic;
  signal PortBReadDDRSel  : std_logic;

  signal PortCWriteSel    : std_logic;
  signal PortCReadSel     : std_logic;
  signal PortCWriteDDRSel : std_logic;
  signal PortCReadDDRSel  : std_logic;

  signal PortDWriteSel    : std_logic;
  signal PortDReadSel     : std_logic;
  signal PortDWriteDDRSel : std_logic;
  signal PortDReadDDRSel  : std_logic;

  signal PortEWriteSel    : std_logic;
  signal PortEReadSel     : std_logic;
  signal PortEWriteDDRSel : std_logic;
  signal PortEReadDDRSel  : std_logic;

  signal PortFWriteSel    : std_logic;
  signal PortFReadSel     : std_logic;
  signal PortFWriteDDRSel : std_logic;
  signal PortFReadDDRSel  : std_logic;
-- 4.X
  signal StatusReg         : std_logic;
  signal SampleRate        : std_logic;
  signal RefCountBus       : std_logic_vector (7 downto 0);
  signal PreScaleWriteSel  : std_logic;
  signal PostScaleWriteSel : std_logic;
  
  signal tempPWM	: std_logic_vector (naxis-1 downto 0);
  signal tempDirA	: std_logic_vector (naxis-1 downto 0);
  signal dividedPWMCSelects : std_logic_vector (naxis -1 downto 0);
  
--#################################################################
--#################################################################
--#################################################################
--#################################################################
  function OneOfTwodecode(ena : std_logic; dec : std_logic) return std_logic_vector is
  variable result   : std_logic_vector(1 downto 0);
  begin
	if ena = '1' then
	  	case dec is
			when '0'   => result := "01";
			when '1'   => result := "10";
        	when others => result := "00";
      	end case;  
    else
		result := "00";
	end if;
	return result;
  end OneOfTwoDecode;

  function OneOffourdecode(ena : std_logic; dec : std_logic_vector(1 downto 0)) return std_logic_vector is
  variable result   : std_logic_vector(3 downto 0);
  begin
	if ena = '1' then
	  	case dec is
			when "00"   => result := "0001";
			when "01"   => result := "0010";
			when "10"   => result := "0100";
      	when "11"   => result := "1000";
        	when others => result := "0000";
      	end case;  
    else
		result := "0000";
	end if;
	return result;
  end OneOfFourDecode;

  function OneOfEightdecode(ena : std_logic; dec : std_logic_vector(2 downto 0)) return std_logic_vector is
  variable result   : std_logic_vector(7 downto 0);
  begin
	if ena = '1' then
	  	case dec is
			when "000"   => result := "00000001";
			when "001"   => result := "00000010";
			when "010"   => result := "00000100";
        	when "011"   => result := "00001000";
        	when "100"   => result := "00010000";
        	when "101"   => result := "00100000";
        	when "110"   => result := "01000000";
        	when "111"   => result := "10000000";
        	when others  => result := "00000000";
      	end case;  
    else
		result := "00000000";
	end if;
	return result;
  end OneOfEightDecode;


  component Sweet16cpu_qp is
    port (
      clk      : in  std_logic;
      reset    : in  std_logic;
      iabus    : out std_logic_vector(11 downto 0);
      idbus    : in  std_logic_vector(15 downto 0);
      mradd    : out std_logic_vector(10 downto 0);
      mwadd    : out std_logic_vector(10 downto 0);
      mibus    : in  std_logic_vector(15 downto 0);
      mobus    : out std_logic_vector(15 downto 0);
      mwrite   : out std_logic;
      carryflg : out std_logic
      );
  end component Sweet16cpu_qp;

  component axis4ram is
    port (
   	addra: in std_logic_vector(9 downto 0);
   	addrb: in std_logic_vector(9 downto 0);
   	clk: in std_logic;
   	dina: in std_logic_vector(15 downto 0);
    	doutb: out std_logic_vector(15 downto 0);
	   wea: in std_logic);  
	end component axis4ram;

  component axis8ram is
    port (
   	addra: in std_logic_vector(10 downto 0);
   	addrb: in std_logic_vector(10 downto 0);
   	clk: in std_logic;
   	dina: in std_logic_vector(15 downto 0);
    	doutb: out std_logic_vector(15 downto 0);
	   wea: in std_logic);  
	end component axis8ram;

  component ph1rom
    port (
      addra : in  std_logic_vector(9 downto 0);
      addrb : in  std_logic_vector(9 downto 0);
      clk  : in  std_logic;
      dina  : in  std_logic_vector(15 downto 0);
      douta : out std_logic_vector(15 downto 0);
      doutb : out std_logic_vector(15 downto 0);
      wea   : in  std_logic);
  end component ph1rom;

  component ph2rom
    port (
      addra : in  std_logic_vector(9 downto 0);
      addrb : in  std_logic_vector(9 downto 0);
      clk  : in  std_logic;
      dina  : in  std_logic_vector(15 downto 0);
      douta : out std_logic_vector(15 downto 0);
      doutb : out std_logic_vector(15 downto 0);
      wea   : in  std_logic);
  end component ph2rom;

  component ph3rom
    port (
      addra : in  std_logic_vector(9 downto 0);
      addrb : in  std_logic_vector(9 downto 0);
      clk  : in  std_logic;
      dina  : in  std_logic_vector(15 downto 0);
      douta : out std_logic_vector(15 downto 0);
      doutb : out std_logic_vector(15 downto 0);
      wea   : in  std_logic);
  end component ph3rom;

   component dpbr1kx16 is
	port (
	addra: IN std_logic_vector(9 downto 0);
	addrb: IN std_logic_vector(9 downto 0);
	clka: IN std_logic;
	clkb: IN std_logic;
	dina: IN std_logic_vector(15 downto 0);
	doutb: OUT std_logic_vector(15 downto 0);
	wea: IN std_logic);
   end component dpbr1kx16;

   component dpbr2kx16 is
	port (
	addra: IN std_logic_vector(10 downto 0);
	addrb: IN std_logic_vector(10 downto 0);
	clka: IN std_logic;
	clkb: IN std_logic;
	dina: IN std_logic_vector(15 downto 0);
	doutb: OUT std_logic_vector(15 downto 0);
	wea: IN std_logic);
   end component dpbr2kx16;

   component dpbr256x16 is
	port (
	addra: IN std_logic_vector(7 downto 0);
	addrb: IN std_logic_vector(7 downto 0);
	clka: IN std_logic;
	clkb: IN std_logic;
	dina: IN std_logic_vector(15 downto 0);
	doutb: OUT std_logic_vector(15 downto 0);
	wea: IN std_logic);
   end component dpbr256x16;
   
	component dpblockram256x16 is
    port (
      clk  : in  std_logic;
      we   : in  std_logic;
      adda : in  std_logic_vector(7 downto 0);
      addb : in  std_logic_vector(7 downto 0);
      din  : in  std_logic_vector(15 downto 0);
      outa : out std_logic_vector(15 downto 0);
      outb : out std_logic_vector(15 downto 0));
  end component dpblockram256x16;

  component mul16a32 is
    port (
      m       : in  std_logic_vector (15 downto 0);
      p       : out std_logic_vector (15 downto 0);
      mlatch  : in  std_logic;
      readm   : in  std_logic;
      selradd : in  std_logic_vector(2 downto 0);
      selwadd : in  std_logic_vector(2 downto 0);
      clk     : in  std_logic
      );
  end component mul16a32;
  
  component alignbox is
    port (
      din     : in  std_logic_vector (15 downto 0);
      dout    : out std_logic_vector (15 downto 0);
      writes  : in  std_logic;
      reads   : in  std_logic;
      selradd : in  std_logic_vector (1 downto 0);
      selwadd : in  std_logic_vector (1 downto 0);
      clk     : in  std_logic
      );
  end component alignbox;

  component dcounter is
    port (
      obus            : out std_logic_vector (15 downto 0);
      ibus            : in  std_logic_vector (9 downto 0);
      quada           : in  std_logic;
      quadb           : in  std_logic;
      index           : in  std_logic;
      ccrloadcmd      : in  std_logic;
      ccrreadcmd      : in  std_logic;
      countoutreadcmd : in  std_logic;
      countlatchcmd   : in  std_logic;
		countclearcmd   : in  std_logic;
	   clearcntclrdcmd  : in  std_logic;
	   clk             : in  std_logic
      );
  end component dcounter;

  component pwmgennx is
    port (
      clk        : in  std_logic;
      refcount   : in  std_logic_vector (7 downto 0);
      ibus       : in  std_logic_vector (7 downto 0);
      loadpwmval : in  std_logic;
      pwmout     : out std_logic
      );
  end component pwmgennx;

  component bitport is
    generic (InitialState:std_logic);
    port (
      clk        : in  std_logic;
      ibus       : in  std_logic;
      load 		  : in  std_logic;
      bitout     : out std_logic
      );
  end component bitport;
  
  component nbitport is
    port (
      clk        : in  std_logic;
      ibus       : in  std_logic;
      load 		  : in  std_logic;
      bitout     : out std_logic
      );
  end component nbitport;
  
  component bitinport is
    port ( 
           obus : out std_logic;
           read : in std_logic;
           bitin : in std_logic);
	end component bitinport;

  component pwmref is
    generic (
	          
		 InitialPreScale:std_logic_vector (15 downto 0);
		 InitialPostScale:std_logic_vector (7 downto 0)
		 );
	 port (
       clk            : in  std_logic;
       refcount       : out std_logic_vector (7 downto 0);
       samplegen      : out std_logic;
       ibus           : in  std_logic_vector (15 downto 0);
       prescaleload   : in  std_logic;
       postscaleload  : in  std_logic;
       clearsamplegen : in  std_logic
      );
  end component pwmref;

  component twelveport is
    port (
      clear    : in  std_logic;
      clk      : in  std_logic;
      ibus     : in  std_logic_vector (11 downto 0);
      obus     : out std_logic_vector (11 downto 0);
      loadport : in  std_logic;
      loadddr  : in  std_logic;
      readddr  : in  std_logic;
      portdata : out std_logic_vector (11 downto 0)
      );
  end component twelveport;

  component twelveiorb is
    port (
      obus     : out std_logic_vector (11 downto 0);
      readport : in  std_logic;
      portdata : in  std_logic_vector (11 downto 0));
  end component twelveiorb;

  component fiveport is
    port (
      clear    : in  std_logic;
      clk      : in  std_logic;
      ibus     : in  std_logic_vector (4 downto 0);
      obus     : out std_logic_vector (4 downto 0);
      loadport : in  std_logic;
      loadddr  : in  std_logic;
      readddr  : in  std_logic;
      portdata : out std_logic_vector (4 downto 0)
      );
  end component fiveport;

  component fiveiorb is
    port (
      obus     : out std_logic_vector (4 downto 0);
      readport : in  std_logic;
      portdata : in  std_logic_vector (4 downto 0));
  end component fiveiorb;

  component IrqDrive is
    port (
      clear    : in  std_logic;
      clk      : in  std_logic;
      ibus     : in  std_logic_vector (7 downto 0);
      obus     : out std_logic_vector (7 downto 0);
		hostibus : in  std_logic_vector (7 downto 0);
      loadport : in  std_logic;
      readport : in  std_logic;
      setirq   : in  std_logic;
      clrirq   : in  std_logic;
		istatus: out std_logic_vector (7 downto 0);
      irqs     : out std_logic_vector (15 downto 3)
      );
  end component IrqDrive;
  
  component OnePinIrqDrive is
    port (		
	   	clear: in std_logic;
			clk: in std_logic;
			ibus: in std_logic_vector (7 downto 0);
			obus: out std_logic_vector (7 downto 0);
			hostibus: in std_logic_vector (7 downto 0);
			loadport: in std_logic;
			readport: in std_logic;
			setirq: in std_logic;
			clrirq: in std_logic;
			istatus: out std_logic_vector (7 downto 0);
			irqpol: in std_logic;
			irq: out std_logic
 			);
   end component OnePinIrqDrive;

  component timer is
    port (
      obus          : out std_logic_vector (15 downto 0);
      timerreadcmd  : in  std_logic;
      timerlatchcmd : in  std_logic;
      timerclearcmd : in  std_logic;
      clk           : in  std_logic
      );
  end component timer;
	
	component mul64 is
   Port ( 
		ibus: in std_logic_vector (15 downto 0);
		obus: out std_logic_vector (15 downto 0);
		readstb: in std_logic;
		writestb: in std_logic;
		selradd: in std_logic_vector (2 downto 0);
		selwadd: in std_logic_vector (2 downto 0);
		clk: in std_logic
	 );
	end component mul64;

	component smul64 is
   Port ( 
		ibus: in std_logic_vector (15 downto 0);
		obus: out std_logic_vector (15 downto 0);
		readstb: in std_logic;
		writestb: in std_logic;
		selradd: in std_logic_vector (2 downto 0);
		selwadd: in std_logic_vector (2 downto 0);
		clk: in std_logic
	 );
	end component smul64;
	
	component div64 is
   Port ( 
		ibus: in std_logic_vector (15 downto 0);
		obus: out std_logic_vector (15 downto 0);
		readstb: in std_logic;
		writestb: in std_logic;
		selradd: in std_logic_vector (2 downto 0);
		selwadd: in std_logic_vector (2 downto 0);
		clk: in std_logic
	 );
	end component div64;

	component sqrt64 is
   Port ( 
		ibus: in std_logic_vector (15 downto 0);
		obus: out std_logic_vector (15 downto 0);
		readstb: in std_logic;
		writestb: in std_logic;
		selradd: in std_logic_vector (2 downto 0);
		selwadd: in std_logic_vector (2 downto 0);
		clk: in std_logic
	 );
	end component sqrt64;

   component bitconst is
   Port ( 
	dout: out std_logic_vector (15 downto 0);
	readst: in std_logic;
	readadd: in std_logic_vector (4 downto 0)
	 );
   end component bitconst;

  
	component Lookuptable is
	Port ( clk : in std_logic;
          ibus : in std_logic_vector(15 downto 0);
          obus : out std_logic_vector(15 downto 0);
          selradd : in std_logic_vector(1 downto 0);
          selwadd : in std_logic;
		    readsel : in std_logic;
		    writesel : in std_logic);
	end component LookupTable;


	 component CFIFO256 is
    Port ( clk : in std_logic;
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic_vector(1 downto 0);
           selwadd : in std_logic_vector(1 downto 0);
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : in std_logic_vector(15 downto 0);
			  hostwrite	: in std_logic;
			  full : out std_logic;
			  half_full : out std_logic
			  );
	 end component CFIFO256;

	 component CFIFO16 is
    Port ( clk : in std_logic;
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic_vector(1 downto 0);
           selwadd : in std_logic_vector(1 downto 0);
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : in std_logic_vector(15 downto 0);
			  hostwrite	: in std_logic;
			  full : out std_logic;
			  half_full : out std_logic
			  );
	 end component CFIFO16;

	 component CFIFO512 is
    Port ( clk : in std_logic;
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic_vector(1 downto 0);
           selwadd : in std_logic_vector(1 downto 0);
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : in std_logic_vector(15 downto 0);
			  hostwrite	: in std_logic;
			  full : out std_logic;
			  half_full : out std_logic
			  );
	 end component CFIFO512;

	 component CFIFO1024 is
    Port ( clk : in std_logic;
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic_vector(1 downto 0);
           selwadd : in std_logic_vector(1 downto 0);
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : in std_logic_vector(15 downto 0);
			  hostwrite	: in std_logic;
			  full : out std_logic;
			  half_full : out std_logic
			  );
	 end component CFIFO1024;

	component RFIFO16 is
	Port ( clk : in std_logic;
           ibus : in std_logic_vector(15 downto 0);
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic;
           selwadd : in std_logic;
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : out std_logic_vector(15 downto 0);
			  hostread	: in std_logic;
			  hostreadte : in std_logic;
			  empty: out std_logic;
			  half_full : out std_logic
			  );
	end component RFIFO16; 
	
	component RFIFO256 is
	Port ( clk : in std_logic;
           ibus : in std_logic_vector(15 downto 0);
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic;
           selwadd : in std_logic;
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : out std_logic_vector(15 downto 0);
			  hostread	: in std_logic;
			  hostreadte : in std_logic;
			   empty : out std_logic;
			  half_full : out std_logic
			  );
	end component RFIFO256;

	component RFIFO1024 is
	Port ( clk : in std_logic;
           ibus : in std_logic_vector(15 downto 0);
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic;
           selwadd : in std_logic;
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : out std_logic_vector(15 downto 0);
			  hostread	: in std_logic;
			  hostreadte : in std_logic;
			   empty : out std_logic;
			  half_full : out std_logic
			  );
	end component RFIFO1024;

	component  hoststat is 
	 port ( 
         obus : out std_logic_vector(15 downto 0);
           read : in std_logic;
           hostread : in std_logic;
           hostdata : out  std_logic_vector(15 downto 0);
           fifostat : in std_logic_vector(7 downto 0);
			  istatus : in std_logic_vector(7 downto 0));
	end component hoststat;

   component hoststatb is
    Port ( clk : in std_logic;
           ibus: in std_logic_vector(15 downto 0);
			  obus : out std_logic_vector(15 downto 0);
           read : in std_logic;
			  write : in std_logic;
           hostread : in std_logic;
           hostdata : out  std_logic_vector(15 downto 0));
          
    end component hoststatb;

	component quadouts32 is
   port ( 
	obus: out std_logic_vector (15 downto 0);
	ibus: in std_logic_vector (15 downto 0);
	ReadCmd: in std_logic;
	WriteCmd: in std_logic;
	ReadAddr: in std_logic_vector(2 downto 0);
	WriteAddr: in std_logic_vector(2 downto 0);
	A: out std_logic;
	B: out std_logic;
	clk: in std_logic
	);
	end component QuadOuts32;

--#################################################################
--#################################################################
--#################################################################
--#################################################################

begin  -- the whole ball-o-wax

BrushROMOnly: if BrushMotor generate  
  ProgROM : ph1rom port map(
    addra => HostROMAddr(9 downto 0),
    addrb => ia(9 downto 0),
    clk  => SynClk,
    dina  => WriteData,
    douta => HostROMData,
    doutb => id,
    wea   => WriteROMData
    );
end generate;

TwoPhaseROMOnly: if TwoPhase generate  
  ProgROM : ph2rom port map(
    addra => HostROMAddr(9 downto 0),
    addrb => ia(9 downto 0),
    clk  => SynClk,
    dina  => WriteData,
    douta => HostROMData,
    doutb => id,
    wea   => WriteROMData
    );
end generate;

ThreePhaseROMOnly: if ThreePhase generate  
  ProgROM : ph3rom port map(
    addra => HostROMAddr(9 downto 0),
    addrb => ia(9 downto 0),
    clk  => SynClk,
    dina  => WriteData,
    douta => HostROMData,
    doutb => id,
    wea   => WriteROMData
    );
end generate;


   AxisRAM4: if (Naxis = 4) generate   
	PagedDataRAM : axis4ram port map (
   	addra => PagedWriteAddress,
   	addrb => PagedReadAddress,
   	clk => SynClk,
   	dina  => mo,
    	doutb=> mipram,
	   wea   => pwrite		
    );

  GlobalDataRAM : dpblockram256x16 port map (
    clk  => SynClk,
    we   => gwrite,
    adda => mwa(7 downto 0),
    addb => mra(7 downto 0),
    din  => mo,
    outb => migram
    );  
end generate;

   AxisRAM8: if (Naxis = 8) generate   
	PagedDataRAM : axis8ram port map (
   	addra => PagedWriteAddress,
   	addrb => PagedReadAddress,
   	clk => SynClk,
   	dina  => mo,
    	doutb=> mipram,
	   wea   => pwrite		
    );

  GlobalDataRAM : dpblockram256x16 port map (
    clk  => SynClk,
    we   => gwrite,
    adda => mwa(7 downto 0),
    addb => mra(7 downto 0),
    din  => mo,
    outb => migram
    );  
end generate;


  CPU : Sweet16cpu_qp port map (
    clk      => SynClk,
    reset    => DBreset,
    iabus    => ia,
    idbus    => id,
    mradd    => mra,
    mwadd    => mwa,
    mibus    => mi,
    mobus    => mo,
    mwrite   => mw,
    carryflg => dbcarry
    );

  TheMultiplier : mul16a32 port map (
    m       => mo,
    p       => mi,
    mlatch  => MregWriteSel,
    readm   => MRegReadSel,
    selradd => ioradd(2 downto 0),
    selwadd => mwa(2 downto 0),
    clk     => synclk
    );

  thealignbox : AlignBox port map (
    din     => mo,
    dout    => mi,
    writes  => AlignWriteSel,
    reads   => AlignReadSel,
    selradd => ioradd(1 downto 0),
    selwadd => mwa(1 downto 0),
    clk     => synclk
    );

  makepcounters : for i in 0 to naxis -1 generate
    counterx : dcounter port map (
      obus            => mi,
      ibus            => mo(9 downto 0),
      quada           => A(i),
      quadb           => B(i),
      index           => Idx(i),
      ccrloadcmd      => CCR0WriteSelects(i),
      ccrreadcmd      => CCR0ReadSelects(i),
      countoutreadcmd => Counter0ReadSelects(i),
      countlatchcmd   => Counter0WriteSelects(i),
		countclearcmd   => Counter0ClearSelects(i),
		clearcntclrdcmd   => Counter0CountClrdSelects(i),
      clk             => SynClk
      );        
  end generate;
  
  PortAB0: if PortAB generate
  
   	porta : twelveport port map (
   	clear    => SysReset,
   	clk      => synclk,
   	ibus     => mo(11 downto 0),
   	obus     => mi(11 downto 0),
   	loadport => PortAWriteSel,
   	loadddr  => PortAWriteDDRSel,
   	readddr  => PortAReadDDRSel,
   	portdata => portAData
   	);  

  		portarb : twelveiorb port map (
    	obus     => mi(11 downto 0),
   	readport => PortAReadSel,
   	portdata => portAData
   	);

  		portb : twelveport port map (
    	clear    => SysReset,
    	clk      => synclk,
    	ibus     => mo(11 downto 0),
    	obus     => mi(11 downto 0),
    	loadport => PortBWriteSel,
    	loadddr  => PortBWriteDDRSel,
    	readddr  => PortBReadDDRSel,
    	portdata => portBData
    	);  

  		portbrb : twelveiorb port map (
    	obus     => mi(11 downto 0),
    	readport => PortBReadSel,
    	portdata => portBData
    	);

  end generate;

SecondaryCountersOnAB: if ABSCount generate
    counters0 : dcounter port map (
    obus            => mi,
    ibus            => mo(9 downto 0),
    quada           => PortBData(8),
    quadb           => PortBData(9),
    index           => PortBData(6),
    ccrloadcmd      => CCR1WriteSelects(0),
    ccrreadcmd      => CCR1ReadSelects(0),
    countoutreadcmd => Counter1ReadSelects(0),
    countlatchcmd   => Counter1WriteSelects(0),
	 countclearcmd   => Counter1ClearSelects(0),
	 clearcntclrdcmd => Counter1CountClrdSelects(0),
    clk             => SynClk
    );  
    counters1 : dcounter port map (
    obus            => mi,
    ibus            => mo(9 downto 0),
    quada           => PortBData(10),
    quadb           => PortBData(11),
    index           => PortBData(7),
    ccrloadcmd      => CCR1WriteSelects(1),
    ccrreadcmd      => CCR1ReadSelects(1),
    countoutreadcmd => Counter1ReadSelects(1),
    countlatchcmd   => Counter1WriteSelects(1),
	 countclearcmd   => Counter1ClearSelects(1),
	 clearcntclrdcmd => Counter1CountClrdSelects(1),	 
    clk             => SynClk
    );  
    counters2 : dcounter port map (
    obus            => mi,
    ibus            => mo(9 downto 0),
    quada           => PortAData(8),
    quadb           => PortAData(9),
    index           => PortAData(6),
    ccrloadcmd      => CCR1WriteSelects(2),
    ccrreadcmd      => CCR1ReadSelects(2),
    countoutreadcmd => Counter1ReadSelects(2),
    countlatchcmd   => Counter1WriteSelects(2),
	 countclearcmd   => Counter1ClearSelects(2),
	 clearcntclrdcmd => Counter1CountClrdSelects(2),
    clk             => SynClk
    );  
    counters3 : dcounter port map (
    obus            => mi,
    ibus            => mo(9 downto 0),
    quada           => PortAData(10),
    quadb           => PortAData(11),
    index           => PortAData(7),
    ccrloadcmd      => CCR1WriteSelects(3),
    ccrreadcmd      => CCR1ReadSelects(3),
    countoutreadcmd => Counter1ReadSelects(3),
    countlatchcmd   => Counter1WriteSelects(3),
	 countclearcmd   => Counter1ClearSelects(3),
	 clearcntclrdcmd => Counter1CountClrdSelects(3),
    clk             => SynClk
    );  
  end generate;

   PortCD0: if PortCD generate
  
   	portc : twelveport port map (
   	clear    => SysReset,
   	clk      => synclk,
   	ibus     => mo(11 downto 0),
   	obus     => mi(11 downto 0),
   	loadport => PortCWriteSel,
   	loadddr  => PortCWriteDDRSel,
   	readddr  => PortCReadDDRSel,
   	portdata => portCData
   	);  

  		portcrb : twelveiorb port map (
    	obus     => mi(11 downto 0),
   	readport => PortCReadSel,
   	portdata => portCData
   	);

  		portd : twelveport port map (
    	clear    => SysReset,
    	clk      => synclk,
    	ibus     => mo(11 downto 0),
    	obus     => mi(11 downto 0),
    	loadport => PortDWriteSel,
    	loadddr  => PortDWriteDDRSel,
    	readddr  => PortDReadDDRSel,
    	portdata => portDData
    	);  

  		portdrb : twelveiorb port map (
    	obus     => mi(11 downto 0),
    	readport => PortDReadSel,
    	portdata => portDData
    	);
	 end generate;

  PortEF0: if PortEF generate
  
   	porte : twelveport port map (
   	clear    => SysReset,
   	clk      => synclk,
   	ibus     => mo(11 downto 0),
   	obus     => mi(11 downto 0),
   	loadport => PortEWriteSel,
   	loadddr  => PortEWriteDDRSel,
   	readddr  => PortEReadDDRSel,
   	portdata => portEData
   	);  

  		porterb : twelveiorb port map (
    	obus     => mi(11 downto 0),
   	readport => PortEReadSel,
   	portdata => portEData
   	);

  		portf : twelveport port map (
    	clear    => SysReset,
    	clk      => synclk,
    	ibus     => mo(11 downto 0),
    	obus     => mi(11 downto 0),
    	loadport => PortFWriteSel,
    	loadddr  => PortFWriteDDRSel,
    	readddr  => PortFReadDDRSel,
    	portdata => portFData
    	);  

  		portdrb : twelveiorb port map (
    	obus     => mi(11 downto 0),
    	readport => PortFReadSel,
    	portdata => portFData
    	);
	 end generate;


  First7I39: if SevenI39_0 generate
  
   	hall0 : fiveport port map (
   	clear    => SysReset,
   	clk      => synclk,
   	ibus     => mo(4 downto 0),
   	obus     => mi(4 downto 0),
   	loadport => HallWriteSelects(0),
   	loadddr  => HallWriteDDRSelects(0),
   	readddr  => HallReadDDRSelects(0),
   	portdata => HALL0(4 downto 0)
   	);  

  		hall0rb : fiveiorb port map (
    	obus     => mi(4 downto 0),
   	readport => HallReadSelects(0),
   	portdata =>  HAll0
   	);
   	
		hall1 : fiveport port map (
   	clear    => SysReset,
   	clk      => synclk,
   	ibus     => mo(4 downto 0),
   	obus     => mi(4 downto 0),
   	loadport => HallWriteSelects(1),
   	loadddr  => HallWriteDDRSelects(1),
   	readddr  => HallReadDDRSelects(1),
   	portdata => HALL1
   	);  

  		hall1rb : fiveiorb port map (
    	obus     => mi(4 downto 0),
   	readport => HallReadSelects(1),
   	portdata =>  HAll1
   	);
		
	end generate;
  
  SCOnFirst7I39: if SevenI39_0SCount generate
  		  	
    counters0 : dcounter port map (
    obus            => mi,
    ibus            => mo(9 downto 0),
    quada           => HALL0(0),
    quadb           => HALL0(1),
    index           => HALL0(2),
    ccrloadcmd      => CCR1WriteSelects(0),
    ccrreadcmd      => CCR1ReadSelects(0),
    countoutreadcmd => Counter1ReadSelects(0),
    countlatchcmd   => Counter1WriteSelects(0),
	 countclearcmd   => Counter1ClearSelects(0),
	 clearcntclrdcmd => Counter1CountClrdSelects(0),
    clk             => SynClk
    );  
    counters1 : dcounter port map (
    obus            => mi,
    ibus            => mo(9 downto 0),
    quada           => HALL1(0),
    quadb           => HALL1(1),
    index           => HALL1(2),
    ccrloadcmd      => CCR1WriteSelects(1),
    ccrreadcmd      => CCR1ReadSelects(1),
    countoutreadcmd => Counter1ReadSelects(1),
    countlatchcmd   => Counter1WriteSelects(1),
	 countclearcmd   => Counter1ClearSelects(1),
	 clearcntclrdcmd => Counter1CountClrdSelects(1),
    clk             => SynClk
    );  
 
  end generate;


  Second7I39: if SevenI39_1 generate
  
   	hall2 : fiveport port map (
   	clear    => SysReset,
   	clk      => synclk,
   	ibus     => mo(4 downto 0),
   	obus     => mi(4 downto 0),
   	loadport => HallWriteSelects(2),
   	loadddr  => HallWriteDDRSelects(2),
   	readddr  => HallReadDDRSelects(2),
   	portdata => HALL2(4 downto 0)
   	);  

  		hall2rb : fiveiorb port map (
    	obus     => mi(4 downto 0),
   	readport => HallReadSelects(2),
   	portdata =>  HAll2
   	);
   	
		hall3 : fiveport port map (
   	clear    => SysReset,
   	clk      => synclk,
   	ibus     => mo(4 downto 0),
   	obus     => mi(4 downto 0),
   	loadport => HallWriteSelects(3),
   	loadddr  => HallWriteDDRSelects(3),
   	readddr  => HallReadDDRSelects(3),
   	portdata => HALL3
   	);  

  		hall3rb : fiveiorb port map (
    	obus     => mi(4 downto 0),
   	readport => HallReadSelects(3),
   	portdata =>  HAll3
   	);
		  	
   end generate;

  SCOnSecond7I39: if SevenI39_1SCount generate
  
    counters2 : dcounter port map (
    obus            => mi,
    ibus            => mo(9 downto 0),
    quada           => HALL2(0),
    quadb           => HALL2(1),
    index           => HALL2(2),
    ccrloadcmd      => CCR1WriteSelects(2),
    ccrreadcmd      => CCR1ReadSelects(2),
    countoutreadcmd => Counter1ReadSelects(2),
    countlatchcmd   => Counter1WriteSelects(2),
    countclearcmd   => Counter1ClearSelects(2),
	 clearcntclrdcmd => Counter1CountClrdSelects(2),
    clk             => SynClk
    );  
    counters3 : dcounter port map (
    obus            => mi,
    ibus            => mo(9 downto 0),
    quada           => HALL3(0),
    quadb           => HALL3(1),
    index           => HALL3(2),
    ccrloadcmd      => CCR1WriteSelects(3),
    ccrreadcmd      => CCR1ReadSelects(3),
    countoutreadcmd => Counter1ReadSelects(3),
    countlatchcmd   => Counter1WriteSelects(3),
	 countclearcmd   => Counter1ClearSelects(3),
	 clearcntclrdcmd => Counter1CountClrdSelects(3),
    clk             => SynClk
    );  
 
  end generate;
  
-- ********* Changes start here ********************
-- Also added a DualPWM boolean in the 4 axis 4i68 configuration  
-- Added tempPWM and tempDiraA signals above
  
  
  
  DualPWMPart: if DualPWM generate
		makePWMS : for i in 0 to naxis-1 generate
			checkDir: process(tempDirA(i))
			begin
				if (tempDirA(i)='0') then
					PWMA(i) <= tempPWM(i);
					PWMB(i) <= '0';
				else
					PWMB(i) <= tempPWM(i);	-- Can be any output, such as DIRA, as long as the ucf has those pins mapped
					PWMA(i) <= '0';
				end if;
			end process;
		end generate;
  end generate;
  
  NoDualPWM : if (DualPWM=false) generate
	dontMakePWMs : for i in 0 to naxis-1 generate
		checkDir: process(tempDirA(i))
		begin
			PWMA(i) <= tempPWM(i);
			DIRA(i) <= tempDirA(i);
		end process;
	end generate;
  end generate;
 
	BrushMotorOnly1: if BrushMotor generate
			makeDIRAs : for i in 0 to naxis-1 generate
			dirporta : bitport 
			generic map (InitialState => '0')
			port map (
			clk        => SynClk,
			ibus       => mo(15),
			load       => DIRASelects(i), 
			bitout     => tempDirA(i)
			);
		end generate;
			makepwmAs : for i in 0 to naxis-1 generate
			pwmgenx : pwmgennx port map (
			clk        => SynClk,
			refcount   => RefCountBus,
			ibus       => mo(15 downto 8),
			loadpwmval => PWMASelects(i),
			pwmout     => tempPWM(i)
			);
		end generate;
			makepwmCs : for i in 0 to naxis-1 generate
			   signal divideSampleRate        : std_logic;
				signal divideRefCountBus       : std_logic_vector (7 downto 0);
--				signal dividedCounter : std_logic_vector(31 downto 0);
			begin
			
			pwmgenx : pwmgennx port map (
			clk        => SynClk,
			refcount   => divideRefCountBus,
			ibus       => mo(15 downto 8),
			loadpwmval => PWMCSelects(i),
			pwmout     => PWMC(i)
			);
			
			slowPWMrefcount : pwmref 
			generic map (
			initialprescale => x"0064",
			initialpostscale  => x"C8")
			port map (
			clk            => SynClk,
			refcount       => divideRefCountBus,
			SampleGen      => divideSampleRate,
			ibus           => mo,
			prescaleload   => '0',
			postscaleload  => '0',
			clearsampleGen => StatusRegWriteSel
			); 
			
--			clockDivide: process(PWMCSelects(i), SynClk)
--			begin
--				if SynClk'event and SynClk = '1' then
--					if PWMCSelects(i) = '1' then
--						dividedCounter <= dividedCounter + 1;
--						if dividedCounter = x"00000046" then
--							dividedPWMCSelects(i) <= PWMCSelects(i);
--							dividedCounter <= (others => '0');
--						else
--							dividedPWMCSelects(i) <= '0';
--						end if;
--					end if;
--				end if;
--			end process;
			
		end generate;
 	end generate;
	
-- ************* Changes end here *****************
	
	SevenI32Only1: if SevenI32 generate
		makeDIRAs : for i in 0 to naxis-1 generate
   	dirporta : bitport 
		   generic map (InitialState => '0')		
			port map (
      	clk        => SynClk,
      	ibus       => mo(9),
      	load       => DIRASelects(i), 
      	bitout     => DirA(i)
    		);
  		end generate;
		makeDIRBs : for i in 0 to naxis-1 generate
   	dirportb : bitport 
		   generic map (InitialState => '0')			
			port map (
      	clk        => SynClk,
      	ibus       => mo(9),
      	load       => DIRBSelects(i), 
      	bitout     => DirB(i)
    		);
  		end generate;
	end generate;

 
 	Enables0: if not ENAKludge generate
   	makeENAs : for i in 0 to naxis-1 generate
    	ENAPortx : nbitport port map (
      	clk        => SynClk,
      	ibus       => mo(0),
      	load       => ENASelects(i), 
      	bitout     => ENA(i)
    		);
   	end generate;
 	end generate;

 	Enables1: if ENAKludge generate  -- This is for 7I32 to enable multiple	ENA
												-- outputs from single port output bit
   	makeENAs : for i in 0 to naxis-1 generate
    	ENAPortx : nbitport port map (
      	clk        => SynClk,
      	ibus       => mo(0),
      	load       => ENASelects(i), 
      	bitout    => lena(i)
    		);
   	end generate;
	end generate;
 	
	SevenI320: if SevenI32 generate
   	makeCCAs : for i in 0 to naxis-1 generate
    	CCAPortx : bitport 
		   generic map (InitialState => '1')
			port map (
      	clk        => SynClk,
      	ibus       => mo(0),
      	load       => CCASelects(i), 
      	bitout    => CCA(i)
    		);
   	end generate;

		makeCCBs : for i in 0 to naxis-1 generate
    	CCBPortx : bitport 
			generic map (InitialState => '1')
			port map (
      	clk        => SynClk,
      	ibus       => mo(0),
      	load       => CCBSelects(i), 
      	bitout    => CCB(i)
    		);
   	end generate;

	   makeAIDXss : for i in 0 to naxis-1 generate
    	AltIDXPortx : bitinport port map ( 
      	obus       => mi(0),
      	read       => AltIDXSelects(i), 
      	bitin    => AltIDX(i)
    		);
   	end generate;
 	end generate;

 	
	TwophaseOnly2: if TwoPhase generate		 
  		makepwmAs : for i in 0 to naxis-1 generate
    		pwmgenx : pwmgennx port map (
      	clk        => SynClk,
      	refcount   => RefCountBus,
      	ibus       => mo(15 downto 8),
      	loadpwmval => PWMASelects(i),
      	pwmout     => PWMA(i)
      	);
			end generate;
  		makepwmBs : for i in 0 to naxis-1 generate
    		pwmgenx : pwmgennx port map (
      	clk        => SynClk,
      	refcount   => RefCountBus,
      	ibus       => mo(15 downto 8),
      	loadpwmval => PWMBSelects(i),
      	pwmout     => PWMB(i)
      	);
  			end generate;
	 end generate;
	
	ThreephaseOnly2: if ThreePhase generate			 
  			makepwmAs : for i in 0 to naxis-1 generate
    			pwmgenx : pwmgennx port map (
      		clk        => SynClk,
      		refcount   => RefCountBus,
      		ibus       => mo(15 downto 8),
      		loadpwmval => PWMASelects(i),
      		pwmout     => PWMA(i)
      		);
  			end generate;	
  			makepwmBs : for i in 0 to naxis-1 generate
    			pwmgenx : pwmgennx port map (
      		clk        => SynClk,
      		refcount   => RefCountBus,
      		ibus       => mo(15 downto 8),
      		loadpwmval => PWMBSelects(i),
      		pwmout     => PWMB(i)
      		);
  			end generate;	
			makepwmCs : for i in 0 to naxis-1 generate
   	 		pwmgenx : pwmgennx port map (
      		clk        => SynClk,
      		refcount   => RefCountBus,
      		ibus       => mo(15 downto 8),
      		loadpwmval => PWMCSelects(i),
      		pwmout     => PWMC(i)
      		);
			end generate;		
		   
			ThreePK1: if ThreePk generate
				makeDIRAs : for i in 0 to naxis-1 generate
   		   	dirporta : bitport 
		   		generic map (InitialState => '0')					
					port map (
     	 			clk        => SynClk,
     	 			ibus       => mo(9),
     	 			load       => DIRASelects(i), 
      			bitout     => DirA(i)
    				);		
	  			end generate;
			
				makeDIRBs : for i in 0 to naxis-1 generate
   				dirportb : bitport 
		   		generic map (InitialState => '0')										
					port map (
      			clk        => SynClk,
      			ibus       => mo(9),
      			load       => DIRBSelects(i), 
      			bitout     => DirB(i)
    				); 
				end generate;
				 	
				makeDIRCs : for i in 0 to naxis-1 generate
   				dirportc : bitport 
		   		generic map (InitialState => '0')										
					port map (
     	 			clk        => SynClk,
     	 			ibus       => mo(9),
     	 			load       => DIRCSelects(i), 
      			bitout     => DirC(i)
    				);  						
				end generate;
 			end generate;
		end generate;
		
			Makequadout: if Quadout generate

			   quadout0: quadouts32 port map ( 
				obus           => mi,
				ibus           => mo,
				ReadCmd        => QuadOutReadSelects(0),
				WriteCmd       => QuadOutWriteSelects(0),
				ReadAddr 		=> ioradd(2 downto 0),
				WriteAddr 		=> mwa(2 downto 0),
				A					=> PortBData(0),
				B					=> PortBData(1),
				clk             => SynClk
				);

			   quadout1: quadouts32 port map ( 
				obus           => mi,
				ibus           => mo,
				ReadCmd        => QuadOutReadSelects(1),
				WriteCmd       => QuadOutWriteSelects(1),
				ReadAddr 		=> ioradd(2 downto 0),
				WriteAddr 		=> mwa(2 downto 0),
				A					=> PortBData(2),
				B					=> PortBData(3),
				clk             => SynClk
				);
	
--			   quadout2: quadouts32 port map ( 
--				obus           => mi,
--				ibus           => mo,
--				ReadCmd        => QuadOutReadSelects(2),
--				WriteCmd       => QuadOutWriteSelects(2),
--				ReadAddr 		=> ioradd(2 downto 0),
--				WriteAddr 		=> mwa(2 downto 0),
--				A					=> PortAData(0),
--				B					=> PortAData(1),
--				clk             => SynClk
--				);

--			   quadout3: quadouts32 port map ( 
--				obus           => mi,
--				ibus           => mo,
--				ReadCmd        => QuadOutReadSelects(3),
--				WriteCmd       => QuadOutWriteSelects(3),
--				ReadAddr 		=> ioradd(2 downto 0),
--				WriteAddr 		=> mwa(2 downto 0),
--				A					=> PortAData(2),
--				B					=> PortAData(3),
--				clk             => SynClk
--				);

		 end generate;

  pwmrefcount : pwmref 
    generic map (
	 initialprescale => InitialPreScale,
	 initialpostscale  => InitialPostscale)     
	 port map (
    clk            => SynClk,
    refcount       => RefCountBus,
    SampleGen      => SampleRate,
    ibus           => mo,
    prescaleload   => PrescaleWriteSel,
    postscaleload  => PostScaleWriteSel,
    clearsampleGen => StatusRegWriteSel
    ); 

  PC104only0: if PC104 generate
  TheIrqDrive : IrqDrive port map (
    clear    => SysReset,
    clk      => SynClk,
    ibus     => mo(7 downto 0),
    obus     => mi(7 downto 0),
	 hostibus => WriteData(7 downto 0),
    loadport => IrqDriveWriteSel,
    readport => IrqDriveReadSel,
    setirq   => IRQSetSel,
    clrirq   => HostIRQClrSel,
	 istatus  => IRQStatus,
    irqs     => IRQ
    );
  end generate;

  AOnePinIRQ: if OnePinIRQ generate
    TheIrqDrive : OnePinIrqDrive port map (
      clear    => SysReset,
      clk      => SynClk,
      ibus     => mo(7 downto 0),
      obus     => mi(7 downto 0),  
   	hostibus => WriteData(7 downto 0),
      loadport => IrqDriveWriteSel,
      readport => IrqDriveReadSel,
      setirq   => IRQSetSel,
      clrirq   => HostIRQClrSel,
      istatus  => IRQStatus,
		irqpol   => IRQPolarity,
      irq      => INT
    );
  end generate;

  Thecycletimer : timer port map (
    obus          => mi,
    timerreadcmd  => TimerReadSel,
    timerlatchcmd => TimerWriteSel,
    timerclearcmd => SampleRate,
    clk           => SynClk
    );
	

  AUnsignedMul64: if not SignedMul64 generate
	Theumul64 : mul64 port map( 
		ibus     => mo,
		obus     => mi,
		readstb  => LMulRegReadSel,
		writestb => LMulRegWriteSel,
		selradd  => ioradd(2 downto 0),
		selwadd  => mwa(2 downto 0),
		clk      => synclk
		 ); 
  end generate;
  
  ASignedMul64: if SignedMul64 generate
	Theumul64 : smul64 port map( 
		ibus     => mo,
		obus     => mi,
		readstb  => LMulRegReadSel,
		writestb => LMulRegWriteSel,
		selradd  => ioradd(2 downto 0),
		selwadd  => mwa(2 downto 0),
		clk      => synclk
		 ); 
  end generate;		 

  A64bitDivider: if HasDivider generate
	Thediv64 : div64 port map( 
		ibus     => mo,
		obus     => mi,
		readstb  => LDivRegReadSel,
		writestb => LDivRegWriteSel,
		selradd  => ioradd(2 downto 0),
		selwadd  => mwa(2 downto 0),
		clk      => synclk
		 ); 
	end generate;

  A64bitRoot: if HasSqrRoot generate

	Thesqrt64 : sqrt64 port map( 
		ibus     => mo,
		obus     => mi,
		readstb  => LSqrRegReadSel,
		writestb => LSqrRegWriteSel,
		selradd  => ioradd(2 downto 0),
		selwadd  => mwa(2 downto 0),
		clk      => synclk
		 ); 
	end generate;

	TheBitconst: bitconst port map( 
		dout     => mi,
		readst   => BitConstSel,
		readadd  => ioradd(4 downto 0)
	 );
 		
  NeedsTable: if TwoPhase or ThreePhase generate
	SineLookup: lookuptable port map(
		clk     => synclk,
      ibus     => mo,
		obus     => mi,
		selradd  => ioradd(1 downto 0),
		selwadd  => mwa(0),
		readsel =>	LookupReadSel,
		writesel	 => LookupWriteSel
	   );
	end generate;


 		TinyFIFOs1: if TinyFIFOs generate	
 		ICDFIFO: CFIFO16 port map(
	 		  clk     		=> synclk,
           obus         => mi,
           selradd  		=> ioradd(1 downto 0),
           selwadd  		=> mwa(1 downto 0),
		     readsel		=> ICDFIFORead,
		     writesel		=> ICDFIFOWrite,
			  hostdata		=> WriteData,
			  hostwrite		=> HostWriteIFIFO,
			  full			=> HostFIFOStatus(7),
			  half_full		=> HostFIFOStatus(6)
			  );
	 
		TableQCD: if threePhase or TwoPhase generate	-- need sine table so no room for 256 deep QCD 
			QCDFIFO: CFIFO16 port map(
				clk     		=> synclk,
				obus        => mi,
				selradd  	=> ioradd(1 downto 0),
				selwadd  	=> mwa(1 downto 0),
				readsel		=> QCDFIFORead,
				writesel		=> QCDFIFOWrite,
				hostdata		=> WriteData,
				hostwrite	=> HostWriteQFIFO,
				full			=> HostFIFOStatus(5),
				half_full	=> HostFIFOStatus(4)
				);
		end generate;
		
		NoTableQCD: if BrushMotor generate			-- no sine table so room for 256 deep QCD
			QCDFIFO: CFIFO256 port map(
				clk     		=> synclk,
				obus        => mi,
				selradd  	=> ioradd(1 downto 0),
				selwadd  	=> mwa(1 downto 0),
				readsel		=> QCDFIFORead,
				writesel		=> QCDFIFOWrite,
				hostdata		=> WriteData,
				hostwrite	=> HostWriteQFIFO,
				full			=> HostFIFOStatus(5),
				half_full	=> HostFIFOStatus(4)
				);
		end generate;

		IRBFIFO:  RFIFO16 port map( 
           clk	     		=> synclk,
           ibus	 		=>	mo,
           obus			=>	mi,
           selradd	   => ioradd(0),
           selwadd  		=> mwa(0),
		     readsel		=> IRBFIFORead,
		     writesel	   => IRBFIFOWrite,
			  hostdata		=> DBus,
			  hostread		=> HostReadIFIFO,
			  hostreadte	=> HostReadIFIFOTE,
			  empty			=> HostFIFOStatus(3),
			  half_full		=> HostFIFOStatus(2)
			  );

		QRBFIFO:  RFIFO16 port map( 
           clk	     		=> synclk,
           ibus	 		=>	mo,
           obus			=>	mi,
           selradd	   => ioradd(0),
           selwadd  		=> mwa(0),
		     readsel		=> QRBFIFORead,
		     writesel	   => QRBFIFOWrite,
			  hostdata		=> DBus,
			  hostread		=> HostReadQFIFO,
			  hostreadte	=> HostReadQFIFOTE,
			  empty			=> HostFIFOStatus(1),
			  half_full		=> HostFIFOStatus(0)
			  );

end generate;
			    
 		MediumFIFOs: if (not BigFIFOs) and (not TinyFIFOs) generate	
 		
		
		Tablem: if ThreePhase or TwoPhase generate 
		
		ICDFIFO: CFIFO256 port map(
	 		  clk     		=> synclk,
           obus         => mi,
           selradd  		=> ioradd(1 downto 0),
           selwadd  		=> mwa(1 downto 0),
		     readsel		=> ICDFIFORead,
		     writesel		=> ICDFIFOWrite,
			  hostdata		=> WriteData,
			  hostwrite		=> HostWriteIFIFO,
			  full			=> HostFIFOStatus(7),
			  half_full		=> HostFIFOStatus(6)
			  );
	end generate;
	
		NoTablem: if BrushMotor generate 
		
		ICDFIFO: CFIFO512 port map(
	 		  clk     		=> synclk,
           obus         => mi,
           selradd  		=> ioradd(1 downto 0),
           selwadd  		=> mwa(1 downto 0),
		     readsel		=> ICDFIFORead,
		     writesel		=> ICDFIFOWrite,
			  hostdata		=> WriteData,
			  hostwrite		=> HostWriteIFIFO,
			  full			=> HostFIFOStatus(7),
			  half_full		=> HostFIFOStatus(6)
			  );
	end generate;	
	
		QCDFIFO: CFIFO512 port map(
	 		  clk     		=> synclk,
           obus         => mi,
           selradd  		=> ioradd(1 downto 0),
           selwadd  		=> mwa(1 downto 0),
		     readsel		=> QCDFIFORead,
		     writesel		=> QCDFIFOWrite,
			  hostdata		=> WriteData,
			  hostwrite		=> HostWriteQFIFO,
			  full			=> HostFIFOStatus(5),
			  half_full		=> HostFIFOStatus(4)
			  );

		IRBFIFO:  RFIFO16 port map( 
           clk	     		=> synclk,
           ibus	 		=>	mo,
           obus			=>	mi,
           selradd	   => ioradd(0),
           selwadd  		=> mwa(0),
		     readsel		=> IRBFIFORead,
		     writesel	   => IRBFIFOWrite,
			  hostdata		=> DBus,
			  hostread		=> HostReadIFIFO,
			  hostreadte	=> HostReadIFIFOTE,
			  empty			=> HostFIFOStatus(3),
			  half_full		=> HostFIFOStatus(2)
			  );

		QRBFIFO:  RFIFO256 port map( 
           clk	     		=> synclk,
           ibus	 		=>	mo,
           obus			=>	mi,
           selradd	   => ioradd(0),
           selwadd  		=> mwa(0),
		     readsel		=> QRBFIFORead,
		     writesel	   => QRBFIFOWrite,
			  hostdata		=> DBus,
			  hostread		=> HostReadQFIFO,
			  hostreadte	=> HostReadQFIFOTE,
			  empty			=> HostFIFOStatus(1),
			  half_full		=> HostFIFOStatus(0)
			  );

end generate;

BigFIFOs1: if BigFIFOs generate

		ICDFIFO: CFIFO1024 port map(
	 		  clk     		=> synclk,
           obus         => mi,
           selradd  		=> ioradd(1 downto 0),
           selwadd  		=> mwa(1 downto 0),
		     readsel		=> ICDFIFORead,
		     writesel		=> ICDFIFOWrite,
			  hostdata		=> WriteData,
			  hostwrite		=> HostWriteIFIFO,
			  full			=> HostFIFOStatus(7),
			  half_full		=> HostFIFOStatus(6)
			  );
	 
		QCDFIFO: CFIFO1024 port map(
	 		  clk     		=> synclk,
           obus         => mi,
           selradd  		=> ioradd(1 downto 0),
           selwadd  		=> mwa(1 downto 0),
		     readsel		=> QCDFIFORead,
		     writesel		=> QCDFIFOWrite,
			  hostdata		=> WriteData,
			  hostwrite		=> HostWriteQFIFO,
			  full			=> HostFIFOStatus(5),
			  half_full		=> HostFIFOStatus(4)
			  );

	IRBFIFO:  RFIFO1024 port map( 
           clk	     		=> synclk,
           ibus	 		=>	mo,
           obus			=>	mi,
           selradd	   => ioradd(0),
           selwadd  		=> mwa(0),
		     readsel		=> IRBFIFORead,
		     writesel	   => IRBFIFOWrite,
			  hostdata		=> DBus,
			  hostread		=> HostReadIFIFO,
			  hostreadte	=> HostReadIFIFOTE,
			  empty			=> HostFIFOStatus(3),
			  half_full		=> HostFIFOStatus(2)
			  );

	QRBFIFO:  RFIFO1024 port map( 
           clk	     		=> synclk,
           ibus	 		=>	mo,
           obus			=>	mi,
           selradd	   => ioradd(0),
           selwadd  		=> mwa(0),
		     readsel		=> QRBFIFORead,
		     writesel	   => QRBFIFOWrite,
			  hostdata		=> DBus,
			  hostread		=> HostReadQFIFO,
			  hostreadte	=> HostReadQFIFOTE,
			  empty			=> HostFIFOStatus(1),
			  half_full		=> HostFIFOStatus(0)
			  );

end generate;
	 
	ahoststat: hoststat port map( 

           obus			=>	mi,
           read			=> HStatusRegReadSel,
           hostread		=> HostStatusRead,
           hostdata		=> DBus,
           fifostat		=> HostFIFOStatus,
			  istatus		=> IRQStatus
			  );

    ahoststatb: hoststatb port map(  
           clk	     		=> synclk,
           ibus	 		=>	mo,
			  obus			=>	mi, 
           read			=> StatusRegBReadSel,
			  write			=> StatusRegBWriteSel,
           hostread	   => HostStatusBRead,
           hostdata		=> DBus
			  );        

	FourI68_0: if FourI68 generate	-- on 4I68 internal CPU clock runs at 48MHz X3/2 = 72 MHz		 
	 
   ClockMult : DCM
   generic map (
      CLKDV_DIVIDE => 2.0,
                          
      CLKFX_DIVIDE => 2, 
      CLKFX_MULTIPLY => 3,			-- 4 FOR 96 ,3 FOR 72
      CLKIN_DIVIDE_BY_2 => FALSE, 
      CLKIN_PERIOD => 20.08333,          
      CLKOUT_PHASE_SHIFT => "NONE", 
      CLK_FEEDBACK => "1X",         
      DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS", 
                                            
      DFS_FREQUENCY_MODE => "LOW",
      DLL_FREQUENCY_MODE => "LOW",
      DUTY_CYCLE_CORRECTION => TRUE,
      FACTORY_JF => X"C080",
      PHASE_SHIFT => 0, 
      STARTUP_WAIT => FALSE)
   port map (
 
      CLK0 => CLK0,   	-- 
      CLKFB => CLK0,  	-- DCM clock feedback
		CLKFX => CLKFX,
      CLKIN => LCLK,    -- Clock input (from IBUFG, BUFG or DCM)
      PSCLK => '0',   	-- Dynamic phase adjust clock input
      PSEN => '0',     	-- Dynamic phase adjust enable input
      PSINCDEC => '0', 	-- Dynamic phase adjust increment/decrement
      RST => '0'        -- DCM asynchronous reset input
   );
  
  BUFG_inst : BUFG
   port map (
      O => SYNCLK,    -- Clock buffer output
      I => CLKFX      -- Clock buffer input
   );
end generate;
  -- End of DCM_inst instantiation

	FourC81_0: if FourC81 generate	-- on 4C81, internal clock runs at 125MHz/2 = 62.5 MHz		 
	 
	ClockMult : DCM
   generic map (
      CLKDV_DIVIDE => 2.0,
                          
      CLKFX_DIVIDE => 4, 
      CLKFX_MULTIPLY => 2,
      CLKIN_DIVIDE_BY_2 => FALSE, 
      CLKIN_PERIOD => 20.0833,          
      CLKOUT_PHASE_SHIFT => "NONE", 
      CLK_FEEDBACK => "1X",         
      DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS", 
                                            
      DFS_FREQUENCY_MODE => "LOW",
      DLL_FREQUENCY_MODE => "LOW",
      DUTY_CYCLE_CORRECTION => TRUE,
      FACTORY_JF => X"C080",
      PHASE_SHIFT => 0, 
      STARTUP_WAIT => FALSE)
   port map (
 
      CLK0 => CLK0,   -- 
      CLKFB => CLK0,  -- DCM clock feedback
		CLKFX => CLKFX,
      CLKIN => SYNCLKIN,   -- Clock input (from IBUFG, BUFG or DCM)
      PSCLK => '0',   		-- Dynamic phase adjust clock input
      PSEN => '0',     		-- Dynamic phase adjust enable input
      PSINCDEC => '0', 		-- Dynamic phase adjust increment/decrement
      RST => '0'        		
									-- DCM asynchronous reset input
   );
  
  BUFG_inst : BUFG
   port map (
      O => SYNCLK,    -- Clock buffer output
      I => CLKFX      -- Clock buffer input
   );
end generate;

   -- End of DCM_inst instantiation

	PPC5200_0: if PPC5200 generate	-- on PPC5200 we use 33 MHZ and double it to = 66 MHz		 
	 
	ClockMult : DCM
   generic map (
      CLKDV_DIVIDE => 2.0,
                          
      CLKFX_DIVIDE => 2, 
      CLKFX_MULTIPLY => 4,
      CLKIN_DIVIDE_BY_2 => FALSE, 
      CLKIN_PERIOD => 20.0833,          
      CLKOUT_PHASE_SHIFT => "NONE", 
      CLK_FEEDBACK => "1X",         
      DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS", 
                                            
      DFS_FREQUENCY_MODE => "LOW",
      DLL_FREQUENCY_MODE => "LOW",
      DUTY_CYCLE_CORRECTION => TRUE,
      FACTORY_JF => X"C080",
      PHASE_SHIFT => 0, 
      STARTUP_WAIT => FALSE)
   port map (
 
      CLK0 => CLK0,   -- 
      CLKFB => CLK0,  -- DCM clock feedback
		CLKFX => CLKFX,
      CLKIN => SYNCLKIN,    -- Clock input (from IBUFG, BUFG or DCM)
      PSCLK => '0',   -- Dynamic phase adjust clock input
      PSEN => '0',     -- Dynamic phase adjust enable input
      PSINCDEC => '0', -- Dynamic phase adjust increment/decrement
      RST => '0'        -- DCM asynchronous reset input
   );
  
  BUFG_inst : BUFG
   port map (
      O => SYNCLK,    -- Clock buffer output
      I => CLKFX      -- Clock buffer input
   );
end generate;

   -- End of DCM_inst instantiation

 
-- Then the processes

--#################################################################
--#################################################################
--#################################################################
--#################################################################
 


UseDirectCLock: if DirectClock generate  -- 
	ClockSource : process(SYNCLKIN)
  	begin
  		SYNCLK <= SYNCLKIN;
	end process;
end generate;
			 

-- First the PC104/ISA bus decodes

  PC104Only: if PC104 generate	

  Decode104 : process (SynClk, AEN, AENLatch, IOSelectR, IOSelectD, IORD, IOWR, SA, SALatch)
  	begin
		
  		DRQ <= "ZZ";

--  	DIS <= '1';                     -- uncomment to disable config decode
    	DIS <= '0';                     -- uncomment to enable config decode

		SysReset <= RSTDRV;
		WordAccess <= '1';
		if SynClk'event and SynClk = '1' then  -- for IORD and IOWR trailing edge detection
			HostReadD2 <= HostReadD1;  
			HostReadD1 <= IORD;
		
			HostWriteD2 <= HostWriteD1;  
			HostWriteD1 <= IOWR;
		
			AENLatch <= PreAENLatch;
  			PreAENLatch <= AEN;  
       
		 	WriteData    <= PreWriteData;
       	PreWriteData <= SD;
      
		 	SALatch    <= PreSALatch;
       	PreSALatch <= SA(9 downto 2);  

			if WriteROMAddr = '1' then
				HostROMAddr <= WriteData(10 downto 0);
				EnableZ <= '1';
				DBReset <= WriteData(15);  			
			end if;
		end if; -- clk
		
		HostIRQClrSel <= WriteData(14) and WriteROMAddr;

		if HostWriteD2 = '0' and HostWriteD1 = '1' then  -- host write strobe on trailing edge of IOWR
			WriteStrobe <= '1';
		else
			WriteStrobe <= '0';
		end if;
	
		if HostReadD2 = '0' and HostReadD1 = '1' then  -- host read trailing edge for FIFO pop
			HostReadTE <= '1';
		else
			HostReadTE <= '0';
		end if;		 
		
		case CONFIGADDR is
			when "00" => DecodePC104 <= DecodePC104_0;
			when "01" => DecodePC104 <= DecodePC104_1;	 
			when "10" => DecodePC104 <= DecodePC104_2;
			when "11" => DecodePC104 <= DecodePC104_3;
			when others => null;
		end case;

		if SA(9 downto 4) = DecodePC104 and AEN = '0' and SA(0) = '0' then -- card select for read is decoded from real time PC/104 signals
			IOSelectR <= '1';
		else
			IOSelectR <= '0';
		end if;
		if SALatch(9 downto 4) = DecodePC104 and AENlatch = '0' then -- card select for writes and ReadTE is decoded from delayed PC/104 signals
			IOSelectD <= '1';											  -- since actual write or ReadTE is done after trailing edge of IOWR/IORD
		else
			IOSelectD <= '0';
		end if;
	  
		if SA(3 downto 2) = "00" and IOSelectR = '1' and IORD = '0' and EnableZ = '1' then -- offset 0
			HostReadIFIFO <= '1';
		else
			HostReadIFIFO <= '0';
		end if;
		if SAlatch(3 downto 2) = "00" and IOSelectD = '1' and HostReadTE = '1' and EnableZ = '1' then -- offset 0
			HostReadIFIFOTE <= '1';
		else
			HostReadIFIFOTE <= '0';
		end if;

		if SALatch(3 downto 2) = "00" and IOSelectD = '1' and WriteStrobe = '1' and EnableZ = '1' then
			HostWriteIFIFO <= '1';
		else
			HostWriteIFIFO <= '0';
		end if;

		if SA(3 downto 2) = "01" and IOSelectR = '1' and IORD = '0' then -- offset 4
			HostReadQFIFO <= '1';
		else
			HostReadQFIFO <= '0';
		end if;
		if SALatch(3 downto 2) = "01" and IOSelectD = '1' and HostReadTE = '1' then -- offset 4
			HostReadQFIFOTE <= '1';
		else
			HostReadQFIFOTE <= '0';
		end if;

		if SALatch(3 downto 2) = "01" and IOSelectD = '1' and WriteStrobe = '1'  then
			HostWriteQFIFO <= '1';
		else
			HostWriteQFIFO <= '0';
		end if;

		if SALatch(3 downto 2) = "10" and IOSelectD = '1' and WriteStrobe = '1' then					-- offset 8
			WriteROMAddr <= '1';
		else
			WriteROMAddr <= '0';
		end if;
		if SA(3 downto 2) = "10" and IOSelectR = '1' and IORD = '0' then					-- offset 8
			HostStatusRead <= '1';
		else
			HostStatusRead <= '0';
		end if;

		if SA(3 downto 2) = "11" and IOSelectR = '1' and IORD = '0' and DBReset = '1' then					-- offset C
			ReadROMData <= '1';
		else
			ReadROMData <= '0';
		end if;

		if SA(3 downto 2) = "11" and IOSelectR = '1' and IORD = '0' and DBReset = '0' then					-- offset C
			HostStatusBRead <= '1';
		else
			HostStatusBRead <= '0';
		end if;
		
		if SALatch(3 downto 2) = "11" and IOSelectD = '1' and WriteStrobe = '1'  and DBReset = '1' then					-- offset C
			WriteROMData <= '1';
		else
			WriteROMData <= '0';
		end if;

	DBus <= "ZZZZZZZZZZZZZZZZ";
	if ReadROMData = '1' then
		DBus <= HostROMData;
 	end if;
    
	end process;


  GenIOCS : process (IOSelectR, WordAccess)
  begin
    if (IOSelectR = '1') and WordAccess = '1' then
      IOCS16 <= '0';
    else
      IOCS16 <= 'Z';
    end if;
  end process GenIOCS;

  SDDrivers : process (IOSelectR, DBus, IORD, WordAccess)
  begin
    if (IOSelectR = '1') and IORD = '0' then
      if WordAccess = '1' then
        SD <= DBus;
      else
        SD(7 downto 0)  <= DBus(7 downto 0);
        SD(15 downto 8) <= "ZZZZZZZZ";
      end if;
    else
      SD <= "ZZZZZZZZZZZZZZZZ";
    end if;
  end process SDDrivers;

  localbuffer : process (IOSelectR, IORD)
  begin
    
    if IOSelectR = '1' then
      LBENA <= '0';
    else
      LBENA <= '1';
    end if;

    if (IOSelectR = '1') and (IORD = '0') then
      LBDIR <= '0';
    else
      LBDIR <= '1';
    end if;
  end process;

end generate;

--#################################################################
--#################################################################
--#################################################################
--#################################################################
-- next the 7I60 interface

  PIC7I60only: if PIC7I60 generate	

  StrobesAndMisc7I60 : process (SynClk )
  
 	begin
    	if synclk'event and synclk = '1' then
      	ICLKDel1 <= ICLK;
      	ICLKDel2 <= ICLKDel1;
			RWDel1 <= RW;
			RWDel2 <= RWDel1;
  
      	if ICLKRise = '1' and RW = '1' then
       		Alatch <= PICDATA;
      	end if;
			if ICLKFall = '1' and RW = '1' and Alatch(0) = '0' then
         	LowByteLatch <= PICDATA; -- latch the low data byte 
      	end if;
		
			if WriteROMAddr = '1' then
				HostROMAddr <= WriteData(10 downto 0);
				DBReset <= WriteData(15);  			
			end if;  
		
		end if; -- clk

      if ICLKDel1 = '0' and ICLKDel2 = '1' then	 	--ICLK fall signals write if RW is high
      	ICLKFall <= '1';
      else
			ICLKFall <= '0';
      end if;

      if ICLKDel1 = '1' and ICLKDel2 = '0' then 	--ICLK rise signals address latch time
			ICLKRise <= '1';
      else
			ICLKRise <= '0';
		end if;

      if RWDel1 = '1' and RWDel2 = '0' then  -- RW rising end signals end of read
        	RWRise <= '1';
      else
			RWRise <= '0';
		end if;

	   if ICLKFall = '1' and RW = '1'  and Alatch(0) = '1' then  -- host write strobe on trailing edge of IOWR
   		WriteStrobe <= '1';
	   else
	   	WriteStrobe <= '0';
   	end if;
	
		if RWRise = '1'  then  -- host read trailing edge for FIFO pop
			HostReadTE <= '1';
		else
			HostReadTE <= '0';
		end if;		 

		ReadStrobe <= not RW;

   	if LowlatchRBSel = '1' and RW = '0' then	 -- read back low byte for diagnostics
      	DBus(7 downto 0) <= LowByteLatch;
		else
      	DBus(7 downto 0) <= "ZZZZZZZZ";
 		end if;
  	end process;

  	DATADrivers : process (DBus, IOSelect, RW)
  	begin
		if IOSelect = '1' and RW = '0' then -- on a PIC read
      	if ICLK = '1' then
				PICDATA <= DBus(7 downto 0);	 -- if clk high return low byte
			else
				PICDATA <= DBus(15 downto 8);	 -- if clk low return high byte
			end if;
    	else
      	PICDATA <= "ZZZZZZZZ";
    	end if;
    	WriteData(7 downto 0) <= LowByteLatch;
	 	WriteData(15 downto 8)  <= PICDATA;
	end process DATADrivers;

  	HostDecode : process (Alatch, ICLKFall, IOSelect, RW)
  	begin
		if Alatch (7 downto 5) = "101" then
      	IOSelect <= '1';
    	else
      	IOSelect <= '0';
    	end if;
	
		HostIRQClrSel <= WriteData(14) and WriteROMAddr;
  
		if ALatch(4 downto 2) = "000" and IOSelect = '1' and ReadStrobe = '1' then -- offset 0
			HostReadIFIFO <= '1';
		else
			HostReadIFIFO <= '0';
		end if;

		if ALatch(4 downto 2) = "000" and IOSelect = '1' and HostReadTE = '1'  then -- offset 0
			HostReadIFIFOTE <= '1';
		else
			HostReadIFIFOTE <= '0';
		end if;

		if ALatch(4 downto 2) = "000" and IOSelect = '1' and WriteStrobe = '1' then
			HostWriteIFIFO <= '1';
		else
			HostWriteIFIFO <= '0';
		end if;

		if ALatch(4 downto 2) = "001" and IOSelect = '1' and ReadStrobe = '1' then -- offset 0
			HostReadQFIFO <= '1';
		else
			HostReadQFIFO <= '0';
		end if;

		if ALatch(4 downto 2) = "001" and IOSelect = '1' and HostReadTE = '1'  then -- offset 0
			HostReadQFIFOTE <= '1';
		else
			HostReadQFIFOTE <= '0';
		end if;

		if ALatch(4 downto 2) = "001" and IOSelect = '1' and WriteStrobe = '1' then
			HostWriteQFIFO <= '1';
		else
			HostWriteQFIFO <= '0';
		end if;

		if ALatch(4 downto 2) = "010" and IOSelect = '1' and WriteStrobe = '1' then					-- offset 8
			WriteROMAddr <= '1';
		else
			WriteROMAddr <= '0';
		end if;
		HostIRQClrSel <= WriteData(14) and WriteROMAddr;
		
		if ALatch(4 downto 2) = "010" and  IOSelect = '1' and ReadStrobe = '1'  then					-- offset 8
			HostStatusRead <= '1';
		else
			HostStatusRead <= '0';
		end if;

		if ALatch(4 downto 2) = "011" and  IOSelect = '1' and ReadStrobe = '1' and DBReset = '1' then					-- offset C
			ReadROMData <= '1';
		else
			ReadROMData <= '0';
		end if;

		if ALatch(4 downto 2) = "011" and IOSelect = '1' and ReadStrobe = '1'  and DBReset = '0' then					-- offset C
			HostStatusBRead <= '1';
		else
			HostStatusBRead <= '0';
		end if;
		
		if ALatch(4 downto 2) = "011" and IOSelect= '1' and WriteStrobe = '1'  and DBReset = '1' then					-- offset C
			WriteROMData <= '1';
		else
			WriteROMData <= '0';
		end if;

		if ALatch(4 downto 2) = "100" and IOSelect = '1' and ReadStrobe = '1'  then					-- offset C
			LowlatchRBSel <= '1';
		else
			LowlatchRBSel <= '0';
		end if;
		
		DBus <= "ZZZZZZZZZZZZZZZZ";
		if ReadROMData = '1' then
			DBus <= HostROMData;
 		end if;
    
	end process;

	tieuploose7I60bits : process ( Synclk)
	begin
		BUSY <= '0';
		BEEP <= '0';
		CMCE <= 'Z';
		REB1 <= '0';
		REB2 <= '0';
		RTS1 <= 'Z';
		RTS2 <= 'Z';
		SPICLK <= 'Z';
		SPSO <= 'Z';
		TEB1 <= 'Z';
		TEB2 <= 'Z';
		TXD1 <= 'Z';
		TXD2 <= 'Z';
		TXDB1 <= 'Z';
		TXDB2 <= 'Z';
	end process;

end generate;

--#################################################################
--#################################################################
--#################################################################
--#################################################################
-------------------------------PCI 9030------------------------------------
PCI9030Only: if PCI9030 generate	
-- The local bus decodes

  Decode9030PCI : process(Add, FastRead, LWR, LClk)
  begin
		SysReset <= '0';
		if LClk'event and LClk = '1' then -- this is local bus (33 MHz clock)
			
			
			if LWR = '0' and BLAST = '0' then	 -- end of write
				WriteReq <= '1';
				WriteData <= LAD(15 downto 0);	-- we always save the write data
			end if;
			
			if LRD = '0' and BLAST = '0' then -- end of read
				ReadReq <= '1';
			end if;				 
			
			if WriteROMAddr = '1' and LWR = '0' and BLAST = '0' then
				HostROMAddr <= LAD(10 downto 0);
				EnableZ <= '1';
				PreDBReset <= LAD(15); 
				ClrIRQReq <= LAD(14); 			
			end if;
		end if; -- lclk
		
		if SynClk'event and SynClk = '1' then
			HostWrited2 <= HostWrited1;
			HostReadd2 <= HostReadd1;
			HostWrited1 <= WriteReq;
			HostReadd1 <= ReadReq;
			ClrIRQD1 <= ClrIRQReq;
			ClrIRQd2 <= ClrIRQd1;
			DBReset <= PreDBReset;
		end if;

		if ClrIRQd2 = '1' and ClrIRQD1 = '1' then  -- clear IRQ on trailing edge of write
			HostIRQClrSel <= '1';
			ClrIRQReq <= '0';  -- async clear
		else
			HostIRQClrSel <= '0';
		end if;
		
		if HostWriteD2 = '1' and HostWriteD1 = '1' then  -- host write strobe on trailing edge of write
			WriteStrobe <= '1';
			WriteReq <= '0';  -- async clear
		else
			WriteStrobe <= '0';
		end if;
			
		if HostReadD2 = '1' and HostReadD1 = '1' then  -- host read trailing edge for FIFO pop
			HostReadTE <= '1';
			ReadReq <= '0';  -- async clear
		else
			HostReadTE <= '0';
		end if;					
	  
		if Add(3 downto 2) = "00" and FastRead = '1' and EnableZ = '1' then -- offset 0
			HostReadIFIFO <= '1';
		else
			HostReadIFIFO <= '0';
		end if;
		if Add(3 downto 2) = "00" and HostReadTE = '1' and EnableZ = '1' then -- offset 0
			HostReadIFIFOTE <= '1';
		else
			HostReadIFIFOTE <= '0';
		end if;

		if Add(3 downto 2) = "00"  and WriteStrobe = '1' and EnableZ = '1' then
			HostWriteIFIFO <= '1';
		else
			HostWriteIFIFO <= '0';
		end if;

		if Add(3 downto 2) = "01" and FastRead = '1' then -- offset 4
			HostReadQFIFO <= '1';
		else
			HostReadQFIFO <= '0';
		end if;
		if Add(3 downto 2) = "01" and HostReadTE = '1' then -- offset 4
			HostReadQFIFOTE <= '1';
		else
			HostReadQFIFOTE <= '0';
		end if;

		if Add(3 downto 2) = "01"  and WriteStrobe = '1'  then
			HostWriteQFIFO <= '1';
		else
			HostWriteQFIFO <= '0';
		end if;

		if Add(3 downto 2) = "10"   then											-- offset 8
			WriteROMAddr <= '1';
		else
			WriteROMAddr <= '0';
		end if;
		if Add(3 downto 2) = "10"  and FastRead = '1' then					-- offset 8
			HostStatusRead <= '1';
		else
			HostStatusRead <= '0';
		end if;

		if Add(3 downto 2) = "11" and FastRead = '1' and DBReset = '1' then					-- offset C
			ReadROMData <= '1';
		else
			ReadROMData <= '0';
		end if;

		if Add(3 downto 2) = "11" and FastRead = '1' and DBReset = '0' then					-- offset C
			HostStatusBRead <= '1';
		else
			HostStatusBRead <= '0';
		end if;
		
		if Add(3 downto 2) = "11" and WriteStrobe = '1'  and DBReset = '1' then					-- offset C
			WriteROMData <= '1';
		else
			WriteROMData <= '0';
		end if;

	DBus <= "ZZZZZZZZZZZZZZZZ";
	if ReadROMData = '1' then
		DBus <= HostROMData;
 	end if;
    

  end process;

  LADDrivers : process (DBus, FastRead)
  begin
    if FastRead = '1' then
      LAD(15 downto 0)  <= DBus;
      LAD(31 downto 16) <= x"0000";
    else
      LAD <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
    end if;
	 Ready <= '0';  -- we're always ready...
  end process LADDrivers;

  AddressLatch : process (LClk)
  begin
    if lclk'event and LClk = '1' then
      if ADS = '0' then
        Add <= LAD(3 downto 0);
      end if;
    end if;
  end process AddressLatch;

  -- we generate an early read from ADS and LR_W
  -- since the 10 nS LRD delay and 5 nS setup time
  -- only give us 15 nS to provide data to the PLX chip

  MakeFastRead : process (LClk, LRD, PreFastRead)
  begin
    if lclk'event and LClk = '1' then
      if ADS = '0' and LW_R = '0'then
        PreFastRead <= '1'; -- Active High
      else
        PreFastRead <= '0';
      end if;
    end if;
    FastRead <= PreFastRead or (not LRD);
  end process MakeFastRead;


end generate;

--#################################################################
--#################################################################
--#################################################################
--#################################################################
-------------------------------PCI 9054------------------------------------
PCI9054Only: if PCI9054 generate	
--

  Decode9054PCI : process(Add, FastRead, BLAST,DEN, LCLK)
  begin
		SysReset <= '0';
		if LClk'event and LClk = '1' then -- this is local bus (48 MHz clock)
			
			
			if LW_R = '1' and DEN = '0' then	 -- end of write
				WriteReq <= '1';
				WriteData <= LAD(15 downto 0);	-- we always save the write data
			end if;
			
			if  LW_R = '0' and DEN = '0' then -- end of read
				ReadReq <= '1';
			end if;				 
			
			if WriteROMAddr = '1' and LW_R = '1' and DEN = '0'  then
				HostROMAddr <= LAD(10 downto 0);
				EnableZ <= '1';
				PreDBReset <= LAD(15); 
				ClrIRQReq <= LAD(14); 			
			end if;
		end if; -- lclk
		
		if SynClk'event and SynClk = '1' then
			HostWrited2 <= HostWrited1;
			HostReadd2 <= HostReadd1;
			HostWrited1 <= WriteReq;
			HostReadd1 <= ReadReq;
			ClrIRQD1 <= ClrIRQReq;
			ClrIRQd2 <= ClrIRQd1;
			DBReset <= PreDBReset;
		end if;

		if ClrIRQd2 = '1' and ClrIRQD1 = '1' then  -- clear IRQ on trailing edge of write
			HostIRQClrSel <= '1';
			ClrIRQReq <= '0';  -- async clear
		else
			HostIRQClrSel <= '0';
		end if;
		
		if HostWriteD2 = '1' and HostWriteD1 = '1' then  -- host write strobe on trailing edge of write
			WriteStrobe <= '1';
			WriteReq <= '0';  -- async clear
		else
			WriteStrobe <= '0';
		end if;
			
		if HostReadD2 = '1' and HostReadD1 = '1' then  -- host read trailing edge for FIFO pop
			HostReadTE <= '1';
			ReadReq <= '0';  -- async clear
		else
			HostReadTE <= '0';
		end if;					
	  
		if Add(3 downto 2) = "00" and FastRead = '1' and EnableZ = '1' then -- offset 0
			HostReadIFIFO <= '1';
		else
			HostReadIFIFO <= '0';
		end if;
		if Add(3 downto 2) = "00" and HostReadTE = '1' and EnableZ = '1' then -- offset 0
			HostReadIFIFOTE <= '1';
		else
			HostReadIFIFOTE <= '0';
		end if;

		if Add(3 downto 2) = "00"  and WriteStrobe = '1' and EnableZ = '1' then
			HostWriteIFIFO <= '1';
		else
			HostWriteIFIFO <= '0';
		end if;

		if Add(3 downto 2) = "01" and FastRead = '1' then -- offset 4
			HostReadQFIFO <= '1';
		else
			HostReadQFIFO <= '0';
		end if;
		if Add(3 downto 2) = "01" and HostReadTE = '1' then -- offset 4
			HostReadQFIFOTE <= '1';
		else
			HostReadQFIFOTE <= '0';
		end if;

		if Add(3 downto 2) = "01"  and WriteStrobe = '1'  then
			HostWriteQFIFO <= '1';
		else
			HostWriteQFIFO <= '0';
		end if;

		if Add(3 downto 2) = "10"   then											-- offset 8
			WriteROMAddr <= '1';
		else
			WriteROMAddr <= '0';
		end if;
		if Add(3 downto 2) = "10"  and FastRead = '1' then					-- offset 8
			HostStatusRead <= '1';
		else
			HostStatusRead <= '0';
		end if;

		if Add(3 downto 2) = "11" and FastRead = '1' and DBReset = '1' then					-- offset C
			ReadROMData <= '1';
		else
			ReadROMData <= '0';
		end if;

		if Add(3 downto 2) = "11" and FastRead = '1' and DBReset = '0' then					-- offset C
			HostStatusBRead <= '1';
		else
			HostStatusBRead <= '0';
		end if;
		
		if Add(3 downto 2) = "11" and WriteStrobe = '1'  and DBReset = '1' then					-- offset C
			WriteROMData <= '1';
		else
			WriteROMData <= '0';
		end if;

	DBus <= "ZZZZZZZZZZZZZZZZ";
	if ReadROMData = '1' then
		DBus <= HostROMData;
 	end if;
    

  end process;

  LADDrivers : process (DBus, FastRead)
  begin
    if FastRead = '1' then
      LAD(15 downto 0)  <= DBus;
      LAD(31 downto 16) <= x"0000";
    else
      LAD <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
    end if;
	 Ready <= '0';  -- we're always ready...
  end process LADDrivers;

  AddressLatch : process (LClk)
  begin
    if lclk'event and LClk = '1' then
      if ADS = '0' then
        Add <= LAD(3 downto 0);
      end if;
    end if;
  end process AddressLatch;

  -- we generate an early read from ADS and LR_W
  -- since the 10 nS LRD delay and 5 nS setup time
  -- only give us 15 nS to provide data to the PLX chip

  	MakeFastRead : process (LClk, DEN, PreFastRead)
  	begin
    	if lclk'event and LClk = '1' then
      	if ADS = '0' and LW_R = '0'then
        	PreFastRead <= '1'; -- Active High
      	else
        	PreFastRead <= '0';
      	end if;
    	end if;
    	FastRead <= PreFastRead or ((not LW_R) and (not DEN));
  	end process MakeFastRead;

	DoHandshake: process (HOLD)
	begin
	
		HOLDA <= HOLD;
	
	end process DoHandShake;


end generate;	-- PCI9054

-------------------------------PPC5200------------------------------------
PPC5200Only: if PPC5200 generate	
-- The local bus decodes

  Decode5200 : process(SYNCLK,FPGA_LBAddr,FPGA_RW,FPGA_CS1)
  begin
		SysReset <= '0';
		if SYNCLK'event and SYNCLK = '1' then -- this is the high speed (66MHZ) clock	
			FPGA_CS1d1 <= FPGA_CS1;
			FPGA_CS1d2 <= FPGA_CS1d1;
			HostReadd2 <= HostReadd1;
			HostReadd1 <= ReadStrobe;
			PreWriteData <= FPGA_LBData;
			WriteData <= PreWriteData; -- pipeline 2 samples of data so its valid when our delayed write strobe occurs
			PreAddr <= FPGA_LBADDR;
			LAddr <= PreAddr;
			
			if WriteROMAddr = '1' and WriteStrobe = '1' then
				HostROMAddr <= WriteData(10 downto 0);
				DBReset <= WriteData(15); 		
			end if;		
		end if;


		if FPGA_CS1d1 = '0' and FPGA_CS1d2 = '0'  and FPGA_CS1 = '0' and FPGA_RW = '1' then -- 2 clocks after into active CS
			ReadStrobe <= '1';
		else
			ReadStrobe <= '0';		
		end if;

		if HostReadD1 = '0' and HostReadD2 = '1' then -- Trailing edge of read
			HostReadTE <= '1';
		else
			HostReadTE <= '0';		
		end if;

		if FPGA_CS1d1 = '1' and FPGA_CS1d2 = '0' and FPGA_RW = '0' then -- 1 clock after CS rise 
			WriteStrobe <= '1';
		else
			WriteStrobe <= '0';		
		end if;						 			

		if  FPGA_LBADDR(9 downto 4) = "100000"  then		-- 200 hex is base address
			IOSelectR <= '1';
		else
			IOSelectR <= '0';
		end if;

		if  LAddr(9 downto 4) = "100000" then	-- 200 hex is base address
			IOSelectD <= '1';
		else
			IOSelectD <= '0';
		end if;

		if FPGA_LBADDR(3 downto 2) = "00" and ReadStrobe = '1' and IOSelectR = '1' then -- offset 0
			HostReadIFIFO <= '1';
		else
			HostReadIFIFO <= '0';
		end if;
		
		if LAddr(3 downto 2) = "00" and HostReadTE = '1' and IOSelectD = '1'  then -- offset 0
			HostReadIFIFOTE <= '1';
		else
			HostReadIFIFOTE <= '0';
		end if;

		if LAddr(3 downto 2) = "00"  and WriteStrobe = '1'  and IOSelectD = '1' then
			HostWriteIFIFO <= '1';
		else
			HostWriteIFIFO <= '0';
		end if;

		if FPGA_LBADDR(3 downto 2) = "01" and ReadStrobe = '1' and IOSelectR = '1' then -- offset 4
			HostReadQFIFO <= '1';
		else
			HostReadQFIFO <= '0';
		end if;
		if LAddr(3 downto 2) = "01" and HostReadTE = '1'  and IOSelectD = '1' then -- offset 4
			HostReadQFIFOTE <= '1';
		else
			HostReadQFIFOTE <= '0';
		end if;

		if LAddR(3 downto 2) = "01"  and WriteStrobe = '1' and IOSelectD = '1' then
			HostWriteQFIFO <= '1';
		else
			HostWriteQFIFO <= '0';
		end if;

		if LAddr(3 downto 2) = "10"  and WriteStrobe = '1' and IOSelectD = '1' then											-- offset 8
			WriteROMAddr <= '1';
		else
			WriteROMAddr <= '0';
		end if;
		HostIRQClrSel <= WriteData(14) and WriteROMAddr;

		if FPGA_LBADDR(3 downto 2) = "10"  and ReadStrobe = '1' and IOSelectR = '1' then					-- offset 8
			HostStatusRead <= '1';
		else
			HostStatusRead <= '0';
		end if;

		if FPGA_LBADDR(3 downto 2) = "11" and ReadStrobe = '1' and IOSelectR = '1' and DBReset = '1' then					-- offset C
			ReadROMData <= '1';
		else
			ReadROMData <= '0';
		end if;

		if FPGA_LBADDR(3 downto 2) = "11" and ReadStrobe = '1' and IOSelectR = '1' and DBReset = '0' then					-- offset C
			HostStatusBRead <= '1';
		else
			HostStatusBRead <= '0';
		end if;
		
		if LAddr(3 downto 2) = "11" and WriteStrobe = '1' and IOSelectD = '1' and DBReset = '1' then					-- offset C
			WriteROMData <= '1';
		else
			WriteROMData <= '0';
		end if;

	DBus <= "ZZZZZZZZZZZZZZZZ";
	if ReadROMData = '1' then
		DBus <= HostROMData;
 	end if;
    

  end process;

  LBDataDrivers : process (DBus, ReadStrobe)
  begin
    if ReadStrobe = '1'  and IOSelectR = '1' then
      FPGA_LBData  <= DBus;
    else
      FPGA_LBData <= "ZZZZZZZZZZZZZZZZ";
    end if;
  end process LBDataDrivers;


end generate;


-- then the internal DSP decodes
-------------------------------KS8695 BUS INTERFACE------------------------------------
KS8695Only: if KS8695 generate	
--

  DecodeKS8695 : process(MA, ECSN, SYNCLK)
  begin
		SysReset <= '0';
		-- we run this from our SYSCLK = SDRAMCLOCK /2 = 62.5 MHz		
		
		if ECSN = '0' and EROEN = '0' then
			ReadStrobe <= '1';
		else
			ReadStrobe <= '0';
		end if;

		if ECSN = '0' and ERWEN = '0' then
			WriteStrobe <= '1';
		else
			WriteStrobe <= '0';
		end if;

		if SynClk'event and SynClk = '1' then
			WriteData <= MD(15 downto 0);
			HostWrited2 <= HostWrited1;
			HostReadd2 <= HostReadd1;
			HostWrited1 <= WriteStrobe;
			HostReadd1 <= ReadStrobe;
			if WriteROMAddr = '1'  then
				HostROMAddr <= WriteData(10 downto 0);
				DBReset <= WriteData(15);  
			end if;
		end if;
		if MA(10 downto 4) = "0001010" then	 -- 0x0A0 base
      	IOSelect <= '1';
    	else
      	IOSelect <= '0';
    	end if;
		
		if HostWriteD2 = '1' and HostWriteD1 = '0' then  -- host write strobe on trailing edge of write
			WriteStrobe <= '1';
		else
			WriteStrobe <= '0';
		end if;
			
		if HostReadD2 = '1' and HostReadD1 = '0' then  -- host read trailing edge for FIFO pop
			HostReadTE <= '1';
		else
			HostReadTE <= '0';
		end if;					
	  
		if MA(3 downto 2) = "00" and ReadStrobe = '1'  and  IOSelect = '1' then -- offset 0
			HostReadIFIFO <= '1';
		else
			HostReadIFIFO <= '0';
		end if;
		if MA(3 downto 2) = "00" and HostReadTE = '1' and  IOSelect = '1' then -- offset 0
			HostReadIFIFOTE <= '1';
		else
			HostReadIFIFOTE <= '0';
		end if;

		if MA(3 downto 2) = "00"  and WriteStrobe = '1' and  IOSelect = '1' then 
			HostWriteIFIFO <= '1';
		else
			HostWriteIFIFO <= '0';
		end if;

		if MA(3 downto 2) = "01" and ReadStrobe = '1'  and  IOSelect = '1' then -- offset 4
			HostReadQFIFO <= '1';
		else
			HostReadQFIFO <= '0';
		end if;
		if MA(3 downto 2) = "01" and HostReadTE = '1' and  IOSelect = '1' then -- offset 4
			HostReadQFIFOTE <= '1';
		else
			HostReadQFIFOTE <= '0';
		end if;

		if MA(3 downto 2) = "01"  and WriteStrobe = '1' and  IOSelect = '1'  then
			HostWriteQFIFO <= '1';
		else
			HostWriteQFIFO <= '0';
		end if;

		if MA(3 downto 2) = "10"  and WriteStrobe = '1' and IOSelect = '1'  then											-- offset 8
			WriteROMAddr <= '1';
		else
			WriteROMAddr <= '0';
		end if;
		HostIRQClrSel <= WriteData(14) and WriteROMAddr;	
		
		if MA(3 downto 2) = "10"  and ReadStrobe = '1' and  IOSelect = '1' then					-- offset 8
			HostStatusRead <= '1';
		else
			HostStatusRead <= '0';
		end if;

		if MA(3 downto 2) = "11" and ReadStrobe = '1' and  IOSelect = '1' then					-- offset C
			ReadROMData <= '1';
		else
			ReadROMData <= '0';
		end if;

		if MA(3 downto 2) = "11" and ReadStrobe = '1' and  IOSelect = '1' then					-- offset C
			HostStatusBRead <= '1';
		else
			HostStatusBRead <= '0';
		end if;
		
		if MA(3 downto 2) = "11" and WriteStrobe = '1'  and  IOSelect = '1'  then					-- offset C
			WriteROMData <= '1';
		else
			WriteROMData <= '0';
		end if;



	DBus <= "ZZZZZZZZZZZZZZZZ";
	if ReadROMData = '1' then
		DBus <= HostROMData;
 	end if;
    

  end process;

  	MDDrivers : process (DBus,ReadStrobe)
  	begin
		MD <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
		if ReadStrobe = '1' and IOSelect = '1' then
			MD(15 downto 0)  <= DBus;
			MD(31 downto 16) <= x"0000";
		end if;
		if ReadStrobe = '1' and MA = "000000000" then -- FPGA ID code for driver
			MD  <= x"4C810A00";
		end if;
		if ReadStrobe = '1' and MA = "000000001" then -- FPGA Rev
      	MD  <= x"00000001";
		end if;
	end process MDDrivers;

end generate;	-- KS8695P

-- then the internal DSP decodes

--#################################################################
--#################################################################
--#################################################################
--#################################################################


  procdecode : process
    (ioradd, iordec, iorsel, iowdec, iowsel, migram, mipram, mw, mwa, synclk)

  begin
-- I/O read addresses need address latch (to match RAM)
    if synclk'event and synclk = '1' then
      ioradd <= mra(9 downto 0);
    end if;

-- first the paged (0-FFh) and global (100h-1FFh) memory decode
    case ioradd(9 downto 8) is          -- read data mux
      when b"00"  => mi <= mipram;
      when b"01"  => mi <= migram;
      when others => mi <= "ZZZZZZZZZZZZZZZZ";
    end case;

-- first 256 words of mem are paged(by axis) RAM
    if mwa(9 downto 8) = b"00" and mw = '1' then
      pwrite <= '1';
    else
      pwrite <= '0';
    end if;

-- second 256 words of mem are global RAM
    if mwa(9 downto 8) = b"01" and mw = '1' then
      gwrite <= '1';
    else
      gwrite <= '0';
    end if;

-- then the I/O decodes - I/O is from 200h up                   
    if iorsel = '1' and iordec(7 downto 2) = b"000000" then -- 0 -> 3 
      ICDFIFORead  <= '1';
    else
      ICDFIFORead  <= '0';
    end if;

    if iowsel = '1' and iowdec(7 downto 2) = b"000000" and mw = '1' then -- 0 -> 3 
      ICDFIFOWrite  <= '1';
    else
      ICDFIFOWrite  <= '0';
    end if;

    if iorsel = '1' and iordec(7 downto 1) = b"0000010" then -- 4 -> 5
      IRBFIFORead <= '1';
    else
      IRBFIFORead <= '0';
    end if;

    if iowsel = '1' and iowdec(7 downto 1) = b"0000010" and mw = '1' then
      IRBFIFOWrite <= '1';
    else
      IRBFIFOWrite <= '0';
    end if;

    if iorsel = '1' and iordec(7 downto 2) = b"000010" then -- 8 -> B
      QCDFIFORead  <= '1';
    else
      QCDFIFORead  <= '0';
    end if;
    if iowsel = '1' and iowdec(7 downto 2) = b"000010" and mw = '1' then -- 8 -> B
      QCDFIFOWrite  <= '1';
    else
      QCDFIFOWrite  <= '0';
    end if;


    if iorsel = '1' and iordec(7 downto 1) = b"0000110" then -- C -> D
      QRBFIFORead <= '1';
    else
      QRBFIFORead <= '0';
    end if;

    if iowsel = '1' and iowdec(7 downto 1) = b"0000110" and mw = '1' then
      QRBFIFOWrite <= '1';
    else
      QRBFIFOWrite <= '0';
    end if;

    if iorsel = '1' and iordec = x"10" then
      AxisRegRead <= '1';
    else
      AxisRegRead <= '0';
    end if;
    if iowsel = '1' and iowdec = x"10" and mw = '1' then
      AxisRegWrite <= '1';
    else
      AxisRegWrite <= '0';
    end if;

    if iorsel = '1' and iordec = x"11" then
      StatusRegReadSel <= '1';
    else
      StatusRegReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"11" and mw = '1' then
      StatusRegWriteSel <= '1';
    else
      StatusRegWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"12" then
      Counter0ReadSel <= '1';
    else
      Counter0ReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"12" and mw = '1' then
      Counter0WriteSel <= '1';
    else
      Counter0WriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"13" then
      CCR0ReadSel <= '1';
    else
      CCR0ReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"13" and mw = '1' then
      CCR0WriteSel <= '1';
    else
      CCR0WriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"14" then
      Counter1ReadSel <= '1';
    else
      Counter1ReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"14" and mw = '1' then
      Counter1WriteSel <= '1';
    else
      Counter1WriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"15" then
      CCR1ReadSel <= '1';
    else
      CCR1ReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"15" and mw = '1' then
      CCR1WriteSel <= '1';
    else
      CCR1WriteSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"16" and mw = '1' then
      PreScaleWriteSel <= '1';
    else
      PreScaleWriteSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"17" and mw = '1' then
      PostScaleWriteSel <= '1';
    else
      PostScaleWriteSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"18" and mw = '1' then
      Counter0ClearSel <= '1';
    else
      Counter0ClearSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"19" and mw = '1' then
      Counter1ClearSel <= '1';
    else
      Counter1ClearSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"1A" and mw = '1' then
      Counter0CountClrdSel <= '1';
    else
      Counter0CountClrdSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"1B" and mw = '1' then
      Counter1CountClrdSel <= '1';
    else
      Counter1CountClrdSel <= '0';
    end if;
	 
    if iowsel = '1' and iowdec = x"20" and mw = '1' then
      PWMASel <= '1';
    else
      PWMASel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"21" and mw = '1' then
      PWMBSel <= '1';
    else
      PWMBSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"22" and mw = '1' then
      PWMCSel <= '1';
    else
      PWMCSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"23" and mw = '1' then
      DirASel <= '1';
    else
      DirASel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"24" and mw = '1' then
      DirBSel <= '1';
    else
      DirBSel <= '0';
    end if;

	 if iowsel = '1' and iowdec = x"25" and mw = '1' then
      DirCSel <= '1';
    else
      DirCSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"26" and mw = '1' then
      EnaSel <= '1';
    else
      EnaSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"28" then
      TimerReadSel <= '1';
    else
      TimerReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"28" and mw = '1' then
      TimerWriteSel <= '1';
    else
      TimerWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"29" then
      HStatusRegReadSel <= '1';
    else
      HStatusRegReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"29" and mw = '1' then
      HStatusRegWriteSel <= '1';
    else
      HStatusRegWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"2A" then
      StatusRegBReadSel <= '1';
    else
      StatusRegBReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"2A" and mw = '1' then
      StatusRegBWriteSel <= '1';
    else
      StatusRegBWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec(7 downto 2) = b"001011" then -- 0x2C -> 0x2F
      AlignReadSel <= '1';
    else
      AlignReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec(7 downto 2) = b"001011" and mw = '1' then
      AlignWriteSel <= '1';
    else
      AlignWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"30" then
      PortAReadSel <= '1';
    else
      PortAReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"30" and mw = '1' then
      PortAWriteSel <= '1';
    else
      PortAWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"31" then
      PortAReadDDRSel <= '1';
    else
      PortAReadDDRSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"31" and mw = '1' then
      PortAWriteDDRSel <= '1';
    else
      PortAWriteDDRSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"32" then
      PortBReadSel <= '1';
    else
      PortBReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"32" and mw = '1' then
      PortBWriteSel <= '1';
    else
      PortBWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"33" then
      PortBReadDDRSel <= '1';
    else
      PortBReadDDRSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"33" and mw = '1' then
      PortBWriteDDRSel <= '1';
    else
      PortBWriteDDRSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"34" then
      PortCReadSel <= '1';
    else
      PortCReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"34" and mw = '1' then
      PortCWriteSel <= '1';
    else
      PortCWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"35" then
      PortCReadDDRSel <= '1';
    else
      PortCReadDDRSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"35" and mw = '1' then
      PortCWriteDDRSel <= '1';
    else
      PortCWriteDDRSel <= '0';
    end if;


    if iorsel = '1' and iordec = x"36" then
      PortDReadSel <= '1';
    else
      PortDReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"36" and mw = '1' then
      PortDWriteSel <= '1';
    else
      PortDWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"37" then
      PortDReadDDRSel <= '1';
    else
      PortDReadDDRSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"37" and mw = '1' then
      PortDWriteDDRSel <= '1';
    else
      PortDWriteDDRSel <= '0';
    end if;


    if iorsel = '1' and iordec = x"38" then
      PortEReadSel <= '1';
    else
      PortEReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"38" and mw = '1' then
      PortEWriteSel <= '1';
    else
      PortEWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"39" then
      PortEReadDDRSel <= '1';
    else
      PortEReadDDRSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"39" and mw = '1' then
      PortEWriteDDRSel <= '1';
    else
      PortEWriteDDRSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"3A" then
      PortFReadSel <= '1';
    else
      PortFReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"3A" and mw = '1' then
      PortFWriteSel <= '1';
    else
      PortFWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"3B" then
      PortFReadDDRSel <= '1';
    else
      PortFReadDDRSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"3B" and mw = '1' then
      PortFWriteDDRSel <= '1';
    else
      PortFWriteDDRSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"3D" then
      IRQDriveReadSel <= '1';
    else
      IRQDriveReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"3D" and mw = '1' then
      IRQDriveWriteSel <= '1';
    else
      IRQDriveWriteSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"3E" and mw = '1' then
      IRQSetSel <= '1';
    else
      IRQSetSel <= '0';
    end if;

    if iorsel = '1' and iordec(7 downto 3) = b"01000" then -- 40 -> 47
      MRegReadSel <= '1';
    else
      MRegReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec(7 downto 3) = b"01000" and mw = '1' then
      MRegWriteSel <= '1';
    else
      MRegWriteSel <= '0';
    end if;
 
    if iorsel = '1' and iordec(7 downto 3) = b"01001" then	-- 48 -> 4F
      LMulRegReadSel <= '1';
    else
      LMulRegReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec(7 downto 3) = b"01001" and mw = '1' then
      LMulRegWriteSel <= '1';
    else
      LMulRegWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec(7 downto 3) = b"01010" then -- 0x50 -> 0x57
      LDivRegReadSel <= '1';
    else
      LDivRegReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec(7 downto 3) = b"01010" and mw = '1' then
      LDivRegWriteSel <= '1';
    else
      LDivRegWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec(7 downto 3) = b"01011" then -- 0x58 -> 0x5F
      LSqrRegReadSel <= '1';
    else
      LSqrRegReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec(7 downto 3) = b"01011" and mw = '1' then
      LSqrRegWriteSel <= '1';
    else
      LSqrRegWriteSel <= '0';
    end if;
 
    if iorsel = '1' and iordec(7 downto 2) = b"011000" then   -- 0x60 -> 0x63
      LookupReadSel <= '1';
    else
      LookupReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec(7 downto 1) = b"0110000" and mw = '1' then	-- 0x60 -> 0x61
      LookupWriteSel <= '1';
    else
      LookupWriteSel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"64" and mw = '1' then
      CCASel <= '1';
    else
      CCASel <= '0';
    end if;

    if iowsel = '1' and iowdec = x"65" and mw = '1' then
      CCBSel <= '1';
    else
      CCBSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"66"  then
      AltIDXSel <= '1';
    else
      AltIDXSel <= '0';
    end if;

	 if iorsel = '1' and iordec(7 downto 3) = b"01101" then	-- 68 -> 6F
      QuadOutReadSel <= '1';
    else
      QuadOutReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec(7 downto 3) = b"01101"  and mw = '1' then
      QuadOutWriteSel <= '1';
    else
      QuadOutWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"72" then
      HallReadSel <= '1';
    else
      HallReadSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"72" and mw = '1' then
      HallWriteSel <= '1';
    else
      HallWriteSel <= '0';
    end if;

    if iorsel = '1' and iordec = x"73" then
      HallReadDDRSel <= '1';
    else
      HallReadDDRSel <= '0';
    end if;
    if iowsel = '1' and iowdec = x"73" and mw = '1' then
      HallWriteDDRSel <= '1';
    else
      HallWriteDDRSel <= '0';
    end if;
    
	 if iorsel = '1' and iordec(7 downto 5) = b"100" then	  -- 80 -> 9F
      BitConstSel <= '1';
    else
      BitConstSel <= '0';
    end if;


  end process procdecode;
-- 4.X

  AStatusReg : process (SampleRate, StatusRegReadSel)
  begin
    if StatusRegReadSel = '1' then
		mi(0) <= SampleRate;
		mi(15 downto 1) <= (others => '0'); 
    else
      mi <=	(others => 'Z');
    end if;
  end process;

  AAxisReg : process (AxisReg, AxisRegRB, AxisRegRead, SynClk, mra, mwa)
  begin
    if SynClk'event and SynClk = '1' then
      if AxisRegWrite = '1' then
        AxisReg <= mo(3 downto 0);
      end if;
    end if;
    if AxisRegRead = '1' then
      AxisRegRB <= AxisReg;
    else
      AxisRegRB <= "ZZZZ";
    end if;
    mi(3 downto 0)                <= AxisRegRB;
    PagedReadAddress(PagedMemMSA downto 8)  <= AxisReg(PagedMemMSA-8 downto 0);
    PagedReadAddress(7 downto 0)  <= mra(7 downto 0);
    PagedWriteAddress(PagedMemMSA downto 8) <= AxisReg(PagedMemMSA-8 downto 0);
    PagedWriteAddress(7 downto 0) <= mwa(7 downto 0);
  end process;

  naxis4: if Naxis = 4 generate
  PagedIOdecode4 : process
    (
      AxisReg, CCR0ReadSel, CCR0WriteSel, CCR1ReadSel, CCR1WriteSel,
      Counter0ReadSel, Counter0WriteSel, Counter0ClearSel, Counter1ReadSel, 
		Counter1WriteSel, Counter1ClearSel,
      DirASel, ENASel, PWMASel) 

  begin

    Counter0ReadSelects <= OneOfFourDecode(Counter0ReadSel,AxisReg(1 downto 0));
    Counter0WriteSelects <= OneOfFourDecode(Counter0WriteSel,AxisReg(1 downto 0));
    Counter1ReadSelects <= OneOfFourDecode(Counter1ReadSel,AxisReg(1 downto 0));
    Counter1WriteSelects <= OneOfFourDecode(Counter1WriteSel,AxisReg(1 downto 0));
    PWMASelects <= OneOfFourDecode(PWMASel,AxisReg(1 downto 0));
    PWMBSelects <= OneOfFourDecode(PWMBSel,AxisReg(1 downto 0));   
    PWMCSelects <= OneOfFourDecode(PWMCSel,AxisReg(1 downto 0));
    DirASelects <= OneOfFourDecode(DirASel,AxisReg(1 downto 0));
    DirBSelects <= OneOfFourDecode(DirBSel,AxisReg(1 downto 0));
    DirCSelects <= OneOfFourDecode(DirCSel,AxisReg(1 downto 0));
    CCR0ReadSelects <= OneOfFourDecode(CCR0ReadSel,AxisReg(1 downto 0));
    CCR0WriteSelects <= OneOfFourDecode(CCR0WriteSel,AxisReg(1 downto 0));
	 Counter0ClearSelects <= OneOfFourDecode(Counter0ClearSel,AxisReg(1 downto 0));
	 Counter0CountClrdSelects <= OneOfFourDecode(Counter0CountClrdSel,AxisReg(1 downto 0));
    CCR1ReadSelects <= OneOfFourDecode(CCR1ReadSel,AxisReg(1 downto 0));
    CCR1WriteSelects <= OneOfFourDecode(CCR1WriteSel,AxisReg(1 downto 0));
	 Counter1ClearSelects <= OneOfFourDecode(Counter1ClearSel,AxisReg(1 downto 0));
	 Counter1CountClrdSelects <= OneOfFourDecode(Counter1CountClrdSel,AxisReg(1 downto 0));
    ENASelects <= OneOfFourDecode(ENASel,AxisReg(1 downto 0));
	 CCASelects <= OneOfFourDecode(CCASel,AxisReg(1 downto 0));
	 CCBSelects <= OneOfFourDecode(CCBSel,AxisReg(1 downto 0));
	 AltIDXSelects <= OneOfFourDecode(AltIDXSel,AxisReg(1 downto 0));
	 QuadOutReadSelects <= OneOfFourDecode(QuadOutReadSel,AxisReg(1 downto 0));
  	 QuadOutWriteSelects <= OneOfFourDecode(QuadOutWriteSel,AxisReg(1 downto 0));
	 HallWriteSelects <= OneOfFourDecode(HallWriteSel,AxisReg(1 downto 0));
	 HallWriteDDRSelects <= OneOfFourDecode(HallWriteDDRSel,AxisReg(1 downto 0));
	 HallReadDDRSelects <= OneOfFourDecode(HallReadDDRSel,AxisReg(1 downto 0));
	 HallReadSelects  <= OneOfFourDecode(HallReadSel,AxisReg(1 downto 0));	
	end process;
 end generate;

  naxis8: if Naxis = 8 generate
  PagedIOdecode8 : process
    (
      AxisReg, CCR0ReadSel, CCR0WriteSel, CCR1ReadSel, CCR1WriteSel,
      Counter0ReadSel, Counter0WriteSel, Counter0ClearSel, Counter1ReadSel, 
		Counter1WriteSel, Counter1ClearSel,
      DirASel, ENASel, PWMASel) 

  begin

    Counter0ReadSelects <= OneOfEightDecode(Counter0ReadSel,AxisReg(2 downto 0));
    Counter0WriteSelects <= OneOfEightDecode(Counter0WriteSel,AxisReg(2 downto 0));
    Counter1ReadSelects <= OneOfEightDecode(Counter1ReadSel,AxisReg(2 downto 0));
    Counter1WriteSelects <= OneOfEightDecode(Counter1WriteSel,AxisReg(2 downto 0));
    PWMASelects <= OneOfEightDecode(PWMASel,AxisReg(2 downto 0));
    PWMBSelects <= OneOfEightDecode(PWMBSel,AxisReg(2 downto 0));   
    PWMCSelects <= OneOfEightDecode(PWMCSel,AxisReg(2 downto 0));
    DirASelects <= OneOfEightDecode(DirASel,AxisReg(2 downto 0));
    DirBSelects <= OneOfEightDecode(DirBSel,AxisReg(2 downto 0));
    DirCSelects <= OneOfEightDecode(DirCSel,AxisReg(2 downto 0));
    CCR0ReadSelects <= OneOfEightDecode(CCR0ReadSel,AxisReg(2 downto 0));
    CCR0WriteSelects <= OneOfEightDecode(CCR0WriteSel,AxisReg(2 downto 0));
    Counter0ClearSelects <= OneOfEightDecode(Counter0CLearSel,AxisReg(2 downto 0));
    Counter0CountClrdSelects <= OneOfEightDecode(Counter0CountClrdSel,AxisReg(2 downto 0));
    CCR1ReadSelects <= OneOfEightDecode(CCR1ReadSel,AxisReg(2 downto 0));
    CCR1WriteSelects <= OneOfEightDecode(CCR1WriteSel,AxisReg(2 downto 0));
    Counter1ClearSelects <= OneOfEightDecode(Counter1CLearSel,AxisReg(2 downto 0));
    Counter1CountClrdSelects <= OneOfEightDecode(Counter1CountClrdSel,AxisReg(2 downto 0));
    ENASelects <= OneOfEightDecode(ENASel,AxisReg(2 downto 0));
	 CCASelects <= OneOfEightDecode(CCASel,AxisReg(2 downto 0));
	 CCBSelects <= OneOfEightDecode(CCBSel,AxisReg(2 downto 0));
	 AltIDXSelects <= OneOfEightDecode(AltIDXSel,AxisReg(2 downto 0));
	 QuadOutReadSelects <= OneOfEightDecode(QuadOutReadSel,AxisReg(2 downto 0));
  	 QuadOutWriteSelects <= OneOfEightDecode(QuadOutWriteSel,AxisReg(2 downto 0));
	 HallWriteSelects <= OneOfEightDecode(HallWriteSel,AxisReg(2 downto 0));
	 HallWriteDDRSelects <= OneOfEightDecode(HallWriteDDRSel,AxisReg(2 downto 0));
	 HallReadDDRSelects <= OneOfEightDecode(HallReadDDRSel,AxisReg(2 downto 0));
	 HallReadSelects  <= OneOfEightDecode(HallReadSel,AxisReg(2 downto 0));	
	end process;
 end generate;

  naxis2: if Naxis = 2 generate
  PagedIOdecode2 : process
    (
      AxisReg, CCR0ReadSel, CCR0WriteSel, CCR1ReadSel, CCR1WriteSel,
      Counter0ReadSel, Counter0WriteSel,Counter0ClearSel,Counter1ReadSel, 
		Counter1WriteSel,Counter1ClearSel,
      DirASel, ENASel, PWMASel) 

  begin

    Counter0ReadSelects <= OneOfTwoDecode(Counter0ReadSel,AxisReg(0));
    Counter0WriteSelects <= OneOfTwoDecode(Counter0WriteSel,AxisReg(0));
    Counter1ReadSelects <= OneOfTwoDecode(Counter1ReadSel,AxisReg(0));
    Counter1WriteSelects <= OneOfTwoDecode(Counter1WriteSel,AxisReg(0));
    PWMASelects <= OneOfTwoDecode(PWMASel,AxisReg(0));
    PWMBSelects <= OneOfTwoDecode(PWMBSel,AxisReg(0));   
    PWMCSelects <= OneOfTwoDecode(PWMCSel,AxisReg(0));
    DirASelects <= OneOfTwoDecode(DirASel,AxisReg(0));
    DirBSelects <= OneOfTwoDecode(DirBSel,AxisReg(0));
    DirCSelects <= OneOfTwoDecode(DirCSel,AxisReg(0));
    CCR0ReadSelects <= OneOfTwoDecode(CCR0ReadSel,AxisReg(0));
    CCR0WriteSelects <= OneOfTwoDecode(CCR0WriteSel,AxisReg(0));
    Counter0ClearSelects <= OneOfTwoDecode(Counter0ClearSel,AxisReg(0));
    Counter0CountClrdSelects <= OneOfTwoDecode(Counter0CountClrdSel,AxisReg(0));
    CCR1ReadSelects <= OneOfTwoDecode(CCR1ReadSel,AxisReg(0));
    CCR1WriteSelects <= OneOfTwoDecode(CCR1WriteSel,AxisReg(0));
    Counter1ClearSelects <= OneOfTwoDecode(Counter1ClearSel,AxisReg(0));
    Counter1CountClrdSelects <= OneOfTwoDecode(Counter1CountClrdSel,AxisReg(0));
    ENASelects <= OneOfTwoDecode(ENASel,AxisReg(0));
	 CCASelects <= OneOfTwoDecode(CCASel,AxisReg(0));
	 CCBSelects <= OneOfTwoDecode(CCBSel,AxisReg(0));
	 AltIDXSelects <= OneOfTwoDecode(AltIDXSel,AxisReg(0));
	 QuadOutReadSelects <= OneOfTwoDecode(QuadOutReadSel,AxisReg(0));
  	 QuadOutWriteSelects <= OneOfTwoDecode(QuadOutWriteSel,AxisReg(0));
	 HallWriteSelects <= OneOfTwoDecode(HallWriteSel,AxisReg(0));
	 HallWriteDDRSelects <= OneOfTwoDecode(HallWriteDDRSel,AxisReg(0));
	 HallReadDDRSelects <= OneOfTwoDecode(HallReadDDRSel,AxisReg(0));
	 HallReadSelects  <= OneOfTwoDecode(HallReadSel,AxisReg(0));	
  end process;
 end generate;

	SevenI32Only3: if SevenI32 generate
	 	SharedEna: process (lena)
		begin
	 		for i in 0 to (naxis-1) loop
				ENAA(i) <= lena(i);
				ENAB(i) <= lena(i);
			end loop;
	   end process;
	 end generate;

	ThreePKOnly: if ThreePK generate
	 	SharedEna: process (lena)
		begin
	 		for i in 0 to (naxis-1) loop
				ENAA(i) <= lena(i);
				ENAB(i) <= lena(i);
				ENAC(i) <= lena(i);				
			end loop;
	   end process;
	 end generate;

  ssconstants : process
    (
       SynClk)
  begin
    if iorsel = '1' then
    	case iordec is
			when x"74"  => mi <= Synclklow;
			when x"75"  => mi <= SynClkHigh;
 			when x"78"  => mi <= x"0000";
        	when x"79"  => mi <= x"FFFF";
        	when x"7A"  => mi <= Naxisrb;
			when x"7B"  => mi <= Revision;
			when x"7C"  => mi <= ICDFIFOSize;
			when x"7D"  => mi <= QCDFIFOSize;
			when x"7E"  => mi <= IRBFIFOSize;
			when x"7F"  => mi <= QRBFIFOSize;
       	when others => mi <= "ZZZZZZZZZZZZZZZZ";
      end case;
    else
      mi <= "ZZZZZZZZZZZZZZZZ";
    end if;
  end process;

EightLEDSOnly:  if  EightLEDS generate
  DebugLEDs8 : process
    (
      LEDLatch, SynClk)
  begin
    
    if SynClk'event and SynClk = '1' then
      if iowsel = '1' and iowdec = x"70" and mw = '1' then  -- 070h
        LEDLatch <= not mo(7 downto 0);
      end if;
    end if;
	 LEDS <=  LEDLatch;
--	 LEDS(0) <= not DBRESET;
 end process;
end generate;

FourLEDSOnly:  if  FourLEDS generate
  DebugLEDs4 : process
    (LEDLatch, SynClk)
  begin
    LEDS(3 downto 0) <=  LEDLatch(3 downto 0);
    if SynClk'event and SynClk = '1' then
      if iowsel = '1' and iowdec = x"70" and mw = '1' then  -- 070h
        LEDLatch <= not mo(7 downto 0);
      end if;
    end if;
 end process;
end generate;

TwoLEDSOnly:  if  TwoLEDS generate
  DebugLEDs2 : process
    (LEDLatch, SynClk)
  begin
    LEDS(1 downto 0) <=  LEDLatch(1 downto 0);
    if SynClk'event and SynClk = '1' then
      if iowsel = '1' and iowdec = x"70" and mw = '1' then  -- 070h
        LEDLatch <= not mo(7 downto 0);
      end if;
    end if;
 end process;
end generate;

OneLEDOnly:  if  OneLED generate
  DebugLEDsJustone : process
    (LEDLatch, SynClk)
  begin
    LED <=  LEDLatch(0);
    if SynClk'event and SynClk = '1' then
      if iowsel = '1' and iowdec = x"70" and mw = '1' then  -- 070h
        LEDLatch <= not mo(7 downto 0);
      end if;
    end if;
 end process;
end generate;
  

end dataflow;
