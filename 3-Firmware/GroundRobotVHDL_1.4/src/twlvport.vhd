library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity twelveport is
    Port (		
	   	clear: in STD_LOGIC;
			clk: in STD_LOGIC;
			ibus: in STD_LOGIC_VECTOR (11 downto 0);
			obus: out STD_LOGIC_VECTOR (11 downto 0);
			loadport: in STD_LOGIC;
			loadddr: in STD_LOGIC;
			readddr: in STD_LOGIC;
			portdata: out STD_LOGIC_VECTOR (11 downto 0)
 			);
end twelveport;

architecture behavioral of twelveport is

signal outreg: STD_LOGIC_VECTOR (11 downto 0);
signal ddrreg: STD_LOGIC_VECTOR (11 downto 0);
signal tsoutreg: STD_LOGIC_VECTOR (11 downto 0);

begin
	atwelveioport: process (
								clk,
								ibus,
								loadport,
								loadddr,
								readddr,
								outreg,ddrreg)
	begin
		if clk'event and clk = '1' then
			if loadport = '1' then
				outreg <= ibus;
			end if; 
			if loadddr = '1' then
				ddrreg <= ibus;
			end if;
			if clear = '1' then
                                ddrreg <= x"000";
                        end if;
		end if; -- clk

		for i in 0 to 11 loop
			if ddrreg(i) = '1' then 
				tsoutreg(i) <= outreg(i);
			else
				tsoutreg(i) <= 'Z';
			end if;
		end loop;
		
		portdata <= tsoutreg;
		
		if readddr = '1' then
			obus <= ddrreg;
 	   else
			obus <= "ZZZZZZZZZZZZ";
		end if;

	end process;
end behavioral;
