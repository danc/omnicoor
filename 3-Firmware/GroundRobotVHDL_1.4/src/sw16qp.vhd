library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
-------------------------------------------------------------------------------

-- Sweet16qp CPU with 4 offsetable index registers	and autoinc
-- JSR indirect added
-- 2 cycle delay from STA to fetch of same loc (LDA,ADD,OR etc etc)
-- 2 delay slots after conditional jumps
-- 2 delay slots needed after index register modification
-- before use of index reg with fetch (no delay needed for STA)
-- 4k program size, 2k memory size
-- memory reference has direct (2k range)
-- and indexed (X,Y,Z,T) with 0-255 word offset
-- negate instruction removed and bnd added
-- operate instructions expanded
-- bnd changed to bnds, bndu added
-- skpnz,skpz,skpnc,skpc added
-- sco removed, sats and satu added

-------------------------------------------------------------------------------

entity Sweet16cpu_qp is
  port (
    clk      : in  std_logic;
    reset    : in  std_logic;
    iabus    : out std_logic_vector(11 downto 0);  -- program address bus
    idbus    : in  std_logic_vector(15 downto 0);  -- program data bus       
    mradd    : out std_logic_vector(10 downto 0);  -- memory read address
    mwadd    : out std_logic_vector(10 downto 0);  -- memory write address
    mibus    : in  std_logic_vector(15 downto 0);  -- memory data in bus     
    mobus    : out std_logic_vector(15 downto 0);  -- memory data out bus
    mwrite   : out std_logic;           -- memory write signal        
    carryflg : out std_logic            -- carry flag
    );
end Sweet16cpu_qp;


architecture Behavioral of Sweet16cpu_qp is

  constant width : integer := 16;		-- data width
  constant maddwidth : integer := 11;	-- memory address width
  constant paddwidth : integer := 12;	-- program counter width

  constant carrymask : std_logic_vector := b"0_1111_1111_1111_1111";  -- mask to drop carry bit

-- basic op codes

  constant opr   : std_logic_vector (3 downto 0) := x"0";
  constant jmp   : std_logic_vector (3 downto 0) := x"1";
  constant jmpnz : std_logic_vector (3 downto 0) := x"2";
  constant jmpz  : std_logic_vector (3 downto 0) := x"3";
  constant jmpnc : std_logic_vector (3 downto 0) := x"4";
  constant jmpc  : std_logic_vector (3 downto 0) := x"5";
  constant jsr   : std_logic_vector (3 downto 0) := x"6";
  constant lor   : std_logic_vector (3 downto 0) := x"7";
  constant lxor  : std_logic_vector (3 downto 0) := x"8";
  constant land  : std_logic_vector (3 downto 0) := x"9";
  constant add   : std_logic_vector (3 downto 0) := x"A";
  constant addc  : std_logic_vector (3 downto 0) := x"B";
  constant sub   : std_logic_vector (3 downto 0) := x"C";
  constant subc  : std_logic_vector (3 downto 0) := x"D";
  constant lda   : std_logic_vector (3 downto 0) := x"E";
  constant sta   : std_logic_vector (3 downto 0) := x"F";

-- operate instructions
  constant nop : std_logic_vector (3 downto 0) := x"0";

-- immediate load type
  constant ldli  : std_logic_vector (3 downto 0) := x"1";
  constant ldhi  : std_logic_vector (3 downto 0) := x"2";
  constant lxbi  : std_logic_vector (3 downto 0) := x"3";

-- accumulator operate type
  constant bswp  : std_logic_vector (7 downto 0) := x"80";
  constant rotcl : std_logic_vector (7 downto 0) := x"81";
  constant rotcr : std_logic_vector (7 downto 0) := x"82";
  constant ashr  : std_logic_vector (7 downto 0) := x"83";
  
  constant sxw   : std_logic_vector (7 downto 0) := x"88";
  constant bnds  : std_logic_vector (7 downto 0) := x"89";
  constant bndu  : std_logic_vector (7 downto 0) := x"8A";
--  constant sats  : std_logic_vector (7 downto 0) := x"8B";
--  constant satu  : std_logic_vector (7 downto 0) := x"8C";

-- flag manipulation  
  constant ztof  : std_logic_vector (7 downto 0) := x"8D";
  constant ctof  : std_logic_vector (7 downto 0) := x"8E";
  constant stof  : std_logic_vector (7 downto 0) := x"8F";

-- index register load/store in address order
  constant ldx   : std_logic_vector (7 downto 0) := x"90";
  constant stx   : std_logic_vector (7 downto 0) := x"91";
  constant ldy   : std_logic_vector (7 downto 0) := x"92";
  constant sty   : std_logic_vector (7 downto 0) := x"93";
  constant ldz   : std_logic_vector (7 downto 0) := x"94";
  constant stz   : std_logic_vector (7 downto 0) := x"95";
  constant ldt   : std_logic_vector (7 downto 0) := x"96";
  constant stt   : std_logic_vector (7 downto 0) := x"97";

  constant ldr   : std_logic_vector (7 downto 0) := x"98";
  constant str   : std_logic_vector (7 downto 0) := x"99";
  constant lds   : std_logic_vector (7 downto 0) := x"9A";


-- skip type instructions
  constant skpnz : std_logic_vector (7 downto 0) := x"C2";
  constant skpz  : std_logic_vector (7 downto 0) := x"C3";
  constant skpnc : std_logic_vector (7 downto 0) := x"C4";
  constant skpc  : std_logic_vector (7 downto 0) := x"C5";
  constant skpp  : std_logic_vector (7 downto 0) := x"C6"; 
  constant skpm  : std_logic_vector (7 downto 0) := x"C7";
  constant skpnf : std_logic_vector (7 downto 0) := x"C8";
  constant skpf  : std_logic_vector (7 downto 0) := x"C9"; 	

-- basic signals

  signal accumcar  : std_logic_vector (width downto 0);  -- accumulator+carry
  alias accum      : std_logic_vector (width-1 downto 0) is accumcar(15 downto 0);
  alias acclowb    : std_logic_vector (7 downto 0) is accumcar(7 downto 0);
  alias acchighb   : std_logic_vector (7 downto 0) is accumcar(width -1 downto 8);
  alias carrybit   : std_logic is accumcar(width);
  alias accumsign  : std_logic is accumcar(15);

  signal pc        : std_logic_vector (paddwidth -1 downto 0); -- program counter - 12 bits = 4k
  signal mra       : std_logic_vector (maddwidth -1 downto 0);  -- memory read address - 11 bits = 2k
  signal mwa       : std_logic_vector (maddwidth -1 downto 0);  -- memory write address - 11 bits = 2k
  signal id1       : std_logic_vector (width -1 downto 0);  -- instruction pipeline 1       
  signal id2       : std_logic_vector (width -1 downto 0);  -- instruction pipeline 2       
  signal pc1_11    : std_logic;         -- pc(11) at instruction pipeline 1     
  signal pc2_11    : std_logic;         -- pc(11) at instruction pipeline 2     
  alias opcode0    : std_logic_vector (3 downto 0) is idbus (width-1 downto width-4);   -- main opcode at pipe0
  alias opcode1    : std_logic_vector (3 downto 0) is id1 (width-1 downto width-4);     -- main opcode at pipe1
  alias opcode2    : std_logic_vector (3 downto 0) is id2 (width-1 downto width-4);     -- main opcode at pipe2
  alias opradd0    : std_logic_vector (maddwidth -1 downto 0) is idbus (maddwidth -1 downto 0);               -- operand address at pipe0
  alias opradd2    : std_logic_vector (maddwidth -1 downto 0) is id2 (maddwidth -1 downto 0);                 -- operand address at pipe2
  alias ind0       : std_logic is idbus(11); -- probably should be @width -5
  alias ind1       : std_logic is id1(11);
  alias ind2       : std_logic is id2(11);
  alias ireg0      : std_logic_vector (1 downto 0) is idbus(10 downto 9);
  alias ireg1      : std_logic_vector (1 downto 0) is id1(10 downto 9);
  alias ireg2      : std_logic_vector (1 downto 0) is id2(10 downto 9);
  alias autoinc2	 : std_logic is id2(8);
  alias offset0    : std_logic_vector (7 downto 0) is idbus (7 downto 0);
  alias offset1    : std_logic_vector (7 downto 0) is id1 (7 downto 0);
  alias offset2    : std_logic_vector (7 downto 0) is id2 (7 downto 0);
  alias iopropcode2 : std_logic_vector (3 downto 0) is id2 (width-5 downto width-8);    -- immediate operate opcode at pipe2
  alias opropcode2 : std_logic_vector (7 downto 0) is id2 (width-5 downto width-12);    -- operate opcode at pipe2
  alias iopr2      : std_logic_vector (7 downto 0) is id2 (7 downto 0);                 -- immediate operand at pipe2
  signal oprr      : std_logic_vector (width -1 downto 0);                              -- operand register
  alias oprrsign   : std_logic is oprr(15);  
  signal idx       : std_logic_vector (width -1 downto 0);
  signal idy       : std_logic_vector (width -1 downto 0);
  signal idz       : std_logic_vector (width -1 downto 0);
  signal idt       : std_logic_vector (width -1 downto 0);
  signal idr       : std_logic_vector (width -1 downto 0);
  signal ids		 : std_logic_vector (width -1 downto 0);
  signal flag      : std_logic;
  signal nextpc    : std_logic_vector (paddwidth -1 downto 0);
  signal zero      : std_logic;
  signal zero_sign : std_logic;
  signal ones_sign : std_logic;
  signal oldaccumsign : std_logic;
  signal oldoprrsign : std_logic;
  signal wasadd : std_logic;

  function rotcleft(v : std_logic_vector ) return std_logic_vector is
    variable result   : std_logic_vector(width downto 0);
  begin
    result(width downto 1) := v(width-1 downto 0);
    result(0)              := v(width);
    return result;
  end rotcleft;

  function rotcright(v : std_logic_vector ) return std_logic_vector is
    variable result    : std_logic_vector(width downto 0);
  begin
    result(width -1 downto 0) := v(width downto 1);
    result(width)             := v(0);
    return result;
  end rotcright;

  function byteswap(v : std_logic_vector ) return std_logic_vector is
    variable result   : std_logic_vector(width -1 downto 0);
  begin
    result(width -1 downto 8) := v(7 downto 0);
    result(7 downto 0)        := v(width-1 downto 8);
    return result;
  end byteswap;

  function signextendbyte(v : std_logic_vector ) return std_logic_vector is
    variable result         : std_logic_vector(width -1 downto 0);
  begin
    if v(7) = '1' then
      result(width-1 downto 8) := x"FF";
    else
      result(width-1 downto 8) := x"00";
    end if;
    result(7 downto 0)         := v(7 downto 0);
    return result;
  end signextendbyte;

  function signextendword(v : std_logic_vector ) return std_logic_vector is
    variable result         : std_logic_vector(width -1 downto 0);
  begin
    if v(15) = '1' then
      result := x"FFFF";
    else
      result := x"0000";
    end if;
    return result;
  end signextendword;

begin  -- the CPU

  idproc : process (clk)                -- instruction data pipeline
  begin
    
    if clk'event and clk = '1' then
      id1    <= idbus;   
      id2  <= id1;     
	  if opcode2 = opr then             -- decode skip instructions
        case opropcode2 is              -- they just null the pipeline
          when skpnz =>
            if zero = '0' then
              id2 <= x"0000";
            end if;
          when skpz =>
            if zero = '1' then
              id2 <= x"0000";
            end if;
          when skpnc =>
            if carrybit = '0' then
              id2 <= x"0000";
            end if;
          when skpc =>
            if carrybit = '1' then
              id2 <= x"0000";
			end if;
		  when skpp =>
            if accumsign = '0' then
              id2 <= x"0000";
            end if;
          when skpm =>
            if accumsign = '1' then
              id2 <= x"0000";
            end if;
		  when skpnf =>
            if flag = '0' then
              id2 <= x"0000";
            end if;
          when skpf =>
            if flag = '1' then
		      id2 <= x"0000";
            end if;
          when others => id2  <= id1;
        end case;
      end if;
      pc1_11 <= pc(11);
      pc2_11 <= pc1_11;
      if reset = '1' then
        id1  <= x"0000";                -- fill pipeline with 0 (nop)
      end if;
    end if;  -- if clk
  end process idproc;

  nextpcproc : process (clk, reset, pc, nextpc, id2,
                        ind0, ind2,idr, idbus, opcode0,
                        opcode2,zero, carrybit, pc2_11)  -- next pc calculation - jump decode
  begin
    if clk'event and clk = '1' then
      pc                      <= nextpc;
    end if;
    iabus                     <= nextpc;  -- program memory address from combinatorial    
    if reset = '1' then                   -- nextadd since blockram has built in addr register
      nextpc                  <= x"000";
    else
      if (opcode0 = jmp) or (opcode0 = jsr) then
        if ind0 = '1' then              	-- indirect
          nextpc(11 downto 0) <= idr(11 downto 0);
        else                            	-- direct
          nextpc(10 downto 0) <= idbus(10 downto 0);
          nextpc(11)          <= pc(11);	-- current page
        end if;
      elsif
        ((opcode2 = jmpnz) and (zero = '0')) or
        ((opcode2 = jmpz) and (zero = '1')) or
        ((opcode2 = jmpnc) and (carrybit = '0')) or
        ((opcode2 = jmpc) and (carrybit = '1')) then
        if ind2 = '1' then              -- indirect
          nextpc              <= idr(11 downto 0);
        else                            -- direct
          nextpc(10 downto 0) <= id2(10 downto 0);
          nextpc(11)          <= pc2_11;  -- set the new pc(11) to what pc(11) was at the jump
        end if;
      else
        nextpc                <= pc + '1';
      end if;  -- opcode = jmp
    end if;  -- no reset
  end process nextpcproc;

  mraproc : process (idbus, idx, idy, mra, ind0, ind1, ind2,
                     ireg0, ireg1, ireg2,
                     offset0, opradd0, clk)  -- memory read address generation
  begin
    mradd <= mra;
    if opcode0 /= opr and ind0 = '1' then
      case ireg0 is
			when "00" => mra <= idx(maddwidth -1 downto 0) + offset0;
      	when "01" => mra <= idy(maddwidth -1 downto 0) + offset0;
			when "10" => mra <= idz(maddwidth -1 downto 0) + offset0;
      	when "11" => mra <= idt(maddwidth -1 downto 0) + offset0;
			when others => null;
      end case;
    else
      mra   <= opradd0;
    end if;
  end process mraproc;

  mwaproc : process (clk, mwa)          -- memory write address generation
  begin
    mwadd     <= mwa;
    if clk'event and clk = '1' then
      if ind2 = '1' then
        case ireg2 is
		    when "00" => mwa <= idx(maddwidth -1 downto 0) + offset2;
          when "01" => mwa <= idy(maddwidth -1 downto 0) + offset2;
		    when "10" => mwa <= idz(maddwidth -1 downto 0) + offset2;
          when "11" => mwa <= idt(maddwidth -1 downto 0) + offset2;
			 when others => null;
         end case;
      else
        mwa   <= opradd2;
      end if;
    end if;
  end process mwaproc;

  oprrproc : process (clk)  		-- memory operand register  -- could remove to
  begin  							-- reduce pipelining depth but would impact I/O read
    if clk'event and clk = '1' then	-- access time  --> not good for larger systems
      oprr <= mibus;
    end if;
  end process oprrproc;

  accumproc : process (clk, accum, carrybit)  -- accumulator instruction decode - operate
  begin
    carryflg                          <= carrybit;
    if accum = x"0000" then
      zero <= '1';
    else
      zero <= '0';
	end if;

	if zero = '1' and flag = '0' then
      zero_sign <= '1';
    else
      zero_sign <= '0';
    end if;

   	if accum = x"FFFF" and flag = '1' then
      ones_sign <= '1';
    else
      ones_sign <= '0';
    end if;
	

    if clk'event and clk = '1' then
	case opcode2 is -- memory reference first
        when land         => accum    <= accum and oprr;
        when lor          => accum    <= accum or oprr;
        when lxor         => accum    <= accum xor oprr;
        when lda          => accum    <= oprr;
        when add          => 
							 accumcar <= (accumcar and carrymask) + oprr;
    						 oldaccumsign <= accumsign;
							 oldoprrsign <= oprrsign;
							 wasadd <= '1';
		when addc  		  => 
							 accumcar <= (accumcar and carrymask) + oprr + carrybit;
							 wasadd <= '1';
      when sub          => 
							 accumcar <= (accumcar and carrymask) - oprr;
    						 oldaccumsign <= accumsign;
							 oldoprrsign <= not oprrsign;
							 wasadd <= '0';
      when subc         => 
							 accumcar <= (accumcar and carrymask) - oprr - carrybit;
							 wasadd <= '0';
		when opr            =>  -- operate
        	case iopropcode2 is
         		when ldli       => acclowb  <= iopr2;					-- load immediate low byte
         	 	when ldhi       => acchighb <= iopr2;					-- load immediate high byte
         	 	when lxbi       => accum    <= signextendbyte(iopr2); -- load signextended byte
	  	 	 	when others     => null;
        	end case;
        	case opropcode2 is
          		when bswp       => accum    <= byteswap(accum);		-- byte swap accmulator
          		when rotcl      => accumcar <= rotcleft(accumcar);	-- rotate left through carry
          		when rotcr      => accumcar <= rotcright(accumcar); -- rotate right through carry  
					when ashr       => accumcar(width-2 downto 0) <= accumcar(width-1 downto 1);  -- shift right arithmetic  
											 accumcar(16) <= accumcar(0);
					when bnds        =>
            		if accumsign = '0' then
              			accum <= x"7FFF";		--  32767
            		else
              			accum <= x"8000";		-- -32768
            		end if;
            		if zero_sign = '0' and ones_sign = '0' then
              		carrybit <= '1';
            		else
             	 	carrybit <= '0';
            		end if;
          		when bndu        =>
            		if zero = '0' then
              			carrybit <= '1';
              			accum <= x"FFFF";
            		else
              			carrybit <= '0';
            		end if;
		 
--		  		when sats			=>
--		  			if oldoprrsign = '0' and oldaccumsign = '0' and accumsign = '1' then
--						accum <= x"7FFF";
--					end if;
--		  			if oldoprrsign = '1' and oldaccumsign = '1' and accumsign = '0' then
--					accum <= x"8000";
--					end if;

--		  		when satu			=>
--		  			if carrybit = '1' and wasadd = '1' then
--						accum <=x"FFFF";
--					end if;
--					if carrybit = '1' and wasadd = '0' then
--		    			accum <=x"0000";
--					end if;

				when ldr        => accum    <= idr;					
          	when ldx        => accum    <= idx;
          	when ldy        => accum    <= idy;
          	when ldz        => accum    <= idz;
          	when ldt        => accum    <= idt;
--				when lds        => accum    <= ids;
          	when str        => idr      <= accum;
          	when stx        => idx      <= accum;
          	when sty        => idy      <= accum;
          	when stz        => idz      <= accum;
          	when stt        => idt      <= accum;
	 	  		when sxw        => accum    <= signextendword(accum);	-- sign extend accumulator word			
		  		when ztof       => flag     <= zero;
		  		when ctof       => flag     <= carrybit;
		  		when stof       => flag     <= accumsign;
          	when others     => null;
        	end case;
     
		when others       => null;
	end case;
	if (opcode2 > 7) and (ind2 = '1') then
		if autoinc2 = '1' then
			case ireg2 is
				when "00" 	 => idx(10 downto 0) <= idx(10 downto 0) +1;
				when "01"    => idy(10 downto 0) <= idy(10 downto 0) +1;
				when "10"    => idz(10 downto 0) <= idz(10 downto 0) +1;
				when "11"    => idt(10 downto 0) <= idt(10 downto 0) +1;
				when others  => null;
			end case;
		end if;					
	end if;	
   if (opcode0 = jsr) then  -- jsr save part  -- save return address in idx reg      
      idr(paddwidth -1 downto 0)              <= pc +1;  -- this needs to be part of the accum process
   end if;  -- or there will be conflicts with stidx/jsr
  end if;  -- clk
  end process accumproc;

  staproc : process (clk, accum)  -- sta decode  -- not much to do but enable mwrite
  begin
    mobus <= accum;
    if clk'event and clk = '1' then
      if opcode2 = sta then
        mwrite <= '1';
--		  ids <= mobus;		-- because of longer turnaround on SP3
      else
        mwrite <= '0';
      end if;
    end if;
  end process staproc;

end Behavioral;
