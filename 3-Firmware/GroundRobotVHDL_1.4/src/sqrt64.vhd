library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity sqrt64 is
   Port ( 
	ibus: in STD_LOGIC_VECTOR (15 downto 0);
	obus: out STD_LOGIC_VECTOR (15 downto 0);
	readstb: in STD_LOGIC;
	writestb: in STD_LOGIC;
	selradd: in STD_LOGIC_VECTOR (2 downto 0);
	selwadd: in STD_LOGIC_VECTOR (2 downto 0);
	clk: in STD_LOGIC
	 );
end sqrt64;


architecture behavioral of sqrt64 is

signal m: STD_LOGIC_VECTOR (63 downto 0);
signal y: STD_LOGIC_VECTOR (63 downto 0);
signal x: STD_LOGIC_VECTOR (63 downto 0);
signal count: STD_LOGIC_VECTOR (5 downto 0);
signal invalid: STD_LOGIC;


function shiftr1(d1:STD_LOGIC_VECTOR(63 downto 0)) return STD_LOGIC_VECTOR is
	variable result : STD_LOGIC_VECTOR(63 downto 0);
begin
	result := '0' & d1(63 downto 1);
	return result;
end shiftr1;

function shiftr2(d1:STD_LOGIC_VECTOR(63 downto 0)) return STD_LOGIC_VECTOR is
	variable result : STD_LOGIC_VECTOR(63 downto 0);
begin
	result := "00" & d1(63 downto 2);
	return result;
end shiftr2;


begin
	asqrt64: 
	process  
							( clk,y,count)
	begin
		if clk'event and clk = '1' then
			if writestb = '1' then
				case selwadd is
					when "000" => 	x(15 downto  0) <= ibus;
										y <= x"0000000000000000";
										m <= x"4000000000000000";
										invalid <= '1';
					when "001" => 	x(31 downto 16) <= ibus;
					when "010" => 	x(47 downto 32) <= ibus;
					when "011" =>  x(63 downto 48) <= ibus;
					               count <= "100000";
										invalid <= '0';
					when others => null;
				end case;
			end if; -- writestb
			if count /= 0 then						
            y <= shiftr1(y);
				if x >= (y or m) then	 -- synthesiser needs more hints
					x <= x - (y or m);	 -- basically just one subtractor is needed
					y <= shiftr1(y) or m; -- synthesiser makes 2 comparators plus a subtractor
				end if;
				m <= shiftr2(m);				
				count <= count -1;
			end if; -- count	
		end if;

		obus <= "ZZZZZZZZZZZZZZZZ";
		if readstb = '1' then
			case selradd is
				when "000" => obus <= y(15 downto 0);
				when "001" => obus <= y(31 downto 16);
				when "010" => obus <= y(47 downto 32);
				when "011" => obus <= y(63 downto 48);
				when "100" => obus <= x(15 downto 0);
				when "101" => obus <= x(31 downto 16);			
				when "110" => obus <= x(47 downto 32);
				when "111" => obus(5 downto 0) <= count;
								  obus(14 downto 6) <= (others => '0');
								  obus(15) <= invalid;
				when others => obus <= "ZZZZZZZZZZZZZZZZ";
			end case;
		else
			 obus <= "ZZZZZZZZZZZZZZZZ";
		end if;

	end process;

end behavioral;

