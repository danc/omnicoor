library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity fiveiorb is
    Port (			
	 		obus: out STD_LOGIC_VECTOR (4 downto 0);
			readport: in STD_LOGIC;
			portdata: in STD_LOGIC_VECTOR (4 downto 0) );
end fiveiorb;

architecture behavioral of fiveiorb is

begin
	afiveiorb: process (portdata,readport)
	begin
		if readport = '1' then
			obus <= portdata;
 	   else
			obus <= (others => 'Z') ;
		end if;
	end process;

end behavioral;
