library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity mul16a32 is
   Port ( 
	m: in STD_LOGIC_VECTOR (15 downto 0);
	p: out STD_LOGIC_VECTOR (15 downto 0);
	mlatch: in STD_LOGIC;
	readm: in STD_LOGIC;
	selradd: in STD_LOGIC_VECTOR (2 downto 0);
	selwadd: in STD_LOGIC_VECTOR (2 downto 0);
	clk: in STD_LOGIC
	 );
end mul16a32;


architecture behavioral of mul16a32 is

signal product: STD_LOGIC_VECTOR (31 downto 0);
signal unsproduct: STD_LOGIC_VECTOR (31 downto 0);
signal alatch: STD_LOGIC_VECTOR (15 downto 0);
signal blatch: STD_LOGIC_VECTOR (15 downto 0);
signal accum: STD_LOGIC_VECTOR (31 downto 0);
signal addend: STD_LOGIC_VECTOR (31 downto 0);
--signal satprod: STD_LOGIC_VECTOR (15 downto 0);
signal patch: STD_LOGIC_VECTOR (15 downto 0);
begin
	amul16s32: 
	process  
							( clk,
							  m,
							  mlatch,
							  alatch,
							  blatch,
--							  satprod,
							  product,
							  unsproduct,
							  readm,
							  selradd,
							  selwadd
							)
	begin
		if clk'event and clk = '1' then
			unsproduct <= alatch * blatch;
			if mlatch = '1' then
				case selwadd is
					when "000" => alatch <= m;
					when "001" => blatch <= m;
					when "010" => accum <= accum + addend;
					when "100" => accum <= accum + addend;
					when "110" => accum(15 downto 0) <= m;	
					when "111" => accum(31 downto 16) <= m;
					when others =>null;
				end case;
			end if;	
		end if;	
		if alatch(15) = '1' then
			patch <= blatch;
		else
			patch <= x"0000";
		end if;
		if selwadd(2) = '0' then
			addend <= product;
		else
			addend <= unsproduct;
		end if;
		product(31 downto 16) <= unsproduct(31 downto 16) - patch;
		product(15 downto 0) <= unsproduct(15 downto 0);
--      saturated product not needed at the moment
-- 		and quite large so has been removed for now
--		satprod <= product(15 downto 0);
--		if (product(31 downto 15) /= "00000000000000000") and
--    (product(31 downto 15) /= "11111111111111111") then
--			if product(31) = '0' then
-- 			satprod <= x"7FFF";
--			else
--				satprod <= x"8000";
--			end if;
--		end if;
		p <= "ZZZZZZZZZZZZZZZZ";

		if readm = '1' then
			case selradd is
				when "000" => p <= alatch;
				when "001" => p <= blatch;			
				when "010" => p <= product(15 downto 0);
				when "011" => p <= product(31 downto 16);
--				when "101" => p <= satprod;
				when "101" => p <= product(23 downto 8);
				when "100" => p <= unsproduct(31 downto 16);
				when "110" => p <= accum(15 downto 0);
				when "111" => p <= accum(31 downto 16);
				when others => p <= "ZZZZZZZZZZZZZZZZ";
			end case;
		else
			p <= "ZZZZZZZZZZZZZZZZ";
		end if;

	end process;

end behavioral;

