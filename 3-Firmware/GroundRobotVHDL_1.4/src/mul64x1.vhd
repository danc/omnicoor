library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity mul64 is
   Port ( 
	ibus: in STD_LOGIC_VECTOR (15 downto 0);
	obus: out STD_LOGIC_VECTOR (15 downto 0);
	readstb: in STD_LOGIC;
	writestb: in STD_LOGIC;
	selradd: in STD_LOGIC_VECTOR (2 downto 0);
	selwadd: in STD_LOGIC_VECTOR (2 downto 0);
	clk: in STD_LOGIC
	 );
end mul64;


architecture behavioral of mul64 is

signal m1: STD_LOGIC_VECTOR (31 downto 0);
signal p: STD_LOGIC_VECTOR(64 downto 0); 
signal count: STD_LOGIC_VECTOR (5 downto 0);
signal invalid: STD_LOGIC;

function rshift(d1:STD_LOGIC_VECTOR(64 downto 0)) return STD_LOGIC_VECTOR is
	variable result : STD_LOGIC_VECTOR(64 downto 0);
begin
	result := '0' & d1(64 downto 1);
	return result;
end rshift;

begin
	amul64: 
	process  
							( clk,
							  selradd,
							  readstb
							)
	begin
		if clk'event and clk = '1' then
			if writestb = '1' then
				case selwadd is				
									 
					when "000" => 	m1(15 downto 0) <= ibus;
					               invalid <= '1';
					when "001" => 	m1(31 downto 16) <= ibus;
					when "010" => 	p(15 downto 0) <= ibus;  -- m2
					when "011" => 	
										p(31 downto 16) <= ibus; -- m
										p(63 downto 32) <= (others => '0');
										count <= "100000";
										invalid <= '0';
					when others => null;
				end case;
			else
				if count /= 0 then
					if p(0) = '1' then
						p <= rshift(m1 + p(64 downto 32) & p(31 downto 0));
					else
				   	p <= rshift(p);
					end if;		
					count <= count -1;
				end if;
			end if; -- count	
		end if;

		obus <= "ZZZZZZZZZZZZZZZZ";
		if readstb = '1' then
			case selradd is
				when "000" => obus <= p(15 downto 0);
				when "001" => obus <= p(31 downto 16); 
				when "010" => obus <= p(47 downto 32);
				when "011" => obus <= p(63 downto 48);
				when "100" => obus <= p(40 downto 25);	  -- div by 2^25
				when "101" => obus <= p(56 downto 41);	  -- for endpoint calculation
				when "110" => obus(6 downto 0) <= p(63 downto 57);
				              obus(15 downto 7) <= (others => '0');
				when "111" => obus(5 downto 0) <= count;
								  obus(14 downto 6) <= (others => '0');
								  obus(15) <= invalid;
				when others => obus <= "ZZZZZZZZZZZZZZZZ";
			end case;
		else
			 obus <= "ZZZZZZZZZZZZZZZZ";
		end if;

	end process;

end behavioral;

