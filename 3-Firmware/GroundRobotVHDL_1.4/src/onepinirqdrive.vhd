library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity OnePinIRQDrive is
    Port (		
	   	clear: in STD_LOGIC;
			clk: in STD_LOGIC;
			ibus: in STD_LOGIC_VECTOR (7 downto 0);
			obus: out STD_LOGIC_VECTOR (7 downto 0);
			hostibus: in STD_LOGIC_VECTOR (7 downto 0);
			loadport: in STD_LOGIC;
			readport: in STD_LOGIC;
			setirq: in STD_LOGIC;
			clrirq: in STD_LOGIC;
			istatus: out STD_LOGIC_VECTOR (7 downto 0);
			irqpol: in STD_LOGIC;
			irq: out STD_LOGIC
 			);
end OnePinIRQDrive;

architecture behavioral of OnePinIRQDrive is

signal istatusreg: STD_LOGIC_VECTOR (7 downto 0);
signal irqgen: STD_LOGIC;
signal irqmask: STD_LOGIC;

begin
	airqdrive: process (
								clk,
								ibus,
								readport,
								irqmask
								)
	begin
		if clk'event and clk = '1' then
			if loadport = '1' then
				irqmask <= ibus(6);
			end if; 
			
			if setirq = '1' and clrirq = '0' then 				
				istatusreg <= istatusreg or ibus;
			end if;
			if setirq = '0' and clrirq = '1' then
				istatusreg <= istatusreg and (not hostibus);			
			end if;
			if setirq = '1' and clrirq = '1' then 
				istatusreg <= (istatusreg and (not hostibus)) or ibus;			
			end if;
			
			if istatusreg = x"00" then
				irqgen <= '0';
			else
				irqgen <= '1';
			end if;
										
			if clear = '1' then
				irqmask <= '0';
				istatusreg <= (others => '0');
          end if;
		end if; -- clk

		istatus <= istatusreg;
		
		if readport = '1' then
			obus(6) <= irqmask;
			obus(7) <= irqgen;
		else
			obus <= (others => 'Z');
		end if;
		irq <= irqpol xor (not (irqgen and irqmask));
	end process;
end behavioral;
