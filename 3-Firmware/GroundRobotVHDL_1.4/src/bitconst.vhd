library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity bitconst is
   Port ( 
	dout: out STD_LOGIC_VECTOR (15 downto 0);
	readst: in STD_LOGIC;
	readadd: in STD_LOGIC_VECTOR (4 downto 0)
	 );
end bitconst;


architecture behavioral of bitconst is

signal const: STD_LOGIC_VECTOR (15 downto 0);

begin
	ABitConstantGen: 
	process  (readst,readadd)
	begin
		const <= x"0000";
		if readst = '1' then
			for i in 0 to 15 loop 
				if i = readadd(3 downto 0) then
					const(i) <= not readadd(4);
				else
					const(i) <= readadd(4);
				end if;
			end loop;
			dout <= const;		
		else 
			dout <= "ZZZZZZZZZZZZZZZZ";
		end if;
	end process;

end behavioral;

