library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- x3 third time is a charm - needs 33 clocks
entity div64 is
   Port ( 
	ibus: in STD_LOGIC_VECTOR (15 downto 0);
	obus: out STD_LOGIC_VECTOR (15 downto 0);
	readstb: in STD_LOGIC;
	writestb: in STD_LOGIC;
	selradd: in STD_LOGIC_VECTOR (2 downto 0);
	selwadd: in STD_LOGIC_VECTOR (2 downto 0);
	clk: in STD_LOGIC
	 );
end div64;


architecture behavioral of div64 is

signal dividend: STD_LOGIC_VECTOR (64 downto 0);
signal divisor: STD_LOGIC_VECTOR (31 downto 0);
signal count: STD_LOGIC_VECTOR (5 downto 0);
signal invalid: STD_LOGIC;

function subtop(d1:STD_LOGIC_VECTOR(64 downto 0); d2:STD_LOGIC_VECTOR(31 downto 0)) return STD_LOGIC_VECTOR is
	variable result : STD_LOGIC_VECTOR(64 downto 0);
begin
	result(64) := d1(64);
	result(63 downto 32) := d1(63 downto 32) - d2;
	result(31 downto 0) := d1(31 downto 0);
	return result;
end subtop;

function shiftl1(d1:STD_LOGIC_VECTOR(64 downto 0)) return STD_LOGIC_VECTOR is
	variable result : STD_LOGIC_VECTOR(64 downto 0);
begin
	result := d1(63 downto 0) & '1';
	return result;
end shiftl1;

function shiftl0(d1:STD_LOGIC_VECTOR(64 downto 0)) return STD_LOGIC_VECTOR is
	variable result : STD_LOGIC_VECTOR(64 downto 0);
begin
	result := d1(63 downto 0) & '0';
	return result;
end shiftl0;


begin
	adiv64: 
	process  
							( clk,
							  selradd,
							  readstb,
							  divisor,
							  dividend
							)
	begin
		if clk'event and clk = '1' then
			if writestb = '1' then
				case selwadd is
					when "000" => 	dividend(15 downto 0) <= ibus;
					invalid <= '1';
					when "001" => 	dividend(31 downto 16) <= ibus;
					when "010" => 	dividend(47 downto 32) <= ibus;
					when "011" => 	dividend(63 downto 48) <= ibus;					
					when "100" => 	divisor(15 downto  0) <= ibus;
					when "101" => 	divisor(31 downto 16) <= ibus;
										count <= "100001"; -- normal divide = 33 clocks
										invalid <= '0';

--					when "111" => 	count <= "100001"; -- normal divide = 33 clocks
--					when "111" => 	count <= ibus(5 downto 0);
					when others => null;
				end case;
			end if; -- writestb
			if count /= 0 then						
				if  dividend(63 downto 32) < divisor  then				
					dividend <= shiftl0(dividend);
				else	
					dividend <= shiftl1(subtop(dividend,divisor));											
				end if;					
				count <= count -1;
			end if; -- count	
		end if;

		obus <= "ZZZZZZZZZZZZZZZZ";
		if readstb = '1' then
			case selradd is
				when "000" => obus <= dividend(15 downto 0);
				when "001" => obus <= dividend(31 downto 16);
				when "010" => obus <= dividend(48 downto 33);
				when "011" => obus <= dividend(64 downto 49);
				when "100" => obus <= divisor(15 downto  0);
				when "101" => obus <= divisor(31 downto 16);
			

				when "111" => obus(5 downto 0) <= count;
								  obus(14 downto 6) <= (others => '0');
								  obus(15) <= invalid;
				when others => obus <= "ZZZZZZZZZZZZZZZZ";
			end case;
		else
			 obus <= "ZZZZZZZZZZZZZZZZ";
		end if;

	end process;

end behavioral;

