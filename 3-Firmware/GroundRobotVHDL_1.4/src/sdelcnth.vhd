library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;


entity dcounter is
  port (
    obus            : out std_logic_vector (15 downto 0);
    ibus            : in  std_logic_vector (9 downto 0);
    quada           : in  std_logic;
    quadb           : in  std_logic;
    index           : in  std_logic;
    ccrloadcmd      : in  std_logic;
    ccrreadcmd      : in  std_logic;
    countoutreadcmd : in  std_logic;
    countlatchcmd   : in  std_logic;
	 countclearcmd   : in  std_logic;
	 clearcntclrdcmd  : in  std_logic;
    clk             : in  std_logic
    );
end dcounter;

architecture behavioral of dcounter is

  signal count          : std_logic_vector (8 downto 0);
  signal countoutlatch  : std_logic_vector (8 downto 0);
  signal quada1         : std_logic;
  signal quada2         : std_logic;
  signal quadacnt       : std_logic_vector (2 downto 0);
  signal quadafilt      : std_logic;
  signal quadb1         : std_logic;
  signal quadb2         : std_logic;
  signal quadbcnt       : std_logic_vector (2 downto 0);
  signal quadbfilt      : std_logic;
  signal qcountup       : std_logic;
  signal qcountdown     : std_logic;
  signal udcountup      : std_logic;
  signal udcountdown    : std_logic;
  signal index1         : std_logic;
  signal index2         : std_logic;
  signal indexcnt       : std_logic_vector (2 downto 0);
  signal indexfilt      : std_logic;
  signal indexdet       : std_logic;
  signal doclear        : std_logic;
  signal clearonindex   : std_logic;    -- ccr register bits...
  signal quadfilter     : std_logic := '1';
  signal countermode    : std_logic;
  signal counterdir     : std_logic;
  signal clearonce     	: std_logic;
  signal indexpol       : std_logic;
  signal localclear     : std_logic;
  signal divisor        : std_logic;
  signal countcleared   : std_logic;
  signal dolatch        : std_logic;          -- dolatch
  signal dotrans        : std_logic;

begin
  acounter : process (ccrreadcmd, clearonindex, clk, countcleared, countermode,
                      countoutlatch, countoutreadcmd, index, indexpol, quada1,
                      quadb1, quadfilter)

  begin
    if clk'event and clk = '1' then     -- per clk stuff
      divisor <= not divisor;
      if countlatchcmd = '1'  then
        dolatch <= '1';
      end if;
      
		if countclearcmd = '1' then
			localclear <= '1';
		end if;
		
		if ccrloadcmd = '1' then
		  clearonce		<= ibus(9);
        counterdir   <= ibus(8);
        countermode  <= ibus(7);
        quadfilter   <= ibus(6);
        countcleared <= ibus(5);
        clearonindex <= ibus(4);
        indexpol     <= ibus(3);
        localclear   <= ibus(2);
      end if;

      if clearcntclrdcmd = '1' then
			countcleared <= '0';
		end if;
		
      if divisor = '1' then             -- divided by 2 stuff

        if quadfilter = '1' then
          quada1 <= quadafilt;
        else quada1 <= quada;
        end if;
        quada2      <= quada1;

        if quadfilter = '1' then
          quadb1 <= quadbfilt;
        else quadb1 <= quadb;
        end if;
        quadb2      <= quadb1;

        if quadfilter = '1' then
          index1 <= indexfilt;
        else index1 <= index;
        end if;
        index2      <= index1;

        -- deadended counter for 'a' input filter --
        if (quada = '1') and (quadacnt /= 7) then
          quadacnt <= quadacnt + 1;
        end if;
        if (quada = '0') and (quadacnt /= 0) then
          quadacnt <= quadacnt -1;
        end if;
        if quadacnt = 7 then
          quadafilt <= '1';
        end if;
        if quadacnt = 0 then
          quadafilt <= '0';
        end if;

        -- deadended counter for 'b' input filter --
        if (quadb = '1') and (quadbcnt /= 7) then
          quadbcnt <= quadbcnt + 1;
        end if;
        if (quadb = '0') and (quadbcnt /= 0) then
          quadbcnt <= quadbcnt -1;
        end if;
        if quadbcnt = 7 then
          quadbfilt <= '1';
        end if;
        if quadbcnt = 0 then
          quadbfilt <= '0';
        end if;

        -- deadended counter for index input filter --
        if (index = '1') and (indexcnt /= 7) then
          indexcnt <= indexcnt + 1;
        end if;
        if (index = '0') and (indexcnt /= 0) then
          indexcnt <= indexcnt -1;
        end if;
        if indexcnt = 7 then
          indexfilt <= '1';
        end if;
        if indexcnt = 0 then
          indexfilt <= '0';
        end if;

        if
          ((clearonindex = '1') and (index1 = '1') and (index2 = '0') and (indexpol = '1')) or  -- rising edge of index
          ((clearonindex = '1') and (index1 = '0') and (index2 = '1') and (indexpol = '0')) then  -- falling edge of index
          indexdet <= '1';
        end if;

        if dolatch = '1' then
          dotrans <= '1';
          dolatch <= '0';
        else
          dotrans <= '0';
        end if;

        if dotrans = '1' then
          countoutlatch <= count;
          count         <= "000000000";
        end if;

        if countermode = '0' and (
          (quada2 = '0' and quada1 = '1' and quadb2 = '0' and quadb1 = '0') or
          (quada2 = '0' and quada1 = '0' and quadb2 = '1' and quadb1 = '0') or
          (quada2 = '1' and quada1 = '1' and quadb2 = '0' and quadb1 = '1') or
          (quada2 = '1' and quada1 = '0' and quadb2 = '1' and quadb1 = '1')) then                  
          qcountup <= '1';
        else
          if dotrans = '0' then
            qcountup <= '0';
          end if;
        end if;

        if (countermode = '1' and
            quadb2 = '1' and quada2 = '0' and quada1 = '1') then
          	udcountup <= '1';
        else
          if dotrans = '0' then
            udcountup <= '0';
          end if;
        end if;
        
        if countermode = '0' and (
          (quada2 = '0' and quada1 = '0' and quadb2 = '0' and quadb1 = '1') or
          (quada2 = '0' and quada1 = '1' and quadb2 = '1' and quadb1 = '1') or
          (quada2 = '1' and quada1 = '0' and quadb2 = '0' and quadb1 = '0') or
          (quada2 = '1' and quada1 = '1' and quadb2 = '1' and quadb1 = '0')) then
          qcountdown <= '1';
        else
          if dotrans = '0' then
            qcountdown <= '0';
          end if;
        end if;

        if (countermode = '1' and
        		quadb2 = '0' and quada2 = '0' and quada1 = '1') then
          	udcountdown <= '1';
        else
          if dotrans = '0' then
            udcountdown <= '0';
          end if;
        end if;

        if ((qcountup = '1' or udcountup = '1') and dotrans = '0') then
          if counterdir = '0' then
			 	count <= count + 1;
        	 else
			 	count <= count -1;
			 end if;
		  end if;
        if ((qcountdown = '1' or udcountdown = '1') and dotrans = '0') then
          if counterdir = '0' then
			 	count <= count - 1;
			 else
				count <= count + 1;
			 end if;
        end if;

        if (indexdet = '1' and quada2 = '0' and quadb2 = '0') or localclear = '1' then
          doclear <= '1';
        end if;

        if doclear = '1' then
          count        <= "000000000";
          countcleared <= '1';
          doclear      <= '0';
          indexdet     <= '0';
          localclear   <= '0';
			 if clearonce = '1' then
			 	clearonindex <= '0';
			 end if;
        end if;

      end if;
    end if;  -- clk
    obus <= "ZZZZZZZZZZZZZZZZ";
    if (countoutreadcmd = '1') and(ccrreadcmd = '0') then
      obus(8 downto 0) <= countoutlatch;
      obus(9)          <= countoutlatch(8);
      obus(10)         <= countoutlatch(8);
      obus(11)         <= countoutlatch(8);
      obus(12)         <= countoutlatch(8);
      obus(13)         <= countoutlatch(8);
      obus(14)         <= countoutlatch(8);
      obus(15)         <= countoutlatch(8);
    end if;
    if (ccrreadcmd = '1') and (countoutreadcmd = '0') then
      obus(9) <= clearonce;
		obus(8) <= counterdir;
		obus(7) <= countermode;
      obus(6) <= quadfilter;
      obus(5) <= countcleared;
      obus(4) <= clearonindex;
      obus(3) <= indexpol;
      obus(2) <= index;
      obus(1) <= quada1;
      obus(0) <= quadb1;
    end if;
    
  end process;
end behavioral;
