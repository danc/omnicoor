library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity CFIFO16 is
    Port ( clk : in std_logic;
           obus : out std_logic_vector(15 downto 0);
           selradd : in std_logic_vector(1 downto 0);
           selwadd : in std_logic_vector(1 downto 0);
		     readsel : in std_logic;
		     writesel : in std_logic;
			  hostdata : in std_logic_vector(15 downto 0);
			  hostwrite	: in std_logic;
			  full : out std_logic;
			  half_full : out std_logic
			  );
end CFIFO16;

architecture Behavioral of CFIFO16 is

  component SRL16E
--
    generic (INIT : bit_vector);


--
    port (D   : in  std_logic;
          CE  : in  std_logic;
          CLK : in  std_logic;
          A0  : in  std_logic;
          A1  : in  std_logic;
          A2  : in  std_logic;
          A3  : in  std_logic;
          Q   : out std_logic); 
  end component;
	

	signal popadd: std_logic_vector(3 downto 0);
	signal popdata: std_logic_vector(15 downto 0);
	signal datacounter: std_logic_vector(4 downto 0);
	alias  datacounterMSB: std_logic is datacounter(4);
	alias  datacounterNMSB: std_logic is datacounter(3);
	signal push: std_logic;  
	signal pop: std_logic;  
	signal clear: std_logic;
	signal fifoempty: std_logic;  

			
begin

	fifosrl: for i in 0 to 15 generate
		asr16e: SRL16E generic map (x"0000") port map(
 			 D	  => hostdata(i),
          CE  => push,
          CLK => clk,
          A0  => popadd(0),
          A1  => popadd(1),
          A2  => popadd(2),
          A3  => popadd(3),
          Q   => popdata(i)
			);	
  	end generate;

	afifo: process (clk,selradd,readsel,popdata,datacounter)
	begin
		if clk'event and clk = '1' then
			
			if push = '1'  and pop = '0' then	-- a push
		 		-- always increment the data counter if not full
				datacounter <= datacounter +1;
				popadd <= popadd +1;						-- popadd must follow data down shiftreg
			end if;		 		
						   
			if  pop = '1' and push = '0' then	-- a pop
				-- always decrement the data counter if not empty
				datacounter <= datacounter -1;
				popadd <= popadd -1;
			end if;

-- if both push and pop are asserted we dont change either counter

	  
			if clear = '1' then -- a clear fifo
				popadd  <= (others => '1');
				datacounter <= (others => '0');
			end if;	
	

		end if; -- clk rise
		
		if hostwrite = '1' and datacounter /= 16 then
			push <= '1';
		else
			push <= '0';
		end if;
		
		if  writesel = '1' and selwadd = "00"  and datacounter /= 0 then
			pop <= '1';
		else			
			pop <= '0';
		end if;
		
		if writesel = '1' and selwadd = "01" then 				 
			clear <= '1';
		else			
			clear <= '0';
		end if;
				
		obus <= (others => 'Z');
		if	readsel = '1' then
			case selradd is 
 				when "00" => obus <= popdata;				-- read popdata
				when "01" => obus(4 downto 0) <= datacounter; -- read datacounter
								 obus(15 downto 5) <= (others => '0');	
				when "10" => obus(1) <= popdata(14);
								 obus(0) <= popdata(13);
								 obus(15 downto 2) <= (others => '0');
			  	when "11" => obus(2) <= popdata(12);	-- read axis
								 obus(1) <= popdata(11);
								 obus(0) <= popdata(10);
								 obus(15 downto 3) <= (others => '0');
				when others => null;
			end case;	    
		else 
			obus <= (others => 'Z'); 
		end if;		
				
		full <= datacounterMSB;
		half_full <= (datacounterNMSB or datacounterMSB);
	
	end process;
end Behavioral;
