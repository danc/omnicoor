library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bitport is
	 generic(InitialState: std_logic);
    Port ( clk : in std_logic;
           ibus : in std_logic;
           load : in std_logic;
           bitout : out std_logic);
end bitport;

architecture Behavioral of bitport is

signal thebit: std_logic := InitialState;

begin
	abitport: process  (clk,thebit)
							
	begin
		if clk'event and clk = '1' then		
			if (load = '1') then 
 		   	thebit <= ibus;
			end if;	
		end if;
		bitout <= thebit;
	end process;
end Behavioral;
