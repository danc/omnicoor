library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bitinport is
    Port ( 
           obus : out std_logic;
           read : in std_logic;
           bitin : in std_logic);
end bitinport;

architecture Behavioral of bitinport is


begin
	abitinport: process  (read,bitin)							
	begin
		if read = '1' then	
 		 	obus <= bitin; 
		else
			obus <= 'Z';
		end if;
	end process;
end Behavioral;
