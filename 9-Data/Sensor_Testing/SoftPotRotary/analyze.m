%clear all
close all
for i = 1:5
    
  %{
eval(['camdata = cam_sys_data_read(''run', num2str(i) '.csv'');'])
ac = [transpose(camdata.t) transpose(camdata.theta)];
eval(['load(''run' , num2str(i) , '.mat'');']);
eval(['test' , num2str(i) , '  = struct(''camera'',ac,''voltage'',av);'])
eval(['test' , num2str(i) , '.voltage(:,1) = test' , num2str(i) , '.voltage(:,1) - test' , num2str(i) , '.voltage(1,1);'])
eval(['test' , num2str(i) , '.camera(:,1) = test' , num2str(i) , '.camera(:,1) / 10 ;'])
%}
eval(['load(''test' , num2str(i) , '.mat'');']); 
figure_handle = figure;
subplot(2,1,2);hold on
eval(['plot(test' , num2str(i) , '.camera(:,1), test' , num2str(i) , '.camera(:,2) );'])
subplot(2,1,1);hold on
eval(['plot(test' , num2str(i) , '.voltage(:,1), test' , num2str(i) , '.voltage(:,2) );'])

all_ha = findobj( figure_handle, 'type', 'axes', 'tag', '' );
linkaxes( all_ha, 'x' );
end