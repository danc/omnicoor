This folder contains data taken during the parameterization of the DJI Motors.

Files:
DJI_MotorWindingResistance.xlsx - This file contains current and voltage measurements taken across the three terminals of a DJI motor. These measurements were then used to calculate the resistance of the motor windings between the terminals (For the Red DJI motors)

