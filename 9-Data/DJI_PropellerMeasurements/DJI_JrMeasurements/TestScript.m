% Extract time slice
start = 7.25;
stop = 9;
dataToUse = data((start*Fs):(stop*Fs),1);
stepSize = ceil(0.5*Fs/1000);    % one spectral slice every 1 ms
window=ceil(100*Fs/1000);       % 100 ms data window
[tf, f, t] = myspecgram(dataToUse(:,1), 2^nextpow2(window), Fs, window, window-stepSize);

abstf = abs(tf);
abstf(abstf < 1) = 0;
abstf = abstf(1:length(f),:);

indices = find(f < 500);
maxF = max(indices);
% f(2:(2^nextpow2(window)*max(f)/Fs))
pcolor(t, f(1:maxF), 20*log10(abstf(1:maxF,:)));
axis xy;
colormap(flipud(gray));
shading interp;