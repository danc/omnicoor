This folder contains data taken during the parameterization of the DJI propellers:

Folders:

DJI_JrMeasurements - This folder contains the data used to determine the moment of inertia for the propeller and motor system.

Files:

DJI_KdMeasurements - These files contain the motor speed, ESC % input, ESC input voltage, and current draw of the motor for varying ESC % input and voltages

DJI_KtMeasurements - These files contain the motor speed, ESC % input, and thrust generated by the four motors under test. The ESC % was varied during the testing.
	The columns inside this file are the following:
		PWM Duty Cycle Input - This is the input from the function generator when set to a 400Hz square wave.
		Motor 0-3 Speeds - These are the speeds of the motors (In RPM) of the motors on the MicroCART quad (Motors are labelled as they are on the quad).
		Thrust Produced - The total thrust reading (In grams) of the scale for the measurements
		Supply Voltage - The voltage being fed into the ESCs
		Current Draw - The current drawn by all 4 motors
		Duty Cycle percentage - The percentage of the 1-2ms duty cycle the ESC was getting
		Motor 0-3 Speeds - These are the computed speeds of the motors (in rad/sec)
		Thrust Produced - The computed thrust produced by all 4 motors (In Newtons)
