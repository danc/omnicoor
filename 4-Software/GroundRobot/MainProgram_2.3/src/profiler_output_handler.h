#ifndef _PROFILER_OUTPUT_HANDLER_H_
#define _PROFILER_OUTPUT_HANDLER_H_

class ProfilerOutputHandler {
	public:
		virtual ~ProfilerOutputHandler() {}

		/*
		 * Called before the profiler starts sending profile data.  Can
		 * be used to initialize / print headers for the printed data
		 *
		 * @param fTotal The total time of the profile loop
		 */
		virtual void beginOutput(float tTotal)=0;

		/*
		 * Called for each sample that was recorded in the profiler.  Contains
		 * data about a section of code that was profiled.
		 *
		 * @param fMin The minimum amount of time this sample took to run
		 * @param fAvg The average amount of time this sample took to run
		 * @param fMax The maximum amount of time this sample took to run
		 * @param fTime The time it took this sample to run
		 * @param callCount The number of times this sample was called
		 * @param name The name of the sample
		 * @param parentCount The number of parent samples before this sample
		 */
		virtual void sample(float fMin, float fAvg, float fMax, float fTime, unsigned char callCount, char *name, unsigned char parentCount)=0;

		/*
		 * Called when all samples have been sent, and the profiler is done
		 * sending data to be outputted.
		 */
		virtual void endOutput()=0;
};

#endif	// _PROFILER_OUTPUT_HANDLER_H_
