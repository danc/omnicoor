#ifndef _NETWORK__NETWORK_BITSTREAM_H_
#define _NETWORK__NETWORK_BITSTREAM_H_

#include "defines.h"

class NetworkBitstream {
	private:
		// The data read in from the network
		char data[MAX_NETWORK_PACKET_SIZE];

		// The length of the data
		unsigned short length;

		// Our current read pointer
		unsigned int readPtr;

		// Our current write pointer
		unsigned int writePtr;
	public:
		/*
		 * Creates a new network bitstream with no data
		 */
		NetworkBitstream();

		/*
		 * Creates a new network bitstream with the given data
		 *
		 * @param data The data read in from the length
		 * @param length The length of data
		 */
		NetworkBitstream(const char *data, unsigned short length);

		/*
		 * Frees any resources used by this object
		 */
		~NetworkBitstream();

		/*
		 * Clears the data in the object, reseting the length
		 * of the data to 0, and reseting the read & write
		 * pointers to 0.
		 */
		void reset();

		/*
		 * Resets the read pointer to the beginning of the data.
		 */
		void resetReadPtr();

		/*
		 * Resets the write pointer to the beginning of the data.
		 */
		void resetWritePtr();

		/*
		 * Gets the length of the data in the stream
		 *
		 * @returns The length of the data
		 */
		int getLength() const;

		/*
		 * Gets the data in the stream
		 *
		 * @returns A pointer to the data in the stream
		 */
		const char *getData() const;

		/*
		 * Sets the data this object represents
		 *
		 * @param data The data in the stream
		 * @param length The length of the data
		 *
		 * @returns void
		 */
		void setData(const char *data, int length);

		/*
		 * Reads in one character from the network stream
		 *
		 * @param in The input network bitstream
		 * @param out The variable to store the data from the network
		 *
		 * @returns The input network bitstream
		 */
		friend NetworkBitstream &operator >>(NetworkBitstream &in, char *data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, short &data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, int &data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, char &data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, float &data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, long &data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, unsigned short &data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, unsigned int &data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, unsigned char &data);
		friend NetworkBitstream &operator >>(NetworkBitstream &in, unsigned long &data);
		//template <typename T>
		//friend NetworkBitstream &operator >>(NetworkBitstream &in, T &out);

		/*
		 * Writes in one data type to the network stream
		 *
		 * @param out The network bitstream to write to
		 * @param data The variable to store write to the network
		 *
		 * @returns The input network bitstream
		 */
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const char *data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const short &data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const int &data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const char &data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const float &data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const long &data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const unsigned short &data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const unsigned int &data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const unsigned char &data);
		friend NetworkBitstream &operator <<(NetworkBitstream &out, const unsigned long &data);
		//template <typename T>
		//friend NetworkBitstream &operator <<(NetworkBitstream &out, const T &data);

};

/*
 * Reads in one character from the network stream
 *
 * @param in The input network bitstream
 * @param out The variable to store the data from the network
 *
 * @returns The input network bitstream
 */
/*
template <typename T>
NetworkBitstream &operator >>(NetworkBitstream &in, T &out) {
	if (in.readPtr + sizeof(T) > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(&out, &in.data[in.readPtr], sizeof(T));
	in.readPtr += sizeof(T);

	return in;
}
*/

/*
 * Writes in one data type to the network stream
 *
 * @param out The network bitstream to write to
 * @param data The variable to store write to the network
 *
 * @returns The input network bitstream
 */
/*
template <typename T>
NetworkBitstream &operator <<(NetworkBitstream &out, const T &data) {
	if (out.writePtr + sizeof(T) > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	memcpy(&out.data[out.writePtr], &data, sizeof(T));
	out.writePtr += sizeof(T);
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}
*/

#endif	// _NETWORK__NETWORK_BITSTREAM_H_
