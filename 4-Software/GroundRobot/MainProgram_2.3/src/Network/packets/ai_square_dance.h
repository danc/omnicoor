#ifndef _NETWORK__PACKETS__AI_SQUARE_DANCE_H_
#define _NETWORK__PACKETS__AI_SQUARE_DANCE_H_

#include "base_packet.h"

class SquareDance : public BasePacket {
	private:
		char corner;

	public:
		/*
		 * Creates a manual motor control by default
		 */
		SquareDance();

		/*
		 * Creates a square dance packet
		 */
		SquareDance(char corner);

		/*
		 * Creates a copy of a MotorControl packet
		 *
		 * @param copy The packet to copy
		 */
		SquareDance(SquareDance *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~SquareDance();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the corner the robot is at
		 */
		char getCorner() const;

		/*
		 * Sets the corner the robot is at
		 */
		void setCorner(char newCorner);
};

#endif	// _NETWORK__PACKETS__AI_SQUARE_DANCE_H_
