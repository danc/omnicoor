#include "localization_packet.h"

#include <cstring>

#include "Network/network_bitstream.h"

/**
 * Creates a manual motor control by default
 */
LocalizationPacket::LocalizationPacket() : BasePacket(OPCODE_LOCALIZATION) {
	memset(robot_data,  0, sizeof(struct localization_robot_t ) * MAX_ROBOTS);
	memset(object_data, 0, sizeof(struct localization_object_t) * MAX_OBJECTS);

	num_robots = 0;
	num_objects = 0;
	timestamp = 0;
}

/*
 * Creates a copy of a LocalizationPacket packet
 *
 * @param copy The packet to copy
 */
LocalizationPacket::LocalizationPacket(LocalizationPacket *copy) : BasePacket(OPCODE_LOCALIZATION) {
	memcpy(robot_data,  copy->robot_data,  sizeof(struct localization_robot_t ) * MAX_ROBOTS);
	memcpy(object_data, copy->object_data, sizeof(struct localization_object_t) * MAX_OBJECTS);

	num_robots =     copy->num_robots;
	num_objects =    copy->num_objects;
	timestamp =      copy->timestamp;
}

/*
 * Frees any resources used by this packet
 */
LocalizationPacket::~LocalizationPacket() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *LocalizationPacket::read(NetworkBitstream &in) {
	int i;
	short x, y;

	// Hard-code the opcode
	opcode = OPCODE_LOCALIZATION;

#	ifdef NET_DEBUG
	printf("\n");
	for (i=0; i<in.getLength(); i++) {
		printf("%x ", in.getData()[i]);
	}
#	endif

	// Read in the packet size
	in>>size;

	//in>>processing_time;
	in>>num_robots;

	// The size of the packet (sizeof(size)) is not included here, as
	// the variable size already has that removed from it
	// num_objects = packet_size - (num_robots * sizeof(struct localization_robot_t)) - sizeof(num_robots)) / sizeof(struct localization_object_t)
	// We don't use sizeof(struct localization_robot_t) or sizeof(struct localization_object_t) because the structs are byte-padded to 8 bytes, which
	// is not good.)
	num_objects = (size - (num_robots * 7) - 1 - 2) / 7;
	if (num_objects < 0) {
		num_objects = 0;
	}

#	ifdef NET_DEBUG
	printf("%d %d %d\n", size, num_robots, num_objects);
#	endif
	for (i=0; i<num_robots; i++) {
		if (i >= 0 && i < MAX_ROBOTS) {
			in>>robot_data[i].robot_id;
			in>>x;
			in>>y;
			in>>robot_data[i].orientation;

			if (x == -1 || y == -1) {
				robot_data[i].x = (float)x;
				robot_data[i].y = (float)y;
			} else {
				robot_data[i].x = (float)x / 100.0f;
				robot_data[i].y = (float)y / 100.0f;
			}

#			ifdef NET_DEBUG
			printf("R: %d %f %f %d\n", robot_data[i].robot_id, robot_data[i].x, robot_data[i].y, robot_data[i].orientation);
#			endif
		} else {
			char c;
			// Just read 7 bytes
			in>>i;
			in>>c>>c>>c;
		}
	}

	if (num_robots > MAX_ROBOTS) {
		num_robots = MAX_ROBOTS;
	}

	if (num_objects > MAX_OBJECTS) {
		num_objects = MAX_OBJECTS;
	}

	for (i=0; i<num_objects; i++) {
		in>>object_data[i].object_id;
		in>>x;
		in>>y;
		in>>object_data[i].radius;

			if (x == -1 || y == -1) {
				object_data[i].x = (float)x;
				object_data[i].y = (float)y;
			} else {
				object_data[i].x = (float)x / 100.0f;
				object_data[i].y = (float)y / 100.0f;
			}

#		ifdef NET_DEBUG
		printf("O: %d %f %f %d\n", object_data[i].object_id, object_data[i].x, object_data[i].y, object_data[i].radius);
#		endif
	}

	// Read the timestamp of the packet in
	// NOTE: This will 'overflow' many times in the lifespan of the program
	in>>timestamp;

#	ifdef NET_DEBUG
	printf("%d\n", timestamp);
#	endif

	return new LocalizationPacket(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void LocalizationPacket::write(NetworkBitstream &out) const {
	BasePacket::write(out);
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the number of robots detected in the field
 *
 * @returns The number of robots in the field
 */
char LocalizationPacket::getNumRobots() {
	return num_robots;
}

/*
 * Gets a pointer to the robot data
 *
 * @returns An array of robots detected by the localization system
 */
const struct localization_robot_t *LocalizationPacket::getRobotData() {
	return robot_data;
}

/*
 * Gets the number of objects detected in the field
 *
 * @returns The number of objects in the field
 */
char LocalizationPacket::getNumObjects() {
	return num_objects;
}

/*
 * Gets a pointer to the object data
 *
 * @returns An array of objects detected by the localization system
 */
const struct localization_object_t *LocalizationPacket::getObjectData() {
	return object_data;
}

/*
 * Gets the time the packet was sent, in milliseconds
 *
 * @returns The timestamp of the packet
 */
unsigned short LocalizationPacket::getTimestamp() {
	return timestamp;
}
