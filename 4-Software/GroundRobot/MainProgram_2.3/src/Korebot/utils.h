
#ifndef _KOREBOT__UTILS_H_
#define _KOREBOT__UTILS_H_

double normalize(double num, int A, int B, int C, int D);
double RAD_TO_DEG(double angle);
char getch();

#endif  // _KOREBOT__UTILS_H_
