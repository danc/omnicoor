#ifndef _KOREBOT__MOTOR_CONTROLLER_COMMON_H_
#define _KOREBOT__MOTOR_CONTROLLER_COMMON_H_

const int MOTOR_CONFIG_NAME_LEN = 40;

/************************* Ground Robot *************************/
struct motor_control_speed_t {
    int heading;
    int linear_speed;
    int rotational_velocity;
};

struct motor_control_manual_t {
    int motor_speed_front;
    int motor_speed_left;
    int motor_speed_right;
    int motor_speed_rear;
    
};

struct motor_t
{
    int dev_desc;   // KNET device descriptor that represents the motor

    char name[MOTOR_CONFIG_NAME_LEN];   // The name of the motor (Used when using KNET)

    int pid_kk;
    int pid_kff;
    int pid_kp;
    int pid_kd;
    int pid_ka;
    int pid_ki;
    int pid_kih;
    int pid_kil;
    int pid_kf0;
    int pid_kf1;
    int pid_kf2;
    int pid_kf3;
    int pid_kdfil;
    int pid_driveplus;
    int pid_driveminus;
    int pid_accelf;
    int pid_accel;
    int pid_slewlimit;
    int pid_maxpwm;

    /*
    int Kp;
    int Ki;
    int Kd;
    */
    // int sample_time;         // The sample time to use with the motors; Only used in closed-loop mode
    int speed_multiplier;       // how much the desired speed must be multipled by to acheive this speed when commanding the motors
    // int current_limit;       // differential constant used in the internal PID controller of the motors
    int margin;                 // The motor control near target margin
    // int current_limit;       // maximum current that a motor can be given (0 = 0 amps, 512 = 2 amps)
    // int encoder_resolution;  // encoder ticks per centimeter traveled.
    int max_motor_speed;        // The maximum speed (Absolute value) of the motor
};

struct motortrain_t
{
    struct motor_t left;
    struct motor_t right;
    struct motor_t front;
    struct motor_t rear;
};

/************************* Pendulum (propellor system) *************************/
typedef struct
{
    double err;
    double preverr;
    double prevI;
    double prevD;
    double prevSatDiff;     // Previous difference to show saturation (used for anti-windup)
    double correction;
    double PID_pcomp;
    double PID_icomp;
    double PID_dcomp;
    double current;
    double desired;
    double desiredSet;
    double filtError;
    double filtErrorPre;
    double timeSum;
    // These are kp, ki, and kd. They are not named such because there are #defines in some of
    // the MESA board includes with those names.
    double p;
    double i;
    double d;
    double Taw;
    double filtGain;
    double errorGain;
} QUADPID;

typedef struct
{
    double  vrpnNow;        // units: seconds
    double  vrpnPrev;
    double  vrpnTime0;
    int     motor_1_pwm;
    int     motor_2_pwm;
    int     motor_3_pwm;
    int     motor_4_pwm;
    double  desired_pitch;
    double  desired_yaw;
    double  desired_roll;
    QUADPID pitch;
    QUADPID roll;
    QUADPID yaw;
    double controlTime;

    // TODO: move to logger.h
    int     marker;       // to keep track of markers
} QUAD;

typedef struct
{
	double vrpnNow;		    // units: seconds
	double vrpnPrev;
    double vrpnTime0;
	double usec;
    float  x;
    float  y;
    float  z;
    float  yaw;
    float  pitch;
    float  roll;
    int    x_vel_set;
    short motor_vel_front;
    long motor_pos_front;
    float motor_pwm_front;
    short motor_vel_left;
    long motor_pos_left;
    float motor_pwm_left;
    short motor_vel_right;
    long motor_pos_right;
    float motor_pwm_right;
    short motor_vel_rear;
    long motor_pos_rear;
    float motor_pwm_rear;
    int counter1;

} ERIS;
#endif  // _KOREBOT__MOTOR_CONTROLLER_COMMON_H_
