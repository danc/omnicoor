#include <string.h>
#include <libconfig.h>

#include "configuration.h"

// Default value for singleton pointer
Configuration *Configuration::s_instance = 0;

/*
 * Creates a default configuration object with no
 * loaded files
 */
Configuration::Configuration() {
	config_init(&config);
}

/*
 * Frees any memory used by this object
 */
Configuration::~Configuration() {
	config_destroy(&config);
}

/*
 * Loads the configuration from the given file
 *
 * @param filename The path to the configuration file to load
 *
 * @returns 1 on success, 0 otherwise
 */
int Configuration::load(const char *filename) {
	int tmp = (config_read_file(&config, filename) == CONFIG_TRUE);
	if(!tmp) {
		printf("Function - Configuration::load\n\tError: %s\n\n", config_error_text(&config));
	}
		//printf("Configuration::load error txt = %s\n\n", config_error_text(&config));
	return (tmp == CONFIG_TRUE);
}


/*
 * Gets the value at path from the config file
 *
 * @param path The path to the value in the config file
 * @param out  The output variable
 *
 * @returns 1 on success, 0 otherwise
 */
int Configuration::getValue(const char *path, int def) {
	int out;
	if ((out = config_lookup_int(&config, path)) == 0) {
		out = def;
	}
	return out;
}

/*
 * Gets the value at path from the config file
 *
 * @param path The path to the value in the config file
 * @param def The default value
 *
 * @returns The value read from the config file, if found, or def if not found
 */
long Configuration::getValue(const char *path, long def) {
	long out;
	if ((out = config_lookup_int(&config, path)) == 0) {
		out = def;
	}
	return out;
}

/*
 * Gets the value at path from the config file
 *
 * @param path The path to the value in the config file
 * @param def The default value
 *
 * @returns The value read from the config file, if found, or def if not found
 */
float Configuration::getValue(const char *path, float def) {
	float out;
	if ((out = config_lookup_float(&config, path)) == 0) {
		out = def;
	}
	return out;
}

/*
 * Gets the value at path from the config file
 *
 * @param path The path to the value in the config file
 * @param out  The output variable
 * @param max_length The size of the array out
 * @param def The default value
 *
 * @returns 1
 */
int Configuration::getValue(const char *path, char *out, int max_length, const char *def) {
	const char *result = config_lookup_string(&config, path);

	if (result == NULL) {
		strncpy(out, def, max_length);
		return 1;
	}

	strncpy(out, result, max_length);

	return 1;
}

/*
 * Gets the size of an array of values in the config file
 *
 * @param path The path to the array
 */
int Configuration::getListSize(const char *path) {
	config_setting_t *setting = config_lookup(&config, path);
	if (setting == NULL) {
		return 0;
	} else {
		return config_setting_length(setting);
	}
}
