#ifndef _COMMON_DEFINES_H
#define _COMMON_DEFINES_H

#define MAX_NETWORK_PACKET_SIZE	1024

#define MAX_ROBOT_NAME_LENGTH	16

#define KOREBOT_ID_BROADCAST	0xFF

// Defines used in the motor control struct
#define MOTOR_CONTROL_MODE_RELEASE		0	 // (Default)
#define MOTOR_CONTROL_MODE_FREE			1
#define MOTOR_CONTROL_MODE_MANUAL		2
#define MOTOR_CONTROL_MODE_SPEED		3
#define MOTOR_CONTROL_COMMAND_STOP		4

// Defines used in the configuration packet
#define CONFIGURATION_OPTION_INVALID			0
#define CONFIGURATION_OPTION_REQUEST_CONFIG		1
#define CONFIGURATION_OPTION_PROFILER			2
#define CONFIGURATION_OPTION_AI_ENABLE			3
#define CONFIGURATION_OPTION_MOTOR_BOUNDS_LIMIT	4
#define CONFIGURATION_OPTION_SENSOR_LOGGING		5

// Defines used in sensor control
#define SENSOR_CONTROL_COMMAND_INVALID				0
#define SENSOR_CONTROL_SENSOR_LIST_REQUEST			1
#define SENSOR_CONTROL_SENSOR_LIST_DATA				2
#define SENSOR_CONTROL_SENSOR_REQUEST				3
#define SENSOR_CONTROL_SENSOR_DATA					4

// Nework OPCodes
#define OPCODE_INVALID					0
#define OPCODE_ROBOT_IDENTIFICATION		1
#define OPCODE_SENSOR					2
#define OPCODE_MOTOR					3
#define OPCODE_LOCALIZATION				4
#define OPCODE_PROFILER					5
#define OPCODE_CONFIGURATION			6
#define OPCODE_ROBOT_DISCOVERY			7
#define OPCODE_AI_LIST					8
#define OPCODE_AI_SQUARE_DANCE			9
#define OPCODE_XBOX_SENSOR				10
#define OPCODE_PEER_MOTOR_SPEED				11

#define MOTOR_FRONT						0
#define MOTOR_REAR						1
#define MOTOR_LEFT						2
#define MOTOR_RIGHT						3

// Sensor IDs
// The following sensors are special and refer to non-KoreIO sensors (motor, localization)
// NOTE: The order of the following special sensors must not be changed!
/*
#define SENSOR_ID_FRONT_MOTOR_SPEED		-1
#define SENSOR_ID_FRONT_MOTOR_POSITION	-2
#define SENSOR_ID_FRONT_MOTOR_CURRENT	-3
#define SENSOR_ID_REAR_MOTOR_SPEED		-4
#define SENSOR_ID_REAR_MOTOR_POSITION	-5
#define SENSOR_ID_REAR_MOTOR_CURRENT	-6
#define SENSOR_ID_LEFT_MOTOR_SPEED		-7
#define SENSOR_ID_LEFT_MOTOR_POSITION	-8
#define SENSOR_ID_LEFT_MOTOR_CURRENT	-9
#define SENSOR_ID_RIGHT_MOTOR_SPEED		-10
#define SENSOR_ID_RIGHT_MOTOR_POSITION	-11
#define SENSOR_ID_RIGHT_MOTOR_CURRENT	-12
*/
// I hope I did this right...
#define SENSOR_ID_FRONT_MOTOR_SPEED		-1
#define SENSOR_ID_FRONT_MOTOR_POSITION	-2
#define SENSOR_ID_FRONT_MOTOR_CURRENT	-3
#define SENSOR_ID_FRONT_MOTOR_PWM		-4
#define SENSOR_ID_REAR_MOTOR_SPEED		-5
#define SENSOR_ID_REAR_MOTOR_POSITION	-6
#define SENSOR_ID_REAR_MOTOR_CURRENT	-7
#define SENSOR_ID_REAR_MOTOR_PWM		-8
#define SENSOR_ID_LEFT_MOTOR_SPEED		-9
#define SENSOR_ID_LEFT_MOTOR_POSITION	-10
#define SENSOR_ID_LEFT_MOTOR_CURRENT	-11
#define SENSOR_ID_LEFT_MOTOR_PWM		-12
#define SENSOR_ID_RIGHT_MOTOR_SPEED		-13
#define SENSOR_ID_RIGHT_MOTOR_POSITION	-14
#define SENSOR_ID_RIGHT_MOTOR_CURRENT	-15
#define SENSOR_ID_RIGHT_MOTOR_PWM		-16





// If this is changed, then the extra sensors must be added in line in the previous list
//#define NUM_SENSORS_PER_MOTOR			3
#define NUM_SENSORS_PER_MOTOR			4

#define NUM_SPECIAL_SENSORS 16

// The number of sensors to keep in history
// NOTE: This must be at least 3 for the localization system tracking to work properly
#define SENSOR_HISTORY					3

/*


Special sensors on the robots / A-D inputs

The Korebots use a PIC18 uC with (presumably) 10-bit A/D channels
Eris has a 16 channel, 16-bit ISA I/O board

The Korebots were supposed to have a 2-axis accelerometer (some may have a 3-axis unit installed)
and a single-axis gyro.

Kryten might have an accelerometer, but there's no gyro on board.

Eris was originally equipped with a Pololu CHR-6d magic box/IMU
that generated all sorts of data over RS-232. It was unceremoniously murdered
during testing and CH Products has since discontinued the part. Some of Dalek's
sensors will be integrated through the I/O board instead. (I think they're from
Dalek, it's hard to tell anymore...)

Since both robots have 16 channels of analog input and will be using the same
sensors, treating them differently isn't important anymore.

#ifdef KOREBOT
	#define SENSOR_ID_ACCELERATION_X		1
	#define SENSOR_ID_ACCELERATION_Y		2
	#define SENSOR_ID_GYROSCOPE				3
#else
	#define SENSOR_ID_ACCELERATION_Y 		2
	#define SENSOR_ID_ACCELERATION_Z 		3
	#define SENSOR_ID_ACCELERATION_X 		4
	#define SENSOR_ID_GYRO_Y 				5
	#define SENSOR_ID_GYRO_Z 				6
	#define SENSOR_ID_GYRO_X 				7
	#define SENSOR_ID_PITCH  				8
	#define SENSOR_ID_ROLL 					9
#endif */

#define SENSOR_ID_ACCELERATION_X		1
#define SENSOR_ID_ACCELERATION_Y		2
#define SENSOR_ID_GYROSCOPE				3


#endif
