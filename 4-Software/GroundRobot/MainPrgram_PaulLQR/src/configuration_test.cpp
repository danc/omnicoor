#include <stdio.h>
#include "configuration.h"

int main() {
	Configuration *config;
	int i;
	long l;
	float f;
	char c[1024];

	config = Configuration::instance();

	if (!config->load("test.cfg")) {
		printf("Error: Could not load config file.\n");
		return -1;
	}

	i = 0;
	l = 0;
	f = 0.0f;

	i = Configuration::instance()->getValue("int", (int)0);
	l = Configuration::instance()->getValue("long", (long)0);
	f = Configuration::instance()->getValue("float", (float)0);
	Configuration::instance()->getValue("string", c, 1024, "Default");

	printf("Testing config loading top level...\n");
	printf("Expected:\t20\t67100200\t4.123456\tI am a string\n");
	printf("Actual:\t\t%d\t%ld\t%f\t%s\n", i, l, f, c);
	printf("\n");

	i = Configuration::instance()->getValue("nested.int", (int)0);
	l = Configuration::instance()->getValue("nested.long", (long)0);
	f = Configuration::instance()->getValue("nested.float", (float)0);
	Configuration::instance()->getValue("nested.string", c, 1024, "Default2");

	printf("Testing config loading nested level...\n");
	printf("Expected:\t100\t200\t3.141590\tI am another string\n");
	printf("Actual:\t\t%d\t%ld\t%f\t%s\n", i, l, f, c);

	i = Configuration::instance()->getListSize("ary");
	l = Configuration::instance()->getValue("ary.[0].value", (long)0);
	Configuration::instance()->getValue("ary.[0].name", c, 1024, "Default3");

	printf("Testing config loading lists...\n");
	printf("Expected:\t2\t103\tElement 1\n");
	printf("Actual:\t\t%d\t%ld\t%s\n", i, l, c);

	l = Configuration::instance()->getValue("ary.[1].value", (long)0);
	Configuration::instance()->getValue("ary.[1].name", c, 1024, "Default4");

	printf("\n");
	printf("Expected:\t2\t4\tElement 2\n");
	printf("Actual:\t\t%d\t%ld\t%s\n", i, l, c);

	return 0;
}
