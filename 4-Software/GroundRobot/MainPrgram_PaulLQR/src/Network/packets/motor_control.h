#ifndef _COMMON_PACKETS_MOTOR_CONTROL_H
#define _COMMON_PACKETS_MOTOR_CONTROL_H

#include "common.h"
#include "base_packet.h"

class MotorControl : public BasePacket {
	private:
		char command;

	public:
		/*
		 * Creates a manual motor control by default
		 */
		MotorControl();

		/*
		 * Creates a manual motor control by default
		 */
		MotorControl(char command);

		/*
		 * Creates a copy of a MotorControl packet
		 *
		 * @param copy The packet to copy
		 */
		MotorControl(MotorControl *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~MotorControl();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the command of the motor control
		 */
		char getCommand() const;

		/*
		 * Sets the command of motor control
		 */
		void setCommand(char newCommand);
};

#endif
