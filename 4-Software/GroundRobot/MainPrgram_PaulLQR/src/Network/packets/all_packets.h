#ifndef _COMMON_PACKETS_ALL_PACKETS_H
#define _COMMON_PACKETS_ALL_PACKETS_H

#include "common.h"
#include "base_packet.h"
#include "robot_identification.h"
#include "robot_discovery.h"
#include "motor_control.h"
#include "motor_control_manual.h"
#include "motor_control_speed.h"
#include "configuration_packet.h"
#include "profiler_packet.h"
#include "sensor_control.h"
#include "sensor_list.h"
#include "sensor_request.h"
#include "sensor_data.h"
#include "localization_packet.h"
#include "ai_list.h"
#include "ai_square_dance.h"
#include "xbox_sensor.h"
#include "peer_motorspeed.h"

#endif
