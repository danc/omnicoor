#include <string.h>
#include <stdio.h>
#include "sensor_data.h"

/*
 * Creates a sensor list packet with no sensors
 */
SensorData::SensorData() : SensorControl(SENSOR_CONTROL_SENSOR_DATA) {
	num_sensors = 0;
}

/*
 * Creates a copy of a SensorData packet
 *
 * @param copy The packet to copy
 */
SensorData::SensorData(SensorData *copy) : SensorControl(SENSOR_CONTROL_SENSOR_DATA) {
	this->num_sensors = copy->num_sensors;
	memcpy(this->sensor_data, copy->sensor_data, sizeof(struct data_t) * MAX_SENSORS_IN_PACKET);
}

/*
 * Frees any resources used by this packet
 */
SensorData::~SensorData() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *SensorData::read(NetworkBitstream &in) {
	unsigned char i;

	in>>num_sensors;

	for (i=0; i<num_sensors; i++) {
		in>>sensor_data[i].id;
		in>>sensor_data[i].timestamp;
		in>>sensor_data[i].value;
	}

	return new SensorData(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void SensorData::write(NetworkBitstream &out) const {
	unsigned char i;
	SensorControl::write(out);
	out<<num_sensors;

	for (i=0; i<num_sensors; i++) {
		out<<sensor_data[i].id;
		out<<sensor_data[i].timestamp;
		out<<sensor_data[i].value;
	}
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Adds the given sensor data to the end of this packet,
 * for sending over the network
 *
 * @param id		 The ID of the sensor
 * @param timestamp	 The timestamp of the sensor
 * @param value		 The value of the sensor
 *
 * @returns 1 on success, 0 otherwise
 */
int SensorData::addSensorValue(int id, float timestamp, long value) {
	if (num_sensors == MAX_SENSORS_IN_PACKET) {
		return 0;
	}
	
	sensor_data[num_sensors].id        = id;
	sensor_data[num_sensors].timestamp = timestamp;
	sensor_data[num_sensors].value     = value;

	num_sensors++;

	return 1;
}


/*
 * Gets the number of sensors in this data packet
 *
 * @returns The number of sensors in the packet
 */
unsigned char SensorData::getNumSensors() const {
	return num_sensors;
}

/*
 * Gets the ID of the sensor associated with the given sensor
 * index
 *
 * @param index The index of the sensor to get
 */
int SensorData::getSensorID(int index) const {
	if (index < num_sensors && index >= 0) {
		return sensor_data[index].id;
	}
	return 0;
}

/*
 * Gets the timestamp of the given sensor index
 *
 * @param index The index of the sensor with the information
 */
float SensorData::getSensorTimestamp(int index) const {
	if (index < num_sensors && index >= 0) {
		return sensor_data[index].timestamp;
	}
	return 0;
}

/*
 * Gets the value of the given sensor
 *
 * @param index The index of the sensor with the information
 */
long SensorData::getSensorValue(int index) const {
	if (index < num_sensors && index >= 0) {
		return sensor_data[index].value;
	}
	return 0;
}
