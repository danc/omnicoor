#ifndef _NETWORK_PACKETS_SENSOR_REQUEST_H
#define _NETWORK_PACKETS_SENSOR_REQUEST_H

#include "common.h"
#include "sensor_control.h"

class SensorRequest : public SensorControl {
	private:
		short sensor_id;
		unsigned char send_sensor_data;

	public:
		/*
		 * Creates a sensor request packet
		 */
		SensorRequest();

		/*
		 * Creates a sensor data request packet
		 *
		 * @param sensor_id The ID of the sensor data to send / stop sending
		 * @param send_sensor_data 1 if the korebot should send sensor data, 0 otherwise
		 */
		SensorRequest(short sensor_id, unsigned char send_sensor_data);

		/*
		 * Creates a copy of a SensorRequest packet
		 *
		 * @param copy The packet to copy
		 */
		SensorRequest(SensorRequest *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~SensorRequest();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the ID of the sensor we are requesting data for
		 *
		 * @returns The ID of the sensor this packet represents
		 */
		short getSensorID();

		/*
		 * Gets the number of available sensors.
		 *
		 * @returns 1 if the korebot should start sending sensor data, 0 otherwise
		 */
		unsigned char getShouldSendSensorData();
};

#endif
