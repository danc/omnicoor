#ifndef _COMMON_PACKETS_PEER_MOTOR_SPEED_H
#define _COMMON_PACKETS_PEER_MOTOR_SPEED_H

#include "common.h"
#include "base_packet.h"



class PeerMotorSpeed : public BasePacket {

	private:
		int id; // robot id
		float total_time;

		struct motor_speeds_t {
			long front;
			long rear;
			long left;
			long right;
		};
		

		
		struct motor_speeds_t motor_speeds;
	public:


		/*
		 * Creates a PeerMotorSpeed packet by default
		 */
		PeerMotorSpeed();

		/*
		 * Creates a PeerMotorSpeed packet with robot id 
		 */
		PeerMotorSpeed(int id);

		/*
		 * Creates a PeerMotorSpeed packet with robot id and motor speeds
		 */
		PeerMotorSpeed(int id, motor_speeds_t motor_speeds);

		/*
		 * Creates a copy of a PeerMotorSpeed packet
		 *
		 * @param copy The packet to copy
		 */
		PeerMotorSpeed(PeerMotorSpeed *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~PeerMotorSpeed();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the motor speeds
		 */
		float getmotorspeeds(struct motor_speeds_t *empty_motor_speeds) const;

		/*
		 * Gets the robot id
		 */
		int getrobotid() const;

		/*
		 * Sets the corner the robot is at
		 */
		void setmotorspeeds(int new_id, struct motor_speeds_t new_motor_speeds, float new_total_time);


};

#endif
