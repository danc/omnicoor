#ifndef _NETWORK_PACKETS_PROFILER_PACKET_H
#define _NETWORK_PACKETS_PROFILER_PACKET_H

#include "common.h"
#include "base_packet.h"
#include "profiler.h"
#include "../tcp_connection.h"

struct profiler_information_t {
	float fMin;
	float fAvg;
	float fMax;
	float fTime;
	unsigned char callCount;
	char name[PROFILER_MAX_NAME_LEN];
	unsigned char parentCount;
};

class ProfilerPacket : public BasePacket, public ProfilerOutputHandler {
	private:
		// The connection to send this packet out on after the profiler has
		// sent all the samples for this iteration of the loop
		TCP_Connection *connection;

		// The number of active samples
		unsigned char num_samples;

		// The loop time
		float loop_time;

		// The data from the profiler
		struct profiler_information_t profile_data[MAX_PROFILER_SAMPLES];

	public:
		/*
		 * Creates a profiler with to profile data
		 */
		ProfilerPacket();

		/*
		 * Creates a copy of the given robot identification
		 *
		 * @param copy The packet to copy
		 */
		ProfilerPacket(ProfilerPacket *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~ProfilerPacket();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/*
		 * Called before the profiler starts sending profile data.  Can
		 * be used to initialize / print headers for the printed data
		 *
		 * @param fTotal The total time of the profile loop
		 */
		void beginOutput(float tTotal);

		/*
		 * Called for each sample that was recorded in the profiler.  Contains
		 * data about a section of code that was profiled.
		 *
		 * @param fMin The minimum amount of time this sample took to run
		 * @param fAvg The average amount of time this sample took to run
		 * @param fMax The maximum amount of time this sample took to run
		 * @param fTime The time it took this sample to run
		 * @param callCount The number of times this sample was called
		 * @param name The name of the sample
		 * @param parentCount The number of parent samples before this sample
		 */
		void sample(float fMin, float fAvg, float fMax, float fTime, unsigned char callCount, char *name, unsigned char parentCount);

		/*
		 * Called when all samples have been sent, and the profiler is done
		 * sending data to be outputted.
		 */
		void endOutput();


		/****************************************************************
		 * 				GETTERS / SETTERS								*
		 ***************************************************************/

		/*
		 * Gets the connection that the profiler transmits profile information on.
		 */
		TCP_Connection *getConnection() const;

		/*
		 * Sets the connection that the profiler will transmit data on.
		 *
		 * @param connection The connection to send the profiler info on
		 */
		void setConnection(TCP_Connection *connection);

		/*
		 * Returns the number of samples in the data
		 */
		unsigned char getNumSamples() const;

		/*
		 * Gets the amount of time it took in the root-level sample (The
		 * loop time)
		 */
		float getLoopTime() const;

		/*
		 * Copies the data for the request sample into the output structure.
		 *
		 * @param sampleNum The sample to get
		 * @param out The data for the sample will be written to this variable
		 *
		 * @returns 1 on success, or 0 on failure (sampleNum was not in range)
		 */
		int getSample(unsigned char sampleNum, struct profiler_information_t &out) const;
};

#endif
