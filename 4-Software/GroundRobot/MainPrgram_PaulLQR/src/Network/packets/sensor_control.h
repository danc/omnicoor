#ifndef _COMMON_PACKETS_SENSOR_CONTROL_H
#define _COMMON_PACKETS_SENSOR_CONTROL_H

#include "common.h"
#include "base_packet.h"

class SensorControl : public BasePacket {
	private:
		char command;

	public:
		/*
		 * Creates a manual sensor control by default
		 */
		SensorControl();

		/*
		 * Creates a manual sensor control by default
		 */
		SensorControl(char command);

		/*
		 * Creates a copy of a SensorControl packet
		 *
		 * @param copy The packet to copy
		 */
		SensorControl(SensorControl *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~SensorControl();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the command of the sensor control
		 */
		char getCommand() const;

		/*
		 * Sets the command of sensor control
		 */
		void setCommand(char newCommand);
};

#endif

