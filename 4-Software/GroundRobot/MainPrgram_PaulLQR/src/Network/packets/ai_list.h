#ifndef _COMMON_PACKETS_AI_LIST_H
#define _COMMON_PACKETS_AI_LIST_H

#include "common.h"
#include "base_packet.h"

#define MAX_AI_NAME_LENGTH	64
#define MAX_NUM_AI			32

class AIList : public BasePacket {
	private:
		struct ai_entry_t {
			char name[MAX_AI_NAME_LENGTH];
			int id;
		};
		
		ai_entry_t ai[MAX_NUM_AI];

		int num_ai;

	public:
		/*
		 * Creates an ai list
		 */
		AIList();

		/*
		 * Creates a copy of a AIList packet
		 *
		 * @param copy The packet to copy
		 */
		AIList(AIList *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~AIList();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Adds an entry with the details of the AI for this system
		 *
		 * @param ID The ID of the AI
		 * @param name The name of the AI
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int addAI(const int ID, const char *name);

		/*
		 * Gets the number of AI in the packet
		 *
		 * @returns The number of AI in the packet
		 */
		int getNumAI();

		/*
		 * Gets the ID of the AI at the given index into the packet
		 *
		 * @param index The index of the AI to get
		 *
		 * @returns The ID of the AI at the given index, or -1 if the index
		 * 				is not valid
		 */
		int getID(int index);

		/*
		 * Gets the name of the AI at the given index into the packet
		 *
		 * @param index The index of the AI to get
		 * 
		 * @returns The name of the AI at that index, or NULL if the index
		 * 				is not valid
		 */
		const char *getName(int index);
};

#endif

