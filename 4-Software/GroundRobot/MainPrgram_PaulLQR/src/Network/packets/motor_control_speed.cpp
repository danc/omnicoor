#include <string.h>
#include "motor_control_speed.h"

/*
 * Creates a speed motor control by default
 */
MotorControlSpeed::MotorControlSpeed() : MotorControl(MOTOR_CONTROL_MODE_SPEED) {
	memset(&parameters, 0, sizeof(motor_control_speed_t));
}

/*
 * Creates a speed motor control by default
 */
MotorControlSpeed::MotorControlSpeed(const struct motor_control_speed_t &parameters) : MotorControl(MOTOR_CONTROL_MODE_SPEED) {
	memcpy(&this->parameters, &parameters, sizeof(motor_control_speed_t));
}

/*
 * Creates a copy of a MotorControlSpeed packet
 *
 * @param copy The packet to copy
 */
MotorControlSpeed::MotorControlSpeed(MotorControlSpeed *copy) : MotorControl(MOTOR_CONTROL_MODE_SPEED) {
	memcpy(&parameters, &copy->parameters, sizeof(motor_control_speed_t));
}

/*
 * Frees any resources used by this packet
 */
MotorControlSpeed::~MotorControlSpeed() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 * 			the information read from the bitstream.
 * 			The base packet should be freed by the
 * 			calling program.
 */
BasePacket *MotorControlSpeed::read(NetworkBitstream &in) {
	in>>parameters.heading;
	in>>parameters.linear_speed;
	in>>parameters.rotational_velocity;
	return new MotorControlSpeed(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void MotorControlSpeed::write(NetworkBitstream &out) const {
	MotorControl::write(out);
	out<<parameters.heading;
	out<<parameters.linear_speed;
	out<<parameters.rotational_velocity;
}

/*
 * Gets the speeds of the motors
 */
const motor_control_speed_t *MotorControlSpeed::getMotorSpeeds() const {
	return &parameters;
}

