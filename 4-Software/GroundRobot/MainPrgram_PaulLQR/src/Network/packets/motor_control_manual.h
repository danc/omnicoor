#ifndef _COMMON_PACKETS_MOTOR_CONTROL_MANUAL_H
#define _COMMON_PACKETS_MOTOR_CONTROL_MANUAL_H

#include "common.h"
#include "base_packet.h"
#include "motor_control.h"
#include "Korebot/motor_controller_common.h"

class MotorControlManual : public MotorControl {
	private:
		struct motor_control_manual_t parameters;

	public:
		/*
		 * Creates a manual motor control by default
		 */
		MotorControlManual();

		/*
		 * Creates a manual motor control by default
		 */
		MotorControlManual(const struct motor_control_manual_t &parameters);

		/*
		 * Creates a copy of a MotorControl packet
		 *
		 * @param copy The packet to copy
		 */
		MotorControlManual(MotorControlManual *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~MotorControlManual();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the speeds of the motors
		 */
		const motor_control_manual_t *getMotorSpeeds() const;
};

#endif

