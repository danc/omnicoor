#include <string.h>
#include <stdio.h>
#include "sensor_request.h"

/*
 * Creates a sensor request packet
 */
SensorRequest::SensorRequest() : SensorControl(SENSOR_CONTROL_SENSOR_REQUEST) {
	this->sensor_id = 0;
	this->send_sensor_data = 0;
}

/*
 * Creates a sensor data request packet
 *
 * @param sensor_id The ID of the sensor data to send / stop sending
 * @param send_sensor_data 1 if the korebot should send sensor data, 0 otherwise
 */
SensorRequest::SensorRequest(short sensor_id, unsigned char send_sensor_data) : SensorControl(SENSOR_CONTROL_SENSOR_REQUEST) {
	this->sensor_id = sensor_id;
	this->send_sensor_data = send_sensor_data;
}

/*
 * Creates a copy of a SensorRequest packet
 *
 * @param copy The packet to copy
 */
SensorRequest::SensorRequest(SensorRequest *copy) : SensorControl(SENSOR_CONTROL_SENSOR_REQUEST) {
	this->sensor_id = copy->sensor_id;
	this->send_sensor_data = copy->send_sensor_data;
}

/*
 * Frees any resources used by this packet
 */
SensorRequest::~SensorRequest() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *SensorRequest::read(NetworkBitstream &in) {

	in>>sensor_id;
	in>>send_sensor_data;

	return new SensorRequest(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void SensorRequest::write(NetworkBitstream &out) const {
	SensorControl::write(out);

	out<<sensor_id;
	out<<send_sensor_data;
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the ID of the sensor we are requesting data for
 *
 * @returns The ID of the sensor this packet represents
 */
short SensorRequest::getSensorID() {
	return sensor_id;
}

/*
 * Gets the number of available sensors.
 *
 * @returns 1 if the korebot should start sending sensor data, 0 otherwise
 */
unsigned char SensorRequest::getShouldSendSensorData() {
	return send_sensor_data;
}
