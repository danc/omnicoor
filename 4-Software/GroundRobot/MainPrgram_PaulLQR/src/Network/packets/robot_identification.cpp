#include <string.h>
#include "robot_identification.h"

/*
 * Creates a manual motor control by default
 */
RobotIdentification::RobotIdentification() : BasePacket(OPCODE_ROBOT_IDENTIFICATION) {
	this->robot_id = -1;
	strcpy(this->robot_name, "");
}

/*
 * Creates a manual motor control by default
 */
RobotIdentification::RobotIdentification(unsigned char robot_id, const char *robot_name) : BasePacket(OPCODE_ROBOT_IDENTIFICATION) {
	this->robot_id = robot_id;
	strncpy(this->robot_name, robot_name, MAX_ROBOT_NAME_LEN);
}

/*
 * Creates a copy of the given robot identification
 *
 * @param copy The packet to copy
 */
RobotIdentification::RobotIdentification(RobotIdentification *copy) : BasePacket(OPCODE_ROBOT_IDENTIFICATION) {
	this->robot_id = copy->robot_id;
	strncpy(this->robot_name, copy->robot_name, MAX_ROBOT_NAME_LEN);
}

/*
 * Frees any resources used by this packet
 */
RobotIdentification::~RobotIdentification() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 */
BasePacket *RobotIdentification::read(NetworkBitstream &in) {
	in>>robot_id;
	in>>robot_name;
	return new RobotIdentification(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void RobotIdentification::write(NetworkBitstream &out) const {
	BasePacket::write(out);
	out<<robot_id;
	out<<((const char *)robot_name);
}

/**********************************************
 * 			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the robot ID
 */
unsigned char RobotIdentification::getRobotID() const {
	return robot_id;
}

/*
 * Sets the robot ID
 */
void RobotIdentification::setRobotID(unsigned char newRobotID) {
	robot_id = newRobotID;
}


/*
 * Gets the name of the robot
 */
const char *RobotIdentification::getRobotName() const {
	return robot_name;
}

/*
 * Sets the name of the robot
 * 
 * @param newRobotName The new name of the robot
 */
void RobotIdentification::setRobotName(const char *newRobotName) {
	strncpy(this->robot_name, newRobotName, MAX_ROBOT_NAME_LEN);
}
