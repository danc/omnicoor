#ifndef _INCLUDE_NETWORK_PACKETS_BASE_PACKET_H
#define _INCLUDE_NETWORK_PACKETS_BASE_PACKET_H

#include "common.h"
#include "Network/network_bitstream.h"

class BasePacket {
	protected:
		// The opcode of the packet
		unsigned char opcode;

		// The size of the data in this packet
		unsigned short size;

		/*
		 * Initializes the base packet with the given OPCode
		 *
		 * @param opcode The opcode of the packet
		 */
		BasePacket(unsigned char opcode);
	public:
		/*
		 * Initializes the opcode to 0 (Invalid)
		 * and the size to 2 (1 byte opcode, 1 byte size)
		 */
		BasePacket();

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~BasePacket();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/*
		 * Finalizes a packet before it is sent out on the network;
		 * Pretty much just updates the packet information with the
		 * correct size of the data.
		 *
		 * @param out The bitstream to write to
		 */
		void finalize(NetworkBitstream &out);

		/*
		 * Gets the opcode for the packet
		 *
		 * @returns The opcode associated with the packet
		 */
		unsigned char getOpcode();

		/*
		 * Sets the opcode for this packet
		 *
		 * @param newOpcode The new opcode for the packet
		 */
		void setOpcode(unsigned char newOpcode);

		/*
		 * Gets the size of this packet
		 *
		 * @returns the size, in bytes, of the packet
		 */
		unsigned char getSize();
};

#endif
