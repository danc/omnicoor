#ifndef _NETWORK_PACKETS_SENSOR_DATA_H
#define _NETWORK_PACKETS_SENSOR_DATA_H

#include "common.h"
#include "sensor_control.h"
#include "sensor_list.h"

class SensorData : public SensorControl {
	private:
		struct data_t {
			int   id;
			float timestamp;
			long   value;
		};

		unsigned char num_sensors;

		struct data_t sensor_data[MAX_SENSORS_IN_PACKET];

	public:
		/*
		 * Creates a sensor data packet
		 */
		SensorData();

		/*
		 * Creates a copy of a SensorData packet
		 *
		 * @param copy The packet to copy
		 */
		SensorData(SensorData *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~SensorData();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Adds the given sensor data to the end of this packet,
		 * for sending over the network
		 *
		 * @param id		 The ID of the sensor
		 * @param timestamp	 The timestamp of the sensor
		 * @param value		 The value of the sensor
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int addSensorValue(int id, float timestamp, long value);


		/*
		 * Gets the number of sensors in this data packet
		 *
		 * @returns The number of sensors in the packet
		 */
		unsigned char getNumSensors() const;

		/*
		 * Gets the ID of the sensor associated with the given sensor
		 * index
		 *
		 * @param index The index of the sensor to get
		 */
		int getSensorID(int index) const;

		/*
		 * Gets the timestamp of the given sensor index
		 *
		 * @param index The index of the sensor with the information
		 */
		float getSensorTimestamp(int index) const;

		/*
		 * Gets the value of the given sensor
		 *
		 * @param index The index of the sensor with the information
		 */
		long getSensorValue(int index) const;
};

#endif
