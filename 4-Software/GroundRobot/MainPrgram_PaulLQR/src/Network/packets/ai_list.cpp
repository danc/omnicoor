#include <string.h>
#include "ai_list.h"

/*
 * Creates an ai list
 */
AIList::AIList() : BasePacket(OPCODE_AI_LIST) {
	memset(ai, 0, sizeof(ai_entry_t) * MAX_NUM_AI);
	num_ai = 0;
}

/*
 * Creates a copy of a AIList packet
 *
 * @param copy The packet to copy
 */
AIList::AIList(AIList *copy) : BasePacket(OPCODE_AI_LIST) {
	memcpy(ai, copy->ai, sizeof(ai_entry_t) * MAX_NUM_AI);
	num_ai = copy->num_ai;
}

/*
 * Frees any resources used by this packet
 */
AIList::~AIList() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 * 			the information read from the bitstream.
 * 			The base packet should be freed by the
 * 			calling program.
 */
BasePacket *AIList::read(NetworkBitstream &in) {
	in>>num_ai;

	for (int i=0; i<num_ai; i++) {
		in>>ai[i].id;
		in>>ai[i].name;
	}

	return new AIList(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void AIList::write(NetworkBitstream &out) const {
	BasePacket::write(out);

	out<<num_ai;

	for (int i=0; i<num_ai; i++) {
		out<<ai[i].id;
		out<<ai[i].name;
	}
}

/*
 * Adds an entry with the details of the AI for this system
 *
 * @param ID The ID of the AI
 * @param name The name of the AI
 *
 * @returns 1 on success, 0 otherwise
 */
int AIList::addAI(const int id, const char *name) {
	if (num_ai >= MAX_NUM_AI) {
		return 0;
	}

	ai[num_ai].id = id;
	strncpy(ai[num_ai].name, name, MAX_AI_NAME_LENGTH);

	num_ai++;
	return 1;
}

/*
 * Gets the number of AI in the packet
 *
 * @returns The number of AI in the packet
 */
int AIList::getNumAI() {
	return num_ai;
}

/*
 * Gets the ID of the AI at the given index into the packet
 *
 * @param index The index of the AI to get
 *
 * @returns The ID of the AI at the given index, or -1 if the index
 * 				is not valid
 */
int AIList::getID(int index) {
	if (index < 0 || index >= num_ai) {
		return -1;
	}
	return ai[index].id;
}

/*
 * Gets the name of the AI at the given index into the packet
 *
 * @param index The index of the AI to get
 * 
 * @returns The name of the AI at that index, or NULL if the index
 * 				is not valid
 */
const char *AIList::getName(int index) {
	if (index < 0 || index >= num_ai) {
		return NULL;
	}
	return ai[index].name;
}
