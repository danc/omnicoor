#ifndef _COMMON_PACKETS_LOCALIZATION_PACKET_H
#define _COMMON_PACKETS_LOCALIZATION_PACKET_H

#include "common.h"
#include "base_packet.h"
#include "Korebot/localization_system_common.h"

class LocalizationPacket : public BasePacket {
	private:
		struct localization_robot_t robot_data[MAX_ROBOTS];
		struct localization_object_t object_data[MAX_OBJECTS];

		char num_robots;
		char num_objects;

		unsigned short timestamp;

	public:
		/*
		 * Creates a localization packet by default
		 */
		LocalizationPacket();

		/*
		 * Creates a copy of a LocalizationPacket packet
		 *
		 * @param copy The packet to copy
		 */
		LocalizationPacket(LocalizationPacket *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~LocalizationPacket();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the number of robots detected in the field
		 *
		 * @returns The number of robots in the field
		 */
		char getNumRobots();

		/*
		 * Gets a pointer to the robot data
		 *
		 * @returns An array of robots detected by the localization system
		 */
		const struct localization_robot_t *getRobotData();

		/*
		 * Gets the number of objects detected in the field
		 *
		 * @returns The number of objects in the field
		 */
		char getNumObjects();

		/*
		 * Gets a pointer to the object data
		 *
		 * @returns An array of objects detected by the localization system
		 */
		const struct localization_object_t *getObjectData();

		/*
		 * Gets the time the packet was sent, in milliseconds
		 *
		 * @returns The timestamp of the packet
		 */
		unsigned short getTimestamp();
};

#endif

