#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "profiler_packet.h"

/*
 * Creates a profiler with to profile data
 */
ProfilerPacket::ProfilerPacket() : BasePacket(OPCODE_PROFILER) {
	connection = NULL;
	num_samples = 0;
}

/*
 * Creates a copy of the given robot identification
 *
 * @param copy The packet to copy
 */
ProfilerPacket::ProfilerPacket(ProfilerPacket *copy) : BasePacket(OPCODE_PROFILER) {
	this->connection  = copy->connection;
	this->num_samples = copy->num_samples;
	this->loop_time   = copy->loop_time;
	memcpy(this->profile_data, copy->profile_data, sizeof(struct profiler_information_t) * MAX_PROFILER_SAMPLES);
}

/*
 * Frees any resources used by this packet
 */
ProfilerPacket::~ProfilerPacket() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 */
BasePacket *ProfilerPacket::read(NetworkBitstream &in) {
	in>>num_samples;
	in>>loop_time;
	for (int i=0; i<num_samples; i++) {
		in>>profile_data[i].fMin;
		in>>profile_data[i].fAvg;
		in>>profile_data[i].fMax;
		in>>profile_data[i].fTime;
		in>>profile_data[i].callCount;
		in>>profile_data[i].name;
		in>>profile_data[i].parentCount;
	}
	return new ProfilerPacket(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void ProfilerPacket::write(NetworkBitstream &out) const {
	BasePacket::write(out);
	out<<num_samples;
	out<<loop_time;
	for (int i=0; i<num_samples; i++) {
		out<<profile_data[i].fMin;
		out<<profile_data[i].fAvg;
		out<<profile_data[i].fMax;
		out<<profile_data[i].fTime;
		out<<profile_data[i].callCount;
		out<<profile_data[i].name;
		out<<profile_data[i].parentCount;
	}
}

/*
 * Called before the profiler starts sending profile data.  Can
 * be used to initialize / print headers for the printed data
 *
 * @param fTotal The total time of the profile loop
 */
void ProfilerPacket::beginOutput(float tTotal) {
	loop_time = tTotal;
	num_samples = 0;
}

/*
 * Called for each sample that was recorded in the profiler.  Contains
 * data about a section of code that was profiled.
 *
 * @param fMin The minimum amount of time this sample took to run
 * @param fAvg The average amount of time this sample took to run
 * @param fMax The maximum amount of time this sample took to run
 * @param fTime The time it took this sample to run
 * @param callCount The number of times this sample was called
 * @param name The name of the sample
 * @param parentCount The number of parent samples before this sample
 */
void ProfilerPacket::sample(float fMin, float fAvg, float fMax, float fTime, unsigned char callCount, char *name, unsigned char parentCount) {
	if (num_samples < MAX_PROFILER_SAMPLES) {
		strncpy(profile_data[num_samples].name, name, PROFILER_MAX_NAME_LEN);
		profile_data[num_samples].fMin = fMin;
		profile_data[num_samples].fMin = fMin;
		profile_data[num_samples].fAvg = fAvg;
		profile_data[num_samples].fMax = fMax;
		profile_data[num_samples].fTime = fTime;
		profile_data[num_samples].callCount = callCount;
		profile_data[num_samples].parentCount = parentCount;

		num_samples++;
	}
}

/*
 * Called when all samples have been sent, and the profiler is done
 * sending data to be outputted.
 */
void ProfilerPacket::endOutput() {
	if (connection != NULL) {
		connection->transmit(this);
	}
}

/*
 * Gets the connection that the profiler transmits profile information on.
 */
TCP_Connection *ProfilerPacket::getConnection() const {
	return connection;
}

/*
 * Sets the connection that the profiler will transmit data on.
 *
 * @param connection The connection to send the profiler info on
 */
void ProfilerPacket::setConnection(TCP_Connection *connection) {
	this->connection = connection;
}

/*
 * Returns the number of samples in the data
 */
unsigned char ProfilerPacket::getNumSamples() const {
	return num_samples;
}

/*
 * Gets the amount of time it took in the root-level sample (The
 * loop time)
 */
float ProfilerPacket::getLoopTime() const {
	return loop_time;
}

/*
 * Copies the data for the request sample into the output structure.
 *
 * @param sampleNum The sample to get
 * @param out The data for the sample will be written to this variable
 *
 * @returns 1 on success, or 0 on failure (sampleNum was not in range)
 */
int ProfilerPacket::getSample(unsigned char sampleNum, struct profiler_information_t &out) const {
	if (sampleNum >= num_samples) {
		return 0;
	}

	memcpy(&out, &profile_data[sampleNum], sizeof(struct profiler_information_t));

	return 1;
}
