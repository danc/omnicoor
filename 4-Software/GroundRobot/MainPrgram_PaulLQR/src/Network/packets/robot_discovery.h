#ifndef _COMMON_PACKETS_ROBOT_DISCOVERY_H
#define _COMMON_PACKETS_ROBOT_DISCOVERY_H

#include <arpa/inet.h>
#include "common.h"
#include "base_packet.h"
#include "../network.h"

class RobotDiscovery : public BasePacket {
	private:
		unsigned char robot_id;
		struct sockaddr_in addr;

	public:
		/*
		 * Creates an invalid robot discovery packet
		 */
		RobotDiscovery();

		/*
		 * Creates an robot discovery packet with the given robot ID
		 *
		 * @param robot_id   The ID of the robot
		 */
		RobotDiscovery(unsigned char robot_id, struct sockaddr_in addr);

		/*
		 * Creates a copy of the given robot identification
		 *
		 * @param copy The packet to copy
		 */
		RobotDiscovery(RobotDiscovery *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~RobotDiscovery();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the ID of the robot
		 */
		unsigned char getRobotID() const;

		/*
		 * Gets the robot address
		 */
		struct sockaddr_in getRobotAddr() const; 

		/*
		 * Sets the ID of the robot
		 */
		void setRobotID(unsigned char newID);
};

#endif
