#include <stdio.h>
#include "base_packet.h"
#include "all_packets.h"

/*
 * Initializes the opcode to 0 (Invalid)
 * and the size to 2 (1 byte opcode, 1 byte size)
 */
BasePacket::BasePacket() {
	this->opcode = 0;
	this->size = 2;
}

/*
 * Initializes the base packet with the given OPCode
 *
 * @param opcode The opcode of the packet
 */
BasePacket::BasePacket(unsigned char opcode) {
	this->opcode = opcode;
	this->size = 2;
}

/*
 * Frees any resources used by this packet
 */
BasePacket::~BasePacket() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 */
BasePacket *BasePacket::read(NetworkBitstream &in) {
	BasePacket *full_packet;
	BasePacket *temp_packet;

	in.resetReadPtr();
	in>>size;
	in>>opcode;

	switch(opcode) {
		case OPCODE_ROBOT_IDENTIFICATION:
			temp_packet = new RobotIdentification();
			break;
		case OPCODE_SENSOR:
			temp_packet = new SensorControl();
			break;
		case OPCODE_MOTOR:
			temp_packet = new MotorControl();
			break;
		case OPCODE_PROFILER:
			temp_packet = new ProfilerPacket();
			break;
		case OPCODE_CONFIGURATION:
			temp_packet = new ConfigurationPacket();
			break;
		case OPCODE_ROBOT_DISCOVERY:
			temp_packet = new RobotDiscovery();
			break;
		case OPCODE_LOCALIZATION:
			temp_packet = new LocalizationPacket();
			break;
		case OPCODE_AI_LIST:
			temp_packet = new AIList();
			break;
		case OPCODE_AI_SQUARE_DANCE:
			temp_packet = new SquareDance();
			break;
		case OPCODE_XBOX_SENSOR:
			temp_packet = new XboxSensor();
			break;
		case OPCODE_PEER_MOTOR_SPEED:
			temp_packet = new PeerMotorSpeed();
			break;
		default:
			fprintf(stderr, "BasePacket::read:Error: Read packet in with unknown OPCODE: %d\n", opcode);
			return new BasePacket();
	};

	full_packet = temp_packet->read(in);
	delete temp_packet;

	return full_packet;
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void BasePacket::write(NetworkBitstream &out) const {
	out.reset();
	out<<size;
	out<<opcode;
}

/*
 * Finalizes a packet before it is sent out on the network;
 * Pretty much just updates the packet information with the
 * correct size of the data.
 *
 * @param out The bitstream to write to
 */
void BasePacket::finalize(NetworkBitstream &out) {
	size = out.getLength();

	out.resetWritePtr();
	out<<size;
	out<<opcode;
}

/*
 * Gets the opcode for the packet
 *
 * @returns The opcode associated with the packet
 */
unsigned char BasePacket::getOpcode() {
	return opcode;
}

/*
 * Sets the opcode for this packet
 *
 * @param newOpcode The new opcode for the packet
 */
void BasePacket::setOpcode(unsigned char newOpcode) {
	opcode = newOpcode;
}

/*
 * Gets the size of this packet
 *
 * @returns the size, in bytes, of the packet
 */
unsigned char BasePacket::getSize() {
	return size;
}
