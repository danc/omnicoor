#ifndef _NETWORK_NETWORK_H
#define _NETWORK_NETWORK_H

#include "common.h"

#define IP_ADDR_MAX_LEN	40



typedef struct {
	// 40 is the maximum length of an IPv6 human-readable IP address
	// (Including null-terminating character)
	char ip_addr[IP_ADDR_MAX_LEN];
	int  port;
} network_config_t;

#endif
