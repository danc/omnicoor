#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "common.h"
#include "tcp_server.h"
#include "udp_connection.h"
#include "packet_handler.h"
#include "packets/base_packet.h"
#include "network_connection_handler.h"
#include "packets/motor_control.h"

class NetworkReceiver : public PacketHandler, public NetworkConnectionHandler {
	public:
		NetworkReceiver() {}
		~NetworkReceiver() {}
		void processPacket(TCP_Connection *connection, BasePacket *base_packet) {
			MotorControl *packet = (MotorControl *)base_packet;

			printf("NetworkReceiver::Received network packet (RobotID %d):\n", connection->getRobotID());
			printf("\tOpcode:\t%d\n", packet->getOpcode());
			printf("\tCommand:\t%c\n", packet->getCommand());
		}

		void processPacket(UDP_Connection *connection, BasePacket *base_packet) {
			MotorControl *packet = (MotorControl *)base_packet;

			printf("NetworkReceiver::Received network packet:\n");
			printf("\tOpcode:\t%d\n", packet->getOpcode());
			printf("\tCommand:\t%c\n", packet->getCommand());
		}


		void NewConnection(TCP_Connection *con) {
			printf("NetworkReceiver::New Connection\n");
			con->setPacketHandler(this);
			con->setRobotID(1);
		}

		void ConnectionClose(TCP_Connection *con) {
			con->setPacketHandler(NULL);
			printf("NetworkReceiver::Connection Close\n");
		}
};

void testNetworkBitStream() {
	int it = -13;
	char chr = 'A';
	long lng = 45;
	float flt = 5.123;
	char str[512];

	unsigned int uit = 14;

	NetworkBitstream nbs("", 0);

	strcpy(str, "BOB");
	nbs<<it<<chr<<lng<<flt<<uit<<((const char *)str);

	printf("Testing NetworkBitStream:\n\n");

	printf("Testing writing to stream...\n");
	printf("Expected: 24:\t-13\tA\t45\t5.123000\t14\tBOB\n");
	printf("Actual:   %d:\t%d\t%c\t%ld\t%f\t%u\t%s\n", nbs.getLength(), it, chr, lng, flt, uit, str);
	printf("\n");

	it = 0;
	chr = 0;
	lng = 0;
	flt = 0.0f;
	uit = 0;
	strcpy(str, "");

	nbs>>it>>chr>>lng>>flt>>uit>>str;

	printf("Testing reading from stream...\n");
	printf("Expected: 24:\t-13\tA\t45\t5.123000\t14\tBOB\n");
	printf("Actual:   %d:\t%d\t%c\t%ld\t%f\t%u\t%s\n", nbs.getLength(), it, chr, lng, flt, uit, str);
	printf("\n");
}

void testTCPConnection() {
	network_config_t net_config;
	TCP_Server serv;
	NetworkReceiver handler;

	strcpy(net_config.ip_addr, "127.0.0.1");
	net_config.port = 12345;

#	ifndef DEBUG
	fprintf(stderr, "This program should be compiled with debugging output enabled.  Otherwise, some tests cannot be run.\n");
#	endif

	if (serv.start(net_config) != 1) {
		printf("Error: Could not start server\n");
	} else {

		serv.setConnectionHandler(&handler);

		sleep(1);

		TCP_Connection con;
		con.setRobotID(1);

		if (con.start(net_config) != 1) {
			printf("Error: Could not connect to server\n");
		} else {
			con.setPacketHandler(&handler);
			sleep(1);
			printf("\n\n");

			MotorControl mc;
			mc.setCommand('A');

			printf("Sending OPCODE 2, Payload 'A' from client to server\n");
			con.transmit(&mc);

			sleep(1);

			printf("\n");
			printf("Sending OPCODE 2, Payload 'B' from client to server\n");
			mc.setCommand('B');
			con.transmit(&mc);

			sleep(1);

			printf("\n");
			printf("Sending OPCODE 2, Payload 'C' from server to client\n");
			mc.setCommand('C');
			if (serv.transmit(1, &mc) != 1) {
				printf("ERROR: Robot ID 1 not found\n");
			}

			sleep(1);

			printf("\n");
			printf("Broadcasting OPCODE 2, Payload 'D' from server to client\n");
			mc.setCommand('D');
			if (serv.broadcast(&mc) != 1) {
				printf("ERROR: Robot ID 1 not found\n");
			}


			sleep(1);

			printf("\n");
			printf("Setting connection to non-threaded command...\n");
			con.setUsePacketHandlerThread(0);

			sleep(1);

			printf("\n");
			printf("Sending OPCODE 2, Payload 'E' from server to client.  This should not show up yet.\n");
			mc.setCommand('E');
			if (serv.transmit(1, &mc) != 1) {
				printf("Error: Robot ID 1 not found\n");
			}

			sleep(1);

			printf("\n");
			printf("Sending OPCODE 2, Payload 'F' from server to client.  This should not show up yet.\n");
			mc.setCommand('F');
			if (serv.transmit(1, &mc) != 1) {
				printf("Error: Robot ID 1 not found\n");
			}

			sleep(1);

			printf("\n");
			printf("Running packet processing method.  The previous 2 packets should show up now.\n");

			sleep(1);

			con.run();

			sleep(1);

			printf("\n");
			printf("Switching back to threaded command.  Packets should now show immediatly after they are broadcast.\n");
			con.setUsePacketHandlerThread(1);

			sleep(1);

			printf("\n");
			printf("Sending OPCODE 2, Payload 'G' from server to client\n");
			mc.setCommand('G');
			if (serv.transmit(1, &mc) != 1) {
				printf("ERROR: Robot ID 1 not found\n");
			}

			sleep(1);

			printf("\n");
			printf("Closing client connection.\n");

			con.stop();

			sleep(1);

			printf("\n");
			printf("Setting server to non-threaded command.\n");
			serv.setUseConnectionHandlerThread(0);

			sleep(1);

			printf("Attempting to connect with a client again.  The server should NOT acknowledge this connection.\n");
			if (con.start(net_config) != 1) {
				printf("ERROR: Could not connect with client!\n");
			}
			sleep(2);

			printf("\n");
			printf("Running server thread.  The connection should be picked up now.\n");
			serv.run();

			sleep(1);

			printf("\n");
			printf("Sending OPCODE 2, Payload 'H' from server to client.\n");
			mc.setCommand('H');
			if (serv.transmit(1, &mc) != 1) {
				printf("Error: Robot ID 1 not found\n");
			}

			sleep(1);

			printf("\n");
			printf("Closing connection to server.  This should NOT be picked up by the server yet.\n");
			con.stop();

			sleep(1);

			printf("\n");
			printf("Switching server to threaded command.  The closed connection should be picked up.\n");
			serv.setUseConnectionHandlerThread(1);

			sleep(1);

			printf("\n");
			printf("Attempting to connect with a client again.  The server should acknowledge this connection within 1 second.\n");
			if (con.start(net_config) != 1) {
				printf("ERROR: Could not connect with client!\n");
			}
			sleep(3);

			printf("\n");
			printf("All tasks complete.\n");
			con.stop();

			printf("\n\n");

		}

		serv.stop();
	}
}

void testUDPConnection() {
	network_config_t net_config;
	UDP_Connection con1;
	NetworkReceiver handler;

	strcpy(net_config.ip_addr, "192.168.0.255");
	net_config.port = 12345;

#	ifndef DEBUG
	fprintf(stderr, "This program should be compiled with debugging output enabled.  Otherwise, some tests cannot be run.\n");
#	endif

	con1.setRobotID(2);
	con1.setPacketHandler(&handler);

	if (con1.start(net_config) != 1) {
		printf("Error: Could not start server\n");
	} else {
		printf("Connection is up!\n");

		MotorControl mc;
		mc.setCommand('A');

		//con1.sendRobotDiscovery();

		/*
		printf("Sending OPCODE 2, Payload 'A'\n");
		if (con1.transmit(KOREBOT_ID_BROADCAST, &mc) != 1) {
			printf("Error: Robot not found\n");
		}
		*/

		sleep(3);

		con1.stop();
	}
}

int main(int argc, char **argv) {
	testNetworkBitStream();
	testTCPConnection();
	//testUDPConnection();

	return 0;
}
