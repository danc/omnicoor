#ifndef _NETWORK_NETWORK_CONNECTION_HANDLER_H
#define _NETWORK_NETWORK_CONNECTION_HANDLER_H

#include "tcp_connection.h"
#include "common.h"

class NetworkConnectionHandler {
	public:
		virtual ~NetworkConnectionHandler() {}

		/*
		 * Called when a new korebot attempts to connect to the server.
		 * The network handler must set the packet handler for the new
		 * connection, or packets received from the connection will not
		 * be processed.
		 *
		 * @param con The new connection
		 */
		virtual void NewConnection(TCP_Connection *con)=0;

		/*
		 * Called when a korebot has closed its connection with the
		 * server.  If the program needs to free up any resources that
		 * this connection was using, they should be freed, as after this
		 * call is made, the connection will no longer be a valid pointer.
		 *
		 * @param con A pointer to the TCP connection that was closed
		 */
		virtual void ConnectionClose(TCP_Connection *con)=0;
};

#endif
