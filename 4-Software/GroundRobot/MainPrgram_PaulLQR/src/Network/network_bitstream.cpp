#include <arpa/inet.h>
#include <string.h>
#include "network_bitstream.h"

/*
 * Creates a new network bitstream with no data
 */
NetworkBitstream::NetworkBitstream() {
	this->length = 0;
	this->readPtr  = 0;
	this->writePtr = 0;
}

/*
 * Creates a new network bitstream with the given data
 *
 * @param data The data read in from the length
 * @param length The length of data
 */
NetworkBitstream::NetworkBitstream(const char *data, unsigned short length) {
	if (length <= MAX_NETWORK_PACKET_SIZE) {
		memcpy(this->data, data, length);
		this->length = length;
	} else {
		this->length = 0;
	}
	this->readPtr  = 0;
	this->writePtr = 0;
}

/*
 * Frees any resources used by this object
 */
NetworkBitstream::~NetworkBitstream() {
	this->length = 0;
}

/*
 * Clears the data in the object, reseting the length
 * of the data to 0, and reseting the read & write
 * pointers to 0.
 */
void NetworkBitstream::reset() {
	this->length = 0;
	this->writePtr = 0;
	this->readPtr = 0;
}

/*
 * Resets the read pointer to the beginning of the data.
 */
void NetworkBitstream::resetReadPtr() {
	this->readPtr = 0;
}

/*
 * Resets the write pointer to the beginning of the data.
 */
void NetworkBitstream::resetWritePtr() {
	this->writePtr = 0;
}

/*
 * Gets the length of the data in the stream
 *
 * @returns The length of the data
 */
int NetworkBitstream::getLength() const {
	return length;
}

/*
 * Gets the data in the stream
 *
 * @returns A pointer to the data in the stream
 */
const char *NetworkBitstream::getData() const {
	return data;
}

/*
 * Sets the data this object represents
 *
 * @param data The data in the stream
 * @param length The length of the data
 *
 * @returns void
 */
void NetworkBitstream::setData(const char *data, int length) {
	if (length > MAX_NETWORK_PACKET_SIZE) {
		length = MAX_NETWORK_PACKET_SIZE;
	}

	memcpy(this->data, data, length);
	this->length = length;
	this->writePtr = 0;
	this->readPtr = 0;
}

NetworkBitstream &operator >>(NetworkBitstream &in, char *data) {
	size_t length;

	in>>length;

	if (in.readPtr + length > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(data, &in.data[in.readPtr], length);
	data[length] = '\0';
	in.readPtr += length;

	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, short &data) {
	short value;
	if (in.readPtr + sizeof(short) > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(&value, &in.data[in.readPtr], sizeof(short));

	data = ntohs(value);

	in.readPtr += sizeof(short);

	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, int &data) {
	int value;

	if (in.readPtr + sizeof(int) > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(&value, &in.data[in.readPtr], sizeof(int));
	data = ntohl(value);

	in.readPtr += sizeof(int);
	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, char &data) {
	if (in.readPtr + 1 > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	data = in.data[in.readPtr];

	in.readPtr += 1;
	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, float &data) {
	if (in.readPtr + sizeof(float) > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(&data, &in.data[in.readPtr], sizeof(float));

	in.readPtr += sizeof(float);
	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, long &data) {
	long value;

	if (in.readPtr + sizeof(long) > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(&value, &in.data[in.readPtr], sizeof(long));

	data = ntohl(value);

	in.readPtr += sizeof(long);
	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, unsigned short &data) {
	unsigned short value;
	if (in.readPtr + sizeof(unsigned short) > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(&value, &in.data[in.readPtr], sizeof(unsigned short));
	data = ntohs(value);

	in.readPtr += sizeof(unsigned short);
	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, unsigned int &data) {
	unsigned int value;
	if (in.readPtr + sizeof(unsigned int) > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(&value, &in.data[in.readPtr], sizeof(unsigned int));
	data = ntohl(value);

	in.readPtr += sizeof(unsigned int);
	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, unsigned char &data) {
	if (in.readPtr + 1 > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	data = (unsigned char)in.data[in.readPtr];

	in.readPtr += 1;

	return in;
}

NetworkBitstream &operator >>(NetworkBitstream &in, unsigned long &data) {
	unsigned long value;
	if (in.readPtr + sizeof(long) > in.length) {
		fprintf(stderr, "Could not read information from network packet: End of packet data.\n");
		throw "Could not read information from network packet: End of packet data.";
	}

	memcpy(&value, &in.data[in.readPtr], sizeof(unsigned long));
	data = ntohl(value);

	in.readPtr += sizeof(unsigned long);
	return in;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const char *data) {
	size_t length = strlen(data);

	out<<length;

	if (out.writePtr + length > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	memcpy(&out.data[out.writePtr], data, length);

	out.writePtr += length;
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const short &data) {
	short netdata;
	if (out.writePtr + sizeof(short) > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	netdata = htons(data);
	memcpy(&out.data[out.writePtr], &netdata, sizeof(short));
	out.writePtr += sizeof(short);
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const int &data) {
	int netdata;
	if (out.writePtr + sizeof(int) > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	netdata = htonl(data);
	memcpy(&out.data[out.writePtr], &netdata, sizeof(int));
	out.writePtr += sizeof(int);
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const char &data) {
	if (out.writePtr + 1 > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	out.data[out.writePtr] = data;
	out.writePtr += 1;
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const float &data) {
	if (out.writePtr + sizeof(float) > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	memcpy(&out.data[out.writePtr], &data, sizeof(float));
	out.writePtr += sizeof(float);
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const long &data) {
	long netdata;
	if (out.writePtr + sizeof(long) > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	netdata = htonl(data);
	memcpy(&out.data[out.writePtr], &netdata, sizeof(long));
	out.writePtr += sizeof(long);
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const unsigned short &data) {
	unsigned short netdata;
	if (out.writePtr + sizeof(unsigned short) > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	netdata = htons(data);
	memcpy(&out.data[out.writePtr], &netdata, sizeof(unsigned short));
	out.writePtr += sizeof(unsigned short);
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const unsigned int &data) {
	unsigned int netdata;
	if (out.writePtr + sizeof(unsigned int) > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	netdata = htonl(data);
	memcpy(&out.data[out.writePtr], &netdata, sizeof(unsigned int));
	out.writePtr += sizeof(unsigned int);
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const unsigned char &data) {
	if (out.writePtr + 1 > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	out.data[out.writePtr] = data;
	out.writePtr += 1;
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}

NetworkBitstream &operator <<(NetworkBitstream &out, const unsigned long &data) {
	unsigned long netdata;
	if (out.writePtr + sizeof(unsigned long) > MAX_NETWORK_PACKET_SIZE) {
		fprintf(stderr, "Could not write information to network packet: Maximum packet length has been exceeded.\n");
		throw "Could not write information to network packet: Maximum packet length has been exceeded.";
	}

	netdata = htonl(data);
	memcpy(&out.data[out.writePtr], &netdata, sizeof(unsigned long));
	out.writePtr += sizeof(unsigned long);
	if (out.length < out.writePtr) {
		out.length = out.writePtr;
	}

	return out;
}
