#ifndef _INCLUDE_NETWORK_PACKET_HANDLER_H
#define _INCLUDE_NETWORK_PACKET_HANDLER_H

#include "packets/base_packet.h"
#include "tcp_connection.h"
#include "udp_connection.h"
#include "common.h"

class TCP_Connection;
class UDP_Connection;

class PacketHandler {
	public:
		virtual ~PacketHandler() {}

		/*
		 * When a packet is received on the network, this method is called to
		 * process the packet.
		 *
		 * @param con    The connection the packet was received on
		 * @param packet The packet that was received from the network
		 */
		virtual void processPacket(TCP_Connection *con, BasePacket *packet)=0;

		/*
		 * When a packet is received on the network, this method is called to
		 * process the packet.
		 *
		 * @param con    The connection the packet was received on
		 * @param packet The packet that was received from the network
		 */
		virtual void processPacket(UDP_Connection *con, BasePacket *packet)=0;
};

#endif
