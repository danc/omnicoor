// Standard Library Files
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <pthread.h>

// Networking files
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <sys/queue.h>

#include "common.h"
#include "tcp_server.h"
#include "tcp_connection.h"

void *tcpListenThreadProxy(void *data);

/*
 * Creates a new tcp_server structure and initializes the values
 * in the structure to default values.
 *
 * @returns A pointer to a TCP Server structure that should be
 *			passed to any further calls on the server
 */
TCP_Server::TCP_Server() {
	init(1, NULL);
}

/*
 * Creates a new tcp_server structure and initializes the values
 * in the structure to default values.
 *
 * @param handler The object to handle new connection requests
 */
TCP_Server::TCP_Server(int useConnectionHandlerThread, NetworkConnectionHandler *handler) {
	init(useConnectionHandlerThread, handler);
}

/*
 * Destroys the TCP server, and all connected clients.
 */
TCP_Server::~TCP_Server() {
	stop();
}

/*
 * Initializes the values for the class to default values.
 *
 * @param useConnectionHandlerThread 1 if we should use a thread to
 * 			handle incoming connections, or 0 if the creator will
 * 			call run() to handle connections themelves
 * @param handler The object to handle new connection requests
 */
void TCP_Server::init(int useConnectionHandlerThread, NetworkConnectionHandler *conHandler) {
	// We are not listening by default
	we_are_listening = 0;

	// Non-Valid socket
	sockfd = -1;

	// By default listen on localhost
	strcpy(config.ip_addr, "127.0.0.1");

	// Just choose a random port in the upper ranges
	config.port = 54321;

	// Shouldn't try to shutdown
	shutdown_ready = 0;

	// Invalid handle
	connection_handler_thread = 0;

	// Connection handler thread is not running
	connection_handler_thread_running = 0;

	num_clients = 0;

	SLIST_INIT(&connections);

	connection_handler = conHandler;

	use_connection_handler_thread = useConnectionHandlerThread;

	connection_handler_is_starting = 0;
}

/*
 * Starts the server.  The server attempts to listen on the given address and port
 *					 specified in the config parameter.
 *
 * @param config The network configuration
 *
 * @returns 1 on success, <= 0 otherwise
 */
int TCP_Server::start(const network_config_t &config) {
	struct sockaddr_in local_address;
	int tcp_args;

	if (we_are_listening) {
		// If we are currently listening for connections, stop listening first
		stop();
	}

	memset(&local_address, 0, sizeof(struct sockaddr_in));
	memcpy(&this->config, &config, sizeof(network_config_t));

	local_address.sin_family = AF_INET;
	local_address.sin_port = htons(config.port);

#	ifdef DEBUG
		printf("Listening on %s:%d\n", config.ip_addr, config.port);
#	endif

	// Set the server IP address
	if (inet_aton(config.ip_addr, (struct in_addr *) &(local_address.sin_addr.s_addr)) == 0) {
		fprintf(stderr, "TCP_Server::start:Error: Could not initialize socket: Invalid Listening Address\n");
		close(sockfd);
		return -1;
	}

	// Create the socket
	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
		perror("TCP_Server::start:Error: Could not initialize socket");
		return -2;
	}

	// Set non-blocking
	if( (tcp_args = fcntl(sockfd, F_GETFL, NULL)) < 0) {
		perror("TCP_Server::start:Error: Could not get socket info");
		destroySocket();
		return -3;
	}

	tcp_args |= O_NONBLOCK;

	if( fcntl(sockfd, F_SETFL, tcp_args) < 0) {
		perror("TCP_Server::start:Error: Could not set non-blocking socket");
		destroySocket();
		return -4;
	}


	// Bind to the specified port
	if (bind(sockfd, (struct sockaddr *) &local_address, sizeof(struct sockaddr_in)) == -1) {
		perror("TCP_Server::start:Error: Could not bind to host / port");
		destroySocket();
		return -1;
	}

	// Start listening for connections
	if (listen(sockfd, 10) == -1) {
		perror("TCP_Server::start:Error: Could not listen for new connections");
		destroySocket();
		return -5;
	}

	we_are_listening = 1;
	shutdown_ready = 0;

	if (use_connection_handler_thread) {
		if (!launchListenThread()) {
			return -6;
		}
	}

	return 1;
}

/*
 * Listens for incoming connections and creates new
 * threads for the incoming connections.
 */
void TCP_Server::listenThread() {
	connection_handler_thread_running = 1;
	connection_handler_is_starting = 0;

	while (!shutdown_ready && use_connection_handler_thread) {
		if (!run()) {
			break;
		}
		sleep(1);
	}
	connection_handler_thread_running = 0;
}

int TCP_Server::run() {
	struct tcp_connection_entry *connection_entry;
	struct tcp_connection_entry *temp;
	struct sockaddr_in client_info;
	int connectionfd;
	socklen_t length;

	if (isListening()) {
		while (1) {
			length = sizeof(struct sockaddr);
			if ((connectionfd = accept(sockfd, (struct sockaddr *)&client_info, &length)) <= 0) {

				// check if this was an error, or just no waiting clients
				if (errno != EAGAIN) {
					perror("TCP_Server::run:Error: Could not accept connection from client");
					return 0;
				} else {
					// Socket is still OK, but there are no waiting connections.

					// Check for clients that have disconnected, and free the
					// memory they were taking up.
					for (connection_entry = SLIST_FIRST(&connections); connection_entry != NULL; ) {
						if (!connection_entry->connection->isConnected()) {
							// Client has lost the connection...
							temp = connection_entry;
							connection_entry = SLIST_NEXT(connection_entry, connections);

#						ifdef DEBUG
								printf("Client %s:%d is no longer connected.\n", temp->connection->config.ip_addr, temp->connection->config.port);
#						endif

							// Inform the rest of the program of the closed connection
							if (connection_handler != NULL) {
								connection_handler->ConnectionClose(temp->connection);
							}

							// Remove the connection from our list
							SLIST_REMOVE(&connections, temp, tcp_connection_entry, connections);

							// Free the resources the connection was using
							temp->connection->stop();
							delete temp->connection;
							free(temp);

							num_clients--;

						} else {
							connection_entry = SLIST_NEXT(connection_entry, connections);
						}
					}
					return 1;
				}
			} else {
				// New connection, create the structures to handle it
				connection_entry                               = (struct tcp_connection_entry *)malloc(sizeof(struct tcp_connection_entry));
				connection_entry->connection                   = new TCP_Connection(use_connection_handler_thread, NULL);
				connection_entry->connection->sockfd           = connectionfd;
				connection_entry->connection->config.port      = ntohs(client_info.sin_port);
				connection_entry->connection->we_are_connected = 1;
				strcpy(connection_entry->connection->config.ip_addr, inet_ntoa(*((struct in_addr*) &(client_info.sin_addr.s_addr))));

#			ifdef DEBUG
					printf("Connection from %s:%d\n", connection_entry->connection->config.ip_addr, connection_entry->connection->config.port);
#			endif

				// Add the new connection to our list of active connections
				SLIST_INSERT_HEAD(&connections, connection_entry, connections);

				num_clients++;

				// Inform the rest of the program of the new connection
				// Make sure to do this before we launch the thread to handle
				// incoming packets, or else we could lose packets.
				if (connection_handler != NULL) {
					connection_handler->NewConnection(connection_entry->connection);
				}

				// Launch a thread to handle the connection (Only if it should run in a thread)
				if ( connection_entry->connection->use_packet_handler_thread ) {
					if (!connection_entry->connection->launchConnectionThread()) {
						SLIST_REMOVE_HEAD(&connections, connections);

						fprintf(stderr, "Failed to launch connection handler thread...Connection discarded\n");

						// Will close the socket
						connection_entry->connection->stop();
						delete connection_entry->connection;

						free(connection_entry);
					}
				}
			}
		}
	}

	return 0;
}

/*
 * Attempts to stop an active server.  Closes any active client
 * connections and stops listening on the port.
 *
 * @returns void
 */
void TCP_Server::stop() {
	struct tcp_connection_entry *entry;

	// Tell the listen thread to shutdown
	shutdown_ready = 1;

#	ifdef DEBUG
		printf("Server is shutting down...\n");
#	endif

	if (connection_handler_thread != 0) {
		// Wait for the socket thread to finish
		if (pthread_join(connection_handler_thread, NULL) != 0) {
			perror("TCP_Server::stop:Error: Could not re-join connection handler thread");
		}

		connection_handler_thread = 0;
	}

	while (SLIST_FIRST(&connections) != NULL) {
		entry = SLIST_FIRST(&connections);

		// Inform the rest of the program of the closed connection
		if (connection_handler != NULL) {
			connection_handler->ConnectionClose(entry->connection);
		}

		SLIST_REMOVE_HEAD(&connections, connections);

		entry->connection->stop();

		delete entry->connection;
		free(entry);

		num_clients--;
	}

	destroySocket();
	we_are_listening = 0;
}

/*
 * Sends a packet to the given korebot
 *
 * @param robot_id The robot to send to, or KOREBOT_ID_BROADCAST for broadcast
 * @param data     The data to send across the network
 *
 * @returns 1 on success, 0 otherwise
 */
int TCP_Server::transmit(unsigned char robot_id, BasePacket *data) {
	struct tcp_connection_entry *connection_entry;

	// Loop through all the active connections
	for (connection_entry = SLIST_FIRST(&connections); connection_entry != NULL; ) {
		// If we are broadcasting the packet, or if this is the desired robot (And the robot ID is valid)
		if (robot_id == KOREBOT_ID_BROADCAST || (robot_id == connection_entry->connection->getRobotID() && connection_entry->connection->getRobotID() != KOREBOT_ID_BROADCAST)) {
			// Send the packet
			connection_entry->connection->transmit(data);

			// If we are not sending this to all the korebots, we are done
			if (robot_id != KOREBOT_ID_BROADCAST) {
				return 1;
			}
		}
		// Move on to the next connection
		connection_entry = SLIST_NEXT(connection_entry, connections);
	}

	if (robot_id == KOREBOT_ID_BROADCAST) {
		// We broadcasted, so we always return success
		return 1;
	} else {
		// We could not find the requested robot
		return 0;
	}
}

/*
 * Broadcasts a packet to all Korebots
 *
 * @param data     The data to send across the network
 *
 * @returns 1 on success, 0 otherwise
 */
int TCP_Server::broadcast(BasePacket *data) {
	return transmit(KOREBOT_ID_BROADCAST, data);
}

/*
 * Launches the connection listener thread
 */
int TCP_Server::launchListenThread() {
	if (connection_handler_thread == 0) {
		connection_handler_is_starting = 1;
		// Start listening for incoming connections
		if (pthread_create(&connection_handler_thread, NULL, &tcpListenThreadProxy, this) != 0) {
			perror("TCP_Server::launchListenThread:Error: Could not create socket manager thread");
			return 0;
		}

		while (connection_handler_is_starting == 1) { 
			sleep(1);
		}
	}

	return 1;
}

/*
 * Returns 1 if the server is active and listening for incoming
 *			connections.  Returns 0 otherwise
 *
 * @returns (See description)
 */
int TCP_Server::isListening() {
	return we_are_listening && (!use_connection_handler_thread || connection_handler_thread_running);
}

/*
 * Returns the number of connected clients
 *
 * @returns The number of connected clients
 */
int TCP_Server::getNumClients() {
	return num_clients;
}

/*
 * Gets the currently active network configuration.
 *
 * @param config The network configuration will be copied into
 *			this parameter
 *
 * @returns void
 */
void TCP_Server::getConfiguration(network_config_t &config) {
	memcpy(&config, &this->config, sizeof(network_config_t));
}

/*
 * Gets the object than handles new connections.
 *
 * @returns NULL, if no object has been specified, or
 * 				the object otherwise
 */
NetworkConnectionHandler *TCP_Server::getConnectionHandler() {
	return connection_handler;
}

/*
 * Sets the object that handles new connections.
 *
 * @param newConHandler The new object to handle new network connections.
 *
 * @returns void
 */
void TCP_Server::setConnectionHandler(NetworkConnectionHandler *newConHandler) {
	connection_handler = newConHandler;
}

/*
 * Gets if this server uses a separate thread for processing
 * incoming connections, or if the creator calls a method to
 * do the processing.
 *
 * @returns 1, if the server uses a thread, 0 otherwise
 */
int TCP_Server::getUseConnectionHandlerThread() {
	return use_connection_handler_thread;
}

/*
 * Sets if this server should use a connection handler thread or
 * if some other part of the program will tell the server when
 * to check for incoming connections.  If the server was already using a
 * thread to process connections, and this method is called with a value
 * of 0, the thread will be stopped before this method returns.
 *
 * If the thread had not been created, it will be created.
 *
 * @param useConnectionHandlerThread 1 if we should use a thread to
 * 			handle incoming connections, or 0 if the creator will
 * 			call run() to handle connections themelves
 */
void TCP_Server::setUseConnectionHandlerThread(int useConnectionHandlerThread) {
	if (use_connection_handler_thread && !useConnectionHandlerThread) {
		// We were using a thread, and we shouldn't.  Set the new value, 
		// and join the thread with this one if it exists.
		use_connection_handler_thread = useConnectionHandlerThread;

		if (connection_handler_thread != 0) {
			// Wait for the socket thread to finish
			if (pthread_join(connection_handler_thread, NULL) != 0) {
				perror("TCP_Server::setUseConnectionHandlerThread:Error: Could not re-join connection handler thread");
			}

			connection_handler_thread = 0;
		}

	} else if (!use_connection_handler_thread && useConnectionHandlerThread) {
		// We were not using a thread, and we want to now.  Start a new
		// thread if a connection is active.

		use_connection_handler_thread = useConnectionHandlerThread;

		if (we_are_listening) {
			launchListenThread();
		}
	}
}

/*
 * Destroys the socket
 */
void TCP_Server::destroySocket() {
	if (sockfd != -1) {
#		ifdef DEBUG
			printf("Closing server listening on %s:%d\n", config.ip_addr, config.port);
#		endif
		close(sockfd);
		sockfd = -1;
	}
}


/*
 * Proxy function to launch the listener thread
 */
void *tcpListenThreadProxy(void *data) {
	if (data != NULL) {
		((TCP_Server *)data)->listenThread();
	}

	return NULL;
}
