#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "profiler.h"
#include "profiler_output_handler.h"

class ConsoleOutputHandler : public ProfilerOutputHandler {
	public:
		ConsoleOutputHandler() {}

		void beginOutput(float tTotal) {
			printf("Loop took %f seconds.\n", tTotal);
			printf("Min Time\tAvg Time\tMax Time\tTime this run\tCall Count\tName\n");
		}

		void sample(float fMin, float fAvg, float fMax, float fTime, unsigned char callCount, char *name, unsigned char parentCount) {
			int i;
			printf("%0.6f\t%f\t%f\t%f\t%d\t\t", 
					fMin, fAvg, fMax, fTime, callCount);
			for (i=0; i<parentCount; i++) {
				printf("\t");
			}

			printf("%s\n", name);
		}

		void endOutput() {
			printf("Output finished.\n\n");
		}

};

void sleep1() {
	PROFILE("Sleep 1 sec");

	sleep(1);
}

void sleep2() {
	PROFILE("Sleep 2 sec");

	sleep(2);
}

void loop1() {
	PROFILE("Loop 1");

	int i=0;
	char str[10];

	for (i=0; i<2000000; i++) {
		strcpy(str, "ABCD");
	}
}

void loop2() {
	PROFILE("Loop 2");

	int i=0;
	char str[10];

	for (i=0; i<5000000; i++) {
		strcpy(str, "ABCD");
	}
}

int main(int argc, char **argv) {
	ConsoleOutputHandler out;

#ifndef PROFILER
	printf("Error: Program was compiled without profiler support.\n");
	return 0;
#endif

	ProfileSample::output_handler = &out;
	ProfileSample::start_profiler = true;

	for (int i=0; i<20; i++) {
		{
			PROFILE("Root Level");

			/*
			sleep1();
			sleep2();
			*/

			loop1();
			loop1();
			loop1();
			loop2();
			loop2();
			loop2();
		}

		ProfileSample::output();
	}

	ProfileSample::stop_profiler = true;

	return 0;
}
