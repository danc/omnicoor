#ifndef _GUI_KOREBOT_MOTOR_TAB_H
#define _GUI_KOREBOT_MOTOR_TAB_H

#include <gtk/gtk.h>
#include "common.h"
#include "korebot_motor_control_ai.h"
#include "korebot_motor_control_release.h"
#include "korebot_motor_control_free.h"
#include "korebot_motor_control_manual.h"
#include "korebot_motor_control_speed.h"
#include "Network/packets/base_packet.h"

#define MOTOR_CONTROL_METHOD_INDEX_RELEASE	0
#define MOTOR_CONTROL_METHOD_INDEX_FREE		1
#define MOTOR_CONTROL_METHOD_INDEX_SPEED	2
#define MOTOR_CONTROL_METHOD_INDEX_MANUAL	3
#define MOTOR_CONTROL_METHOD_INDEX_AI		4

class KorebotTab;

class KorebotMotorTab {
	private:
		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		// The motor control selection dropdown box
		GtkWidget *motor_control_select;

		// Widget to stop the motors on this korebot
		GtkWidget *stop_motors;

		// The parent tab, used to send packets out on the network
		KorebotTab *parent;

		// Panels fore each of the different control methods
		KorebotMotorControlRelease *control_release;
		KorebotMotorControlFree    *control_free;
		KorebotMotorControlSpeed   *control_speed;
		KorebotMotorControlManual  *control_manual;
		KorebotMotorControlAI      *control_ai;
	public:

		/*
		 * Creates a default korebot motor tab
		 *
		 * @param parentTab The parent tab for this tab
		 */
		KorebotMotorTab(KorebotTab *parentTab);

		/*
		 * Frees any resources used by this korebot motor tab
		 */
		~KorebotMotorTab();

		/*
		 * Creates all the GTK elements needed by the korebot motor tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the korebot motor tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot motor tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:
		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);

		/*
		 * When the user selects a new motor control method, this method is
		 * called.
		 */
		void onMotorControlSelect();


	friend void korebot_motor_tab_on_action(GtkWidget *widget, gpointer data);
};

#endif
