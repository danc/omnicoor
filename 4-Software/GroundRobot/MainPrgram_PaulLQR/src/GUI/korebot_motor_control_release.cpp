#include <string.h>
#include "korebot_motor_control_release.h"
#include "korebot_tab.h"
#include "Network/packets/motor_control.h"

void korebot_motor_control_release_on_action(GtkWidget *widget, gpointer data);

/*
 * Creates a default korebot release motor control frame
 *
 * @param parentTab The parent tab for this tab
 */
KorebotMotorControlRelease::KorebotMotorControlRelease(KorebotTab *parentTab) {
	container       = NULL;
	release_control = NULL;
	parent          = parentTab;
}

/*
 * Releases any resources used by this frame
 */
KorebotMotorControlRelease::~KorebotMotorControlRelease() {
}

/*
 * Creates all the GTK elements needed by the frame
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotMotorControlRelease::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_vbox_new(FALSE, 0);

	release_control = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(release_control), gtk_label_new("Release Control of Motors"));
	gtk_signal_connect(GTK_OBJECT(release_control), "clicked", G_CALLBACK(korebot_motor_control_release_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(container), release_control, FALSE, FALSE, 20);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotMotorControlRelease::start() {
	gtk_widget_show_all(release_control);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotMotorControlRelease::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the release motor control frame.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot motor tab
 */
GtkWidget *KorebotMotorControlRelease::getContainer() {
	return container;
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotMotorControlRelease::processPacket(BasePacket *packet) {
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotMotorControlRelease::onUserAction(GtkWidget *widget) {
	if (widget == release_control) {
		MotorControl mot_con((char)MOTOR_CONTROL_MODE_RELEASE);
		parent->transmit(&mot_con);
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotMotorControlRelease::onIdle() {
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotMotorTab instance
 */
void korebot_motor_control_release_on_action(GtkWidget *widget, gpointer data) {
	((KorebotMotorControlRelease *)data)->onUserAction(widget);
}

