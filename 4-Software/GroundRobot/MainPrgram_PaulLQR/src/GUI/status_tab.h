#ifndef _GUI_STATUS_TAB_H
#define _GUI_STATUS_TAB_H

#include <gtk/gtk.h>
#include "common.h"

class StatusTab {
	private:
		GtkWidget *container;

		GtkWidget *tcp_server_label;
		GtkWidget *tcp_server_active;
		int tcp_server_is_active;

	public:
		/*
		 * Creates a default status tab
		 */
		StatusTab();

		/*
		 * Frees any resources used by this status tab
		 */
		~StatusTab();

		/*
		 * Creates all the GTK elements needed by the status tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the status tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the status tab
		 */
		GtkWidget *getContainer();

		/*
		 * Returns if the display thinks the TCP server is active.
		 *
		 * @returns 1, if the server is active, 0 if it is not, or -1 if 
		 * 			the status is unknown
		 */
		int getTcpServerIsActive();

		/*
		 * Set if the TCP server is active
		 *
		 * @param isActive 1, if the server is active, 0 otherwise
		 */
		void setTcpServerIsActive(int isActive);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:
		/*
		 * Sets the text for a boolean status object
		 *
		 * @param widget The label that represents the status of the object
		 * @param isActive 1, if the object is active, 0 if the object is inactive,
		 * 				or -1 if the status is unknown
		 */
		void setObjectActiveText(GtkWidget *widget, int isActive);
};

#endif
