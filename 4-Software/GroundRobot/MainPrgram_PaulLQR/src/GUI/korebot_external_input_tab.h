#ifndef _GUI_KOREBOT_EXTERNAL_INPUT_TAB_H
#define _GUI_KOREBOT_EXTERNAL_INPUT_TAB_H

#include <gtk/gtk.h>
#include "common.h"
#include "korebot_external_input_xbox.h"
#include "Network/packets/base_packet.h"

#define EXTERNAL_INPUT_XBOX		0

class KorebotTab;

class KorebotExternalInputTab {
	private:
		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		// The external_input control selection dropdown box
		GtkWidget *external_input_select;

		// The parent tab, used to send packets out on the network
		KorebotTab *parent;

		// Panels fore each of the different control methods
		KorebotExternalInputXBox *input_xbox;
	public:

		/*
		 * Creates a default korebot external_input tab
		 *
		 * @param parentTab The parent tab for this tab
		 */
		KorebotExternalInputTab(KorebotTab *parentTab);

		/*
		 * Frees any resources used by this korebot external_input tab
		 */
		~KorebotExternalInputTab();

		/*
		 * Creates all the GTK elements needed by the korebot external_input tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the korebot external_input tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot external_input tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:
		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);

		/*
		 * When the user selects a new external_input control method, this method is
		 * called.
		 */
		void onExternalInputSelect();


	friend void korebot_external_input_tab_on_action(GtkWidget *widget, gpointer data);
};

#endif
