#include <string.h>
#include "korebot_tab.h"
#include "main_gui.h"

/*
 * Creates a default korebot tab
 */
KorebotTab::KorebotTab(MainGUI *parent, unsigned char robotID, const char *robotName) {
	this->parent  = parent;
	this->robot_id = robotID;
	strncpy(this->robot_name, robotName, MAX_ROBOT_NAME_LENGTH);

	container      = NULL;
	motor_control  = NULL;
	sensors        = NULL;
	config         = NULL;
	profile_tab    = NULL;
	external_input = NULL;
}

/*
 * Frees any resources used by this korebot tab
 */
KorebotTab::~KorebotTab() {

	if (profile_tab != NULL) {
		delete profile_tab;
	}

	if (motor_control != NULL) {
		delete motor_control;
	}

	if (sensors != NULL) {
		delete sensors;
	}

	if (config != NULL) {
		delete config;
	}

	if (external_input != NULL) {
		delete external_input;
	}
}

/*
 * Creates all the GTK elements needed by the korebot tab
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotTab::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_notebook_new();

	config = new KorebotConfigurationTab(this);
	config->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(container), config->getContainer(), gtk_label_new("Configuration"));

	profile_tab = new KorebotProfilerTab(this);
	profile_tab->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(container), profile_tab->getContainer(), gtk_label_new("Profiler"));

	motor_control = new KorebotMotorTab(this);
	motor_control->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(container), motor_control->getContainer(), gtk_label_new("Motor Control"));

	sensors = new KorebotSensorTab(this);
	sensors->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(container), sensors->getContainer(), gtk_label_new("Sensors"));

	external_input = new KorebotExternalInputTab(this);
	external_input->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(container), external_input->getContainer(), gtk_label_new("External Inputs"));

	return 1;
}

/*
 * Shows the tab
 */
void KorebotTab::start() {
	config->start();
	sensors->start();
	motor_control->start();
	profile_tab->start();
	external_input->start();

	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotTab::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Sends a packet over the connection
 *
 * @param packet The packet to send
 */
void KorebotTab::transmit(BasePacket *packet) {
	parent->transmit(robot_id, packet);
}

/*
 * When a packet is received on the network, this method is called to
 * process the packet.
 *
 * @param packet The packet that was received from the network
 */
void KorebotTab::processPacket(TCP_Connection *con, BasePacket *packet) {
	switch(packet->getOpcode()) {

		case OPCODE_SENSOR:
			// Pass the packet on to the sensors tab
			sensors->processPacket(packet);
			break;

		case OPCODE_AI_LIST:
			// Send the packet to the global korebot tab, so it has
			// a list of available AI
			if (robot_id != KOREBOT_ID_BROADCAST) {
				parent->processPacket(con, packet);
			}
			// Fall through...Send the packet to the motor controller
		case OPCODE_MOTOR:
			// Pass the packet on to the motor tab
			motor_control->processPacket(packet);
			break;

		case OPCODE_PROFILER:
			profile_tab->processPacket(packet);
			break;

		case OPCODE_CONFIGURATION:
			config->processPacket(packet);
			break;

		case OPCODE_XBOX_SENSOR:
			external_input->processPacket(packet);
			break;

		default:
			fprintf(stderr, "KorebotTab::processPacket: Received unknown packet (Korebot: %d, Opcode: %d)\n", con->getRobotID(), packet->getOpcode());
			break;
	}
}

/*
 * When a packet is received on the network, this method is called to
 * process the packet.
 *
 * @param connection The source connection
 * @param packet The packet that was received from the network
 */
void KorebotTab::processPacket(UDP_Connection *connection, BasePacket *packet) {
}


/*
 * Gets the top-level container for the korebot tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot tab
 */
GtkWidget *KorebotTab::getContainer() {
	return container;
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotTab::onIdle() {
	motor_control->onIdle();
	sensors->onIdle();
	config->onIdle();
	profile_tab->onIdle();
	external_input->onIdle();
}

/*
 * Gets the name of the robot associated with this tab
 *
 * @returns A pointer to the name of the robot
 */
const char *KorebotTab::getRobotName() {
	return robot_name;
}

/*
 * Gets the ID of the robot associated with this tab
 *
 * @returns The ID associated with this ID
 */
unsigned char KorebotTab::getKorebotID() {
	return robot_id;
}
