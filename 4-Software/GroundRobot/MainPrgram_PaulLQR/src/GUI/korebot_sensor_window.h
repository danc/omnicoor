#ifndef _GUI_KOREBOT_SENSOR_WINDOW_H
#define _GUI_KOREBOT_SENSOR_WINDOW_H

#include <gtk/gtk.h>
#include <gtkdatabox.h>
#include <gtkdatabox_points.h>
#include <gtkdatabox_lines.h>
#include "common.h"
#include "Network/packets/base_packet.h"

#define MAX_SENSOR_DATAPOINTS 800

class KorebotSensorTab;
class KorebotTab;

class KorebotSensorWindow {
	private:
		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		// A widget to pack the data graph
		GtkWidget *vbox;

		// A widget to pause display output
		GtkWidget *pause_output;

		// Widgets to control the graph
		GdkColor color;
		GtkWidget *databox;
		GtkWidget *databox_table;
		GtkDataboxGraph *graph;
		GtkDataboxGraph *marker;

		// Widgets to show coordinate data
		GtkWidget *hbox;
		GtkWidget *xlabel;
		GtkWidget *xvalue;
		GtkWidget *ylabel;
		GtkWidget *yvalue;

		// The parent tab, used to send packets out on the network
		KorebotTab *parent;

		// The ID of the sensor displayed in this window
		short id;

		// The X and Y coordinates of the sensor data
		gfloat *points_x;
		gfloat *points_y;

		gfloat *points_x_temp;
		gfloat *points_y_temp;

		gfloat *markX;
		gfloat *markY;

		int window_width;
		int window_height;

		int current_read_index;

		int is_paused;

	public:

		/*
		 * Creates a default korebot sensor window
		 *
		 * @param parentTab The parent tab for this tab
		 * @param id The ID of the sensor
		 */
		KorebotSensorWindow(KorebotTab *parentTab, short id);

		/*
		 * Frees any resources used by this window
		 */
		~KorebotSensorWindow();

		/*
		 * Creates all the GTK elements needed by the window
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init(const char *sensor_name);

		/*
		 * Shows the window
		 */
		void start();

		/*
		 * Hides the window
		 */
		void stop();

		/*
		 * Gets the top-level container for the korebot sensor tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot sensor tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Adds a datapoint to the plot this window represents
		 *
		 * @param timestamp The timestamp of the sensor
		 * @param value The value of the sensor
		 */
		void addDataPoint(float timestamp, int value);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:
		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);

		/*
		 * Called when the mouse moves in our GUI
		 *
		 * @param widget The widget that caused the action
		 * @param event  Details about the mouse motion event
		 */
		void onMouseMotion(GtkWidget *widget, GdkEventMotion *event);

	friend void korebot_sensor_window_on_action(GtkWidget *widget, gpointer data);
	friend void korebot_sensor_window_mouse_motion(GtkWidget *widget, GdkEventMotion *event, gpointer data);
};

#endif

