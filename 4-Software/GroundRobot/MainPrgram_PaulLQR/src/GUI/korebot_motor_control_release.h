#ifndef _GUI_KOREBOT_MOTOR_CONTROL_RELEASE_H
#define _GUI_KOREBOT_MOTOR_CONTROL_RELEASE_H

#include <gtk/gtk.h>
#include "common.h"
#include "Network/packets/base_packet.h"

class KorebotTab;

class KorebotMotorControlRelease {
	private:
		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		// A button to release control of the motors, so that
		// another program can control them
		GtkWidget *release_control;

		// The parent tab, used to send packets out on the network
		KorebotTab *parent;
	public:

		/*
		 * Creates a default korebot manual motor control frame
		 *
		 * @param parentTab The parent tab for this tab
		 */
		KorebotMotorControlRelease(KorebotTab *parentTab);

		/*
		 * Releases any resources used by this frame
		 */
		~KorebotMotorControlRelease();

		/*
		 * Creates all the GTK elements needed by the frame
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the manual motor control frame.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot motor tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:
		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);

	friend void korebot_motor_control_release_on_action(GtkWidget *widget, gpointer data);

};

#endif


