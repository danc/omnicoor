#include <string.h>
#include "configuration.h"
#include "korebot_motor_control_manual.h"
#include "korebot_tab.h"
#include "Korebot/motor_controller_common.h"
#include "Network/packets/motor_control_manual.h"

void korebot_motor_control_manual_on_action(GtkWidget *widget, gpointer data);

/*
 * Creates a default korebot manual motor control frame
 *
 * @param parentTab The parent tab for this tab
 */
KorebotMotorControlManual::KorebotMotorControlManual(KorebotTab *parentTab) {
	container         = NULL;
	korebot_image     = NULL;
	motor_speed_front = NULL;
	motor_speed_rear  = NULL;
	motor_speed_left  = NULL;
	motor_speed_right = NULL;
	parent            = parentTab;
}

/*
 * Frees any resources used by this frame
 */
KorebotMotorControlManual::~KorebotMotorControlManual() {
}

/*
 * Creates all the GTK elements needed by the frame
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotMotorControlManual::init() {
	GtkAdjustment *adjustment = NULL;
	char image_path[256];
	int min_speed, max_speed;

	if (container != NULL) {
		return 1;
	}

	// Load the minimum & maximum motor speeds from the configuration file
	min_speed = Configuration::instance()->getValue("main_window.motor_controller.manual.min_speed", (int)-1);
	max_speed = Configuration::instance()->getValue("main_window.motor_controller.manual.max_speed", (int) 1);
	Configuration::instance()->getValue("main_window.motor_controller.manual.background_image", image_path, 256, "bottom.jpg");

	container = gtk_table_new(3, 5, FALSE);

	// Background Image
	korebot_image = gtk_image_new_from_file(image_path);
	gtk_table_attach_defaults(GTK_TABLE(container), korebot_image, 1, 2, 1, 2);

	// FRONT MOTOR
	motor_speed_front = gtk_hscale_new_with_range(min_speed, max_speed, 1);
	adjustment = gtk_range_get_adjustment(GTK_RANGE(motor_speed_front));
	gtk_adjustment_set_value(adjustment, 0);
	gtk_range_set_update_policy(GTK_RANGE(motor_speed_front), GTK_UPDATE_DELAYED);
	gtk_signal_connect(GTK_OBJECT(motor_speed_front), "value_changed", G_CALLBACK(korebot_motor_control_manual_on_action), (gpointer)this);
	gtk_table_attach_defaults(GTK_TABLE(container), motor_speed_front, 1, 2, 0, 1);

	// REAR MOTOR
	motor_speed_rear = gtk_hscale_new_with_range(min_speed, max_speed, 1);
	adjustment = gtk_range_get_adjustment(GTK_RANGE(motor_speed_rear));
	gtk_adjustment_set_value(adjustment, 0);
	gtk_range_set_update_policy(GTK_RANGE(motor_speed_rear), GTK_UPDATE_DELAYED);
	gtk_range_set_inverted(GTK_RANGE(motor_speed_rear), TRUE);
	gtk_signal_connect(GTK_OBJECT(motor_speed_rear), "value_changed", G_CALLBACK(korebot_motor_control_manual_on_action), (gpointer)this);
	gtk_table_attach_defaults(GTK_TABLE(container), motor_speed_rear, 1, 2, 2, 3);

	// LEFT MOTOR
	motor_speed_left = gtk_vscale_new_with_range(min_speed, max_speed, 1);
	adjustment = gtk_range_get_adjustment(GTK_RANGE(motor_speed_left));
	gtk_adjustment_set_value(adjustment, 0);
	gtk_range_set_update_policy(GTK_RANGE(motor_speed_left), GTK_UPDATE_DELAYED);
	gtk_signal_connect(GTK_OBJECT(motor_speed_left), "value_changed", G_CALLBACK(korebot_motor_control_manual_on_action), (gpointer)this);
	gtk_table_attach_defaults(GTK_TABLE(container), motor_speed_left, 0, 1, 1, 2);

	// RIGHT MOTOR
	motor_speed_right = gtk_vscale_new_with_range(min_speed, max_speed, 1);
	adjustment = gtk_range_get_adjustment(GTK_RANGE(motor_speed_right));
	gtk_adjustment_set_value(adjustment, 0);
	gtk_range_set_update_policy(GTK_RANGE(motor_speed_right), GTK_UPDATE_DELAYED);
	gtk_range_set_inverted(GTK_RANGE(motor_speed_right), TRUE);
	gtk_signal_connect(GTK_OBJECT(motor_speed_right), "value_changed", G_CALLBACK(korebot_motor_control_manual_on_action), (gpointer)this);
	gtk_table_attach_defaults(GTK_TABLE(container), motor_speed_right, 2, 3, 1, 2);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotMotorControlManual::start() {
	gtk_widget_show(korebot_image);
	gtk_widget_show(motor_speed_front);
	gtk_widget_show(motor_speed_rear);
	gtk_widget_show(motor_speed_left);
	gtk_widget_show(motor_speed_right);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotMotorControlManual::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the manual motor control frame.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot motor tab
 */
GtkWidget *KorebotMotorControlManual::getContainer() {
	return container;
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotMotorControlManual::onUserAction(GtkWidget *widget) {
	GtkAdjustment *adj;
	struct motor_control_manual_t parameters;

	if (parent != NULL) {
		memset(&parameters, 0, sizeof(struct motor_control_manual_t));

		adj = gtk_range_get_adjustment(GTK_RANGE(motor_speed_front));
		if (adj != NULL) {
			parameters.motor_speed_front = (int)adj->value;
		}

		adj = gtk_range_get_adjustment(GTK_RANGE(motor_speed_right));
		if (adj != NULL) {
			parameters.motor_speed_right = (int)adj->value;
		}

		adj = gtk_range_get_adjustment(GTK_RANGE(motor_speed_rear));
		if (adj != NULL) {
			parameters.motor_speed_rear = (int)adj->value;
		}

		adj = gtk_range_get_adjustment(GTK_RANGE(motor_speed_left));
		if (adj != NULL) {
			parameters.motor_speed_left = (int)adj->value;
		}

		MotorControlManual packet(parameters);
		parent->transmit(&packet);
	}
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotMotorControlManual::processPacket(BasePacket *packet) {
	MotorControl*mc = (MotorControl*)packet;
	struct motor_control_manual_t parameters;
	GtkAdjustment *adj;

	if (mc->getCommand() == MOTOR_CONTROL_COMMAND_STOP) {
		memset(&parameters, 0, sizeof(struct motor_control_manual_t));
	} else {
		memcpy(&parameters, ((MotorControlManual *)packet)->getMotorSpeeds(), sizeof(struct motor_control_manual_t));
	}

	adj = gtk_range_get_adjustment(GTK_RANGE(motor_speed_front));
	if (adj != NULL) {
		adj->value = parameters.motor_speed_front;
	}

	adj = gtk_range_get_adjustment(GTK_RANGE(motor_speed_right));
	if (adj != NULL) {
		adj->value = parameters.motor_speed_right;
	}

	adj = gtk_range_get_adjustment(GTK_RANGE(motor_speed_rear));
	if (adj != NULL) {
		adj->value = parameters.motor_speed_rear;
	}

	adj = gtk_range_get_adjustment(GTK_RANGE(motor_speed_left));
	if (adj != NULL) {
		adj->value = parameters.motor_speed_left;
	}

	// Set the widgets to redraw
	gtk_widget_queue_draw (GTK_WIDGET(motor_speed_front));
	gtk_widget_queue_draw (GTK_WIDGET(motor_speed_right));
	gtk_widget_queue_draw (GTK_WIDGET(motor_speed_rear));
	gtk_widget_queue_draw (GTK_WIDGET(motor_speed_left));
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotMotorControlManual::onIdle() {
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotMotorTab instance
 */
void korebot_motor_control_manual_on_action(GtkWidget *widget, gpointer data) {
	((KorebotMotorControlManual *)data)->onUserAction(widget);
}
