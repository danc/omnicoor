#ifndef _GUI_KOREBOT_CONFIGURATION_TAB_H
#define _GUI_KOREBOT_CONFIGURATION_TAB_H

#include <gtk/gtk.h>
#include "common.h"

class KorebotTab;

class KorebotConfigurationTab {
	private:
		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		GtkWidget *enable_profiler;
		GtkWidget *enable_bounds_limiting;
		GtkWidget *enable_logging;

		// The parent tab, used to send packets out on the network
		KorebotTab *parent;
	public:

		/*
		 * Creates a default korebot configuration tab
		 *
		 * @param parentTab The parent tab for this tab
		 */
		KorebotConfigurationTab(KorebotTab *parentTab);

		/*
		 * Frees any resources used by this korebot configuration tab
		 */
		~KorebotConfigurationTab();

		/*
		 * Creates all the GTK elements needed by the korebot configuration tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the korebot configuration tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot configuration tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:
		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);

	friend void korebot_configuration_on_action(GtkWidget *widget, gpointer data);
};

#endif
