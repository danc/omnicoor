#ifndef _GUI_LOCALIZATION_TAB_H
#define _GUI_LOCALIZATION_TAB_H

#include <gtk/gtk.h>
#include "common.h"
#include "Network/packets/base_packet.h"
#include "Korebot/localization_system_common.h"

class LocalizationTab {
	private:
		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		GtkWidget *display;

		int min_x, min_y, max_x, max_y, robot_radius;

		char num_robots;
		char num_objects;
	public:

		/*
		 * Creates a default localization tab
		 */
		LocalizationTab();

		/*
		 * Frees any resources used by this localization tab
		 */
		~LocalizationTab();

		/*
		 * Creates all the GTK elements needed by the localization tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the localization tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the localization tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

};

#endif
