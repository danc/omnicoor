#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "configuration.h"
#include "Network/network.h"
#include "Network/tcp_connection.h"
#include "Network/packet_handler.h"
#include "Network/packets/all_packets.h"

int main(int argc, char **argv) {
	char id = 0;
	char buf[80];
	network_config_t config;
	RobotIdentification rid;
	TCP_Connection *con;

	int delay = 10;
	if (argc >= 2) {
		delay = atoi(argv[1]);
	}
	if (argc >= 3) {
		id = atoi(argv[2]);
	}

	Configuration::instance()->load("gui.cfg");

	Configuration::instance()->getValue("network.server_ip_address", config.ip_addr, IP_ADDR_MAX_LEN, "127.0.0.1");
	config.port = Configuration::instance()->getValue("network.server_port", 65530);

	con = new TCP_Connection();

	while (1) {
		id++;

		sprintf(buf, "Korebot %d", id);

		rid.setRobotID(id);
		rid.setRobotName(buf);

		if (con->start(config) == 1) {
			printf("Connected to server\n");
			sleep(2);

			printf("Sending robot ID %d\n", id);
			con->transmit(&rid);

			sleep(delay);
			con->stop();
		}
		sleep(10);
	}

	return 0;
}
