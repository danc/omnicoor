#include <string.h>
#include <gtkdatabox_markers.h>
#include <glib/gprintf.h>
#include "configuration.h"
#include "korebot_sensor_window.h"
#include "korebot_sensor_tab.h"
#include "korebot_tab.h"
#include "Network/packets/all_packets.h"
#include "profiler.h"

void korebot_sensor_window_on_action(GtkWidget *widget, gpointer data);
void korebot_sensor_window_mouse_motion(GtkWidget *widget, GdkEventMotion *event, gpointer data);

/*
 * Creates a default korebot sensor tab
 *
 * @param parentWindow The parent tab for this tab
 * @param id The ID of the sensor
 */
KorebotSensorWindow::KorebotSensorWindow(KorebotTab *parentTab, short id) {
	container     = NULL;
	vbox          = NULL;
	databox       = NULL;
	databox_table = NULL;
	graph         = NULL;
	marker        = NULL;
	points_x      = NULL;
	points_y      = NULL;
	points_x_temp = NULL;
	points_y_temp = NULL;
	hbox          = NULL;
	xlabel        = NULL;
	xvalue        = NULL;
	ylabel        = NULL;
	yvalue        = NULL;


	parent = parentTab;
	this->id = id;

	color.red   = 0;
	color.green = 0;
	color.blue  = 0;

	window_width  = 0;
	window_height = 0;

	current_read_index = 0;

	is_paused = 0;
}

/*
 * Frees any resources used by this korebot sensor tab
 */
KorebotSensorWindow::~KorebotSensorWindow() {
	// Tell the korebot to stop sending this sensor information
	SensorRequest request(id, 0);
	parent->transmit(&request);
}

/*
 * Creates all the GTK elements needed by the korebot sensor tab
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotSensorWindow::init(const char *sensor_name) {
	char buffer[MAX_SENSOR_NAME_LEN + MAX_ROBOT_NAME_LEN + 2];

	if (container != NULL) {
		return 1;
	}

	window_width  = Configuration::instance()->getValue("main_window.sensor_dialog.width",  (int)300);
	window_height = Configuration::instance()->getValue("main_window.sensor_dialog.height", (int)200);

	color.red   = 0;
	color.green = 0;
	color.blue  = 0;

	container = gtk_dialog_new();
	gtk_widget_set_size_request (container, window_width, window_height);

	sprintf(buffer, "%s: %s", parent->getRobotName(), sensor_name);
	gtk_window_set_title(GTK_WINDOW(container), buffer);

	gtk_databox_create_box_with_scrollbars_and_rulers (&databox, &databox_table,
										TRUE, TRUE, TRUE, TRUE);
	gtk_box_pack_start (GTK_BOX(GTK_DIALOG(container)->vbox), databox_table, TRUE, TRUE, 0);

	// These values are managed by the GTK engine
	points_x      = g_new0 (gfloat, MAX_SENSOR_DATAPOINTS);
	points_y      = g_new0 (gfloat, MAX_SENSOR_DATAPOINTS);
	points_x_temp = g_new0 (gfloat, MAX_SENSOR_DATAPOINTS);
	points_y_temp = g_new0 (gfloat, MAX_SENSOR_DATAPOINTS);

	memset(points_x, 0, sizeof(gfloat) * MAX_SENSOR_DATAPOINTS);
	memset(points_y, 0, sizeof(gfloat) * MAX_SENSOR_DATAPOINTS);
	memset(points_x_temp, 0, sizeof(gfloat) * MAX_SENSOR_DATAPOINTS);
	memset(points_y_temp, 0, sizeof(gfloat) * MAX_SENSOR_DATAPOINTS);

	markX = g_new0 (gfloat, 1);
	markY = g_new0 (gfloat, 1);

	points_y[0] = 256;
	points_y[1] = -256;
	points_x[0] = ProfileSample::getTime() - 60.0f;
	points_x[1] = ProfileSample::getTime();

	graph = gtk_databox_lines_new (MAX_SENSOR_DATAPOINTS, points_x, points_y, &color, 1);
	gtk_databox_graph_add (GTK_DATABOX(databox), graph);
	g_signal_connect(G_OBJECT (databox),
			     "motion_notify_event",
			     G_CALLBACK (korebot_sensor_window_mouse_motion), (gpointer)this);

	color.red   = 0xFFFF;
	color.green = 0;
	color.blue  = 0;

	marker = gtk_databox_markers_new(1, markX, markY, &color, 7,
					GTK_DATABOX_MARKERS_TRIANGLE);
	gtk_databox_markers_set_position (GTK_DATABOX_MARKERS (marker), 0,
					GTK_DATABOX_MARKERS_S);
	gtk_databox_graph_add (GTK_DATABOX (databox), marker);

	pause_output = gtk_check_button_new_with_label("Pause Output");
	gtk_signal_connect(GTK_OBJECT(pause_output), "toggled", G_CALLBACK(korebot_sensor_window_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(container)->vbox), pause_output, FALSE, FALSE, 5);

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(container)->vbox), hbox, FALSE, FALSE, 5);

	xlabel = gtk_label_new("X:");
	gtk_box_pack_start(GTK_BOX(hbox), xlabel, FALSE, FALSE, 5);

	xvalue = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox), xvalue, FALSE, FALSE, 5);

	ylabel = gtk_label_new("Y:");
	gtk_box_pack_start(GTK_BOX(hbox), ylabel, FALSE, FALSE, 5);

	yvalue = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox), yvalue, FALSE, FALSE, 5);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotSensorWindow::start() {
	SensorRequest request(id, 1);

	parent->transmit(&request);

	gtk_widget_show_all(container);
}

/*
 * Hides the tab
 */
void KorebotSensorWindow::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the korebot sensor tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot sensor tab
 */
GtkWidget *KorebotSensorWindow::getContainer() {
	return container;
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotSensorWindow::processPacket(BasePacket *packet) {
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotSensorWindow::onUserAction(GtkWidget *widget) {
	if (widget == pause_output) {
		is_paused = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pause_output));
	}
}

/*
 * Called when the mouse moves in our GUI
 *
 * @param widget The widget that caused the action
 * @param event  Details about the mouse motion event
 */
void KorebotSensorWindow::onMouseMotion(GtkWidget *widget, GdkEventMotion *event) {
	gfloat x, y;
	gchar str[40];
	int i, low, high;

	// We only look at this if the GUI is paused (Otherwise the data will be overwritten
	// in the idle function)
	if (is_paused) {
		x = gtk_databox_pixel_to_value_x (GTK_DATABOX(databox), (gint16)event->x);
		y = gtk_databox_pixel_to_value_y (GTK_DATABOX(databox), (gint16)event->y);


		// Perform a binary search to find the closest data point
		low = 0;
		high = current_read_index-1;
		i = 0;
		while (low <= high) {
			i = low + (high - low) / 2;
			if (points_x[i] > x) {
				high = i-1;
			} else if (points_x[i] < x) {
				low = i+1;
			} else {
				break;
			}
		}

		g_sprintf (str, "%f", points_x[i]);
		gtk_entry_set_text (GTK_ENTRY (xvalue), str);

		g_sprintf (str, "%f", points_y[i]);
		gtk_entry_set_text (GTK_ENTRY (yvalue), str);

		markX[0] = points_x[i];
		markY[0] = points_y[i];

		gtk_widget_queue_draw (GTK_WIDGET(databox));
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotSensorWindow::onIdle() {
	gchar str[40];

	if (!is_paused && points_x != NULL && points_y != NULL && points_x_temp != NULL && points_y_temp != NULL) {
		memcpy(points_x, points_x_temp, sizeof(gfloat) * MAX_SENSOR_DATAPOINTS);
		memcpy(points_y, points_y_temp, sizeof(gfloat) * MAX_SENSOR_DATAPOINTS);
		gtk_databox_auto_rescale (GTK_DATABOX (databox), 0.05);

		markX[0] = points_x[MAX_SENSOR_DATAPOINTS-1];
		markY[0] = points_y[MAX_SENSOR_DATAPOINTS-1];

		g_sprintf(str, "%f", markX[0]);
		gtk_entry_set_text(GTK_ENTRY(xvalue), str);

		g_sprintf(str, "%f", markY[0]);
		gtk_entry_set_text(GTK_ENTRY(yvalue), str);

		gtk_widget_queue_draw (GTK_WIDGET(databox));
	}
}

/*
 * Adds a datapoint to the plot this window represents
 *
 * @param timestamp The timestamp of the sensor
 * @param value The value of the sensor
 */
void KorebotSensorWindow::addDataPoint(float timestamp, int value) {
	if (points_x_temp != NULL && points_y_temp != NULL) {

		if (current_read_index < MAX_SENSOR_DATAPOINTS) {
			points_x_temp[current_read_index] = timestamp;
			points_y_temp[current_read_index] = value;

			current_read_index++;

			// So that we don't skew the data, the first datapoint is copied across the entire graph
			for (int i=current_read_index; i<MAX_SENSOR_DATAPOINTS; i++) {
				points_x_temp[i] = timestamp;
				points_y_temp[i] = value;
			}
		} else {
			memcpy(&points_x_temp[0], &points_x_temp[1], sizeof(gfloat) * (MAX_SENSOR_DATAPOINTS-1));
			memcpy(&points_y_temp[0], &points_y_temp[1], sizeof(gfloat) * (MAX_SENSOR_DATAPOINTS-1));
			points_x_temp[MAX_SENSOR_DATAPOINTS-1] = timestamp;
			points_y_temp[MAX_SENSOR_DATAPOINTS-1] = value;
		}
	}
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the korebotSensorWindow instance
 */
void korebot_sensor_window_on_action(GtkWidget *widget, gpointer data) {
	((KorebotSensorWindow *)data)->onUserAction(widget);
}


/*
 * Proxy function for when the user moves the mouse in the GUI
 *
 * @param widget The widget that caused the action
 * @param event  Details about the mouse motion event
 * @param data   A pointer to the KorebotSensorWindow instance
 */
void korebot_sensor_window_mouse_motion(GtkWidget *widget, GdkEventMotion *event, gpointer data) {
	((KorebotSensorWindow *)data)->onMouseMotion(widget, event);
}
