#ifndef _GUI_KOREBOT_TAB_H
#define _GUI_KOREBOT_TAB_H

#include <gtk/gtk.h>
#include "common.h"
#include "korebot_motor_tab.h"
#include "korebot_sensor_tab.h"
#include "korebot_configuration_tab.h"
#include "korebot_profiler_tab.h"
#include "korebot_external_input_tab.h"
#include "Network/packet_handler.h"

class MainGUI;

class KorebotTab : public PacketHandler {
	private:
		// The robot ID assocaited with this tab
		unsigned char robot_id;

		// The name of the robot
		char robot_name[MAX_ROBOT_NAME_LENGTH];

		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		// Tab to control the motors
		KorebotMotorTab *motor_control;

		// Tab to view sensor data
		KorebotSensorTab *sensors;

		// Tab to view configuration for korebot
		KorebotConfigurationTab *config;

		KorebotExternalInputTab *external_input;

		// Tab for viewing profile information
		KorebotProfilerTab *profile_tab;

		MainGUI *parent;
	public:

		/*
		 * Creates a default korebot tab
		 */
		KorebotTab(MainGUI *parent, unsigned char robotID, const char *robotName);

		/*
		 * Frees any resources used by this korebot tab
		 */
		~KorebotTab();

		/*
		 * Creates all the GTK elements needed by the korebot tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Sends a packet over the connection
		 *
		 * @param packet The packet to send
		 */
		void transmit(BasePacket *packet);

		/*
		 * When a packet is received on the network, this method is called to
		 * process the packet.
		 *
		 * @param packet The packet that was received from the network
		 */
		void processPacket(TCP_Connection *con, BasePacket *packet);

		/*
		 * When a packet is received on the network, this method is called to
		 * process the packet.
		 *
		 * @param connection The source connection
		 * @param packet The packet that was received from the network
		 */
		void processPacket(UDP_Connection *connection, BasePacket *packet);

		/*
		 * Gets the top-level container for the korebot tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot tab
		 */
		GtkWidget *getContainer();

		/*
		 * Gets the name of the robot associated with this tab
		 *
		 * @returns A pointer to the name of the robot
		 */
		const char *getRobotName();

		/*
		 * Gets the ID of the robot associated with this tab
		 *
		 * @returns The ID associated with this ID
		 */
		unsigned char getKorebotID();

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

};

#endif

