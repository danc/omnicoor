#include <string.h>
#include "korebot_external_input_tab.h"
#include "main_gui.h"
#include "Network/packets/all_packets.h"

void korebot_external_input_tab_on_action(GtkWidget *widget, gpointer data);

/*
 * Creates a default korebot external_input tab
 *
 * @param parentTab The parent tab for this tab
 */
KorebotExternalInputTab::KorebotExternalInputTab(KorebotTab *parentTab) {
	container             = NULL;
	external_input_select = NULL;
	input_xbox            = NULL;

	parent = parentTab;
}

/*
 * Frees any resources used by this korebot external_input tab
 */
KorebotExternalInputTab::~KorebotExternalInputTab() {
	if (input_xbox != NULL) {
		delete input_xbox;
	}
}

/*
 * Creates all the GTK elements needed by the korebot external_input tab
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotExternalInputTab::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_table_new(7, 1, FALSE);

	// ExternalInput selection
	external_input_select = gtk_combo_box_new_text();
	// If you change the order of the following items, make sure to change the order
	// of the order of the defines in korebot_external_input_tab.h accordingly
	gtk_combo_box_append_text(GTK_COMBO_BOX(external_input_select), "XBox External Input");
	gtk_combo_box_set_active(GTK_COMBO_BOX(external_input_select), EXTERNAL_INPUT_XBOX);
	g_signal_connect(G_OBJECT(external_input_select), "changed", G_CALLBACK(korebot_external_input_tab_on_action), (gpointer)this);
	gtk_table_attach(GTK_TABLE(container), external_input_select, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	input_xbox = new KorebotExternalInputXBox(parent);
	input_xbox->init();
	gtk_table_attach_defaults(GTK_TABLE(container), input_xbox->getContainer(), 0, 1, 6, 7);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotExternalInputTab::start() {
	onExternalInputSelect();

	gtk_widget_show(external_input_select);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotExternalInputTab::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the korebot external_input tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot external_input tab
 */
GtkWidget *KorebotExternalInputTab::getContainer() {
	return container;
}


/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotExternalInputTab::onUserAction(GtkWidget *widget) {
	if (widget == external_input_select) {
		onExternalInputSelect();
	}
}

/*
 * When the user selects a new external_input method, this method is
 * called.
 */
void KorebotExternalInputTab::onExternalInputSelect() {
	gint active_external_input_method = gtk_combo_box_get_active(GTK_COMBO_BOX(external_input_select));

	// Enable the corresponding external_input method
	switch(active_external_input_method) {
		case EXTERNAL_INPUT_XBOX:
			input_xbox->start();
			break;
		default:
			input_xbox->stop();
			break;
	};
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotExternalInputTab::processPacket(BasePacket *packet) {
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotExternalInputTab::onIdle() {
	input_xbox->onIdle();
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotExternalInputTab instance
 */
void korebot_external_input_tab_on_action(GtkWidget *widget, gpointer data) {
	((KorebotExternalInputTab *)data)->onUserAction(widget);
}

