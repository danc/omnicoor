#include <string.h>
#include "configuration.h"
#include "korebot_motor_control_speed.h"
#include "korebot_tab.h"
#include "Korebot/motor_controller_common.h"
#include "Network/packets/motor_control_speed.h"

void korebot_motor_control_speed_on_action(GtkWidget *widget, gpointer data);

/*
 * Creates a default korebot speed motor control pane
 *
 * @param parentTab The parent tab for this tab
 */
KorebotMotorControlSpeed::KorebotMotorControlSpeed(KorebotTab *parentTab) {
	container              = NULL;
	heading                = NULL;
	linear_speed           = NULL;
	rotational_speed       = NULL;
	linear_speed_label     = NULL;
	rotational_speed_label = NULL;
	go_button	       = NULL;
	parent                 = parentTab;
}

/*
 * Frees any resources used by this frame
 */
KorebotMotorControlSpeed::~KorebotMotorControlSpeed() {
}

/*
 * Creates all the GTK elements needed by the frame
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotMotorControlSpeed::init() {
	GtkAdjustment *adjustment;
	int linear_speed_max, linear_speed_min;
	int rotational_speed_max, rotational_speed_min;

	if (container != NULL) {
		return 1;
	}

	linear_speed_min = Configuration::instance()->getValue("main_window.motor_controller.speed.linear_speed_min", (int)-1);
	linear_speed_max = Configuration::instance()->getValue("main_window.motor_controller.speed.linear_speed_max", (int) 1);
	rotational_speed_min = Configuration::instance()->getValue("main_window.motor_controller.speed.rotational_speed_min", (int)-1);
	rotational_speed_max = Configuration::instance()->getValue("main_window.motor_controller.speed.rotational_speed_max", (int) 1);

	container = gtk_table_new(2, 5, FALSE);

	go_button = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(go_button), gtk_label_new("Send Speeds"));
	g_signal_connect(G_OBJECT(go_button), "clicked", G_CALLBACK(korebot_motor_control_speed_on_action), (gpointer)this);
	gtk_table_attach(GTK_TABLE(container), go_button, 1, 2, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	/* Heading */
	adjustment = (GtkAdjustment *)gtk_adjustment_new(0, 0, 360, 1, 0, 0);
	heading = gtk_dial_new(adjustment);
	gtk_dial_set_update_policy(GTK_DIAL(heading), GTK_UPDATE_DELAYED);
	//g_signal_connect(GTK_OBJECT(adjustment), "value_changed", G_CALLBACK(korebot_motor_control_speed_on_action), (gpointer)this);
	gtk_table_attach_defaults(GTK_TABLE(container), heading, 1, 2, 1, 5);

	/* Linear Speed */
	linear_speed = gtk_hscale_new_with_range(linear_speed_min, linear_speed_max, 1);
	adjustment = gtk_range_get_adjustment(GTK_RANGE(linear_speed));
	gtk_adjustment_set_value(adjustment, 0);
	gtk_range_set_update_policy(GTK_RANGE(linear_speed), GTK_UPDATE_DELAYED);
	//gtk_signal_connect(GTK_OBJECT(linear_speed), "value_changed", G_CALLBACK(korebot_motor_control_speed_on_action), (gpointer)this);
	gtk_table_attach_defaults(GTK_TABLE(container), linear_speed, 0, 1, 0, 1);

	linear_speed_label = gtk_label_new("Linear Speed");
	gtk_table_attach_defaults(GTK_TABLE(container), linear_speed_label, 0, 1, 1, 2);

	/* rotational Speed */
	rotational_speed = gtk_hscale_new_with_range(rotational_speed_min, rotational_speed_max, 1);
	adjustment = gtk_range_get_adjustment(GTK_RANGE(rotational_speed));
	gtk_adjustment_set_value(adjustment, 0);
	gtk_range_set_update_policy(GTK_RANGE(rotational_speed), GTK_UPDATE_DELAYED);
	//gtk_signal_connect(GTK_OBJECT(rotational_speed), "value_changed", G_CALLBACK(korebot_motor_control_speed_on_action), (gpointer)this);
	gtk_table_attach_defaults(GTK_TABLE(container), rotational_speed, 0, 1, 2, 3);

	rotational_speed_label = gtk_label_new("Rotational Speed");
	gtk_table_attach_defaults(GTK_TABLE(container), rotational_speed_label, 0, 1, 3, 4);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotMotorControlSpeed::start() {
	gtk_widget_show(heading);
	gtk_widget_show(linear_speed);
	gtk_widget_show(rotational_speed);
	gtk_widget_show_all(go_button);

	gtk_widget_show(linear_speed_label);
	gtk_widget_show(rotational_speed_label);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotMotorControlSpeed::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the speed motor control frame.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the speed motor controller frame
 */
GtkWidget *KorebotMotorControlSpeed::getContainer() {
	return container;
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotMotorControlSpeed::onUserAction(GtkWidget *widget) {
	GtkAdjustment *adj;
	struct motor_control_speed_t parameters;

	if (parent != NULL) {
		memset(&parameters, 0, sizeof(struct motor_control_speed_t));

		adj = gtk_dial_get_adjustment(GTK_DIAL(heading));
		if (adj != NULL) {
			parameters.heading = (int)adj->value;
		}

		adj = gtk_range_get_adjustment(GTK_RANGE(linear_speed));
		if (adj != NULL) {
			parameters.linear_speed = (int)adj->value;
		}

		adj = gtk_range_get_adjustment(GTK_RANGE(rotational_speed));
		if (adj != NULL) {
			parameters.rotational_velocity = (int)adj->value;
		}

		MotorControlSpeed packet(parameters);
		parent->transmit(&packet);
	}
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotMotorControlSpeed::processPacket(BasePacket *packet) {
	MotorControl*mc = (MotorControl*)packet;
	GtkAdjustment *adj;
	struct motor_control_speed_t parameters;

	if (mc->getCommand() == MOTOR_CONTROL_COMMAND_STOP) {
		parameters.heading = 0;
		parameters.linear_speed = 0;
		parameters.rotational_velocity = 0;
	} else {
		memcpy(&parameters, ((MotorControlSpeed *)packet)->getMotorSpeeds(), sizeof(struct motor_control_speed_t));
	}

	adj = gtk_dial_get_adjustment(GTK_DIAL(heading));
	if (adj != NULL) {
		adj->value = parameters.heading;
		gtk_signal_emit_by_name (GTK_OBJECT (adj), "value_changed");
	}

	adj = gtk_range_get_adjustment(GTK_RANGE(linear_speed));
	if (adj != NULL) {
		adj->value = parameters.linear_speed;
	}

	adj = gtk_range_get_adjustment(GTK_RANGE(rotational_speed));
	if (adj != NULL) {
		adj->value = parameters.rotational_velocity;
	}

	gtk_widget_queue_draw (GTK_WIDGET(heading));
	gtk_widget_queue_draw (GTK_WIDGET(linear_speed));
	gtk_widget_queue_draw (GTK_WIDGET(rotational_speed));
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotMotorControlSpeed::onIdle() {
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotMotorTab instance
 */
void korebot_motor_control_speed_on_action(GtkWidget *widget, gpointer data) {
	((KorebotMotorControlSpeed *)data)->onUserAction(widget);
}
