#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "korebot_external_input_xbox.h"
#include "korebot_tab.h"
#include "Network/packets/xbox_sensor.h"

void korebot_external_input_xbox_on_action(GtkWidget *widget, gpointer data);

int KorebotExternalInputXBox::num_xbox_instances = 0;
int KorebotExternalInputXBox::num_joysticks = -1;
struct joystick_t *KorebotExternalInputXBox::joysticks = NULL;

/*
 * Creates a default korebot XBox motor control frame
 *
 * @param parentTab The parent tab for this tab
 */
KorebotExternalInputXBox::KorebotExternalInputXBox(KorebotTab *parentTab) {
	container     = NULL;
	joystick_list = NULL;
	parent        = parentTab;

	num_xbox_instances++;
	selected_joystick = -1;
}

/*
 * Frees any resources used by this frame
 */
KorebotExternalInputXBox::~KorebotExternalInputXBox() {
	int i;
	num_xbox_instances--;

	// Free the list of joysticks if there are no instances of the XBox GUI open anymore
	if (num_xbox_instances == 0) {
		num_joysticks = -1;
		if (joysticks != NULL) {
			for (i=0; i<num_joysticks; i++) {
				if (joysticks[i].fd != 0) {
					close(joysticks[i].fd);
					joysticks[i].fd = 0;
					joysticks[i].num_in_use = 0;
				}
			}

			free(joysticks);
			joysticks = NULL;
		}
	}
}

/*
 * Creates all the GTK elements needed by the frame
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotExternalInputXBox::init() {
	if (container != NULL) {
		return 1;
	}

	// This is the first XBox control that has been initialized.
	// Fill in the list of available joystick devices
	if (num_joysticks == -1) {
		const char joystick_directory[] = "/dev/input";
		const char joystick_prefix[] = "js";
		DIR *dir;
		struct dirent *entry;

		num_joysticks = 0;

		// First get the number of joysticks present on the system
		dir = opendir(joystick_directory);
		if (dir == NULL) {
			fprintf(stderr, "Error: Could not retrieve list of available joysticks.\n");
		} else {
			while ((entry = readdir (dir))) {
				// Joysticks start with "js" in the name
				if (strcasestr(entry->d_name, joystick_prefix) != NULL) {
					num_joysticks++;
				}
			}
			closedir(dir);
		}

		// Now get details about the joysticks
		dir = opendir(joystick_directory);
		if (dir == NULL) {
			num_joysticks = 0;
		} else {
			int i = 0;

			// Create the array of joysticks
			joysticks = (struct joystick_t *)malloc(sizeof(struct joystick_t) * num_joysticks);
			memset(joysticks, 0, sizeof(struct joystick_t) * num_joysticks);

			while ((entry = readdir(dir)) && i < num_joysticks) {
				if (strcasestr(entry->d_name, joystick_prefix) != NULL) {
					snprintf(joysticks[i].device, MAX_DEVICE_LEN, "%s/%s", joystick_directory, entry->d_name);
					i++;
				}
			}
			closedir(dir);
		}
	}

	container = gtk_vbox_new(FALSE, 0);

	joystick_list = gtk_combo_box_new_text();

	gtk_combo_box_append_text(GTK_COMBO_BOX(joystick_list), "No Joystick");
	for (int i=0; i<num_joysticks; i++) {
		gtk_combo_box_append_text(GTK_COMBO_BOX(joystick_list), joysticks[i].device);
	}

	g_signal_connect(G_OBJECT(joystick_list), "changed", G_CALLBACK(korebot_external_input_xbox_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(container), joystick_list, FALSE, FALSE, 20);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotExternalInputXBox::start() {
	gtk_widget_show_all(joystick_list);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotExternalInputXBox::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the XBox motor control frame.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot motor tab
 */
GtkWidget *KorebotExternalInputXBox::getContainer() {
	return container;
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotExternalInputXBox::processPacket(BasePacket *packet) {
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotExternalInputXBox::onUserAction(GtkWidget *widget) {
	if (widget == joystick_list) {
		// Decrease the use count of the currently-selected joystick
		if (selected_joystick >= 0) {
			joysticks[selected_joystick].num_in_use--;
		}

		// Get the now-selected joystick
		selected_joystick = gtk_combo_box_get_active(GTK_COMBO_BOX(joystick_list)) - 1;
		if (selected_joystick < 0 || selected_joystick > num_joysticks) {
			// Invalid selection
			selected_joystick = -1;
			return;
		}

		// Open the device if it isn't already open, and update the number of tabs listening to it
		joysticks[selected_joystick].num_in_use++;
		if (joysticks[selected_joystick].fd == 0) {
			joysticks[selected_joystick].fd = open(joysticks[selected_joystick].device, O_RDONLY | O_NONBLOCK);
			if (joysticks[selected_joystick].fd == -1) {
				perror("KorebotExternalInputXBox::onUserAction: Could not open joystick device");
				joysticks[selected_joystick].num_in_use--;
				selected_joystick = -1;
			}
		}
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotExternalInputXBox::onIdle() {
	struct js_event_t event;
	int i;

	if (parent->getKorebotID() == KOREBOT_ID_BROADCAST) {
		for (i=0; i<num_joysticks; i++) {
			joysticks[i].changed = 0;
			if (joysticks[i].fd != 0 && joysticks[i].num_in_use > 0) {
				while (read(joysticks[i].fd, &event, sizeof(struct js_event_t)) != -1) {
					if (event.type == JS_EVENT_AXIS) {
						switch(event.number) {
							case 0:		// LEFT ANALOG X
								joysticks[i].state.left_analog_x = event.value;
								break;
							case 1:		// LEFT ANALOG Y
								joysticks[i].state.left_analog_y = event.value;
								break;
							case 2:		// LEFT TRIGGER
								joysticks[i].state.left_trigger = event.value;
								break;
							case 3:		// RIGHT X
								joysticks[i].state.right_analog_x = event.value;
								break;
							case 4:		// RIGHT Y
								joysticks[i].state.right_analog_y = event.value;
								break;
							case 5:		// LEFT TRIGGER
								joysticks[i].state.right_trigger = event.value;
								break;
							case 6:		// D-PAD LEFT / RIGHT
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_RIGHT) & ((~XBOX_BUTTON_MASK_RIGHT) | ((event.value > 0) ? XBOX_BUTTON_MASK_RIGHT : 0));
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_LEFT) & ((~XBOX_BUTTON_MASK_LEFT ) | ((event.value < 0) ? XBOX_BUTTON_MASK_LEFT  : 0));
								break;
							case 7:		// D-PAD UP / DOWN
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_UP) & ((~XBOX_BUTTON_MASK_UP  ) | ((event.value < 0) ? XBOX_BUTTON_MASK_UP   : 0));
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_DOWN) & ((~XBOX_BUTTON_MASK_DOWN) | ((event.value > 0) ? XBOX_BUTTON_MASK_DOWN : 0));
								break;
						}
					} else if (event.type == JS_EVENT_BUTTON) {
						switch(event.number) {
							case 0:		// A BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_A) & ((~XBOX_BUTTON_MASK_A) | ((event.value > 0) ? XBOX_BUTTON_MASK_A : 0));
								break;
							case 1:		// B BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_B) & ((~XBOX_BUTTON_MASK_B) | ((event.value > 0) ? XBOX_BUTTON_MASK_B : 0));
								break;
							case 2:		// BLACK BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_BLACK) & ((~XBOX_BUTTON_MASK_BLACK) | ((event.value > 0) ? XBOX_BUTTON_MASK_BLACK : 0));
								break;
							case 3:		// X BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_X) & ((~XBOX_BUTTON_MASK_X) | ((event.value > 0) ? XBOX_BUTTON_MASK_X : 0));
								break;
							case 4:		// Y BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_Y) & ((~XBOX_BUTTON_MASK_Y) | ((event.value > 0) ? XBOX_BUTTON_MASK_Y : 0));
								break;
							case 5:		// WHITE BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_WHITE) & ((~XBOX_BUTTON_MASK_WHITE) | ((event.value > 0) ? XBOX_BUTTON_MASK_WHITE : 0));
								break;
							case 6:		// START BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_START) & ((~XBOX_BUTTON_MASK_START) | ((event.value > 0) ? XBOX_BUTTON_MASK_START : 0));
								break;
							case 7:		// LEFT ANALAG STICK BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_LSTICK) & ((~XBOX_BUTTON_MASK_LSTICK) | ((event.value > 0) ? XBOX_BUTTON_MASK_LSTICK : 0));
								break;
							case 8:		// RIGHT ANALAG STICK BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_RSTICK) & ((~XBOX_BUTTON_MASK_RSTICK) | ((event.value > 0) ? XBOX_BUTTON_MASK_RSTICK : 0));
								break;
							case 9:		// BACK BUTTON
								joysticks[i].state.buttons = (joysticks[i].state.buttons | XBOX_BUTTON_MASK_BACK) & ((~XBOX_BUTTON_MASK_BACK) | ((event.value > 0) ? XBOX_BUTTON_MASK_BACK : 0));
								break;
						}
					}

					joysticks[i].changed = 1;
				}

				if (errno != EAGAIN) {
					perror("KorebotExternalInputXBox::onIdle: Error: Could not read from device");
				}
			}
		}
	}

	if (selected_joystick >= 0 && selected_joystick < num_joysticks && joysticks[selected_joystick].changed) {
		XboxSensor sensor(joysticks[selected_joystick].state);
		parent->transmit(&sensor);
	}
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotMotorTab instance
 */
void korebot_external_input_xbox_on_action(GtkWidget *widget, gpointer data) {
	((KorebotExternalInputXBox *)data)->onUserAction(widget);
}
