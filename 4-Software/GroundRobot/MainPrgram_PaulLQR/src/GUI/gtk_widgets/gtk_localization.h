#ifndef __GTK_LOCALIZATION_H__
#define __GTK_LOCALIZATION_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include "Korebot/localization_system_common.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_LOCALIZATION(obj)          GTK_CHECK_CAST (obj, gtk_localization_get_type (), GtkLocalization)
#define GTK_LOCALIZATION_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_localization_get_type (), GtkLocalizationClass)
#define GTK_IS_LOCALIZATION(obj)       GTK_CHECK_TYPE (obj, gtk_localization_get_type ())


typedef struct _GtkLocalization        GtkLocalization;
typedef struct _GtkLocalizationClass   GtkLocalizationClass;

struct _GtkLocalization
{
	GtkWidget widget;

	gint min_x, max_x, min_y, max_y;
	gint robot_radius;
	gint object_radius;

	char num_robots;
	char num_objects;

	struct localization_object_t objects[MAX_OBJECTS];
	struct localization_robot_t  robots[MAX_ROBOTS];
};

struct _GtkLocalizationClass
{
	GtkWidgetClass parent_class;
};


GtkWidget*     gtk_localization_new                    (gint min_x, gint min_y, gint max_x, gint max_y, gint robot_radius);
GtkType        gtk_localization_get_type               (void);

void           gtk_localization_set_robots             (GtkLocalization *localization, char num_robots,  const struct localization_robot_t  *robots);
void           gtk_localization_set_objects            (GtkLocalization *localization, char num_objects, const struct localization_object_t *objects);
#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_LOCALIZATION_H__ */
