#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <string.h>
#include <math.h>
#include "gtk_localization.h"
#include "Korebot/localization_system_common.h"

#define LOCALIZATION_DEFAULT_SIZE 100

/* Forward declarations */
static void gtk_localization_class_init (GtkLocalizationClass *class);
static void gtk_localization_init (GtkLocalization *localization);
static void gtk_localization_destroy (GtkObject *object);
static void gtk_localization_realize (GtkWidget *widget);
static void gtk_localization_size_request (GtkWidget *widget, GtkRequisition *requisition);
static void gtk_localization_size_allocate (GtkWidget *widget, GtkAllocation *allocation);
static gboolean gtk_localization_expose( GtkWidget *widget, GdkEventExpose *event );
static gboolean gtk_localization_button_press( GtkWidget *widget, GdkEventButton *event );
static gboolean gtk_localization_button_release( GtkWidget *widget, GdkEventButton *event );
static gboolean gtk_localization_motion_notify( GtkWidget *widget, GdkEventMotion *event );
//static void gtk_localization_update_mouse (GtkLocalization *localization, gint x, gint y);
//static void gtk_localization_update (GtkLocalization *localization);

/* Local data */

static GtkWidgetClass *parent_class = NULL;

GtkType gtk_localization_get_type ()
{
	static GtkType localization_type = 0;

	if (!localization_type)
	{
		static const GtkTypeInfo localization_info =
		{
			"GtkLocalization",
			sizeof (GtkLocalization),
			sizeof (GtkLocalizationClass),
			(GtkClassInitFunc) gtk_localization_class_init,
			(GtkObjectInitFunc) gtk_localization_init,
			/* reserved_1 */ NULL,
			/* reserved_1 */ NULL,
			(GtkClassInitFunc) NULL
		};

		localization_type = gtk_type_unique (GTK_TYPE_WIDGET, &localization_info);
	}

	return localization_type;
}

static void gtk_localization_class_init (GtkLocalizationClass *class)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = (GtkObjectClass*) class;
	widget_class = (GtkWidgetClass*) class;

	parent_class = gtk_type_class (gtk_widget_get_type ());

	object_class->destroy = gtk_localization_destroy;

	widget_class->realize = gtk_localization_realize;
	widget_class->expose_event = gtk_localization_expose;
	widget_class->size_request = gtk_localization_size_request;
	widget_class->size_allocate = gtk_localization_size_allocate;
	widget_class->button_press_event = gtk_localization_button_press;
	widget_class->button_release_event = gtk_localization_button_release;
	widget_class->motion_notify_event = gtk_localization_motion_notify;
}

static void gtk_localization_init (GtkLocalization *localization)
{
	localization->robot_radius = 0;
	localization->min_x = -1;
	localization->min_y = -1;
	localization->max_x = 1;
	localization->max_y = 1;

	localization->num_robots  = 0;
	localization->num_objects = 0;

	memset(localization->objects, 0, sizeof(struct localization_object_t) * MAX_OBJECTS);
	memset(localization->robots,  0, sizeof(struct localization_robot_t ) * MAX_ROBOTS );
}

GtkWidget* gtk_localization_new (gint min_x, gint min_y, gint max_x, gint max_y, gint robot_radius)
{
	GtkLocalization *localization;

	localization = gtk_type_new (gtk_localization_get_type ());

	localization->min_x = min_x;
	localization->min_y = min_y;
	localization->max_x = max_x;
	localization->max_y = max_y;
	localization->robot_radius = robot_radius;
	localization->object_radius = robot_radius / 2;

	return GTK_WIDGET (localization);
}

static void gtk_localization_destroy (GtkObject *object)
{
	GtkLocalization *localization;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GTK_IS_LOCALIZATION (object));

	localization = GTK_LOCALIZATION (object);

	if (GTK_OBJECT_CLASS (parent_class)->destroy) {
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
	}
}

static void gtk_localization_realize (GtkWidget *widget)
{
	GtkLocalization *localization;
	GdkWindowAttr attributes;
	gint attributes_mask;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_LOCALIZATION (widget));

	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
	localization = GTK_LOCALIZATION (widget);

	attributes.x = widget->allocation.x;
	attributes.y = widget->allocation.y;
	attributes.width = widget->allocation.width;
	attributes.height = widget->allocation.height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.event_mask = gtk_widget_get_events (widget) | 
								GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | 
								GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK |
								GDK_POINTER_MOTION_HINT_MASK;
	attributes.visual = gtk_widget_get_visual (widget);
	attributes.colormap = gtk_widget_get_colormap (widget);

	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
	widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);

	widget->style = gtk_style_attach (widget->style, widget->window);

	gdk_window_set_user_data (widget->window, widget);

	gtk_style_set_background (widget->style, widget->window, GTK_STATE_ACTIVE);
}

static void gtk_localization_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	requisition->width = LOCALIZATION_DEFAULT_SIZE;
	requisition->height = LOCALIZATION_DEFAULT_SIZE;
}

static void gtk_localization_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
	GtkLocalization *localization;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_LOCALIZATION (widget));
	g_return_if_fail (allocation != NULL);

	widget->allocation = *allocation;
	if (GTK_WIDGET_REALIZED (widget))
	{
		localization = GTK_LOCALIZATION (widget);

		gdk_window_move_resize (widget->window, allocation->x, allocation->y, allocation->width, allocation->height);
	}
}

static gboolean gtk_localization_expose( GtkWidget *widget, GdkEventExpose *event )
{
	GtkLocalization *localization;
	GdkColor color;
	gint i;
	gint xc, yc;
	gint dx, dy;
	gint radius;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_LOCALIZATION (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	if (event->count > 0) {
		return FALSE;
	}

	localization = GTK_LOCALIZATION (widget);

	gdk_window_clear_area (widget->window, 0, 0, widget->allocation.width, widget->allocation.height);

	// (14, 177)  => (0    , height)
	// (277, 177) => (width, height)
	// (14, 6)    => (0    , 0)
	// (277, 6)   => (width, 0)
	//
	// [x - min] / [max - min] = [xc] / [width]
	// [y - min] / [max - min] = [yc] / [height]
	//
	// [xc] = [x - min] / [max - min] * [width]
	// [yc] = [height] * [y - min] / [max - min]

	// Use black for the Korebot
	color.red   = 0x5555;
	color.green = 0x5555;
	color.blue  = 0x5555;
	gdk_gc_set_rgb_fg_color(widget->style->fg_gc[widget->state], &color);

	radius = widget->allocation.width * localization->robot_radius / (localization->max_x - localization->min_x);
	for (i=0; i<localization->num_robots; i++) {
		if (localization->robots[i].x != -1 && localization->robots[i].y != -1) {
			xc = widget->allocation.width  * (localization->robots[i].x - localization->min_x) / (localization->max_x - localization->min_x);
			yc = widget->allocation.height - widget->allocation.height * (localization->robots[i].y - localization->min_y) / (localization->max_y - localization->min_y);

			dx = (radius * 1.75) *  cos(3.14159 * ((localization->robots[i].orientation) % 360)/180.0f);
			dy = (radius * 1.75) * -sin(3.14159 * ((localization->robots[i].orientation) % 360)/180.0f);

			gdk_draw_arc(widget->window, widget->style->fg_gc[widget->state], TRUE, xc - radius, yc - radius, radius*2, radius*2, 0, 360*64);
			gdk_draw_line(widget->window, widget->style->fg_gc[widget->state], xc, yc, xc + dx, yc + dy);
		}
	}

	// Use white for the objects
	color.red   = 0xFFFF;
	color.green = 0xFFFF;
	color.blue  = 0xFFFF;
	gdk_gc_set_rgb_fg_color(widget->style->fg_gc[widget->state], &color);

	for (i=0; i<localization->num_objects; i++) {
		if (localization->objects[i].x != -1 && localization->objects[i].y != -1) {
			xc = widget->allocation.width  * (localization->objects[i].x - localization->min_x) / (localization->max_x - localization->min_x);
			yc = widget->allocation.height - widget->allocation.height * (localization->objects[i].y - localization->min_y) / (localization->max_y - localization->min_y);
			radius = widget->allocation.width * localization->object_radius / (localization->max_x - localization->min_x);

			gdk_draw_arc(widget->window, widget->style->fg_gc[widget->state], TRUE, xc, yc, radius*2, radius*2, 0, 360*64);
		}
	}

	color.red   = 0x0000;
	color.green = 0x0000;
	color.blue  = 0x0000;
	gdk_gc_set_rgb_fg_color(widget->style->fg_gc[widget->state], &color);

	return FALSE;
}

static gboolean gtk_localization_button_press( GtkWidget *widget, GdkEventButton *event )
{
	GtkLocalization *localization;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_LOCALIZATION (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	localization = GTK_LOCALIZATION (widget);

	return FALSE;
}

static gboolean gtk_localization_button_release( GtkWidget *widget, GdkEventButton *event )
{
	GtkLocalization *localization;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_LOCALIZATION (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	localization = GTK_LOCALIZATION (widget);

	return FALSE;
}

static gboolean gtk_localization_motion_notify( GtkWidget *widget, GdkEventMotion *event )
{
	GtkLocalization *localization;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_LOCALIZATION (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	localization = GTK_LOCALIZATION (widget);

	return FALSE;
}

/*
static void gtk_localization_update_mouse (GtkLocalization *localization, gint x, gint y)
{
	gint xc, yc;

	g_return_if_fail (localization != NULL);
	g_return_if_fail (GTK_IS_LOCALIZATION (localization));

	xc = GTK_WIDGET(localization)->allocation.width / 2;
	yc = GTK_WIDGET(localization)->allocation.height / 2;

}

static void gtk_localization_update (GtkLocalization *localization)
{
	g_return_if_fail (localization != NULL);
	g_return_if_fail (GTK_IS_LOCALIZATION (localization));

	gtk_widget_draw (GTK_WIDGET(localization), NULL);
}
*/


void gtk_localization_set_robots(GtkLocalization *localization, char num_robots,  const struct localization_robot_t  *robots) {
	g_return_if_fail (localization != NULL);
	g_return_if_fail (GTK_IS_LOCALIZATION (localization));

	memcpy(localization->robots, robots, sizeof(struct localization_robot_t) * num_robots);
	localization->num_robots = num_robots;

	gtk_widget_queue_draw (GTK_WIDGET(localization));
}

void gtk_localization_set_objects(GtkLocalization *localization, char num_objects, const struct localization_object_t *objects) {
	g_return_if_fail (localization != NULL);
	g_return_if_fail (GTK_IS_LOCALIZATION (localization));

	memcpy(localization->objects, objects, sizeof(struct localization_object_t) * num_objects);
	localization->num_objects = num_objects;

	gtk_widget_queue_draw (GTK_WIDGET(localization));
}
