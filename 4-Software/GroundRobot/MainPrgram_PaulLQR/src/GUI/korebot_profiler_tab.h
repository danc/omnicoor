#ifndef _GUI_KOREBOT_PROFILER_TAB_H
#define _GUI_KOREBOT_PROFILER_TAB_H

#include <gtk/gtk.h>
#include "common.h"
#include "Network/packets/base_packet.h"

class KorebotTab;

// Make it sufficiently big to handle all the data
#define MAX_PROFILE_OUTPUT_LEN	4096

class KorebotProfilerTab {
	private:
		char output_text[MAX_PROFILE_OUTPUT_LEN];

		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		// The widget that will show the results of the profiler operation
		GtkWidget *output;

		// The parent tab, used to send packets out on the network
		KorebotTab *parent;

		// 1 if new data has arrived, and we need to update the profiler text
		int update_profiler_text;
	public:

		/*
		 * Creates a default korebot profiler tab
		 *
		 * @param parentTab The parent tab for this tab
		 */
		KorebotProfilerTab(KorebotTab *parentTab);

		/*
		 * Frees any resources used by this korebot profiler tab
		 */
		~KorebotProfilerTab();

		/*
		 * Creates all the GTK elements needed by the korebot profiler tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the korebot profiler tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot profiler tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

};

#endif

