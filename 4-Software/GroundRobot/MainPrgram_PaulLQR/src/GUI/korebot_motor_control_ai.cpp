#include <string.h>
#include "korebot_motor_control_ai.h"
#include "korebot_tab.h"
#include "Network/packets/ai_list.h"
#include "Network/packets/configuration_packet.h"

void korebot_motor_control_ai_on_action(GtkWidget *widget, gpointer data);

/*
 * Creates a default korebot AI motor control frame
 *
 * @param parentTab The parent tab for this tab
 */
KorebotMotorControlAI::KorebotMotorControlAI(KorebotTab *parentTab) {
	container = NULL;
	start_ai  = NULL;
	ai_ids    = NULL;
	num_ai    = 0;
	parent    = parentTab;
}

/*
 * Frees any resources used by this frame
 */
KorebotMotorControlAI::~KorebotMotorControlAI() {
	if (ai_ids != NULL) {
		delete ai_ids;
		ai_ids = NULL;
	}
}

/*
 * Creates all the GTK elements needed by the frame
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotMotorControlAI::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_vbox_new(FALSE, 0);

	ai_list = gtk_combo_box_new_text();
	g_signal_connect(G_OBJECT(ai_list), "changed", G_CALLBACK(korebot_motor_control_ai_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(container), ai_list, FALSE, FALSE, 20);

	start_ai = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(start_ai), gtk_label_new("Start AI"));
	gtk_signal_connect(GTK_OBJECT(start_ai), "clicked", G_CALLBACK(korebot_motor_control_ai_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(container), start_ai, FALSE, FALSE, 20);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotMotorControlAI::start() {
	gtk_widget_show(ai_list);
	gtk_widget_show_all(start_ai);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotMotorControlAI::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the AI motor control frame.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot motor tab
 */
GtkWidget *KorebotMotorControlAI::getContainer() {
	return container;
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotMotorControlAI::processPacket(BasePacket *packet) {
	int i;

	if (packet->getOpcode() == OPCODE_AI_LIST) {
		AIList *list = (AIList *)packet;

		if (ai_ids != NULL) {
			delete ai_ids;
		}

		for (i=0; i<num_ai; i++) {
			gtk_combo_box_remove_text(GTK_COMBO_BOX(ai_list), 0);
		}

		num_ai = list->getNumAI();
		ai_ids = new int[num_ai];

		for (i=0; i<num_ai; i++) {
			ai_ids[i] = list->getID(i);
			gtk_combo_box_append_text(GTK_COMBO_BOX(ai_list), list->getName(i));
		}
	}
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotMotorControlAI::onUserAction(GtkWidget *widget) {
	if (widget == start_ai) {
		gint active_ai = gtk_combo_box_get_active(GTK_COMBO_BOX(ai_list));

		if (active_ai >= 0 && active_ai < num_ai) {
			ConfigurationPacket conf(CONFIGURATION_OPTION_AI_ENABLE, ai_ids[active_ai]);
			parent->transmit(&conf);
		}
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotMotorControlAI::onIdle() {
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotMotorTab instance
 */
void korebot_motor_control_ai_on_action(GtkWidget *widget, gpointer data) {
	((KorebotMotorControlAI *)data)->onUserAction(widget);
}
