#include <string.h>
#include <stdlib.h>
#include "korebot_sensor_tab.h"
#include "Korebot/sensor_manager_common.h"
#include "Network/packets/all_packets.h"

void korebot_sensor_tab_on_action(GtkWidget *widget, gpointer data);

/*
 * Creates a default korebot sensor tab
 *
 * @param parentTab The parent tab for this tab
 */
KorebotSensorTab::KorebotSensorTab(KorebotTab *parentTab) {
	container = NULL;
	sensors = NULL;

	num_sensors = 0;

	parent = parentTab;
}

/*
 * Frees any resources used by this korebot sensor tab
 */
KorebotSensorTab::~KorebotSensorTab() {
	int i;

	if (sensors != NULL) {
		for (i=0; i < num_sensors; i++) {
			if (sensors[i].window != NULL) {
				sensors[i].window->stop();
				delete sensors[i].window;
				sensors[i].window = NULL;
			}
		}
		free(sensors);
		sensors = NULL;
	}
}

/*
 * Creates all the GTK elements needed by the korebot sensor tab
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotSensorTab::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_vbox_new(TRUE, 5);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotSensorTab::start() {
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotSensorTab::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the korebot sensor tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot sensor tab
 */
GtkWidget *KorebotSensorTab::getContainer() {
	return container;
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotSensorTab::processPacket(BasePacket *packet) {
	SensorControl *sc = (SensorControl *)packet;

	if (sc->getCommand() == SENSOR_CONTROL_SENSOR_LIST_DATA) {
		SensorList *sl = (SensorList *)sc;
		unsigned char i=0;

		this->num_sensors = sl->getNumSensors();

		sensors = (struct button_list_t *)malloc(sizeof(struct button_list_t) * sl->getNumSensors());
		memset(sensors, 0, sizeof(struct button_list_t) * sl->getNumSensors());

		// Create buttons so we can launch graphs for each available sensor
		for (i=0; i < sl->getNumSensors(); i++) {
			sl->getSensorData(i, &sensors[i].sensor_data);
		}

	} else if (sc->getCommand() == SENSOR_CONTROL_SENSOR_DATA) {
		SensorData *sd = (SensorData *)packet;
		unsigned char i=0;
		unsigned char j=0;

		for (i=0; i<num_sensors && j < sd->getNumSensors(); i++) {
			if (sensors[i].window != NULL && sensors[i].sensor_data.id == sd->getSensorID(j)) {
				sensors[i].window->addDataPoint(sd->getSensorTimestamp(j),
												sd->getSensorValue(j));
				j++;
			}
		}
	}
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotSensorTab::onUserAction(GtkWidget *widget) {
	int i = 0;

	for (i=0; i < num_sensors; i++) {
		if (widget == sensors[i].button) {
			// The user clicked on a button.  Disable the button, and create
			// a window to show the new window
			gtk_widget_set_sensitive(GTK_WIDGET(widget), FALSE);

			if (sensors[i].window == NULL) {
				sensors[i].window = new KorebotSensorWindow(parent, sensors[i].sensor_data.id);
				sensors[i].window->init(sensors[i].sensor_data.name);

				g_signal_connect(G_OBJECT(sensors[i].window->getContainer()), "destroy", G_CALLBACK(korebot_sensor_tab_on_action), this);

				sensors[i].window->start();
			}
		} else if (sensors[i].window != NULL && widget == sensors[i].window->getContainer()) {
			// The user closed the window that was displaying the sensor data.
			// Delete the window object, and re-enable the button
			sensors[i].window->stop();
			delete sensors[i].window;
			sensors[i].window = NULL;

			gtk_widget_set_sensitive(GTK_WIDGET(sensors[i].button), TRUE);
		}
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotSensorTab::onIdle() {
	unsigned char i;

	for (i=0; i < num_sensors; i++) {
		if (sensors[i].button == NULL) {

			sensors[i].button = gtk_button_new_with_label(sensors[i].sensor_data.name);
			g_signal_connect(G_OBJECT(sensors[i].button), "clicked", G_CALLBACK(korebot_sensor_tab_on_action), (gpointer)this);
			gtk_box_pack_start(GTK_BOX(container), sensors[i].button, FALSE, 0, 0);
			gtk_widget_show(sensors[i].button);

		} else if (sensors[i].window != NULL) {
			sensors[i].window->onIdle();
		}
	}
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotSensorTab instance
 */
void korebot_sensor_tab_on_action(GtkWidget *widget, gpointer data) {
	((KorebotSensorTab *)data)->onUserAction(widget);
}
