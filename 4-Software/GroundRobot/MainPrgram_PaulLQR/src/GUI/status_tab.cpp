#include "status_tab.h"

/*
 * Creates a default status tab
 */
StatusTab::StatusTab() {
	container = NULL;
	tcp_server_label = NULL;
	tcp_server_active = NULL;

	tcp_server_is_active = -1;
}

/*
 * Frees any resources used by this status tab
 */
StatusTab::~StatusTab() {
}

/*
 * Creates all the GTK elements needed by the status tab
 *
 * @returns 1 on success, 0 otherwise
 */
int StatusTab::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_table_new(1, 2, false);

	tcp_server_label = gtk_label_new(NULL);
	gtk_label_set_markup (GTK_LABEL (tcp_server_label), "TCP Server:");
	gtk_label_set_justify(GTK_LABEL(tcp_server_label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment(GTK_MISC(tcp_server_label), 0.0f, 0.0f);
	gtk_table_attach_defaults(GTK_TABLE(container), tcp_server_label, 0, 1, 0, 1);

	tcp_server_active = gtk_label_new(NULL);
	setObjectActiveText(tcp_server_active, tcp_server_is_active);
	gtk_label_set_justify(GTK_LABEL(tcp_server_active), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_alignment(GTK_MISC(tcp_server_active), 1.0f, 0.0f);
	gtk_table_attach_defaults(GTK_TABLE(container), tcp_server_active, 1, 2, 0, 1);

	return 1;
}

/*
 * Shows the tab
 */
void StatusTab::start() {
	gtk_widget_show(tcp_server_label);
	gtk_widget_show(tcp_server_active);
	gtk_widget_show(container);
}

/*
 * Gets the top-level container for the status tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the status tab
 */
GtkWidget *StatusTab::getContainer() {
	return container;
}

/*
 * Returns if the display thinks the TCP server is active.
 *
 * @returns 1, if the server is active, 0 if it is not, or -1 if 
 * 			the status is unknown
 */
int StatusTab::getTcpServerIsActive() {
	return tcp_server_is_active;
}

/*
 * Set if the TCP server is active
 *
 * @param isActive 1, if the server is active, 0 otherwise
 */
void StatusTab::setTcpServerIsActive(int isActive) {
	tcp_server_is_active = isActive;

	setObjectActiveText(tcp_server_active, tcp_server_is_active);
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void StatusTab::onIdle() {
}

/*
 * Sets the text for a boolean status object
 *
 * @param widget The label that represents the status of the object
 * @param isActive 1, if the object is active, 0 if the object is inactive,
 * 				or -1 if the status is unknown
 */
void StatusTab::setObjectActiveText(GtkWidget *widget, int isActive) {
	if (isActive < 0) {
		gtk_label_set_markup (GTK_LABEL (widget), "<span color=\"#777777\">UNKNOWN</span>");
	} else if (isActive == 0) {
		gtk_label_set_markup (GTK_LABEL (widget), "<span color=\"#FF0000\">FAIL</span>");
	} else {
		gtk_label_set_markup (GTK_LABEL (widget), "<span color=\"#00FF00\">OK</span>");
	}
	gtk_widget_queue_draw (widget);
}
