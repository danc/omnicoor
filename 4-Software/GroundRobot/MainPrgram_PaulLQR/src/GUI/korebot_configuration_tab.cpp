#include <string.h>
#include "korebot_tab.h"
#include "korebot_configuration_tab.h"
#include "Network/packets/configuration_packet.h"

void korebot_configuration_on_action(GtkWidget *widget, gpointer data);

/*
 * Creates a default korebot configuration tab
 *
 * @param parentTab The parent tab for this tab
 */
KorebotConfigurationTab::KorebotConfigurationTab(KorebotTab *parentTab) {
	container = NULL;
	enable_profiler = NULL;
	enable_bounds_limiting = NULL;
	enable_logging = NULL;

	parent = parentTab;
}

/*
 * Frees any resources used by this korebot configuration tab
 */
KorebotConfigurationTab::~KorebotConfigurationTab() {
}

/*
 * Creates all the GTK elements needed by the korebot configuration tab
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotConfigurationTab::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_vbox_new(FALSE, 0);

	enable_profiler = gtk_check_button_new_with_label("Enable Profiler");
	gtk_signal_connect(GTK_OBJECT(enable_profiler), "toggled", G_CALLBACK(korebot_configuration_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(container), enable_profiler, FALSE, FALSE, 5);

	enable_bounds_limiting = gtk_check_button_new_with_label("Enable Bounds Limiting");
	gtk_signal_connect(GTK_OBJECT(enable_bounds_limiting), "toggled", G_CALLBACK(korebot_configuration_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(container), enable_bounds_limiting, FALSE, FALSE, 5);

	enable_logging = gtk_check_button_new_with_label("Enable Sensor Logging");
	gtk_signal_connect(GTK_OBJECT(enable_logging), "toggled", G_CALLBACK(korebot_configuration_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(container), enable_logging, FALSE, FALSE, 5);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotConfigurationTab::start() {
	gtk_widget_show(enable_logging);
	gtk_widget_show(enable_bounds_limiting);
	gtk_widget_show(enable_profiler);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotConfigurationTab::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the korebot configuration tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot configuration tab
 */
GtkWidget *KorebotConfigurationTab::getContainer() {
	return container;
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotConfigurationTab::processPacket(BasePacket *packet) {
	ConfigurationPacket *cp = (ConfigurationPacket *)packet;

	if (cp->getOption() == CONFIGURATION_OPTION_PROFILER) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enable_profiler), cp->getValue());
		gtk_widget_queue_draw (GTK_WIDGET(enable_profiler));
	} else if (cp->getOption() == CONFIGURATION_OPTION_MOTOR_BOUNDS_LIMIT) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enable_bounds_limiting), cp->getValue());
		gtk_widget_queue_draw (GTK_WIDGET(enable_bounds_limiting));
	} else if (cp->getOption() == CONFIGURATION_OPTION_SENSOR_LOGGING) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enable_logging), cp->getValue());
		gtk_widget_queue_draw (GTK_WIDGET(enable_logging));
	}
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotConfigurationTab::onUserAction(GtkWidget *widget) {
	ConfigurationPacket config;

	if (widget == enable_profiler) {
		gboolean value = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(enable_profiler));
		config.setOption(CONFIGURATION_OPTION_PROFILER);
		config.setValue(value);
		parent->transmit(&config);
	} else if (widget == enable_bounds_limiting) {
		gboolean value = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(enable_bounds_limiting));
		config.setOption(CONFIGURATION_OPTION_MOTOR_BOUNDS_LIMIT);
		config.setValue(value);
		parent->transmit(&config);
	} else if (widget == enable_logging) {
		gboolean value = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(enable_logging));
		config.setOption(CONFIGURATION_OPTION_SENSOR_LOGGING);
		config.setValue(value);
		parent->transmit(&config);
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotConfigurationTab::onIdle() {
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotMotorTab instance
 */
void korebot_configuration_on_action(GtkWidget *widget, gpointer data) {
	((KorebotConfigurationTab *)data)->onUserAction(widget);
}

