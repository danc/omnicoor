#include "options_tab.h"

/*
 * Creates a default options tab
 */
OptionsTab::OptionsTab() {
	container = NULL;
}

/*
 * Frees any resources used by this options tab
 */
OptionsTab::~OptionsTab() {
}

/*
 * Creates all the GTK elements needed by the options tab
 *
 * @returns 1 on success, 0 otherwise
 */
int OptionsTab::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_vbox_new(FALSE, 0);

	return 1;
}

/*
 * Shows the tab
 */
void OptionsTab::start() {
	gtk_widget_show(container);
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void OptionsTab::onIdle() {
}

/*
 * Gets the top-level container for the options tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the options tab
 */
GtkWidget *OptionsTab::getContainer() {
	return container;
}

