#include <stdlib.h>
#include <string.h>
#include "main_gui.h"
#include "configuration.h"
#include "Network/packets/all_packets.h"

void main_gui_on_action(GtkWidget *widget, gpointer data);

/*
 * Called every few seconds, and checks the status
 * of various background tasks.
 *
 * @param data A pointer to the MainGUI object
 *
 * @returns 1, if the timer should continue, or 0 if the timer should
 *			stop calling this method
 */
gint statusTimer(gpointer data);

/*
 * Called whenever the GUI is idle, and is used when the GUI needs
 * to be updated.
 *
 * @param data A pointer to the MainGUI object
 *
 * @returns 1, if the timer should continue to run, or 0 if the timer
 *				shloud stop calling this method
 */
gboolean main_gui_idle(gpointer data);

/*
 * Creates a GUI object.  No GUI is shown, and no elements
 * have been created.
 */
MainGUI::MainGUI() {
	window       = NULL;
	view         = NULL;
	tablist      = NULL;
	options      = NULL;
	status       = NULL;
	localization = NULL;
	stop_all     = NULL;

	all_robots   = NULL;

	timer = 0;

	SLIST_INIT(&connections);

	server = new TCP_Server();
	server->setConnectionHandler(this);

	localization_server = new UDP_Connection();
	localization_server->setPacketHandler(this);
	localization_server->setUsePacketHandlerThread(0);
}

/*
 * Frees any memory used by this GUI object
 */
MainGUI::~MainGUI() {
	// All memory used by GTK are freed by the GTK engine.
	// We therefore don't need to free any of the GTK objects we created

	delete localization_server;
	delete server;

	if (timer != 0) {
		g_source_remove(timer);
	}

	if (all_robots != NULL) {
		delete all_robots;
	}
	if (localization != NULL) {
		delete localization;
	}
	if (options != NULL) {
		delete options;
	}
	if (status != NULL) {
		delete status;
	}
}

/*
 * Initializes the GUI.  This must be called after
 * gtk_init(), or else it will fail at runtime.
 *
 * @returns 1 on success, 0 otherwise
 */
int MainGUI::init() {
	if (window != NULL) {
		return 0;
	}

	Configuration::instance()->getValue("network.server.ip_address", net_config.ip_addr, IP_ADDR_MAX_LEN, "127.0.0.1");
	net_config.port = Configuration::instance()->getValue("network.server.port", 65530);

	Configuration::instance()->getValue("network.localization.ip_address", localization_config.ip_addr, IP_ADDR_MAX_LEN, "127.0.0.1");
	localization_config.port = Configuration::instance()->getValue("network.localization.port", 65530);

	// The main window
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "KoreBot Command & Control");
	gtk_window_set_default_size(GTK_WINDOW(window),
			Configuration::instance()->getValue("main_window.width", (int)680),
			Configuration::instance()->getValue("main_window.height", (int)700)
		);
	g_signal_connect_swapped(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

	// A view to hold everything
	view = gtk_vbox_new(FALSE, 0);

	stop_all = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(stop_all), gtk_label_new("STOP ALL KOREBOTS"));
	g_signal_connect(G_OBJECT(stop_all), "clicked", G_CALLBACK(main_gui_on_action), (gpointer)this);
	gtk_box_pack_start(GTK_BOX(view), stop_all, FALSE, FALSE, 10);

	// Create the top-level tab containers
	tablist = gtk_notebook_new();
	gtk_notebook_set_tab_pos(GTK_NOTEBOOK(tablist), GTK_POS_TOP);
	gtk_box_pack_start(GTK_BOX(view), tablist, TRUE, TRUE, 1);

	// Status tab
	status = new StatusTab();
	status->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(tablist), status->getContainer(), gtk_label_new("Status"));

	// Options tab
	options = new OptionsTab();
	options->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(tablist), options->getContainer(), gtk_label_new("Options"));

	// Localization tab
	localization = new LocalizationTab();
	localization->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(tablist), localization->getContainer(), gtk_label_new("Localization"));

	// A tab to control all connected robots
	all_robots = new KorebotTab(this, KOREBOT_ID_BROADCAST, "(All Robots)");
	all_robots->init();
	gtk_notebook_append_page(GTK_NOTEBOOK(tablist), all_robots->getContainer(), gtk_label_new(all_robots->getRobotName()));

	// Add the view to the window
	gtk_container_add(GTK_CONTAINER(window), view);

	timer = g_timeout_add(Configuration::instance()->getValue("main_window.response_timer", 5000),
						statusTimer,
						(gpointer)this);

	g_idle_add(main_gui_idle, this);

	return 1;
}

/*
 * Shows the GUI.  This is a blocking call, and will not return
 * control to the calling method until the GUI has shutdown.
 *
 * @returns void
 */
void MainGUI::start() {
	status->start();
	options->start();
	localization->start();
	all_robots->start();
	gtk_widget_show_all(stop_all);
	gtk_widget_show(tablist);
	gtk_widget_show(view);
	gtk_widget_show(window);

	// Attempt to start the server; If it fails, that's OK, we'll try again later.
	server->start(net_config);
	localization_server->start(localization_config);

	gtk_main();
}

/*
 * Called when a new korebot attempts to connect to the server.
 * The network handler must set the packet handler for the new
 * connection, or packets received from the connection will not
 * be processed.
 *
 * @param con The new connection
 */
void MainGUI::NewConnection(TCP_Connection *con) {
	con->setPacketHandler(this);
}

/*
 * Sends a packet over the connection
 *
 * @param robot_id The ID of the robot to send the packet to
 * @param packet The packet to send
 */
void MainGUI::transmit(unsigned char robot_id, BasePacket *packet) {
	struct connected_korebot_list *connection_entry;

	server->transmit(robot_id, packet);
	if (robot_id == KOREBOT_ID_BROADCAST) {
		// If this is a packet for all robots, pass it on to the robot tabs
		for (connection_entry = SLIST_FIRST(&connections); connection_entry != NULL; ) {
			if (connection_entry->tab != NULL) {
				connection_entry->tab->processPacket(connection_entry->connection, packet);
			}
			connection_entry = SLIST_NEXT(connection_entry, korebots);
		}
	}
}

/*
 * When a packet is received on the network, this method is called to
 * process the packet.
 *
 * @param connection The source connection
 * @param packet The packet that was received from the network
 */
void MainGUI::processPacket(TCP_Connection *connection, BasePacket *packet) {
	struct connected_korebot_list *connection_entry;
	RobotIdentification *rid;

	// The only thing we do is wait for a robot identification packet
	if (packet->getOpcode() == OPCODE_ROBOT_IDENTIFICATION) {
		rid = (RobotIdentification *)packet;
		connection->setRobotID(rid->getRobotID());

		connection_entry             = (struct connected_korebot_list *)malloc(sizeof(struct connected_korebot_list));
		connection_entry->tab        = NULL;
		connection_entry->connection = connection;
		connection_entry->robot_id = rid->getRobotID();
		strncpy(connection_entry->robot_name, rid->getRobotName(), MAX_ROBOT_NAME_LEN);

		// By adding the connection to the list of connections, we know
		// to add the tab to the GUI in the idle function.
		// We can't add the elements here, as it will cause GTK to segfault

		// By adding the connection_entry to the lsit
		SLIST_INSERT_HEAD(&connections, connection_entry, korebots);
	} else {
		all_robots->processPacket(connection, packet);
	}
}

/*
 * When a packet is received on the network, this method is called to
 * process the packet.
 *
 * @param connection The source connection
 * @param packet The packet that was received from the network
 */
void MainGUI::processPacket(UDP_Connection *connection, BasePacket *packet) {
	if (packet->getOpcode() == OPCODE_LOCALIZATION) {
		localization->processPacket(packet);
	}
}

/*
 * Called when a korebot has closed its connection with the
 * server.  If the program needs to free up any resources that
 * this connection was using, they should be freed, as after this
 * call is made, the connection will no longer be a valid pointer.
 *
 * @param con A pointer to the TCP connection that was closed
 */
void MainGUI::ConnectionClose(TCP_Connection *con) {
	// Anything we would do here is taken care of in the onIdle method
}

/*
 * Checks the status of various background tasks associated with
 * the obect.
 */
void MainGUI::checkStatus() {
	if (server->isListening()) {
		status->setTcpServerIsActive(1);
	} else {
		// The server is not listening, attempt to start it
		server->start(net_config);
		status->setTcpServerIsActive(0);
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void MainGUI::onIdle() {
	ConfigurationPacket config_packet;
	struct connected_korebot_list *connection_entry_next;
	struct connected_korebot_list *connection_entry;
	gint   widget_page_num;

	localization_server->run();

	status->onIdle();
	options->onIdle();
	localization->onIdle();
	all_robots->onIdle();

	for (connection_entry_next = SLIST_FIRST(&connections); connection_entry_next != NULL; ) {
		connection_entry = connection_entry_next;
		connection_entry_next = SLIST_NEXT(connection_entry_next, korebots);

		if (connection_entry->connection == NULL) {
			// The connection is NULL, so the connection has closed; Remove
			// the GUI elements associated with the connection, and remove
			// the connection from our list of active connections.
			SLIST_REMOVE(&connections, connection_entry, connected_korebot_list, korebots);

			if (connection_entry->tab != NULL) {
				connection_entry->tab->stop();

				if (GTK_IS_WIDGET(tablist)) {
					widget_page_num = gtk_notebook_page_num(GTK_NOTEBOOK(tablist), connection_entry->tab->getContainer());
					gtk_notebook_remove_page(GTK_NOTEBOOK(tablist), widget_page_num);
				}

				delete connection_entry->tab;
			}

			free(connection_entry);

		} else if (connection_entry->tab == NULL) {
			// The connection entry is in the list, but the tab has not been created.
			// We need to add the tab to the GUI so the user can control the korebot

			// We move the packet handling to single-threaded mode, as
			// otherwise GTK f*cks itself if we try to change it in anything other
			// than the onIdle methods.
			//
			// Fucking GTK
			connection_entry->connection->setUsePacketHandlerThread(0);

			connection_entry->tab        = new KorebotTab(this, connection_entry->robot_id, connection_entry->robot_name);
			connection_entry->tab->init();

			// Pass packet handling off to the new tab
			connection_entry->connection->setPacketHandler(connection_entry->tab);

			gtk_notebook_append_page(GTK_NOTEBOOK(tablist), connection_entry->tab->getContainer(), gtk_label_new(connection_entry->robot_name));
			connection_entry->tab->start();

			config_packet.setOption(CONFIGURATION_OPTION_REQUEST_CONFIG);
			config_packet.setValue(1);
			connection_entry->connection->transmit(&config_packet);
		} else {
			if (connection_entry->connection->run() < 0) {
				connection_entry->connection->stop();
				connection_entry->connection = NULL;
			}
			connection_entry->tab->onIdle();
		}
	}
}

/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void MainGUI::onUserAction(GtkWidget *widget) {
	if (widget == stop_all) {
		MotorControl mot_con((char)MOTOR_CONTROL_COMMAND_STOP);
		transmit(KOREBOT_ID_BROADCAST, &mot_con);
	}
}

/*
 * Called every few seconds, and checks the status
 * of various background tasks.
 *
 * @param data A pointer to the MainGUI object
 *
 * @returns 1, if the timer should continue, or 0 if the timer should
 * 			stop calling this method
 */
gint statusTimer(gpointer data) {
	((MainGUI *)data)->checkStatus();

	return 1;
}

/*
 * Called whenever the GUI is idle, and is used when the GUI needs
 * to be updated.
 *
 * @param data A pointer to the MainGUI object
 *
 * @returns 1, if the timer should continue to run, or 0 if the timer
 *				shloud stop calling this method
 */
gboolean main_gui_idle(gpointer data) {
	((MainGUI *)data)->onIdle();

   return TRUE;
};

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotMotorTab instance
 */
void main_gui_on_action(GtkWidget *widget, gpointer data) {
	((MainGUI *)data)->onUserAction(widget);
}
