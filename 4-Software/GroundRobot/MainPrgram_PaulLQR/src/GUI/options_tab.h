#ifndef _GUI_OPTIONS_TAB_H
#define _GUI_OPTIONS_TAB_H

#include <gtk/gtk.h>
#include "common.h"

class OptionsTab {
	private:
		GtkWidget *container;

	public:
		/*
		 * Creates a default options tab
		 */
		OptionsTab();

		/*
		 * Frees any resources used by this options tab
		 */
		~OptionsTab();

		/*
		 * Creates all the GTK elements needed by the options tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the options tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the options tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();
};

#endif

