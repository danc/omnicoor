#include <string.h>
#include "korebot_motor_tab.h"
#include "main_gui.h"
#include "Network/packets/all_packets.h"

void korebot_motor_tab_on_action(GtkWidget *widget, gpointer data);

/*
 * Creates a default korebot motor tab
 *
 * @param parentTab The parent tab for this tab
 */
KorebotMotorTab::KorebotMotorTab(KorebotTab *parentTab) {
	container            = NULL;
	motor_control_select = NULL;
	control_release      = NULL;
	control_free         = NULL;
	control_speed        = NULL;
	control_manual       = NULL;
	control_ai           = NULL;
	stop_motors          = NULL;

	parent = parentTab;
}

/*
 * Frees any resources used by this korebot motor tab
 */
KorebotMotorTab::~KorebotMotorTab() {
	if (control_release != NULL) {
		delete control_release;
	}
	if (control_free != NULL) {
		delete control_free;
	}
	if (control_speed != NULL) {
		delete control_speed;
	}
	if (control_manual != NULL) {
		delete control_manual;
	}
	if (control_ai != NULL) {
		delete control_ai;
	}
}

/*
 * Creates all the GTK elements needed by the korebot motor tab
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotMotorTab::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_table_new(7, 2, FALSE);

	stop_motors = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(stop_motors), gtk_label_new("Stop Korebot"));
	g_signal_connect(G_OBJECT(stop_motors), "clicked", G_CALLBACK(korebot_motor_tab_on_action), (gpointer)this);
	gtk_table_attach(GTK_TABLE(container), stop_motors, 1, 2, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	// Motor control selection
	motor_control_select = gtk_combo_box_new_text();
	// If you change the order of the following items, make sure to change the order
	// of the order of the defines in korebot_motor_tab.h accordingly
	gtk_combo_box_append_text(GTK_COMBO_BOX(motor_control_select), "Release Motors");
	gtk_combo_box_append_text(GTK_COMBO_BOX(motor_control_select), "Free Motors");
	gtk_combo_box_append_text(GTK_COMBO_BOX(motor_control_select), "Speed Motor Control");
	gtk_combo_box_append_text(GTK_COMBO_BOX(motor_control_select), "Manual Motor Control");
	gtk_combo_box_append_text(GTK_COMBO_BOX(motor_control_select), "AI Controlled");
	gtk_combo_box_set_active(GTK_COMBO_BOX(motor_control_select), MOTOR_CONTROL_METHOD_INDEX_RELEASE);
	g_signal_connect(G_OBJECT(motor_control_select), "changed", G_CALLBACK(korebot_motor_tab_on_action), (gpointer)this);
	gtk_table_attach(GTK_TABLE(container), motor_control_select, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	control_ai = new KorebotMotorControlAI(parent);
	control_ai->init();
	gtk_table_attach_defaults(GTK_TABLE(container), control_ai->getContainer(), 0, 2, 2, 3);

	control_manual = new KorebotMotorControlManual(parent);
	control_manual->init();
	gtk_table_attach_defaults(GTK_TABLE(container), control_manual->getContainer(), 0, 2, 3, 4);

	control_speed = new KorebotMotorControlSpeed(parent);
	control_speed->init();
	gtk_table_attach_defaults(GTK_TABLE(container), control_speed->getContainer(), 0, 2, 4, 5);

	control_free = new KorebotMotorControlFree(parent);
	control_free->init();
	gtk_table_attach_defaults(GTK_TABLE(container), control_free->getContainer(), 0, 2, 5, 6);

	control_release = new KorebotMotorControlRelease(parent);
	control_release->init();
	gtk_table_attach_defaults(GTK_TABLE(container), control_release->getContainer(), 0, 2, 6, 7);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotMotorTab::start() {

	onMotorControlSelect();
	
	gtk_widget_show_all(stop_motors);
	gtk_widget_show(motor_control_select);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotMotorTab::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the korebot motor tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot motor tab
 */
GtkWidget *KorebotMotorTab::getContainer() {
	return container;
}


/*
 * Called when the user does something in our GUI
 *
 * @param widget The widget that caused the action
 */
void KorebotMotorTab::onUserAction(GtkWidget *widget) {
	if (widget == motor_control_select) {
		onMotorControlSelect();
	} else if (widget == stop_motors) {
		MotorControl mot_con((char)MOTOR_CONTROL_COMMAND_STOP);
		parent->transmit(&mot_con);
		processPacket(&mot_con);
	}
}

/*
 * When the user selects a new motor control method, this method is
 * called.
 */
void KorebotMotorTab::onMotorControlSelect() {
	gint active_motor_control_method = gtk_combo_box_get_active(GTK_COMBO_BOX(motor_control_select));

	// Enable the corresponding motor control method
	switch(active_motor_control_method) {
		case MOTOR_CONTROL_METHOD_INDEX_RELEASE:
			control_ai->     stop();
			control_speed->  stop();
			control_manual-> stop();
			control_free->   stop();
			control_release->start();
			break;
		case MOTOR_CONTROL_METHOD_INDEX_FREE:
			control_ai->     stop();
			control_speed->  stop();
			control_manual-> stop();
			control_free->   start();
			control_release->stop();
			break;
		case MOTOR_CONTROL_METHOD_INDEX_AI:
			control_ai->     start();
			control_speed->  stop();
			control_manual-> stop();
			control_free->   stop();
			control_release->stop();
			break;
		case MOTOR_CONTROL_METHOD_INDEX_MANUAL:
			control_ai->     stop();
			control_speed->  stop();
			control_manual-> start();
			control_free->   stop();
			control_release->stop();
			break;
		case MOTOR_CONTROL_METHOD_INDEX_SPEED:
			control_ai->     stop();
			control_speed->  start();
			control_manual-> stop();
			control_free->   stop();
			control_release->stop();
			break;
		default:
			control_ai->     stop();
			control_speed->  stop();
			control_manual-> stop();
			control_free->   stop();
			control_release->stop();
			break;
	};
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotMotorTab::processPacket(BasePacket *packet) {
	if (packet->getOpcode() == OPCODE_MOTOR) {
		MotorControl *mc = (MotorControl *)packet;

		switch(mc->getCommand()) {
			case MOTOR_CONTROL_MODE_RELEASE:
				gtk_combo_box_set_active(GTK_COMBO_BOX(motor_control_select), MOTOR_CONTROL_METHOD_INDEX_RELEASE);
				control_release->processPacket(packet);
				break;
			case MOTOR_CONTROL_MODE_FREE:
				gtk_combo_box_set_active(GTK_COMBO_BOX(motor_control_select), MOTOR_CONTROL_METHOD_INDEX_FREE);
				control_free->processPacket(packet);
				break;
			case MOTOR_CONTROL_MODE_MANUAL:
				gtk_combo_box_set_active(GTK_COMBO_BOX(motor_control_select), MOTOR_CONTROL_METHOD_INDEX_MANUAL);
				control_manual->processPacket(packet);
				break;
			case MOTOR_CONTROL_MODE_SPEED:
				gtk_combo_box_set_active(GTK_COMBO_BOX(motor_control_select), MOTOR_CONTROL_METHOD_INDEX_SPEED);
				control_speed->processPacket(packet);
				break;
			case MOTOR_CONTROL_COMMAND_STOP:
				gtk_combo_box_set_active(GTK_COMBO_BOX(motor_control_select), MOTOR_CONTROL_METHOD_INDEX_SPEED);
				control_free->processPacket(packet);
				control_manual->processPacket(packet);
				control_speed->processPacket(packet);
				control_ai->processPacket(packet);
				break;
		}
		onMotorControlSelect();

	} else if (packet->getOpcode() == OPCODE_AI_LIST) {
		control_ai->processPacket(packet);
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotMotorTab::onIdle() {
	control_release->onIdle();
	control_free->onIdle();
	control_speed->onIdle();
	control_manual->onIdle();
	control_ai->onIdle();
}

/*
 * Proxy function for when the user does something in the GUI
 *
 * @param widget The widget that caused the action
 * @param data   A pointer to the KorebotMotorTab instance
 */
void korebot_motor_tab_on_action(GtkWidget *widget, gpointer data) {
	((KorebotMotorTab *)data)->onUserAction(widget);
}
