#include <string.h>
#include "korebot_profiler_tab.h"
#include "Network/packets/profiler_packet.h"
#include "profiler.h"

/*
 * Creates a default korebot profiler tab
 *
 * @param parentTab The parent tab for this tab
 */
KorebotProfilerTab::KorebotProfilerTab(KorebotTab *parentTab) {
	container = NULL;
	output = NULL;

	parent = parentTab;

	update_profiler_text = 0;
	memset(output_text, 0, MAX_PROFILE_OUTPUT_LEN);
}

/*
 * Frees any resources used by this korebot profiler tab
 */
KorebotProfilerTab::~KorebotProfilerTab() {
}

/*
 * Creates all the GTK elements needed by the korebot profiler tab
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotProfilerTab::init() {
	if (container != NULL) {
		return 1;
	}

	container = gtk_vbox_new(FALSE, 0);

	output = gtk_label_new("No Profile Information Available.\n");
	gtk_label_set_justify(GTK_LABEL(output), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment(GTK_MISC(output), 0.0f, 0.0f);
	gtk_box_pack_start(GTK_BOX(container), output, TRUE, TRUE, 5);

	return 1;
}

/*
 * Shows the tab
 */
void KorebotProfilerTab::start() {
	gtk_widget_show(output);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void KorebotProfilerTab::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Gets the top-level container for the korebot profiler tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the korebot profiler tab
 */
GtkWidget *KorebotProfilerTab::getContainer() {
	return container;
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void KorebotProfilerTab::onIdle() {
	if (update_profiler_text) {
		gtk_label_set_text(GTK_LABEL(output), output_text);
		gtk_widget_queue_draw (GTK_WIDGET(output));
		update_profiler_text = 0;
	}
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void KorebotProfilerTab::processPacket(BasePacket *packet) {
	ProfilerPacket *pp = (ProfilerPacket *)packet;
	struct profiler_information_t info;
	int i;
	int len;

	sprintf(output_text, "Loop Time: %f\n\n", pp->getLoopTime());

	sprintf(output_text, "%sMin Percent\tAvg Percent\tMax Percent\tTime\t\tCall Count\tName\n", output_text); 
	for (i=0; i<pp->getNumSamples(); i++) {
		pp->getSample(i, info);

		sprintf(output_text,
				"%s% 0.4e\t% 0.4e\t% 0.4e\t% 0.4e\t%d\t\t\t",
				output_text,
				info.fMin,
				info.fAvg,
				info.fMax,
				info.fTime,
				info.callCount);

		if (info.parentCount != 0) {
			len = strlen(output_text);
			memset(&output_text[len], '\t', info.parentCount);
			output_text[len+info.parentCount] = '\0';
		}

		sprintf(output_text, "%s%s\n", output_text, info.name);
	}

	update_profiler_text = 1;
}

