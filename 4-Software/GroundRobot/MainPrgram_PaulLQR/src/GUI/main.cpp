#include "configuration.h"
#include "main_gui.h"

int main(int argc, char **argv) {
	MainGUI mgui;

	if (!Configuration::instance()->load("gui.cfg")) {
		perror("Error: Could not load configuration file");
		return -1;
	}

	gtk_init(&argc, &argv);

	mgui.init();

	mgui.start();


	return 0;
}
