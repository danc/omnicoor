#ifndef _GUI_KOREBOT_MOTOR_CONTROL_SPEED_H
#define _GUI_KOREBOT_MOTOR_CONTROL_SPEED_H

#include <gtk/gtk.h>
#include "gtk_widgets/gtk_dial.h"
#include "common.h"
#include "Network/packets/base_packet.h"

class KorebotTab;

class KorebotMotorControlSpeed {
	private:
		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		GtkWidget *heading;
		GtkWidget *linear_speed;
		GtkWidget *rotational_speed;

		GtkWidget *linear_speed_label;
		GtkWidget *rotational_speed_label;
		GtkWidget *go_button;
		// The parent tab, used to send packets out on the network
		KorebotTab *parent;
	public:

		/*
		 * Creates a default korebot speed motor control pane
		 *
		 * @param parentTab The parent tab for this tab
		 */
		KorebotMotorControlSpeed(KorebotTab *parentTab);

		/*
		 * Frees any resources used by this frame
		 */
		~KorebotMotorControlSpeed();

		/*
		 * Creates all the GTK elements needed by the frame
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the speed motor control frame.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the speed motor controller frame
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();


	private:

		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);

	friend void korebot_motor_control_speed_on_action(GtkWidget *widget, gpointer data);
};

#endif

