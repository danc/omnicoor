#ifndef _GUI_KOREBOT_EXTERNAL_INPUT_XBOX_H
#define _GUI_KOREBOT_EXTERNAL_INPUT_XBOX_H

#include <gtk/gtk.h>
#include <linux/input.h>
#include "common.h"
#include "Network/packets/base_packet.h"
#include "Korebot/external_sensors_common.h"

class KorebotTab;

#define MAX_DEVICE_LEN		32

#define JS_EVENT_BUTTON         0x01    /* button pressed/released */
#define JS_EVENT_AXIS           0x02    /* joystick moved */
#define JS_EVENT_INIT           0x80    /* initial state of device */

struct js_event_t {
	// Event timestamp in milliseconds
	__u32 time;

	// Value
	__s16 value;

	// Event Type
	__u8 type;

	// Axis / Button Number
	__u8 number;
};

struct joystick_t {
	char device[MAX_DEVICE_LEN];
	xbox_controller_t state;
	int fd;
	int num_in_use;
	bool changed;
};

class KorebotExternalInputXBox {
	private:
		static int num_xbox_instances;
		static int num_joysticks;
		static struct joystick_t *joysticks;

		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		// A drop-down widget to select the currently active XBox
		GtkWidget *joystick_list;

		// The parent tab, used to send packets out on the network
		KorebotTab *parent;

		int selected_joystick;
	public:

		/*
		 * Creates a default korebot XBox motor control frame
		 *
		 * @param parentTab The parent tab for this tab
		 */
		KorebotExternalInputXBox(KorebotTab *parentTab);

		/*
		 * Frees any resources used by this frame
		 */
		~KorebotExternalInputXBox();

		/*
		 * Creates all the GTK elements needed by the frame
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the XBox motor control frame.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot motor tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:

		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);

	friend void korebot_external_input_xbox_on_action(GtkWidget *widget, gpointer data);

};

#endif

