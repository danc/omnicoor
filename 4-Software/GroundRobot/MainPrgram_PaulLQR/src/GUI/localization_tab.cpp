#include "localization_tab.h"
#include "configuration.h"
#include "gtk_widgets/gtk_localization.h"
#include "Network/packets/localization_packet.h"

/*
 * Creates a default localization tab
 */
LocalizationTab::LocalizationTab() {
	container = NULL;
	display   = NULL;

	num_robots = 0;
	num_objects = 0;
}

/*
 * Frees any resources used by this localization tab
 */
LocalizationTab::~LocalizationTab() {
}

/*
 * Creates all the GTK elements needed by the localization tab
 *
 * @returns 1 on success, 0 otherwise
 */
int LocalizationTab::init() {
	if (container != NULL) {
		return 1;
	}

	min_x = Configuration::instance()->getValue("main_window.localization.min_x", (int)-1);
	min_y = Configuration::instance()->getValue("main_window.localization.min_y", (int)-1);
	max_x = Configuration::instance()->getValue("main_window.localization.max_x", (int) 1);
	max_y = Configuration::instance()->getValue("main_window.localization.max_y", (int) 1);
	robot_radius = Configuration::instance()->getValue("main_window.localization.robot_radius", (int)10);

	container = gtk_vbox_new(FALSE, 0);

	display = gtk_localization_new(min_x, min_y, max_x, max_y, robot_radius);
	gtk_box_pack_start(GTK_BOX(container), display, TRUE, TRUE, 5);

	return 1;
}

/*
 * Shows the tab
 */
void LocalizationTab::start() {
	gtk_widget_show(display);
	gtk_widget_show(container);
}

/*
 * Hides the tab
 */
void LocalizationTab::stop() {
	if (GTK_IS_WIDGET(container)) {
		gtk_widget_hide(container);
	}
}

/*
 * Called when a packet arrives over the network.
 *
 * @param packet The packet received from the network.
 */
void LocalizationTab::processPacket(BasePacket *packet) {
	if (packet != NULL && packet->getOpcode() == OPCODE_LOCALIZATION) {
		LocalizationPacket *loc = (LocalizationPacket *)packet;

		num_robots = loc->getNumRobots();
		num_objects = loc->getNumObjects();

		if (num_robots < 0 || num_objects < 0 || num_objects > MAX_OBJECTS || num_robots > MAX_ROBOTS) {
			num_robots = 0;
			num_objects = 0;
			return;
		}

		gtk_localization_set_robots(GTK_LOCALIZATION(display), num_robots, loc->getRobotData());
		gtk_localization_set_objects(GTK_LOCALIZATION(display), num_objects, loc->getObjectData());
		gtk_widget_queue_draw (GTK_WIDGET(display));
	}
}

/*
 * Called when the GUI has some idle time.  Anything that needs
 * to update drawn GUI elements should happen in this method, or
 * else the program will be unstable (segfault)
 */
void LocalizationTab::onIdle() {
}

/*
 * Gets the top-level container for the localization tab.
 * Can be used to add to another container, or a window.
 *
 * @returns The container for the localization tab
 */
GtkWidget *LocalizationTab::getContainer() {
	return container;
}


