#ifndef _GUI_MAIN_GUI_H
#define _GUI_MAIN_GUI_H

#include <gtk/gtk.h>
#include <sys/queue.h>
#include "Network/network.h"
#include "Network/tcp_server.h"
#include "Network/tcp_connection.h"
#include "Network/udp_connection.h"
#include "Network/network_connection_handler.h"
#include "Network/packet_handler.h"
#include "Network/packets/robot_identification.h"
#include "common.h"
#include "status_tab.h"
#include "options_tab.h"
#include "korebot_tab.h"
#include "localization_tab.h"

class MainGUI : public NetworkConnectionHandler, public PacketHandler {
	private:
		typedef struct connected_korebot_list {
			SLIST_ENTRY(connected_korebot_list) korebots;

			char robot_id;

			char robot_name[MAX_ROBOT_NAME_LEN];

			TCP_Connection *connection;
			KorebotTab     *tab;
		};

		// The window
		GtkWidget *window;

		// The widget that holds all child widgets for this window
		GtkWidget *view;

		// The widget that shows the tabs
		GtkWidget *tablist;

		// A button to stop all connected korebots
		GtkWidget *stop_all;

		// A tab to show the status of various subsystems
		StatusTab *status;

		// A tab for generic options for the entire system
		OptionsTab *options;

		// A tab to represent the localization system
		LocalizationTab *localization;

		// A tab to represent all connected korebots
		KorebotTab *all_robots;

		// A timer that fires to update the status of the network
		gint timer;

		// The configuration for the TCP server
		network_config_t net_config;
		network_config_t localization_config;
		
		// The server that listens for new korebots
		TCP_Server *server;

		UDP_Connection *localization_server;

		// The head to the list of connected korebots
		SLIST_HEAD(listhead, connected_korebot_list) connections;

	public:
		/*
		 * Creates a GUI object.  No GUI is shown, and no elements
		 * have been created.
		 */
		MainGUI();

		/*
		 * Frees any memory used by this GUI object
		 */
		~MainGUI();

		/*
		 * Initializes the GUI.  This must be called after
		 * gtk_init(), or else it will fail at runtime.
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the GUI.  This is a blocking call, and will not return
		 * control to the calling method until the GUI has shutdown.
		 *
		 * @returns void
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Called when a new korebot attempts to connect to the server.
		 * The network handler must set the packet handler for the new
		 * connection, or packets received from the connection will not
		 * be processed.
		 *
		 * @param con The new connection
		 */
		void NewConnection(TCP_Connection *con);

		/*
		 * Called when a korebot has closed its connection with the
		 * server.  If the program needs to free up any resources that
		 * this connection was using, they should be freed, as after this
		 * call is made, the connection will no longer be a valid pointer.
		 *
		 * @param con A pointer to the TCP connection that was closed
		 */
		void ConnectionClose(TCP_Connection *con);

		/*
		 * Sends a packet over the connection
		 *
		 * @param robot_id The ID of the robot to send the packet to
		 * @param packet The packet to send
		 */
		void transmit(unsigned char robot_id, BasePacket *packet);

		/*
		 * When a packet is received on the network, this method is called to
		 * process the packet.
		 *
		 * @param connection The source connection
		 * @param packet The packet that was received from the network
		 */
		void processPacket(TCP_Connection *connection, BasePacket *packet);

		/*
		 * When a packet is received on the network, this method is called to
		 * process the packet.
		 *
		 * @param connection The source connection
		 * @param packet The packet that was received from the network
		 */
		void processPacket(UDP_Connection *connection, BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:
		/*
		 * Checks the status of various background tasks associated with
		 * the obect.
		 */
		void checkStatus();

		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);
	
	friend gint statusTimer(gpointer data);

	friend gboolean main_gui_idle(gpointer data);

	friend void main_gui_on_action(GtkWidget *widget, gpointer data);
};

#endif
