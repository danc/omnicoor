#ifndef _GUI_KOREBOT_SENSOR_TAB_H
#define _GUI_KOREBOT_SENSOR_TAB_H

#include <gtk/gtk.h>
#include "common.h"
#include "korebot_sensor_window.h"
#include "Network/packets/base_packet.h"
#include "Korebot/sensor_manager_common.h"

class KorebotTab;

class KorebotSensorTab {
	private:
		struct button_list_t {
			GtkWidget *button;

			KorebotSensorWindow *window;

			struct sensor_info_t sensor_data;
		};

		// The widget that all child widgets for this object are added to
		GtkWidget *container;

		// The parent tab, used to send packets out on the network
		KorebotTab *parent;

		struct button_list_t *sensors;

		int num_sensors;
	public:

		/*
		 * Creates a default korebot sensor tab
		 *
		 * @param parentTab The parent tab for this tab
		 */
		KorebotSensorTab(KorebotTab *parentTab);

		/*
		 * Frees any resources used by this korebot sensor tab
		 */
		~KorebotSensorTab();

		/*
		 * Creates all the GTK elements needed by the korebot sensor tab
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shows the tab
		 */
		void start();

		/*
		 * Hides the tab
		 */
		void stop();

		/*
		 * Gets the top-level container for the korebot sensor tab.
		 * Can be used to add to another container, or a window.
		 *
		 * @returns The container for the korebot sensor tab
		 */
		GtkWidget *getContainer();

		/*
		 * Called when a packet arrives over the network.
		 *
		 * @param packet The packet received from the network.
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Called when the GUI has some idle time.  Anything that needs
		 * to update drawn GUI elements should happen in this method, or
		 * else the program will be unstable (segfault)
		 */
		void onIdle();

	private:
		/*
		 * Called when the user does something in our GUI
		 *
		 * @param widget The widget that caused the action
		 */
		void onUserAction(GtkWidget *widget);
		
	friend void korebot_sensor_tab_on_action(GtkWidget *widget, gpointer data);
};

#endif
