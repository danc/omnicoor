#ifndef _COMMON_PROFILER_H
#define _COMMON_PROFILER_H

#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "profiler_output_handler.h"

#define PROFILER_MAX_NAME_LEN	64
#define MAX_PROFILER_SAMPLES	24

class ProfileSample
{
	public:
		/*
		 * Starts profiling code with an identifier of the given name
		 *
		 * @param sampleName A human-readable name to use to identify the
		 *			section of code you will be timing
		 */
		ProfileSample(const char *sampleName);

		/*
		 * Stop profiling this section of the code
		 */
		~ProfileSample();

		/*
		 * Sends the profiler data to the output handler
		 */
		static void output();

		/*
		 * Resets the average statistics for a given sample
		 *
		 * @param sampleName The sample to reset statistics for
		 */
		static void resetSample(const char *sampleName);

		/*
		 * Resets all statistics, and clears any samples that are not
		 * active.
		 */
		static void resetAll();

		// The handler that will receive statistics for the profiler
		static ProfilerOutputHandler *output_handler;

		// If the profiler should be started at the start of the next run,
		// then start it
		static bool start_profiler;

		// If the profiler should be stopped at the end of the next run,
		// then stop it
		static bool stop_profiler;

		static inline bool getProfilerIsRunning() {
			return profiler_is_running;
		}

		/*
		 * Returns the number of seconds since a random point in time
		 * (Such as startup time). The random point does not change
		 * between calls.
		 *
		 * @returns A value that can be used for timers
		 */
		static inline float getTime() {
			struct timeval time;
			gettimeofday(&time, NULL);

			// tv_sec is too large for what we need it for, so I just
			// take the lowest 32 bits, which is plenty.  This will
			// overflow about once every 18.2 hours, longer than the 
			// battery power on the korebot lasts
			return ((float)(time.tv_sec & 0x00FFFF)) + ((float)time.tv_usec) / 1000000.0f;
		}

	protected:

		/**
		 * Calculates the percentages of all the tasks that ran this run of the loop
		 */
		static void calculatePercentages();

		// The index of this sample into the samples array
		int sample_index;

		// The parent of this sample
		int parent_index;

		/************************************************
		*           BEGIN STATIC FIELDS                 *
		************************************************/
		static struct profileSample {

			// Some initializations
			profileSample()
			{
				is_valid   = false;
				data_count = 0;
				average_pc = -1;
				min_pc     = -1;
				max_pc     = -1;
			}

			// Whether or not this sample is valid (for use with fixed-size arrays)
			bool is_valid;

			// Is this sample currently being profiled?
			bool is_open;

			// Number of times this sample has been profiled this frame
			unsigned char call_count;

			// Name of the sample
			char name[PROFILER_MAX_NAME_LEN];

			// Starting time on the clock, in seconds
			float start_time;

			// Total time recorded across all profiles of this sample
			float total_time;

			// Total time taken by children of this sample
			float child_time;

			// Number of parents this sample has (Useful for indenting)
			unsigned char parent_count;

			// Average percentage of time taken up
			float average_pc;

			// Minimum percentage of time taken up
			float min_pc;

			// Maximum percentage of time taken up
			float max_pc;

			// The total time taken for this sample
			float sample_time;

			// Number of percentage values that have been stored
			unsigned long data_count;
		} samples[MAX_PROFILER_SAMPLES];

		// The currently active sample
		static int last_opened_sample;

		// The number of instances of the ProfileSample object
		static unsigned char sample_instances;

		// The number of active samples
		static unsigned char open_sample_count;

		// The time the root-level sample started
		static float root_begin;

		// The time the root-level sample ended
		static float root_end;

		// Is the profiler running?
		// Set this to 1 when you want to start profiling
		static bool profiler_is_running;
};

// If we should use the profiler, define a macro to start the profiler
#ifdef PROFILER
#define PROFILE(name) ProfileSample _profile_sample(name);
#else
#define PROFILE(name)
#endif

#endif
