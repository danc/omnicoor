#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "profiler.h"
#include "profiler_output_handler.h"

int ProfileSample::last_opened_sample = -1;
unsigned char ProfileSample::open_sample_count = 0;
unsigned char ProfileSample::sample_instances = 0;
ProfileSample::profileSample ProfileSample::samples[MAX_PROFILER_SAMPLES];
ProfilerOutputHandler *ProfileSample::output_handler = NULL;
float ProfileSample::root_begin = 0.0f;
float ProfileSample::root_end = 0.0f;
bool ProfileSample::profiler_is_running = false;
bool ProfileSample::start_profiler = false;
bool ProfileSample::stop_profiler = false;

ProfileSample::ProfileSample(const char *sampleName) {
	int i=0;
	int store_index = -1;

	if (start_profiler && sample_instances == 0) {
		profiler_is_running = true;
		start_profiler = false;
	}

	sample_instances++;

	if (!profiler_is_running) {
		return;
	}

	for (i=0; i < MAX_PROFILER_SAMPLES; ++i) {
		if (!samples[i].is_valid) {
			if (store_index < 0) {
				store_index = i;
			}
		} else {
			if (strcmp(samples[i].name, sampleName) == 0) {

				// We don't handle recursive functions very well...
				if (samples[i].is_open) {
					throw "Tried to profile a sample which was already being profiled.";
				}

				// Save the index of this sample
				sample_index = i;

				// Set the parent of this sample
				parent_index = last_opened_sample;

				// This is now the last opened sample
				last_opened_sample = i;

				samples[i].parent_count = open_sample_count;
				open_sample_count++;

				// We are recording this sample
				samples[i].is_open = true;

				// We have called this sample one more time, as well
				samples[i].call_count++;

				// Save the start time of the sample (Profiling has begun...)
				samples[i].start_time = getTime();

				// If there is no parent sample, then this is the root sample
				if (parent_index < 0) {
					root_begin = samples[i].start_time;
				}

				return;
			}
		}
	}

	// Could not find the sample, so it must be a new sample
	if (store_index < 0) {
		throw "Profiler has run out of sample slots";
	}

	// This index in the array is now valid
	samples[store_index].is_valid = true;

	// Save the name of the sample
	strncpy(samples[store_index].name, sampleName, PROFILER_MAX_NAME_LEN);

	// Save the index of this sample
	sample_index = store_index;

	// Set the parent index
	parent_index = last_opened_sample;

	// This is now the last opened sample
	last_opened_sample = store_index;

	// The number of parents is equivical to the number of active samples
	samples[store_index].parent_count = open_sample_count;

	// One more active sample...
	open_sample_count++;

	// We are recording this sample
	samples[store_index].is_open = true;

	// New sample, there can only be one call count
	samples[store_index].call_count = 1;

	// Set the timers to 0
	samples[store_index].total_time = 0.0f;
	samples[store_index].child_time = 0.0f;

	// Save the start time of the sample (Profiling has begun...)
	samples[store_index].start_time = getTime();

	// If there is no parent sample, then this is the root sample
	if (parent_index < 0) {
		root_begin = samples[store_index].start_time;
	}
}

ProfileSample::~ProfileSample() {
	float end_time;
	float time_taken;

	sample_instances--;

	if (!profiler_is_running) {
		return;
	}

	// Record the end time of the sample...Timing is now complete for this sample
	end_time = getTime();

	samples[sample_index].is_open = false;

	time_taken = end_time - samples[sample_index].start_time;

	samples[sample_index].total_time += time_taken;
	last_opened_sample = parent_index;
	open_sample_count--;

	// If we want to stop the profiler, and this is the root-level profiler,
	// then stop the profiler
	if (stop_profiler && sample_instances == 0) {
		profiler_is_running = false;
		stop_profiler = false;
	}

	if (parent_index >= 0) {
		samples[parent_index].child_time += time_taken;
	} else {
		// Last sample in the main loop
		root_end = end_time;
		calculatePercentages();
	}
}

/**
 * Calculates the percentages of all the tasks that ran this run of the loop
 */
void ProfileSample::calculatePercentages() {
	int i;
	float percentage;
	float total_pc;

	// We can't output something if the profiler was never running
	if (!profiler_is_running) {
		return;
	}

	for (i=0; i<MAX_PROFILER_SAMPLES; ++i) {

		// Only send valid samples
		if (samples[i].is_valid) {

			// The time for just this sample (Not child samples)
			samples[i].sample_time = samples[i].total_time - samples[i].child_time;

			// The percentage of the total loop this sample took
			percentage = (samples[i].sample_time / (root_end - root_begin)) * 100.0f;

			total_pc = samples[i].average_pc * samples[i].data_count;
			total_pc += percentage;
			samples[i].data_count++;
			samples[i].average_pc = total_pc / samples[i].data_count;

			if ((samples[i].min_pc == -1) || (percentage < samples[i].min_pc)) {
				samples[i].min_pc = percentage;
			}

			if ((samples[i].max_pc == -1) || (percentage > samples[i].max_pc)) {
				samples[i].max_pc = percentage;
			}

			// Reset some variables for the next time
			samples[i].call_count = 0;
			samples[i].total_time = 0;
			samples[i].child_time = 0;
		}
	}
}

/*
 * Sends the profiler data to the output handler
 */
void ProfileSample::output() {
	int i;


	// We can't output something if the profiler was never running
	if (!profiler_is_running) {
		return;
	}

	// Can't output anything if we don't have somewhere to output to
	if (output_handler == NULL) {
		return;
	}

	// Let the output handler know we are ready to send information
	output_handler->beginOutput(root_end - root_begin);

	for (i=0; i<MAX_PROFILER_SAMPLES; ++i) {

		// Only send valid samples
		if (samples[i].is_valid) {

			output_handler->sample(
					samples[i].min_pc,
					samples[i].average_pc,
					samples[i].max_pc,
					samples[i].sample_time,
					samples[i].call_count,
					samples[i].name,
					samples[i].parent_count);
		}
	}

	// And we are done.
	output_handler->endOutput();
}

void ProfileSample::resetSample(const char *sampleName) {
	for (int i=0; i < MAX_PROFILER_SAMPLES; ++i) {
		if (samples[i].is_valid && strcmp(samples[i].name, sampleName) == 0) {
			// Found the sample
			samples[i].max_pc = -1;
			samples[i].min_pc = -1;
			samples[i].data_count = 0;
			return;
		}
	}
}

void ProfileSample::resetAll() {
	for (int i=0; i < MAX_PROFILER_SAMPLES; ++i) {
		if (samples[i].is_valid) {
			samples[i].max_pc = -1;
			samples[i].min_pc = -1;
			samples[i].data_count = 0;

			if (!samples[i].is_open) {
				samples[i].is_valid = false;
			}

			return;
		}
	}
}
