#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H

#include <libconfig.h>
#include "common.h"

class Configuration {
	private:
		static Configuration *s_instance;

		config_t config;
	public:
		/*
		 * Gets the singleton configuration obect
		 *
		 * @returns The configuration object
		 */
		static Configuration *instance() {
			if (!s_instance) {
				s_instance = new Configuration();
			}
			return s_instance;
		}

		/*
		 * Frees any memory used by this object
		 */
		~Configuration();

		/*
		 * Loads the configuration from the given file
		 *
		 * @param The path to the configuration file to load
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int load(const char *fileName);

		/*
		 * Gets the value at path from the config file
		 *
		 * @param path The path to the value in the config file
		 * @param out  The output variable
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int getValue(const char *path, int def);

		/*
		 * Gets the value at path from the config file
		 *
		 * @param path The path to the value in the config file
		 * @param out  The output variable
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		long getValue(const char *path, long def);

		/*
		 * Gets the value at path from the config file
		 *
		 * @param path The path to the value in the config file
		 * @param out  The output variable
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		float getValue(const char *path, float def);

		/*
		 * Gets the value at path from the config file
		 *
		 * @param path The path to the value in the config file
		 * @param out  The output variable
		 * @param max_length The size of the array out
		 * @param def The default value
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int getValue(const char *path, char *out, int max_length, const char *def);

		/*
		 * Gets the size of an array of values in the config file
		 *
		 * @param path The path to the array
		 */
		int getListSize(const char *path);

	private:
		/*
		 * Creates a default configuration object with no
		 * loaded files
		 */
		Configuration();
};

#endif
