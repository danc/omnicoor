#ifndef _KOREBOT_LOCALIZATION_SYSTEM_H
#define _KOREBOT_LOCALIZATION_SYSTEM_H

#include <stdlib.h>

#include "common.h"
#include "localization_system_common.h"
#include "Network/network.h"
#include "Network/packet_handler.h"
#include "Network/packets/all_packets.h"


class LocalizationSystem : public PacketHandler
{
private:
	// This is a singleton object, so here's the instance to the object
	static LocalizationSystem *s_instance;

	// The number of active robots
	unsigned char num_robots;
	unsigned char num_objects;

	struct localization_robot_history_t
	{
		struct localization_robot_t data[SENSOR_HISTORY];
		float time_since_last_found;
		unsigned char robot_id;
		char found;
	};

	struct localization_object_history_t
	{
		struct localization_object_t data[SENSOR_HISTORY];
		float time_since_last_found;
		char object_id;
		char found;
	};

	unsigned short last_timestamp;
	long total_time;

	// Details about the robots / objects
	struct localization_robot_history_t  robots[MAX_ROBOTS];
	struct localization_object_history_t objects[MAX_OBJECTS];

	network_config_t net_config;
	UDP_Connection *localization_connection;

	int num_packets_received;

public:
	/*
	 * Returns a single instance of the LocalizationSystem.
	 *
	 * @returns A single instance of the LocalizationSystem
	 */
	static LocalizationSystem *instance()
	{
		if (s_instance == NULL)
		{
			s_instance = new LocalizationSystem();
		}
		return s_instance;
	}


	/*
	 * Frees any resources used by the localization system.
	 */
	~LocalizationSystem();

	/*
	 * Initializes the LocalizationSystem.
	 *
	 * @returns 1 on success, 0 otherwise
	 */
	int init();

	/*
	 * Shuts the localization system down, destroying any resources & closing
	 * the network connection down in the process
	 */
	void shutdown();

	/*
	 * Runs one iteration of the localization system; Checks if
	 * there are any localization packets from the network.
	 *
	 * @param dt The time difference since the last call to this method
	 */
	void run(float dt);

	/*
	 * Processes a packet received over the network
	 *
	 * @param packet The packet received from the network
	 */
	void processPacket(TCP_Connection *con, BasePacket *packet);

	/*
	 * Processes a packet received over the network
	 *
	 * @param packet The packet received from the network
	 */
	void processPacket(UDP_Connection *con, BasePacket *packet);

	/*
	 * Sends status information about the sensor manager to the control server.
	 *
	 * This includes the mode & speeds of the motors.
	 */
	void sendConfig();

	/*
	 * Prints the status of various modules on the Korebot
	 */
	void printStatus();

	/********************************************************
	 * 		GETTERS / SETTERS			*
	 *******************************************************/

	/*
	 * Gets the number of robots the localization system received
	 *
	 * @returns The number of known robots
	 */
	unsigned char getNumRobots() const;

	/*
	 * Gets the most recent position information about the robot with the given robot ID
	 *
	 * @param robot_id The ID of the robot to get information about
	 * @param out The structure to store the data in
	 *
	 * @returns 1, if the robot could be found, 0 otherwise
	 */
	int getRobotData(unsigned char robot_id, struct localization_robot_t &out) const;

	/*
	 * Gets a history of information about the robot with the given ID
	 *
	 * @param robot_id The ID of the robot to get information about
	 *
	 * @returns A pointer to an array (Of length SENSOR_HISTORY) with robot history.  Index 0 in this array is the
	 * 			furthest in the past, while index SENSOR_HISTORY-1 is the most recent item.
	 * 			Returns NULL if no robot could be found with the given ID
	 */
	const struct localization_robot_t *getRobotData(unsigned char robot_id) const;

	/*
	 * Gets the number of objects the localization system received
	 *
	 * @returns The number of known objects
	 */
	unsigned char getNumObjects() const;

	/*
	 * Gets the most recent position information about the object with the given object ID
	 *
	 * @param object_id The ID of the object to get information about
	 * @param out The structure to store the data in
	 *
	 * @returns 1, if the object could be found, 0 otherwise
	 */
	int getObjectData(unsigned char object_id, struct localization_object_t &out) const;

	/*
	 * Gets a history of information about the object with the given ID
	 *
	 * @param object_id The ID of the object to get information about
	 *
	 * @returns A pointer to an array (Of length SENSOR_HISTORY) with object history.  Index 0 in this array is the
	 * 			furthest in the past, while index SENSOR_HISTORY-1 is the most recent item.
	 * 			Returns NULL if no object could be found with the given ID
	 */
	const struct localization_object_t *getObjectData(unsigned char object_id) const;

	/*
	 * Gets the number of packets that were received from the vision system this run of the loop
	 *
	 * @returns See above
	 */
	int getNumPacketsReceivedThisLoop() const;

private:
	/*
	 * Creates the LocalizationSystem object.
	 * No parsing of network packets happens yet, and the network
	 * will not be configured yet either.
	 */
	LocalizationSystem();

	friend class KorebotAI;
};

#endif

