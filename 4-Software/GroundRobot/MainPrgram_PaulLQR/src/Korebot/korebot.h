#ifndef _KOREBOT_KOREBOT_H
#define _KOREBOT_KOREBOT_H

#include "Network/network.h"
#include "Network/tcp_connection.h"
#include "Network/packet_handler.h"
#include "Network/packets/robot_identification.h"
#include "Network/packets/profiler_packet.h"

#define MENU_MAIN_MENU	0
#define MENU_AI_SELECT	1

class Korebot : public PacketHandler
{
private:
	// Korebot is a singleton
	static Korebot *s_instance;

	// The connection to the control server
	TCP_Connection *control_server_connection;

	UDP_Connection *korebot_peer_connection;

	ProfilerPacket *profiler_packet_handler;

	// The network configuration for the control server connection
	network_config_t control_server_config;
	network_config_t korebot_peer_config;
	network_config_t korebot_broadcast_config;

	unsigned char robot_id;

	char robot_name[MAX_ROBOT_NAME_LEN];

	// The currently active menu
	char menu_mode;
public:
	static Korebot *instance()
	{
		if (s_instance == NULL)
		{
			s_instance = new Korebot();
		}
		return s_instance;
	}

	/*
	 * Frees up any resources used by this korebot
	 */
	~Korebot();

	/*
	 * Initializes the Korebot; Attempts to connect to
	 * servers, sends korebot identification packets, etc.
	 *
	 * @returns 1 on success, 0 otherwise
	 */
	int init();

	/*
	 * Shuts the korebot down.  Stops the motors, releases any
	 * control it has over devices, frees any memory it is using,
	 * and destroys the Korebot object.
	 */
	void shutdown();

	/*
	 * Starts the main Korebot loop
	 */
	void start();

	/*
	 * Prints & processes commands from the console menu
	 */
	void menu();

	/*
	 * Prints the main menu
	 */
	void mainMenu();

	/*
	 * Prints the status of various modules on the Korebot
	 */
	void printStatus();

	/*
	 * When a packet is received on the network, this method is called to
	 * process the packet.
	 *
	 * @param con    The connection the packet was received on
	 * @param packet The packet that was received from the network
	 */
	void processPacket(TCP_Connection *con, BasePacket *packet);

	/*
	 * When a packet is received on the network, this method is called to
	 * process the packet.
	 *
	 * @param con    The connection the packet was received on
	 * @param packet The packet that was received from the network
	 */
	void processPacket(UDP_Connection *con, BasePacket *packet);

	/*
	 * Gets the connection to the control server, so other parts of
	 * the program can send information to the control server.
	 *
	 * @returns The connection to teh control server
	 */
	TCP_Connection *getControlServerConnection();


	/*
	 * Gets the connection to peer Korebots, so other parts of
	 * the program can communicate with other Korebots.
	 *
	 * @returns The connection to peer Korebots
	 */
	UDP_Connection *getKorebotPeerConnection();

	/*
	 * Gets the korebot ID associated with this korebot
	 *
	 * @returns The ID of this korebot
	 */
	unsigned char getRobotID();

	/*
	 * Gets the name of the robot associated with this korebot
	 *
	 * @returns The name of the robot
	 */
	const char *getRobotName();

private:

	/*
	 * Initializes the Korebot
	 */
	Korebot();

	/*
	 * Attempts to connect to the control server; If successfull,
	 * sends an RobotIdentification packet so the control server knows
	 * about us.
	 *
	 * @returns 1 on success, 0 otherwise
	 */
	int connectToControlServer();

	friend class KorebotAI;
};

#endif
