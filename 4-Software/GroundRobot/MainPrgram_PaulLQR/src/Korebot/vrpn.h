
#ifndef VRPN_H
#define VRPN_H

#include <string>
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"

static const std::string vrpnIP = "192.168.0.196";
static const int portNum = 60560;
static const int buffLen = 8;

/***** struct to hold the VRPN data when received *****/
typedef struct
{
    double usec;
    float x;
    float y;
    float z;
    float yaw;
    float pitch;
    float roll;
} vrpn_data_t;

void vrpn_init(std::string connectionName, void (*)(void*, const vrpn_TRACKERCB));

void vrpn_go();

#endif
