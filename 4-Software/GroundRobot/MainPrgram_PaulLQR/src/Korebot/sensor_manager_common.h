#ifndef _KOREBOT_SENSOR_MANAGER_COMMON_H
#define _KOREBOT_SENSOR_MANAGER_COMMON_H

#define MAX_SENSOR_NAME_LEN	64

#include "common.h"

struct motor_encoder_info_t {
	long speed;
	long position;
	long current;
};

// The list of available sensors
struct sensor_info_t {
	// A unique ID to differentiate each sensor from the rest of the sensors
	short id;

	// The human-readable name of the sensor
	char name[MAX_SENSOR_NAME_LEN];

	// The IO port number of the sensor on the KoreIO board
	int  port;

	// 1 if the korebot should send the sensor data for this sensor to the client, false otherwise
	int sending_data;

	// The timestamp of the last read sensor value
	float timestamp;

	// The values read in, in order of 0~farthest in the past, SENSOR_HISTORY-1 ~ Last read value
	long values[SENSOR_HISTORY];
};

#endif
