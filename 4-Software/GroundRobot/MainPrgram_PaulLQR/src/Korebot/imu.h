/* IMU.h
 * The functions listed below are the public functions open to the user
   Author: Seth Beinhart
	beinhart@iastate.edu
*/
#ifndef _ERIS_IMU_CONTROLLER_H
#define _ERIS_IMU_CONTROLLER_H

#include <stdio.h>
#include "serial.h"
typedef  unsigned short int usInt16;
typedef  short int Int16;

struct STATUS{
	int Mode;
	float Freq;
	usInt16 Gyro_Z_Bias;
	usInt16 Gyro_Y_Bias;
	usInt16 Gyro_X_Bias;
	usInt16 Accel_Z_Bias;
	usInt16 Accel_Y_Bias;
	usInt16 Accel_X_Bias;
	
};

typedef struct STATUS IMU_STATUS;
struct IMU_DATA{
	BYTE  ACTIVE_CHANNELS;
	Int16 pitch;   // -360 to 360
	Int16 roll;
	double gyro_Z; //degrees/s
	double gyro_Y;
	double gyro_X;
	double accel_Z;// m/s^2
	double accel_Y;
	double accel_X;
};

typedef struct IMU_DATA IMU_SENSOR_DATA;
int Set_Gyro_Bias(usInt16 Z_Bias, usInt16 Y_Bias,usInt16 X_Bias, int SetZ,int SetY,int SetX);
int Set_Accel_Bias(usInt16 Z_Bias, usInt16 Y_Bias,usInt16 X_Bias, int SetZ,int SetY,int SetX);
int Set_ACTIVE_CHANNELS(BYTE pChannels);
int Self_Calibrate_Gyro(int pmax_retrys);
BYTE IMU_Self_Test();
int Get_Status(IMU_STATUS * pIn, int update);
int initIMU();
int deinitIMU();
int Set_Mode(int pmode, float freq);
int Manual_Sensor_Update(void);
IMU_SENSOR_DATA* Get_IMU_SENSOR_DATA_STRUCT();

#endif
