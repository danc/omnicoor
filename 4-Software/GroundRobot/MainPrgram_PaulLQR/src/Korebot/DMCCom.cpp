/*DMCCom.cpp : Wraps up the main communication functions with the FIFO's and status registers.
@author: Seth Beinhart

FOR USE with ERIS BASED SYSTEMS
*/

#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/io.h>
#include <sys/stat.h>
#include <stdlib.h>


//#include "IO5.H"
#include "4i65_defines.h"
#include "DMCCom.h"



static uword P_FPGAData, P_FPGAIOControlnStatus32 ;
static void findpcicard(unsigned cardnum);
void read_all(void);
uword read_register(uword addr);




/** Function: Initlilizes the Address of the Fpga and the FIFO's  It also Sends 0x0000 to the SRA to enable the ICDFIFO
**/
void init_mcont()
{
	findpcicard(0);		//Sets up P_FPGAData
 	P_ICDFIFO= P_FPGAData;
 	P_QCDFIFO= P_FPGAData+4;
 	P_SRA= P_FPGAData+8;
 	P_SRB= P_FPGAData+12;
	outw(0x0000, P_SRA);
}




/************************************************************************************************************/
/* THE following are the Write_Command functions. Note write_quad is not implimented since it is not needed*/
/**********************************************************************************************************/



/* Function: DOESNT ALLOWS USER TO SPECIFY FIFO Sends a 16 bit write_word cmd followed by the data stored in val to the address specifed by addr.  
   Input: axis- The motor axis the parameter is associated with
	  parameter- The parameter we wish to alter. Note: should be directly taken from a mesa net header
	  val- Data to write to the parameter. CAST IS REQUIRED for all signed data.

  Return: 0 upon successful write to the fifo
          -1 indicates failure. Likly due to FIFO overflow... retry later

SoftDMC Vars that should utilize this are any unsiged 16 bit or less vars including:
FLAG
UINT
INT // Cast Required
*/

int write_word(char axisvar, uword parameter, uword val)
{
	if(write_word_FIFO(axisvar,parameter, val, P_ICDFIFO)!=-1){
		
		return 0;
	}
	else{	// Couldnt write to ICDFIFO try QCDFIFO
		return(write_word_FIFO(axisvar,parameter, val, P_QCDFIFO));
	}	
}





/* Function: ALLOWS USER TO SPECIFY FIFO Sends a 16 bit  write_word cmd followed by the  data stored in val to the address specifed by addr.  
   Input: axis- The motor axis the parameter is associated with
	  parameter- The parmeter we wish to alter. Note: should be directly taken from a mesa net header
	  val- Data to write to the parameter. CAST IS REQUIRED for all signed data
	  addr- Fifo we wish to write to;
  Return: 0 upon successful write to the fifo
          -1 indicates failure. Likly due to FIFO overflow... retry later

SoftDMC Vars that should utilize this are any 16 bit or less vars including:
FLAG
UINT
INT //Cast Required
*/

int write_word_FIFO(char axisvar, uword parameter, uword val, uword addr){


	if(axisvar>=4 ||  parameter>=0x400){
		return  -1;
	}
	// see if ICDFIFO is half full If it is there may not be enough room return -1
	if(addr==P_ICDFIFO){
		if(read_register(P_SRA)&0x4000)	
		return -1;
	}
	// see if QCDFIFO is half full If it is there may not be enough room return -1
	if(addr==P_QCDFIFO){
		if(read_register(P_SRA)&0x1000)	
		return -1;
	}
	
	// There is enough room on the fifo go ahead and write it.

	// create packet
	uword cmd = 0x8000; //enables write word 100xxxxxxxxxxxxx
	//merge parm and axis
	cmd = (cmd | (((0x7) & axisvar)<<10) | ((0x3FF)&parameter));  // places axis and paramter in proper locations in packet

	//write out commands
	//printf("**Sending: %x -> ADR:%x **\n", cmd, addr);
	outw(cmd, addr);	// Send Write command		
	//printf("**Sending: %x -> ADR:%x **\n", val, addr);
	outw(val, addr);	// Send Write least significant 16 bits of 32 bit parameter to ICD FIFO
	return 0;

}





/* Function: DOESNT ALLOWS USER TO SPECIFY FIFO Sends a 16 bit write_word cmd followed by the data stored in val to the address specifed by addr.  
   Input: axis- The motor axis the parameter is associated with
	  parameter- The parmeter we wish to alter. Note: should be directly taken from a mesa net header
	  val-  Data to write to the parameter (32 bit)  CAST IS REQUIRED for all signed data.

  Return: 0 upon successful write to the fifo
          -1 indicates failure. Likly due to FIFO overflow... retry later

SoftDMC Vars that should utilize this are any 16 bit or less vars including:
ulong
long //Cast required
*/


int write_long(char axisvar, uword parameter, udword val)
{
	if(write_long_FIFO(axisvar,parameter, val, P_ICDFIFO)!=-1){
		
		return 0;
	}
	else{	// Couldnt write to ICDFIFO try QCDFIFO
		return(write_long_FIFO(axisvar,parameter, val, P_QCDFIFO));
	}	
}





/* Function: ALLOWS USER TO SPECIFY FIFO Sends a 16 bit  write_word cmd followed by the data stored in val to the address specifed by addr.  
   Input: axis- The motor axis the parameter is associated with
	  parameter- The parmeter we wish to alter. Note: should be directly taken from a mesa net header
	  val- binary data sent to the FPGA a CAST IS REQUIRED for any signed data.
	  addr- Fifo we wish to write to;
  Return: 0 upon successful write to the fifo
          -1 indicates failure. Likly due to FIFO overflow... retry later

SoftDMC Vars that should utilize this are any 16 bit or less vars including:
ulong
long // Cast required

*/

int write_long_FIFO(char axisvar, uword parameter, udword val, uword addr){

		if(axisvar>=4 ||  parameter>=0x400){
		return  -1;
	}
	// see if ICDFIFO is half full If it is ther may not be enough room return -1
	if(addr==P_ICDFIFO){
		if(read_register(P_SRA)&0x4000)	
		return -1;
	}
	// see if QCDFIFO is half full If it is ther may not be enough room return -1
	if(addr==P_QCDFIFO){
		if(read_register(P_SRA)&0x1000)	
		return -1;
	}
	
	// There is enough room on the fifo go ahead and write it.

	// create packet
	uword cmd = 0xA000; //enables write long 101xxxxxxxxxxxxx
	//merge parm and axis
	cmd = (cmd | (((0x7) & axisvar)<<10) | ((0x3FF)&parameter));  // places axis and paramter in proper locations in packet

	//write out commands
//	printf("**Sending: %x -> ADR:%x **\n", cmd, addr);
	outw(cmd, addr);	// Send Write command	

	uword LSW=val&0xFFFF;
	uword MSW=val>>16;

//	printf("**Sending: %x -> ADR:%x **\n", LSW, addr);
	outw(LSW, addr);	// Send Write least significant 16 bits of 32 bit parameter to ICD FIFO
//	printf("**Sending: %x -> ADR:%x **\n", MSW, addr);
	outw(MSW, addr);	// Send Write least significant 16 bits of 32 bit parameter to ICD FIFO
	return 0;

}




/****************************************************************************************************/
/* THE following are the Read_Command functions. Note Read_quad is not implimented since it is not needed*/
/*********************************************************************************************************/

/* Function: Doesn't ALLOW USER TO SPECIFY FIFO Sends a 16 bit  read_word cmd  to the address specifed by addr.
   Input: axis- The motor axis the parameter is associated with
	  parameter- The parmeter we wish to alter. Note: should be directly taken from a mesa net header
	  ret- pointer to where the data is to be stored
  Return: 0 upon successful write to the fifo
          -1 indicates failure. Likly due to FIFO overflow... retry later

SoftDMC Vars that should utilize this are any 16 bit or less vars including:
FLAG
UINT
INT //Cast of return value required

*/
int read_word(char axisvar, uword parameter,uword* pret)
{
	if(read_word_FIFO(axisvar,parameter,pret, P_ICDFIFO) != -1){
//		printf("here\n");
		return 0;
	}
	else{	// Couldnt write to ICDFIFO try QCDFIFO
		return(read_word_FIFO(axisvar,parameter,pret, P_QCDFIFO));
	}	
}


/* Function: ALLOWS USER TO SPECIFY FIFO Sends a 16 bit  read_word cmd  to the address specifed by addr.  
   Input: axis- The motor axis the parameter is associated with
	  parameter- The parmeter we wish to alter. Note: should be directly taken from a mesa net header
	  ret- pointer to where the data is to be stored
	  addr- Fifo we wish to write to;
  Return: 0 upon successful write to the fifo
          -1 indicates failure. Likly due to FIFO overflow... retry later

SoftDMC Vars that should utilize this are any  16 bit or less vars including:
FLAG
UINT
INT //Cast of return valur required
*/
int read_word_FIFO(char axisvar, uword parameter,uword *pret, uword addr){

		if(axisvar>=4 ||  parameter>=0x400){
		return  -1;
	}
	// see if ICDFIFO is half full If it is ther may not be enough room return -1
	if(addr==P_ICDFIFO){
		if(read_register(P_SRA)&0x4000)
		return -1;
	}
	// see if QCDFIFO is half full If it is ther may not be enough room return -1
	if(addr==P_QCDFIFO){
		if(read_register(P_SRA)&0x1000)	
		return -1;
	}
	
	// There is enough room on the fifo go ahead and write it.

	// create packet
	uword cmd = 0x0; //enables read word 000xxxxxxxxxxxxx
	//merge parm and axis
	cmd = (cmd | (((0x7) & axisvar)<<10) | ((0x3FF)&parameter));  // places axis and paramter in proper locations in packet

	//write out commands
//	printf("**Sending: %x -> ADR:%x **\n", cmd, addr);
	outw(cmd, addr);	// Send read command	


	if(addr==P_ICDFIFO){
		while((0x800&read_register(P_SRA)))	//busy wait to see if IRE is False
		{}
		*pret=read_register(P_ICDFIFO);
		return(0);	
		
	}
	else if(addr==P_QCDFIFO){
		while((0x200&read_register(P_SRA)))	//busy wait to see if IRE is False
		{}
		
		*pret=read_register(P_QCDFIFO);
		return(0);	
	}
	else{
//		printf("what the heck\n");
	}
	return -1;
}

/* Function: Doent ALLOWS USER TO SPECIFY FIFO Sends a 16 bit  read_word cmd  to the address specifed by addr.
	     Attempts to put it on the ICDFIFO if it can it trys QCD  
   Input: axis- The motor axis the parameter is associated with
	  parameter- The parmeter we wish to alter. Note: should be directly taken from a mesa net header
	  ret- pointer to where the data is to be stored (32Bits)
  Return: 0 upon successful write to the fifo
          -1 indicates failure. Likly due to FIFO overflow... retry later

SoftDMC Vars that should utilize this are any 16 bit or less vars including:
ulong 
long //Cast of return value required
*/
int read_long(char axisvar, uword parameter,udword* pret)
{
	if(read_long_FIFO(axisvar,parameter,pret, P_ICDFIFO) != -1){
		return 0;
	}
	else{	// Couldnt write to ICDFIFO try QCDFIFO
		return(read_long_FIFO(axisvar,parameter,pret, P_QCDFIFO));
	}	
}



/* Function: ALLOWS USER TO SPECIFY FIFO Sends a 16 bit  read_word cmd  to the address specifed by addr.  
   Input: axis- The motor axis the parameter is associated with
	  parameter- The parmeter we wish to alter. Note: should be directly taken from a mesa net header
	  ret- pointer to where the data is to be stored(32 bit)
	  addr- Fifo we wish to write to;
  Return: 0 upon successful write to the fifo
          -1 indicates failure. Likly due to FIFO overflow... retry later

SoftDMC Vars that should utilize this are any 16 bit or less vars including:
ulong 
long //Cast of return value required

*/

int read_long_FIFO(char axisvar, uword parameter,udword *pret, uword addr){

		if(axisvar>=4 ||  parameter>=0x400){
	
		return  -1;
	}
	// see if ICDFIFO is half full If it is ther may not be enough room return -1
	if(addr==P_ICDFIFO){
		if(read_register(P_SRA)&0x4000)
		return -1;
	}
	// see if QCDFIFO is half full If it is ther may not be enough room return -1
	if(addr==P_QCDFIFO){
		if(read_register(P_SRA)&0x1000)	
		return -1;
	}
	
	// There is enough room on the fifo go ahead and write it.

	// create packet
	uword cmd = 0x2000; //enables read word 000xxxxxxxxxxxxx
	//merge parm and axis
	cmd = (cmd | (((0x7) & axisvar)<<10) | ((0x3FF)&parameter));  // places axis and paramter in proper locations in packet

	//write out commands
//	printf("**Sending: %x -> ADR:%x **\n", cmd, addr);
	outw(cmd, addr);	// Send read command	

	uword LSW;
	uword MSW;
	if(addr==P_ICDFIFO){
		while((0x800&read_register(P_SRA)))	//busy wait to see if IRE is False
		{}
		LSW=read_register(P_ICDFIFO);
		while((0x800&read_register(P_SRA)))	//busy wait to see if IRE is False
		{}
		MSW=read_register(P_ICDFIFO);
		*pret=MSW;
		*pret=*pret<<16;
		*pret=*pret| LSW;
	
		return(0);	
		
	}
	else if(addr==P_QCDFIFO){
		while((0x200&read_register(P_SRA)))	//busy wait to see if IRE is False
		{}
		LSW=read_register(P_QCDFIFO);

		while((0x800&read_register(P_SRA)))	//busy wait to see if IRE is False
		{}
		MSW=read_register(P_ICDFIFO);
		*pret=MSW;
		*pret=*pret<<16;
		*pret=*pret|LSW;

		return(0);	
	}
	else{
//		printf("what the heck\n");
	}
	return -1;
}



/**********************************************************************************************************************/
/***************** PRIVATE FUNCTIONS this functions are for internal use only*****************************************/
/********************************************************************************************************************/
/** Function: Reads and prints the current values stored int Global Adress's for The FIFOS and SR.
**/


void read_all(void){
	printf("Reading ICD Fifo:\n");

	
	printf("\t %x\n", read_register(P_ICDFIFO));
	printf("Reading QCD Fifo:\n");
	printf("\t %x\n", read_register(P_QCDFIFO));
	printf("Reading Status A:\n");
	printf("\t %x\n", read_register(P_SRA));
	printf("Reading Status B:\n");
	printf("\t %x\n", read_register(P_SRB));	
}


/**Function: Reads 1 of the 4 16 bit registers we have access to on the fpga. Note it is restricted to this address's
but undefined results will be returned
**/

uword read_register(uword addr){

	uword rtn =  inw(addr);
	return rtn;	

}


/* ---- findpcicard function from MesaNet SC5I20L.C Linux uploader */

static void findpcicard(unsigned cardnum)

{
	udword vendordev, busdevfunc ;
	udword bases[6] ;

	int numfields ;

	unsigned
		i, numlines ;

	size_t linelen ;

	FILE *f ;

	char namebuf[256 + 1] ;
	char *lineptr ;


	if(snprintf(namebuf, sizeof(namebuf), "%s/devices", PATH_PROC_BUS_PCI) >= sizeof(namebuf))
	{
		//fatalerror(EC_NTRNL, "File name too long: %s...", PATH_PROC_BUS_PCI) ;
	}
	if((f = fopen(namebuf, "r")) == 0)
	{
		/* (Shouldn't happen.)
		*/
		//fatalerror(EC_SYS, "File not found: %s", namebuf) ;
	}

	lineptr = 0 ;
	linelen = 0 ;
	numlines = 0 ;
	i = cardnum ;
	for( ; ; )
	{
		if(getline(&lineptr, &linelen, f) < 0)
		{
			free(lineptr) ;
			if(numlines == 0)
			{
				//fatalerror(EC_SYS, "Unexpected end of file in %s", namebuf) ;
			}
			else
			{
				break ;
			}
		}
		++numlines ;

		/* Bus/dev/func vendor/device IRQ base0 base1 base2 base3 base4 base5...
		*/
		numfields = sscanf(lineptr, "%x %lx %*x  %lx %lx %lx %lx %lx %lx",
						   &busdevfunc, &vendordev,
						   &bases[0], &bases[1], &bases[2], &bases[3], &bases[4], &bases[5]) ;
		if(numfields < 8)
		{
			free(lineptr) ;
			//fatalerror(EC_SYS, "Incomplete PCI device information found: %s", namebuf) ;
		}
		if(vendordev == ((VENDORID5I20 << 16) | DEVICEID5I20))
		{
			if(i++ != cardnum)
			{
				continue ;
			}

			fclose(f) ;
			free(lineptr) ;

			printf("\nUsing PCI device at bus %u/device %u/function %u.",
				   (busdevfunc >> 8), ((busdevfunc >> 3) & 0x1F), (busdevfunc & 0x07)) ;
			P_FPGAIOControlnStatus32 = ((uword)bases[BRNUM_5I20LCLCFGIO] & ~0x3) ;
			printf("\n\nPLX 9030 configuration I/O base port: 0x%04hX", P_FPGAIOControlnStatus32) ;
			P_FPGAIOControlnStatus32 += R_5I20CONTROLNSTATUS ;
			printf("\nPLX 9030 control/status port:         0x%04hX", P_FPGAIOControlnStatus32) ;
			P_FPGAData = ((uword)bases[BRNUM_5I20IO16] & ~0x3) ;
			printf("\nPLX 9030 16-bit I/O base:             0x%04hX", P_FPGAData) ;
			printf("\nPLX 9030 32-bit I/O base:             0x%04hX", ((uword)bases[BRNUM_5I20IO32] & ~0x3)) ;
			putchar('\n') ;
			fflush(stdout) ;

			/* Enable direct access to the I/O port range.

			   Don't try this without adult supervision!
			*/
			if(iopl(3) < 0) /* Danger, Will Robinson!  Danger!  Danger! */
			{
				printf("Can't enable direct port access.");
				//fatalerror(EC_SYS, "Can't enable direct port access.") ;
			}

			return ;
		}
	}
	printf("Can't find 5I20 card number\n");
	//fatalerror(EC_USER, "Can't find 5I20 card number %u.", cardnum) ;
}
