#include <stdio.h>
#include <string.h>
#include "motor_controller.h"
#include "configuration.h"
#include "korebot.h"
#include "profiler.h"
#include "ai.h"
#include "Network/packets/all_packets.h"
#include "localization_system.h"
#include "DMCCom.h"
#include <time.h>
MotorController *MotorController::s_instance = NULL;

/*
 * Creates the MotorController object.
 * Nothing is requested of the motor controller board at this ponint.
 */
MotorController::MotorController()
{
    memset(&motors, 0, sizeof(motortrain_t));
    memset(&motor_speeds, 0, sizeof(motor_control_manual_t));
    memset(&current_motor_speeds, 0, sizeof(motor_control_manual_t));
    memset(&current_speed_control_settings, 0, sizeof(motor_control_speed_t));
    memset(&current_manual_control_settings, 0, sizeof(motor_control_manual_t));

    current_mode = MOTOR_CONTROL_MODE_RELEASE;

    use_bounds_limits = 0;
    use_status_logging = 0;
    e_stop = 0;
}

/*
 * Frees any resources used by the motor controller.
 */
MotorController::~MotorController()
{
    stop();
    releaseMotors();
}

int MotorController::quadInit()
{
    init_mcont();
    // Left Motor
    Configuration::instance()->getValue("motortrain.left.knet_dev_name", motors.left.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.left.sample_time         = Configuration::instance()->getValue("motortrain.left.sample_time",              (int)0);
    motors.left.speed_multiplier      = Configuration::instance()->getValue("motortrain.left.speed_multiplier",           (int)0);
    motors.left.pid_kk                = Configuration::instance()->getValue("motortrain.left.pid_kk",                 (int)0);
    motors.left.pid_kff               = Configuration::instance()->getValue("motortrain.left.pid_kff",                (int)0);
    motors.left.pid_kp                = Configuration::instance()->getValue("motortrain.left.pid_kp",                 (int)0);
    motors.left.pid_kd                = Configuration::instance()->getValue("motortrain.left.pid_kd",                 (int)0);
    motors.left.pid_ka                = Configuration::instance()->getValue("motortrain.left.pid_ka",                 (int)0);
    motors.left.pid_ki                = Configuration::instance()->getValue("motortrain.left.pid_ki",                 (int)0);
    motors.left.pid_kih               = Configuration::instance()->getValue("motortrain.left.pid_kih",                (int)0);
    motors.left.pid_kil               = Configuration::instance()->getValue("motortrain.left.pid_kil",                (int)0);
    motors.left.pid_kf0               = Configuration::instance()->getValue("motortrain.left.pid_kf0",                (int)0);
    motors.left.pid_kf1               = Configuration::instance()->getValue("motortrain.left.pid_kf1",                (int)0);
    motors.left.pid_kf2               = Configuration::instance()->getValue("motortrain.left.pid_kf2",                (int)0);
    motors.left.pid_kf3               = Configuration::instance()->getValue("motortrain.left.pid_kf3",                (int)0);
    motors.left.pid_kdfil             = Configuration::instance()->getValue("motortrain.left.pid_kdfil",              (int)0);
    motors.left.pid_driveplus         = Configuration::instance()->getValue("motortrain.left.pid_driveplus",          (int)0);
    motors.left.pid_driveminus        = Configuration::instance()->getValue("motortrain.left.pid_minus",              (int)0);
    motors.left.margin                = Configuration::instance()->getValue("motortrain.left.margin",                       (int)0);
    //motors.left.current_limit       = Configuration::instance()->getValue("motortrain.left.current_limit",              (int)0);
    motors.left.pid_accel             = Configuration::instance()->getValue("motortrain.left.pid_accel",                (int)0);
    motors.left.pid_accelf            = Configuration::instance()->getValue("motortrain.left.pid_accelf",               (int)0);
    motors.left.pid_slewlimit         = Configuration::instance()->getValue("motortrain.left.pid_slewlimit",          (int)0);
    motors.left.pid_maxpwm            = Configuration::instance()->getValue("motortrain.left.pid_maxpwm",               (int)0);
    //motors.left.encoder_resolution  = Configuration::instance()->getValue("motortrain.left.encoder_resolution",       (int)0);
    motors.left.max_motor_speed       = Configuration::instance()->getValue("motortrain.left.max_motor_speed",            (int)0);

    // Right Motor
    Configuration::instance()->getValue("motortrain.right.knet_dev_name", motors.right.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.right.sample_time        = Configuration::instance()->getValue("motortrain.right.sample_time",            (int)0);
    motors.right.speed_multiplier     = Configuration::instance()->getValue("motortrain.right.speed_multiplier",         (int)0);
    motors.right.pid_kk               = Configuration::instance()->getValue("motortrain.right.pid_kk",               (int)0);
    motors.right.pid_kff              = Configuration::instance()->getValue("motortrain.right.pid_kff",              (int)0);
    motors.right.pid_kp               = Configuration::instance()->getValue("motortrain.right.pid_kp",               (int)0);
    motors.right.pid_kd               = Configuration::instance()->getValue("motortrain.right.pid_kd",               (int)0);
    motors.right.pid_ka               = Configuration::instance()->getValue("motortrain.right.pid_ka",               (int)0);
    motors.right.pid_ki               = Configuration::instance()->getValue("motortrain.right.pid_ki",               (int)0);
    motors.right.pid_kih              = Configuration::instance()->getValue("motortrain.right.pid_kih",              (int)0);
    motors.right.pid_kil              = Configuration::instance()->getValue("motortrain.right.pid_kil",              (int)0);
    motors.right.pid_kf0              = Configuration::instance()->getValue("motortrain.right.pid_kf0",              (int)0);
    motors.right.pid_kf1              = Configuration::instance()->getValue("motortrain.right.pid_kf1",              (int)0);
    motors.right.pid_kf2              = Configuration::instance()->getValue("motortrain.right.pid_kf2",              (int)0);
    motors.right.pid_kf3              = Configuration::instance()->getValue("motortrain.right.pid_kf3",              (int)0);
    motors.right.pid_kdfil            = Configuration::instance()->getValue("motortrain.right.pid_kdfil",            (int)0);
    motors.right.pid_driveplus        = Configuration::instance()->getValue("motortrain.right.pid_driveplus",        (int)0);
    motors.right.pid_driveminus       = Configuration::instance()->getValue("motortrain.right.pid_minus",            (int)0);
    motors.right.margin               = Configuration::instance()->getValue("motortrain.right.margin",                     (int)0);
    //motors.right.current_limit      = Configuration::instance()->getValue("motortrain.right.current_limit",            (int)0);
    motors.right.pid_accel            = Configuration::instance()->getValue("motortrain.right.pid_accel",              (int)0);
    motors.right.pid_accelf           = Configuration::instance()->getValue("motortrain.right.accelf",                 (int)0);
    motors.right.pid_slewlimit        = Configuration::instance()->getValue("motortrain.right.pid_slewlimit",        (int)0);
    motors.right.pid_maxpwm           = Configuration::instance()->getValue("motortrain.right.pid_maxpwm",             (int)0);
    //motors.right.encoder_resolution = Configuration::instance()->getValue("motortrain.right.encoder_resolution",     (int)0);
    motors.right.max_motor_speed      = Configuration::instance()->getValue("motortrain.right.max_motor_speed",          (int)0);


    // Front Motor
    Configuration::instance()->getValue("motortrain.front.knet_dev_name", motors.front.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.front.sample_time        = Configuration::instance()->getValue("motortrain.front.sample_time",            (int)0);
    motors.front.speed_multiplier     = Configuration::instance()->getValue("motortrain.front.speed_multiplier",         (int)0);
    motors.front.pid_kk               = Configuration::instance()->getValue("motortrain.front.pid_kk",                 (int)0);
    motors.front.pid_kff              = Configuration::instance()->getValue("motortrain.front.pid_kff",                (int)0);
    motors.front.pid_kp               = Configuration::instance()->getValue("motortrain.front.pid_kp",                 (int)0);
    motors.front.pid_kd               = Configuration::instance()->getValue("motortrain.front.pid_kd",                 (int)0);
    motors.front.pid_ka               = Configuration::instance()->getValue("motortrain.front.pid_ka",                 (int)0);
    motors.front.pid_ki               = Configuration::instance()->getValue("motortrain.front.pid_ki",                 (int)0);
    motors.front.pid_kih              = Configuration::instance()->getValue("motortrain.front.pid_kih",                (int)0);
    motors.front.pid_kil              = Configuration::instance()->getValue("motortrain.front.pid_kil",                (int)0);
    motors.front.pid_kf0              = Configuration::instance()->getValue("motortrain.front.pid_kf0",                (int)0);
    motors.front.pid_kf1              = Configuration::instance()->getValue("motortrain.front.pid_kf1",                (int)0);
    motors.front.pid_kf2              = Configuration::instance()->getValue("motortrain.front.pid_kf2",                (int)0);
    motors.front.pid_kf3              = Configuration::instance()->getValue("motortrain.front.pid_kf3",                (int)0);
    motors.front.pid_kdfil            = Configuration::instance()->getValue("motortrain.front.pid_kdfil",              (int)0);
    motors.front.pid_driveplus        = Configuration::instance()->getValue("motortrain.front.pid_driveplus",          (int)0);
    motors.front.pid_driveminus       = Configuration::instance()->getValue("motortrain.front.pid_minus",              (int)0);
    motors.front.margin               = Configuration::instance()->getValue("motortrain.front.margin",             (int)0);
    //motors.front.current_limit      = Configuration::instance()->getValue("motortrain.front.current_limit",    (int)0);
    motors.front.pid_accel            = Configuration::instance()->getValue("motortrain.front.pid_accel",              (int)0);
    motors.front.pid_accelf           = Configuration::instance()->getValue("motortrain.front.pid_accelf",             (int)0);
    motors.front.pid_slewlimit        = Configuration::instance()->getValue("motortrain.front.pid_slewlimit",          (int)0);
    motors.front.pid_maxpwm           = Configuration::instance()->getValue("motortrain.front.pid_maxpwm",             (int)0);
    //motors.front.encoder_resolution = Configuration::instance()->getValue("motortrain.front.encoder_resolution", (int)0);
    motors.front.max_motor_speed      = Configuration::instance()->getValue("motortrain.front.max_motor_speed",    (int)0);

    // Rear Motor
    Configuration::instance()->getValue("motortrain.rear.knet_dev_name", motors.rear.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.rear.sample_time         = Configuration::instance()->getValue("motortrain.rear.sample_time",        (int)0);
    motors.rear.speed_multiplier      = Configuration::instance()->getValue("motortrain.rear.speed_multiplier",   (int)0);
    motors.rear.pid_kk                = Configuration::instance()->getValue("motortrain.rear.pid_kk",         (int)0);
    motors.rear.pid_kff               = Configuration::instance()->getValue("motortrain.rear.pid_kff",                (int)0);
    motors.rear.pid_kp                = Configuration::instance()->getValue("motortrain.rear.pid_kp",                 (int)0);
    motors.rear.pid_kd                = Configuration::instance()->getValue("motortrain.rear.pid_kd",                 (int)0);
    motors.rear.pid_ka                = Configuration::instance()->getValue("motortrain.rear.pid_ka",                 (int)0);
    motors.rear.pid_ki                = Configuration::instance()->getValue("motortrain.rear.pid_ki",                 (int)0);
    motors.rear.pid_kih               = Configuration::instance()->getValue("motortrain.rear.pid_kih",                (int)0);
    motors.rear.pid_kil               = Configuration::instance()->getValue("motortrain.rear.pid_kil",                (int)0);
    motors.rear.pid_kf0               = Configuration::instance()->getValue("motortrain.rear.pid_kf0",                (int)0);
    motors.rear.pid_kf1               = Configuration::instance()->getValue("motortrain.rear.pid_kf1",                (int)0);
    motors.rear.pid_kf2               = Configuration::instance()->getValue("motortrain.rear.pid_kf2",                (int)0);
    motors.rear.pid_kf3               = Configuration::instance()->getValue("motortrain.rear.pid_kf3",                (int)0);
    motors.rear.pid_kdfil             = Configuration::instance()->getValue("motortrain.rear.pid_kdfil",              (int)0);
    motors.rear.pid_driveplus         = Configuration::instance()->getValue("motortrain.rear.pid_driveplus",          (int)0);
    motors.rear.pid_driveminus        = Configuration::instance()->getValue("motortrain.rear.pid_minus",              (int)0);
    motors.rear.margin                = Configuration::instance()->getValue("motortrain.rear.margin",             (int)0);
    //motors.rear.current_limit       = Configuration::instance()->getValue("motortrain.rear.current_limit",    (int)0);
    motors.rear.pid_accel             = Configuration::instance()->getValue("motortrain.rear.pid_accel",              (int)0);
    motors.rear.pid_accelf            = Configuration::instance()->getValue("motortrain.rear.pid_accelf",             (int)0);
    motors.rear.pid_slewlimit         = Configuration::instance()->getValue("motortrain.rear.pid_slewlimit",          (int)0);
    motors.rear.pid_maxpwm            = Configuration::instance()->getValue("motortrain.rear.pid_maxpwm",             (int)0);
    //motors.rear.encoder_resolution  = Configuration::instance()->getValue("motortrain.rear.encoder_resolution", (int)0);
    motors.rear.max_motor_speed       = Configuration::instance()->getValue("motortrain.rear.max_motor_speed",    (int)0);

    setMotors();

    return 1;
}


/*
 * Initializes the MotorController.  If the mode is anything other
 * than free, then the controller will attempt to acquire the motor
 * device descriptors as defined in the Configuration object.
 *
 * @returns 1 on success, 0 otherwise
 */
int MotorController::init()
{
    // Load in values from config file
    //ERIS CODE ADDED HERE
    init_mcont();

    // Left Motor
    Configuration::instance()->getValue("motortrain.left.knet_dev_name", motors.left.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.left.sample_time             = Configuration::instance()->getValue("motortrain.left.sample_time",            (int)0);
    motors.left.speed_multiplier        = Configuration::instance()->getValue("motortrain.left.speed_multiplier",       (int)0);
    motors.left.pid_kk                  = Configuration::instance()->getValue("motortrain.left.pid_kk",                 (int)0);
    motors.left.pid_kff                 = Configuration::instance()->getValue("motortrain.left.pid_kff",                (int)0);
    motors.left.pid_kp                  = Configuration::instance()->getValue("motortrain.left.pid_kp",                 (int)0);
    motors.left.pid_kd                  = Configuration::instance()->getValue("motortrain.left.pid_kd",                 (int)0);
    motors.left.pid_ka                  = Configuration::instance()->getValue("motortrain.left.pid_ka",                 (int)0);
    motors.left.pid_ki                  = Configuration::instance()->getValue("motortrain.left.pid_ki",                 (int)0);
    motors.left.pid_kih                 = Configuration::instance()->getValue("motortrain.left.pid_kih",                (int)0);
    motors.left.pid_kil                 = Configuration::instance()->getValue("motortrain.left.pid_kil",                (int)0);
    motors.left.pid_kf0                 = Configuration::instance()->getValue("motortrain.left.pid_kf0",                (int)0);
    motors.left.pid_kf1                 = Configuration::instance()->getValue("motortrain.left.pid_kf1",                (int)0);
    motors.left.pid_kf2                 = Configuration::instance()->getValue("motortrain.left.pid_kf2",                (int)0);
    motors.left.pid_kf3                 = Configuration::instance()->getValue("motortrain.left.pid_kf3",                (int)0);
    motors.left.pid_kdfil               = Configuration::instance()->getValue("motortrain.left.pid_kdfil",              (int)0);
    motors.left.pid_driveplus           = Configuration::instance()->getValue("motortrain.left.pid_driveplus",          (int)0);
    motors.left.pid_driveminus          = Configuration::instance()->getValue("motortrain.left.pid_minus",              (int)0);
    motors.left.margin                  = Configuration::instance()->getValue("motortrain.left.margin",                 (int)0);
    //motors.left.current_limit           = Configuration::instance()->getValue("motortrain.left.current_limit",          (int)0);
    motors.left.pid_accel                 = Configuration::instance()->getValue("motortrain.left.pid_accel",              (int)0);
    motors.left.pid_accelf                = Configuration::instance()->getValue("motortrain.left.pid_accelf",             (int)0);
    motors.left.pid_slewlimit           = Configuration::instance()->getValue("motortrain.left.pid_slewlimit",          (int)0);
    motors.left.pid_maxpwm                = Configuration::instance()->getValue("motortrain.left.pid_maxpwm",             (int)0);
    //motors.left.encoder_resolution      = Configuration::instance()->getValue("motortrain.left.encoder_resolution",     (int)0);
    motors.left.max_motor_speed         = Configuration::instance()->getValue("motortrain.left.max_motor_speed",        (int)0);

    // Right Motor
    Configuration::instance()->getValue("motortrain.right.knet_dev_name", motors.right.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.right.sample_time            = Configuration::instance()->getValue("motortrain.right.sample_time",           (int)0);
    motors.right.speed_multiplier       = Configuration::instance()->getValue("motortrain.right.speed_multiplier",      (int)0);
    motors.right.pid_kk                 = Configuration::instance()->getValue("motortrain.right.pid_kk",                (int)0);
    motors.right.pid_kff                = Configuration::instance()->getValue("motortrain.right.pid_kff",               (int)0);
    motors.right.pid_kp                 = Configuration::instance()->getValue("motortrain.right.pid_kp",                (int)0);
    motors.right.pid_kd                 = Configuration::instance()->getValue("motortrain.right.pid_kd",                (int)0);
    motors.right.pid_ka                 = Configuration::instance()->getValue("motortrain.right.pid_ka",                (int)0);
    motors.right.pid_ki                 = Configuration::instance()->getValue("motortrain.right.pid_ki",                (int)0);
    motors.right.pid_kih                = Configuration::instance()->getValue("motortrain.right.pid_kih",               (int)0);
    motors.right.pid_kil                = Configuration::instance()->getValue("motortrain.right.pid_kil",               (int)0);
    motors.right.pid_kf0                = Configuration::instance()->getValue("motortrain.right.pid_kf0",               (int)0);
    motors.right.pid_kf1                = Configuration::instance()->getValue("motortrain.right.pid_kf1",               (int)0);
    motors.right.pid_kf2                = Configuration::instance()->getValue("motortrain.right.pid_kf2",               (int)0);
    motors.right.pid_kf3                = Configuration::instance()->getValue("motortrain.right.pid_kf3",               (int)0);
    motors.right.pid_kdfil              = Configuration::instance()->getValue("motortrain.right.pid_kdfil",             (int)0);
    motors.right.pid_driveplus          = Configuration::instance()->getValue("motortrain.right.pid_driveplus",         (int)0);
    motors.right.pid_driveminus         = Configuration::instance()->getValue("motortrain.right.pid_minus",             (int)0);
    motors.right.margin                 = Configuration::instance()->getValue("motortrain.right.margin",                (int)0);
    //motors.right.current_limit          = Configuration::instance()->getValue("motortrain.right.current_limit",         (int)0);
    motors.right.pid_accel                = Configuration::instance()->getValue("motortrain.right.pid_accel",             (int)0);
    motors.right.pid_accelf               = Configuration::instance()->getValue("motortrain.right.accelf",                (int)0);
    motors.right.pid_slewlimit          = Configuration::instance()->getValue("motortrain.right.pid_slewlimit",         (int)0);
    motors.right.pid_maxpwm               = Configuration::instance()->getValue("motortrain.right.pid_maxpwm",            (int)0);
    //motors.right.encoder_resolution     = Configuration::instance()->getValue("motortrain.right.encoder_resolution",    (int)0);
    motors.right.max_motor_speed        = Configuration::instance()->getValue("motortrain.right.max_motor_speed",       (int)0);


    // Front Motor
    Configuration::instance()->getValue("motortrain.front.knet_dev_name", motors.front.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.front.sample_time            = Configuration::instance()->getValue("motortrain.front.sample_time",           (int)0);
    motors.front.speed_multiplier       = Configuration::instance()->getValue("motortrain.front.speed_multiplier",      (int)0);
    motors.front.pid_kk                 = Configuration::instance()->getValue("motortrain.front.pid_kk",                (int)0);
    motors.front.pid_kff                = Configuration::instance()->getValue("motortrain.front.pid_kff",               (int)0);
    motors.front.pid_kp                 = Configuration::instance()->getValue("motortrain.front.pid_kp",                (int)0);
    motors.front.pid_kd                 = Configuration::instance()->getValue("motortrain.front.pid_kd",                (int)0);
    motors.front.pid_ka                 = Configuration::instance()->getValue("motortrain.front.pid_ka",                (int)0);
    motors.front.pid_ki                 = Configuration::instance()->getValue("motortrain.front.pid_ki",                (int)0);
    motors.front.pid_kih                = Configuration::instance()->getValue("motortrain.front.pid_kih",               (int)0);
    motors.front.pid_kil                = Configuration::instance()->getValue("motortrain.front.pid_kil",               (int)0);
    motors.front.pid_kf0                = Configuration::instance()->getValue("motortrain.front.pid_kf0",               (int)0);
    motors.front.pid_kf1                = Configuration::instance()->getValue("motortrain.front.pid_kf1",               (int)0);
    motors.front.pid_kf2                = Configuration::instance()->getValue("motortrain.front.pid_kf2",               (int)0);
    motors.front.pid_kf3                = Configuration::instance()->getValue("motortrain.front.pid_kf3",               (int)0);
    motors.front.pid_kdfil              = Configuration::instance()->getValue("motortrain.front.pid_kdfil",             (int)0);
    motors.front.pid_driveplus          = Configuration::instance()->getValue("motortrain.front.pid_driveplus",         (int)0);
    motors.front.pid_driveminus         = Configuration::instance()->getValue("motortrain.front.pid_minus",             (int)0);
    motors.front.margin                 = Configuration::instance()->getValue("motortrain.front.margin",                (int)0);
    //motors.front.current_limit          = Configuration::instance()->getValue("motortrain.front.current_limit",         (int)0);
    motors.front.pid_accel                = Configuration::instance()->getValue("motortrain.front.pid_accel",             (int)0);
    motors.front.pid_accelf               = Configuration::instance()->getValue("motortrain.front.pid_accelf",            (int)0);
    motors.front.pid_slewlimit          = Configuration::instance()->getValue("motortrain.front.pid_slewlimit",         (int)0);
    motors.front.pid_maxpwm               = Configuration::instance()->getValue("motortrain.front.pid_maxpwm",            (int)0);
    //motors.front.encoder_resolution     = Configuration::instance()->getValue("motortrain.front.encoder_resolution",    (int)0);
    motors.front.max_motor_speed        = Configuration::instance()->getValue("motortrain.front.max_motor_speed",       (int)0);

    // Rear Motor
    Configuration::instance()->getValue("motortrain.rear.knet_dev_name", motors.rear.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.rear.sample_time             = Configuration::instance()->getValue("motortrain.rear.sample_time",            (int)0);
    motors.rear.speed_multiplier        = Configuration::instance()->getValue("motortrain.rear.speed_multiplier",       (int)0);
    motors.rear.pid_kk                  = Configuration::instance()->getValue("motortrain.rear.pid_kk",                 (int)0);
    motors.rear.pid_kff                 = Configuration::instance()->getValue("motortrain.rear.pid_kff",                (int)0);
    motors.rear.pid_kp                  = Configuration::instance()->getValue("motortrain.rear.pid_kp",                 (int)0);
    motors.rear.pid_kd                  = Configuration::instance()->getValue("motortrain.rear.pid_kd",                 (int)0);
    motors.rear.pid_ka                  = Configuration::instance()->getValue("motortrain.rear.pid_ka",                 (int)0);
    motors.rear.pid_ki                  = Configuration::instance()->getValue("motortrain.rear.pid_ki",                 (int)0);
    motors.rear.pid_kih                 = Configuration::instance()->getValue("motortrain.rear.pid_kih",                (int)0);
    motors.rear.pid_kil                 = Configuration::instance()->getValue("motortrain.rear.pid_kil",                (int)0);
    motors.rear.pid_kf0                 = Configuration::instance()->getValue("motortrain.rear.pid_kf0",                (int)0);
    motors.rear.pid_kf1                 = Configuration::instance()->getValue("motortrain.rear.pid_kf1",                (int)0);
    motors.rear.pid_kf2                 = Configuration::instance()->getValue("motortrain.rear.pid_kf2",                (int)0);
    motors.rear.pid_kf3                 = Configuration::instance()->getValue("motortrain.rear.pid_kf3",                (int)0);
    motors.rear.pid_kdfil               = Configuration::instance()->getValue("motortrain.rear.pid_kdfil",              (int)0);
    motors.rear.pid_driveplus           = Configuration::instance()->getValue("motortrain.rear.pid_driveplus",          (int)0);
    motors.rear.pid_driveminus          = Configuration::instance()->getValue("motortrain.rear.pid_minus",              (int)0);
    motors.rear.margin                  = Configuration::instance()->getValue("motortrain.rear.margin",                 (int)0);
    //motors.rear.current_limit           = Configuration::instance()->getValue("motortrain.rear.current_limit",          (int)0);
    motors.rear.pid_accel                 = Configuration::instance()->getValue("motortrain.rear.pid_accel",              (int)0);
    motors.rear.pid_accelf                = Configuration::instance()->getValue("motortrain.rear.pid_accelf",             (int)0);
    motors.rear.pid_slewlimit           = Configuration::instance()->getValue("motortrain.rear.pid_slewlimit",          (int)0);
    motors.rear.pid_maxpwm                = Configuration::instance()->getValue("motortrain.rear.pid_maxpwm",             (int)0);
    //motors.rear.encoder_resolution      = Configuration::instance()->getValue("motortrain.rear.encoder_resolution",     (int)0);
    motors.rear.max_motor_speed         = Configuration::instance()->getValue("motortrain.rear.max_motor_speed",        (int)0);


    // Limiting factors of the localization system (Bounds checking)
    min_x = Configuration::instance()->getValue("localization.min_x", (int)0);
    min_y = Configuration::instance()->getValue("localization.min_y", (int)0);
    max_x = Configuration::instance()->getValue("localization.max_x", (int)0);
    max_y = Configuration::instance()->getValue("localization.max_y", (int)0);

    setMotors();

    return 1;
}

// ****************************************************************************

// TODO: Dylan added this temporarily
void MotorController::setQuad(MotorController::QUAD* q)
{
    quad = q;
}
MotorController::QUAD* MotorController::getQuad()
{
    return quad;
}
// TODO: Dylan added this temporarily

void MotorController::initQuadMotor(struct motor_t &motor)
{
    if (strcmp(motor.name, "Erismotor:PriMotor0") == 0)
    {
        motor.dev_desc = 0;
    }
    else if (strcmp(motor.name, "Erismotor:PriMotor1") == 0)
    {
        motor.dev_desc = 1;
    }
    else if (strcmp(motor.name, "Erismotor:PriMotor2") == 0)
    {
        motor.dev_desc = 2;
    }
    else if (strcmp(motor.name, "Erismotor:PriMotor3") == 0)
    {
        motor.dev_desc = 3;
    }
    else
    {
        motor.dev_desc = -1;
    }
    // If the device exists.
    if (motor.dev_desc != -1)
    {
        write_word(motor.dev_desc, openloop, 0xFFFF);   // in open loop mode
        write_word(motor.dev_desc, profile, 1);         // Set to Trapezoidal profile
        write_word(motor.dev_desc, prescale, 100);      // PWM Rate
        write_word(motor.dev_desc, postscale, 200);     // Sample Rate
    }
    else
    {
        fprintf(stderr, "Error: Could not initialize motor.\n");
    }

}

void MotorController::setDutyCycle(int pwmgen_value, int motor_num)
{
    if (motor_num == 1)
    {
        write_word(motors.left.dev_desc, pwmgenc, pwmgen_value); // Duty cycle
    }
    if (motor_num == 2)
    {
        write_word(motors.right.dev_desc, pwmgenc, pwmgen_value); // Duty cycle
    }
    if (motor_num == 3)
    {
        write_word(motors.front.dev_desc, pwmgenc, pwmgen_value); // Duty cycle
    }
    if (motor_num == 4)
    {
        write_word(motors.rear.dev_desc, pwmgenc, pwmgen_value); // Duty cycle
    }
}

void MotorController::lqrBad(badestLQR *badest)
{
 badest->x[0]=badest->omega_slope*badest->u[0];
 badest->x[1]=badest->omega_slope*badest->u[1];
 badest->x[2]=badest->omega_slope*badest->u[2];
 badest->x[3]=badest->omega_slope*badest->u[3];
 badest->x[6]=(badest->x[4]-badest->roll_pre)/badest->timeStep;
 badest->x[7]=(badest->x[5]-badest->pitch_pre)/badest->timeStep;
 
 badest->u[0]=-badest->K[0][0]*badest->x[0]-badest->K[0][1]*badest->x[1]-badest->K[0][2]*badest->x[2]-badest->K[0][3]*badest->x[3]-badest->K[0][4]*(badest->x[4]-badest->desired_roll)-badest->K[0][5]*(badest->x[5]-badest->desired_pitch)-badest->K[0][6]*badest->x[6]-badest->K[0][7]*badest->x[7];
 badest->u[1]=-badest->K[1][0]*badest->x[0]-badest->K[1][1]*badest->x[1]-badest->K[1][2]*badest->x[2]-badest->K[1][3]*badest->x[3]-badest->K[1][4]*(badest->x[4]-badest->desired_roll)-badest->K[1][5]*(badest->x[5]-badest->desired_pitch)-badest->K[1][6]*badest->x[6]-badest->K[1][7]*badest->x[7];
 badest->u[2]=-badest->K[2][0]*badest->x[0]-badest->K[2][1]*badest->x[1]-badest->K[2][2]*badest->x[2]-badest->K[2][3]*badest->x[3]-badest->K[2][4]*(badest->x[4]-badest->desired_roll)-badest->K[2][5]*(badest->x[5]-badest->desired_pitch)-badest->K[2][6]*badest->x[6]-badest->K[2][7]*badest->x[7];
 badest->u[3]=-badest->K[3][0]*badest->x[0]-badest->K[3][1]*badest->x[1]-badest->K[3][2]*badest->x[2]-badest->K[3][3]*badest->x[3]-badest->K[3][4]*(badest->x[4]-badest->desired_roll)-badest->K[3][5]*(badest->x[5]-badest->desired_pitch)-badest->K[3][6]*badest->x[6]-badest->K[3][7]*badest->x[7];

 badest->roll_pre=badest->x[4];
 badest->pitch_pre=badest->x[5];
}
void MotorController::KControl(eliaK *k){
    for(int i=0; i<11; i++){
        k->x[i]=k->xnext[i];
    }
    for(int i=0; i<11; i++){
        k->xnext[i]=k->A[i][0]*k->x[0]+k->A[i][1]*k->x[1]+k->A[i][2]*k->x[2]+k->A[i][3]*k->x[3]+k->A[i][4]*k->x[4]+k->A[i][5]*k->x[5]+k->A[i][6]*k->x[6]+k->A[i][7]*k->x[7]+k->A[i][8]*k->x[8]+k->A[i][9]*k->x[9]+k->A[i][10]*k->x[10]+k->B[i][0]*k->ref+k->B[i][1]*k->pos;
    }
    k->y=k->C[0]*k->x[0]+k->C[1]*k->x[1]+k->C[2]*k->x[2]+k->C[3]*k->x[3]+k->C[4]*k->x[4]+k->C[5]*k->x[5]+k->C[6]*k->x[6]+k->C[7]*k->x[7]+k->C[8]*k->x[8]+k->C[9]*k->x[9]+k->C[10]*k->x[10]+k->D[0]*k->ref+k->D[1]*k->pos;

}
void MotorController::lqrControl(motorLQR *lqr)
{	
	//lqr->x_next[0]= lqr->Aestd[0][0]*lqr->x[0]+lqr->Aestd[0][1]*lqr->x[1]+lqr->Aestd[0][2]*lqr->x[2]+lqr->Aestd[0][3]*lqr->x[3]+lqr->L[0][0]*lqr->y[0]+lqr->L[0][1]*lqr->y[1]+lqr->BN[0][0]*lqr->desired_roll+lqr->BN[0][1]*lqr->desired_pitch;
	//lqr->x_next[1]= lqr->Aestd[1][0]*lqr->x[0]+lqr->Aestd[1][1]*lqr->x[1]+lqr->Aestd[1][2]*lqr->x[2]+lqr->Aestd[1][3]*lqr->x[3]+lqr->L[1][0]*lqr->y[0]+lqr->L[1][1]*lqr->y[1]+lqr->BN[1][0]*lqr->desired_roll+lqr->BN[1][1]*lqr->desired_pitch;
	//lqr->x_next[2]= lqr->Aestd[2][0]*lqr->x[0]+lqr->Aestd[2][1]*lqr->x[1]+lqr->Aestd[2][2]*lqr->x[2]+lqr->Aestd[2][3]*lqr->x[3]+lqr->L[2][0]*lqr->y[0]+lqr->L[2][1]*lqr->y[1]+lqr->BN[2][0]*lqr->desired_roll+lqr->BN[2][1]*lqr->desired_pitch;
	//lqr->x_next[3]= lqr->Aestd[3][0]*lqr->x[0]+lqr->Aestd[3][1]*lqr->x[1]+lqr->Aestd[3][2]*lqr->x[2]+lqr->Aestd[3][3]*lqr->x[3]+lqr->L[3][0]*lqr->y[0]+lqr->L[3][1]*lqr->y[1]+lqr->BN[3][0]*lqr->desired_roll+lqr->BN[3][1]*lqr->desired_pitch;
	
    lqr->x_next[0]= lqr->Aestd[0][0]*lqr->x[0]+lqr->Aestd[0][1]*lqr->x[1]+lqr->Aestd[0][2]*lqr->x[2]+lqr->Aestd[0][3]*lqr->x[3]+lqr->L[0][0]*lqr->y[0]+lqr->L[0][1]*lqr->y[1];
    lqr->x_next[1]= lqr->Aestd[1][0]*lqr->x[0]+lqr->Aestd[1][1]*lqr->x[1]+lqr->Aestd[1][2]*lqr->x[2]+lqr->Aestd[1][3]*lqr->x[3]+lqr->L[1][0]*lqr->y[0]+lqr->L[1][1]*lqr->y[1];
    lqr->x_next[2]= lqr->Aestd[2][0]*lqr->x[0]+lqr->Aestd[2][1]*lqr->x[1]+lqr->Aestd[2][2]*lqr->x[2]+lqr->Aestd[2][3]*lqr->x[3]+lqr->L[2][0]*lqr->y[0]+lqr->L[2][1]*lqr->y[1];
    lqr->x_next[3]= lqr->Aestd[3][0]*lqr->x[0]+lqr->Aestd[3][1]*lqr->x[1]+lqr->Aestd[3][2]*lqr->x[2]+lqr->Aestd[3][3]*lqr->x[3]+lqr->L[3][0]*lqr->y[0]+lqr->L[3][1]*lqr->y[1];
	
    //lqr->u[0]=-lqr->K[0][0]*lqr->x_next[0]-lqr->K[0][1]*lqr->x_next[1]-lqr->K[0][2]*lqr->x_next[2]-lqr->K[0][3]*lqr->x_next[3]+lqr->N[0][0]*lqr->desired_roll+lqr->N[0][1]*lqr->desired_pitch;
	//lqr->u[1]=-lqr->K[1][0]*lqr->x_next[0]-lqr->K[1][1]*lqr->x_next[1]-lqr->K[1][2]*lqr->x_next[2]-lqr->K[1][3]*lqr->x_next[3]+lqr->N[1][0]*lqr->desired_roll+lqr->N[1][1]*lqr->desired_pitch;
	//lqr->u[2]=-lqr->K[2][0]*lqr->x_next[0]-lqr->K[2][1]*lqr->x_next[1]-lqr->K[2][2]*lqr->x_next[2]-lqr->K[2][3]*lqr->x_next[3]+lqr->N[2][0]*lqr->desired_roll+lqr->N[2][1]*lqr->desired_pitch;
    //lqr->u[3]=-lqr->K[3][0]*lqr->x_next[0]-lqr->K[3][1]*lqr->x_next[1]-lqr->K[3][2]*lqr->x_next[2]-lqr->K[3][3]*lqr->x_next[3]+lqr->N[3][0]*lqr->desired_roll+lqr->N[3][1]*lqr->desired_pitch;
	
    lqr->u[0]=-lqr->K[0][0]*(lqr->x_next[0]-lqr->desired_roll)-lqr->K[0][1]*(lqr->x_next[1]-lqr->desired_pitch)-lqr->K[0][2]*lqr->x_next[2]-lqr->K[0][3]*lqr->x_next[3];
    lqr->u[1]=-lqr->K[1][0]*(lqr->x_next[0]-lqr->desired_roll)-lqr->K[1][1]*(lqr->x_next[1]-lqr->desired_pitch)-lqr->K[1][2]*lqr->x_next[2]-lqr->K[1][3]*lqr->x_next[3];
    lqr->u[2]=-lqr->K[2][0]*(lqr->x_next[0]-lqr->desired_roll)-lqr->K[2][1]*(lqr->x_next[1]-lqr->desired_pitch)-lqr->K[2][2]*lqr->x_next[2]-lqr->K[2][3]*lqr->x_next[3];
    lqr->u[3]=-lqr->K[3][0]*(lqr->x_next[0]-lqr->desired_roll)-lqr->K[3][1]*(lqr->x_next[1]-lqr->desired_pitch)-lqr->K[3][2]*lqr->x_next[2]-lqr->K[3][3]*lqr->x_next[3];
	
    lqr->x[0]=lqr->x_next[0];
	lqr->x[1]=lqr->x_next[1];
	lqr->x[2]=lqr->x_next[2];
	lqr->x[3]=lqr->x_next[3];
}

/**
 * Runs a PID control algorithm on the QUADPID struct.
 * Saturates the correction value at -500 and 500
 *
 * @param pidptr A pointer to a QUADPID struct
 *
 */
double MotorController::pidControl(QUADPID *pidptr)
{
    pidptr->err = pidptr->desired - pidptr->current;
    if (pidptr->err == pidptr->preverr)
    {
        return pidptr->correction;
    }
    pidptr->PID_pcomp = pidptr->p * pidptr->err;
    pidptr->PID_icomp += pidptr->i * pidptr->err*SAMPLE_PERIOD;
    pidptr->PID_dcomp = pidptr->d * (pidptr->err - pidptr->preverr) / SAMPLE_PERIOD;
    pidptr->preverr = pidptr->err;
    pidptr->correction = pidptr->PID_pcomp + pidptr->PID_icomp + pidptr->PID_dcomp;
    // Saturate correction
    /*
    if (pidptr->correction < PID_SATURATION_MIN)
    {
        pidptr->correction = PID_SATURATION_MIN;
    }
    else if (pidptr->correction > PID_SATURATION_MAX)
    {
        pidptr->correction = PID_SATURATION_MAX;
    }*/
    return pidptr->correction;
}

double MotorController::pidControlFilt(QUADPID *pidptr)
{
    pidptr->err = pidptr->desired - pidptr->current;
    pidptr->filtError = pidptr->filtErrorPre*pidptr->filtGain+pidptr->err*pidptr->errorGain;
    if (pidptr->err == pidptr->preverr)
    {
        return pidptr->correction;
    }
    pidptr->PID_pcomp = pidptr->p * pidptr->err;
    pidptr->PID_icomp += pidptr->i * pidptr->err*SAMPLE_PERIOD;
    pidptr->PID_dcomp = pidptr->d * (pidptr->filtError - pidptr->filtErrorPre) / SAMPLE_PERIOD;
    pidptr->preverr = pidptr->err;
    pidptr->filtErrorPre = pidptr->filtError;
    pidptr->correction = pidptr->PID_pcomp + pidptr->PID_icomp + pidptr->PID_dcomp;
    // Saturate correction
    /*
    if (pidptr->correction < PID_SATURATION_MIN)
    {
        pidptr->correction = PID_SATURATION_MIN;
    }
    else if (pidptr->correction > PID_SATURATION_MAX)
    {
        pidptr->correction = PID_SATURATION_MAX;
    }*/
    return pidptr->correction;
}
// ****************************************************************************

/*
 * Initializes one motor
 *
 * @param motor The motor structure to initialize
 */
void MotorController::initMotor(struct motor_t &motor)
{
    //printf("** initMotor **\n");
    if (motor.dev_desc == NULL)
    {

        printf("Initializing motor %s\n", motor.name);
        if (strcmp(motor.name, "Erismotor:PriMotor0") == 0)
        {
            motor.dev_desc = 0;
        }
        else if (strcmp(motor.name, "Erismotor:PriMotor1") == 0)
        {
            motor.dev_desc = 1;
        }
        else if (strcmp(motor.name, "Erismotor:PriMotor2") == 0)
        {
            motor.dev_desc = 2;
        }
        else if (strcmp(motor.name, "Erismotor:PriMotor3") == 0)
        {
            motor.dev_desc = 3;

        }
        else
        {
            motor.dev_desc = -1;
        }
        printf("*motor.dev_desc = %d\n", motor.dev_desc);

        // If the device exists.
        if (motor.dev_desc != -1)
        {

            // Write PID values
            write_word(motor.dev_desc, kk, (uword)motor.pid_kk);
            write_word(motor.dev_desc, kff, (uword)motor.pid_kff);
            write_word(motor.dev_desc, kp, (uword)motor.pid_kp);
            write_word(motor.dev_desc, kd, (uword)motor.pid_kd);
            write_word(motor.dev_desc, ka, (uword)motor.pid_ka);
            write_long(motor.dev_desc, ki, (udword)motor.pid_ki);
            write_word(motor.dev_desc, kih, (uword)motor.pid_kih);
            write_word(motor.dev_desc, kil, (uword)motor.pid_kil);
            write_word(motor.dev_desc, kf0, (uword)motor.pid_kf0);
            write_word(motor.dev_desc, kf0, (uword)motor.pid_kf0);
            write_word(motor.dev_desc, kf1, (uword)motor.pid_kf1);
            write_word(motor.dev_desc, kf2, (uword)motor.pid_kf2);
            write_word(motor.dev_desc, kf3, (uword)motor.pid_kf3);
            write_word(motor.dev_desc, kdfil, (uword)motor.pid_kdfil);
            write_word(motor.dev_desc, driveplus, (uword)motor.pid_driveplus);
            write_word(motor.dev_desc, driveminus, (uword)motor.pid_driveminus);
            write_long(motor.dev_desc, accel, (udword)motor.pid_accel);
            write_long(motor.dev_desc, slewlimit, (udword)motor.pid_slewlimit);
            write_word(motor.dev_desc, maxpwm, motor.pid_maxpwm);

            //Hardcoded values
            write_word(0, prescale, 6711);  // PWM Rate
            write_word(0, postscale, 200);  // Sample Rate
            write_long(motor.dev_desc, desvel, 100); //ensure target velocity 0
            write_word(motor.dev_desc, profile, 1);
            write_word(motor.dev_desc, pid, 0xFFFF);  // Sets PID usage to true
        }
        else
        {
            fprintf(stderr, "Error: Could not initialize motor.\n");
        }

    }

}

// ****************************************************************************
void MotorController::initAllQuadMotors()
{
    initQuadMotor(motors.front);
    initQuadMotor(motors.left);
    initQuadMotor(motors.right);
    initQuadMotor(motors.rear);
}
// ****************************************************************************

/*
 * Initializes all the motors
 */
void MotorController::initAllMotors()
{
    initMotor(motors.front);
    initMotor(motors.left);
    initMotor(motors.right);
    initMotor(motors.rear);
}

/*
 * Releases control of the given motor
 *
 * @param motor The motor to release control of
 */
void MotorController::releaseMotor(struct motor_t &motor)
{
    if (motor.dev_desc != NULL)
    {
        printf("Releasing motor %s\n", motor.name);
        //ERIS CODE ADDED HERE
        printf("ERIS - MotorController::releaseMotor function is depricated \n");
    }
}

/*
 * Shuts the motor controller down, destroying any resources & releasing
 * any devices the controller is holding.
 */
void MotorController::shutdown()
{
    if (s_instance != NULL)
    {
        delete s_instance;
        s_instance = NULL;
    }
}

/*
 * This will stop all motors.  This is different from setMotors() because
 * the controller will still maintain the connection to the motor
 * device descriptors, but the motors will be stopped.
 */
void MotorController::stop()
{
    // Set the speeds to 0
    memset(&motor_speeds, 0, sizeof(motor_control_manual_t));

    // This stops the motors immedietly, as opposed to other methods
    // which wait for run() to be called to set the speeds
    setMotorSpeeds();

    current_mode = MOTOR_CONTROL_MODE_FREE;
}

/*
 * This will set the motors to free mode, releasing control of the
 * motor device descriptors so a third-party program can use them.
 * This is different from free control, as free control will cause
 * the wheels to stop moving, but the controller will still hold
 * a reference to the motor device descriptors; With setMotors,
 * the controller will no longer hold this reference, and the controller
 * will have to acquire the descriptor again if the user wants to
 * move the korebot.
 */
void MotorController::releaseMotors()
{
    if (current_mode != MOTOR_CONTROL_MODE_RELEASE)
    {
        stop();
        releaseMotor(motors.front);
        releaseMotor(motors.left);
        releaseMotor(motors.right);
        releaseMotor(motors.rear);
    }
    current_mode = MOTOR_CONTROL_MODE_RELEASE;
}

/*
 * This will set the motors to free mode.  In free mode, the
 * association with the motor device descriptors is still maintained,
 * but the speeds of the motors are never set, allowing other programs
 * to set the motor speeds, and this program acts as a sensor for the motor
 * encoders
 */
void MotorController::setMotors()
{
    if (current_mode == MOTOR_CONTROL_MODE_RELEASE)
    {
        //initAllMotors();
        initAllQuadMotors();
    }

    stop();
    current_mode = MOTOR_CONTROL_MODE_FREE;
}

/*
 * This will run the motors in speed-profile mode, moving with the
 * rotational speed & in the direction given.
 *
 * @param command The parameters to control the motors
 */
void MotorController::setMotors(const struct motor_control_speed_t *command)
{
    if (current_mode == MOTOR_CONTROL_MODE_RELEASE)
    {
        //initAllMotors();
        initAllQuadMotors();
    }

    memcpy(&current_speed_control_settings, command, sizeof(motor_control_speed_t));
    current_mode = MOTOR_CONTROL_MODE_SPEED;
}

/*
 * This will control the motors in a manual way, such that
 * each motor can be controlled individually.
 *
 * @param command The parameters for the movement command.
 */
void MotorController::setMotors(const struct motor_control_manual_t *command)
{
    if (current_mode == MOTOR_CONTROL_MODE_RELEASE)
    {
        //initAllMotors();
        initAllQuadMotors();
    }
    current_mode = MOTOR_CONTROL_MODE_MANUAL;
    memcpy(&current_manual_control_settings, command, sizeof(motor_control_manual_t));
    motor_speeds.motor_speed_front = motors.front.speed_multiplier * command->motor_speed_front;
    motor_speeds.motor_speed_rear  = motors.rear.speed_multiplier  * command->motor_speed_rear;
    motor_speeds.motor_speed_left  = motors.left.speed_multiplier  * command->motor_speed_left;
    motor_speeds.motor_speed_right = motors.right.speed_multiplier * command->motor_speed_right;

    //printf("(f, r, l, r) = (%d, %d, %d, %d)\n", motor_speeds.motor_speed_front, motor_speeds.motor_speed_rear, motor_speeds.motor_speed_left, motor_speeds.motor_speed_right);
}


/*
 * Runs one iteration of the motor controller; Sets
 * the speeds of the motors according to how the user / AI
 * wants them to go.
 *
 * @param dt The time difference since the last call to this method
 */
void MotorController::run(float dt)
{
    PROFILE("MotorController::run");
    e_stop = 0;

    if (use_bounds_limits)
    {
        const struct localization_robot_t *location_history;
        struct localization_robot_t our_location;

        if ((location_history = LocalizationSystem::instance()->getRobotData(Korebot::instance()->getRobotID())) == NULL)
        {
            // Unrecognized KorebotID.  Stop
            e_stop = 1;
        }
        else
        {
            double dx = location_history[SENSOR_HISTORY - 2].x - location_history[SENSOR_HISTORY - 1].x;
            double dy = location_history[SENSOR_HISTORY - 2].y - location_history[SENSOR_HISTORY - 1].y;
            double dt = location_history[SENSOR_HISTORY - 2].timestamp - location_history[SENSOR_HISTORY - 1].timestamp;

            double vx = dx / dt;
            double vy = dy / dt;

            // Attempt to predict where the ball is with the 200ms localization system delay
            our_location.x = location_history[SENSOR_HISTORY - 1].x + vx * 200;
            our_location.y = location_history[SENSOR_HISTORY - 1].y + vy * 200;
            our_location.orientation = location_history[SENSOR_HISTORY - 1].orientation;

            short localization_heading = (current_speed_control_settings.heading + our_location.orientation) % 360;
            if (current_speed_control_settings.linear_speed < 0)
            {
                localization_heading = (localization_heading + 180) % 360;
            }

            // Position is unknown, stop
            if (our_location.x < 0 || our_location.y < 0)
            {
                e_stop = 1;
            }

            if (our_location.x < min_x)
            {
                if (current_mode == MOTOR_CONTROL_MODE_SPEED)
                {
                    if (localization_heading > 90 && localization_heading < 270)
                    {
                        e_stop = 1;
                    }
                }
                else
                {
                    e_stop = 1;
                }
            }
            else if (our_location.x > max_x)
            {
                if (current_mode == MOTOR_CONTROL_MODE_SPEED)
                {
                    if (localization_heading < 90 || localization_heading > 270)
                    {
                        e_stop = 1;
                    }
                }
                else
                {
                    e_stop = 1;
                }
            }

            if (our_location.y < min_y)
            {
                if (current_mode == MOTOR_CONTROL_MODE_SPEED)
                {
                    if (localization_heading > 180)
                    {
                        e_stop = 1;
                    }
                }
                else
                {
                    e_stop = 1;
                }
            }
            else if (our_location.y > max_y)
            {
                if (current_mode == MOTOR_CONTROL_MODE_SPEED)
                {
                    if (localization_heading < 180)
                    {
                        e_stop = 1;
                    }
                }
                else
                {
                    e_stop = 1;
                }
            }

            /*printf("%d::%d (%d,%d)::(%d,%d)-(%d,%d)\n",
                    e_stop,
                    localization_heading,
                    our_location.x, our_location.y,
                    min_x,min_y,max_x,max_y);*/
        }
    }

    switch (current_mode)
    {
        case MOTOR_CONTROL_MODE_SPEED:
            motor_speeds.motor_speed_front = motors.front.speed_multiplier * ( (sin_256(current_speed_control_settings.heading) * current_speed_control_settings.linear_speed) / 256 - current_speed_control_settings.rotational_velocity);
            motor_speeds.motor_speed_rear  = motors.rear.speed_multiplier  * (-(sin_256(current_speed_control_settings.heading) * current_speed_control_settings.linear_speed) / 256 - current_speed_control_settings.rotational_velocity);
            motor_speeds.motor_speed_right = motors.right.speed_multiplier * ( (cos_256(current_speed_control_settings.heading) * current_speed_control_settings.linear_speed) / 256 - current_speed_control_settings.rotational_velocity);
            motor_speeds.motor_speed_left  = motors.left.speed_multiplier  * (-(cos_256(current_speed_control_settings.heading) * current_speed_control_settings.linear_speed) / 256 - current_speed_control_settings.rotational_velocity);
        case MOTOR_CONTROL_MODE_MANUAL:
            setMotorSpeeds();
            break;
    };
}

/*
 * Passes the motor speeds onto the motor board.  After
 * this command has been executed, the robot will begin to
 * move.
 */
void MotorController::setMotorSpeeds()
{
    // set the motor specified by its descriptor (dev_desc), to the speed we desire
    PROFILE("MotorController::setMotorSpeeds");

    //ERIS CODE ADDED HERE
    //printf("**setMotorSpeeds() **\n");
    if ((motors.front.dev_desc != -1) && (motors.rear.dev_desc != -1) && (motors.right.dev_desc != -1) && (motors.left.dev_desc != -1))
    {
        if (e_stop)
        {
            printf("e_stop?!?!\n");
            if (current_motor_speeds.motor_speed_front != 0)
            {
                write_long(motors.front.dev_desc, desvel, 0);
            }
            if (current_motor_speeds.motor_speed_rear != 0)
            {
                write_long(motors.rear.dev_desc, desvel, 0);
            }
            if (current_motor_speeds.motor_speed_right != 0)
            {
                write_long(motors.right.dev_desc, desvel, 0);
            }
            if (current_motor_speeds.motor_speed_left != 0)
            {
                write_long(motors.left.dev_desc, desvel, 0);
            }

            memset(&current_motor_speeds, 0, sizeof(motor_control_manual_t));
        }
        else
        {
            if (use_status_logging)
            {
                printStatus();
            }
            //printf("(current_motor_speeds.motor_speed_front) %d != %d (motor_speeds.motor_speed_front)\n", current_motor_speeds.motor_speed_front, motor_speeds.motor_speed_front);
            if (current_motor_speeds.motor_speed_front != motor_speeds.motor_speed_front)
            {
                if (motor_speeds.motor_speed_front > motors.front.max_motor_speed)
                {
                    motor_speeds.motor_speed_front = motors.front.max_motor_speed;
                }
                else if (motor_speeds.motor_speed_front < -motors.front.max_motor_speed)
                {
                    motor_speeds.motor_speed_front = -motors.front.max_motor_speed;
                }

                write_long(motors.front.dev_desc, desvel, motor_speeds.motor_speed_front);
                //printf("front speed = %d\n", motor_speeds.motor_speed_front);

            }
            //printf("motor speed rear: %d = %d\n", current_motor_speeds.motor_speed_rear, motor_speeds.motor_speed_rear);
            if (current_motor_speeds.motor_speed_rear != motor_speeds.motor_speed_rear)
            {
                if (motor_speeds.motor_speed_rear > motors.rear.max_motor_speed)
                {
                    motor_speeds.motor_speed_rear = motors.rear.max_motor_speed;
                }
                else if (motor_speeds.motor_speed_rear < -motors.rear.max_motor_speed)
                {
                    motor_speeds.motor_speed_rear = -motors.rear.max_motor_speed;
                }

                write_long(motors.rear.dev_desc, desvel, motor_speeds.motor_speed_rear);
                //printf("front speed = %d\n", motor_speeds.motor_speed_rear);


            }
            //printf("motor speed right: %d = %d\n", current_motor_speeds.motor_speed_right, motor_speeds.motor_speed_right);
            if (current_motor_speeds.motor_speed_right != motor_speeds.motor_speed_right)
            {
                if (motor_speeds.motor_speed_right > motors.right.max_motor_speed)
                {
                    motor_speeds.motor_speed_right = motors.right.max_motor_speed;
                }
                else if (motor_speeds.motor_speed_right < -motors.right.max_motor_speed)
                {
                    motor_speeds.motor_speed_right = -motors.right.max_motor_speed;
                }

                write_long(motors.right.dev_desc, desvel, motor_speeds.motor_speed_right);
                //printf("front speed = %d\n", motor_speeds.motor_speed_right);


            }
            //printf("motor speed left: %d = %d\n", current_motor_speeds.motor_speed_left, motor_speeds.motor_speed_left);
            if (current_motor_speeds.motor_speed_left != motor_speeds.motor_speed_left)
            {
                if (motor_speeds.motor_speed_left > motors.left.max_motor_speed)
                {
                    motor_speeds.motor_speed_left = motors.left.max_motor_speed;
                }
                else if (motor_speeds.motor_speed_left < -motors.left.max_motor_speed)
                {
                    motor_speeds.motor_speed_left = -motors.left.max_motor_speed;
                }

                write_long(motors.left.dev_desc, desvel, motor_speeds.motor_speed_left);
                //printf("front speed = %d\n", motor_speeds.motor_speed_left);


            }
            memcpy(&current_motor_speeds, &motor_speeds, sizeof(motor_control_manual_t));
        }
    }

}

/*
 * Processes a packet received over the network
 *
 * @param packet The packet received from the network
 */
void MotorController::processPacket(MotorControl *packet)
{
    PROFILE("MotorController::processPacket");
    switch (packet->getCommand())
    {
        case MOTOR_CONTROL_MODE_RELEASE:
            releaseMotors();
            break;
        case MOTOR_CONTROL_MODE_FREE:
            setMotors();
            break;
        case MOTOR_CONTROL_MODE_MANUAL:
            setMotors(((MotorControlManual *)packet)->getMotorSpeeds());
            break;
        case MOTOR_CONTROL_MODE_SPEED:
            setMotors(((MotorControlSpeed *)packet)->getMotorSpeeds());
            break;
        case MOTOR_CONTROL_COMMAND_STOP:
            AI::instance()->stop();
            stop();
            break;

        default:
            fprintf(stderr, "MotorController::processPacket:Error: Unknown motor command (%d)\n", packet->getCommand());
            break;
    }
}

/*
 * Sends status information about the sensor manager to the control server.
 *
 * This includes the mode & speeds of the motors.
 */
void MotorController::sendConfig()
{
    if (current_mode == MOTOR_CONTROL_MODE_RELEASE)
    {
        MotorControl control_method((char)MOTOR_CONTROL_MODE_RELEASE);
        Korebot::instance()->getControlServerConnection()->transmit(&control_method);
    }
    else if (current_mode == MOTOR_CONTROL_MODE_FREE)
    {
        MotorControl control_method((char)MOTOR_CONTROL_MODE_FREE);
        Korebot::instance()->getControlServerConnection()->transmit(&control_method);
    }
    else if (current_mode == MOTOR_CONTROL_MODE_MANUAL)
    {
        MotorControlManual control_method(current_manual_control_settings);
        Korebot::instance()->getControlServerConnection()->transmit(&control_method);
    }
    else if (current_mode == MOTOR_CONTROL_MODE_SPEED)
    {
        MotorControlSpeed control_method(current_speed_control_settings);
        Korebot::instance()->getControlServerConnection()->transmit(&control_method);
    }

    ConfigurationPacket config(CONFIGURATION_OPTION_MOTOR_BOUNDS_LIMIT, use_bounds_limits);
    Korebot::instance()->getControlServerConnection()->transmit(&config);
}

/*
 * Prints the status of various modules on the Korebot
 */
void MotorController::printStatus()
{
    FILE * PrintFile;
    PrintFile = fopen ("Motor_Status.txt", "a");

    fprintf(PrintFile, "MOTOR CONTROLLER STATUS:\n");
    fprintf(PrintFile, "Current Control Mode:\t\t\t\t");
    switch (current_mode)
    {
        case MOTOR_CONTROL_MODE_RELEASE:
            fprintf(PrintFile, "Released\n");
            break;
        case MOTOR_CONTROL_MODE_FREE:
            fprintf(PrintFile, "Free\n");
            break;
        case MOTOR_CONTROL_MODE_MANUAL:
            fprintf(PrintFile, "Manual\n");
            break;
        case MOTOR_CONTROL_MODE_SPEED:
            fprintf(PrintFile, "Speed\n");
            break;
        default:
            fprintf(PrintFile, "Unknown\n");
            break;
    }

    fprintf(PrintFile, "Bounds Limitation:\t\t\t\t%s\n", e_stop ? "Robot is out of bounds, motors are stopped." : "Motors are Running");
    printMotorStatus("Left", motors.left);
    printMotorStatus("Right", motors.right);
    printMotorStatus("Front", motors.front);
    printMotorStatus("Rear", motors.rear);

    fprintf(PrintFile, "\n");
    fclose(PrintFile);
}

/*
 * Prints the status of the given motor to console
 *
 * @param motor A human-readable description of the motor
 * @param motor The motor details
 */
void MotorController::printMotorStatus(const char *name, const motor_t &motor)
{
    // Software & Hardware Options
    unsigned char software , hardware;

    // Error & Status Regs
    unsigned char error , status;

    // Measured value
    long value;
    FILE * PrintFile;
    PrintFile = fopen ("Motor_Status.txt", "a");

    if (motor.dev_desc != NULL)
    {
        fprintf(PrintFile, "%s Motor Status:\n", name);

        software = 0;
        hardware = 0;
        error    = 0;
        status   = 0;
        fprintf(PrintFile, "\tNot meaningful when compiled for x86 architectures\n");
        fclose(PrintFile);
    }
}

/*
 * Gets if the motor controller is using bounds limiting
 *
 * @returns 1, if the korebot stops the motors when it leaves the bounds of the playing field, 0 otherwise
 */
int MotorController::getUseBoundsLimit()
{
    return use_bounds_limits;
}
/*
 * Gets if the motor controller is logging status
 *
 * @returns 1, if the motor controller is logging, 0 otherwise
 */
int MotorController::getUseStatusLogging()
{
    return use_status_logging;
}
/*
 * Sets if the motor controller should stop the motors if the korebot leaves the bounds of the playing field
 *
 * @param useBoundsLimit 1, if the korebot should stay in-bounds, 0 otherwise
 */
void MotorController::setUseBoundsLimit(int useBoundsLimit)
{
    use_bounds_limits = useBoundsLimit;
}
/*
 * Sets if the motor controller should log the status information to a file
 *
 * @param useStatusLogging 1, if the motor controller should log, 0 otherwise
 */
void MotorController::setUseStatusLogging(int useStatusLogging)
{
    use_status_logging = useStatusLogging;
}
