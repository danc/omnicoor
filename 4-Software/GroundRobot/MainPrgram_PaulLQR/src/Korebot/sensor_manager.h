#ifndef _KOREBOT_SENSOR_MANAGER_H
#define _KOREBOT_SENSOR_MANAGER_H

#include <stdlib.h>
#include "sensor_manager_common.h"
#include "Network/packets/sensor_control.h"
#include "motor_controller.h"

// header file for the PCM-MIO-G

// #include "imu.h"		// Our Pololu CHR-6d is dead and the part has been discontinued


#ifdef KOREBOT
extern "C" {

#include <korebot/korebot.h>
#include <korebot/i2ccom.h>

}

#else
	// Eris Code Here
	// Define some i2c types for when we are compiling for x86 architecture
	// These keep the compiler happy, but they'll never be used by an x86 robot
	#define i2c_dev_t int
	#define i2c_t int

#endif

#define KOREIO 0x40
#define MAX_LOG_PATH_LEN 256

class SensorManager {
	private:
		// This is a singleton object, so here's the instance to the object
		static SensorManager *s_instance;

		// The list of sensors
		struct sensor_info_t *sensors;

		struct motor_encoder_info_t encoder_values[4];

		short pwm_values[4];

		// The size of the sensors array
		unsigned char num_sensors;

		i2c_t *i2c_bus;

		int logging_enabled;

		FILE *log_file;

		char logging_folder[MAX_LOG_PATH_LEN];

		// Eris doesn't use the Pololu CHR-6d anymore...
		//IMU_SENSOR_DATA *IMU_sensors;

	public:
		/*
		 * Returns a single instance of the SensorManager.
		 *
		 * @returns A single instance of the SensorManager
		 */
		static SensorManager *instance() {
			if (s_instance == NULL) {
				s_instance = new SensorManager();
			}
			return s_instance;
		}


		/*
		 * Frees any resources used by the sensor manager.
		 */
		~SensorManager();

		/*
		 * Initializes the SensorManager.  Each available sensor defined
		 * in the config file will be acquired, so that the data can be
		 * read later on.
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shuts the sensor manager down, destroying any resources & releasing
		 * any devices the manager is holding.
		 */
		void shutdown();

		/*
		 * Runs one iteration of the sensor manager; Reads
		 * the sensor manager data from the sensors.
		 * 
		 * @param dt The time difference since the last call to this method
		 */
		void run(float dt);

		/*
		 * Processes a packet received over the network
		 *
		 * @param packet The packet received from the network
		 */
		void processPacket(SensorControl *packet);

		/*
		 * Sends status information about the sensor manager to the control server.
		 *
		 * This includes sensors available on this korebot.
		 */
		void sendConfig();

		/*
		 * Prints the status of various modules on the Korebot
		 */
		void printStatus();

		/*
		 * Gets if the sensors are logging data to file
		 *
		 * @returns 1, if the sensors are logging to file, 0 otherwise
		 */
		int getLoggingEnabled();

		/*
		 * Sets if the sensors are logging data to file
		 *
		 * @param enableLogging 1, if logging should be enabled, 0 otherwise
		 */
		void setLoggingEnabled(int enableLogging);

		/*
		 * Gets a history of sensor values for the sensor with the given ID
		 *
		 * @param sensor_id The ID of the sensor to get
		 *
		 * @returns An array of sensor values, with index 0 the farthest in the past and index
		 * 			SENSOR_HISTORY - 1 the latest value
		 */
		long *getSensorValues(int sensor_id);

	private:
		/*
		 * Creates the SensorManager object.
		 * Nothing is requested of the sensor board at this ponint.
		 */
		SensorManager();


		/*
		 * Reads the PWM value data for the motors
		 */
		void readPWM();



		/*
		 * Reads the motor encoder data for the motors
		 */
		void readMotorEncoders();

		/*
		 * Reads the motor encoder data for one particular motor
		 *
		 * @param device A pointer to the device of the motor
		 * @param out Where to store the data read in
		 */
		void readMotorEncoders(knet_dev_t *device, struct motor_encoder_info_t &out);

		/*
		 * Writes an 8-bit value to the I2C device dev, register reg
		 *
		 * @param dev The device on the I2C bus to write to
		 * @param reg The register on the device to write to
		 * @param val The value to write to the register
		 *
		 * @returns The number of bytes written, or < 0 if an error ocurred
		 */
		int i2c_write8(i2c_dev_t dev, unsigned char reg, unsigned char val);

		/*
		 * Reads a 32-bit value from the I2C device dev, register reg, into val
		 *
		 * @param dev The device on the I2C bus to write to
		 * @param reg The register on the device to read from
		 * @param val A pointer to a long variable where the value will be read into
		 *
		 * @returns 0 if successful, -1 otherwise
		 */
		int i2c_read32(i2c_dev_t dev, char reg, long &time, short &value);

	friend class KorebotAI;
};

#endif
