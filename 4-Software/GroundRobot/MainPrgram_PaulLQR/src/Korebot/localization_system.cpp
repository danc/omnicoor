#include <string.h>
#include "configuration.h"
#include "localization_system.h"

LocalizationSystem *LocalizationSystem::s_instance = NULL;

/*
 * Creates the LocalizationSystem object.
 * No parsing of network packets happens yet, and the network
 * will not be configured yet either.
 */
LocalizationSystem::LocalizationSystem() {
	num_robots = 0;
	num_objects = 0;

	last_timestamp = 0;
	total_time = 0;

	memset(robots, 0, sizeof(struct localization_robot_history_t) * MAX_ROBOTS);
	memset(objects, 0, sizeof(struct localization_object_history_t) * MAX_OBJECTS);
	memset(&net_config, 0, sizeof(network_config_t));

	localization_connection = new UDP_Connection(0, this);

	num_packets_received = 0;
}

/*
 * Frees any resources used by the localization system.
 */
LocalizationSystem::~LocalizationSystem() {
	if (localization_connection != NULL) {
		localization_connection->stop();
		delete localization_connection;
		localization_connection = NULL;
	}
}

/*
 * Initializes the LocalizationSystem. 
 *
 * @returns 1 on success, 0 otherwise
 */
int LocalizationSystem::init() {
	net_config.port = Configuration::instance()->getValue("network.localization.port", (int)12345);
	Configuration::instance()->getValue("network.localization.ip_address", net_config.ip_addr, IP_ADDR_MAX_LEN, "127.0.0.1");

	if (!localization_connection->start(net_config)) {
		fprintf(stderr, "LocalizationSystem::init:Error: Could not start localization network connection.\n");
		return 0;
	}

	return 1;
}

/*
 * Shuts the localization system down, destroying any resources & closing
 * the network connection down in the process
 */
void LocalizationSystem::shutdown() {
	if (s_instance != NULL) {
		delete s_instance;
		s_instance = NULL;
	}
}

/*
 * Runs one iteration of the localization system; Checks if
 * there are any localization packets from the network.
 * 
 * @param dt The time difference since the last call to this method
 */
void LocalizationSystem::run(float dt) {
	PROFILE("LocalizationSystem::run");
	unsigned int i;
	//int dx, dy;

	num_packets_received = 0;

	for (i=0; i<num_robots; i++) {
		robots[i].found = 0;
	}
	for (i=0; i<num_objects; i++) {
		objects[i].found = 0;
	}

	if (localization_connection != NULL) {
		num_packets_received = localization_connection->run();
	}

	for (i=0; i<num_robots; i++) {
		if (robots[i].found == 0) {
			robots[i].time_since_last_found += dt;

			// Haven't received the robot position for over 1 second,
			// so we lost the robot
			if (robots[i].time_since_last_found > 1.0f) {
				robots[i].data[SENSOR_HISTORY-1].x = -1;
				robots[i].data[SENSOR_HISTORY-1].y = -1;
			}

			// Predict the movement of the robot
			// TODO: This needs to be based on time between packets somehow
			/*
			if (robots[i].data[SENSOR_HISTORY-1].x != -1 && robots[i].data[SENSOR_HISTORY-1].y != -1) {
				dx = robots[i].data[SENSOR_HISTORY-1].x - robots[i].data[SENSOR_HISTORY-2].x;
				dy = robots[i].data[SENSOR_HISTORY-1].y - robots[i].data[SENSOR_HISTORY-2].y;

				memcpy(robots[i].data, &robots[i].data[1], sizeof(struct localization_robot_t) * (SENSOR_HISTORY - 1));
				robots[i].data[SENSOR_HISTORY-1].x = robots[i].data[SENSOR_HISTORY-2].x + dx;
				robots[i].data[SENSOR_HISTORY-1].y = robots[i].data[SENSOR_HISTORY-2].y + dy;
			}
			*/
		}
	}

	for (i=0; i<num_objects; i++) {
		if (objects[i].found == 0) {
			objects[i].time_since_last_found += dt;

			// Haven't received the object position for over 1 second,
			// so we lost the object
			if (objects[i].time_since_last_found > 1.0f) {
				objects[i].data[SENSOR_HISTORY-1].x = -1;
				objects[i].data[SENSOR_HISTORY-1].y = -1;
			}

			// Predict the movement of the object
			// TODO: This needs to be based on time between packets somehow
			/*
			if (objects[i].data[SENSOR_HISTORY-1].x != -1 && objects[i].data[SENSOR_HISTORY-1].y != -1) {
				dx = objects[i].data[SENSOR_HISTORY-1].x - objects[i].data[SENSOR_HISTORY-2].x;
				dy = objects[i].data[SENSOR_HISTORY-1].y - objects[i].data[SENSOR_HISTORY-2].y;

				memcpy(objects[i].data, &objects[i].data[1], sizeof(struct localization_object_t) * (SENSOR_HISTORY - 1));
				objects[i].data[SENSOR_HISTORY-1].x = objects[i].data[SENSOR_HISTORY-2].x + dx;
				objects[i].data[SENSOR_HISTORY-1].y = objects[i].data[SENSOR_HISTORY-2].y + dy;
			}
			*/
		}
	}
}

/*
 * Processes a packet received over the network
 *
 * @param packet The packet received from the network
 */
void LocalizationSystem::processPacket(TCP_Connection *con, BasePacket *packet) {
	// Nothing to do here (We don't handle TCP packets)
}

/*
 * Processes a packet received over the network
 *
 * @param packet The packet received from the network
 */
void LocalizationSystem::processPacket(UDP_Connection *con, BasePacket *packet) {
	if (packet->getOpcode() == OPCODE_LOCALIZATION) {
		LocalizationPacket *loc = (LocalizationPacket *)packet;

		unsigned char num_robots_in_packet = loc->getNumRobots();
		unsigned char num_objects_in_packet = loc->getNumObjects();
		unsigned int i;
		unsigned int j;

		const struct localization_robot_t *robot_data = loc->getRobotData();
		const struct localization_object_t *object_data = loc->getObjectData();

		unsigned short this_timestamp = loc->getTimestamp();

		if (this_timestamp < last_timestamp) {
			total_time = total_time + (0xFFFF - last_timestamp + this_timestamp);
		} else {
			total_time = total_time + (this_timestamp - last_timestamp);
		}

		last_timestamp = this_timestamp;

		for (i=0; i<num_robots_in_packet; i++) {
			for (j=0; j<num_robots; j++) {
				if (robots[j].robot_id == robot_data[i].robot_id) {
					// Shift all the sensor data back one index
					memcpy(robots[j].data, &robots[j].data[1], sizeof(struct localization_robot_t) * (SENSOR_HISTORY-1));

					// Add the new sensor
					memcpy(&robots[j].data[SENSOR_HISTORY-1], &robot_data[i], sizeof(struct localization_robot_t));

					robots[j].data[SENSOR_HISTORY-1].timestamp = total_time;

					robots[j].found = 1;
					robots[j].time_since_last_found = 0.0f;
					break;
				}
			}
			// New robot, add a new entry
			if (j == num_robots) {
				if (num_robots >= MAX_ROBOTS) {
					fprintf(stderr, "LocalizationSystem::processPacket: Error: Received more than the maximum number of robots.\n");
					continue;
				} else {
					memcpy(&robots[num_robots].data[SENSOR_HISTORY-1], &robot_data[i], sizeof(struct localization_robot_t));
					robots[num_robots].data[SENSOR_HISTORY-1].timestamp = total_time;
					robots[num_robots].robot_id = robot_data[i].robot_id;
					robots[num_robots].found = 1;
					robots[num_robots].time_since_last_found = 0.0f;

					num_robots++;
				}
			}
		}

		for (i=0; i<num_objects_in_packet; i++) {
			for (j=0; j<num_objects; j++) {
				if (objects[j].object_id == object_data[i].object_id) {
					// Shift all the sensor data back one index
					memcpy(objects[j].data, &objects[j].data[1], sizeof(struct localization_object_t) * (SENSOR_HISTORY-1));

					// Add the new sensor
					memcpy(&objects[j].data[SENSOR_HISTORY-1], &object_data[i], sizeof(struct localization_object_t));
					objects[j].data[SENSOR_HISTORY-1].timestamp = total_time;
					objects[j].found = 1;
					objects[j].time_since_last_found = 0.0f;
					break;
				}
			}
			// New object, add a new entry
			if (j == num_objects) {
				if (num_objects >= MAX_OBJECTS) {
					fprintf(stderr, "LocalizationSystem::processPacket: Error: Received more than the maximum number of objects.\n");
					continue;
				} else {
					memcpy(&objects[num_objects].data, &object_data[i], sizeof(struct localization_object_t));
					objects[num_objects].data[SENSOR_HISTORY-1].timestamp = total_time;
					objects[num_objects].object_id = object_data[i].object_id;
					objects[num_objects].found = 1;
					objects[num_objects].time_since_last_found = 0.0f;

					num_objects++;
				}
			}
		}
	}
}

/*
 * Sends status information about the sensor manager to the control server.
 *
 * This includes the mode & speeds of the motors.
 */
void LocalizationSystem::sendConfig() {
	// Nothing to do here
}

/*
 * Prints the status of various modules on the Korebot
 */
void LocalizationSystem::printStatus() {
	int i;
	printf("LOCALIZATION SYSTEM STATUS:\n");
	printf("UDP Server:\t\t\t\t\t%s\n", localization_connection->isListening() ? "Listening" : "Not Listening");
	printf("Total Known Korebots:\t\t\t\t%d\n", num_robots);
	printf("Total Known Objects:\t\t\t\t%d\n", num_objects);
	printf("Robot Details: (ID, Time since Last Found)\n");
	for (i=0; i<num_robots; i++) {
		printf("\t%d\t%f\n", robots[i].robot_id, robots[i].time_since_last_found);
	}
	
	printf("Object Details: (ID, Time since Last Found)\n");
	for (i=0; i<num_objects; i++) {
		printf("\t%d\t%f\n", objects[i].object_id, objects[i].time_since_last_found);
	}
	printf("\n");
}

/********************************************************
 * 		GETTERS / SETTERS			*
 *******************************************************/

/*
 * Gets the number of robots the localization system received
 *
 * @returns The number of known robots
 */
unsigned char LocalizationSystem::getNumRobots() const {
	return num_robots;
}

/*
 * Gets information about the robot with the given robot ID
 *
 * @param robot_id The ID of the robot to get information about
 * @param out The structure to store the data in
 *
 * @returns 1, if the robot could be found, 0 otherwise
 */
int LocalizationSystem::getRobotData(unsigned char robot_id, struct localization_robot_t &out) const {
	unsigned char i = 0;
	for (i=0; i<num_robots; i++) {
		if (robots[i].robot_id == robot_id) {
			memcpy(&out, &robots[i].data[SENSOR_HISTORY-1], sizeof(localization_robot_t));
			return 1;
		}
	}
	return 0;
}

/*
 * Gets a history of information about the robot with the given ID
 *
 * @param robot_id The ID of the robot to get information about
 *
 * @returns A pointer to an array (Of length SENSOR_HISTORY) with robot history.  Index 0 in this array is the
 * 			furthest in the past, while index SENSOR_HISTORY-1 is the most recent item.
 * 			Returns NULL if no robot could be found with the given ID
 */
const struct localization_robot_t *LocalizationSystem::getRobotData(unsigned char robot_id) const {
	unsigned char i = 0;
	for (i=0; i<num_robots; i++) {
		if (robots[i].robot_id == robot_id) {
			return robots[i].data;
		}
	}
	return NULL;
}

/*
 * Gets the number of objects the localization system received
 *
 * @returns The number of known objects
 */
unsigned char LocalizationSystem::getNumObjects() const {
	return num_objects;
}

/*
 * Gets information about the object with the given object ID
 *
 * @param object_id The ID of the object to get information about
 * @param out The structure to store the data in
 *
 * @returns 1, if the object could be found, 0 otherwise
 */
int LocalizationSystem::getObjectData(unsigned char object_id, struct localization_object_t &out) const {
	unsigned char i = 0;
	for (i=0; i<num_objects; i++) {
		if (objects[i].object_id == object_id) {
			memcpy(&out, &objects[i].data[SENSOR_HISTORY-1], sizeof(localization_object_t));
			return 1;
		}
	}
	return 0;
}

/*
 * Gets a history of information about the object with the given ID
 *
 * @param object_id The ID of the object to get information about
 *
 * @returns A pointer to an array (Of length SENSOR_HISTORY) with object history.  Index 0 in this array is the
 * 			furthest in the past, while index SENSOR_HISTORY-1 is the most recent item.
 * 			Returns NULL if no object could be found with the given ID
 */
const struct localization_object_t *LocalizationSystem::getObjectData(unsigned char object_id) const {
	unsigned char i = 0;
	for (i=0; i<num_objects; i++) {
		if (objects[i].object_id == object_id) {
			return objects[i].data;
		}
	}
	return NULL;
}

/*
 * Gets the number of packets that were received from the vision system this run of the loop
 *
 * @returns See above
 */
int LocalizationSystem::getNumPacketsReceivedThisLoop() const {
	return num_packets_received;
}
