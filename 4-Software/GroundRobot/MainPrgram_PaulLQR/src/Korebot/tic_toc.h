#ifndef TIC_TOC
#define TIC_TOC

#include <iostream>
#include <ctime>

// Just a static variable that will be visible only within this file

 static clock_t start_time=clock();

void tic()
{
    start_time=clock();
}

double toc()
{
    clock_t end_time=clock();   
    return end_time - start_time;
}

#endif TIC_TOC