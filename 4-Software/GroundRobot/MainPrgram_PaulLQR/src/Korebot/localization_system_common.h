#ifndef _KOREBOT_LOCALIZATION_SYSTEM_COMMON_H
#define _KOREBOT_LOCALIZATION_SYSTEM_COMMON_H

#define MAX_ROBOTS		5
#define MAX_OBJECTS		8

struct localization_robot_t {
	unsigned char robot_id;
	float x;
	float y;
	short orientation;

	long timestamp;
};

struct localization_object_t {
	unsigned char object_id;
	float x;
	float y;
	short radius;

	long timestamp;
};

#endif
