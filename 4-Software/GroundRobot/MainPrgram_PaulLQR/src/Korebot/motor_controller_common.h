#ifndef _KOREBOT_MOTOR_CONTROLLER_COMMON_H
#define _KOREBOT_MOTOR_CONTROLLER_COMMON_H

struct motor_control_speed_t {
	int heading;
	int linear_speed;
	int rotational_velocity;
};

struct motor_control_manual_t {
	int motor_speed_front;
	int motor_speed_left;
	int motor_speed_right;
	int motor_speed_rear;
};

#endif
