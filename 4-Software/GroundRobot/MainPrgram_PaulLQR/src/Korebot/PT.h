/* pt.h
   A list of all the Packet types used by the imu as per the data sheet
	Author: Seth Beinhart
	beinhart@iastate.edu
*/
#ifndef _IMU_PT_H
#define _IMU_PT_H

//Sent packet PT
#define SET_FIR_CORNERS 0x80
#define SET_FIR_TAPS 0x81
#define SET_ACTIVE_CHANNELS 0x82
#define SET_SILENT_MODE 0x83
#define SET_BROADCAST_MODE 0x84
#define SET_X_GYRO_BIAS 0x85
#define SET_Y_GYRO_BIAS 0x86
#define SET_Z_GYRO_BIAS 0x87
#define SET_X_ACCEL_BIAS 0x88
#define SET_Y_ACCEL_BIAS 0x89
#define SET_Z_ACCEL_BIAS 0x8A
#define ZERO_RATE_GYROS 0x8B
#define SELF_TEST 0x8C
#define SET_PROCESS_WEIGHT 0x8D
#define WRITE_TO_FLASH 0xA0
#define GET_DATA 0x01
#define GET_GYRO_BIAS 0x02
#define GET_ACCEL_BIAS 0x03
#define GET_FIR_CONFIG 0x04
#define GET_FIR_TAP_CONFIG 0x05
#define GET_ACTIVE_CHANNELS 0x06
#define GET_BROADCAST_MODE 0x07
#define GET_PROCESS_WEIGHT 0x08

//Recieved Packets PT
#define COMMAND_COMPLETE 0xB0
#define COMMAND_FAILED 0xB1
#define BAD_CHECKSUM 0xB2
#define BAD_DATA_LENGTH 0xB3
#define UNRECOGNIZED_PACKET 0xB4
#define BUFFER_OVERFLOW 0xB5
#define STATUS_REPORT 0xB6
#define SENSOR_DATA 0xB7
#define GYRO_BIAS_REPORT 0xB8
#define ACCEL_BIAS_REPORT 0xB9
#define FIR_CONFIG_REPORT 0xBA
#define FIR_TAP_CONFIG_REPORT 0xBB
#define ACTIVE_CHANNEL_REPORT 0xBC
#define BROADCAST_MODE_REPORT 0xBD
#define PROCESS_WEIGHT_REPORT 0xBE

#endif
