#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include "profiler.h"
#include "ai.h"
#include "configuration.h"
#include "Network/packets/ai_list.h"

AI *AI::s_instance = NULL;

/*
 * Creates the AI object.
 * No parsing of network packets happens yet, and the network
 * will not be configured yet either.
 */
AI::AI() {
	active_ai = NULL;
	num_ai = 0;
	SLIST_INIT(&known_ai);
}

/*
 * Frees any resources used by the localization system.
 */
AI::~AI() {
	struct ai_entry_t *current_entry;
	struct ai_entry_t *temp;

	for (current_entry = SLIST_FIRST(&known_ai); current_entry != NULL; ) {
		temp = current_entry;
		current_entry = SLIST_NEXT(current_entry, next);

		// Remove the connection from our list
		SLIST_REMOVE(&known_ai, temp, ai_entry_t, next);

		// Destroy the Korebot AI
		if (temp->korebot_ai != NULL) {
			(*temp->destroy_ai)(temp->korebot_ai);
		}

		// Free the reference to the library
		if (temp->lib_handle != NULL) {
			dlclose(temp->lib_handle);
		}

		free(temp);
		num_ai--;
	}
}

/*
 * Initializes the AI. 
 *
 * @returns 1 on success, 0 otherwise
 */
int AI::init() {
	char directory[MAX_AI_PATH_LEN];
	char path[MAX_AI_PATH_LEN];
	char *error;
	DIR *dir;
	struct dirent *entry;
	struct ai_entry_t *ai_entry;
	struct ai_entry_t *temp;
	char duplicate_ai_found;

	int *ai_ids = NULL;

	int min_id = 0;
	int max_id = 0;

	Configuration::instance()->getValue("korebot.ai_lib_path", directory, MAX_AI_PATH_LEN, "/");

	dir = opendir(directory);

	if (dir == NULL) {
		perror("Could not load Korebot AI Plugin Directory");
		return 0;
	}

	printf("\n");
	printf("Loading AI....This could take a bit.\n");
	while ((entry = readdir (dir))) {
		if (strcasestr(entry->d_name, ".so") != NULL) {
			sprintf(path, "%s%s", directory, entry->d_name);


			ai_entry = (struct ai_entry_t *)malloc(sizeof(struct ai_entry_t));
			memset(ai_entry, 0, sizeof(struct ai_entry_t));

			ai_entry->lib_handle = dlopen(path, RTLD_LAZY);
			if (ai_entry->lib_handle == NULL) {
				fprintf(stderr, "AI::init: Error: Could not load library: %s\n", dlerror());
				free(ai_entry);
				continue;
			}

			ai_entry->create_ai = (create_ai_t *)dlsym(ai_entry->lib_handle, "createAI");
			if ((error = dlerror()) != NULL) {
				fprintf(stderr, "AI::init: Error: Could not find library initialization routine: %s\n", error);
				dlclose(ai_entry->lib_handle);
				free(ai_entry);
				continue;
			}

			ai_entry->destroy_ai = (destroy_ai_t *)dlsym(ai_entry->lib_handle, "destroyAI");
			if ((error = dlerror()) != NULL) {
				fprintf(stderr, "AI::init: Error: Could not find library destruction routine: %s\n", error);
				dlclose(ai_entry->lib_handle);
				free(ai_entry);
				continue;
			}

			// Attempt to create the AI object
			ai_entry->korebot_ai = (*ai_entry->create_ai)();

			if (ai_entry->korebot_ai != NULL) {
#				ifdef DEBUG
				printf("Successfully loaded AI %s\n", ai_entry->korebot_ai->getName());
#				endif

				// Initialize the AI
				ai_entry->korebot_ai->init(Configuration::instance(), SensorManager::instance(), LocalizationSystem::instance(), MotorController::instance(), ExternalSensors::instance(), Korebot::instance());

				if (ai_entry->korebot_ai->getUniqueID() < min_id) {
					min_id = ai_entry->korebot_ai->getUniqueID();
				}
				if (ai_entry->korebot_ai->getUniqueID() > max_id) {
					max_id = ai_entry->korebot_ai->getUniqueID();
				}
			} else {
#				ifdef DEBUG
				printf("Failed to load library %s\n", path);
#				endif
			}

			// Insert the AI into our list
			SLIST_INSERT_HEAD(&known_ai, ai_entry, next);

			num_ai++;
		}
	}
	closedir(dir);

	if (num_ai == 0) {
		fprintf(stderr, "AI::init: Error: Could not find any AI plugins to load.\n");
	} else {
		ai_ids = new int[max_id - min_id + 1];
		memset(ai_ids, 0, sizeof(int) * (max_id - min_id + 1));

		// Search for AIs with the same ID.  This causes unexpected behavior, and is almost always
		// an oversight when creating a new AI, so print a message if it happens
		duplicate_ai_found = 0;
		for (ai_entry = SLIST_FIRST(&known_ai); ai_entry != NULL; ) {

			if (ai_entry->korebot_ai != NULL) {
				if (ai_ids[ai_entry->korebot_ai->getUniqueID() - min_id] == 1) {
					if (!duplicate_ai_found) {
						printf("\n");
						printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
						printf("!                                                      \n");
						printf("! WARNING: DUPLICATE AI ID FOUND.                      \n");
						printf("!   This could cause unexpected behavior when starting \n");
						printf("!   an AI with the given ID.                           \n");
						printf("!                                                      \n");
					}

					duplicate_ai_found = 1;

					for (temp = SLIST_FIRST(&known_ai); temp != NULL; ) {
						if (temp->korebot_ai->getUniqueID() == ai_entry->korebot_ai->getUniqueID()) {
							printf("!   %d: %s\n", temp->korebot_ai->getUniqueID(), temp->korebot_ai->getName());
						}
						temp = SLIST_NEXT(temp, next);
					}
					printf("!                                                      \n");
				}

				ai_ids[ai_entry->korebot_ai->getUniqueID() - min_id]++;
			}

			ai_entry = SLIST_NEXT(ai_entry, next);
		}
		if (duplicate_ai_found) {
			printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			printf("\n");
		}

		delete[] ai_ids;
	}

	printf("Done Loading AI.\n");

	return 1;
}

/*
 * Shuts the localization system down, destroying any resources & closing
 * the network connection down in the process
 */
void AI::shutdown() {
	if (s_instance != NULL) {
		delete s_instance;
		s_instance = NULL;
	}
}

/*
 * Draws the menu to select the active AI
 */
void AI::drawMenu() {
	struct ai_entry_t *current_entry;

	printf("Select the AI to start.\n");
	printf("Q:\tCancel\n");
	for (current_entry = SLIST_FIRST(&known_ai); current_entry != NULL; ) {

		if (current_entry->korebot_ai != NULL) {
			printf("%d:\t%s\n", current_entry->korebot_ai->getUniqueID(), current_entry->korebot_ai->getName());
		}

		current_entry = SLIST_NEXT(current_entry, next);
	}
}

/*
 * Starts the AI with the given ID
 *
 * @param ID The ID of the AI to start
 *
 * @returns 1, if the AI with the given ID could be found & is started, 0 otherwise
 */
int AI::start(int ID) {
	struct ai_entry_t *current_entry;

	for (current_entry = SLIST_FIRST(&known_ai); current_entry != NULL; ) {

		if (current_entry->korebot_ai != NULL && current_entry->korebot_ai->getUniqueID() == ID) {
			if (active_ai != NULL) {
				active_ai->onStop();
			}

			active_ai = current_entry->korebot_ai;
			active_ai->onStart();

			printf("Starting AI: %s\n", active_ai->getName());
			return 0;
		}

		current_entry = SLIST_NEXT(current_entry, next);
	}
	return 0;
}

/*
 * Stops the AI
 */
void AI::stop() {
	if (active_ai != NULL) {
		MotorController::instance()->stop();
		active_ai->onStop();
		active_ai = NULL;
	}
}

/*
 * Runs one iteration of the localization system; Checks if
 * there are any localization packets from the network.
 * 
 * @param dt The time difference since the last call to this method
 */
void AI::run(float dt) {
	PROFILE("AI::run");

	if (active_ai != NULL) {
		active_ai->run(dt);
	}
}

/*
 * Processes a packet received over the network
 *
 * @param packet The packet received from the network
 */
void AI::processPacket(BasePacket *packet) {
	if (active_ai != NULL) {
		active_ai->processPacket(packet);
	}
}

/*
 * Sends status information about the sensor manager to the control server.
 *
 * This includes the currently active AI
 */
void AI::sendConfig() {
	struct ai_entry_t *current_entry;
	AIList list;

	// Build a list of all the AI we have found, and send it off to the control server
	for (current_entry = SLIST_FIRST(&known_ai); current_entry != NULL; ) {

		if (current_entry->korebot_ai != NULL) {
			list.addAI(current_entry->korebot_ai->getUniqueID(), current_entry->korebot_ai->getName());
		}

		current_entry = SLIST_NEXT(current_entry, next);
	}
	Korebot::instance()->getControlServerConnection()->transmit(&list);
}

/*
 * Prints the status of various modules on the Korebot
 */
void AI::printStatus() {
	printf("AI STATUS:\n");
	printf("Total Known AI:\t\t\t\t\t%d\n", num_ai);
	printf("Currently Executing AI:\t\t\t\t%s\n", (active_ai == NULL) ? "None" : active_ai->getName());
	printf("\n");
}

/*
 * Returns if the AI is currently running an AI
 *
 * @returns 1, if the AI is running, 0 otherwise
 */
int AI::isRunning() {
	return active_ai != NULL;
}
