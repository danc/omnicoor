#ifndef _KOREBOT_MOTOR_CONTROLLER_H
#define _KOREBOT_MOTOR_CONTROLLER_H

#include <stdlib.h>
#include <sstream>
#include <fstream>

typedef void knet_dev_t;

#include "common.h"
#include "motor_controller_common.h"
#include "Network/packets/all_packets.h"

#define	MOTOR_CONFIG_NAME_LEN 40

class MotorController
{

	//ERIS CODE ADDED HERE
private:

	//int current_control;

	struct motor_t
	{
		int dev_desc;			// KNET device descriptor that represents the motor

		// The name of the motor (Used when using KNET)
		char name[MOTOR_CONFIG_NAME_LEN];
		int pid_kk;
		int pid_kff;
		int pid_kp;
		int pid_kd;
		int pid_ka;
		int pid_ki;
		int pid_kih;
		int pid_kil;
		int pid_kf0;
		int pid_kf1;
		int pid_kf2;
		int pid_kf3;
		int pid_kdfil;
		int pid_driveplus;
		int pid_driveminus;
		int pid_accelf;
		int pid_accel;
		int pid_slewlimit;
		int pid_maxpwm;


		/*
					int Kp;
					int Ki;
					int Kd;
		*/

		//			int sample_time;			// The sample time to use with the motors; Only used in closed-loop mode
		int speed_multiplier;		// how much the desired speed must be multipled by to acheive this speed when commanding the motors
		//			int current_limit;				// differential constant used in the internal PID controller of the motors
		int margin;					// The motor control near target margin
		//int current_limit;			// maximum current that a motor can be given (0 = 0 amps, 512 = 2 amps)
		//			int encoder_resolution;		// encoder ticks per centimeter traveled.
		int max_motor_speed;		// The maximum speed (Absolute value) of the motor
	};
	
	struct motortrain_t
	{
		struct motor_t left;
		struct motor_t right;
		struct motor_t front;
		struct motor_t rear;
	};
	// This is a singleton object, so here's the instance to the object
	static MotorController *s_instance;

	// The current speeds of the motors
	struct motor_control_manual_t motor_speeds;
	struct motor_control_manual_t current_motor_speeds;

	// The current mode we are controlling the motors
	char current_mode;

	// 1, if the motors should stop spinning, 0 otherwise
	char e_stop;

	short min_x;
	short min_y;
	short max_x;
	short max_y;
	int use_bounds_limits;
	int use_status_logging;

	// Structures to hold the settings for the different
	// control methods

	struct motor_control_speed_t current_speed_control_settings;
	struct motor_control_manual_t current_manual_control_settings;

public:
	/*
	 * Returns a single instance of the MotorController.
	 *
	 * @returns A single instance of the MotorController
	 */
	static MotorController *instance()
	{
		if (s_instance == NULL)
		{
			s_instance = new MotorController();
		}
		return s_instance;
	}
	// Contains configuration information about the motors
	struct motortrain_t motors;

	//***************************************************************************************************************
#define SAMPLE_PERIOD       0.01 // seconds
#define PID_SATURATION_MIN  -500/2
#define PID_SATURATION_MAX  500
	struct eliaK{
		float A[11][11];
		float B[11][2];
		float C[11];
		float D[2];
		float x[11];
		float xnext[11];
		float y;
		float ref;
		float pos;                                                                       
	};
	struct motorLQR
	{
		float Aestd[4][4];
		float A[4][4];
		float C[2][4];
		float B[4][4];
		float L[4][2];
		float K[4][4];
		float x_next[4];
		float x[4];
		float u[4];
		float y[2];
		float desired_pitch;
		float desired_roll;
		float N[4][2];
		float BN[4][2];
	};
	
	struct badestLQR
	{
		float K[4][8];
		float x[8];
		float desired_pitch;
		float desired_roll;
		float pitch_pre;
		float roll_pre;
		float omega_slope;
		float timeStep;
		float u[4];
		float N[4][2];
	};
	
	typedef struct
	{
		double err;
		double preverr;
		double prevI;
		double prevD;
		double correction;
		double PID_pcomp;
		double PID_icomp;
		double PID_dcomp;
		double current;
		double desired;
		double desiredSet;
		double filtError;
		double filtErrorPre;
		double timeSum;
		// These are kp, ki, and kd. They are not named such because there are #defines in some of the MESA board
		// includes with those names.
		double p;
		double i;
		double d;
		double filtGain;
		double errorGain;
	} QUADPID;

	typedef struct
	{
		double            vrpnNow;                      // units: seconds
		double            vrpnPrev;
		double            vrpnTime0;
		int               motor_1_pwm;
		int               motor_2_pwm;
		int               motor_3_pwm;
		int               motor_4_pwm;
		double            desired_pitch;
		double            desired_yaw;
		double            desired_roll;
		std::stringstream line;             // line to be printed to terminal
		std::stringstream logFilename;      // name of the LogFile contained in a string
		std::stringstream directory;        // name of the logFile directory contained in a string
		std::fstream      logFile;          // log file object
		int               marker;           // to keep track of markers
		QUADPID           pitch;
		QUADPID           roll;
		QUADPID           yaw;
		motorLQR		  lqrControl;
		badestLQR 		  lqrBad;
		eliaK			  K;
		double			  controlTime;
	} QUAD;

	// TODO: Dylan added temporarily
    QUAD* quad;
    void setQuad(QUAD* q);
    QUAD* getQuad();
    // TODO: Dylan added temporarily
	void lqrControl(motorLQR *lqr);
	void lqrBad(badestLQR *badest);
	void initQuadMotor(struct motor_t &motor);
	void initAllQuadMotors();
	void setDutyCycle(int pwmgen_value, int motor_num);
	double pidControl(QUADPID *pidptr);
	double pidControlFilt(QUADPID *pidptr);
	void KControl(eliaK *k);
	//***************************************************************************************************************


	/*
	 * Frees any resources used by the motor controller.
	 */
	~MotorController();

	int quadInit();

	/*
	 * Initializes the MotorController.  If the mode is anything other
	 * than free, then the controller will attempt to acquire the motor
	 * device descriptors as defined in the Configuration object.
	 *
	 * @returns 1 on success, 0 otherwise
	 */
	int init();

	/*
	 * Shuts the motor controller down, destroying any resources & releasing
	 * any devices the controller is holding.
	 */
	void shutdown();

	/*
	 * This will stop all motors.  This is different from setMotors() because
	 * the controller will still maintain the connection to the motor
	 * device descriptors, but the motors will be stopped.
	 */
	void stop();

	/*
	 * This will set the motors to free mode, releasing control of the
	 * motor device descriptors so a third-party program can use them.
	 * This is different from stop(), as stop() will cause
	 * the wheels to stop moving, but the controller will still hold
	 * a reference to the motor device descriptors; With setMotors,
	 * the controller will no longer hold this referenc, and the controller
	 * will have to acquire the descriptor again if the user wants to
	 * move the korebot.
	 */
	void releaseMotors();

	/*
	 * This will set the motors to free mode.  In free mode, the
	 * association with the motor device descriptors is still maintained,
	 * but the speeds of the motors are never set, allowing other programs
	 * to set the motor speeds, and this program acts as a sensor for the motor
	 * encoders
	 */
	void setMotors();

	/*
	 * This will run the motors in speed-profile mode, moving with the
	 * rotational speed & in the direction given.
	 *
	 * @param command The parameters to control the motors
	 */
	void setMotors(const struct motor_control_speed_t *command);

	/*
	 * This will control the motors in a manual way, such that
	 * each motor can be controlled individually.
	 *
	 * @param command The parameters for the movement command.
	 */
	void setMotors(const struct motor_control_manual_t *command);

	/*
	 * Runs one iteration of the motor controller; Sets
	 * the speeds of the motors according to how the user / AI
	 * wants them to go.
	 *
	 * @param dt The time difference since the last call to this method
	 */
	void run(float dt);

	/*
	 * Processes a packet received over the network
	 *
	 * @param packet The packet received from the network
	 */
	void processPacket(MotorControl *packet);

	/*
	 * Sends status information about the sensor manager to the control server.
	 *
	 * This includes the mode & speeds of the motors.
	 */
	void sendConfig();

	/*
	 * Prints the status of various modules on the Korebot
	 */
	void printStatus();

	/*
	 * Gets if the motor controller is using bounds limiting
	 *
	 * @returns 1, if the korebot stops the motors when it leaves the bounds of the playing field, 0 otherwise
	 */
	int getUseBoundsLimit();

	/*
	 * Gets if the motor controller is logging status
	 *
	 * @returns 1, if the motor controller is logging, 0 otherwise
	 */
	int getUseStatusLogging();

	/*
	 * Sets if the motor controller should stop the motors if the korebot leaves the bounds of the playing field
	 *
	 * @param useBoundsLimit 1, if the korebot should stay in-bounds, 0 otherwise
	 */
	void setUseBoundsLimit(int useBoundsLimit);

	/*
	 * Sets if the motor controller should log the statuse
	 *
	 * @param useStatusLogging 1, if the motor controller should log, 0 otherwise
	 */
	void setUseStatusLogging(int useStatusLogging);

	/*
	 * Computes the output of sin(degree)*256 using a lookup table
	 *
	 * @returns (int)(sin(degree)*256
	 */
	static int sin_256(unsigned int degree)
	{
		static int sin_values[360] = {0, 4, 9, 13, 18, 22, 27, 31, 36, 40, 44, 49, 53, 58, 62, 66, 71, 75, 79, 83, 88, 92, 96, 100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 139, 143, 147, 150, 154, 158, 161, 165, 168, 171, 175, 178, 181, 184, 187, 190, 193, 196, 199, 202, 204, 207, 210, 212, 215, 217, 219, 222, 224, 226, 228, 230, 232, 234, 236, 237, 239, 241, 242, 243, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 254, 255, 255, 255, 256, 256, 256, 256, 256, 256, 256, 255, 255, 255, 254, 254, 253, 252, 251, 250, 249, 248, 247, 246, 245, 243, 242, 241, 239, 237, 236, 234, 232, 230, 228, 226, 224, 222, 219, 217, 215, 212, 210, 207, 204, 202, 199, 196, 193, 190, 187, 184, 181, 178, 175, 171, 168, 165, 161, 158, 154, 150, 147, 143, 139, 136, 132, 128, 124, 120, 116, 112, 108, 104, 100, 96, 92, 88, 83, 79, 75, 71, 66, 62, 58, 53, 49, 44, 40, 36, 31, 27, 22, 18, 13, 9, 4, 0, -4, -9, -13, -18, -22, -27, -31, -36, -40, -44, -49, -53, -58, -62, -66, -71, -75, -79, -83, -88, -92, -96, -100, -104, -108, -112, -116, -120, -124, -128, -132, -136, -139, -143, -147, -150, -154, -158, -161, -165, -168, -171, -175, -178, -181, -184, -187, -190, -193, -196, -199, -202, -204, -207, -210, -212, -215, -217, -219, -222, -224, -226, -228, -230, -232, -234, -236, -237, -239, -241, -242, -243, -245, -246, -247, -248, -249, -250, -251, -252, -253, -254, -254, -255, -255, -255, -256, -256, -256, -256, -256, -256, -256, -255, -255, -255, -254, -254, -253, -252, -251, -250, -249, -248, -247, -246, -245, -243, -242, -241, -239, -237, -236, -234, -232, -230, -228, -226, -224, -222, -219, -217, -215, -212, -210, -207, -204, -202, -199, -196, -193, -190, -187, -184, -181, -178, -175, -171, -168, -165, -161, -158, -154, -150, -147, -143, -139, -136, -132, -128, -124, -120, -116, -112, -108, -104, -100, -96, -92, -88, -83, -79, -75, -71, -66, -62, -58, -53, -49, -44, -40, -36, -31, -27, -22, -18, -13, -9, -4};
		return sin_values[degree % 360];
	};

	/*
	 * Computes the output of cos(degree)*256 using a lookup table
	 *
	 * @returns (int)(cos(degree)*256
	 */
	static int cos_256(unsigned int degree)
	{
		static int cos_values[360] = {256, 256, 256, 256, 255, 255, 255, 254, 254, 253, 252, 251, 250, 249, 248, 247, 246, 245, 243, 242, 241, 239, 237, 236, 234, 232, 230, 228, 226, 224, 222, 219, 217, 215, 212, 210, 207, 204, 202, 199, 196, 193, 190, 187, 184, 181, 178, 175, 171, 168, 165, 161, 158, 154, 150, 147, 143, 139, 136, 132, 128, 124, 120, 116, 112, 108, 104, 100, 96, 92, 88, 83, 79, 75, 71, 66, 62, 58, 53, 49, 44, 40, 36, 31, 27, 22, 18, 13, 9, 4, 0, -4, -9, -13, -18, -22, -27, -31, -36, -40, -44, -49, -53, -58, -62, -66, -71, -75, -79, -83, -88, -92, -96, -100, -104, -108, -112, -116, -120, -124, -128, -132, -136, -139, -143, -147, -150, -154, -158, -161, -165, -168, -171, -175, -178, -181, -184, -187, -190, -193, -196, -199, -202, -204, -207, -210, -212, -215, -217, -219, -222, -224, -226, -228, -230, -232, -234, -236, -237, -239, -241, -242, -243, -245, -246, -247, -248, -249, -250, -251, -252, -253, -254, -254, -255, -255, -255, -256, -256, -256, -256, -256, -256, -256, -255, -255, -255, -254, -254, -253, -252, -251, -250, -249, -248, -247, -246, -245, -243, -242, -241, -239, -237, -236, -234, -232, -230, -228, -226, -224, -222, -219, -217, -215, -212, -210, -207, -204, -202, -199, -196, -193, -190, -187, -184, -181, -178, -175, -171, -168, -165, -161, -158, -154, -150, -147, -143, -139, -136, -132, -128, -124, -120, -116, -112, -108, -104, -100, -96, -92, -88, -83, -79, -75, -71, -66, -62, -58, -53, -49, -44, -40, -36, -31, -27, -22, -18, -13, -9, -4, 0, 4, 9, 13, 18, 22, 27, 31, 36, 40, 44, 49, 53, 58, 62, 66, 71, 75, 79, 83, 88, 92, 96, 100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 139, 143, 147, 150, 154, 158, 161, 165, 168, 171, 175, 178, 181, 184, 187, 190, 193, 196, 199, 202, 204, 207, 210, 212, 215, 217, 219, 222, 224, 226, 228, 230, 232, 234, 236, 237, 239, 241, 242, 243, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 254, 255, 255, 255, 256, 256, 256};
		return cos_values[degree % 360];
	};

private:
	/*
	 * Creates the MotorController object.
	 * Nothing is requested of the motor controller board at this ponint.
	 */
	MotorController();

	/*
	 * Initializes one motor
	 *
	 * @param motor The motor structure to initialize
	 */
	void initMotor(struct motor_t &motor);

	/*
	 * Initializes all the motors
	 */
	void initAllMotors();

	/*
	 * Releases control of the given motor
	 *
	 * @param motor The motor to release control of
	 */
	void releaseMotor(struct motor_t &motor);

	/*
	 * Passes the motor speeds onto the motor board.  After
	 * this command has been executed, the robot will begin to
	 * move.
	 */
	void setMotorSpeeds();

	/*
	 * Prints the status of the given motor to console
	 *
	 * @param name  A human-readable description of the motor
	 * @param motor The motor details
	 */
	void printMotorStatus(const char *name, const motor_t &motor);

	/*
	 * Creates and opens a log file
	 */
	//		bool logStart();

	/*
	 * Closes the log file. Terminates logging
	 */
	//		bool logStop();

	/*
	 * This function prints a line to the log file. This is the
	 * one and only function that enters data into the file. Hence,
	 * change in headers should be made here.
	 */
	//		void logStandard();


	friend class KorebotAI;
	friend class SensorManager;
};

#endif
