/*** FOR USE WITH ERIS BASED SYSTEMS***/

#ifndef _DMCCOM_H
#define _DMCCOM_H

#include "IO5.H"
#include "INCLUDE5.H"  //Because of this we cannot use Axis as a input car since it is defined.. DUMB!

#include "CON5.H"

typedef unsigned char  unsigned8,  ubyte ;
typedef unsigned short unsigned16, uword ;
typedef unsigned long unsigned32, udword ;
typedef char  signed8,  byte ;
typedef short signed16, word ;
typedef long signed32,  dword ;



/*** Varibles used in the addr portion of the functions****/

static uword P_ICDFIFO;
static uword P_QCDFIFO;
static uword P_SRA;
static uword P_SRB;


/*** Functions that should be used mostly by the user***/
void init_mcont();
int read_word(char axisvar, uword parameter,uword *pret);
int read_long(char axisvar, uword parameter,udword* pret);
int write_word(char axisvar, uword parameter, uword val);
int write_long(char axisvar, uword parameter, udword val);
/** Functions that give control to user DONT USE UNLESS YOU HAVE TO. Use functions above**/
int write_long_FIFO(char axisvar, uword parameter, udword val, uword addr);
int read_word_FIFO(char axisvar, uword parameter,uword *pret, uword addr);
int read_long_FIFO(char axisvar, uword parameter,udword *pret, uword addr);
int write_word_FIFO(char axisvar, uword parameter, uword val, uword addr);

#endif
