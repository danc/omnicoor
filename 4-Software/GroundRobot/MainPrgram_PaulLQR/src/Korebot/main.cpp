#include <iomanip>
#include <iostream>
#include <string>

#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>
#include <termios.h>
#include <time.h>
#include <cmath>

#include "configuration.h"
#include "korebot.h"
#include "motor_controller.h"

//*************** VRPN stuff ***************//
#include "vrpn.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <quat.h>
#include "tic_toc.h"
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);
//*************** VRPN stuff ***************//

//*************** Logger stuff ***************//
#include "logger.h"
//*************** Logger stuff ***************//

using namespace std;

#define DUTY_CYCLE_MIN          26000
#define DUTY_CYCLE_MAX          56000
#define DUTY_CYCLE_MID          ((DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 2) + DUTY_CYCLE_MIN
#define DUTY_CYCLE_HALF_RANGE   (DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 2
#define DUTY_CYCLE_MID          ((DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 2) + DUTY_CYCLE_MIN
#define DUTY_CYCLE_QUATER_RANGE   (DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 4
#define DUTY_CYCLE_QUATER         ((DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 4) + DUTY_CYCLE_MIN
#define PITCH                   1
#define ROLL                    2

#define CLEAR                   "\033[H\033[J" // for data logging
#define PID_SATURATION_MIN  -500
#define PID_SATURATION_MAX  500


// This program can be compiled without the actual Korebot callbacks,
// allowing it to be run on a normal Linux machine for testing

extern int app_shutdown;
MotorController::QUAD quad;
char cmd;
pthread_mutex_t mutex;
int cur_axis;

/****** function prototypes ******/
void *UIThread(void *threadID);
void *mainThread(void *threadID);
void manual_control_UI();
void manual_pwm_control();
void PID_controller(vrpn_data_t *vrpnData);
void VRPN_connection();
double rampCalc(double refDes, double ref);
double normalize(double num, int A, int B, int C, int D);
char getch();
void arm_quad_motors();
void initializePID();
void control_value_adjust();
void initializeLQR();
void initializeBadLQR();
void normalizeLQR(float *u, int size);
void initK();
void startSine(double curTime);
/****** function prototypes ******/

/********** BOX_STEP mode **********************/
static double nextChange = 0;           // Time to change the value
static double changeRate = 20;          // Time between changes (seconds)
static int state = 0;                   // Starting state
static float boxSize = 0.0872664626 ;//0.1745329252(10 deg);//0.03490658504(2 deg); //0.0872664626(5 deg);   // Size in radians to move per step (5 degrees)
static double rampSlope =0.03490658504/4;
void boxStepFunction(double curTime);

/***********************************************/

static double freq=.1;




static void ctrlc_handler( int sig )
{
    app_shutdown = 1;
}

struct timeval t1;
double startTime = 0;

int main(int argc, char** argv)
{
    char config_filename[64];
    pthread_t threads[2];
    void *status;
    // Handle Ctrl+c so we we can shutdown gracefully
    signal(SIGINT, &ctrlc_handler);

    // Attempt to load the config file
    FILE *fp = fopen("/root/config_file", "r");
    if (fp != NULL)
    {
        fgets(config_filename, 64, fp);
        fclose(fp);
        while (strlen(config_filename) > 0 &&
                (config_filename[strlen(config_filename) - 1] == ' ' ||
                 config_filename[strlen(config_filename) - 1] == '\t' ||
                 config_filename[strlen(config_filename) - 1] == '\n' ||
                 config_filename[strlen(config_filename) - 1] == '\r'))
        {
            config_filename[strlen(config_filename) - 1] = 0;
        }
    }

    printf("Loading config \"%s\"\n\n", config_filename);
    if (!Configuration::instance()->load(config_filename))
    {
        fprintf(stderr, "Fatal Error: Could not load Korebot config file.\n");
        return -1;
    }

    // Init
    MotorController::instance()->quadInit();
    initializePID();
	initializeLQR();
	initializeBadLQR();
    initK();
    // Initialize data logging stuff
    Logger::instance()->initialize();

    gettimeofday(&t1, 0);
    startTime = (t1.tv_usec / 1000000.0) + t1.tv_sec;

    pthread_mutex_init(&mutex, NULL);
    pthread_create(&threads[0], NULL, UIThread, (void*)1);
    pthread_create(&threads[1], NULL, mainThread, (void*)2);
    pthread_join(threads[1], &status);

    return 0;
}

void* mainThread(void* threadID)
{
    cur_axis = PITCH;

    cout << "Press 1 to do PID control (no extra starting delay)\n";
    cout << "Press 3 to do PID control with a startup delay (3 second delay on motor startup for testing alone)\n";
    cout << "Press esc to quit\n";
    while (1)
    {
        cout << "\r";
        switch (cmd)
        {
            case '3':
                // 3 second pause for when someone is testing alone
                sleep(3);
            // Fall through to run normally
            case '1':
                // Set the cmd character back to null
                pthread_mutex_lock(&mutex);
                cmd = 0;
                pthread_mutex_unlock(&mutex);

                // Arm the quad motors
                arm_quad_motors();

                // Initialize vrpn and begin accepting data
                vrpn_init("192.168.0.120:3883", handle_pos);
                vrpn_go();
                break;

            case 27:    // esc key
                // Stop the program
                Logger::instance()->stop();
                MotorController::instance()->stop();
                exit(0);
                break;
        }
    }
}

void initK(){
    quad.K.A[0][0] =2.675;
    quad.K.A[0][1] =-1.239;
    quad.K.A[0][2] =0.6977;
    quad.K.A[0][3] =0.0002441;
    quad.K.A[0][4] =-0.003665;
    quad.K.A[0][5] =0.02074;
    quad.K.A[0][6] =0.01426;
    quad.K.A[0][7] =0.00918;
    quad.K.A[0][8] =0.03718;
    quad.K.A[0][9] =0.07524;
    quad.K.A[0][10]=-0.0674;

    quad.K.A[1][0] =1.965;
    quad.K.A[1][1] =-0.06594;
    quad.K.A[1][2] =-0.03088;
    quad.K.A[1][3] =0;
    quad.K.A[1][4] =0;
    quad.K.A[1][5] =0;
    quad.K.A[1][6] =0;
    quad.K.A[1][7] =0;
    quad.K.A[1][8] =0;
    quad.K.A[1][9] =0;
    quad.K.A[1][10]=0;

    quad.K.A[2][0] =-0.01672;
    quad.K.A[2][1] =0.4686;
    quad.K.A[2][2] =-0.01471;
    quad.K.A[2][3] =0;
    quad.K.A[2][4] =0;
    quad.K.A[2][5] =0;
    quad.K.A[2][6] =0;
    quad.K.A[2][7] =0;
    quad.K.A[2][8] =0;
    quad.K.A[2][9]=0;
    quad.K.A[2][10]=0;

    quad.K.A[3][0] =-0.0284;
    quad.K.A[3][1] =-0.03455;
    quad.K.A[3][2] =-0.01618;
    quad.K.A[3][3] =0;
    quad.K.A[3][4] =15.01;
    quad.K.A[3][5] =-84.96;
    quad.K.A[3][6] =-58.41;
    quad.K.A[3][7] =-37.6;
    quad.K.A[3][8] =-152.3;
    quad.K.A[3][9] =-308.2;
    quad.K.A[3][10]=276.1;

    quad.K.A[4][0] =-0.02937;
    quad.K.A[4][1] =-0.05516;
    quad.K.A[4][2] =-0.02583;
    quad.K.A[4][3] =0;
    quad.K.A[4][4] =0.3089;
    quad.K.A[4][5] =-0.1874;
    quad.K.A[4][6] =-0.9931;
    quad.K.A[4][7] =0.1502;
    quad.K.A[4][8] =0.7497;
    quad.K.A[4][9] =0.8501;
    quad.K.A[4][10]=-0.5594;

    quad.K.A[5][0] =-0.004257;
    quad.K.A[5][1] =-0.007995;
    quad.K.A[5][2] =-0.003744;
    quad.K.A[5][3] =0;
    quad.K.A[5][4] =0.3599 ;
    quad.K.A[5][5] =0.2961;
    quad.K.A[5][6] =-0.1727;
    quad.K.A[5][7] =-1.374;
    quad.K.A[5][8] =-1.14;
    quad.K.A[5][9] =0.3434;
    quad.K.A[5][10]=-0.5949;

    quad.K.A[6][0] =0.0174;
    quad.K.A[6][1] =0.03268;
    quad.K.A[6][2] =0.01531;
    quad.K.A[6][3] =0;
    quad.K.A[6][4] =0.1751;
    quad.K.A[6][5] =-0.3912;
    quad.K.A[6][6] =0.8357;
    quad.K.A[6][7] =0.2475;
    quad.K.A[6][8] =-1.543;
    quad.K.A[6][9] =-1.851;
    quad.K.A[6][10]=1.261;

    quad.K.A[7][0] =-0.004728;
    quad.K.A[7][1] =-0.008981;
    quad.K.A[7][2] =-0.004206;
    quad.K.A[7][3] =0;
    quad.K.A[7][4] =-0.2504;
    quad.K.A[7][5] =0.3968;
    quad.K.A[7][6] =0.03269;
    quad.K.A[7][7] =1.274;
    quad.K.A[7][8] =1.841;
    quad.K.A[7][9] =-0.8006;
    quad.K.A[7][10]=1.582;

    quad.K.A[8][0] =-0.003381;
    quad.K.A[8][1] =-0.006349;
    quad.K.A[8][2] =-0.002973;
    quad.K.A[8][3] =0;
    quad.K.A[8][4] =-0.1548;
    quad.K.A[8][5] =0.2483;
    quad.K.A[8][6] =0.03638;
    quad.K.A[8][7] =0.3484;
    quad.K.A[8][8] =1.519; 
    quad.K.A[8][9] =0.4115; 
    quad.K.A[8][10]=-0.1847;

    quad.K.A[9][0] =-0.001587;
    quad.K.A[9][1] =-0.002981;
    quad.K.A[9][2] =-0.001396;
    quad.K.A[9][3] =0;
    quad.K.A[9][4] =-0.06124;
    quad.K.A[9][5] =0.1008;
    quad.K.A[9][6] =0.01749;
    quad.K.A[9][7] =0.1392;
    quad.K.A[9][8] =0.1918;
    quad.K.A[9][9] =1.221;
    quad.K.A[9][10]=-0.4265;

    quad.K.A[10][0] =6.357*pow(10,-5);
    quad.K.A[10][1] =0.0001194;
    quad.K.A[10][2] =5.591*pow(10,-5);
    quad.K.A[10][3] =0;
    quad.K.A[10][4] =-0.0001074;
    quad.K.A[10][5] =0.0001772;
    quad.K.A[10][6] =3.109*pow(10,-5);
    quad.K.A[10][7] =0.0002432;
    quad.K.A[10][8] =0.0003256;
    quad.K.A[10][9] =0.0008199;
    quad.K.A[10][10]=0.7726;

    quad.K.B[0][0] =160.2;
    quad.K.B[1][0] =305.6;
    quad.K.B[2][0] =145.5;
    quad.K.B[3][0] =160.2;
    quad.K.B[4][0] =255.7;
    quad.K.B[5][0] =37.06;
    quad.K.B[6][0] =-151.5;
    quad.K.B[7][0] =41.63;
    quad.K.B[8][0] =29.43;
    quad.K.B[9][0] =13.82;
    quad.K.B[10][0]=-0.5534;

    quad.K.B[0][1] =-4.789*pow(10, -8);
    quad.K.B[1][1] =0;
    quad.K.B[2][1] =0;
    quad.K.B[3][1] =3716;
    quad.K.B[4][1] =0.009852;
    quad.K.B[5][1] =-0.028;
    quad.K.B[6][1] =-0.0192;
    quad.K.B[7][1] =-0.09229;
    quad.K.B[8][1] =0.2272;
    quad.K.B[9][1] =-0.5341;
    quad.K.B[10][1]=-0.4967;

    quad.K.C[0] =-328.7;
    quad.K.C[1] =285.1;
    quad.K.C[2] =-244.3;
    quad.K.C[3] =1;
    quad.K.C[4] =-15.01;
    quad.K.C[5] =84.96;
    quad.K.C[6] =58.41;
    quad.K.C[7] =37.6;
    quad.K.C[8] =152.3;
    quad.K.C[9] =308.2;
    quad.K.C[10]=-276.1;

    quad.K.D[0] =-160.2;
    quad.K.D[1] =-0.0001962;
    
    for(int i=0; i<11; i++){
        quad.K.x[i]=0;
        quad.K.xnext[i]=0;
    }
    quad.K.y=0;
    quad.K.ref=0;
    quad.K.pos=0;
}

void initializeLQR()
{

    quad.lqrControl.A[0][0]=1.000287641990169;
    quad.lqrControl.A[0][1]=0;
    quad.lqrControl.A[0][2]=0.010000958788249;
    quad.lqrControl.A[0][3]=0;
    quad.lqrControl.A[1][0]=0;
    quad.lqrControl.A[1][1]=quad.lqrControl.A[0][0];
    quad.lqrControl.A[1][2]=0;
    quad.lqrControl.A[1][3]=quad.lqrControl.A[0][2];
    quad.lqrControl.A[2][0]=0.057531155805553;
    quad.lqrControl.A[2][1]=0;
    quad.lqrControl.A[2][2]=quad.lqrControl.A[0][0];
    quad.lqrControl.A[2][3]=0;
    quad.lqrControl.A[3][0]=0;
    quad.lqrControl.A[3][1]=quad.lqrControl.A[2][0];
    quad.lqrControl.A[3][2]=0;
    quad.lqrControl.A[3][3]=quad.lqrControl.A[0][0];

    quad.lqrControl.B[0][0]=-8.824473591707657*pow(10, -11);
    quad.lqrControl.B[0][1]=-5.164606207574092*pow(10, -9);
    quad.lqrControl.B[0][2]=8.824473591707657*pow(10, -11);
    quad.lqrControl.B[0][3]=5.164606207574092*pow(10, -9);
    quad.lqrControl.B[1][0]=-quad.lqrControl.B[0][1];
    quad.lqrControl.B[1][1]=-quad.lqrControl.B[0][0];
    quad.lqrControl.B[1][2]=-quad.lqrControl.B[0][3];
    quad.lqrControl.B[1][3]=-quad.lqrControl.B[0][2];
    quad.lqrControl.B[2][0]=-1.764979323112678*pow(10, -8);
    quad.lqrControl.B[2][1]=-1.032970757253260*pow(10, -6);
    quad.lqrControl.B[2][2]=1.764979323112678*pow(10, -8);
    quad.lqrControl.B[2][3]=1.032970757253260*pow(10, -6);
    quad.lqrControl.B[3][0]=-quad.lqrControl.B[2][1];
    quad.lqrControl.B[3][1]=-quad.lqrControl.B[2][0];
    quad.lqrControl.B[3][2]=-quad.lqrControl.B[2][3];
    quad.lqrControl.B[3][3]=-quad.lqrControl.B[2][2];

    quad.lqrControl.C[0][0]=1;
    quad.lqrControl.C[0][1]=0;
    quad.lqrControl.C[0][2]=0;
    quad.lqrControl.C[0][3]=0;
    quad.lqrControl.C[1][0]=0;
    quad.lqrControl.C[1][1]=1;
    quad.lqrControl.C[1][2]=0;
    quad.lqrControl.C[1][3]=0;

    quad.lqrControl.K[0][0]=765.9604195408374;
    quad.lqrControl.K[0][1]=71974.33727330630;
    quad.lqrControl.K[0][2]=339.2475280355225;
    quad.lqrControl.K[0][3]=28201.50565659873;
    quad.lqrControl.K[1][0]=-quad.lqrControl.K[0][1];
    quad.lqrControl.K[1][1]=-quad.lqrControl.K[0][0];
    quad.lqrControl.K[1][2]=-quad.lqrControl.K[0][3];
    quad.lqrControl.K[1][3]=-quad.lqrControl.K[0][2];
    quad.lqrControl.K[2][0]=-quad.lqrControl.K[0][0];
    quad.lqrControl.K[2][1]=-quad.lqrControl.K[0][1];
    quad.lqrControl.K[2][2]=-quad.lqrControl.K[0][2];
    quad.lqrControl.K[2][3]=-quad.lqrControl.K[0][3];
    quad.lqrControl.K[1][0]=quad.lqrControl.K[0][1];
    quad.lqrControl.K[1][1]=quad.lqrControl.K[0][0];
    quad.lqrControl.K[1][2]=quad.lqrControl.K[0][3];
    quad.lqrControl.K[1][3]=quad.lqrControl.K[0][2];

    quad.lqrControl.L[0][0]=0.500575283980341;
    quad.lqrControl.L[0][1]=0;
    quad.lqrControl.L[1][0]=0;
    quad.lqrControl.L[1][1]=0.900575283980367;
    quad.lqrControl.L[2][0]=6.071344931707554;
    quad.lqrControl.L[2][1]=0;
    quad.lqrControl.L[3][0]=0;
    quad.lqrControl.L[3][1]=20.081507333407950;

    quad.lqrControl.N[0][0]=158.3696039358338;
    quad.lqrControl.N[0][1]=21850.55132351262;
    quad.lqrControl.N[1][0]=-quad.lqrControl.N[0][1];
    quad.lqrControl.N[1][1]=-quad.lqrControl.N[0][0];
    quad.lqrControl.N[2][0]=-491.5590424457500;
    quad.lqrControl.N[2][1]=-57433.18039733962;
    quad.lqrControl.N[3][0]=-quad.lqrControl.N[2][1];
    quad.lqrControl.N[3][1]=-quad.lqrControl.N[2][0];

	quad.lqrControl.BN[0][0]= quad.lqrControl.B[0][0]*quad.lqrControl.N[0][0]+quad.lqrControl.B[0][1]*quad.lqrControl.N[1][0]+quad.lqrControl.B[0][2]*quad.lqrControl.N[2][0]+quad.lqrControl.B[0][3]*quad.lqrControl.N[3][0];
	quad.lqrControl.BN[0][1]= quad.lqrControl.B[0][0]*quad.lqrControl.N[0][1]+quad.lqrControl.B[0][1]*quad.lqrControl.N[1][1]+quad.lqrControl.B[0][2]*quad.lqrControl.N[2][1]+quad.lqrControl.B[0][3]*quad.lqrControl.N[3][1];
	quad.lqrControl.BN[1][0]= quad.lqrControl.B[1][0]*quad.lqrControl.N[0][0]+quad.lqrControl.B[1][1]*quad.lqrControl.N[1][0]+quad.lqrControl.B[1][2]*quad.lqrControl.N[2][0]+quad.lqrControl.B[1][3]*quad.lqrControl.N[3][0];
	quad.lqrControl.BN[1][1]= quad.lqrControl.B[1][0]*quad.lqrControl.N[0][1]+quad.lqrControl.B[1][1]*quad.lqrControl.N[1][1]+quad.lqrControl.B[1][2]*quad.lqrControl.N[2][1]+quad.lqrControl.B[1][3]*quad.lqrControl.N[3][1];
	quad.lqrControl.BN[2][0]= quad.lqrControl.B[2][0]*quad.lqrControl.N[0][0]+quad.lqrControl.B[2][1]*quad.lqrControl.N[1][0]+quad.lqrControl.B[2][2]*quad.lqrControl.N[2][0]+quad.lqrControl.B[2][3]*quad.lqrControl.N[3][0];
	quad.lqrControl.BN[2][1]= quad.lqrControl.B[2][0]*quad.lqrControl.N[0][1]+quad.lqrControl.B[2][1]*quad.lqrControl.N[1][1]+quad.lqrControl.B[2][2]*quad.lqrControl.N[2][1]+quad.lqrControl.B[2][3]*quad.lqrControl.N[3][1];
	quad.lqrControl.BN[3][0]= quad.lqrControl.B[3][0]*quad.lqrControl.N[0][0]+quad.lqrControl.B[3][1]*quad.lqrControl.N[1][0]+quad.lqrControl.B[3][2]*quad.lqrControl.N[2][0]+quad.lqrControl.B[3][3]*quad.lqrControl.N[3][0];
	quad.lqrControl.BN[3][1]= quad.lqrControl.B[3][0]*quad.lqrControl.N[0][1]+quad.lqrControl.B[3][1]*quad.lqrControl.N[1][1]+quad.lqrControl.B[3][2]*quad.lqrControl.N[2][1]+quad.lqrControl.B[3][3]*quad.lqrControl.N[3][1];
	
	quad.lqrControl.Aestd[0][0]=quad.lqrControl.A[0][0]-quad.lqrControl.B[0][0]*quad.lqrControl.K[0][0]-quad.lqrControl.B[0][1]*quad.lqrControl.K[1][0]-quad.lqrControl.B[0][2]*quad.lqrControl.K[2][0]-quad.lqrControl.B[0][3]*quad.lqrControl.K[3][0]-quad.lqrControl.L[0][0]*quad.lqrControl.C[0][0]-quad.lqrControl.L[0][1]*quad.lqrControl.C[1][0];
	quad.lqrControl.Aestd[0][1]=quad.lqrControl.A[0][1]-quad.lqrControl.B[0][0]*quad.lqrControl.K[0][1]-quad.lqrControl.B[0][1]*quad.lqrControl.K[1][1]-quad.lqrControl.B[0][2]*quad.lqrControl.K[2][1]-quad.lqrControl.B[0][3]*quad.lqrControl.K[3][1]-quad.lqrControl.L[0][0]*quad.lqrControl.C[0][1]-quad.lqrControl.L[0][1]*quad.lqrControl.C[1][1];
	quad.lqrControl.Aestd[0][2]=quad.lqrControl.A[0][2]-quad.lqrControl.B[0][0]*quad.lqrControl.K[0][2]-quad.lqrControl.B[0][1]*quad.lqrControl.K[1][2]-quad.lqrControl.B[0][2]*quad.lqrControl.K[2][2]-quad.lqrControl.B[0][3]*quad.lqrControl.K[3][2]-quad.lqrControl.L[0][0]*quad.lqrControl.C[0][2]-quad.lqrControl.L[0][1]*quad.lqrControl.C[1][2];
	quad.lqrControl.Aestd[0][3]=quad.lqrControl.A[0][3]-quad.lqrControl.B[0][0]*quad.lqrControl.K[0][3]-quad.lqrControl.B[0][1]*quad.lqrControl.K[1][3]-quad.lqrControl.B[0][2]*quad.lqrControl.K[2][3]-quad.lqrControl.B[0][3]*quad.lqrControl.K[3][3]-quad.lqrControl.L[0][0]*quad.lqrControl.C[0][3]-quad.lqrControl.L[0][1]*quad.lqrControl.C[1][3];
	quad.lqrControl.Aestd[1][0]=quad.lqrControl.A[1][0]-quad.lqrControl.B[1][0]*quad.lqrControl.K[0][0]-quad.lqrControl.B[1][1]*quad.lqrControl.K[1][0]-quad.lqrControl.B[1][2]*quad.lqrControl.K[2][0]-quad.lqrControl.B[1][3]*quad.lqrControl.K[3][0]-quad.lqrControl.L[1][0]*quad.lqrControl.C[0][0]-quad.lqrControl.L[1][1]*quad.lqrControl.C[1][0];
	quad.lqrControl.Aestd[1][1]=quad.lqrControl.A[1][1]-quad.lqrControl.B[1][0]*quad.lqrControl.K[0][1]-quad.lqrControl.B[1][1]*quad.lqrControl.K[1][1]-quad.lqrControl.B[1][2]*quad.lqrControl.K[2][1]-quad.lqrControl.B[1][3]*quad.lqrControl.K[3][1]-quad.lqrControl.L[1][0]*quad.lqrControl.C[0][1]-quad.lqrControl.L[1][1]*quad.lqrControl.C[1][1];
	quad.lqrControl.Aestd[1][2]=quad.lqrControl.A[1][2]-quad.lqrControl.B[1][0]*quad.lqrControl.K[0][2]-quad.lqrControl.B[1][1]*quad.lqrControl.K[1][2]-quad.lqrControl.B[1][2]*quad.lqrControl.K[2][2]-quad.lqrControl.B[1][3]*quad.lqrControl.K[3][2]-quad.lqrControl.L[1][0]*quad.lqrControl.C[0][2]-quad.lqrControl.L[1][1]*quad.lqrControl.C[1][2];
	quad.lqrControl.Aestd[1][3]=quad.lqrControl.A[1][3]-quad.lqrControl.B[1][0]*quad.lqrControl.K[0][3]-quad.lqrControl.B[1][1]*quad.lqrControl.K[1][3]-quad.lqrControl.B[1][2]*quad.lqrControl.K[2][3]-quad.lqrControl.B[1][3]*quad.lqrControl.K[3][3]-quad.lqrControl.L[1][0]*quad.lqrControl.C[0][3]-quad.lqrControl.L[1][1]*quad.lqrControl.C[1][3];
	quad.lqrControl.Aestd[2][0]=quad.lqrControl.A[2][0]-quad.lqrControl.B[2][0]*quad.lqrControl.K[0][0]-quad.lqrControl.B[2][1]*quad.lqrControl.K[1][0]-quad.lqrControl.B[2][2]*quad.lqrControl.K[2][0]-quad.lqrControl.B[2][3]*quad.lqrControl.K[3][0]-quad.lqrControl.L[2][0]*quad.lqrControl.C[0][0]-quad.lqrControl.L[2][1]*quad.lqrControl.C[1][0];
	quad.lqrControl.Aestd[2][1]=quad.lqrControl.A[2][1]-quad.lqrControl.B[2][0]*quad.lqrControl.K[0][1]-quad.lqrControl.B[2][1]*quad.lqrControl.K[1][1]-quad.lqrControl.B[2][2]*quad.lqrControl.K[2][1]-quad.lqrControl.B[2][3]*quad.lqrControl.K[3][1]-quad.lqrControl.L[2][0]*quad.lqrControl.C[0][1]-quad.lqrControl.L[2][1]*quad.lqrControl.C[1][1];
	quad.lqrControl.Aestd[2][2]=quad.lqrControl.A[2][2]-quad.lqrControl.B[2][0]*quad.lqrControl.K[0][2]-quad.lqrControl.B[2][1]*quad.lqrControl.K[1][2]-quad.lqrControl.B[2][2]*quad.lqrControl.K[2][2]-quad.lqrControl.B[2][3]*quad.lqrControl.K[3][2]-quad.lqrControl.L[2][0]*quad.lqrControl.C[0][2]-quad.lqrControl.L[2][1]*quad.lqrControl.C[1][2];
	quad.lqrControl.Aestd[2][3]=quad.lqrControl.A[2][3]-quad.lqrControl.B[2][0]*quad.lqrControl.K[0][3]-quad.lqrControl.B[2][1]*quad.lqrControl.K[1][3]-quad.lqrControl.B[2][2]*quad.lqrControl.K[2][3]-quad.lqrControl.B[2][3]*quad.lqrControl.K[3][3]-quad.lqrControl.L[2][0]*quad.lqrControl.C[0][3]-quad.lqrControl.L[2][1]*quad.lqrControl.C[1][3];
	quad.lqrControl.Aestd[3][0]=quad.lqrControl.A[3][0]-quad.lqrControl.B[3][0]*quad.lqrControl.K[0][0]-quad.lqrControl.B[3][1]*quad.lqrControl.K[1][0]-quad.lqrControl.B[3][2]*quad.lqrControl.K[2][0]-quad.lqrControl.B[3][3]*quad.lqrControl.K[3][0]-quad.lqrControl.L[3][0]*quad.lqrControl.C[0][0]-quad.lqrControl.L[3][1]*quad.lqrControl.C[1][0];
	quad.lqrControl.Aestd[3][1]=quad.lqrControl.A[3][1]-quad.lqrControl.B[3][0]*quad.lqrControl.K[0][1]-quad.lqrControl.B[3][1]*quad.lqrControl.K[1][1]-quad.lqrControl.B[3][2]*quad.lqrControl.K[2][1]-quad.lqrControl.B[3][3]*quad.lqrControl.K[3][1]-quad.lqrControl.L[3][0]*quad.lqrControl.C[0][1]-quad.lqrControl.L[3][1]*quad.lqrControl.C[1][1];
	quad.lqrControl.Aestd[3][2]=quad.lqrControl.A[3][2]-quad.lqrControl.B[3][0]*quad.lqrControl.K[0][2]-quad.lqrControl.B[3][1]*quad.lqrControl.K[1][2]-quad.lqrControl.B[3][2]*quad.lqrControl.K[2][2]-quad.lqrControl.B[3][3]*quad.lqrControl.K[3][2]-quad.lqrControl.L[3][0]*quad.lqrControl.C[0][2]-quad.lqrControl.L[3][1]*quad.lqrControl.C[1][2];
	quad.lqrControl.Aestd[3][3]=quad.lqrControl.A[3][3]-quad.lqrControl.B[3][0]*quad.lqrControl.K[0][3]-quad.lqrControl.B[3][1]*quad.lqrControl.K[1][3]-quad.lqrControl.B[3][2]*quad.lqrControl.K[2][3]-quad.lqrControl.B[3][3]*quad.lqrControl.K[3][3]-quad.lqrControl.L[3][0]*quad.lqrControl.C[0][3]-quad.lqrControl.L[3][1]*quad.lqrControl.C[1][3];
	
	quad.lqrControl.x[0]=0;
	quad.lqrControl.x[1]=0;
	quad.lqrControl.x[2]=0;
	quad.lqrControl.x[3]=0;
	
	quad.lqrControl.u[0]=0;
	quad.lqrControl.u[1]=0;
	quad.lqrControl.u[2]=0;
	quad.lqrControl.u[3]=0;
	
	quad.lqrControl.x_next[0]=0;
	quad.lqrControl.x_next[1]=0;
	quad.lqrControl.x_next[2]=0;
	quad.lqrControl.x_next[3]=0;
	
	quad.lqrControl.desired_pitch=0;
	quad.lqrControl.desired_roll=0;
}

void initializeBadLQR()
{
	/*quad.lqrBad.K[0][0]=10.7278;
	quad.lqrBad.K[0][1]=0.0989;//-
	quad.lqrBad.K[0][2]=-10.7278;
	quad.lqrBad.K[0][3]=-0.0989;//+
	quad.lqrBad.K[0][4]=532.1574;
	quad.lqrBad.K[0][5]=1.7933*pow(10,5);
	quad.lqrBad.K[0][6]=317.8496;
	quad.lqrBad.K[0][7]=4.4245*pow(10,4);
	
	quad.lqrBad.K[1][0]=0.0989;//-
	quad.lqrBad.K[1][1]=10.7278;
	quad.lqrBad.K[1][2]=-0.0989;//+
	quad.lqrBad.K[1][3]=-10.7278;
	quad.lqrBad.K[1][4]=-1.7933*pow(10,5);//+
	quad.lqrBad.K[1][5]=-532.1574;//+
	quad.lqrBad.K[1][6]=-4.4245*pow(10,4);//+
	quad.lqrBad.K[1][7]=-317.8496;//+
	
	quad.lqrBad.K[2][0]=-10.7278;
	quad.lqrBad.K[2][1]=-0.0989;//+
	quad.lqrBad.K[2][2]=10.7278;
	quad.lqrBad.K[2][3]=0.0989;//-
	quad.lqrBad.K[2][4]=-532.1574;
	quad.lqrBad.K[2][5]=-1.7933*pow(10,5);
	quad.lqrBad.K[2][6]=-317.8496;
	quad.lqrBad.K[2][7]=-4.4245*pow(10,4);
	
	quad.lqrBad.K[3][0]=-0.0989;//+
	quad.lqrBad.K[3][1]=-10.7278;
	quad.lqrBad.K[3][2]=0.0989;//-
	quad.lqrBad.K[3][3]=10.7278;
	quad.lqrBad.K[3][4]=1.7933*pow(10,5);//-
	quad.lqrBad.K[3][5]=532.1574;//-
	quad.lqrBad.K[3][6]=4.4245*pow(10,4);//-
	quad.lqrBad.K[3][7]=317.8496;//-
	
	quad.lqrBad.N[0][0]=141.3516;
	quad.lqrBad.N[0][1]=1.3451*pow(10,5);
	quad.lqrBad.N[1][0]=-1.3451*pow(10,5);
	quad.lqrBad.N[1][1]=-141.3516;
	quad.lqrBad.N[2][0]=-474.5410;
	quad.lqrBad.N[2][1]=-1.7009*pow(10, 5);
	quad.lqrBad.N[3][0]=1.7009*pow(10, 5);
	quad.lqrBad.N[3][1]=474.5410;
	*/
    quad.lqrBad.N[0][0]=158.3696039358338;
    quad.lqrBad.N[0][1]=21850.55132351262;
    quad.lqrBad.N[1][0]=-quad.lqrBad.N[0][1];
    quad.lqrBad.N[1][1]=-quad.lqrBad.N[0][0];
    quad.lqrBad.N[2][0]=-491.5590424457500;
    quad.lqrBad.N[2][1]=-57433.18039733962;
    quad.lqrBad.N[3][0]=-quad.lqrBad.N[2][1];
    quad.lqrBad.N[3][1]=-quad.lqrBad.N[2][0];

    quad.lqrBad.K[0][0]=6.214216019240372;
    quad.lqrBad.K[0][1]=0.044023776171092;
    quad.lqrBad.K[0][2]=-6.214216019137907;
    quad.lqrBad.K[0][3]=-0.044023776010638;
    quad.lqrBad.K[0][4]=545.2208738248235;
    quad.lqrBad.K[0][5]=62472.55399566490;
    quad.lqrBad.K[0][6]=241.0072786718822;
    quad.lqrBad.K[0][7]=23623.84720675836;
    quad.lqrBad.K[1][0]=0.044023776178144;
    quad.lqrBad.K[1][1]=6.214216019192959;
    quad.lqrBad.K[1][2]=-0.044023776177134;
    quad.lqrBad.K[1][3]=-6.214216019191290;
    quad.lqrBad.K[1][4]=-62472.55399543191;
    quad.lqrBad.K[1][5]=-545.2208729292178;
    quad.lqrBad.K[1][6]=-23623.84720659507;
    quad.lqrBad.K[1][7]=-241.0072784674231;
    quad.lqrBad.K[2][0]=-6.214216019122387;
    quad.lqrBad.K[2][1]=-0.044023776169924;
    quad.lqrBad.K[2][2]=6.214216019019892;
    quad.lqrBad.K[2][3]=0.044023776009603;
    quad.lqrBad.K[2][4]=-545.2208738174472;
    quad.lqrBad.K[2][5]=-62472.55399451842;
    quad.lqrBad.K[2][6]=-241.0072786684346;
    quad.lqrBad.K[2][7]=-23623.84720631747;
    quad.lqrBad.K[3][0]=-0.044023775993404;
    quad.lqrBad.K[3][1]=-6.214216019191033;
    quad.lqrBad.K[3][2]=0.044023775992528;
    quad.lqrBad.K[3][3]=6.214216019189404;
    quad.lqrBad.K[3][4]=62472.55399544383;
    quad.lqrBad.K[3][5]=545.2208747212894;
    quad.lqrBad.K[3][6]=23623.84720660027;
    quad.lqrBad.K[3][7]=241.0072791573342;

	quad.lqrBad.pitch_pre=0;
	quad.lqrBad.roll_pre=0;
	quad.lqrBad.omega_slope=0.0218;
	quad.lqrBad.timeStep=.01;
	quad.lqrBad.desired_pitch=0;
	quad.lqrBad.desired_roll=0;
	
	quad.lqrBad.u[0]=0;
	quad.lqrBad.u[1]=0;
	quad.lqrBad.u[2]=0;
	quad.lqrBad.u[3]=0;
}

void initializePID()
{
    quad.pitch.desired = Configuration::instance()->getValue("pendulum.pitch.setpoint", (float)0);
    quad.pitch.p       = Configuration::instance()->getValue("pendulum.pitch.pid_p",    (int)0);
    quad.pitch.i       = Configuration::instance()->getValue("pendulum.pitch.pid_i",    (int)0);
    quad.pitch.d       = Configuration::instance()->getValue("pendulum.pitch.pid_d",    (int)0);
    quad.pitch.filtErrorPre=0;
    quad.pitch.filtGain=0.84189;
    quad.pitch.errorGain=0.1583;  
    quad.roll.desired  = Configuration::instance()->getValue("pendulum.roll.setpoint",  (float)0);
    quad.roll.p        = Configuration::instance()->getValue("pendulum.roll.pid_p",     (int)0);
    quad.roll.i        = Configuration::instance()->getValue("pendulum.roll.pid_i",     (int)0);
    quad.roll.d        = Configuration::instance()->getValue("pendulum.roll.pid_d",     (int)0);
    quad.roll.filtErrorPre=0;
    quad.roll.filtGain=0.84189;
    quad.roll.errorGain=0.1583;   
    quad.controlTime=0; 
    quad.roll.desiredSet=0.0;
    quad.pitch.desiredSet=0.0;
    quad.roll.timeSum=0.0;

    // TODO: Dylan, move this - it is here for logging.
    MotorController::instance()->setQuad(&quad); 
}

void PID_controller(vrpn_data_t *vrpnData)
{
    tic();
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;

    control_value_adjust();

    pthread_mutex_lock(&mutex);
    cmd = NULL;
    pthread_mutex_unlock(&mutex);
    cout << "1: Edit Pitch PID" << endl << "3: Edit Roll PID" << endl;
    cout << "7: Increase P" << endl << "4: Decrease P" << endl;
    cout << "8: Increase I" << endl << "5: Decrease I" << endl;
    cout << "9: Increase D" << endl << "6: Decrease D" << endl;
    cout << "i: Increase Setpoint" << endl << "k: Decrease Stepoint" << endl;
    cout << "2: Reset cumulitive I" << endl << "q: Shut off motors and quit" << endl << endl;
    if (cur_axis == PITCH)
    {
        cout << "EDITING PITCH" << endl << endl;
    }
    else if (cur_axis == ROLL)
    {
        cout <<"EDITING ROLL" << endl << endl;
    }
    cout << setprecision(3) << fixed << setw(6);
    cout << "Pitch" << endl << "Current P: " << quad.pitch.p << " Current I: " << quad.pitch.i
         << " Current D: " << quad.pitch.d << " Setpoint: " << quad.pitch.desired << endl;
    cout << "Roll" << endl << "Current P: " << quad.roll.p << " Current I: " << quad.roll.i
         << " Current D: " << quad.roll.d << " Setpoint: " << quad.roll.desired 
         << "Elia K: " << quad.K.y << endl;
    cout << "Control Time: " << quad.controlTime << endl;

    // PID control
    quad.pitch.current = vrpnData->pitch;
    quad.roll.current = vrpnData->roll;
    quad.yaw.current = vrpnData->yaw;
    quad.K.pos=vrpnData->roll;
    //quad.roll.desired=rampCalc(quad.roll.desiredSet, quad.roll.desired);
    //quad.roll.desired=boxSize*sin(2*3.14159*freq*quad.roll.timeSum);
    //MotorController::instance()->pidControl(&quad.pitch);
    //MotorController::instance()->pidControl(&quad.roll);
    MotorController::instance()->pidControlFilt(&quad.pitch);
    MotorController::instance()->pidControlFilt(&quad.roll);
    //MotorController::instance()->KControl(&quad.K);
    //MotorController::instance()->pidControl(&quad.yaw);

    // PID correction normalization for PWM
    //PWM_value_correction_pitch = (int)normalize(quad.pitch.correction, PID_SATURATION_MIN, PID_SATURATION_MAX, -DUTY_CYCLE_HALF_RANGE,
                                 //DUTY_CYCLE_HALF_RANGE);
    //PWM_value_correction_roll = (int)normalize(quad.roll.correction, PID_SATURATION_MIN, PID_SATURATION_MAX, -DUTY_CYCLE_HALF_RANGE,
                                //DUTY_CYCLE_HALF_RANGE);
    PWM_value_correction_pitch=30*quad.pitch.correction;
    PWM_value_correction_roll=30*quad.roll.correction;
    //PWM_value_correction_roll=30*quad.K.y;
    //PWM_value_correction_opposite = (int)normalize(-quad.pitch.correction, PID_SATURATION_MIN, PID_SATURATION_MAX,
    //          DUTY_CYCLE_MIN, DUTY_CYCLE_MAX);

    quad.motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    if(quad.motor_1_pwm<DUTY_CYCLE_MIN){
        quad.motor_1_pwm=DUTY_CYCLE_MIN;
    }else if(quad.motor_1_pwm>DUTY_CYCLE_MAX){
        quad.motor_1_pwm=DUTY_CYCLE_MAX;
    }
    quad.motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    //quad.motor_2_pwm=DUTY_CYCLE_MIN+quad.roll.desiredSet;
    if(quad.motor_2_pwm<DUTY_CYCLE_MIN){
        quad.motor_2_pwm=DUTY_CYCLE_MIN;
    }else if(quad.motor_2_pwm>DUTY_CYCLE_MAX){
        quad.motor_2_pwm=DUTY_CYCLE_MAX;
    }
    quad.motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    if(quad.motor_3_pwm<DUTY_CYCLE_MIN){
        quad.motor_3_pwm=DUTY_CYCLE_MIN;
    }else if(quad.motor_3_pwm>DUTY_CYCLE_MAX){
        quad.motor_3_pwm=DUTY_CYCLE_MAX;
    }
    quad.motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;
    //quad.motor_4_pwm=DUTY_CYCLE_MIN+quad.roll.desiredSet;
    if(quad.motor_4_pwm<DUTY_CYCLE_MIN){
        quad.motor_4_pwm=DUTY_CYCLE_MIN;
    }else if(quad.motor_4_pwm>DUTY_CYCLE_MAX){
        quad.motor_4_pwm=DUTY_CYCLE_MAX;
    }
    MotorController::instance()->setDutyCycle(quad.motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(quad.motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(quad.motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(quad.motor_4_pwm, 4);
    
	//LQR controller implementation
    /*
	normalizeLQR(quad.lqrControl.u, 4);
	quad.motor_1_pwm = DUTY_CYCLE_MID + (int)quad.lqrControl.u[0];
	quad.motor_2_pwm = DUTY_CYCLE_MID + (int)quad.lqrControl.u[1];
    quad.motor_3_pwm = DUTY_CYCLE_MID + (int)quad.lqrControl.u[2];
    quad.motor_4_pwm = DUTY_CYCLE_MID + (int)quad.lqrControl.u[3];

    MotorController::instance()->setDutyCycle(quad.motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(quad.motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(quad.motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(quad.motor_4_pwm, 4);
	
	quad.lqrControl.y[0]=(float)vrpnData->roll;
	quad.lqrControl.y[1]=(float)vrpnData->pitch;
	MotorController::instance()->lqrControl(&quad.lqrControl);
	*/
	//LQR controller for 8 state estimator (this estimator isn't really a valid estimator in terms mathematical control theory but should produce a good estimation of both angular velocity and motor velocity)
	/*quad.lqrBad.x[4]=(float)vrpnData->roll;
	quad.lqrBad.x[5]=(float)vrpnData->pitch;
	MotorController::instance()->lqrBad(&quad.lqrBad);
	normalizeLQR(quad.lqrBad.u, 4);
	
	quad.motor_1_pwm = DUTY_CYCLE_MID + (int)quad.lqrBad.u[0];
	quad.motor_2_pwm = DUTY_CYCLE_MID + (int)quad.lqrBad.u[3];
    quad.motor_3_pwm = DUTY_CYCLE_MID + (int)quad.lqrBad.u[2];
    quad.motor_4_pwm = DUTY_CYCLE_MID + (int)quad.lqrBad.u[1];
    
    MotorController::instance()->setDutyCycle(quad.motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(quad.motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(quad.motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(quad.motor_4_pwm, 4);
    */
    //startSine(quad.vrpnNow);
    boxStepFunction(vrpnData->usec);
    quad.pitch.current=(float)vrpnData->pitch;
    quad.roll.current=(float)vrpnData->roll;
    cout << CLEAR;
    quad.controlTime = (quad.controlTime + toc())/2;
}
double rampCalc(double refDes, double ref){
    double error=refDes-ref;
    double refNew;
    if(error>0){
        refNew=ref+rampSlope*.01;
        if(refNew>refDes){
            refNew=refDes;
        }
    }else if(error<0){
        refNew=ref-rampSlope*.01;
        if(refNew<refDes){
            refNew=refDes;
        }
    }else{
        refNew=refDes;
    }
    return refNew;
}
// *************** VRPN stuff ***************//
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t)
{

    q_vec_type euler;
    q_to_euler(euler, t.quat);

    quad.vrpnNow = 0;
    vrpn_data_t *vrpnData;
    vrpnData = (vrpn_data_t*) malloc(sizeof(vrpn_data_t));

    vrpnData->usec = t.msg_time.tv_sec
                     + (t.msg_time.tv_usec / 1000000.0);
    vrpnData->x = t.pos[0];
    vrpnData->y = t.pos[1];
    vrpnData->z = t.pos[2];
    vrpnData->yaw = euler[0];
    vrpnData->pitch = euler[1];
    vrpnData->roll = euler[2];
	
    quad.vrpnPrev = quad.vrpnNow;
    if (quad.vrpnNow == 0)
    {
        quad.vrpnPrev = 0;
        quad.vrpnTime0 = vrpnData->usec;
        quad.vrpnNow = vrpnData->usec;
    }
    else
    {
        quad.vrpnNow = vrpnData->usec - quad.vrpnTime0;
    }
    // TODO: Dylan, make the vrpn_data_t global (not really, but kinda)
    //       because we only ever need 1 of these.

    free(vrpnData);
    PID_controller(vrpnData);

    Logger::instance()->logData();
}
// *************** VRPN stuff ***************//
void startSine(double curTime){
    if(100000<curTime){
        quad.roll.timeSum+=.01;
    }else{
        quad.roll.timeSum=0;
    }
}

/**
 * This function performs a demo by moving in a box formation (first moves to a corner from center,
 * then steps around in a square). It takes 9 steps to be centered again.
 *
 * This function uses the following global variables"
 *  nextChange  - Time in seconds of the next state change
 *  state       - The current state - the position in the box
 *  boxSize     - Angle size in radians of the box
 *  changeRate  - Time in seconds between each state change
 *
 * @param curTime  is the current vrpn time
 */
void boxStepFunction(double curTime)
{
    // If it is the first time in the loop, set nextChange to the current time
    if (nextChange == 0)
    {
        nextChange = curTime;
    }
    else if (nextChange <= curTime)
    {
        nextChange = curTime += changeRate;
        switch (state)
        {
            //1D Roll
            case 0:
                state = 1;
                quad.lqrControl.desired_pitch = 0.0;
                quad.lqrControl.desired_roll  = 0.0;
                quad.lqrBad.desired_pitch = 0.0;
                quad.lqrBad.desired_roll  = 0.0;
                quad.K.ref=0.0;
                quad.roll.desiredSet= 0.0;
                quad.roll.desired= 0.0;
                quad.pitch.desired=0.0;
                break;
            case 1:
                state = 2;
               // quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = -boxSize;
                //quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.K.ref=-boxSize;
                quad.roll.desiredSet=-boxSize;
                 quad.roll.desired=-boxSize;
                //quad.pitch.desired=-boxSize;
                break;
            case 2:
                state = 3;
                //quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = 0.0;
                //quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = 0.0;
                quad.K.ref=0.0;
                quad.roll.desiredSet=0.0;
                quad.roll.desired=0.0;
                //quad.pitch.desired=-boxSize;
                break;
            case 3:
                state = 0;
                //quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = boxSize;
                //quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = boxSize;
                quad.K.ref=boxSize;
                quad.roll.desiredSet=boxSize;
                quad.roll.desired=boxSize;
                //quad.pitch.desired=-boxSize;
                break;
            case 4:
                state = 5;
                quad.lqrControl.desired_pitch = 0;
                //quad.lqrControl.desired_roll  = boxSize;
                quad.lqrBad.desired_pitch = 0;
                //quad.lqrBad.desired_roll  = boxSize;
                //quad.roll.desired=boxSize;
                quad.pitch.desired=0;
                break;
            case 5:
                state = 6;
                quad.lqrControl.desired_pitch = boxSize;
                //quad.lqrControl.desired_roll  = boxSize;
                quad.lqrBad.desired_pitch = boxSize;
                //quad.lqrBad.desired_roll  = boxSize;
                //quad.roll.desired=boxSize;
                quad.pitch.desired=boxSize;
                break;
            case 6:
                state = 7;
                quad.lqrControl.desired_pitch = boxSize;
                quad.lqrControl.desired_roll  = 0.0;
                quad.lqrBad.desired_pitch = boxSize;
                quad.lqrBad.desired_roll  = 0.0;
                quad.roll.desired=0;
                quad.pitch.desired=boxSize;
                break;
            case 7:
                state = 8;
                quad.lqrControl.desired_pitch = boxSize;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = boxSize;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=boxSize;
                break;
            case 8:
                state = 9;
                quad.lqrControl.desired_pitch = 0.0;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = 0.0;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=0.0;
                break;
            case 9:
                state = 0;
                quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=-boxSize;
                break;
            //1D Pitch
            /*case 0:
                state = 1;
                quad.lqrControl.desired_pitch = 0.0;
                quad.lqrControl.desired_roll  = 0.0;
                quad.lqrBad.desired_pitch = 0.0;
                quad.lqrBad.desired_roll  = 0.0;
                quad.roll.desired=0.0;
                quad.pitch.desired=0.0;
                break;
            case 1:
                state = 4;
                quad.lqrControl.desired_pitch = -boxSize;
                //quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = -boxSize;
                //quad.lqrBad.desired_roll  = -boxSize;
                //quad.roll.desired=-boxSize;
                quad.pitch.desired=-boxSize;
                break;
            case 2:
                state = 3;
                quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = 0.0;
                quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = 0.0;
                quad.roll.desired=0.0;
                quad.pitch.desired=-boxSize;
                break;
            case 3:
                state = 4;
                quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = boxSize;
                quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = boxSize;
                quad.roll.desired=boxSize;
                quad.pitch.desired=-boxSize;
                break;
            case 4:
                state = 5;
                quad.lqrControl.desired_pitch = 0;
                //quad.lqrControl.desired_roll  = boxSize;
                quad.lqrBad.desired_pitch = 0;
                //quad.lqrBad.desired_roll  = boxSize;
                //quad.roll.desired=boxSize;
                quad.pitch.desired=0;
                break;
            case 5:
                state = 0;
                quad.lqrControl.desired_pitch = boxSize;
                //quad.lqrControl.desired_roll  = boxSize;
                quad.lqrBad.desired_pitch = boxSize;
                //quad.lqrBad.desired_roll  = boxSize;
                //quad.roll.desired=boxSize;
                quad.pitch.desired=boxSize;
                break;
            case 6:
                state = 7;
                quad.lqrControl.desired_pitch = boxSize;
                quad.lqrControl.desired_roll  = 0.0;
                quad.lqrBad.desired_pitch = boxSize;
                quad.lqrBad.desired_roll  = 0.0;
                quad.roll.desired=0;
                quad.pitch.desired=boxSize;
                break;
            case 7:
                state = 8;
                quad.lqrControl.desired_pitch = boxSize;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = boxSize;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=boxSize;
                break;
            case 8:
                state = 9;
                quad.lqrControl.desired_pitch = 0.0;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = 0.0;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=0.0;
                break;
            case 9:
                state = 0;
                quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=-boxSize;
                break;*/
            //2D box
            /*case 0:
                state = 1;
                quad.lqrControl.desired_pitch = 0.0;
                quad.lqrControl.desired_roll  = 0.0;
                quad.lqrBad.desired_pitch = 0.0;
                quad.lqrBad.desired_roll  = 0.0;
                quad.roll.desired=0.0;
                quad.pitch.desired=0.0;
                break;
            case 1:
                state = 2;
                quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=-boxSize;
                break;
            case 2:
                state = 3;
                quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = 0.0;
                quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = 0.0;
                quad.roll.desired=0.0;
                quad.pitch.desired=-boxSize;
                break;
            case 3:
                state = 4;
                quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = boxSize;
                quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = boxSize;
                quad.roll.desired=boxSize;
                quad.pitch.desired=-boxSize;
                break;
            case 4:
                state = 5;
                quad.lqrControl.desired_pitch = 0;
                quad.lqrControl.desired_roll  = boxSize;
                quad.lqrBad.desired_pitch = 0;
                quad.lqrBad.desired_roll  = boxSize;
                quad.roll.desired=boxSize;
                quad.pitch.desired=0;
                break;
            case 5:
                state = 6;
                quad.lqrControl.desired_pitch = boxSize;
                quad.lqrControl.desired_roll  = boxSize;
                quad.lqrBad.desired_pitch = boxSize;
                quad.lqrBad.desired_roll  = boxSize;
                quad.roll.desired=boxSize;
                quad.pitch.desired=boxSize;
                break;
            case 6:
                state = 7;
                quad.lqrControl.desired_pitch = boxSize;
                quad.lqrControl.desired_roll  = 0.0;
                quad.lqrBad.desired_pitch = boxSize;
                quad.lqrBad.desired_roll  = 0.0;
                quad.roll.desired=0;
                quad.pitch.desired=boxSize;
                break;
            case 7:
                state = 8;
                quad.lqrControl.desired_pitch = boxSize;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = boxSize;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=boxSize;
                break;
            case 8:
                state = 9;
                quad.lqrControl.desired_pitch = 0.0;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = 0.0;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=0.0;
                break;
            case 9:
                state = 0;
                quad.lqrControl.desired_pitch = -boxSize;
                quad.lqrControl.desired_roll  = -boxSize;
                quad.lqrBad.desired_pitch = -boxSize;
                quad.lqrBad.desired_roll  = -boxSize;
                quad.roll.desired=-boxSize;
                quad.pitch.desired=-boxSize;
                break; */
        }
    }
}

 void normalizeLQR(float *u, int size)
{
	for(int i=0; i<size; i++)
	{
		if(u[i]>15000)
		{
			u[i]=15000;
		}
		else if(u[i]< -15000)
		{
			u[i]=-15000;
		}
	}
}

/**
 * Normalizes a number within range A and B to be equivalent in range C to D
 */
double normalize(double num, int A, int B, int C, int D)
{
    double newValue = 0;

    newValue = C + (num - A) * (D - C) / (B - A);

    return newValue;
}

void arm_quad_motors()
{
    /*
    MotorController::instance()->setDutyCycle(DUTY_CYCLE_MIN, 1);
    MotorController::instance()->setDutyCycle(DUTY_CYCLE_MIN, 2);
    MotorController::instance()->setDutyCycle(DUTY_CYCLE_MIN, 3);
    MotorController::instance()->setDutyCycle(DUTY_CYCLE_MIN, 4);
    */
    
    int pwm_duty_cycle = 20000;
    int increment_amount = 1000;

    while (pwm_duty_cycle < 40000)
    {
        pwm_duty_cycle += increment_amount;
        MotorController::instance()->setDutyCycle(pwm_duty_cycle, 1);
        MotorController::instance()->setDutyCycle(pwm_duty_cycle, 2);
        MotorController::instance()->setDutyCycle(pwm_duty_cycle, 3);
        MotorController::instance()->setDutyCycle(pwm_duty_cycle, 4);
    }
    sleep(1);
    MotorController::instance()->setDutyCycle(28000, 1);
    MotorController::instance()->setDutyCycle(28000, 2);
    MotorController::instance()->setDutyCycle(28000, 3);
    MotorController::instance()->setDutyCycle(28000, 4);
    
}

char getch()
{
    char buf = 0;
    struct termios old = {0};

    if (tcgetattr(0, &old) < 0)
    {
        perror("tcsetattr()");
    }
    old.c_lflag &= ~ICANON;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
    {
        perror("tcsetattr ICANON");
    }
    if (read(0, &buf, 1) < 0)
    {
        perror ("read()");
    }
    old.c_lflag |= ICANON;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
    {
        perror ("tcsetattr ~ICANON");
    }
    return (buf);
}

void control_value_adjust()
{
    // TODO: Add control for adjusting setpoint (with limits on angle)
    switch (cmd)
    {
        case '7':
            if (cur_axis == PITCH) quad.pitch.p += 1;
            else if (cur_axis == ROLL) quad.roll.p += 1;
            break;
        case '4':
            if (cur_axis == PITCH) quad.pitch.p -= 1;
            else if (cur_axis == ROLL) quad.roll.p -= 1;
            break;
        case '8':
            if (cur_axis == PITCH) quad.pitch.i += 1;
            else if (cur_axis == ROLL) quad.roll.i += 1;
            break;
        case '5':
            if (cur_axis == PITCH) quad.pitch.i -= 1;
            else if (cur_axis == ROLL) quad.roll.i -= 1;
            break;
        case '9':
            if (cur_axis == PITCH) quad.pitch.d += 1;
            else if (cur_axis == ROLL) quad.roll.d += 1;
            break;
        case '6':
            if (cur_axis == PITCH) quad.pitch.d -= 1;
            else if (cur_axis == ROLL) quad.roll.d -= 1;
            break;
        case '2':
            if (cur_axis == PITCH) quad.pitch.PID_icomp = 0;
            else if (cur_axis == ROLL) quad.roll.PID_icomp = 0;
            break;
        case 'i':
            if (cur_axis == PITCH)
            {
                quad.lqrBad.desired_pitch += 0.01;
				quad.lqrControl.desired_pitch += 0.01;
                quad.pitch.desired += 0.01;
            }
            else if (cur_axis == ROLL)
            {
                quad.lqrBad.desired_roll += 0.01;
				quad.lqrControl.desired_roll += 0.01;
                quad.roll.desired += 0.01;
            }
            break;
        case 'k':
            if (cur_axis == PITCH)
            {
                quad.lqrBad.desired_pitch -= 0.01;
                quad.lqrControl.desired_pitch -= 0.01;
                quad.pitch.desired -= 0.01;
            }
            else if (cur_axis == ROLL)
            {
                quad.lqrBad.desired_roll -= 0.01;
                quad.lqrControl.desired_roll -= 0.01;
                quad.roll.desired -= 0.01;
            }
            break;
        case '1':
            cur_axis = PITCH;
            break;
        case '3':
            cur_axis = ROLL;
            break;
        case 'm' :
            quad.marker++; // TODO: NEED TO TEST THIS  
            break;
        case 'q' :
            MotorController::instance()->setDutyCycle(28000, 1);
            MotorController::instance()->setDutyCycle(28000, 2);
            MotorController::instance()->setDutyCycle(28000, 3);
            MotorController::instance()->setDutyCycle(28000, 4);

            // Stop the logger
            Logger::instance()->stop();
            exit(0);
            break;
    }
}


void *UIThread(void *threadID)
{
    char temp;

    while (1)
    {
        temp = getch();
        pthread_mutex_lock(&mutex);
        cmd = temp;
        pthread_mutex_unlock(&mutex);
        usleep(1000);
    }
}
