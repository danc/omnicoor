/* Serial.h
  Functions used for easier access to the serial interface.
  Author: Seth Beinhart
  Beinhart@iastate.edu
*/
#ifndef _SERIAL_CONTROLLER_H
#define _SERIAL_CONTROLLER_H
#include <stdio.h>
typedef  unsigned char BYTE;

int initport(int fd) ;
int writeport(int fd, BYTE *chars, int bytesToWrite );
int readport(int fd, BYTE *result, int bytesToRead);
int getbaud(int fd);
#endif
