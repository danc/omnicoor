#include <math.h>
#include "Korebot/korebot_ai.h"
#include "Korebot/external_sensors_common.h"
#include "ai_xbox_control.h"

#define TANGENT_TABLE_SIZE 180
#define MAX_CONTROLLER_VALUE 32768
#define ROTATION_SETTINGS 100
#define SPEED_SETTINGS 100	// 200 is about full speed, it's unsafe for Eris
#define PI 3.1419
#define DEFAULT_PRIORITY 3
#define DEFAULT_LINEAR_SPEED 1
#define DEFAULT_LINEAR_ACCELERATION 1
#define DEFAULT_ROTATIONAL_SPEED 1
#define DEFAULT_ROTATIONAL_ACCELERATION 1

KorebotAIXBoxControl::KorebotAIXBoxControl() {
}

KorebotAIXBoxControl::~KorebotAIXBoxControl() {
}

/*
 * Get a uniqueue ID for this AI to differentiate it from the rest
 * of the AI
 *
 * @returns A uniqueue ID for this AI
 */
int KorebotAIXBoxControl::getUniqueID() {
	return 22;
}

/*
 * Gets a human-readable name for what this AI does.
 *
 * @returns A string that contains the name of the AI
 */
const char *KorebotAIXBoxControl::getName() {
	return "XBox Control";
}

/*
 * Initializes the Korebot AI.
 *
 * @param configuration An object that contains the configuration for the program
 * @param sensors A pointer to the sensor manager
 * @param localization_system The localization system with information about positions of objects / robots
 * @param motor_controller The motor controller
 * @parma korebot A class with generic information about this Korebot (Including network connections)
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotAIXBoxControl::init(Configuration *configuration, SensorManager *sensors, LocalizationSystem *localization_system, MotorController *motor_controller, ExternalSensors *external_sensors, Korebot *korebot) {
	KorebotAI::init(configuration, sensors, localization_system, motor_controller, external_sensors, korebot);

	return 1;
}

/*
 * Called before the AI is about to start.  Any initialization of the
 * motor controller, korebot, sensors, or localization should happen now
 */
void KorebotAIXBoxControl::onStart() {
}

/*
 * Called before the AI is about to stop executing.
 */
void KorebotAIXBoxControl::onStop() {
}

/*
 * Called each run of the main loop.  Motor speeds should be updated
 * in this method.
 *
 * @param time The difference in time since the last call to this method
 */
void KorebotAIXBoxControl::run(float time) {
	struct motor_control_speed_t speed;
	xbox_controller_t *xbox_history;
	double rotation;

	memset(&speed, 0, sizeof(struct motor_control_speed_t));

	xbox_history = ExternalSensors::instance()->getXboxControllerValues();

	// LEFT
	rotation = -1*((double)ROTATION_SETTINGS/(MAX_CONTROLLER_VALUE*2))*(xbox_history[SENSOR_HISTORY-1].left_trigger  + MAX_CONTROLLER_VALUE);
	// RIGHT
	rotation +=   ((double)ROTATION_SETTINGS/(MAX_CONTROLLER_VALUE*2))*(xbox_history[SENSOR_HISTORY-1].right_trigger + MAX_CONTROLLER_VALUE);

	double arcTangent = (int)((atan2(-xbox_history[SENSOR_HISTORY-1].left_analog_y,xbox_history[SENSOR_HISTORY-1].left_analog_x) * 180.0 / PI) + 270) % 360;
	double xSpeed = ((double)SPEED_SETTINGS / MAX_CONTROLLER_VALUE) * abs(xbox_history[SENSOR_HISTORY-1].left_analog_x);
	double ySpeed = ((double)SPEED_SETTINGS / MAX_CONTROLLER_VALUE) * abs(xbox_history[SENSOR_HISTORY-1].left_analog_y);
	double linearSpeed = mySpeed(xSpeed, ySpeed);

	speed.heading             = (short)arcTangent;
	speed.rotational_velocity = (short)rotation;
	speed.linear_speed        = (short)linearSpeed;

	MotorController::instance()->setMotors(&speed);
}

double KorebotAIXBoxControl::mySpeed(double xSpeed, double ySpeed) {
	/* Does not return even values since it is calculating on a square not a circle */
	double speed;

	speed = sqrt(xSpeed*xSpeed + ySpeed*ySpeed);
	if(speed < ((double)SPEED_SETTINGS * 0.08))
	{
		return 0;
	}
	else
	{
		return speed;
	}
}


/*
 * Called when a packet is received over the network that 
 * is destined for the currently-running AI
 *
 * @param packet The packet that was received.
 */
void KorebotAIXBoxControl::processPacket(BasePacket *packet) {
}


/*
 * Creates an instance of a KorebotAI class to control the Korebot
 */
KorebotAI *createAI() {
	return new KorebotAIXBoxControl();
}

/*
 * Destroys an instance of the KorebotAI class
 */
void destroyAI(KorebotAI *ai) {
	delete ai;
}
