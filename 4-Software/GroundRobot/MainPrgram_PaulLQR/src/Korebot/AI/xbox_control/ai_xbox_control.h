#ifndef _KOREBOT_AI_AI_XBOX_CONTROL_H
#define _KOREBOT_AI_AI_XBOX_CONTROL_H

#include "Korebot/korebot_ai.h"

class KorebotAIXBoxControl : public KorebotAI {
	public:
		KorebotAIXBoxControl();

		~KorebotAIXBoxControl();

		/*
		 * Get a uniqueue ID for this AI to differentiate it from the rest
		 * of the AI
		 *
		 * @returns A uniqueue ID for this AI
		 */
		int getUniqueID();

		/*
		 * Gets a human-readable name for what this AI does.
		 *
		 * @returns A string that contains the name of the AI
		 */
		const char *getName();

		/*
		 * Initializes the Korebot AI.
		 *
		 * @param configuration An object that contains the configuration for the program
		 * @param sensors A pointer to the sensor manager
		 * @param localization_system The localization system with information about positions of objects / robots
		 * @param motor_controller The motor controller
		 * @parma korebot A class with generic information about this Korebot (Including network connections)
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init(Configuration *configuration, SensorManager *sensors, LocalizationSystem *localization_system,
				MotorController *motor_controller, ExternalSensors *external_sensors, Korebot *korebot);

		/*
		 * Called before the AI is about to start.  Any initialization of the
		 * motor controller, korebot, sensors, or localization should happen now
		 */
		void onStart();

		/*
		 * Called before the AI is about to stop executing.
		 */
		void onStop();

		/*
		 * Called each run of the main loop.  Motor speeds should be updated
		 * in this method.
		 *
		 * @param time The difference in time since the last call to this method
		 */
		void run(float time);

		/*
		 * Called when a packet is received over the network that 
		 * is destined for the currently-running AI
		 *
		 * @param packet The packet that was received.
		 */
		void processPacket(BasePacket *packet);

	private:
		double mySpeed(double xSpeed, double ySpeed);

};

#endif
