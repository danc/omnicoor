#include <math.h>
#include "Korebot/korebot_ai.h"
#include "ai_circle.h"

KorebotAICircle::KorebotAICircle() {
}

KorebotAICircle::~KorebotAICircle() {
}

/*
 * Get a uniqueue ID for this AI to differentiate it from the rest
 * of the AI
 *
 * @returns A uniqueue ID for this AI
 */
int KorebotAICircle::getUniqueID() {
	return 1;
}

/*
 * Gets a human-readable name for what this AI does.
 *
 * @returns A string that contains the name of the AI
 */
const char *KorebotAICircle::getName() {
	return "Move In Circle";
}

/*
 * Initializes the Korebot AI.
 *
 * @param configuration An object that contains the configuration for the program
 * @param sensors A pointer to the sensor manager
 * @param localization_system The localization system with information about positions of objects / robots
 * @param motor_controller The motor controller
 * @parma korebot A class with generic information about this Korebot (Including network connections)
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotAICircle::init(Configuration *configuration, SensorManager *sensors, LocalizationSystem *localization_system, MotorController *motor_controller, ExternalSensors *external_sensors, Korebot *korebot) {
	KorebotAI::init(configuration, sensors, localization_system, motor_controller, external_sensors, korebot);

	return 1;
}

/*
 * Called before the AI is about to start.  Any initialization of the
 * motor controller, korebot, sensors, or localization should happen now
 */
void KorebotAICircle::onStart() {
	memset(&speed, 0, sizeof(struct motor_control_speed_t));
	memset(&speed2, 0, sizeof(struct motor_control_manual_t));
	//speed.linear_speed = 50;
	//speed.rotational_velocity = 10;
	
	speed.linear_speed = 10;
	angle = 0;
}

/*
 * Called before the AI is about to stop executing.
 */
void KorebotAICircle::onStop() {
}

/*
 * Called each run of the main loop.  Motor speeds should be updated
 * in this method.
 *
 * @param time The difference in time since the last call to this method
 */
void KorebotAICircle::run(float time) {
	speed2.motor_speed_front = (int)(45.0*sin(angle * 3.14159 / 180.0));//30
	speed2.motor_speed_rear = -speed2.motor_speed_front;

	speed2.motor_speed_left = (int)(45.0*cos(angle * 3.14159 / 180.0));
	speed2.motor_speed_right = -speed2.motor_speed_left;

	//printf("%d %d\n", speed2.motor_speed_front, speed2.motor_speed_right);

	MotorController::instance()->setMotors(&speed2);

	angle+=3;//2
}

/*
 * Called when a packet is received over the network that 
 * is destined for the currently-running AI
 *
 * @param packet The packet that was received.
 */
void KorebotAICircle::processPacket(BasePacket *packet) {
}


/*
 * Creates an instance of a KorebotAI class to control the Korebot
 */
KorebotAI *createAI() {
	return new KorebotAICircle();
}

/*
 * Destroys an instance of the KorebotAI class
 */
void destroyAI(KorebotAI *ai) {
	delete ai;
}
