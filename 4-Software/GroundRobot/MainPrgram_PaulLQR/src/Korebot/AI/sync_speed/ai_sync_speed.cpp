#include "Korebot/korebot_ai.h"
#include "ai_sync_speed.h"
#include "Network/packets/peer_motorspeed.h"
#include <math.h>
#include <strings.h>

const int KorebotAISquare::a[n*n] =
{
      21845
};

const int KorebotAISquare::b[n*r] =
{
     10922
};

const int KorebotAISquare::c[m*n] =
{
      10922
};

const int KorebotAISquare::d[m*r] =
{
     21845
};


KorebotAISquare::KorebotAISquare() {
}

KorebotAISquare::~KorebotAISquare() {
}

/*
 * Get a uniqueue ID for this AI to differentiate it from the rest
 * of the AI
 *
 * @returns A uniqueue ID for this AI
 */
int KorebotAISquare::getUniqueID() {
	return 63;
}

/*
 * Gets a human-readable name for what this AI does.
 *
 * @returns A string that contains the name of the AI
 */
const char *KorebotAISquare::getName() {
	return "Sync Speeds";
}

/*
 * Initializes the Korebot AI.
 *
 * @param configuration An object that contains the configuration for the program
 * @param sensors A targeter to the sensor manager
 * @param localization_system The localization system with information about positions of objects / robots
 * @param motor_controller The motor controller
 * @parma korebot A class with generic information about this Korebot (Including network connections)
 *
 * @returns 1 on success, 0 otherwise
 */
int KorebotAISquare::init(Configuration *configuration, SensorManager *sensors, LocalizationSystem *localization_system, MotorController *motor_controller, ExternalSensors *external_sensors, Korebot *korebot) {
	KorebotAI::init(configuration, sensors, localization_system, motor_controller, external_sensors, korebot);

	return 1;
}

/*
 * Called before the AI is about to start.  Any initialization of the
 * motor controller, korebot, sensors, or localization should happen now
 */
void KorebotAISquare::onStart() {

	robot_id = Korebot::instance()->getRobotID();

	my_motor_speed_packet = new PeerMotorSpeed(robot_id);

	memset(&vel_command, 0, sizeof(struct motor_control_manual_t));

	memset(&my_speed, 0, sizeof(struct motor_speeds_t));
	memset(&other_robots_speed, 0, sizeof(struct motor_speeds_t));
	
	x[0]=0;
	count=0;
	
	input_prev = 0;

	total_time = 0;
			
}

/*
 * Called before the AI is about to stop executing.
 */
void KorebotAISquare::onStop() {
}

/*
 * Called each run of the main loop.  Motor speeds should be updated
 * in this method.
 *
 * @param time The difference in time since the last call to this method
 */
void KorebotAISquare::run(float time) {

	/*
	 * TO SEND A PACKET:
	 *
	 * Korebot::instance()->getKorebotPeerConnection()->transmit(<Korebot ID>, <Packet *>);
	 *
	 * Create a packet by using, e.g.:
	 * SquareDance *square_dance = new SquareDance(<Number>);
	 *
	 * Thus, to send it over the network to Korebot 1:
	 * Korebot::instance()->getKorebotPeerConnection()->transmit(1, square_dance);
	 *
	 * When the packet is received on the destination korebot (AND IT IS NOT GUARANTEED THAT IT WILL), 
	 * the class will be sent as a parameter to the processPackets method defined below.  Any packets received from
	 * the network will be processed before run() is called during that iteration of the loop, so if you need anything from
	 * the packet, you must save it as a member variable in the class.
	 */

	//Get Encoder speed Values 
	right_vel_p = SensorManager::instance()->getSensorValues(SENSOR_ID_RIGHT_MOTOR_SPEED); 
	front_vel_p = SensorManager::instance()->getSensorValues(SENSOR_ID_FRONT_MOTOR_SPEED); 
	left_vel_p  = SensorManager::instance()->getSensorValues(SENSOR_ID_LEFT_MOTOR_SPEED); 
	rear_vel_p  = SensorManager::instance()->getSensorValues(SENSOR_ID_REAR_MOTOR_SPEED); 

	/*
	// store the current measurement 
	if ((right_vel_p[2]-right_vel_p[1])>10000 || (right_vel_p[2]-right_vel_p[1])<-10000){
		right_vel_p[2]=right_vel_p[1];
	}
	if ((front_vel_p[2]-front_vel_p[1])>10000 || (front_vel_p[2]-front_vel_p[1])<-10000){
		front_vel_p[2]=front_vel_p[1];
	}
	if ((left_vel_p[2]-left_vel_p[1])>10000 || (left_vel_p[2]-left_vel_p[1])<-10000){
		left_vel_p[2]=left_vel_p[1];
	}
	if ((rear_vel_p[2]-rear_vel_p[1])>10000 || (rear_vel_p[2]-rear_vel_p[1])<-10000){
		rear_vel_p[2]=rear_vel_p[1];
	}
	*/
	my_speed.right = right_vel_p[2];
	my_speed.front = front_vel_p[2];
	my_speed.left  = left_vel_p[2];
	my_speed.rear  = rear_vel_p[2];

	my_measured_speed 	    = (float) (my_speed.right + my_speed.front + my_speed.left + my_speed.rear)/4;
	other_robots_measured_speed = (float) (other_robots_speed.right + other_robots_speed.front + other_robots_speed.left + other_robots_speed.rear)/4;

	// change the robot's speed into radians/sec
	if (robot_id == 1){ // if Kryten

		my_measured_speed = my_measured_speed/(6125*8.25);
		other_robots_measured_speed = other_robots_measured_speed/(141*9.5);
		command_speed = 3*8.25;
		transmit_id = 0;
	}else{
		my_measured_speed = my_measured_speed/(141*9.5);
		other_robots_measured_speed = other_robots_measured_speed/(6125*8.25);
		command_speed = 3*9.5;
		transmit_id = 1;
	}		
		
	input=other_robots_measured_speed - my_measured_speed;
	
	if ( count == 50 ) {
		printf("\n\nSTARTING CONTROLLER\n\n");

		output_prev = 3;
	}

	/* CONTROLLER */
	if (count >= 50){
		output = (1.666) * input - (1.25) * input_prev + output_prev;
	}
	
	input_prev = input;
	output_prev = output;


	/*
	U[0]=(int) input*109223;

	if (count>20){
		Update(U);
	}
	*/

	

	if (robot_id == 1 && count > 50){ // start controlling Kryten to Dalec's Velocity

		command_speed = output * 9.5;

		if( command_speed > 80 ){

			command_speed = 80;

		}else if (command_speed < -80 ) {

			command_speed = -80;
		}
	}


	printf("input= %f \toutput= %f\tmy time= %f\tpacket time= %f\ttime btw= %f\n",input,output,total_time,packet_time, total_time-packet_time );

	my_motor_speed_packet->setmotorspeeds( robot_id , my_speed ,total_time);

	if (Korebot::instance()->getKorebotPeerConnection()->transmit(transmit_id,my_motor_speed_packet) != 1){
		printf("communication error\n");
	}
	
	vel_command.motor_speed_front = command_speed;
	vel_command.motor_speed_rear  = command_speed;	
	vel_command.motor_speed_left  = command_speed;
	vel_command.motor_speed_right = command_speed;

	MotorController::instance()->setMotors(&vel_command);	
	

	total_time += time*1000;	

	count++;

}

/*
 * Called when a packet is received over the network that 
 * is destined for the currently-running AI
 *
 * @param packet The packet that was received.
 */
void KorebotAISquare::processPacket(BasePacket *packet) {

	PeerMotorSpeed *motor_speed_packet = (PeerMotorSpeed*) packet; //typecast the base packet into a PeerMotorSpeed packet

	if (motor_speed_packet->getrobotid() != robot_id){

		packet_time = motor_speed_packet->getmotorspeeds(&other_robots_speed);
	}
	
}

void KorebotAISquare::Update(int u[r])
{
    int i, j, n_offset = 0, r_offset = 0;
    int x_next[n];
    int accum;
    
    // Evaluate x_next = A*x + B*u
    for (i=0; i<n; i++)
    {
        accum = 0;

        for (j=0; j<n; j++)
            accum += (int) a[n_offset+j]*x[j];
            
        for (j=0; j<r; j++)
            accum += (int) b[r_offset+j]*u[j];
        
        accum = (accum >> 15) * 360437;
        x_next[i] = (int) (accum >> 15);

        n_offset += n;
        r_offset += r;
    }
    
    n_offset = 0;
    r_offset = 0;
    
    // Evaluate y = C*x + D*u
    for (i=0; i<m; i++)
    {
        accum = 0;

        for (j=0; j<n; j++)
            accum += (int) c[n_offset+j]*x[j];
            
        for (j=0; j<r; j++)
            accum += (int) d[r_offset+j]*u[j];
            
        accum = (accum >> 15) * 7520073;
        y[i] = (int) (accum >> 15);

        n_offset += n;
        r_offset += r;
    }
    output= y[0]; //*(9.4248/32767);

    // Update x to its next value
    for (i=0; i<n; i++)
        x[i] = x_next[i];
}

/*
 * Creates an instance of a KorebotAI class to control the Korebot
 */
KorebotAI *createAI() {
	return new KorebotAISquare();
}

/*
 * Destroys an instance of the KorebotAI class
 */
void destroyAI(KorebotAI *ai) {
	delete ai;
}
