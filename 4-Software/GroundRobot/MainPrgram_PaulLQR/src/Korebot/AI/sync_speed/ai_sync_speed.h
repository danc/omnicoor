#ifndef _KOREBOT_AI_AI_SQUARE_H
#define _KOREBOT_AI_AI_SQUARE_H


#include "Korebot/korebot_ai.h"
#include "Network/packets/peer_motorspeed.h"


class KorebotAISquare : public KorebotAI, public PeerMotorSpeed {

	public:
		// n = #states, m = #outputs, r = #inputs
    		enum {n = 1, m = 1, r = 1};
		void Update(int u[r]);	

		KorebotAISquare();

		~KorebotAISquare();

		/*
		 * Get a uniqueue ID for this AI to differentiate it from the rest
		 * of the AI
		 *
		 * @returns A uniqueue ID for this AI
		 */
		int getUniqueID();

		/*
		 * Gets a human-readable name for what this AI does.
		 *
		 * @returns A string that contains the name of the AI
		 */
		const char *getName();

		/*
		 * Initializes the Korebot AI.
		 *
		 * @param configuration An object that contains the configuration for the program
		 * @param sensors A pointer to the sensor manager
		 * @param localization_system The localization system with information about positions of objects / robots
		 * @param motor_controller The motor controller
		 * @parma korebot A class with generic information about this Korebot (Including network connections)
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init(Configuration *configuration, SensorManager *sensors, LocalizationSystem *localization_system,
				MotorController *motor_controller, ExternalSensors *external_sensors, Korebot *korebot);

		/*
		 * Called before the AI is about to start.  Any initialization of the
		 * motor controller, korebot, sensors, or localization should happen now
		 */
		void onStart();

		/*
		 * Called before the AI is about to stop executing.
		 */
		void onStop();

		/*
		 * Called each run of the main loop.  Motor speeds should be updated
		 * in this method.
		 *
		 * @param time The difference in time since the last call to this method
		 */
		void run(float time);

		/*
		 * Called when a packet is received over the network that 
		 * is destined for the currently-running AI
		 *
		 * @param packet The packet that was received.
		 */
		void processPacket(BasePacket *packet);
	private:
		int robot_id;
		unsigned char transmit_id;
		int command_speed;
		float my_measured_speed;
		float other_robots_measured_speed;
		
		long* right_vel_p;
		long* left_vel_p;
		long* front_vel_p;
		long* rear_vel_p;


		PeerMotorSpeed *my_motor_speed_packet;
		

		struct motor_control_manual_t vel_command;
		struct motor_speeds_t my_speed;
		struct motor_speeds_t other_robots_speed;

		static const int a[n*n], b[n*r], c[m*n], d[m*r];
		int x[n], y[m];

		float output;
		float output_prev;
		float input;
		float input_prev;

		int U[r];
		int count;
		float total_time;
		float packet_time;

	


};

#endif

