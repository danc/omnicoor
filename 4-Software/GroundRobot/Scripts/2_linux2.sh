#!/bin/sh

# Configure the wifi
wconfig wlan0 essid KNET mode managed rate 11M
ifup wlan0

# Set the config file
rm config_file
echo "eris.cfg" > config_file

# Mount the computer that contains the up to date code
mount -t nfs 192.168.0.60:/home/shared /mnt/nfs

# This sleep statement is here because the FPGA likes to fail sometimes
# when the scripts run too quickly (still have no idea why this
# happens). When the sleep statement is removed, this failure is more
# prone to occurring.
sleep 2

# Program the Mesa 4i68 FPGA board
./sc4i68 S4168_1.BIT 0

# This sleep statement is here for the same reason as the one above
sleep 2

# cd into the directory where the most recent eris code is located
cd /mnt/nfs/MainProject

# Run the startx86 executable
./startx86

