#ifndef _NETWORK__NETWORK_H_
#define _NETWORK__NETWORK_H_

#define IP_ADDR_MAX_LEN	40

typedef struct {
	// 40 is the maximum length of an IPv6 human-readable IP address
	// (Including null-terminating character)
	char ip_addr[IP_ADDR_MAX_LEN];
	int  port;
} network_config_t;

#endif  // _NETWORK__NETWORK_H_
