#ifndef _NETWORK__TCP_SERVER_H_
#define _NETWORK__TCP_SERVER_H_

#include <pthread.h>
#include <sys/queue.h>

#include "network.h"
#include "network_connection_handler.h"

class BasePacket;
class TCP_Connection;

class TCP_Server {
	private:
		struct tcp_connection_entry {
			SLIST_ENTRY(tcp_connection_entry) connections;

			TCP_Connection *connection;
		};

		// The configuration of the network
		network_config_t config;

		// 1 if we are listening on the network, 0 otherwise
		int we_are_listening;

		// 1 if the connection handler thread is running, 0 otherwise
		int connection_handler_thread_running;

		// The file descriptor of the socket we create
		int sockfd;

		// 1 when the server should shut itself down.  0 otherwise.
		int shutdown_ready;

		// Store the TCP connection thread handler somewhere
		pthread_t connection_handler_thread;

		SLIST_HEAD(listhead, tcp_connection_entry) connections;

		// The number of connected clients
		int num_clients;

		NetworkConnectionHandler *connection_handler;

		// 1 if we should use a thread to handle incoming connections, 0 if
		// the creator will call a method to handle connections themselves
		int use_connection_handler_thread;

		// 1 when we are first launching the connection handler; Once
		// the handler thread has started, this becomes false
		int connection_handler_is_starting;
	public:
		/*
		 * Creates a new tcp_server structure and initializes the values
		 * in the structure to default values.
		 */
		TCP_Server();

		/*
		 * Creates a new tcp_server structure and initializes the values
		 * in the structure to default values.
		 *
		 * @param useConnectionHandlerThread 1 if we should use a thread to
		 * 			handle incoming connections, or 0 if the creator will
		 * 			call run() to handle connections themelves
		 * @param handler The object to handle new connection requests
		 */
		TCP_Server(int useConnectionHandlerThread, NetworkConnectionHandler *handler);

		/*
		 * Destroys the TCP server, and all connected clients.
		 */
		~TCP_Server();

		/*
		 * Starts the server.  The server attempts to listen on the given address and port
		 *					 specified in the config parameter.
		 *
		 * @param config The network configuration
		 *
		 * @returns 1 on success, <= 0 otherwise
		 */
		int start(const network_config_t &config);

		/*
		 * Attempts to stop an active server.  Closes any active client
		 * connections and stops listening on the port.
		 *
		 * @returns void
		 */
		void stop();

		/*
		 * Sends a packet to the given korebot
		 *
		 * @param robot_id The robot to send to, or KOREBOT_ID_BROADCAST for broadcast
		 * @param data     The data to send across the network
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int transmit(unsigned char robot_id, BasePacket *data);

		/*
		 * Broadcasts a packet to all Korebots
		 *
		 * @param data     The data to send across the network
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int broadcast(BasePacket *data);

		/*
		 * Checks for any incoming connections, and sends them to
		 * the connection handler.  This should not be called
		 * if use_connection_handler_thread is true.
		 *
		 * @returns 1 on success, 0 if there is a problem with the socket.
		 * 			If this function returns 0, the connection is no longer
		 * 			valid and the server should be stopped.
		 */
		int run();


		/////////////////////////////////////////////////////
		//                GETTERS / SETTERS                //
		/////////////////////////////////////////////////////

		/*
		 * Returns 1 if the server is active and listening for incoming
		 *			connections.  Returns 0 otherwise
		 *
		 * @returns (See description)
		 */
		int isListening();

		/*
		 * Returns the number of connected clients
		 *
		 * @returns The number of connected clients
		 */
		int getNumClients();

		/*
		 * Gets the currently active network configuration.
		 *
		 * @param config The network configuration will be copied into
		 *			this parameter
		 *
		 * @returns void
		 */
		void getConfiguration(network_config_t &config);

		/*
		 * Gets the object than handles new connections.
		 *
		 * @returns NULL, if no object has been specified, or
		 * 				the object otherwise
		 */
		NetworkConnectionHandler *getConnectionHandler();

		/*
		 * Sets the object that handles new connections.
		 *
		 * @param newConHandler The new object to handle new network connections.
		 *
		 * @returns void
		 */
		void setConnectionHandler(NetworkConnectionHandler *newConHandler);

		/*
		 * Gets if this server uses a separate thread for processing
		 * incoming connections, or if the creator calls a method to
		 * do the processing.
		 *
		 * @returns 1, if the server uses a thread, 0 otherwise
		 */
		int getUseConnectionHandlerThread();

		/*
		 * Sets if this server should use a connection handler thread or
		 * if some other part of the program will tell the server when
		 * to check for incoming connections.  If the server was already using a
		 * thread to process connections, and this method is called with a value
		 * of 0, the thread will be stopped before this method returns.
		 *
		 * If the thread had not been created, it will be created.
		 *
		 * @param useConnectionHandlerThread 1 if we should use a thread to
		 * 			handle incoming connections, or 0 if the creator will
		 * 			call run() to handle connections themelves
		 */
		void setUseConnectionHandlerThread(int useConnectionHandlerThread);

	private:
		/*
		 * Initializes the values for the class to default values.
		 *
		 * @param useConnectionHandlerThread 1 if we should use a thread to
		 * 			handle incoming connections, or 0 if the creator will
		 * 			call run() to handle connections themelves
		 * @param handler The object to handle new connection requests
		 */
		void init(int useConnectionHandlerThread, NetworkConnectionHandler *handler);

		/*
		 * Destroys the socket
		 */
		void destroySocket();

		/*
		 * Listens for incoming connections and creates new
		 * threads for the incoming connections.
		 */
		void listenThread();

		/*
		 * Launches the connection listener thread
		 */
		int launchListenThread();

	friend void *tcpListenThreadProxy(void *data);
};

#endif	// _NETWORK__TCP_SERVER_H_
