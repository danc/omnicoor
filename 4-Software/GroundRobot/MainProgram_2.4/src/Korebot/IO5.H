#ifndef _KOREBOT__IO5_H_
#define _KOREBOT__IO5_H_

//***** FOR USE WITH ERIS BASED SYSTEMS***/

/* ************************* PORTS ********************************************** */
#define ififoread                               512         /* (uint) immediate fifo */
#define ififoreadcount                          513         /* (uint) count of data */
#define ififoparam                              514         /* (uint) parameter */
#define ififoaxis                               515         /* (uint) axis */
#define ififowrite                              516         /* (uint)  */
#define ififowritecount                         517         /* (uint)  */
#define qfiforead                               520         /* (uint) queued fifo */
#define qfiforeadcount                          521         /* (uint)  */
#define qfifoparam                              522         /* (uint)  */
#define qfifoaxis                               523         /* (uint)  */
#define qfifowrite                              524         /* (uint)  */
#define qfifowritecount                         525         /* (uint)  */
#define cntcntlp                                531         /* (uint) primary counter control reg */
#define cntcntls                                533         /* (uint) secondary counter control reg */
#define prescale                                534         /* (uint) pwm rate = 50mhz/256*(65536/prescale), write only */
#define postscale                               535         /* (uint) sample rate = pwm rate/postscale, write only */
#define cntclrp                                 536         /* (uint) write to clear primary counter */
#define cntclrs                                 537         /* (uint) write to clear secondary counter */
#define clrclrdp                                538         /* (uint) clear count cleared bit */
#define clrclrds                                539         /* (uint) clear count cleared bit */
#define pwmgena                                 544         /* (uint) pwm generator per axis msbyte of word */
#define pwmgenb                                 545         /* (uint) pwm generator per axis msbyte of word */
#define pwmgenc                                 546         /* (uint) pwm generator per axis msbyte of word */
#define dira                                    547         /* (uint) per axis msbit of word */
#define dirb                                    548         /* (uint) per axis msbit of word */
#define dirc                                    549         /* (uint) per axis msbit of word */
#define ena                                     550         /* (uint) per axis pwm enable */
/* ports are 12 bits wide  */
#define porta                                   560         /* (uint)  */
#define portaddr                                561         /* (uint) bit is 1 for output */
#define portb                                   562         /* (uint)  */
#define portbddr                                563         /* (uint) bit is 1 for output */
#define portc                                   564         /* (uint)  */
#define portcddr                                565         /* (uint) bit is 1 for output */
#define portd                                   566         /* (uint)  */
#define portdddr                                567         /* (uint) bit is 1 for output */
#define porte                                   568         /* (uint)  */
#define porteddr                                569         /* (uint) bit is 1 for output */
#define portf                                   570         /* (uint)  */
#define portfddr                                571         /* (uint) bit is 1 for output */
#define irqsetupreg                             573         /* (uint) irq setup reg */
#define irqcause                                574         /* (uint) write identifier to set  */
/* multiplier hardware */
#define mula                                    576         /* (int) signed multiplier read/write */
#define mulb                                    577         /* (uint) unsigned multiplier read/write */
#define hprod                                   578         /* (long) 32bit signed product read only */
#define hprodh                                  580         /* (uint) 16bit unsigned product hi word read only */
#define hprodmid                                581         /* (uint) 16bit unsigned product mid word 8.8 */
#define hacc                                    582         /* (long) r/w, adds signed prod when write to hprod, */
/* 				unsigned prod when write to hprodh */
#define hmula                                   584         /* (long) 32*32=64 */
#define hmulb                                   586         /* (long)  */
#define hlprod                                  584         /* (double)  */
#define hmulcnt                                 591         /* (uint)  */
#define hdividend                               592         /* (double) 64/32=32 */
#define hdivisor                                596         /* (long)  */
#define hquotient                               592         /* (long)  */
#define hremainder                              594         /* (long)  */
#define hdivcnt                                 599         /* (uint)  */
#define hradicand                               600         /* (double) sqrt(64)=32 */
#define hroot                                   600         /* (long)  */
#define hsqrtcnt                                607         /* (uint)  */
#define lookupadd                               608         /* (uint) sine lookup table address r/w 0..511 */
#define lookupdata                              609         /* (uint) sine lookup table data r/w left justified */
#define lookupantidata                          610         /* (uint) sine lookup table data r/w antiphase left justified */
#define pfd0                                    612         /* (uint) 7i32 mode bit */
#define pfd1                                    613         /* (uint) 7i32 mode bit */
#define AltIndexbit                             614         /* (uint) read alternate index bit for 7I32 */
#define quadoutrate                             616         /* (uint) rate register */
#define quadoutcount                            617         /* (uint) input pulse count 8 bit */
#define quadoutmode                             618         /* (uint) bit0: output enable, bit1: freerun mode */
#define leds                                    624         /* (uint) byte  */
#define beep                                    625         /* (uint) msb */
/* hardware constants	 */
#define sysclk                                  628         /* (long) CPU clock frequency */
#define zero                                    632         /* (int) constant */
#define one                                     640         /* (int) constant */
#define false                                   632         /* (flag) constant */
#define minusone                                633         /* (int)  */
#define true                                    633         /* (flag)  */
#define naxis                                   634         /* (uint) number of axis's in hardware */
#define hwrevision                              635         /* (uint) hwtype in hi byte revision in low byte */
#define icdfifosize                             636         /* (uint) immediate command\data fifo size  */
#define qcdfifosize                             637         /* (uint) queued command\data fifo size  */
#define irbfifosize                             638         /* (uint) immediate readback fifo size */
#define qrbfifosize                             639         /* (uint) queued readback fifo size */

#endif  // _KOREBOT__IO5_H_