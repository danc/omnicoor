---------------------
* Table of Contents *
---------------------
  * Introduction
  * Installing Dependencies
  * Compiling
  * Troubleshooting
  * Authors

----------------
* Introduction *
----------------
This README describes how to compile the GroundRobot codebase. These instructions will work
for any GroundRobot release from 2.0 later.

By following these instructions, you should be able to compile the GroundRobot codebase,
but there may be issues that are not described within this document. If an issue occurs,
that you cannot solve, attempt to google the solution or ask someone with more linux
experience to assist you.

---------------------------
* Installing Dependencies *
---------------------------
1. Install the libconfig-1.3.1 and vrpn_07_31 dependencies.
    a) Follow the Compiling instructions within the omnicoor git: omnicoor/4-Software/Dependencies

2. Install autotools (automake and autoconf)
    a) sudo yum install automake
    b) sudo yum install autoconf

3. Install other dependencies (note: require the 32 bit version):
    a) sudo yum install librt.so.1
    b) sudo yum install libdl.so.2
    c) sudo yum install libstdc++
    d) sudo yum install libc.so.6
    e) sudo yum install libpthread.so.0
    f) sudo yum install ld-linux.so.2
    g) sudo yum install libconfig-devel

-------------------
* Compiling *
-------------------
1. cd into the desired directory

2. automake --add-missing
    - adds missing files required for autotools
    - this is necessary because our gitignore does not allow some of these files to be committed

3. autoreconf
    - remakes the build system, this is necessary to configure the system to compile correctly
      on your machine.

4. ./configure
      - Runs the configure script

5. make
    - compiles the code

-------------------
* Troubleshooting *
-------------------
configure: error: no acceptable C compiler found in $PATH
    You must install the development tools - quickest way is to use:
    * redhat base: yum groupinstall "Development tools"
    * Debian base: apt-get install build-essential

configure: error: C compiler cannot create executables
    Ensure that the 32 bit version of your development tools are installd (the compilers)


When there is an issue concerning a library that cannot be found(that you know was installed),
chances are that the 32-bit version of the library needs to be installed.
    When you use yum to install the dependency, it will list out the package install. If the install ends with a .i686, then the 32 bit version was installed. If it lists .x86_64, then the 64 bit version was installed. In order to install the 32 bit version, try calling yum install on the library it displayed, but replace the .x86_64 with .i686
    EXAMPLE:
        Input:  sudo yum install libstdc++.so.1
        Output: .... libstdc++-4.8.3-7.fc20.x86_64  .....
        It installed the x86_64 version, so we try again by doing the following:
        Input:  sudo yum install libstdc++-4.8.3-7.fc20.i686
        Success!!!

-----------
* Authors *
-----------
  Dylan Gransee - dgransee@gmail.com
  Robert Larsen
