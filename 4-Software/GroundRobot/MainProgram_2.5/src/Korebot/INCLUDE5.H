#ifndef _KOREBOT__INCLUDE5_H_
#define _KOREBOT__INCLUDE5_H_

/*** FOR USE WITH ERIS BASED SYSTEMS***/

/* The functional interface to the motor controller is done by a dual port ram */
/* sample:	the time between iterations of the pid loop, see prescale and postscale */
/* counts:	encoder counts ppr*4 */
/* UNITS */
/* despos		encoder counts                     BBBB.        B=byte S=signextend */
/* desposf	fractional encoder counts               BBB0    B=byte S=signextend */
/* velocity	8.24 counts per sample 		   SSSB.BBB     (127 max motor counts per sample) */
/* velocityf	is the fractional part of vel         0.000BB */
/* accel		8.24 counts per sample per sample     B.BBB */
/* accelf		is the fractional part of accel       0.000BB */
/* jerk		added to accel.accelf each sample     S.SBBBB */
/* avgvel		8.8 velocity units */
/* pwm		8.8 signed number, only integer part used for output */
/* TYPES (contents of ram address) */
/* 	<blank>	number of bits is hardware dependent	 */
/* 	byte	8 bit unsigned value */
/* 	ptr	10 bit address pointer */
/* 	flag	16 bit value 0 is false, nonzero is true */
/* 	mask	16 bit mask bit true enables */
/* 	uint	16 bit unsigned value 0..65535 */
/* 	int	16 bit signed value -32768..32767 */
/* 	ulong	32 bit unsigned value 0..4294967295 */
/* 	long	32 bit signed value -2147483648..2147483647 */
/* 	double	64 bit signed value -1.8E19..1.8E19 */
/* PARAMETER	VALUE	TYPE	FUNCTION */
#define nulld                                   0           /* (double) 32bit empty location */
#define null                                    0           /* (ptr) uninitialized pointers point to dummy */
#define dummy                                   0           /* (uint) default pointers point here */
#define dummyhi                                 1           /* (uint) default 32bit pointers point here  */
#define go                                      2           /* (flag) set true to start move */
#define pid                                     3           /* (flag) set true to enable pid  */
#define profile                                 4           /* (uint) 0=off, 1=trapezoidal profile, 2=host profile */
#define dist                                    5           /* (int) used by profile5 */
#define decel                                   5           /* (flag) true when decel */
#define dirinv                                  6           /* (flag) set true to invert motor drive polarity */
#define filterblock                             7           /* (ptr) pointer to filterblock */
#define profileblock							8	/*%ptr	pointer to profileblock COMMENTED BACK IN by me M */
#define home                                    9           /* (flag) becomes true when home has occured */
#define motion                                  10          /* (flag) becomes false when velocity = 0 */
#define slew                                    11          /* (flag) true when maximum velocity is reached */
#define error4                                  12          /* (uint) non-zero when excessive position, or drive error */
#define errormask                               13          /* (mask) mask for error, default enabled */
#define events                                  14          /* (uint) number of events, 24 max */
#define maxpwm                                  15          /* (uint) pwm limited to this value */
#define avgvel                                  16          /* (int) average velocity 8.8 */
#define exposerr                                17          /* (uint) excessive position error limit <32767 */
#define maxnegerr                               18          /* (int) max negative position error to PID */
#define maxposerr                               19          /* (int) max positive position error to PID */
#define driveerror                              20          /* (uint) variable to count */
#define posenc                                  21          /* (ptr) pointer to position encoder */
#define velenc                                  22          /* (ptr) pointer to velocity encoder */
#define actvel                                  23          /* (int) actual velocity */
#define bnderr                                  24          /* (int) bounded (@follow-@posenc) */
#define extvelocity                             25          /* (ptr) pointer to actvel, for external velocity */
#define follow                                  26          /* (ptr) pointer to desired position */
#define pwm                                     27          /* (int) pwm value */
/* multiphase */
#define phaseai                                 28          /* (uint) max current, 21845/amp hi current (65535/amp lo) */
#define phasebi                                 29          /* (uint) max current, 21845/amp hi current (65535/amp lo) */
#define offseti                                 30          /* (uint) offset current (to remove dead zone in driver) */
#define encodercnt                              31          /* (uint) counts per rev, use smallest integer for accuracy */
#define encfactor                               32          /* (uint) (sinetablesize * #poles * 256) / encodercnt */
#define slipk                                   33          /* (int) add to phaselead (for induction motors) */
#define phaseshift                              34          /* (flag) set true to invert one motor phase */
#define openloop                                35          /* (flag) when true use despos, pid must be on, default on */
/* 32bit user parameters */
#define homeposp                                36          /* (long) position loaded when primary index occurs */
#define homeposs                                38          /* (long) position loaded when seconday index occurs */
#define phasek                                  40          /* (long) phase konstant */
#define phasea                                  42          /* (long) phase accumulator, phasek is added to phasea each sample */
#define encp                                    44          /* (long) primary encoder, read only */
#define encs                                    46          /* (long) secondary encoder, read only */
#define desvel                                  48          /* (long) desired velocity,  */
/* 				value set by slewlimit sign set by direction of move */
/* filter param */
#define filterstart                             50          /* begining of filter block */
#define kk		50	/*%int	pwm offset, initial pwm value */
#define kff		51	/*%uint	friction in the direction of motion */
#define kp		52	/*%uint	kpd*error added to pwm */
#define kd		53 	/*%uint	kdd*actualvelocity added to pwm */
#define ka		54	/*%uint	unused accel error */
#define ki		55	/*%ulong	ki*error added to integralq each sample, high word added to pwm */
#define kih		56	/*	%uint	high word of ki not used */
#define kil		57 	/*%uint	high word limit of ki, 32767 max */
#define kf0		58	/*%uint	(unimplemented) spring */
#define kf1		59	/*%uint	velocity ff */
#define kf2		60	/*%uint	accel ff */
#define kf3		61	/*%uint	phasor velocity feedforward unused */
#define kdfil		62	/*%uint	amount of filtering */
#define driveplus	63	/*%uint	amount added to driveerror if maxpwm, generates error if overflow */
#define driveminus	64	/*%uint	amount subtracted from driveerror if not maxpwm, bounded at 0 */
#define nextfilblock	65	/*%ptr	pointer to next filter block */
/* profile parameters with breakpoint registers */
#define profilestart                            66          /* beginning of motion block */
#define desposd                                 66          /* (double) 32.32 profile position */
#define desposf                                 66          /* (long) .32 fractional part of profile position */
#define despos                                  68          /* (long) 32. profile position */
#define velocity                                70          /* (long) profile velocity		 */
#define accelf                                  72          /* (int) fractional part of accel for jerk */
#define accel                                   73          /* (long) acceleration, if 0 then no motion */
#define jerk                                    75          /* (long) added to accel each sample */
#define breakpoint                              77          /* (long) block breakpoint value */
#define nextproblock                            79          /* (ptr) pointer to next profile block */
#define nextpos                                 80          /* (long) where we want to go next */
#define slewlimit                               82          /* (long) slew speed limit, 2^23-1 max for kf1,kf2 */
/* multiphase motor parameters internal use only */
#define phaselead                               84          /* (int) lead angle */
#define motorangle                              85          /* (uint) sintablesize / angle between phases */
#define motorphase                              86          /* (uint) encp mod encodercnt */
#define homeoffset                              87          /* (int) offset from home position */
/* internal use only */
#define calcaccel                               88          /* (long) used for determining accel inflection point */
#define delta                                   90          /* (long) velq - velocityq */
#define fvelocity                               92          /* (uint) .16 velocity */
#define flastvelocity                           93          /* (uint) .16 old velocity */
#define flagxor                                 94          /* (uint) wait for flag xor invert */
#define flagand                                 95          /* (uint) wait for flag and mask  */
#define fractionaccel                           96          /* (long) accel / n  */
#define halfaccel                               96          /* (long) accel / n  */
#define fixup                                   98          /* (long) profile generator error */
#define ouraccel                                100         /* (long) start with half accel  */
#define stopathome                              102         /* (flag) if true clear desvel and go during home */
#define velocityf                               103         /* (int) fractional part of velocity for jerk */
/* 48bit number internal use only */
#define integral                                104         /* (long) integral error accumulator */
#define integralovf                             106         /* (int) overflow for integral */
/* 16bit internal use only */
#define reset                                   107         /* (flag) true when axis is reset */
#define olddist                                 108         /* (int)  */
#define acceln                                  108         /* (uint) n for accelfract	 */
#define lastpos                                 109         /* (uint) last actual position low half */
#define oldgo                                   110         /* (flag) internal */
#define oldpid                                  111         /* (flag) internal */
/* user ram */
#define firstevent                              112         /* (uint)  */
#define eventfirst                              112         /* (uint)  */
#define event1                                  112         /* (uint)  */
#define event2                                  118         /* (uint)  */
#define event3                                  124         /* (uint)  */
#define event4                                  130         /* (uint)  */
#define event5                                  136         /* (uint)  */
#define event6                                  142         /* (uint)  */
#define event7                                  148         /* (uint)  */
#define event8                                  154         /* (uint)  */
#define event9                                  160         /* (uint)  */
#define event10                                 166         /* (uint)  */
#define event11                                 172         /* (uint)  */
#define event12                                 178         /* (uint)  */
#define event13                                 184         /* (uint)  */
#define event14                                 190         /* (uint)  */
#define event15                                 196         /* (uint)  */
#define event16                                 202         /* (uint)  */
#define event17                                 208         /* (uint)  */
#define event18                                 214         /* (uint)  */
#define event19                                 220         /* (uint)  */
#define event20                                 226         /* (uint)  */
#define event21                                 232         /* (uint)  */
#define event22                                 238         /* (uint)  */
#define event23                                 244         /* (uint)  */
#define event24                                 250         /* (uint)  */
#define eventlast                               250         /* (uint)  */
#define lastevent                               250         /* (uint)  */
#define endofaxisram                            255         
/* global ram */
#define firstgevent                             256         /* (uint)  */
#define geventfirst                             256         /* (uint)  */
#define gevent1                                 256         /* (uint) host events */
#define gevent2                                 262         /* (uint)  */
#define gevent3                                 268         /* (uint)  */
#define gevent4                                 274         /* (uint)  */
#define gevent5                                 280         /* (uint)  */
#define gevent6                                 286         /* (uint)  */
#define gevent7                                 292         /* (uint)  */
#define gevent8                                 298         /* (uint)  */
#define gevent9                                 304         /* (uint)  */
#define gevent10                                310         /* (uint)  */
#define gevent11                                316         /* (uint)  */
#define gevent12                                322         /* (uint)  */
#define gevent13                                328         /* (uint)  */
#define gevent14                                334         /* (uint)  */
#define gevent15                                340         /* (uint)  */
#define gevent16                                346         /* (uint)  */
#define gevent17                                352         /* (uint)  */
#define gevent18                                358         /* (uint)  */
#define gevent19                                364         /* (uint)  */
#define gevent20                                370         /* (uint)  */
#define gevent21                                376         /* (uint)  */
#define gevent22                                382         /* (uint)  */
#define gevent23                                388         /* (uint)  */
#define gevent24                                394         /* (uint)  */
#define gevent25                                400         /* (uint)  */
#define gevent26                                406         /* (uint)  */
#define gevent27                                412         /* (uint)  */
#define gevent28                                418         /* (uint)  */
#define gevent29                                424         /* (uint)  */
#define gevent30                                430         /* (uint)  */
#define gevent31                                436         /* (uint)  */
#define gevent32                                442         /* (uint)  */
#define gevent33                                448         /* (uint)  */
#define gevent34                                454         /* (uint)  */
#define gevent35                                460         /* (uint)  */
#define gevent36                                466         /* (uint)  */
#define geventlast                              466         /* (uint)  */
#define lastgevent                              466         /* (uint)  */
#define endofglobalram                          471         
/* these locations are reserved, do not use */
#define greservedstart                          472         
#define greservedend                            495         
#define ledaxis                                 496         /* (ptr) axis for leds to monitor */
#define led                                     497         /* (ptr) parameter for leds to monitor, if not zero */
#define beeper                                  498         /* (ptr) beep uses what this points to, if not zero */
#define proctimer                               499         /* (uint) max ???read proctimer here */
#define timeout                                 500         /* (uint) incremented each time we haven't finished before the next sample */
#define gevents                                 501         /* (uint) number of global events 36 max */
#define axis                                    502         /* (ptr) the current axis */
#define sync                                    503         /* (flag) set before host, cleared after */
#define gphasek                                 504         /* (long) gphase konstant */
#define gphasea                                 506         /* (long) gphase accumulator, gphasek is added to gphasea each sample */
#define gphasef                                 508         /* (flag) set true on carry of gphasea */
#define cputype                                 509         /* (uint) CPU type */
#define controltype                             510         /* (uint) number of motor phases */
#define swrevision                              511         /* (uint) majorrev hibyte minorrev lobyte */

#endif  // _KOREBOT__INCLUDE5_H_
