
#include "logger.h"

#include <iomanip>
#include <iostream>
#include <string>
#include <sys/stat.h>

#include "utils.h"

using namespace std;

// TODO: this needs to move somewhere better
// This is used to clear the terminal
const string CLEAR = "\033[H\033[J";

/* TODO: list
         Clean up the initialize function
         Add getter/setter functions for booleans
*/


// Default value for singleton pointer
Logger *Logger::_instance = 0;

Logger::Logger()
{
    isInitialized = false;
    logPitch = true;
    logRoll = true;
	logEris = true;
    logLocal = true;
    logMotors = false;
    logVelCtrl = true;
    quad = MotorController::instance()->quad;
	eris = MotorController::instance()->eris;
    motors =&MotorController::instance()->motors;
}

Logger::~Logger()
{

}

bool Logger::initialize()
{
    time_t curTime;             // Time
    struct tm *ts;              // Time struct
    char dirName[50];          // Directory name
    char fName[50];            // File name
    stringstream logFilename;   // Log file path

    // Get the current time
    curTime = time(0);

    // Put the time into a tm struct
    ts = localtime(&curTime);

    // Format the dirName to look like: "2011-4-15_DAY" based on current date
    strftime(dirName, sizeof(dirName), "%Y-%m-%d_%a/", ts);

    // Format the fName to look like: "12.34.10" based on current time
    strftime(fName, sizeof(fName), "log(%H.%M.%S).txt", ts);

    // Create the default log file directory
    //logFilename << "./Logs/";
    logFilename << "/root/Logs/";
    mkdir(logFilename.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    // Create the log file directory for this day
    logFilename << dirName;
    mkdir(logFilename.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    // Create and open the log file for this run
    logFilename << fName;
    logFile.open(logFilename.str().c_str(), fstream::out);

    // Print the header information for this log file
    writeHeader();

    // Set the width of the data being printed out
    logFile.width(1);

    // return value primarily used for debugging purposes
    if (logFile.is_open())
    {
        isInitialized = true;
        return true;
    }

    cerr << "Failure to create and open logfile" << endl;
    return false;
}

void Logger::writeHeader()
{
    /*
     * The first thing to be printed in the log file is relevant information regarding the
     * PID constants being used for this run.
     */
    logFile << "#Constants\t\tpitch_P\t\tpitch_I\t\tpitch_D" << endl;
    logFile << "#PIDValues\t\t" << quad->pitch.p
            << "\t\t" << quad->pitch.i
            << "\t\t" << quad->pitch.d << endl;
    logFile << "#Constants\t\troll_P\t\troll_I\t\troll_D" << endl;
    logFile << "#PIDValues\t\t" << quad->roll.p
            << "\t\t" << quad->roll.i
            << "\t\t" << quad->roll.d << endl;

    /*
     * Log file column titles.
     * Always log the time, marker, and motor thrust of all 4 motors
     * Based on user settings, log the pitch and roll values
     */
    logFile << "%Time\t\tMarker\t\t";
    logFile << "Motor_1\tMotor_2\tMotor_3\tMotor_4\t";

    if (logPitch)
    {
        logFile << "Pitch\t\tPitch_pid_p\t\tPitch_pid_i\t\tPitch_pid_d\t\tPitch_desired\t\t";

        // TODO: don't do now
        // logFile << "Pitch_p\t\tPitch_d\t\tPitch_i\t\t";
        // logFile << "Pitch_setPoint\t\t";
    }
    if (logRoll)
    {
        logFile << "Roll\t\t Roll_pid_p\t\tRoll_pid_i\t\tRoll_pid_d\t\tRoll_desired";

        // TODO: don't do now
        // logFile << "Roll_p\t\tRoll_d\t\tRoll_i\t\t";
        // logFile << "Roll_setPoint\t\t";
    }
	if (logEris)
	{
		//Eris time, eris x, eris y, eris z, eris roll, eris pitch, eris yaw, dersired x velosity
		logFile <<"\t\tEris_time\t\tEris_x\t\tEris_y\t\tEris_z\t\tEris_roll\t\tEris_pitch\t\tEris_yaw\t\tx_vel_set";
	}
    if (logLocal)
    {
        logFile <<"\t\txb_vel\t\tleft_pos\t\tright_pos";
    }
    if(logMotors)
    {
        logFile << "\t\tLeft_P\t\tLeft_I\t\tLeft_D\t\tLeft_PWM\t\tLeft_desired_vel\t\tRight_P\t\tRight_I\t\tRight_D\t\tRight_PWM\t\tRight_desired_vel\t\txb_vel_filt";
    }
    {
        logFile << "\t\tXv_P\t\tXv_i\t\tXv_d\t\txb_vel_desired\t\tLeft_PWM\t\tRight_PWM\t\txb_vel_filt";
    }
    logFile << endl;

    /*
     * Log file colum units.
     * Always log the time, marker, and motor thrust of all 4 motors
     * Based on user settings, log the pitch and roll values
     */
    // Time
    logFile << "&sec\t\tMarker\t\t";
    // Motor thrust (1-4)
    logFile << "%thrust\t%thrust\t%thrust\t%thrust\t";

    if (logPitch)
    {
        // pitch, pitch error, pitch_pid_p, pitch_pid_d, pitch_pid_d roll_desired
        logFile << "degrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\t";

        // TODO: don't do now
        // pitch p constant, pitch d constant
        //logFile << "degrees\t\tdegrees\t\tdegrees\t\t";

        // pitch set point
        //logFile << "degrees\t\t";
    }
    if (logRoll)
    {
        // roll, roll error, roll_pid_p, roll_pid_d, roll_pid_i roll_desired
        logFile << "degrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees";

        // TODO: don't do now
        // roll p constant, roll d constant, roll i constant
        // logFile << "degrees\t\tdegrees\t\tdegrees\t\t";
        // roll set point
        // logFile << "degrees\t\t";
    }
	if (logEris)
	{
		//Eris time, eris x, eris y, eris z, eris roll, eris pitch, eris yaw x velocity setpoint
		logFile << "\t\tsec\t\tm\t\tm\t\tm\t\tdegrees\t\tdegrees\t\tdegrees\t\tcm/s";
	}
    if(logLocal)
    {
        logFile << "\t\tm/s\t\ttic\t\ttic";
    }
    if(logMotors)
    {
        logFile << "\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tm/s\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tm/s\t\tm/s";
    }
    if(logVelCtrl)
    {
        logFile << "\t\tPWM\t\tPWM\t\tPWM\t\tm/s\t\tPWM\t\tPWM\t\tm/s";
    }
    quad->marker = 0;    // initializing the value of the marker to 0
    logFile << endl;

}

void Logger::stop()
{
    logFile.close();
}

void Logger::logData()
{
    /*
     * Log file data.
     * Always log the time and motor thrust of all 4 motors
     * Based on user settings, log the pitch and roll values
     */

    //Formatting settings (fixed point output for decimal values)
    logFile << fixed;

    // Time
    logFile << quad->vrpnNow     << "\t";
    logFile << quad->marker      << "\t";

    // Motor thrust for motors 1-4
    logFile << quad->motor_1_pwm << "\t"
            << quad->motor_2_pwm << "\t"
            << quad->motor_3_pwm << "\t"
            << quad->motor_4_pwm << "\t";

    // Log the pitch data values
    if (logPitch)
    {
        logFile << RAD_TO_DEG(quad->pitch.current)   << "\t\t"
                << quad->pitch.PID_pcomp             << "\t\t"
                << quad->pitch.PID_icomp             << "\t\t"
                << quad->pitch.PID_dcomp             << "\t\t"
                << RAD_TO_DEG(quad->pitch.desired)   << "\t\t";

        // TODO: don't do now
        //logFile << quad->pitch.p << "\t"
        //        << quad->pitch.d << "\t"
        //        << quad->pitch.i << "\t";
        //logFile << quad->pitch.desired << "\t";
    }

    // Log the roll data values
    if (logRoll)
    {
        logFile << RAD_TO_DEG(quad->roll.current)    << "\t\t"
                << quad->roll.PID_pcomp              << "\t\t"
                << quad->roll.PID_icomp              << "\t\t"
                << quad->roll.PID_dcomp              << "\t\t"
                << RAD_TO_DEG(quad->roll.desired);

        // TODO: don't do now
        //logFile << quad->roll.p << "\t"
        //        << quad->roll.d << "\t"
        //        << quad->roll.i << "\t";
        //logFile << quad->roll.desired << "\t";
    }
	//logs the data collected about Eris's time postion and orientation
	if (logEris)
	{
		logFile << "\t\t" << eris->vrpnNow              << "\t\t"
						  << eris->x                    << "\t\t"
				          << eris->y                    << "\t\t"
				          << eris->z                    << "\t\t"
				          << RAD_TO_DEG(eris->roll)     << "\t\t"
				          << RAD_TO_DEG(eris->pitch)    << "\t\t"
				          << RAD_TO_DEG(eris->yaw)      << "\t\t"
                          << eris->x_vel_set;
	}
    if (logLocal)
    {
        logFile << "\t\t" << eris->xb_vel       << "\t\t"
                          << (int)(eris->motor_pos_left)       << "\t\t"
                          << (int)(eris->motor_pos_right );
    }
    if(logMotors){
        logFile << "\t\t" << motors->left.openFPGA.PID_pcomp   << "\t\t"
                          << motors->left.openFPGA.PID_icomp   << "\t\t"
                          << motors->left.openFPGA.PID_dcomp   << "\t\t"
                          << motors->left.openFPGA.correction  << "\t\t"
                          << motors->left.openFPGA.desired     << "\t\t"
                          << motors->right.openFPGA.PID_pcomp  << "\t\t"
                          << motors->right.openFPGA.PID_icomp  << "\t\t"
                          << motors->right.openFPGA.PID_dcomp  << "\t\t"
                          << motors->right.openFPGA.correction << "\t\t"
                          << motors->right.openFPGA.desired    << "\t\t"
                          << eris->xb_vel_filt; 

    }
    if(logVelCtrl)
    {
        logFile << "\t\t" << motors->x_vel_ctrl.PID_pcomp   << "\t\t"
                          << motors->x_vel_ctrl.PID_icomp   << "\t\t"
                          << motors->x_vel_ctrl.PID_dcomp   << "\t\t"
                          << motors->x_vel_ctrl.desired     << "\t\t"
                          << -motors->x_vel_ctrl.correction << "\t\t"
                          << motors->x_vel_ctrl.correction  << "\t\t"
                          << eris->xb_vel_filt; 

    }
    logFile << endl;
}
