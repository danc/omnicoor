#ifndef _KOREBOT__CON5_H_
#define _KOREBOT__CON5_H_

///***** FOR USE WITH ERIS BASED SYSTEMS****/

/* Copyright (C) 2007 MESA Electronics.  All rights reserved. */
/* bits5.inc */
/* for rev 30 or greater hardware */
#define majorrev                                4           /* (byte) updated when parameters change, hi byte */
#define minorrev                                106         /* (byte) lo byte */
#define truef                                   65535       /* (flag) value for flag types */
#define falsef                                  0           /* (flag) value for flag types */
/* profile modes */
#define noprofile                               0           /* (uint) value for profile_uint */
#define trapezoidalmode                         1           /* (uint) value for profile_uint */
#define externalprofile                       

#endif  // _KOREBOT__CON5_H_
