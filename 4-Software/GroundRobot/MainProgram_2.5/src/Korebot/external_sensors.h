#ifndef _KOREBOT__EXTERNAL_SENSORS_H_
#define _KOREBOT__EXTERNAL_SENSORS_H_

#include <stddef.h>
#include "defines.h"
#include "external_sensors_common.h"

class BasePacket;

class ExternalSensors {
	private:
		// This is a singleton object, so here's the instance to the object
		static ExternalSensors *s_instance;

		xbox_controller_t *xbox_data;

	public:
		/*
		 * Returns a single instance of the ExternalSensors.
		 *
		 * @returns A single instance of the ExternalSensors
		 */
		static ExternalSensors *instance() {
			if (s_instance == NULL) {
				s_instance = new ExternalSensors();
			}
			return s_instance;
		}

		/*
		 * Frees any resources used by the sensor manager.
		 */
		~ExternalSensors();

		/*
		 * Initializes the ExternalSensors.  Each available sensor defined
		 * in the config file will be acquired, so that the data can be
		 * read later on.
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shuts the sensor manager down, destroying any resources & releasing
		 * any devices the manager is holding.
		 */
		void shutdown();

		/*
		 * Runs one iteration of the sensor manager; Reads
		 * the sensor manager data from the sensors.
		 * 
		 * @param dt The time difference since the last call to this method
		 */
		void run(float dt);

		/*
		 * Processes a packet received over the network
		 *
		 * @param packet The packet received from the network
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Sends status information about the sensor manager to the control server.
		 *
		 * This includes sensors available on this korebot.
		 */
		void sendConfig();

		/*
		 * Prints the status of various modules on the Korebot
		 */
		void printStatus();

		/*
		 * Gets the history of XBox controller values
		 *
		 * @returns An array of sensor values, with index 0 the farthest in the past and index
		 * 			SENSOR_HISTORY - 1 the latest value
		 */
		xbox_controller_t *getXboxControllerValues();

	private:
		/*
		 * Creates the ExternalSensors object.
		 * Nothing is requested of the sensor board at this ponint.
		 */
		ExternalSensors();

	friend class KorebotAI;
};

#endif	// _KOREBOT__EXTERNAL_SENSORS_H_
