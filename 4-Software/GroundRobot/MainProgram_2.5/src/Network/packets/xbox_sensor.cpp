#include "xbox_sensor.h"

#include <cstring>

#include "Network/network_bitstream.h"

/*
 * Creates a sensor data packet
 */
XboxSensor::XboxSensor() : BasePacket(OPCODE_XBOX_SENSOR) {
	memset(&data, 0, sizeof(xbox_controller_t));
}

/*
 * Creates a invalid sensor control by default
 */
XboxSensor::XboxSensor(const xbox_controller_t &state) : BasePacket(OPCODE_XBOX_SENSOR) {
	memcpy(&data, &state, sizeof(xbox_controller_t));
}

/*
 * Creates a copy of a XboxSensor packet
 *
 * @param copy The packet to copy
 */
XboxSensor::XboxSensor(XboxSensor *copy) : BasePacket(OPCODE_XBOX_SENSOR) {
	memcpy(&data, &copy->data, sizeof(xbox_controller_t));
}

/*
 * Frees any resources used by this packet
 */
XboxSensor::~XboxSensor() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *XboxSensor::read(NetworkBitstream &in) {
	in>>data.left_analog_x;
	in>>data.left_analog_y;

	in>>data.right_analog_x;
	in>>data.right_analog_y;

	in>>data.left_trigger;
	in>>data.right_trigger;

	in>>data.buttons;

	return new XboxSensor(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void XboxSensor::write(NetworkBitstream &out) const {
	BasePacket::write(out);

	out<<data.left_analog_x;
	out<<data.left_analog_y;

	out<<data.right_analog_x;
	out<<data.right_analog_y;

	out<<data.left_trigger;
	out<<data.right_trigger;

	out<<data.buttons;
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the state of the controller
 *
 * @returns A pointer to a structure containing the current state of the controller
 */
const xbox_controller_t *XboxSensor::getData() {
	return &data;
}
