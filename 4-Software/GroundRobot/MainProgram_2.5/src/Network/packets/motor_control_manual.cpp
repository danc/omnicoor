#include "motor_control_manual.h"

#include <cstring>

#include "Network/network_bitstream.h"

/*
 * Creates a manual motor control by default
 */
MotorControlManual::MotorControlManual() : MotorControl(MOTOR_CONTROL_MODE_MANUAL) {
	memset(&parameters, 0, sizeof(motor_control_manual_t));
}

/*
 * Creates a manual motor control by default
 */
MotorControlManual::MotorControlManual(const struct motor_control_manual_t &parameters) : MotorControl(MOTOR_CONTROL_MODE_MANUAL) {
	memcpy(&this->parameters, &parameters, sizeof(motor_control_manual_t));
}

/*
 * Creates a copy of a MotorControlManual packet
 *
 * @param copy The packet to copy
 */
MotorControlManual::MotorControlManual(MotorControlManual *copy) : MotorControl(MOTOR_CONTROL_MODE_MANUAL) {
	memcpy(&parameters, &copy->parameters, sizeof(motor_control_manual_t));
}

/*
 * Frees any resources used by this packet
 */
MotorControlManual::~MotorControlManual() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 * 			the information read from the bitstream.
 * 			The base packet should be freed by the
 * 			calling program.
 */
BasePacket *MotorControlManual::read(NetworkBitstream &in) {
	in>>parameters.motor_speed_front;
	in>>parameters.motor_speed_right;
	in>>parameters.motor_speed_left;
	in>>parameters.motor_speed_rear;
	return new MotorControlManual(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void MotorControlManual::write(NetworkBitstream &out) const {
	MotorControl::write(out);
	out<<parameters.motor_speed_front;
	out<<parameters.motor_speed_right;
	out<<parameters.motor_speed_left;
	out<<parameters.motor_speed_rear;
}

/*
 * Gets the speeds of the motors
 */
const motor_control_manual_t *MotorControlManual::getMotorSpeeds() const {
	return &parameters;
}
