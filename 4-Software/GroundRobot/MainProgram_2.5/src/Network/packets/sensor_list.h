#ifndef _NETWORK__PACKETS__SENSOR_LIST_H_
#define _NETWORK__PACKETS__SENSOR_LIST_H_

#include "sensor_control.h"
#include "Korebot/sensor_manager_common.h"

// 12 should be enough sensors for now
#define MAX_SENSORS_IN_PACKET	16

class SensorList : public SensorControl {
	private:
		unsigned char num_sensors;

		struct sensor_info_t sensors[MAX_SENSORS_IN_PACKET];
	public:
		/*
		 * Creates a sensor list packet with no sensors
		 */
		SensorList();

		/*
		 * Creates a sensor list with the given data
		 *
		 * @param num_sensors The number of sensors in the packet
		 * @param sensors Information about the sensors
		 */
		SensorList(unsigned char num_sensors, sensor_info_t *sensors);

		/*
		 * Creates a copy of a SensorList packet
		 *
		 * @param copy The packet to copy
		 */
		SensorList(SensorList *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~SensorList();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the number of available sensors.
		 *
		 * @returns The number of sensors on the korebot
		 */
		unsigned char getNumSensors();

		/*
		 * Gets the sensor information for the given sensor
		 * 
		 * @parm index The index of the sensor
		 * @param out Where to store the data
		 *
		 * @retuns 1 on success, 0 otherwise
		 */
		bool getSensorData(unsigned int index, struct sensor_info_t *out);
};

#endif	// _NETWORK__PACKETS__SENSOR_LIST_H_
