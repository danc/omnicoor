#include <iomanip>
#include <iostream>
#include <string>

#include <cstdlib>
#include <pthread.h>
#include <quat.h>
#include <signal.h>

#include "configuration.h"
#include "korebot.h"
#include "logger.h"
#include "motor_controller.h"
#include "utils.h"
#include "vrpn.h"
#include "key_bindings.h"
#include "sensor_manager.h"
#include "sensor_manager_common.h"
using namespace std;

/********** defines for pwm values *************/
#define DUTY_CYCLE_MIN          26000
#define DUTY_CYCLE_MAX          56000
#define DUTY_CYCLE_MID          ((DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 2) + DUTY_CYCLE_MIN
#define DUTY_CYCLE_HALF_RANGE   (DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 2
/***********************************************/

/********** function prototypes ****************/
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_eris(void*, const vrpn_TRACKERCB t);
void *mainThread(void *threadID);
void PID_controller(vrpn_data_t *vrpnData);
void tuning_UI(vrpn_data_t *vrpnData);
void robotControl_UI(vrpn_data_t *vrpnData);
void robotControlMode();
void tuningMode();
void boxStepMode(double curTime);
bool loadConfigFile();
void *UIThread(void *threadID);
static void ctrlc_handler(int sig);
/***********************************************/

/********** Enum declarations ******************/
enum Axis {ROLL, PITCH};                    // For the axis angle (pitch or roll)
enum Mode {STOPPED, TUNING, BOX_STEP, ROBOT_CONTROL};// For the current mode we are executing in
/***********************************************/

/********** General variables ******************/
extern int app_shutdown;                // When 1, the ground robot is supposed to shut down
pthread_mutex_t mutex;                  // Mutex for the threads
static char cmd;                        // Used in the UI thread for taking input
const string CLEAR = "\033[H\033[J";    // When sent to cout, this clears the terminal screen
static double startTime = 0;            // For tracking the start time of the program
static Mode currentMode = STOPPED;      // For keeping track of the current running mode
/***********************************************/

/********** TUNING mode ************************/
Axis cur_axis = PITCH;   // The current axis that is being modified (for tuning mode)
/***********************************************/

/********** BOX_STEP mode **********************/
static double nextChange = 0;           // Time to change the value
static double changeRate = 10;          // Time between changes (seconds)
static int state = 0;                   // Starting state
static float boxSize =  0.0872664626;   // Size in radians to move per step (5 degrees)
/***********************************************/

/********** ROBOT_CONTROL mode *****************/
struct motor_control_manual_t groundRobotMotors;    // Struct for the ground robots motor speeds
static int MAX_SPEED = 40;//14080;                          // Maximum linear speed
static int MAX_ROT_SPEED = 8;                       // Maximum rotational speed
/***********************************************/

int main(int argc, char** argv)
{
    pthread_t threads[2];
    void *status;

    // Load the config file
    if (loadConfigFile())
        cout << "Config file loaded" << endl;

    // Initialize motor controller, data logger, and vrpn
    if (1 == MotorController::instance()->init())
        cout << "\nMotors are ready" << endl;
    if (Logger::instance()->initialize())
        cout << "Log file is ready" << endl;
    vrpn_init("192.168.0.120:3883", handle_pos, handle_eris);
    cout << "VRPN is ready" << endl; // This might not always be true, but we assume it is.
                                     // Might want to add some verification and a return value to
                                     // The vrpn_init function in vrpn.h?
    MotorController::instance()->eris->counter1=0;
    // Get the program start time
    struct timeval tm;
    gettimeofday(&tm, 0);
    startTime = (tm.tv_usec / 1000000.0) + tm.tv_sec;

    // Initialize mutexes and join threads
    pthread_mutex_init(&mutex, NULL);
    pthread_create(&threads[0], NULL, UIThread, (void*)1);
    pthread_create(&threads[1], NULL, mainThread, (void*)2);
    pthread_join(threads[1], &status);

    return 0;
}

void* mainThread(void* threadID)
{
    currentMode = STOPPED;

    cout << "\n\nChoose a running mode (modes begin after 3 second delay):\n";
    cout << "\tPress v to start in tuning mode\n";
    cout << "\tPress b to start in robot control mode (driving mode)\n";
    cout << "\tPress n to start in box step mode (changes the setpoint every few seconds)\n";
    cout << "\tPress esc to quit\n";
    while (1)
    {
        cout << "\r";
        switch (cmd)
        {
            case 'v':
                currentMode = TUNING;
                break;

            case 'b':
                currentMode = ROBOT_CONTROL;
                break;

            case 'n':
                currentMode = BOX_STEP;
                break;

            case 27:    // esc key
                // Stop the program
                Logger::instance()->stop();
                MotorController::instance()->stop();
                currentMode = STOPPED;
                exit(0);
                break;
        }

        if (currentMode != STOPPED)
        {
            sleep(3);   // Wait 3 seconds before beginning

            // Set the cmd character back to null
            pthread_mutex_lock(&mutex);
            cmd = 0;
            pthread_mutex_unlock(&mutex);

            // Arm the quad motors
            MotorController::instance()->armQuadMotors();

            // Begin accepting data from vrpn
            vrpn_go();
        }
    }
}

/**
 * vrpn callback function that is called everytime vrpn data of Eris is
 * received. The callback gets the data and calls the pid controller
 * by passing that data in.
 *
 * @param d     no clue what this is
 * @param t     the vrpn data that was received
 */
void VRPN_CALLBACK handle_eris(void*, const vrpn_TRACKERCB t){
	// Convert the angles from quat to euler angles
    q_vec_type euler;
    q_to_euler(euler, t.quat);
	
	// TODO: this should be set un the initialize function, and we should be setting this to
    //       the time in here?
    MotorController::instance()->eris->vrpnNow = 0;

    // Set the vrpn data struct
    vrpn_data_t vrpnData;
    vrpnData.usec = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
    vrpnData.x = t.pos[0];
    vrpnData.y = t.pos[1];
    vrpnData.z = t.pos[2];
    vrpnData.yaw = euler[0];
    vrpnData.pitch = euler[1];
    vrpnData.roll = euler[2];
	
	//Does something with vrpnNow
    MotorController::instance()->eris->vrpnPrev = MotorController::instance()->eris->vrpnNow;
    if (MotorController::instance()->eris->vrpnNow == 0)
    {
        MotorController::instance()->eris->vrpnPrev = 0;
        MotorController::instance()->eris->vrpnTime0 = vrpnData.usec;
        MotorController::instance()->eris->vrpnNow = vrpnData.usec;
    }
    else
    {
		//from what I can tell vrpnNow is always set to zero so the code never reaches this statement
        MotorController::instance()->eris->vrpnNow = vrpnData.usec - MotorController::instance()->eris->vrpnTime0;
    }
	
	//writes the vrpn time in usec to the Eris VRPN data struct
	MotorController::instance()->eris->usec=vrpnData.usec;
	
	//Writes the position and orientation of Eris to a struct to be logged latter
	MotorController::instance()->eris->x=vrpnData.x;
	MotorController::instance()->eris->y=vrpnData.y;
	MotorController::instance()->eris->z=vrpnData.z;
	MotorController::instance()->eris->roll=vrpnData.roll;
	MotorController::instance()->eris->pitch=vrpnData.pitch;
	MotorController::instance()->eris->yaw=vrpnData.yaw;
    /*if (MotorController::instance()->eris->counter1<1){
        MotorController::instance()->eris->counter1=MotorController::instance()->eris->counter1+1;
    }else{
        SensorManager::instance()->readMotorEncoders();
        MotorController::instance()->eris->counter1=0;
    }*/
    SensorManager::instance()->readMotorEncoders();
    usleep(1000);
    Logger::instance()->logData();

}
/**
 * vrpn callback function that is called everytime vrpn data of the pendulum is 
 * received. The callback gets the data and calls the pid controller
 * by passing that data in.
 *
 * @param d     no clue what this is
 * @param t     the vrpn data that was received
 */
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t)
{
    // Convert the angles from quat to euler angles
    q_vec_type euler;
    q_to_euler(euler, t.quat);

    // TODO: this should be set un the initialize function, and we should be setting this to
    //       the time in here?
    MotorController::instance()->quad->vrpnNow = 0;

    // Set the vrpn data struct
    vrpn_data_t vrpnData;
    vrpnData.usec = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
    vrpnData.x = t.pos[0];
    vrpnData.y = t.pos[1];
    vrpnData.z = t.pos[2];
    vrpnData.yaw = euler[0];
    vrpnData.pitch = euler[1];
    vrpnData.roll = euler[2];

    MotorController::instance()->quad->vrpnPrev = MotorController::instance()->quad->vrpnNow;
    if (MotorController::instance()->quad->vrpnNow == 0)
    {
        MotorController::instance()->quad->vrpnPrev = 0;
        MotorController::instance()->quad->vrpnTime0 = vrpnData.usec;
        MotorController::instance()->quad->vrpnNow = vrpnData.usec;
    }
    else
    {
        MotorController::instance()->quad->vrpnNow = vrpnData.usec - MotorController::instance()->quad->vrpnTime0;
    }

    // Based on the current mode, display the correct UI and check for user input
    switch (currentMode)
    {
        case BOX_STEP:
            boxStepMode(vrpnData.usec);
        // Drop through here to use the TUNING ui/controls 
        case TUNING:
            // prevent robot from moving
            MotorController::instance()->stopGroundRobot();
            tuning_UI(&vrpnData);
            tuningMode();
            break;
        case ROBOT_CONTROL:
            robotControl_UI(&vrpnData);
            robotControlMode();
            break;
        case STOPPED:
            // Stop the motors and logger
            MotorController::instance()->stop();
            Logger::instance()->stop();
            exit(0);
            break;
    }

    // Run the pid controller with the newly recieved vrpn data
    PID_controller(&vrpnData);

    // Log the new data
    //Logger::instance()->logData();
}

void PID_controller(vrpn_data_t *vrpnData)
{
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;

    // PID control
    MotorController::instance()->quad->pitch.current = vrpnData->pitch;
    MotorController::instance()->quad->roll.current = vrpnData->roll;
    MotorController::instance()->quad->yaw.current = vrpnData->yaw;
    //MotorController::instance()->pidControl(&MotorController::instance()->quad->pitch);
    //MotorController::instance()->pidControl(&MotorController::instance()->quad->roll);
    //MotorController::instance()->pidControl(&MotorController::instance()->quad->yaw);
	MotorController::instance()->pidControlFilt(&MotorController::instance()->quad->pitch);
    MotorController::instance()->pidControlFilt(&MotorController::instance()->quad->roll);
	//converts the PID controller output to the right magnitude 
	PWM_value_correction_pitch=30.0*MotorController::instance()->quad->pitch.correction;
	PWM_value_correction_roll=30.0*MotorController::instance()->quad->roll.correction;
    
	//calculates the desired motor outputs based on the PWM correction value
	MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;
    
    
    /*MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;
    */
	
	//makes sure that the PWM commands are within the valid range for the ESCs
	if (MotorController::instance()->quad->motor_1_pwm<DUTY_CYCLE_MIN){
		MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MID;
	} else if (MotorController::instance()->quad->motor_1_pwm>DUTY_CYCLE_MAX){
		MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MAX;
	}
	if (MotorController::instance()->quad->motor_2_pwm<DUTY_CYCLE_MIN){
		MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MID;
	} else if (MotorController::instance()->quad->motor_2_pwm>DUTY_CYCLE_MAX){
		MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MAX;
	}	
	if (MotorController::instance()->quad->motor_3_pwm<DUTY_CYCLE_MIN){
		MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MID;
	} else if (MotorController::instance()->quad->motor_3_pwm>DUTY_CYCLE_MAX){
		MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MAX;
	}
	if (MotorController::instance()->quad->motor_4_pwm<DUTY_CYCLE_MIN){
		MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MID;
	} else if (MotorController::instance()->quad->motor_4_pwm>DUTY_CYCLE_MAX){
		MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MAX;
	}	
	
	//sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);
}

void tuning_UI(vrpn_data_t *vrpnData)
{
    // Clear the UI
    cout << CLEAR;

    // Display the current mode
    cout << "Current mode: \t" << ((currentMode == TUNING) ? "Tuning mode" : "Box step mode");
    cout << endl << endl;

    cout << "Change running mode:" << endl;
    cout << KeyBindings::TUNING_MODE    << ": Change to tuning mode" << endl;
    cout << KeyBindings::ROBOT_CTRL_MODE<< ": Change to robot control mode" << endl;
    cout << KeyBindings::BOX_STEP_MODE  << ": Change to box step mode" << endl;
    cout << endl;

    // Print out the tuning commands
    cout << "Tuning commands:" << endl;
    cout << KeyBindings::TUNE_PITCH     << ": Tune pitch axis       | ";
    cout << KeyBindings::TUNE_ROLL      << ": Tune roll axis" << endl;
    cout << endl;

    cout << KeyBindings::TUNE_P_UP      << ": Increase P            | ";
    cout << KeyBindings::TUNE_P_DN      << ": Decrease P" << endl;
    cout << KeyBindings::TUNE_I_UP      << ": Increase I            | ";
    cout << KeyBindings::TUNE_I_DN      << ": Decrease I" << endl;
    cout << KeyBindings::TUNE_D_UP      << ": Increase D            | ";
    cout << KeyBindings::TUNE_D_DN      << ": Decrease D" << endl;
    cout << KeyBindings::TUNE_SP_UP     << ": Increase setpoint     | ";
    cout << KeyBindings::TUNE_SP_DN     << ": Decrease setpoint" << endl;
    cout << KeyBindings::TUNE_TAW_UP    << ": Increase Taw          | ";
    cout << KeyBindings::TUNE_TAW_DN    << ": Decrease Taw"  << endl;
    cout << KeyBindings::RESET_I_COMP   << ": Reset cumulative I" << endl;
    cout << endl;

    cout << KeyBindings::ADD_LOG_MARKER << ": Add marker to logfile" << endl;
    cout << endl;

    cout << "ESC"                       << ": QUIT system"  << endl;
    cout << endl << endl;

    // Print current mode
    if (cur_axis == PITCH)
    {
        cout << "EDITING PITCH" << endl << endl;
    }
    else if (cur_axis == ROLL)
    {
        cout << "EDITING ROLL" << endl << endl;
    }

    // Display current data
    cout << "Current data:" << endl;
    cout << setprecision(3) << fixed << setw(6);
    cout << "         | Pitch:       | Roll:" << endl;
    cout << "Current  | " << vrpnData->pitch << "\t| "
                          << vrpnData->roll << endl;
    cout << "P        | " << MotorController::instance()->quad->pitch.p << "\t| "
                          << MotorController::instance()->quad->roll.p << endl;
    cout << "I        | " << MotorController::instance()->quad->pitch.i << "\t| "
                          << MotorController::instance()->quad->roll.i << endl;
    cout << "D        | " << MotorController::instance()->quad->pitch.d << "\t| "
                          << MotorController::instance()->quad->roll.d << endl;
    cout << "Setpoint | " << MotorController::instance()->quad->pitch.desired << "\t| "
                          << MotorController::instance()->quad->roll.desired << endl;
    cout << "Taw      | " << MotorController::instance()->quad->pitch.Taw << "\t| "
                          << MotorController::instance()->quad->roll.Taw << endl;
}

void robotControl_UI(vrpn_data_t *vrpnData)
{
    // Clear the UI
    cout << CLEAR;

    cout << "Current mode: \tRobot control" << endl;
    cout << endl;

    cout << "Change running mode:" << endl;
    cout << KeyBindings::TUNING_MODE    << ": Change to tuning mode" << endl;
    cout << KeyBindings::ROBOT_CTRL_MODE<< ": Change to robot control mode" << endl;
    cout << KeyBindings::BOX_STEP_MODE  << ": Change to box step mode" << endl;
    cout << endl;

    // Print out the keybindings
    cout << "Movement:" << endl;
    cout << KeyBindings::FORWARD        << ": forward               | ";
    cout << KeyBindings::FORWARD_RIGHT  << ": forward right" << endl;
    cout << KeyBindings::LEFT           << ": left                  | ";
    cout << KeyBindings::FORWARD_LEFT   << ": forward left" << endl;
    cout << KeyBindings::REVERSE        << ": reverse               | ";
    cout << KeyBindings::REVERSE_RIGHT  << ": reverse right" << endl;
    cout << KeyBindings::RIGHT          << ": right                 | ";
    cout << KeyBindings::REVERSE_LEFT   << ": reverse left" << endl;
    cout << KeyBindings::ROTATE_CW      << ": rotate clockwise      | ";
    cout << KeyBindings::ROTATE_CCW     << ": reverse counter-clockwise" << endl;
    cout << endl;

    cout << KeyBindings::STOP_ROBOT     << ": STOP ground robot motors" << endl;
    cout << "ESC"                       << ": QUIT system" << endl;
    cout << endl;

    cout << "Setpoint:" << endl;
    cout << KeyBindings::PITCH_SP_UP    << ": Pitch setpoint up     | ";
    cout << KeyBindings::PITCH_SP_DN    << ": Pitch setpoint down" << endl;
    cout << KeyBindings::ROLL_SP_UP     << ": Roll setpoint up      | ";
    cout << KeyBindings::ROLL_SP_DN     << ": Roll setpoint down" << endl;
    cout << endl;

    cout << KeyBindings::RESET_I_COMP   << ": Reset cumulative I" << endl;
    cout << endl;

    cout << KeyBindings::ADD_LOG_MARKER << ": Add marker to logfile" << endl;
    cout << endl;

    // Display current data
    cout << "Current data:" << endl;
    cout << setprecision(3) << fixed << setw(6);
    cout << "         | Pitch:       | Roll:" << endl;
    cout << "Current  | " << vrpnData->pitch << "\t| "
                          << vrpnData->roll << endl;
    cout << "P        | " << MotorController::instance()->quad->pitch.p << "\t| "
                          << MotorController::instance()->quad->roll.p << endl;
    cout << "I        | " << MotorController::instance()->quad->pitch.i << "\t| "
                          << MotorController::instance()->quad->roll.i << endl;
    cout << "D        | " << MotorController::instance()->quad->pitch.d << "\t| "
                          << MotorController::instance()->quad->roll.d << endl;
    cout << "Setpoint | " << MotorController::instance()->quad->pitch.desired << "\t| "
                          << MotorController::instance()->quad->roll.desired << endl;
    cout << "Taw      | " << MotorController::instance()->quad->pitch.Taw << "\t| "
                          << MotorController::instance()->quad->roll.Taw << endl;
}

void tuningMode()
{
    switch (cmd)
    {
        /***** Tune P *****/
        case KeyBindings::TUNE_P_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.p += 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.p += 1;
            break;
        case KeyBindings::TUNE_P_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.p -= 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.p -= 1;
            break;

        /***** Tune I *****/
        case KeyBindings::TUNE_I_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.i += 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.i += 1;
            break;
        case KeyBindings::TUNE_I_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.i -= 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.i -= 1;
            break;

        /***** Tune D *****/
        case KeyBindings::TUNE_D_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.d += 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.d += 1;
            break;
        case KeyBindings::TUNE_D_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.d -= 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.d -= 1;
            break;

        /***** Tune setpoint *****/
        case KeyBindings::TUNE_SP_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.desired += 0.01;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.desired += 0.01;
            break;
        case KeyBindings::TUNE_SP_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.desired -= 0.01;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.desired -= 0.01;
            break;

        /***** Tune taw *****/
        case KeyBindings::TUNE_TAW_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.Taw += 0.01;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.Taw += 0.01;
            break;
        case KeyBindings::TUNE_TAW_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.Taw -= 0.01;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.Taw -= 0.01;
            break;

        /***** Switch tuning axis *****/
        case KeyBindings::TUNE_PITCH:
            cur_axis = PITCH;
            break;
        case KeyBindings::TUNE_ROLL:
            cur_axis = ROLL;
            break;

        /***** Reset cumulative I *****/
        case KeyBindings::RESET_I_COMP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.PID_icomp = 0;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.PID_icomp = 0;
            break;

        /***** Change mode *****/
        case KeyBindings::ROBOT_CTRL_MODE:
            currentMode = ROBOT_CONTROL;
            break;
        case KeyBindings::TUNING_MODE:
            currentMode = TUNING;
            break;
        case KeyBindings::BOX_STEP_MODE:
            currentMode = BOX_STEP;
            break;

        /***** Logfile *****/
        case KeyBindings::ADD_LOG_MARKER:
            MotorController::instance()->quad->marker++;
            break;

        /***** Quit *****/
        case KeyBindings::QUIT:
            // Stop the motors and logger
            MotorController::instance()->stop();
            Logger::instance()->stop();
            exit(0);
            break;
    }

    // Set the cmd back to 0
    pthread_mutex_lock(&mutex);
    cmd = 0;
    pthread_mutex_unlock(&mutex);
}

void robotControlMode()
{

    switch (cmd)
    {
        /***** Reset cumulative I *****/
        case KeyBindings::RESET_I_COMP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.PID_icomp = 0;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.PID_icomp = 0;
            break;

        /***** Pendulum setpoint *****/
        case KeyBindings::PITCH_SP_UP:
            MotorController::instance()->quad->pitch.desired += 0.01;
            break;
        case KeyBindings::PITCH_SP_DN:
            MotorController::instance()->quad->pitch.desired -= 0.01;
            break;
        case KeyBindings::ROLL_SP_UP:
            MotorController::instance()->quad->roll.desired += 0.01;
            break;
        case KeyBindings::ROLL_SP_DN:
            MotorController::instance()->quad->roll.desired -= 0.01;
            break;

        /***** Logfile *****/
        case KeyBindings::ADD_LOG_MARKER:
            MotorController::instance()->quad->marker++;
            break;

        /***** Change mode *****/
        case KeyBindings::TUNING_MODE:
            currentMode = TUNING;
            break;
        case KeyBindings::BOX_STEP_MODE:
            currentMode = BOX_STEP;
            break;

        /***** Ground Robot *****/
        case KeyBindings::STOP_ROBOT:
            //MotorController::instance()->stopGroundRobot();
            MotorController::instance()->eris->x_vel_set=0;
            groundRobotMotors.motor_speed_left = 0;
            groundRobotMotors.motor_speed_right = 0;
            groundRobotMotors.motor_speed_front = 0;
            groundRobotMotors.motor_speed_rear = 0;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            break;
        case KeyBindings::FORWARD:
            groundRobotMotors.motor_speed_left = -MAX_SPEED;
            groundRobotMotors.motor_speed_right = MAX_SPEED;
            groundRobotMotors.motor_speed_front = 0;
            groundRobotMotors.motor_speed_rear = 0;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=MAX_SPEED;
            break;
        case KeyBindings::RIGHT:   // Move right
            groundRobotMotors.motor_speed_left = 0;
            groundRobotMotors.motor_speed_right = 0;
            groundRobotMotors.motor_speed_front = -MAX_SPEED;
            groundRobotMotors.motor_speed_rear = MAX_SPEED;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=0;
            break;
        case KeyBindings::REVERSE:   // Move backwards
            groundRobotMotors.motor_speed_left = MAX_SPEED;
            groundRobotMotors.motor_speed_right = -MAX_SPEED;
            groundRobotMotors.motor_speed_front = 0;
            groundRobotMotors.motor_speed_rear = 0;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=-MAX_SPEED;
            break;
        case KeyBindings::LEFT:
            groundRobotMotors.motor_speed_left = 0;
            groundRobotMotors.motor_speed_right = 0;
            groundRobotMotors.motor_speed_front = MAX_SPEED;
            groundRobotMotors.motor_speed_rear = -MAX_SPEED;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=0;
            break;
        case KeyBindings::FORWARD_RIGHT:
            groundRobotMotors.motor_speed_left = -MAX_SPEED;
            groundRobotMotors.motor_speed_right = MAX_SPEED;
            groundRobotMotors.motor_speed_front = -MAX_SPEED;
            groundRobotMotors.motor_speed_rear = MAX_SPEED;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=MAX_SPEED;
            break;
        case KeyBindings::FORWARD_LEFT:
            groundRobotMotors.motor_speed_left = -MAX_SPEED;
            groundRobotMotors.motor_speed_right = MAX_SPEED;
            groundRobotMotors.motor_speed_front = MAX_SPEED;
            groundRobotMotors.motor_speed_rear = -MAX_SPEED;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=MAX_SPEED;
            break;
        case KeyBindings::REVERSE_RIGHT:
            groundRobotMotors.motor_speed_left = MAX_SPEED;
            groundRobotMotors.motor_speed_right = -MAX_SPEED;
            groundRobotMotors.motor_speed_front = -MAX_SPEED;
            groundRobotMotors.motor_speed_rear = MAX_SPEED;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=-MAX_SPEED;
            break;
        case KeyBindings::REVERSE_LEFT:
            groundRobotMotors.motor_speed_left = MAX_SPEED;
            groundRobotMotors.motor_speed_right = -MAX_SPEED;
            groundRobotMotors.motor_speed_front = MAX_SPEED;
            groundRobotMotors.motor_speed_rear = -MAX_SPEED;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=-MAX_SPEED;
            break;
        case KeyBindings::ROTATE_CW:
            groundRobotMotors.motor_speed_left = -MAX_ROT_SPEED;
            groundRobotMotors.motor_speed_right = -MAX_ROT_SPEED;
            groundRobotMotors.motor_speed_front = -MAX_ROT_SPEED;
            groundRobotMotors.motor_speed_rear = -MAX_ROT_SPEED;
            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=0;
            break;
        case KeyBindings::ROTATE_CCW:
            groundRobotMotors.motor_speed_left = MAX_ROT_SPEED;
            groundRobotMotors.motor_speed_right = MAX_ROT_SPEED;
            groundRobotMotors.motor_speed_front = MAX_ROT_SPEED;
            groundRobotMotors.motor_speed_rear = MAX_ROT_SPEED;

            MotorController::instance()->setMotors(&groundRobotMotors);
            MotorController::instance()->setMotorSpeeds();
            MotorController::instance()->eris->x_vel_set=0;
            break;
        case KeyBindings::QUIT:
            // Stop the motors and logger
            MotorController::instance()->stop();
            Logger::instance()->stop();
            MotorController::instance()->eris->x_vel_set=0;
            exit(0);
            break;
    };

    // Set the cmd back to 0
    pthread_mutex_lock(&mutex);
    cmd = 0;
    pthread_mutex_unlock(&mutex);
}

/**
 * This function performs a demo by moving in a box formation (first moves to a corner from center,
 * then steps around in a square). It takes 9 steps to be centered again.
 *
 * This function uses the following global variables"
 *  nextChange  - Time in seconds of the next state change
 *  state       - The current state - the position in the box
 *  boxSize     - Angle size in radians of the box
 *  changeRate  - Time in seconds between each state change
 *
 * @param curTime  is the current vrpn time
 */
void boxStepMode(double curTime)
{
    // Initialize if it is the first time here
    if (nextChange == 0)
    {
        nextChange = curTime;
    }
    else if (nextChange <= curTime)
    {
        nextChange = curTime += changeRate;
        switch (state)
        {
            //1D roll
            /*case 0:
                state = 1;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 1:
                state = 2;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
            case 2:
                state = 3;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 3:
                state = 0;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = boxSize;
                break;
                */
            //1D pitch
            case 0:
                state = 1;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 1:
                state = 2;
                MotorController::instance()->quad->roll.desired = 0.0;
                MotorController::instance()->quad->pitch.desired  = -boxSize;
                break;
            case 2:
                state = 3;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 3:
                state = 0;
                MotorController::instance()->quad->roll.desired = 0.0;
                MotorController::instance()->quad->pitch.desired  = boxSize;
                break;
            //2D box
            /*case 0:
                state = 1;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 1:
                state = 2;
                MotorController::instance()->quad->pitch.desired = -boxSize;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
            case 2:
                state = 3;
                MotorController::instance()->quad->pitch.desired = -boxSize;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 3:
                state = 4;
                MotorController::instance()->quad->pitch.desired = -boxSize;
                MotorController::instance()->quad->roll.desired  = boxSize;
                break;
            case 4:
                state = 5;
                MotorController::instance()->quad->pitch.desired = 0;
                MotorController::instance()->quad->roll.desired  = boxSize;
                break;
            case 5:
                state = 6;
                MotorController::instance()->quad->pitch.desired = boxSize;
                MotorController::instance()->quad->roll.desired  = boxSize;
                break;
            case 6:
                state = 7;
                MotorController::instance()->quad->pitch.desired = boxSize;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 7:
                state = 8;
                MotorController::instance()->quad->pitch.desired = boxSize;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
            case 8:
                state = 9;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
            case 9:
                state = 0;
                MotorController::instance()->quad->pitch.desired = -boxSize;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
                */
                
        }
    }
}

/**
 * Lods the configuration file in the Configuration class
 *
 * @return bool value that for success or failure
 */
bool loadConfigFile()
{
    char config_filename[64];

    // Handle Ctrl+c so we we can shutdown gracefully
    signal(SIGINT, &ctrlc_handler);

    // Attempt to load the config file
    FILE *fp = fopen("/root/config_file", "r");
    if (fp != NULL)
    {
        fgets(config_filename, 64, fp);
        fclose(fp);
        while (strlen(config_filename) > 0 &&
                (config_filename[strlen(config_filename) - 1] == ' ' ||
                 config_filename[strlen(config_filename) - 1] == '\t' ||
                 config_filename[strlen(config_filename) - 1] == '\n' ||
                 config_filename[strlen(config_filename) - 1] == '\r'))
        {
            config_filename[strlen(config_filename) - 1] = 0;
        }
    }

    printf("Loading config \"%s\"...\n", config_filename);
    if (!Configuration::instance()->load(config_filename))
    {
        fprintf(stderr, "Fatal Error: Could not load Korebot config file.\n");
        return false;
    }

    return true;
}

/**
 * This thread just waits for user input (in the form of a single keypress). Once input is received,
 * a global variable for the key is set so that the main thread can access the key that was pressed.
 *
 * @param threadID is the thread ID
 * @return NULL since it shouldn't end in our case
 */
void *UIThread(void *threadID)
{
    char temp;

    // Loop forever while getting input
    while (1)
    {
        temp = getch();
        pthread_mutex_lock(&mutex);
        cmd = temp;
        pthread_mutex_unlock(&mutex);
        usleep(1000);
    }
    return NULL;
}

/**
 * This is a ctrl+c handler. It sets app_shutdown to 1, which should shut down the program.
 * I am not sure of the specifics because this was written before the May15-27 team
 *
 * @param sig the signal that it receives when the key is pressed
 */
static void ctrlc_handler(int sig)
{
    app_shutdown = 1;
}
