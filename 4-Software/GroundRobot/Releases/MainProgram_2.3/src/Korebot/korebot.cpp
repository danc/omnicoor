#include <string.h>
#include <sys/select.h>
#include "korebot.h"
#include "configuration.h"
#include "profiler.h"
#include "motor_controller.h"
#include "sensor_manager.h"
#include "localization_system.h"
#include "external_sensors.h"
#include "ai.h"

#include "Network/network.h"
#include "Network/tcp_connection.h"
#include "Network/udp_connection.h"
#include "Network/packet_handler.h"
#include "Network/packets/robot_identification.h"
#include "Network/packets/profiler_packet.h"
#include "Network/packets/configuration_packet.h"

Korebot *Korebot::s_instance = NULL;

int app_shutdown = 0;

/*
 * Initializes the Korebot
 */
Korebot::Korebot()
{
	control_server_connection = new TCP_Connection(0, this);
	korebot_peer_connection = new UDP_Connection(0, this);

	profiler_packet_handler = new ProfilerPacket();
	profiler_packet_handler->setConnection(control_server_connection);

	ProfileSample::output_handler = profiler_packet_handler;

	memset(&control_server_config, 0, sizeof(network_config_t));

	menu_mode = MENU_MAIN_MENU;
}

/*
 * Frees up any resources used by this korebot
 */
Korebot::~Korebot()
{
	ProfileSample::output_handler = NULL;

	if (profiler_packet_handler != NULL)
	{
		ProfileSample::output_handler = NULL;
		delete profiler_packet_handler;
	}

	AI::instance()->shutdown();
	SensorManager::instance()->shutdown();
	MotorController::instance()->shutdown();
	LocalizationSystem::instance()->shutdown();
	ExternalSensors::instance()->shutdown();

	if (control_server_connection != NULL)
	{
		control_server_connection->stop();
		delete control_server_connection;
	}

	if (korebot_peer_connection != NULL)
	{
		korebot_peer_connection->stop();
		delete korebot_peer_connection;
	}
}

/*
 * Initializes the Korebot; Attempts to connect to
 * servers, sends korebot identification packets, etc.
 *
 * @returns 1 on success, 0 otherwise
 */
int Korebot::init()
{
	control_server_config.port = Configuration::instance()->getValue("network.control_server.port", (int)54321);
	Configuration::instance()->getValue("network.control_server.ip_address", control_server_config.ip_addr, IP_ADDR_MAX_LEN, "127.0.0.1");

	korebot_peer_config.port = Configuration::instance()->getValue("network.peer.port", (int)55555);
	Configuration::instance()->getValue("network.peer.ip_address", korebot_peer_config.ip_addr, IP_ADDR_MAX_LEN, "127.0.0.1");

	printf("my config ip addr = %s\n\n" , korebot_peer_config.ip_addr);

	korebot_broadcast_config.port = Configuration::instance()->getValue("network.broadcast.port", (int)55555);
	Configuration::instance()->getValue("network.broadcast.ip_address", korebot_broadcast_config.ip_addr, IP_ADDR_MAX_LEN, "127.0.0.1");

	robot_id = Configuration::instance()->getValue("korebot.robot_id", (int)0);
	Configuration::instance()->getValue("korebot.robot_name", robot_name, MAX_ROBOT_NAME_LEN, "UNKNOWN");

	control_server_connection->setRobotID(robot_id);
	korebot_peer_connection->setRobotID(robot_id);

	korebot_peer_connection->getConfiguration(korebot_peer_config);

	if (!korebot_peer_connection->isListening())
	{
		if (korebot_peer_connection->start(korebot_broadcast_config) == 1)
		{
			// Let other Korebots on the network know about us
			//printf("start worked");
			korebot_peer_connection->sendRobotDiscovery();
		}
	}

	SensorManager::instance()->init();
	ExternalSensors::instance()->init();
	LocalizationSystem::instance()->init();
	MotorController::instance()->init();
	AI::instance()->init();

	// Attempt to connect to the control server
	if (connectToControlServer() != 1)
	{
		fprintf(stderr, "Failed to connect to Korebot control server...Reverting to console menu\n");
		mainMenu();
	}

	return 1;
}

/*
 * Shuts the korebot down.  Stops the motors, releases any
 * control it has over devices, frees any memory it is using,
 * and destroys the Korebot object.
 */
void Korebot::shutdown()
{
	if (s_instance != NULL)
	{
		delete s_instance;
		s_instance = NULL;
	}
}

/*
 * Starts the main Korebot loop
 */
void Korebot::start()
{
	float current_time;
	float last_time;
	float profiler_delay;

	int temp;

	current_time = ProfileSample::getTime();
	profiler_delay = 0.0f;

	while (!app_shutdown)
	{
		{
			// This extra block is used to separate the profiler's main loop from
			// the profiler's output stage

			PROFILE("Main Loop");

			last_time = current_time;
			current_time = ProfileSample::getTime();

			if (control_server_connection->isConnected())
			{
				PROFILE("Control Server Incoming Packet Processing");


				if (control_server_connection->run() < 0)
				{
					// run() encountered an error; The connection may have
					// been terminated by the server.
					control_server_connection->stop();

					printf("Lost connection to Koreobt Control Server...Reverting to Console mode.\n");
					mainMenu();
				}

			}
			else
			{
				menu();
			}

			if (korebot_peer_connection->isListening())
			{
				PROFILE("Korebot Peer Incoming Packet Processing");


				if ((temp = korebot_peer_connection->run()) < 0)
				{
					korebot_peer_connection->stop();
					printf("Lost connection to Korebot peers...\n");
				}
			}

			//printf("numofpackets = %d\n", temp);

			SensorManager::instance()->run(current_time - last_time);
			ExternalSensors::instance()->run(current_time - last_time);
			LocalizationSystem::instance()->run(current_time - last_time);
			AI::instance()->run(current_time - last_time);
			MotorController::instance()->run(current_time - last_time);

			// Add some pretend delays into the loop for simulation
			// Guess what? It's not a simulation anymore, this is a real robot.
			usleep(10000);
		}

#		ifdef PROFILER
		// Throttle the network traffic.  This gives us a chance to
		// average a bunch of loops, as well as limit network traffic

		if (profiler_delay + 0.5f < current_time)
		{
			ProfileSample::output();
			profiler_delay = current_time;
		}
#		endif
	}
}

/*
 * Prints & processes commands from the console menu
 */
void Korebot::menu()
{
	char input[7];

	// The following 3 variables are used for non-blocking I/O
	fd_set rfds;
	struct timeval tv;
	int retval;

	// Setup some variables to support non-blocking I/O
	tv.tv_sec = 0;
	tv.tv_usec = 0;

	FD_ZERO(&rfds);
	FD_SET(STDIN_FILENO, &rfds);
	retval = select(STDIN_FILENO + 1, &rfds, NULL, NULL, &tv);
	if (retval == -1)
	{
		perror("Korebot::menu: Error while polling for user input");
	}
	else if (retval)
	{
		// The user pressed a key
		int size = read(0, input, 6);

		if (menu_mode == MENU_MAIN_MENU)
		{
			if (input[0] == 'Q' || input[0] == 'q')
			{
				printf("Exiting\n");
				app_shutdown = 1;
			}
			else if (input[0] == 'C' || input[0] == 'c')
			{
				if (connectToControlServer())
				{
					printf("Successfully connected to control server!\n");
				}
				else
				{
					mainMenu();
				}
			}
			else if (input[0] == 'S' || input[0] == 's')
			{
				AI::instance()->stop();
				MotorController::instance()->stop();

				printf("Stopping Korebot.\n");
				mainMenu();
			}
			else if (input[0] == 'a' || input[0] == 'A')
			{
				if (!AI::instance()->isRunning())
				{
					printf("\n");
					AI::instance()->drawMenu();
					menu_mode = MENU_AI_SELECT;
				}
				else
				{
					AI::instance()->stop();
					printf("Stopping AI\n");
					mainMenu();
				}
			}
			else if (input[0] == 'b' || input[0] == 'B')
			{
				if (MotorController::instance()->getUseBoundsLimit())
				{
					printf("\n");
					printf("Korebot is no longer restricted to the bounds of the playing field.\n");
					MotorController::instance()->setUseBoundsLimit(0);
					mainMenu();
				}
				else
				{
					printf("\n");
					printf("Korebot is now restricted to the bounds of the playing field.\n");
					MotorController::instance()->setUseBoundsLimit(1);
					mainMenu();
				}
			}
			else if (input[0] == 'e' || input[0] == 'E')
			{
				if (SensorManager::instance()->getLoggingEnabled())
				{
					printf("\n");
					printf("Korebot is no longer logging sensor data.\n");
					SensorManager::instance()->setLoggingEnabled(0);
					mainMenu();
				}
				else
				{
					printf("\n");
					printf("Korebot is now logging sensor data.\n");
					SensorManager::instance()->setLoggingEnabled(1);
					mainMenu();
				}
			}
			else if (input[0] == 't' || input[0] == 'T')
			{
				printStatus();
			}
		}
		else if (menu_mode == MENU_AI_SELECT)
		{
			int id;
			input[size] = 0;
			sscanf(input, "%d", &id);
			AI::instance()->start(id);

			menu_mode = MENU_MAIN_MENU;
			mainMenu();
		}
	}
}

void Korebot::mainMenu()
{
	printf("\n");
	if (AI::instance()->isRunning())
	{
		printf("A: Stop AI.\n");
	}
	else
	{
		printf("A: Start AI.\n");
	}

	if (MotorController::instance()->getUseBoundsLimit())
	{
		printf("B: Don't restrict motor to bounds of playing field\n");
	}
	else
	{
		printf("B: Restrict motor to bounds of playing field\n");
	}

	if (SensorManager::instance()->getLoggingEnabled())
	{
		printf("E: Stop logging of sensor data\n");
	}
	else
	{
		printf("E: Start logging sensor data\n");
	}

	printf("C: Attempt to connect to control server again.\n");
	printf("S: Stop Korebot.\n");
	printf("T: Status.\n");
	printf("Q: Quit.\n");
}

/*
 * Prints the status of various modules on the Korebot
 */
void Korebot::printStatus()
{
	printf("MAIN KOREBOT STATUS:\n");
	printf("Robot ID:\t\t\t\t\t%d\n", robot_id);
	printf("Connection to Control Server:\t\t\t%s\n", control_server_connection->isConnected() ? "Connected" : "Not Connected");
	printf("Korebot Peer Connection:\t\t\t%s\n",    korebot_peer_connection->isListening()   ? "Connected" : "Not Connected");
	printf("Profiler Status:\t\t\t\t%s\n", ProfileSample::getProfilerIsRunning() ? "Running" : "Disabled");
	printf("\n");

	SensorManager::instance()->printStatus();
	ExternalSensors::instance()->printStatus();
	MotorController::instance()->printStatus();
	LocalizationSystem::instance()->printStatus();
	AI::instance()->printStatus();
}

/*
 * Attempts to connect to the control server; If successfull,
 * sends an RobotIdentification packet so the control server knows
 * about us.
 *
 * @returns 1 on success, 0 otherwise.
 */
int Korebot::connectToControlServer()
{
	RobotIdentification rid_packet;
	if (!control_server_connection->isConnected())
	{
		if (control_server_connection->start(control_server_config) == 1)
		{
			rid_packet.setRobotID(robot_id);
			rid_packet.setRobotName(robot_name);
			control_server_connection->transmit(&rid_packet);
			return 1;
		}
		return 0;
	}
	return 1;
}

/*
 * When a packet is received on the network, this method is called to
 * process the packet.
 *
 * @param con    The connection the packet was received on
 * @param packet The packet that was received from the network
 */
void Korebot::processPacket(TCP_Connection *con, BasePacket *packet)
{
	ConfigurationPacket *config = NULL;
	switch (packet->getOpcode())
	{
		case OPCODE_MOTOR:
			// Send it off to the motor controller to process
			MotorController::instance()->processPacket((MotorControl *)packet);
			break;
		case OPCODE_CONFIGURATION:
			config = (ConfigurationPacket *)packet;
			if (config->getOption() == CONFIGURATION_OPTION_PROFILER)
			{
				if (config->getValue())
				{
					ProfileSample::start_profiler = true;
				}
				else
				{
					ProfileSample::stop_profiler = true;
				}
			}
			else if (config->getOption() == CONFIGURATION_OPTION_REQUEST_CONFIG)
			{
				MotorController::instance()->sendConfig();
				SensorManager::instance()->sendConfig();
				LocalizationSystem::instance()->sendConfig();
				AI::instance()->sendConfig();

				config = new ConfigurationPacket(CONFIGURATION_OPTION_PROFILER, ProfileSample::getProfilerIsRunning());
				control_server_connection->transmit(config);
				delete config;
			}
			else if (config->getOption() == CONFIGURATION_OPTION_AI_ENABLE)
			{
				AI::instance()->start(config->getValue());
			}
			else if (config->getOption() == CONFIGURATION_OPTION_MOTOR_BOUNDS_LIMIT)
			{
				MotorController::instance()->setUseBoundsLimit(config->getValue());
			}
			else if (config->getOption() == CONFIGURATION_OPTION_SENSOR_LOGGING)
			{
				SensorManager::instance()->setLoggingEnabled(config->getValue());
			}
			else
			{
				fprintf(stderr, "Korebot::processPacket: Unknown configuration option (%d) received.\n", config->getOption());
			}
			break;
		case OPCODE_SENSOR:
			SensorManager::instance()->processPacket((SensorControl *)packet);
			break;
		case OPCODE_XBOX_SENSOR:
			ExternalSensors::instance()->processPacket(packet);
			break;
		default:
			// Unknown packet, send it to the AI to process
			AI::instance()->processPacket(packet);
			break;
	}
}

/*
 * When a packet is received on the network, this method is called to
 * process the packet.
 *
 * @param con    The connection the packet was received on
 * @param packet The packet that was received from the network
 */
void Korebot::processPacket(UDP_Connection *con, BasePacket *packet)
{
	AI::instance()->processPacket(packet);
}

/*
 * Gets the connection to the control server, so other parts of
 * the program can send information to the control server.
 *
 * @returns The connection to teh control server
 */
TCP_Connection *Korebot::getControlServerConnection()
{
	return control_server_connection;
}

/*
 * Gets the connection to peer Korebots, so other parts of
 * the program can communicate with other Korebots.
 *
 * @returns The connection to peer Korebots
 */
UDP_Connection *Korebot::getKorebotPeerConnection()
{
	return korebot_peer_connection;
}

/*
 * Gets the korebot ID associated with this korebot
 *
 * @returns The ID of this korebot
 */
unsigned char Korebot::getRobotID()
{
	return robot_id;
}

/*
 * Gets the name of the robot associated with this korebot
 *
 * @returns The name of the robot
 */
const char *Korebot::getRobotName()
{
	return robot_name;
}
