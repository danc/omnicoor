#include "utils.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <termios.h>

/**
 * Normalizes a number within range A and B to be equivalent in range C to D
 */
double normalize(double num, int A, int B, int C, int D)
{
    double newValue = 0;

    newValue = C + (num - A) * (D - C) / (B - A);

    return newValue;
}

/**
 * Radians to degrees convertion utility function
 */
double RAD_TO_DEG(double angle)
{
    double temp = angle / M_PI * 180.0;
    while ( temp > 180.0 || temp < -180.0 )
    {
        if ( temp > 180)
        {
            temp -= 180;
        }
        if (temp < -180)
        {
            temp += 180;
        }
    }
    return temp;
}

char getch()
{
    char buf = 0;
    struct termios old = {0};

    if (tcgetattr(0, &old) < 0)
    {
        perror("tcsetattr()");
    }
    old.c_lflag &= ~ICANON;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
    {
        perror("tcsetattr ICANON");
    }
    if (read(0, &buf, 1) < 0)
    {
        perror ("read()");
    }
    old.c_lflag |= ICANON;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
    {
        perror ("tcsetattr ~ICANON");
    }
    return (buf);
}
