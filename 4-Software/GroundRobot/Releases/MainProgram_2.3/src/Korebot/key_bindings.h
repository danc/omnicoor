#ifndef _KOREBOT__KEY_BINDINGS_H_
#define _KOREBOT__KEY_BINDINGS_H_

/**
 * This class holds the key bindings for the command line interface. Changes and additions can
 * easily be made by anyone who does not like the available commands.
 * 
 * The different sets may have the same command because you can only use one set of commands at
 * once.
 * 
 */
class KeyBindings
{
public:
    // Can be used in any mode
    static const char TUNING_MODE       = 'v';    // F1
    static const char ROBOT_CTRL_MODE   = 'b';    // F2
    static const char BOX_STEP_MODE     = 'n';    // F3
    static const char RESET_I_COMP      = '2';
    static const char ADD_LOG_MARKER    = 'm';
    static const char QUIT              = 27;       // escape key

    // Only used when in ROBOT_CONTROL mode
    static const char FORWARD           = 'w';
    static const char REVERSE           = 's';
    static const char RIGHT             = 'd';
    static const char LEFT              = 'a';
    static const char STOP_ROBOT        = 'p';
    static const char FORWARD_RIGHT     = 'y';
    static const char FORWARD_LEFT      = 't';
    static const char REVERSE_RIGHT     = 'i';
    static const char REVERSE_LEFT      = 'u';
    static const char ROTATE_CW         = 'e';
    static const char ROTATE_CCW        = 'q';

    static const char PITCH_SP_UP       = '7';
    static const char PITCH_SP_DN       = '4';
    static const char ROLL_SP_UP        = '8';
    static const char ROLL_SP_DN        = '5';

    // Only used when running in TUNING and BOX_STEP modes 
    static const char TUNE_P_UP         = '7';
    static const char TUNE_P_DN         = '4';
    static const char TUNE_I_UP         = '8';
    static const char TUNE_I_DN         = '5';
    static const char TUNE_D_UP         = '9';
    static const char TUNE_D_DN         = '6';
    static const char TUNE_SP_UP        = 'i';
    static const char TUNE_SP_DN        = 'k';
    static const char TUNE_TAW_UP       = 'o';
    static const char TUNE_TAW_DN       = 'l';
    static const char TUNE_PITCH        = '1';
    static const char TUNE_ROLL         = '3';
};

#endif  // _KOREBOT__KEY_BINDINGS_H_