---------------------
* Table of Contents *
---------------------
  * Introduction
  * Contents
  * Compiling
  * Authors

----------------
* Introduction *
----------------
This README describes the contents of the Releases folder. This folder contains the many
iterations of code that were being developed for the current ground robot (omnibot Eris).

------------
* Contents *
------------
MainProgram_2.1.tar.gz
  * This version of the main program uses the new softdmc_1.2.bit file to control the pendulum.
    pwmgenc outputs are the ones being used for the pendulum motors.

MainProgram_2.0.tar.gz
  * The MainProgram that is ported to only compile the ground robot code (Eris) (no longer compiles
    korebot code). It can control the 1D pendulum. Might be able to control 2D, but don't remember
    for sure.

MainProgram_1.0.tar.gz
  * A directory containing the ground robot code used for just running the omnibot Eris with no
    extra control (Last worked with the xbox controller)
  * This version of code will only compile on the old Debian 6 computer (Omnibot-linux-2) that may
    or may not still be in the lab (It was when I left in May 2015).
    If you would like this code to work on new computers, but don't need to support the korebot code
    that was compiled using arm, then follow these steps below:
      - In reality, you can probably use the code from MainProgram_2.0.tar.gz, but you would need to
        copy the main.cpp and MotorController.cpp/h file from MainProgram_1.0 (since we changed
        them). The only thing that is not compiled when using that version is the GUI, but that is
        not 100% necessary. If you want to try and compile everything, then follow the below steps.
      - Read up on how autotools (automake and autoconf) work
      - Copy all source files (.cpp, .c, .h) into their own directory. Keep the original structure
        so that the files are located in the same relative location.
      - Copy the Makefile.am files from the old structure into the new one as well.
      - Edit the Makefile.am files to be similar to the onew in MainProgram_2.1 or later
        (essentially, remove the arm compiling code that is at the bottom).
      - Create a configure.ac file that is similar to the one in MainProgram_1.0. This will look
        very similar to the configure.ac file in MainProgram_2.0.
      - Install the dependencies listed in README.Compiling
      - GTKDataBox will need to be installed because it is used for the GUI. Look online to figure
        out how to do this, we did not do it.
      - Run automake --add-missing
      - Run autoreconf
      - You may need to run automake --add-missing again
      - Run ./configure
      - Run make
      - Hopefully it worked. If there was an issue on the way, it was probably my fault because I
        forgot to tell you something. Read up on how to use autotools and you will figure it out.

vrpn_tcp.tar.gz
  * I believe this was used with MainProgram_2.0.tar.gz, but I cannot recall completely.
    This vrpn client was used when we did tcp communications to send the data to Eris, but we ran
    into many issues using this. It is best if this is not used anymore, but it is here for
    reference.

-------------
* Compiling *
-------------
View README.Compiling for more information on how to compile the code within the subdirectories
listed

-----------
* Authors *
-----------
  Dylan Gransee - dgransee@gmail.com
  Robert Larsen
