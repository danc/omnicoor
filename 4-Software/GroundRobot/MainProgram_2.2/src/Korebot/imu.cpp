/******BROADCAST MODE IS NOT WORKING*****/
/* IMU.c 
 *This code allows for easy access to the IMU 
 *
 Author: Seth Beinhart
         Beinhart@iastate.edu
*/
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <pthread.h>
#include <stdlib.h>

//#include "serial.h"
#include "imu.h"
#include "PT.h"
/*private Functions*/
int getPacket( BYTE* PT, int MaxBuffer,  BYTE* data);
int sendPacket( BYTE PT, BYTE N,  BYTE* data);
int Update_Mode(void);
int Update_Gyro_Bias();
int Update_Accel_Bias();
void* BroadCastWorker(void* p);
int Parse_Data_Packet(BYTE * data, int N);


// Global Vars
int fd=0;
pthread_t tid;
IMU_STATUS* REPORT=NULL;
IMU_SENSOR_DATA* IMU_SENSOR_DATA_STRUCT =NULL;
pthread_mutex_t IMU_mutex;


/* Function: initIMU()
		Opens COM1. Allocates IMU_SESOR_DATA_STRUCT and REPORT Structures

*/
int initIMU()
{
	fd = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd == -1) {
		perror("open_port: Unable to open /dev/ttyS0 - ");
		return 1;
	} 
	else {
		fcntl(fd, F_SETFL, 0);
	}

	initport(fd);
	
	fcntl(fd, F_SETFL, FNDELAY); // don't block serial read?????

	pthread_mutex_init(&IMU_mutex,NULL);

	REPORT=(IMU_STATUS *)malloc(sizeof(IMU_STATUS));
	IMU_SENSOR_DATA_STRUCT=(IMU_SENSOR_DATA *)malloc(sizeof(IMU_SENSOR_DATA));
	if(Set_Mode(0,0)==-1)
	{

		close(fd);// set the starting mode
		fd=0;
	}
	return(fd);
}
/*Function: deinitIMU()
 	 Destroys the IMU structures and closes the serial connection. Any calls to imu functions after this is called are undefinded
 **/
int deinitIMU()
{
	free(REPORT);
	free(IMU_SENSOR_DATA_STRUCT);
	REPORT=NULL;
	if(tid!=NULL)
	{
		pthread_mutex_lock(&IMU_mutex);//Insures the mutex is released by the thread before it is destroyed
		pthread_cancel(tid);
		pthread_mutex_unlock(&IMU_mutex);
	}	
	
	pthread_mutex_destroy(&IMU_mutex);
	close(fd);
return 0;
}
/** Private Function used by all public functions to format the packet sent to the imu
	A get packet should be called following this function. It is suggested that a mutex is used around these 
	calls.
**/
int sendPacket( BYTE PT,BYTE N, BYTE* data)
{

	int i=0;
	int PacketSize=7+(unsigned int)N;
	if(PacketSize>256)
	{
		return -1;	//invailed param
	}
	BYTE txpacket[PacketSize];
	txpacket[0]='s';
	txpacket[1]='n';
	txpacket[2]='p';
	txpacket[3]=PT;
	txpacket[4]=N;
	//Copy Payload into packet;	
	for(i=0;i<(int)(N);i++)
	{

		txpacket[i+5]=data[i];
	}
	//CALCULATE CHECKSUM
	int checksum=0;
	int j=0;
	for(j=0;j<PacketSize-2;j++)
	{
		checksum+=txpacket[j];
		
	}
	txpacket[i+5]= (BYTE)(checksum>>8)& 0x0ff;
	txpacket[i+6]= (BYTE) checksum & 0x0ff;

	if(!writeport(fd, txpacket,PacketSize)) 
	{
		
		return -1;
	}
	
	return 0;

}

/*Private Function
 * Gets the first packet residing on the read buffer
 	Return Value: Number of Bytes in the payload section of the packet (data) or -1 indicates  general error -2 indicates buffer is too small -5 indicates  checksum error
	Inputs:
		PT* - Pointer to a USER allocated address for the PT
		Max_BUFFER - MAXimum size of allocated space for the data array (payload)
		data* - point to the begining of a user allocated of array of size Max_Buffer
	NOTE: If the buffer is too small the data on the read buffer will be thrown out and a -2  is returned
	the command should be resent.
	
	If there is no data on the serial read buffer or an incomplete packet is present a -1 will be returned. 
*/

int getPacket(  BYTE* PT,int MaxBuffer,  BYTE* data)
{
	

	int read;
	BYTE sResult[2];
	int bBeginingFound=0;
	usInt16 checksum=0;
	while(!bBeginingFound)
	{
		checksum=0; //reset checksum
		read=readport(fd,sResult,1);
		while(read>0 && sResult[0]!='s')
		{
			read=readport(fd,sResult,1);
			
		}
		checksum+=(usInt16) sResult[0];
		
		//Is it s or end of data? The following read will pull an n if its an s otherwise it will terminate with error
		read=readport(fd,sResult,1);
		if(read!=0)		// is there another char?
		{
			
			if(sResult[0]=='n')	//if its a N check to see if the next is a P if its not and there are still chars on the buffer restart looking for an s
			{	
				checksum+=(usInt16) sResult[0];
				
				read=readport(fd,sResult,1);
				
				if(read!=0)	
				{
					if(sResult[0]=='p')	// is the next char a p? if so the start codon for the packet has begun break out. Else start looking for s if chars are still present
					{
						checksum+=(usInt16) sResult[0];

					 	bBeginingFound=1;
					}
		
				}else
				{
					return -1;	
				}
			}
		}else
		{
			return -1;
		}
	}

	//SNP should be removed from the buffer so the true start to the packet should be here.
	// The function would of exited had the data not been avi
	
	//next grab the PT
	read=readport(fd,sResult,1);
	if(read!=0)
	{
		 *PT=sResult[0];
		 checksum+=(usInt16) sResult[0];
		 }
	else return -1;
	
	//Get the size of the payload
	 int N;
	read=readport(fd,sResult,1);
	if(read!=0)
	{
		 N=(unsigned int)sResult[0];
		checksum+=(usInt16) sResult[0];
		
	}
	else 
	{
	return -1;
	}	//
	
	//Data too large?
	if(N>MaxBuffer)
	{
		printf("BUFFER OVERFLOW\n");
		return(-2);
	}
	//Get the the payload
	if(N>0)
	{
		read=readport(fd,data,N);
	}
	
	//Get the checksum
	read=readport(fd,sResult,2);
	if(read==2)
	{
		//calculate checksum
		int i;
		for(i=0;i<N;i++)
		{
			checksum+=(usInt16)data[i];
			
		}	
		unsigned int check=0; 
		check=sResult[0];	// lower byte
		check=(check<<8)|sResult[1];
		if(check!=checksum)
		{
			return -5;
		}
	
	
	}
	return  N;


}
/*Function: Get_Status()
  Input:  pIn is A user allocated IMU_STATUS structure that is empty. This structure is then populated by this function.
	  	  update: Forces the Report struc to update.
	  	  Note: It should not be required to update since fields are updates as the change but is there if needed
  Return: 0 Indicates Success
	  	  -1 indicates failure...
			Likely reasons for failure is either Pin==Null or the imu has been Dinit'ed
	
*/
int Get_Status(IMU_STATUS * pIn, int update)
{

	//add calibartion
	if(REPORT==NULL|| pIn==NULL)
	{
	 	return -1;
	}
	if(update)
	{
		while(Update_Mode()){}
		while(Update_Gyro_Bias()){}
		while(Update_Accel_Bias()){}
	}
	pIn->Mode=REPORT-> Mode;
	pIn->Freq=REPORT-> Freq;
	pIn->Gyro_Z_Bias=REPORT->Gyro_Z_Bias;
	pIn->Gyro_Y_Bias=REPORT->Gyro_Y_Bias;
	pIn->Gyro_X_Bias=REPORT->Gyro_X_Bias;
	pIn->Accel_Z_Bias=REPORT->Accel_Z_Bias;
	pIn->Accel_Y_Bias=REPORT->Accel_Y_Bias;
	pIn->Accel_X_Bias=REPORT->Accel_X_Bias;
	return 0;



}
/* Private function. Updates REPORTS freq and mode feilds
   Return: -1 indicates failure 
            0 indicates success;
*/
int Update_Mode(void)
{
	BYTE pt=GET_BROADCAST_MODE;
	BYTE n=0;
	BYTE rpt=0;
	BYTE rsCmd[255];
	int size=0;
	pthread_mutex_lock(&IMU_mutex);
	tcflush(fd,TCIFLUSH);   
	sendPacket(pt,n,NULL);
	
	while(rpt!=BROADCAST_MODE_REPORT && size!=-1)
	{	

		size=getPacket(&rpt,255,rsCmd);
	}
	pthread_mutex_unlock(&IMU_mutex);
	if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
	{
		return -1;
	}
	if(rsCmd[1]==0x1)
	{
		REPORT->Mode=1;
		REPORT->Freq=(280/255)*rsCmd[0]+20;
		
	}
	else{
	 REPORT->Mode=0;
	 REPORT->Freq=-1;	//indicates silent mode
	}
	
	return 0;
}
/*Private Function:
	Updates the REPORT Struct to signify the bias's the imu is using
*/
int Update_Gyro_Bias()
{


	BYTE pt=GET_GYRO_BIAS;
	BYTE n=0;
	BYTE rpt=0;
	BYTE rsCmd[255];
	int size=0;

	pthread_mutex_lock(&IMU_mutex);
	
	tcflush(fd,TCIFLUSH);   
	sendPacket(pt,n,NULL);
	while(rpt!=GYRO_BIAS_REPORT && size!=-1)
	{	
		size=getPacket(&rpt,255,rsCmd);
	}
	pthread_mutex_unlock(&IMU_mutex);
	if(size==-1 || size== -5 || size!=6)			// nothing left on the buffer or invaild checksum has happened or invaild size
	{
		return -1;
	}
	//Parse the z Bias
	REPORT->Gyro_Z_Bias=rsCmd[0];	//msbyte
	REPORT->Gyro_Z_Bias=REPORT->Gyro_Z_Bias<<8|rsCmd[1];
	//Parse the Y Bias
	REPORT->Gyro_Y_Bias=rsCmd[2];	//msbyte
	REPORT->Gyro_Y_Bias=REPORT->Gyro_Y_Bias<<8|rsCmd[3];
	//Parse the X Bias
	REPORT->Gyro_X_Bias=rsCmd[4];	//msbyte
	REPORT->Gyro_X_Bias=REPORT->Gyro_X_Bias<<8|rsCmd[5];

	return 0;

}
/*Private Function:
	Updates the Report Struct to signify the bia's the imu is using
*/
int Update_Accel_Bias()
{


	BYTE pt=GET_ACCEL_BIAS;
	BYTE n=0;
	BYTE rpt=0;
	BYTE rsCmd[255];
	int size=0;

	pthread_mutex_lock(&IMU_mutex);
	tcflush(fd,TCIFLUSH);   
	sendPacket(pt,n,NULL);
	
	while(rpt!=ACCEL_BIAS_REPORT && size!=-1)
	{	
		size=getPacket(&rpt,255,rsCmd);
	}
	pthread_mutex_unlock(&IMU_mutex);
	if(size==-1 || size== -5 || size!=6)			// nothing left on the buffer or invaild checksum has happened or invaild size
	{
		return -1;
	}
	//Parse the Z Bias
	REPORT->Accel_Z_Bias=rsCmd[0];	//msbyte
	REPORT->Accel_Z_Bias=REPORT->Accel_Z_Bias<<8|rsCmd[1];
	//Parse the Y Bias
	REPORT->Accel_Y_Bias=rsCmd[2];	//msbyte
	REPORT->Accel_Y_Bias=REPORT->Accel_Y_Bias<<8|rsCmd[3];
	//Parse the X Bias
	REPORT->Accel_X_Bias=rsCmd[4];	//msbyte
	REPORT->Accel_X_Bias=REPORT->Accel_X_Bias<<8|rsCmd[5];

	return 0;

}




/* 	Function: Manual_Sensor_Update
		Manually Updates the IMU_SENSOR_DATA struct which can be acquired by calling  Get_Sensor_Data_Struct(). This struct will not be updated
 	 	automatically when not in broadcast mode so this function is needed to update the struct
 	 Returns: 0 if Successful -1 if failed... likely reasons include transmission errors or IMU is in broadcast mode;
*/
int Manual_Sensor_Update(void)
{
	if(REPORT->Mode==1)
	{
		return -1;		// cannot manually update when in broadcast mode
	}

	BYTE pt=GET_DATA;
	BYTE n=0;
	BYTE rpt=0;
	BYTE rsCmd[255];
	int size=0;
	pthread_mutex_lock(&IMU_mutex);
	tcflush(fd,TCIFLUSH);   
	sendPacket(pt,n,NULL);
	
	while(rpt!=SENSOR_DATA && size!=-1)
	{	

		size=getPacket(&rpt,255,rsCmd);


	}
	pthread_mutex_unlock(&IMU_mutex);
	if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
	{
		return -1;	// no packet ready
	}
	return(Parse_Data_Packet(rsCmd,size));
	
	
		

}


/* Private function: Parses the data receive with a get_data packet. This function updates the IMU_Sensor_Data Struct that is given to the user
   Returns: 0 indicates success
   	    -1 indicates failure
*/
int Parse_Data_Packet(BYTE * data, int N)
{
 	BYTE mask=0x01;
	IMU_SENSOR_DATA_STRUCT->ACTIVE_CHANNELS=data[0];
	int i=1;
	while(mask && i<N)
	{
		Int16 channel_data=0;
		channel_data=data[i];	//msbyte
		channel_data=channel_data<<8|data[i+1];
	 	if(data[0]&mask)
		{
			if(mask==1)
			{
				IMU_SENSOR_DATA_STRUCT->pitch= channel_data*0.0109863 ;
				i+=2;
			}
			else if(mask==2)
			{
				IMU_SENSOR_DATA_STRUCT->roll= channel_data*0.0109863;
				i+=2;
			}
			else if(mask==4)
			{
				IMU_SENSOR_DATA_STRUCT->gyro_Z= channel_data*0.02014;
				i+=2;
			}
			else if(mask==8)
			{	
				IMU_SENSOR_DATA_STRUCT->gyro_Y= channel_data*0.02014;
				i+=2;
			}
			else if(mask==16)
			{
				IMU_SENSOR_DATA_STRUCT->gyro_X= channel_data*0.02014;
				i+=2;
			}
			else if(mask==32)
			{
				IMU_SENSOR_DATA_STRUCT-> accel_Z= channel_data*0.0001678;
				i+=2;
			}
			else if(mask==64)
			{
				IMU_SENSOR_DATA_STRUCT->accel_Y= channel_data*0.0001678;
				i+=2;
			}
			else if(mask==128)
			{		
				IMU_SENSOR_DATA_STRUCT->accel_X= channel_data*0.0001678;
				i+=2;
			}
			
		}
		mask=mask<<1;
	}
	if(mask!=0)
	{
		//printf("Issue\n");
		return -1;
	}
	
	return (0);
	
}
/*Function:Set_ACTIVE_CHANNELS
  	  	  Specifies which channel data will be Transmitted over the UART and will update the IMU_SENSOR_DATA_STRUCT
  Input:	Each bit in PChannels corresponds to a specific channel. See section 7.1.4  for byte D1 to see the mapping
  Return: 0 Indicates success -1 Indicates failure
*/
int Set_ACTIVE_CHANNELS(BYTE pChannels)
{


	BYTE pt=SET_ACTIVE_CHANNELS;
	BYTE n=1;
	BYTE rpt=0;
	BYTE sCmd[255];
	sCmd[0]=pChannels;
	BYTE rsCmd[255];
	int size=0;

	pthread_mutex_lock(&IMU_mutex);
	tcflush(fd,TCIFLUSH);   
	sendPacket(pt,n,sCmd);
	
	while(rpt!=COMMAND_COMPLETE && size!=-1)
	{	

		size=getPacket(&rpt,255,rsCmd);

	}
	pthread_mutex_unlock(&IMU_mutex);
	if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
	{
		return -1;
	}
	
	return 0;

}


/*Function:Set_Gyro_Bias
	Manually Sets the zero rate gyro zero-point for each of the given inputs.
  Input: Each of the usInt16 corresponds to the bias that should be set on that channel. the Set* refers to weather or not that channel should
 	 be set. If this var is set to 0 the corresponding channel bias will not be altered and the usInt16 value discarded.
  Returns: -1 will be returned if any bias's are not updated. However this does not mean that they were not set. Use Get_Status with the update flag set to determine
	if the bias was set
*/
int Set_Gyro_Bias(usInt16 Z_Bias, usInt16 Y_Bias,usInt16 X_Bias, int SetZ,int SetY,int SetX)
{

	BYTE rpt=0;
	BYTE sCmd[255];
	BYTE rsCmd[255];
	int size=0;
	int retval=0;
	
	
	BYTE pt=SET_Z_GYRO_BIAS;
	BYTE n=2;
	sCmd[0]=(BYTE)(Z_Bias>>8)&0x0ff;
	sCmd[1]=(BYTE)Z_Bias & 0x0ff;
	if(SetZ)
	{
		//Set the Z gyro Bias
		pthread_mutex_lock(&IMU_mutex);
		tcflush(fd,TCIFLUSH);   
		sendPacket(pt,n,sCmd);
	
		while(rpt!=COMMAND_COMPLETE && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);
	
		}
		pthread_mutex_unlock(&IMU_mutex);
		if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
		{
			retval= -1;
		}
		else
		{	
			//Success
			REPORT->Gyro_Z_Bias=Z_Bias;
		}
	}
	if(SetY)
	{
		//Set the Y GYro bias
		pt=SET_Y_GYRO_BIAS;
		BYTE n=2;
		sCmd[0]=(BYTE)(Y_Bias>>8)&0x0ff;
		sCmd[1]=(BYTE)Y_Bias & 0x0ff;


		pthread_mutex_lock(&IMU_mutex);
		tcflush(fd,TCIFLUSH);   
		sendPacket(pt,n,sCmd);
	
		while(rpt!=COMMAND_COMPLETE && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);
		
		}
		pthread_mutex_unlock(&IMU_mutex);
		if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
		{
			retval= -1;
		}
		else
		{
			//Success
			REPORT->Gyro_Y_Bias=Y_Bias;
		}
	}

	if(SetX)
	{
		//Set the X GYro bias
		pt=SET_X_GYRO_BIAS;
		BYTE n=2;
		sCmd[0]=(BYTE)(X_Bias>>8)&0x0ff;
		sCmd[1]=(BYTE)X_Bias & 0x0ff;


		pthread_mutex_lock(&IMU_mutex);
		tcflush(fd,TCIFLUSH);   
		sendPacket(pt,n,sCmd);
	
		while(rpt!=COMMAND_COMPLETE && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);
		
		}
		pthread_mutex_unlock(&IMU_mutex);
		if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
		{
			retval= -1;
		}
		else
		{
			//Success
			REPORT->Gyro_X_Bias=X_Bias;
		}
	}



	return retval;

}



/*Function: Set_Accel_Bias
	Manually Sets the zero rate Accel zero-point for each of the given inputs.
  Input: Each of the usInt16 corresponds to the bias that should be set on that channel. the Set* refers to weather or not that channel should
  	be set. If this var is set to 0 the corresponding channel bias will not be altered and the usInt16 value discarded.
  Returns: -1 will be returned if any bias's are not updated. However this does not mean that they were not set. Use Get_Status with the update flag set to determine
	if the bias was set
*/
int Set_Accel_Bias(usInt16 Z_Bias, usInt16 Y_Bias,usInt16 X_Bias, int SetZ,int SetY,int SetX)
{



	BYTE rpt=0;
	BYTE sCmd[255];
	BYTE rsCmd[255];
	int size=0;
	int retval=0;
	
	
	BYTE pt=SET_Z_ACCEL_BIAS;
	BYTE n=2;
	sCmd[0]=(BYTE)(Z_Bias>>8)&0x0ff;
	sCmd[1]=(BYTE)Z_Bias & 0x0ff;
	if(SetZ)
	{
		//Set the Z accel Bias
		pthread_mutex_lock(&IMU_mutex);
		tcflush(fd,TCIFLUSH);   
		sendPacket(pt,n,sCmd);
	
		while(rpt!=COMMAND_COMPLETE && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);
	
		}
		pthread_mutex_unlock(&IMU_mutex);
		if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
		{
			retval= -1;
		}
		else
		{	
			//Success
			REPORT->Accel_Z_Bias=Z_Bias;
		}
	}
	if(SetY)
	{
		//Set the Y accel bias
		pt=SET_Y_ACCEL_BIAS;
		BYTE n=2;
		sCmd[0]=(BYTE)(Y_Bias>>8)&0x0ff;
		sCmd[1]=(BYTE)Y_Bias & 0x0ff;


		pthread_mutex_lock(&IMU_mutex);
		tcflush(fd,TCIFLUSH);   
		sendPacket(pt,n,sCmd);
	
		while(rpt!=COMMAND_COMPLETE && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);
		
		}
		pthread_mutex_unlock(&IMU_mutex);
		if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
		{
			retval= -1;
		}
		else
		{
			//Success
			REPORT->Accel_Y_Bias=Y_Bias;
		}
	}

	if(SetX)
	{
		//Set the X accel bias
		pt=SET_X_ACCEL_BIAS;
		BYTE n=2;
		sCmd[0]=(BYTE)(X_Bias>>8)&0x0ff;
		sCmd[1]=(BYTE)X_Bias & 0x0ff;


		pthread_mutex_lock(&IMU_mutex);
		tcflush(fd,TCIFLUSH);   
		sendPacket(pt,n,sCmd);
	
		while(rpt!=COMMAND_COMPLETE && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);
		
		}
		pthread_mutex_unlock(&IMU_mutex);
		if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
		{
			retval= -1;
		}
		else
		{
			//Success
			REPORT->Accel_X_Bias=X_Bias;
		}
	}



	return retval;

}



/********** APPEARS TO RETURN A GYRO_BIAS_REPORT NOT DOCUMENTED THO... MAY WANT TO  UPDATE REPORT->GYRO_-_BIAs in future***/

/* Function: Self_Calibrate_Gyro()
	Starts the self-calibration process for all 3 gyro axes on the robot. The robot should not be moved during this process
  Input: The max number of retry attempts if the calibration packet fails
  Return: 0 Indicates success
		-1 Indicates max retry failed
*/

int Self_Calibrate_Gyro(int pmax_retrys)
{
	printf("\nGyro Self-Calibration in progress... Please do not move the robot\n");
	BYTE pt=ZERO_RATE_GYROS;
	BYTE n=0;
	BYTE rpt=0;
	BYTE rsCmd[255];
	int size=0;
	int max_retry=pmax_retrys;
	int retry =1;
	pthread_mutex_lock(&IMU_mutex);
	while(retry<=max_retry)	
	{
		printf("\nAttempt# %d of %d\n",retry,max_retry);
		tcflush(fd,TCIFLUSH);  
		//sleep(1); 
		sendPacket(pt,n,NULL);
		sleep(6+retry);	// allow time for test to complete
		while(rpt!=GYRO_BIAS_REPORT && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);

		}
		if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
		{
			retry++;
		}else
		{
			pthread_mutex_unlock(&IMU_mutex); //success has occured so unlock the mutex and return success
			printf("\nSuccess: it is now safe to move the robot\n");
			return 0;
		}
		pthread_mutex_unlock(&IMU_mutex);
	}
	printf("FAILED: it is now safe to move the robot\n");
	return -1;	//Max retrys occured
}
/* Function: IMU_SELF_TEST
	Instructs the IMU to perform a Self-Test of all Sensor Channels. The Robot should not be moved during this procedure.
   Returns: A BYTE Corresponding to D1 in 7.2.8 of the IMU documentation
	
*/
BYTE IMU_Self_Test()
{
	printf("\nIMU Self Test in progress... Please do not move the robot\n");
	BYTE pt=SELF_TEST;
	BYTE n=0;
	BYTE rpt=0;

	BYTE rsCmd[255];
	int size=0;

	pthread_mutex_lock(&IMU_mutex);
	tcflush(fd,TCIFLUSH);
   
	
	sendPacket(pt,n,NULL);
	sleep(1);// allow time for the command to complete
	while(rpt!=STATUS_REPORT && size!=-1)
	{	
		
		size=getPacket(&rpt,255,rsCmd);

	}
	pthread_mutex_unlock(&IMU_mutex);
	if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
	{
		printf("\nFAILED: it is now safe to move the robot\n");
		return -1;
	}
	printf("Success: it is now safe to move the robot\n");
	return rsCmd[0];

}
/* Function:Get_IMU_SENSOR_DATA_STRUCT()
   Returns: A pointer to an IMU_SENSOR_DATA structure. This structure will be updated only when Manual_Sensor_Update() is called when in silent mode.
   This structure will be updated Automatically when in broadcast mode.
   This structure SHOULD NOT BE WRITTEN TO by the user. This structure will be destroyed by the deInit function.
*/
IMU_SENSOR_DATA* Get_IMU_SENSOR_DATA_STRUCT()
{
	return(IMU_SENSOR_DATA_STRUCT);
}
/* Enables or disables broadcast mode
  pmode=0... sets silent mode
  pmode=1 ... sets broadcast mode
  if pmode =1 a frequency of max of 300hz...
  Returns: 0 if successful completion
  Returns: -1 if COMMAND_COMPLETE is never recieved from imu
*/
int Set_Mode(int pmode, float freq)
{
	if (pmode!=0 && pmode !=1)
	{
		return -1; 	// Invaild param
	}

	if(pmode==1 &&(freq<20.0||freq>300))
	{
		return -1;
	}

	BYTE pt=0;
	BYTE n=0;
	BYTE rpt=0;
	BYTE rsCmd[255];
	int size=0;
	BYTE X =((freq-20)*255)/280;
	freq=((280/255)*X+20);	//incase any impression comes up
	//set broadcast mode
	if( pmode==1)
	{
		pt=SET_BROADCAST_MODE;
		n=1;
		rsCmd[0]=X;
		//printf("SETTING BROAD MODE\n");
		pthread_mutex_lock(&IMU_mutex);
		tcflush(fd,TCIFLUSH);   
		sendPacket(pt,n,rsCmd);

		while(rpt!=COMMAND_COMPLETE && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);
			
		}
		pthread_mutex_unlock(&IMU_mutex);
		if(size!=-1 || size!=-5)	// There existed something on the buffer and it didnt return a -5 for invaild checksum 
		{
			REPORT->Mode=1;
			REPORT->Freq= freq;
			//launch worker thread
			if(tid!=NULL)	// kill old if its running
			{
				pthread_mutex_lock(&IMU_mutex);//Insures the mutex is released by the thread before it is destroyed	
				pthread_cancel(tid);
				pthread_mutex_unlock(&IMU_mutex);
				tid=NULL;
			}
			pthread_create(&tid, NULL, BroadCastWorker, &(REPORT->Freq));
			
			return 0;
		}
	}
	//Set Silent Mode
	if( pmode==0)
	{
		pt=SET_SILENT_MODE;
		//printf("SETTING SILENT MODE\n");
		pthread_mutex_lock(&IMU_mutex);
		tcflush(fd,TCIFLUSH);   
		sendPacket(pt,n,NULL);
		
		while(rpt!=COMMAND_COMPLETE && size!=-1)
		{	
	
			size=getPacket(&rpt,255,rsCmd);
		}
		pthread_mutex_unlock(&IMU_mutex);
		//TODO:Kill worker thread if it exist
		if(tid!=NULL)
		{
			pthread_mutex_lock(&IMU_mutex);	//Insures the mutex is released by the thread before it is destroyed
			pthread_cancel(tid);
			pthread_mutex_unlock(&IMU_mutex);
			tid=NULL;
		}
		if(size!=-1 || size!=-5)	// There existed something on the buffer and it didnt return a -5 for invaild checksum 
		{
			REPORT->Mode=0;
			REPORT->Freq= -1;;
			return 0;
		}
		
	}

	return -1;
}
/** Thread function used to update the IMU_SENSOR_DATA_STRUCT as broadcast packets arrive
THIS FUNCTION DOES NOT WORK TO UPDATE THE STRUCT??????**/
void* BroadCastWorker(void * p)
{
	float freq_period=*((float*) p);	//Starts out as frequency
	printf("freq=%f\n",freq_period);
	BYTE rpt=0;
	BYTE rsCmd[255];
	int size=0;
	freq_period=1/freq_period;
	freq_period*=(float)1000000;
	
	printf("IN thread %d\n",(int)freq_period);
	while(1)
	{
		usleep((int)freq_period);
		
		pthread_mutex_lock(&IMU_mutex);
		printf("IN thread %d\n",(int) freq_period);
		while(rpt!=SENSOR_DATA && size!=-1)
		{	

			size=getPacket(&rpt,255,rsCmd);
			//size=getPacket(&rpt,255,rsCmd);
			//size=getPacket(&rpt,255,rsCmd);
		}
		pthread_mutex_unlock(&IMU_mutex);
		if(size==-1 || size== -5)			// nothing left on the buffer or invaild checksum has happened
		{
			
			continue;	// no packet ready or invaild checksum	
		}
		else{
			Parse_Data_Packet(rsCmd,size);
		}
	}	
}
