#include "sensor_manager.h"

#include <stdio.h>
#include <string.h>

#include "configuration.h"
#include "korebot.h"
#include "motor_controller.h"
#include "profiler.h"

#include "Network/tcp_connection.h"
#include "Network/packets/configuration_packet.h"
#include "Network/packets/sensor_data.h"
#include "Network/packets/sensor_list.h"
#include "Network/packets/sensor_request.h"
#include "INCLUDE5.H"

//ERIS CODE INSERTED HERE
#include "DMCCom.h"
extern "C" {
#include "mio_io.h"
}
// #include "imu.h"		// Our Pololu CHR-6d is dead and the part has been discontinued

SensorManager *SensorManager::s_instance = NULL;

/*
 * Creates the SensorManager object.
 * Nothing is requested of the sensor board at this point.
 */
SensorManager::SensorManager()
{
	sensors = NULL;
	num_sensors = 0;

	i2c_bus = NULL;

	logging_enabled = 0;
	log_file = NULL;
	memset(logging_folder, 0, MAX_LOG_PATH_LEN);

	// Eris/x86 doesn't have any special initialization routines to use
	// the PCM-MIO-G sensor board.
}

/*
 * Frees any resources used by the sensor manager.
 */
SensorManager::~SensorManager()
{
	logging_enabled = 0;
	if (log_file != NULL)
	{
		fclose(log_file);
		log_file = NULL;
	}

	if (sensors != NULL)
	{
		free(sensors);
		sensors = NULL;
	}
	num_sensors = 0;

	/* This was only done for KOREBOT code, but it may be needed for Eris at some point?
	if (i2c_bus != NULL)
	{
		// Close our connection to the I2C bus
		i2c_close(i2c_bus);
		free(i2c_bus);
	}
	*/
}

/*
 * Initializes the SensorManager.  Each available sensor defined
 * in the config file will be acquired, so that the data can be
 * read later on.
 *
 * @returns 1 on success, 0 otherwise
 */
int SensorManager::init()
{
	int i;
	char path[32];
	char bus[32];
	char motor_names[4][8] = {"Front", "Rear", "Left", "Right"};
	char sensor_names[NUM_SENSORS_PER_MOTOR][16] = {"Speed", "Position", "Current", "PWM"};

	Configuration::instance()->getValue("korebot.i2c_bus_device", bus, 32, "/dev/null");
	Configuration::instance()->getValue("korebot.sensor_log_folder", logging_folder, MAX_LOG_PATH_LEN, "/dev/null");

	i2c_bus = (i2c_t *)malloc(sizeof(i2c_t));
	memset(i2c_bus, 0, sizeof(i2c_t));

	//Eris Code Inserted Here
	// Fire up the PCM-MIO-G, it's really simple.
	/*
	 * adc_set_channel_mode - Set Channel input mode and range
	 *		int adc_set_channel_mode(
	 *			int dev_num,
	 *			int channel,
	 *			int input_mode,
	 *			int duplex,
	 *			int range)
	 *	Arguments:
	 *		dev_num - The device to be accessed (0-3)
	 *		channel - The channel number to set (0-15)
	 *		input_mode - ADC_SINGLE_ENDED, ADC_DIFFERENTIAL
	 *		duplex - The swing of the input voltage	ADC_UNIPOLAR, ADC_BIPOLAR
	 *	range - The input voltage top end	ADC_TOP_5V,	ADC_TOP_10V
	 *	Return
	 *		0 = Function completed successfully.
	 *		1 = Error occurred. Check mio_error_code.
	 */

	// I don't know what you'll be sensing with your robots, so I'll just set
	// the whole board for 0-5V, single ended operation.

	int channel;
	printf( "Initializing PCM-MIO-G Channel: ");
	for (channel = 0; channel < 16; channel++)
	{
		if (adc_set_channel_mode(0, channel, ADC_SINGLE_ENDED, ADC_UNIPOLAR, ADC_TOP_5V) == 0)
		{
			printf("%d ", channel);
		}
		else
		{
			printf("\nADC Configuration failed for channel %d\nerror code: %d\n", channel, mio_error_code);
			num_sensors -= 9;	// This was in the original IMU serial code, I suppose it makes sense to
			// reduce the number of sensors if they won't be available.
			return 0;			// Eris didn't have this before, I'm adding it as per the Korebot code above
		}
	}

	if (channel == 16)
	{
		printf("\nAll channels completed!\n");
	}

	num_sensors = Configuration::instance()->getListSize("sensors") + NUM_SPECIAL_SENSORS;

	if (num_sensors == 0)
	{
		fprintf(stderr, "Warning: No sensors available.\n");
		return 1;
	}

	// Create our array and initialize it to all 0's
	sensors = (struct sensor_info_t *)malloc(sizeof(struct sensor_info_t) * num_sensors);
	memset(sensors, 0, sizeof(struct sensor_info_t) * num_sensors);
	//MOTOR SENSORS ARE CAPTURED HERE
	for (i = 0; i < 4; i++)
	{
		for (int j = 0; j < NUM_SENSORS_PER_MOTOR; j++)
		{
			sensors[i * NUM_SENSORS_PER_MOTOR + j].id = -i * NUM_SENSORS_PER_MOTOR - j - 1;
			sensors[i * NUM_SENSORS_PER_MOTOR + j].port = -1;
			snprintf(sensors[i * NUM_SENSORS_PER_MOTOR + j].name, MAX_SENSOR_NAME_LEN, "%s Motor %s", motor_names[i], sensor_names[j]);
		}
	}
	// ALL other sensors starting after the motor sensors
	for (i = NUM_SPECIAL_SENSORS; i < num_sensors; i++)
	{
		sprintf(path, "sensors.[%d].id", i - NUM_SPECIAL_SENSORS);
		sensors[i].id = Configuration::instance()->getValue(path, (int)0);

		sprintf(path, "sensors.[%d].name", i - NUM_SPECIAL_SENSORS);
		Configuration::instance()->getValue(path, sensors[i].name, MAX_SENSOR_NAME_LEN, "Invalid Sensor");

		sprintf(path, "sensors.[%d].port", i - NUM_SPECIAL_SENSORS);
		sensors[i].port = Configuration::instance()->getValue(path, (int)0);
	}
	printf("NUMBER OF SENSORS = %d\n", num_sensors);
	return 1;
}

/*
 * Shuts the sensor manager down, destroying any resources & releasing
 * any devices the manager is holding.
 */
void SensorManager::shutdown()
{
	if (s_instance != NULL)
	{
		delete s_instance;
		s_instance = NULL;
	}
}

/*
 * Runs one iteration of the sensor manager; Reads
 * the sensor manager data from the sensors.
 *
 * @param dt The time difference since the last call to this method
 */
void SensorManager::run(float dt)
{
	PROFILE("SensorManager::run");
	SensorData data_packet;

	unsigned char i;
	long value;
	float timestamp;

	timestamp = ProfileSample::getTime();

	readMotorEncoders();


	if (logging_enabled && log_file != NULL)
	{
		fprintf(log_file, "%f", timestamp);
	}

	// Big, burly robots read all of their sensors at once!
	// The PCM-MIO-G can trigger all 16 channels at the same time. It's much, much
	// faster than querying each channel individually.
	unsigned short conversionBuffer[16];
	adc_convert_all_channels(0, conversionBuffer);
	//if(mio_error_code)
	//{
	//	printf("PCM-MIO-G Error: %s\n",mio_error_string);
	//}
	// Blammo! Wasn't that great?

	for (i = 0; i < num_sensors; i++)
	{

		memcpy(sensors[i].values, &sensors[i].values[1], sizeof(long) * (SENSOR_HISTORY - 1));
		if (sensors[i].id >= 0)
		{
			// All 16 channels of the PCM-MIO-G have been read
			// Just take the next one in the array...

			if (!mio_error_code)
			{
				value = (long)conversionBuffer[i - 16];
				//printf("loop: %d\nconvBuf:%d\nval:%d\n", i, conversionBuffer[i], value);
			}
			else
			{
				//printf("no sensor data was collected\n");
			}
		}
		else
		{

			switch (sensors[i].id)
			{
				case SENSOR_ID_FRONT_MOTOR_SPEED:
				case SENSOR_ID_RIGHT_MOTOR_SPEED:
				case SENSOR_ID_LEFT_MOTOR_SPEED:
				case SENSOR_ID_REAR_MOTOR_SPEED:
					value = encoder_values[(sensors[i].id + 1) / -NUM_SENSORS_PER_MOTOR].speed;
					break;
				case SENSOR_ID_FRONT_MOTOR_POSITION:
				case SENSOR_ID_RIGHT_MOTOR_POSITION:
				case SENSOR_ID_LEFT_MOTOR_POSITION:
				case SENSOR_ID_REAR_MOTOR_POSITION:
					value = encoder_values[(sensors[i].id + 2) / -NUM_SENSORS_PER_MOTOR].position;
					break;
				case SENSOR_ID_FRONT_MOTOR_CURRENT:
				case SENSOR_ID_RIGHT_MOTOR_CURRENT:
				case SENSOR_ID_LEFT_MOTOR_CURRENT:
				case SENSOR_ID_REAR_MOTOR_CURRENT:
					value = encoder_values[(sensors[i].id + 3) / -NUM_SENSORS_PER_MOTOR].current;
					break;
				case SENSOR_ID_FRONT_MOTOR_PWM:
				case SENSOR_ID_RIGHT_MOTOR_PWM:
				case SENSOR_ID_LEFT_MOTOR_PWM:
				case SENSOR_ID_REAR_MOTOR_PWM:
					value = pwm_values[(sensors[i].id + 4) / -NUM_SENSORS_PER_MOTOR];
					break;
				default:
					value = -1;
					break;
			}
		}

		sensors[i].values[SENSOR_HISTORY - 1] = value;
		sensors[i].timestamp = timestamp;

		if (sensors[i].sending_data)
		{
			data_packet.addSensorValue(sensors[i].id, timestamp, value);
		}

		if (logging_enabled && log_file != NULL)
		{
			fprintf(log_file, ",%d", value);
		}
	}

	if (data_packet.getNumSensors())
	{
		Korebot::instance()->getControlServerConnection()->transmit(&data_packet);
	}

	if (logging_enabled && log_file != NULL)
	{
		fprintf(log_file, "\n");
	}
}

/*
 * Processes a packet received over the network
 *
 * @param packet The packet received from the network
 */
void SensorManager::processPacket(SensorControl *packet)
{
	unsigned char i;
	if (packet->getCommand() == SENSOR_CONTROL_SENSOR_REQUEST)
	{
		SensorRequest *request = (SensorRequest *)packet;

		// The client wants us to send sensor data, so find the sensor
		// they want us to send data to and set the flag
		for (i = 0; i < num_sensors; i++)
		{
			if (sensors[i].id == request->getSensorID())
			{
				sensors[i].sending_data = request->getShouldSendSensorData();
			}
		}
	}
}


/*
 * Reads the motor encoder data for the motors
 */
void SensorManager::readMotorEncoders()
{
	MotorController *instance = MotorController::instance();
	udword udtmp;
	uword utmp;
	read_long(instance->motors.front.dev_desc, encp, &udtmp);
	encoder_values[MOTOR_FRONT].position = (long) udtmp;
	read_long(instance->motors.left.dev_desc, encp, &(udtmp));
	encoder_values[MOTOR_LEFT].position = (long) udtmp;
	read_long(instance->motors.right.dev_desc, encp, &(udtmp));
	encoder_values[MOTOR_RIGHT].position = (long) udtmp;
	read_long(instance->motors.rear.dev_desc, encp, &(udtmp));
	encoder_values[MOTOR_REAR].position = (long) udtmp;


	read_word(instance->motors.front.dev_desc, actvel, &(utmp));
	encoder_values[MOTOR_FRONT].speed = (short) utmp;
	read_word(instance->motors.left.dev_desc, actvel, &(utmp));
	encoder_values[MOTOR_LEFT].speed = (short) utmp;
	read_word(instance->motors.right.dev_desc, actvel, &(utmp));
	encoder_values[MOTOR_RIGHT].speed = (short) utmp;
	read_word(instance->motors.rear.dev_desc, actvel, &(utmp));
	encoder_values[MOTOR_REAR].speed = (short) utmp;

	read_word(instance->motors.front.dev_desc, pwm, &(utmp));
	pwm_values[MOTOR_FRONT] = (short)utmp;
	read_word(instance->motors.left.dev_desc, pwm, &(utmp));
	pwm_values[MOTOR_LEFT] = (short)utmp;
	read_word(instance->motors.right.dev_desc, pwm, &(utmp));
	pwm_values[MOTOR_RIGHT] = (short)utmp;
	read_word(instance->motors.rear.dev_desc, pwm, &(utmp));
	pwm_values[MOTOR_REAR] = (short)utmp;
}

/*
 * Reads the motor encoder data for one particular motor
 *
 * @param device A pointer to the device of the motor
 * @param out Where to store the data read in
 */

void SensorManager::readMotorEncoders(knet_dev_t *device, struct motor_encoder_info_t &out)
{
	if (device != NULL)
	{
		printf("Eris: Function depricated.\n");
		out.speed = 0;
		out.position = 0;
		out.current = 0;
	}
	else
	{
		out.speed = -1;
		out.position = -1;
		out.current = -1;
	}
}

/*
 * Gets a history of sensor values for the sensor with the given ID
 *
 * @param sensor_id The ID of the sensor to get
 *
 * @returns An array of sensor values, with index 0 the farthest in the past and index
 *			SENSOR_HISTORY - 1 the latest value
 */
long *SensorManager::getSensorValues(int sensor_id)
{
	unsigned char i;

	for (i = 0; i < num_sensors; i++)
	{
		if (sensors[i].id == sensor_id)
		{
			return sensors[i].values;
		}
	}
	return NULL;
}

/*
 * Sends status information about the sensor manager to the control server.
 *
 * This includes sensors available on this korebot.
 */
void SensorManager::sendConfig()
{
	SensorList list(num_sensors, sensors);
	Korebot::instance()->getControlServerConnection()->transmit(&list);

	ConfigurationPacket config(CONFIGURATION_OPTION_SENSOR_LOGGING, logging_enabled);
	Korebot::instance()->getControlServerConnection()->transmit(&config);
}

/*
 * Prints the status of various modules on the Korebot
 */
void SensorManager::printStatus()
{
	int i;
	printf("SENSOR MANAGER STATUS:\n");
	printf("Sensor Logging:\t\t\t\t\t%s\n", logging_enabled ? "Enabled" : "Disabled");
	printf("Sensor History:\t\t\t\t\t%d\n", SENSOR_HISTORY);
	printf("List of Available Sensors (ID: Name):\n");
	for (i = 0; i < num_sensors; i++)
	{
		printf("\t%d:\t%s\n", sensors[i].id, sensors[i].name);
	}

	printf("\n");
}

/*
 * Gets if the sensors are logging data to file
 *
 * @returns 1, if the sensors are logging to file, 0 otherwise
 */
int SensorManager::getLoggingEnabled()
{
	return logging_enabled;
}

/*
 * Sets if the sensors are logging data to file
 *
 * @param enableLogging 1, if logging should be enabled, 0 otherwise
 */
void SensorManager::setLoggingEnabled(int enableLogging)
{
	struct timeval cur_time;
	struct tm *loc_time;
	char filename[MAX_LOG_PATH_LEN];
	int i;

	// Truncate the value to 0 or 1
	enableLogging = enableLogging & 1;

	logging_enabled = enableLogging;
	if (logging_enabled)
	{
		if (log_file != NULL)
		{
			fclose(log_file);
		}
		gettimeofday(&cur_time, NULL);
		loc_time = localtime(&(cur_time.tv_sec));

		snprintf(filename, MAX_LOG_PATH_LEN, "%s/log_%s_%d-%d-%d_%d:%d:%d.txt", logging_folder, Korebot::instance()->getRobotName(), (loc_time->tm_year + 1900), loc_time->tm_mon + 1, loc_time->tm_mday, loc_time->tm_hour, loc_time->tm_min, loc_time->tm_sec);
		log_file = fopen(filename, "w");

		if (log_file == NULL)
		{
			perror("Could not open log file for writing");
			logging_enabled = 0;
		}
		else
		{
			// Add a header describing the fields
			fprintf(log_file, "Timestamp");
			for (i = 0; i < num_sensors; i++)
			{
				fprintf(log_file, ",%s", sensors[i].name);
			}
			fprintf(log_file, "\n");
		}

	}
	else if (log_file != NULL)
	{
		fclose(log_file);
		log_file = NULL;
	}
}

/*
 * Writes an 8-bit value to the I2C device dev, register reg
 *
 * @param i2c A pointer to an open I2C bus connection
 * @param dev The device on the I2C bus to write to
 * @param reg The register on the device to write to
 * @param val The value to write to the register
 *
 * @returns The number of bytes written, or < 0 if an error ocurred
 */
int SensorManager::i2c_write8(i2c_dev_t dev, unsigned char reg, unsigned char val)
{
	if (i2c_bus == NULL)
	{
		return -1;
	}

	return 1;
}

/*
 * Reads a 32-bit value from the I2C device dev, register reg, into val
 *
 * @param dev The device on the I2C bus to write to
 * @param reg The register on the device to read from
 * @param val A pointer to a long variable where the value will be read into
 *
 * @returns 0 if successful, -1 otherwise
 */
int SensorManager::i2c_read32(i2c_dev_t dev, char reg, long &time, short &value)
{
	char buff[6];
	char *ptr;

	memset(buff, 0, 6);

	/* Read the value from the ADC */
	ptr = (char *)&value;
	ptr[0] = buff[1];
	ptr[1] = buff[0];

	/* Read the timestamp on the ADC */
	ptr = (char *)&time;
	ptr[0] = buff[5];
	ptr[1] = buff[4];
	ptr[2] = buff[3];
	ptr[3] = buff[2];

	return 0;
}
