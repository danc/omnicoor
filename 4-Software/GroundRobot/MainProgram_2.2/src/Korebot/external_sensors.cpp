#include "external_sensors.h"

#include <cstring>

#include "external_sensors_common.h"
#include "Network/packets/profiler_packet.h"
#include "Network/packets/xbox_sensor.h"

ExternalSensors *ExternalSensors::s_instance = NULL;

/*
 * Creates the ExternalSensors object.
 * Nothing is requested of the sensor board at this ponint.
 */
ExternalSensors::ExternalSensors() {
	memset(&xbox_data, 0, sizeof(xbox_controller_t) * SENSOR_HISTORY);
}

/*
 * Frees any resources used by the sensor manager.
 */
ExternalSensors::~ExternalSensors() {
}

/*
 * Initializes the ExternalSensors.  Each available sensor defined
 * in the config file will be acquired, so that the data can be
 * read later on.
 *
 * @returns 1 on success, 0 otherwise
 */
int ExternalSensors::init() {
	return 1;
}

/*
 * Shuts the sensor manager down, destroying any resources & releasing
 * any devices the manager is holding.
 */
void ExternalSensors::shutdown() {
	if (s_instance != NULL) {
		delete s_instance;
		s_instance = NULL;
	}
}

/*
 * Runs one iteration of the sensor manager; Reads
 * the sensor manager data from the sensors.
 * 
 * @param dt The time difference since the last call to this method
 */
void ExternalSensors::run(float dt) {
	PROFILE("ExternalSensors::run");
	// Nothing to do here
}

/*
 * Processes a packet received over the network
 *
 * @param packet The packet received from the network
 */
void ExternalSensors::processPacket(BasePacket *packet) {
	int i;
	if (packet->getOpcode() == OPCODE_XBOX_SENSOR) {
		for (i=0; i<SENSOR_HISTORY-1; i++) {
			memcpy(&xbox_data[i], &xbox_data[i+1], sizeof(xbox_controller_t));
		}

		memcpy(&xbox_data[i], ((XboxSensor*)packet)->getData(), sizeof(xbox_controller_t));
	}
}

/*
 * Sends status information about the sensor manager to the control server.
 *
 * This includes sensors available on this korebot.
 */
void ExternalSensors::sendConfig() {
	// Nothing to do here
}

/*
 * Prints the status of various modules on the Korebot
 */
void ExternalSensors::printStatus() {
	// Nothing to do here
}

/*
 * Gets the history of XBox controller values
 *
 * @returns An array of sensor values, with index 0 the farthest in the past and index
 * 			SENSOR_HISTORY - 1 the latest value
 */
xbox_controller_t *ExternalSensors::getXboxControllerValues() {
	return xbox_data;
}
