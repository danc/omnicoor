#include "peer_motorspeed.h"

#include <cstring>

#include "Network/network_bitstream.h"

/*
 * Creates a PeerMotorSpeed packet by default
 */
PeerMotorSpeed::PeerMotorSpeed() : BasePacket(OPCODE_PEER_MOTOR_SPEED) {
	id = -1;
	memset(&motor_speeds, 0, sizeof(motor_speeds_t));
}

/*
 * Creates a PeerMotorSpeed packet with robot id 
 */
PeerMotorSpeed::PeerMotorSpeed(int new_id) : BasePacket(OPCODE_PEER_MOTOR_SPEED) {
	id = new_id;
	memset(&motor_speeds, 0, sizeof(motor_speeds_t));
}

/*
 * Creates a PeerMotorSpeed packet with robot id and motor speeds
 */
PeerMotorSpeed::PeerMotorSpeed(int new_id, motor_speeds_t new_motor_speeds){
	id = new_id;
	motor_speeds = new_motor_speeds;
}

/*
 * Creates a copy of a PeerMotorSpeed packet
 *
 * @param copy The packet to copy
 */
PeerMotorSpeed::PeerMotorSpeed(PeerMotorSpeed *copy) : BasePacket(OPCODE_PEER_MOTOR_SPEED) {
	id = copy->id;
	motor_speeds = copy->motor_speeds;
}

/*
 * Frees any resources used by this packet
 */
PeerMotorSpeed::~PeerMotorSpeed() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *PeerMotorSpeed::read(NetworkBitstream &in) {
	in>>id;
	in>>total_time;
	in>>motor_speeds.front;
	in>>motor_speeds.rear;
	in>>motor_speeds.left;
	in>>motor_speeds.right;
	

	return new PeerMotorSpeed(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void PeerMotorSpeed::write(NetworkBitstream &out) const {
	BasePacket::write(out);
	out<<id;
	out<<total_time;
	out<<motor_speeds.front;
	out<<motor_speeds.rear;
	out<<motor_speeds.left;
	out<<motor_speeds.right;
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the motor speeds
 */
float PeerMotorSpeed::getmotorspeeds(motor_speeds_t *empty_motor_speeds) const {
	
	memcpy(empty_motor_speeds, &motor_speeds,sizeof(motor_speeds_t));
	return total_time;

}

/*
 * Gets the robot id
 */
int PeerMotorSpeed::getrobotid() const {
	return id;
}

/*
 * Sets the robot id and the motor speeds
 */
void PeerMotorSpeed::setmotorspeeds(int new_id, motor_speeds_t new_motor_speeds, float new_total_time) {
	id = new_id;
	motor_speeds = new_motor_speeds;
	total_time = new_total_time;
}








