#include "motor_control.h"

#include "motor_control_manual.h"
#include "motor_control_speed.h"
#include "Network/network_bitstream.h"

/*
 * Creates a manual motor control by default
 */
MotorControl::MotorControl() : BasePacket(OPCODE_MOTOR) {
	this->command = MOTOR_CONTROL_MODE_FREE;
}

/*
 * Creates a manual motor control by default
 */
MotorControl::MotorControl(char command) : BasePacket(OPCODE_MOTOR) {
	this->command = command;
}

/*
 * Creates a copy of a MotorControl packet
 *
 * @param copy The packet to copy
 */
MotorControl::MotorControl(MotorControl *copy) : BasePacket(OPCODE_MOTOR) {
	this->command = copy->command;
}

/*
 * Frees any resources used by this packet
 */
MotorControl::~MotorControl() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *MotorControl::read(NetworkBitstream &in) {
	MotorControlManual manual_control;
	MotorControlSpeed speed_control;

	in>>command;

	switch(command) {
		case MOTOR_CONTROL_MODE_MANUAL:
			return manual_control.read(in);
		case MOTOR_CONTROL_MODE_SPEED:
			return speed_control.read(in);
	}

	return new MotorControl(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void MotorControl::write(NetworkBitstream &out) const {
	BasePacket::write(out);
	out<<command;
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the command of the motor control
 */
char MotorControl::getCommand() const {
	return command;
}

/*
 * Sets the command of motor control
 */
void MotorControl::setCommand(char newCommand) {
	command = newCommand;
}
