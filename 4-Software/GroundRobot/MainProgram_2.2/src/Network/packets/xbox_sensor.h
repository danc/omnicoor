#ifndef _NETWORK__PACKETS__XBOX_SENSOR_H_
#define _NETWORK__PACKETS__XBOX_SENSOR_H_

#include "base_packet.h"
#include "Korebot/external_sensors_common.h"

class NetworkBitstream;

class XboxSensor : public BasePacket {
	private:
		xbox_controller_t data;

	public:
		/*
		 * Creates a sensor data packet
		 */
		XboxSensor();

		/*
		 * Creates a sensor data packet
		 */
		XboxSensor(const xbox_controller_t &state);

		/*
		 * Creates a copy of a XboxSensor packet
		 *
		 * @param copy The packet to copy
		 */
		XboxSensor(XboxSensor *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~XboxSensor();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;
		
		/*
		 * Gets the state of the controller
		 *
		 * @returns A pointer to a structure containing the current state of the controller
		 */
		const xbox_controller_t *getData();
};

#endif	// _NETWORK__PACKETS__XBOX_SENSOR_H_
