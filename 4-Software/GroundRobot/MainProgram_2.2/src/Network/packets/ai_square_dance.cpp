#include "ai_square_dance.h"

#include "Network/network_bitstream.h"

/*
 * Creates a manual motor control by default
 */
SquareDance::SquareDance() : BasePacket(OPCODE_AI_SQUARE_DANCE) {
	corner = 1;
}

/*
 * Creates a manual motor control by default
 */
SquareDance::SquareDance(char newCorner) : BasePacket(OPCODE_AI_SQUARE_DANCE) {
	corner = newCorner;
}

/*
 * Creates a copy of a SquareDance packet
 *
 * @param copy The packet to copy
 */
SquareDance::SquareDance(SquareDance *copy) : BasePacket(OPCODE_AI_SQUARE_DANCE) {
	corner = copy->corner;
}

/*
 * Frees any resources used by this packet
 */
SquareDance::~SquareDance() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *SquareDance::read(NetworkBitstream &in) {
	in>>corner;

	return new SquareDance(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void SquareDance::write(NetworkBitstream &out) const {
	BasePacket::write(out);
	out<<corner;
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the corner the robot is at
 */
char SquareDance::getCorner() const {
	return corner;
}

/*
 * Sets the corner the robot is at
 */
void SquareDance::setCorner(char newCorner) {
	corner = newCorner;
}
