#ifndef _NETWORK__TCP_CONNECTION_H_
#define _NETWORK__TCP_CONNECTION_H_

#include <pthread.h>

#include "network.h"

#include "network_bitstream.h"

class BasePacket;
class PacketHandler;

class TCP_Connection {
	private:
		// The configuration of the network
		network_config_t config;

		// 1 if we are connected to the server, 0 otherwise
		int we_are_connected;

		// The file descriptor of the socket we create
		int sockfd;

		// 1 when the connection should shut itself down.  0 otherwise.
		int shutdown_ready;

		// 1 if the connection handler thread is running, 0 otherwise
		int connection_handler_running;

		// The ID of the robot this connection is connected to
		unsigned char robot_id;

		// Store the TCP connection thread handler somewhere
		pthread_t connection_thread;

		// Handles storing of network data for the program to read in
		NetworkBitstream bitstream;

		// Mutex to control a lock on the bitstream
		pthread_mutex_t bitstreamMutex;

		// The object that handles incoming packets
		PacketHandler *packetHandler;

		// 1 if we should use a thread to handle incoming packets, 0 if
		// the creator will call a method to handle packets themselves
		int use_packet_handler_thread;

		// 1 when we are first launching the connection handler; Once
		// the handler thread has started, this becomes false
		int connection_handler_is_starting;
	public:
		/*
		 * Creates a new object that represents a TCP connection.
		 */
		TCP_Connection();

		/*
		 * Creates a new object that represents a TCP connection,
		 * which passes received packets to the given packet handler.
		 *
		 * @param usePacketHandlerThread 1 if we should use a thread to
		 * 			handle incoming packets, or 0 if the creator will
		 * 			call run() to handle packets themelves
		 * @param handler The object to handler received packets
		 */
		TCP_Connection(int usePacketHandlerThread, PacketHandler *handler);

		/*
		 * Stops the active connection and frees any memory this
		 * object is using.
		 */
		~TCP_Connection();

		/*
		 * Attempts to connect to the server on the port given in the config
		 * structure passed in.  If successful, a thread is spawned to handle
		 * the connection.
		 *
		 * @param config The network configuration
		 *
		 * @returns 1 on success, <= 0 otherwise
		 */
		int start(const network_config_t &config);

		/*
		 * Attempts to close an active connection
		 *
		 * @returns void
		 */
		void stop();

		/*
		 * Sends a packet to the connected client
		 *
		 * @param packet The packet to send
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int transmit(BasePacket *packet);

		/*
		 * Checks for any incoming packets, and sends them to
		 * the packet handler.  This should not be called
		 * if use_packet_handler_thread is true.
		 *
		 * @returns The number of packets received, or -1 if there is a problem with the socket.
		 * 			If this function returns -1, the connection is no longer
		 * 			valid and the connection should be stopped.
		 */
		int run();

		/////////////////////////////////////////////////////
		//                GETTERS / SETTERS                //
		/////////////////////////////////////////////////////

		/*
		 * Returns 1 if the client is connected to the server and processing
		 *				packets.  Returns 0 otherwise
		 *
		 * @returns (See description)
		 */
		int isConnected();

		/*
		 * Gets the address of the machine we are connected to.
		 *
		 * @param config The network configuration will be copied into
		 *			this parameter
		 *
		 * @returns void
		 */
		void getConfiguration(network_config_t &config);

		/*
		 * Gets the object that handles received packets
		 *
		 * @returns The object that handles received packets
		 */
		PacketHandler *getPacketHandler();

		/*
		 * Sets the object that handles received packets
		 *
		 * @param newHandler The new object to handle received packets
		 */
		void setPacketHandler(PacketHandler *newHandler);

		/*
		 * Gets the ID of the robot this connection is connected to
		 *
		 * @returns The ID of the robot
		 */
		unsigned char getRobotID();

		/*
		 * Sets the ID of the robot this connection is connected to
		 *
		 * @param newRobotID The new robot ID
		 */
		void setRobotID(unsigned char newRobotID);

		/*
		 * Gets if this connection uses a separate thread for processing
		 * incoming packets, or if the creator calls a method to
		 * do the processing.
		 *
		 * @returns 1, if the connection uses a thread, 0 otherwise
		 */
		int getUsePacketHandlerThread();

		/*
		 * Sets if this connection should use a packet handler thread or
		 * if some other part of the program will tell the connection when
		 * to listen for packets.  If the connection was already using a
		 * thread to process packets, and this method is called with a value
		 * of 0, the thread will be stopped before this method returns.
		 *
		 * If the thread had not been created, it will be created.
		 *
		 * @param usePacketHandlerThread 1 if we should use a thread to
		 * 			handle incoming packets, or 0 if the creator will
		 * 			call run() to handle packets themelves
		 */
		void setUsePacketHandlerThread(int usePacketHandlerThread);

	private:
		/*
		 * Initializes the object with default values.
		 *
		 * @param usePacketHandlerThread 1 if we should use a thread to
		 * 			handle incoming packets, or 0 if the creator will
		 * 			call run() to handle packets themelves
		 * @param handler The object that will handle received packets
		 */
		void init(int usePacketHandlerThread, PacketHandler *handler);

		/*
		 * Destroys the socket
		 */
		void destroySocket();

		/*
		 * Manages a TCP connection with the server.
		 * Receives packets & passes them to the packet handler.
		 */
		void connectionThread();

		/*
		 * Launches the connection thread
		 */
		int launchConnectionThread();

	friend class TCP_Server;
	friend void *tcpConnectionThreadProxy(void *data);
};

#endif	// _NETWORK__TCP_CONNECTION_H_
