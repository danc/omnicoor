
#ifndef _KOREBOT__LOGGER_H_
#define _KOREBOT__LOGGER_H_

#include <sstream>
#include <fstream>
#include "motor_controller.h"

class Logger
{
private:
    static Logger *_instance;

    /**
     * Creates a default Logger object. It is private to prevent
     * someone from destroying the current instance.
     */
    Logger();

public:
    /**
     * Gets the singleton logger obect
     *
     * @returns The logger object
     */
    static Logger *instance()
    {
        if (!_instance)
        {
            _instance = new Logger();
        }
        return _instance;
    }

    ~Logger();

    /**
     * Initializes the data logging by creating the log files.
     * This is called during initialization.
     */
    bool initialize();

    /**
     * Closes the log file.     */
    void stop();

    /**
     * Writes a line to the log file.
     * This is called whenever we need to log something into the file
     */
    void logData(); // TODO: Add an argument

private:
    std::fstream      logFile;  // log file object

    bool isInitialized; // True when the initialized function has been called

    bool logPitch;  // Logs pitch, error, pid values, pid constants, and set point
    bool logRoll;   // Logs roll, error, pid values, pid constants, and set point
    bool logEris;   // Logs the time, position, and orientation of Eris
    bool logLocal;
    bool logMotors;
    bool logVelCtrl;
    bool logLQI;
    bool logSSctrl;
    bool logPacket;
    
    QUAD* quad;
    ERIS* eris;
    motortrain_t* motors;
    /**
     * Writes the pid constants to the top of the log file.
     * Writes the column titles and units to the top of the log file.
     */
    void writeHeader();

};

#endif  // _KOREBOT__LOGGER_H_
