#ifndef _KOREBOT__KOREBOT_AI_H_
#define _KOREBOT__KOREBOT_AI_H_

class BasePacket;
class Configuration;
class SensorManager;
class LocalizationSystem;
class MotorController;
class ExternalSensors;
class Korebot;

class KorebotAI {
	public:
		virtual ~KorebotAI() {}

		/* 
		 * Get a uniqueue ID for this AI to differentiate it from the rest
		 * of the AI
		 *
		 * @returns A uniqueue ID for this AI
		 */
		virtual int getUniqueID()=0;

		/*
		 * Gets a human-readable name for what this AI does.
		 *
		 * @returns A string that contains the name of the AI
		 */
		virtual const char *getName()=0;

		/*
		 * Initializes the Korebot AI.
		 *
		 * @param configuration An object that contains the configuration for the program
		 * @param sensors A pointer to the sensor manager
		 * @param localization_system The localization system with information about positions of objects / robots
		 * @param motor_controller The motor controller
		 * @parma korebot A class with generic information about this Korebot (Including network connections)
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		virtual int init(Configuration *configuration, SensorManager *sensors, LocalizationSystem *localization_system,
				MotorController *motor_controller, ExternalSensors *external_sensors, Korebot *korebot);

		/*
		 * Called before the AI is about to start.  Any initialization of the
		 * motor controller, korebot, sensors, or localization should happen now
		 */
		virtual void onStart()=0;

		/*
		 * Called before the AI is about to stop executing.
		 */
		virtual void onStop()=0;

		/*
		 * Called each run of the main loop.  Motor speeds should be updated
		 * in this method.
		 *
		 * @param time The difference in time since the last call to this method
		 */
		virtual void run(float time)=0;

		/*
		 * Called when a packet is received over the network that 
		 * is destined for the currently-running AI
		 *
		 * @param packet The packet that was received.
		 */
		virtual void processPacket(BasePacket *packet)=0;

		/*******************************
		 *      GETTERS / SETTERS      *
		 ******************************/

};


/**************************************
 * 		LIBRARY CALLBACK METHODS      *
 *************************************/
#ifdef __cplusplus
extern "C" {
#endif

typedef KorebotAI *create_ai_t();
typedef void destroy_ai_t(KorebotAI *);

/*
 * Creates an instance of a KorebotAI class to control the Korebot
 */
KorebotAI *createAI();

/*
 * Destroys an instance of the KorebotAI class
 */
void destroyAI(KorebotAI *);

#ifdef __cplusplus
}
#endif

#endif	// _KOREBOT__KOREBOT_AI_H_
