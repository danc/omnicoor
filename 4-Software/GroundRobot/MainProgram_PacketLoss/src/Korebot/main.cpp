//#define ssTracking
//#define ssDrop
//#define drop10
//#define drop30
//#define drop50
//#define mot50
//#define RADAlqi
//#define RADAlqi_est
#define rada_lqg
//#define rada_split_lqg
//#define rada_split_lqg_ol
//#define yaw_off
//#define H2ctrl
//#define ssMotors
//#define accR
//#define posRamp
#include <iomanip>
#include <iostream>
#include <string>

#include <cstdlib>
#include <cstdio>
#include <pthread.h>
#include <quat.h>
#include <signal.h>
#include <ctime>

#include "configuration.h"
#include "korebot.h"
#include "logger.h"
#include "motor_controller.h"
#include "utils.h"
#include "vrpn.h"
#include "key_bindings.h"
#include "sensor_manager.h"
#include "sensor_manager_common.h"
#include <math.h> 

using namespace std;


/********** defines for pwm values *************/
#define DUTY_CYCLE_MIN          26000
#define DUTY_CYCLE_MAX          56000
#define MAX_ERIS_DUTY_CYCLE     65535
#define DUTY_CYCLE_MID          ((DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 2) + DUTY_CYCLE_MIN
#define DUTY_CYCLE_HALF_RANGE   (DUTY_CYCLE_MAX - DUTY_CYCLE_MIN) / 2
#define WHEEL_DEAD_ZONE         6656 //35000
#define WHEEL_OL_SPEED          26880
/***********************************************/

/********** function prototypes ****************/
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_eris(void*, const vrpn_TRACKERCB t);
void *mainThread(void *threadID);
void *controlLoop(void *threadID);
void PID_controller(vrpn_data_t *vrpnData);
void PID_eris(vrpn_data_t *vrpnData);
void tuning_UI(vrpn_data_t *vrpnData);
void robotControl_UI(vrpn_data_t *vrpnData);
void robotControlMode();
void tuningMode();
void boxStepMode(double curTime);
bool loadConfigFile();
void *UIThread(void *threadID);
static void ctrlc_handler(int sig);
void RADA_lqi(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris);
void RADA_SSctrl(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris);
void RADA_SSctrl_loss(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris);
void motors50_test();
void RADA_lqi_est(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris);
void RADA_lqg(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris);
void quad_lqg(vrpn_data_t *vrpnDataPen);
void eris_lqg(vrpn_data_t *vrpnDataEris);
void PID_eris_ol(vrpn_data_t *vrpnData);
/***********************************************/

/********** Enum declarations ******************/
enum Axis {ROLL, PITCH};                    // For the axis angle (pitch or roll)
enum Mode {STOPPED, TUNING, BOX_STEP, ROBOT_CONTROL};// For the current mode we are executing in
/***********************************************/

/********** General variables ******************/
extern int app_shutdown;                // When 1, the ground robot is supposed to shut down
pthread_mutex_t mutex; // Mutex for the threads
pthread_mutex_t mutex_vrpn;
static char cmd;                        // Used in the UI thread for taking input
const string CLEAR = "\033[H\033[J";    // When sent to cout, this clears the terminal screen
static double startTime = 0;            // For tracking the start time of the program
static Mode currentMode = STOPPED;      // For keeping track of the current running mode
static Mode controlMode = STOPPED;
static FILE *randomData;
static unsigned char myRandomData[2];
static size_t randomDataLen;
static ssize_t result;

#ifdef drop10
static unsigned char ErisDrop=26; //out of 255
static unsigned char PenDrop=26;
#else
#ifdef drop30
static unsigned char ErisDrop=77; //out of 255
static unsigned char PenDrop=77;
#else
#ifdef drop50
static unsigned char ErisDrop=128; //out of 255
static unsigned char PenDrop=128;
#else
static unsigned char ErisDrop=0; //out of 255
static unsigned char PenDrop=0;
#endif
#endif
#endif



/***********************************************/

/********** TUNING mode ************************/
Axis cur_axis = PITCH;   // The current axis that is being modified (for tuning mode)
/***********************************************/

/********** BOX_STEP mode **********************/
static double nextChange = 0;           // Time to change the value
static double changeRate = 20;          // Time between changes (seconds)
static int state = 0;                   // Starting state
static float boxSize =  0.0872664626;// 0.01745329252 0.0872664626;   // Size in radians to move per step (5 degrees)
/***********************************************/

/********** ROBOT_CONTROL mode *****************/
struct motor_control_manual_t groundRobotMotors;    // Struct for the ground robots motor speeds
static int MAX_SPEED = 35;//14080;                          // Maximum linear speed
static int MAX_ROT_SPEED = 0;                       // Maximum rotational speed
static float yawInc =  9*0.0872664626;
/***********************************************/
#ifndef TIC_TOC
#define TIC_TOC

struct timeval startt, endt;
long  secst, usecst;
double mtimet;
void tic(){
    gettimeofday(&startt, NULL);
}
void toc(){
    gettimeofday(&endt, NULL);
    
}

void deltT(){
    secst=endt.tv_sec-startt.tv_sec;
    usecst=endt.tv_usec-startt.tv_usec;
    mtimet=secst*1000+usecst/1000;
    cout<<"Elapsed time is "<<mtimet<<" milliseconds"<<endl;
}
#endif TIC_TOC
int main(int argc, char** argv)
{
    pthread_t threads[2];
    void *status;

    // Load the config file
    if (loadConfigFile())
        cout << "Config file loaded" << endl;

    // Initialize motor controller, data logger, and vrpn
    if (1 == MotorController::instance()->init())
        cout << "\nMotors are ready" << endl;
    if (Logger::instance()->initialize())
        cout << "Log file is ready" << endl;
    vrpn_init("192.168.0.120:3883", handle_pos, handle_eris);
    cout << "VRPN is ready" << endl; // This might not always be true, but we assume it is.
                                     // Might want to add some verification and a return value to
                                     // The vrpn_init function in vrpn.h?
   
    MotorController::instance()->quad->vrpnNow = 0;

    //pthread_mutex_lock(&mutex_vrpn);
    // Set the vrpn data struct
    //vrpn_data_t vrpnData;
    MotorController::instance()->quad->vrpnData_null.usec = 0;
    MotorController::instance()->quad->vrpnData_null.x = 0;
    MotorController::instance()->quad->vrpnData_null.y = 0;
    MotorController::instance()->quad->vrpnData_null.z = 0;
    MotorController::instance()->quad->vrpnData_null.yaw = 0;
    MotorController::instance()->quad->vrpnData_null.pitch = 0;
    MotorController::instance()->quad->vrpnData_null.roll = 0;

    MotorController::instance()->eris->vrpnData_null.usec = 0;
    MotorController::instance()->eris->vrpnData_null.x = 0;
    MotorController::instance()->eris->vrpnData_null.y = 0;
    MotorController::instance()->eris->vrpnData_null.z = 0;
    MotorController::instance()->eris->vrpnData_null.yaw = 0;
    MotorController::instance()->eris->vrpnData_null.pitch = 0;
    MotorController::instance()->eris->vrpnData_null.roll = 0;

    MotorController::instance()->eris->packetDrop=0;
    MotorController::instance()->quad->packetDrop=0;


    tic();
    usleep(1000);
    toc();
    deltT();

    MotorController::instance()->eris->counter1=0;
    // Get the program start time
    struct timeval tm;
    gettimeofday(&tm, 0);
    startTime = (tm.tv_usec / 1000000.0) + tm.tv_sec;

    // Initialize mutexes and join threads
    pthread_mutex_init(&mutex, NULL);
    pthread_mutex_init(&mutex_vrpn, NULL);
    pthread_create(&threads[0], NULL, UIThread, (void*)1);
    pthread_create(&threads[1], NULL, mainThread, (void*)2);
    //pthread_create(&threads[2], NULL, controlLoop, (void*)3);
    pthread_join(threads[1], &status);

    return 0;
}

void* mainThread(void* threadID)
{
    currentMode = STOPPED;

    cout << "\n\nChoose a running mode (modes begin after 3 second delay):\n";
    cout << "\tPress v to start in tuning mode\n";
    cout << "\tPress b to start in robot control mode (driving mode)\n";
    cout << "\tPress n to start in box step mode (changes the setpoint every few seconds)\n";
    cout << "\tPress esc to quit\n";
    while (1)
    {
        cout << "\r";
        switch (cmd)
        {
            case 'v':
                currentMode = TUNING;
                break;

            case 'b':
                currentMode = ROBOT_CONTROL;
                break;

            case 'n':
                currentMode = BOX_STEP;
                break;

            case 27:    // esc key
                // Stop the program
                Logger::instance()->stop();
                MotorController::instance()->stop();
                currentMode = STOPPED;
                controlMode=STOPPED;
                exit(0);
                break;
        }

        if (currentMode != STOPPED)
        {
            sleep(3);   // Wait 3 seconds before beginning
            
            // Set the cmd character back to null
            pthread_mutex_lock(&mutex);
            cmd = 0;
            pthread_mutex_unlock(&mutex);

            // Arm the quad motors
            MotorController::instance()->armQuadMotors();

            // Begin accepting data from vrpn
            controlMode=ROBOT_CONTROL;
            vrpn_go();
            
        }
    }
}

/**
 * vrpn callback function that is called everytime vrpn data of Eris is
 * received. The callback gets the data and calls the pid controller
 * by passing that data in.
 *
 * @param d     no clue what this is
 * @param t     the vrpn data that was received
 */
void VRPN_CALLBACK handle_eris(void*, const vrpn_TRACKERCB t){
	// Convert the angles from quat to euler angles
    q_vec_type euler;
    q_to_euler(euler, t.quat);
	
	// TODO: this should be set un the initialize function, and we should be setting this to
    //       the time in here?
    MotorController::instance()->eris->vrpnNow = 0;

    //pthread_mutex_lock(&mutex_vrpn);
    // Set the vrpn data struct
    MotorController::instance()->eris->vrpnData.usec = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
    MotorController::instance()->eris->vrpnData.x = t.pos[0];
    MotorController::instance()->eris->vrpnData.y = t.pos[1];
    MotorController::instance()->eris->vrpnData.z = t.pos[2];
    MotorController::instance()->eris->vrpnData.yaw = euler[0];
    MotorController::instance()->eris->vrpnData.pitch = euler[1];
    MotorController::instance()->eris->vrpnData.roll = euler[2];
	//pthread_mutex_unlock(&mutex_vrpn);

	//Does something with vrpnNow
    MotorController::instance()->eris->vrpnPrev = MotorController::instance()->eris->vrpnNow;
    if (MotorController::instance()->eris->vrpnNow == 0)
    {
        MotorController::instance()->eris->vrpnPrev = 0;
        MotorController::instance()->eris->vrpnTime0 = MotorController::instance()->eris->vrpnData.usec;
        MotorController::instance()->eris->vrpnNow = MotorController::instance()->eris->vrpnData.usec;
        MotorController::instance()->eris->vrpnLast = MotorController::instance()->eris->vrpnData.usec;
    }
    else
    {
		//from what I can tell vrpnNow is always set to zero so the code never reaches this statement
        MotorController::instance()->eris->vrpnNow = MotorController::instance()->eris->vrpnData.usec - MotorController::instance()->eris->vrpnTime0;
    }
	
	//writes the vrpn time in usec to the Eris VRPN data struct
	MotorController::instance()->eris->usec=MotorController::instance()->eris->vrpnData.usec;
	
	//Writes the position and orientation of Eris to a struct to be logged latter
	MotorController::instance()->eris->x=MotorController::instance()->eris->vrpnData.x;
	MotorController::instance()->eris->y=MotorController::instance()->eris->vrpnData.y;
	MotorController::instance()->eris->z=MotorController::instance()->eris->vrpnData.z;
	MotorController::instance()->eris->roll=MotorController::instance()->eris->vrpnData.roll;
	MotorController::instance()->eris->pitch=MotorController::instance()->eris->vrpnData.pitch;
	MotorController::instance()->eris->yaw=MotorController::instance()->eris->vrpnData.yaw;
    if(MotorController::instance()->eris->x_pre==-10000){
        MotorController::instance()->eris->x_vel=0;
        MotorController::instance()->eris->y_vel=0;
        MotorController::instance()->eris->dt=0.01;
        MotorController::instance()->eris->x_pre=MotorController::instance()->eris->vrpnData.x;
        MotorController::instance()->eris->y_pre=MotorController::instance()->eris->vrpnData.y;
    }else{
        //calculates (estimates) x and y velocities in camera reference frame in meters/s
        //dt=MotorController::instance()->quad->vrpnNow-MotorController::instance()->quad->vrpnLast;
        
        MotorController::instance()->eris->x_vel=(MotorController::instance()->eris->vrpnData.x-MotorController::instance()->eris->x_pre)/(.01);
        MotorController::instance()->eris->y_vel=(MotorController::instance()->eris->vrpnData.y-MotorController::instance()->eris->y_pre)/(.01);
        MotorController::instance()->eris->x_pre=MotorController::instance()->eris->vrpnData.x;
        MotorController::instance()->eris->y_pre=MotorController::instance()->eris->vrpnData.y;
        MotorController::instance()->eris->dt=MotorController::instance()->eris->vrpnNow-MotorController::instance()->eris->vrpnLast;
        MotorController::instance()->eris->vrpnLast = MotorController::instance()->eris->vrpnNow;
    }
    /*if (MotorController::instance()->eris->counter1<1){
        MotorController::instance()->eris->counter1=MotorController::instance()->eris->counter1+1;
    }else{
        SensorManager::instance()->readMotorEncoders();
        MotorController::instance()->eris->counter1=0;
    }*/
    #ifndef mot50
    #ifndef ssDrop
    #ifndef RADAlqi
    #ifndef H2ctrl
    #ifndef rada_lqg
    #ifdef rada_split_lqg
    eris_lqg(&(MotorController::instance()->eris->vrpnData));
    #else
    #ifdef rada_split_lqg_ol
    PID_eris_ol(&MotorController::instance()->eris->vrpnData);
    #else
    PID_eris(&MotorController::instance()->eris->vrpnData);
    #endif
    #endif
    #endif
    #endif
    #endif
    #endif
    #endif
    //SensorManager::instance()->readMotorEncoders();
    //Logger::instance()->logData();
    
}
/**
 * vrpn callback function that is called everytime vrpn data of the pendulum is 
 * received. The callback gets the data and calls the pid controller
 * by passing that data in.
 *
 * @param d     no clue what this is
 * @param t     the vrpn data that was received
 */
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t)
{
    // Convert the angles from quat to euler angles
    q_vec_type euler;
    q_to_euler(euler, t.quat);

    // TODO: this should be set un the initialize function, and we should be setting this to
    //       the time in here?
    MotorController::instance()->quad->vrpnNow = 0;

    //pthread_mutex_lock(&mutex_vrpn);
    // Set the vrpn data struct
    //vrpn_data_t vrpnData;
    MotorController::instance()->quad->vrpnData.usec = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
    MotorController::instance()->quad->vrpnData.x = t.pos[0];
    MotorController::instance()->quad->vrpnData.y = t.pos[1];
    MotorController::instance()->quad->vrpnData.z = t.pos[2];
    MotorController::instance()->quad->vrpnData.yaw = euler[0];
    MotorController::instance()->quad->vrpnData.pitch = euler[1];
    MotorController::instance()->quad->vrpnData.roll = euler[2];
    //pthread_mutex_unlock(&mutex_vrpn);

    MotorController::instance()->quad->vrpnPrev = MotorController::instance()->quad->vrpnNow;
    if (MotorController::instance()->quad->vrpnNow == 0)
    {
        MotorController::instance()->quad->vrpnPrev = 0;
        MotorController::instance()->quad->vrpnTime0 = MotorController::instance()->quad->vrpnData.usec;
        MotorController::instance()->quad->vrpnNow = MotorController::instance()->quad->vrpnData.usec;
        MotorController::instance()->quad->vrpnLast=MotorController::instance()->quad->vrpnNow;
        MotorController::instance()->quad->RADA.y_in[0]=MotorController::instance()->quad->vrpnData.roll;
        MotorController::instance()->quad->RADA.y_in[1]=MotorController::instance()->quad->vrpnData.pitch;
    }
    else
    {
        MotorController::instance()->quad->vrpnNow = MotorController::instance()->quad->vrpnData.usec - MotorController::instance()->quad->vrpnTime0;
    }

    MotorController::instance()->quad->usec=MotorController::instance()->quad->vrpnData.usec;
    // Based on the current mode, display the correct UI and check for user input
    switch (currentMode)
    {
        case BOX_STEP:
            boxStepMode(MotorController::instance()->quad->vrpnData.usec);
        // Drop through here to use the TUNING ui/controls 
        case TUNING:
            // prevent robot from moving
            MotorController::instance()->stopGroundRobot();
            tuning_UI(&(MotorController::instance()->quad->vrpnData));
            tuningMode();
            break;
        case ROBOT_CONTROL:
            robotControl_UI(&(MotorController::instance()->quad->vrpnData));
            robotControlMode();
            break;
        case STOPPED:
            // Stop the motors and logger
            MotorController::instance()->stop();
            Logger::instance()->stop();
            exit(0);
            break;
    }
    #ifdef mot50
    motors50_test();
    #else
    
    #ifdef ssDrop
        randomData = fopen("/dev/urandom", "r");
        randomDataLen = 0;
        
        while (randomDataLen < sizeof myRandomData)
        {
            result = fread( myRandomData+randomDataLen, 1,  (sizeof myRandomData) - randomDataLen, randomData);
            if (result < 0)
            {
                // error, unable to read /dev/random 
            }
            randomDataLen += result;
        }
        
        fclose(randomData);

        myRandomData[0]=myRandomData[1];
        if(myRandomData[0]<ErisDrop && myRandomData[1]<PenDrop){
            MotorController::instance()->eris->packetDrop=0;
            MotorController::instance()->quad->packetDrop=0;
            RADA_SSctrl_loss(&MotorController::instance()->quad->vrpnData_null, &(MotorController::instance()->eris->vrpnData_null));
        }else if(myRandomData[0]<ErisDrop){
            MotorController::instance()->eris->packetDrop=0;
            MotorController::instance()->quad->packetDrop=1;
            RADA_SSctrl_loss(&MotorController::instance()->quad->vrpnData, &(MotorController::instance()->eris->vrpnData_null));
        }else if(myRandomData[1]<PenDrop){
            MotorController::instance()->eris->packetDrop=1;
            MotorController::instance()->quad->packetDrop=0;
            RADA_SSctrl_loss(&MotorController::instance()->quad->vrpnData_null, &(MotorController::instance()->eris->vrpnData));
        }else{
            MotorController::instance()->eris->packetDrop=1;
            MotorController::instance()->quad->packetDrop=1;
            RADA_SSctrl_loss(&MotorController::instance()->quad->vrpnData, &(MotorController::instance()->eris->vrpnData));
        }
    #else
        MotorController::instance()->eris->packetDrop=1;
        MotorController::instance()->quad->packetDrop=1;
    #ifdef RADAlqi
        #ifdef RADAlqi_est
        RADA_lqi_est(&(MotorController::instance()->quad->vrpnData), &(MotorController::instance()->eris->vrpnData));
        #else
        RADA_lqi(&(MotorController::instance()->quad->vrpnData), &(MotorController::instance()->eris->vrpnData));
        #endif
    #else
    #ifdef rada_lqg
        RADA_lqg(&(MotorController::instance()->quad->vrpnData), &(MotorController::instance()->eris->vrpnData));
    #else
    #ifdef H2ctrl
        RADA_SSctrl(&MotorController::instance()->quad->vrpnData, &(MotorController::instance()->eris->vrpnData));
    #else
    #ifdef rada_split_lqg
        quad_lqg(&(MotorController::instance()->quad->vrpnData));
    #else
    #ifdef rada_split_lqg_ol
        quad_lqg(&(MotorController::instance()->quad->vrpnData));
    #else
        // Run the pid controller with the newly recieved vrpn data
       PID_controller(&MotorController::instance()->quad->vrpnData);
    #endif
    #endif
    #endif
    #endif
    #endif
    #endif
    #endif
    // Log the new data
    Logger::instance()->logData();
}

void motors50_test(){
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;

    //sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);
}

void RADA_SSctrl(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris){
    int motors_left_correct, motors_right_correct, motors_front_correct, motors_back_correct;
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;
    double ux, uy, uxb, uyb;
    tic();
    MotorController::instance()->quad->RADA_pack.ref_x+=MotorController::instance()->quad->RADA_pack.slope_x*.01;
    MotorController::instance()->quad->RADA_pack.ref_y+=MotorController::instance()->quad->RADA_pack.slope_y*.01;
    #ifdef ssTracking
        MotorController::instance()->quad->RADA_pack.u[3]=MotorController::instance()->quad->RADA_pack.ref_x-vrpnDataEris->x;
        MotorController::instance()->quad->RADA_pack.u[4]=MotorController::instance()->quad->RADA_pack.ref_y-vrpnDataEris->y;
        MotorController::instance()->quad->RADA_pack.u[5]=MotorController::instance()->quad->RADA_pack.ref_phi-vrpnDataPen->roll;
        MotorController::instance()->quad->RADA_pack.u[6]=MotorController::instance()->quad->RADA_pack.ref_theta-vrpnDataPen->pitch;
        MotorController::instance()->quad->RADA_pack.u[7]=MotorController::instance()->quad->RADA_pack.ref_psi-vrpnDataEris->yaw;
        MotorController::instance()->quad->RADA_pack.u[0]+=MotorController::instance()->quad->RADA_pack.u[5]*.01;
        MotorController::instance()->quad->RADA_pack.u[1]+=MotorController::instance()->quad->RADA_pack.u[6]*.01;
        MotorController::instance()->quad->RADA_pack.u[2]+=MotorController::instance()->quad->RADA_pack.u[7]*.01;
        //cout << "\tHello\n";
    #else
        MotorController::instance()->quad->RADA_pack.u[0]=MotorController::instance()->quad->RADA_pack.ref_x-vrpnDataEris->x;
        MotorController::instance()->quad->RADA_pack.u[1]=MotorController::instance()->quad->RADA_pack.ref_y-vrpnDataEris->y;
        MotorController::instance()->quad->RADA_pack.u[2]=MotorController::instance()->quad->RADA_pack.ref_phi-vrpnDataPen->roll;
        MotorController::instance()->quad->RADA_pack.u[3]=MotorController::instance()->quad->RADA_pack.ref_theta-vrpnDataPen->pitch;
        MotorController::instance()->quad->RADA_pack.u[4]=MotorController::instance()->quad->RADA_pack.ref_psi-vrpnDataEris->yaw;
    #endif
    MotorController::instance()->SS_CTRL_CALC(&(MotorController::instance()->quad->RADA_pack));

    PWM_value_correction_roll=1*(int)MotorController::instance()->quad->RADA_pack.Y[0];
    PWM_value_correction_pitch=1*(int)MotorController::instance()->quad->RADA_pack.Y[1];

    if(MotorController::instance()->quad->RADA_pack.slope_x>0){
        ux=MotorController::instance()->quad->RADA_pack.Y[3]+WHEEL_DEAD_ZONE;
    }else if(MotorController::instance()->quad->RADA_pack.slope_x<0){
        ux=MotorController::instance()->quad->RADA_pack.Y[3]-WHEEL_DEAD_ZONE;
    }else{
        ux=MotorController::instance()->quad->RADA_pack.Y[3];
    }
    if(MotorController::instance()->quad->RADA_pack.slope_y>0){
        uy=MotorController::instance()->quad->RADA_pack.Y[4]+WHEEL_DEAD_ZONE;
    }else if(MotorController::instance()->quad->RADA_pack.slope_y<0){
        uy=MotorController::instance()->quad->RADA_pack.Y[4]-WHEEL_DEAD_ZONE;
    }else{
        uy=MotorController::instance()->quad->RADA_pack.Y[4];
    }

    uxb=ux*cos(vrpnDataEris->yaw)+uy*sin(vrpnDataEris->yaw);
    uyb=-ux*sin(vrpnDataEris->yaw)+uy*cos(vrpnDataEris->yaw);

    motors_left_correct=1*(int)(-uxb-MotorController::instance()->quad->RADA_pack.Y[2]);
    motors_right_correct=1*(int)(uxb-MotorController::instance()->quad->RADA_pack.Y[2]);
    motors_front_correct=1*(int)(-uyb-MotorController::instance()->quad->RADA_pack.Y[2]);
    motors_back_correct=1*(int)(uyb-MotorController::instance()->quad->RADA_pack.Y[2]);


    //checks to make sure the command is within the valid range of PWM commands
    if(motors_left_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_left_correct>MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_right_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_right_correct>MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_front_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_front_correct>MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_back_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_back_correct>MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=MAX_ERIS_DUTY_CYCLE;
    }

    //sets PWM command for each motor
    groundRobotMotors.motor_speed_left = motors_left_correct;
    groundRobotMotors.motor_speed_right = motors_right_correct;
    groundRobotMotors.motor_speed_front = motors_front_correct;
    groundRobotMotors.motor_speed_rear = motors_back_correct;
    ///*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;  
    // */
    /*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;
    // */
    
    //makes sure that the PWM commands are within the valid range for the ESCs
    if (MotorController::instance()->quad->motor_1_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_1_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_2_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_2_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MAX;
    }   
    if (MotorController::instance()->quad->motor_3_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_3_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_4_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_4_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MAX;
    }   
    
    //sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);
    MotorController::instance()->setMotors(&groundRobotMotors);
    MotorController::instance()->setMotorSpeeds();
    toc();
    deltT();
}

void RADA_SSctrl_loss(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris){
    int motors_left_correct, motors_right_correct, motors_front_correct, motors_back_correct;
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;
    double ux, uy, uxb, uyb;
    tic();
    MotorController::instance()->quad->RADA_pack_loss.Xi_a[0]=1;
    MotorController::instance()->quad->RADA_pack_loss.Xi_a[1]=1;
    MotorController::instance()->quad->RADA_pack_loss.Xi_a[2]=1;
    MotorController::instance()->quad->RADA_pack_loss.Xi_a[3]=1;
    MotorController::instance()->quad->RADA_pack_loss.Xi_a[4]=1;
    if(MotorController::instance()->quad->packetDrop==0){
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[0]=0;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[1]=0;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[5]=0;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[6]=0;
    }else{
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[0]=1;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[1]=1;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[5]=1;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[6]=1;
    }
    if(MotorController::instance()->eris->packetDrop==0){
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[2]=0;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[3]=0;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[4]=0;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[7]=0;
    }else{
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[2]=1;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[3]=1;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[4]=1;
        MotorController::instance()->quad->RADA_pack_loss.Xi_s[7]=1;
    }

    MotorController::instance()->quad->RADA_pack_loss.ref_x+=MotorController::instance()->quad->RADA_pack_loss.slope_x*.01;
    MotorController::instance()->quad->RADA_pack_loss.ref_y+=MotorController::instance()->quad->RADA_pack_loss.slope_y*.01;
    if(MotorController::instance()->quad->packetDrop==0){
        MotorController::instance()->quad->RADA_pack_loss.u[3]=MotorController::instance()->quad->RADA_pack_loss.slope_x*.01;
        MotorController::instance()->quad->RADA_pack_loss.u[4]=MotorController::instance()->quad->RADA_pack_loss.slope_y*.01;
        MotorController::instance()->quad->RADA_pack_loss.u[5]=0;
        MotorController::instance()->quad->RADA_pack_loss.u[6]=0;
        MotorController::instance()->quad->RADA_pack_loss.u[7]=0;
        MotorController::instance()->quad->RADA_pack_loss.u[0]+=0;
        MotorController::instance()->quad->RADA_pack_loss.u[1]+=0;
        MotorController::instance()->quad->RADA_pack_loss.u[2]+=0;
    }else{
        MotorController::instance()->quad->RADA_pack_loss.u[3]=MotorController::instance()->quad->RADA_pack_loss.ref_x+vrpnDataEris->x;
        MotorController::instance()->quad->RADA_pack_loss.u[4]=MotorController::instance()->quad->RADA_pack_loss.ref_y+vrpnDataEris->y;
        MotorController::instance()->quad->RADA_pack_loss.u[5]=MotorController::instance()->quad->RADA_pack_loss.ref_phi+vrpnDataPen->roll;
        MotorController::instance()->quad->RADA_pack_loss.u[6]=MotorController::instance()->quad->RADA_pack_loss.ref_theta+vrpnDataPen->pitch;
        MotorController::instance()->quad->RADA_pack_loss.u[7]=MotorController::instance()->quad->RADA_pack_loss.ref_psi+vrpnDataEris->yaw;
        MotorController::instance()->quad->RADA_pack_loss.u[0]+=MotorController::instance()->quad->RADA_pack_loss.u[5]*.01;
        MotorController::instance()->quad->RADA_pack_loss.u[1]+=MotorController::instance()->quad->RADA_pack_loss.u[6]*.01;
        MotorController::instance()->quad->RADA_pack_loss.u[2]+=MotorController::instance()->quad->RADA_pack_loss.u[7]*.01;
    }
    
    MotorController::instance()->SS_CTRL_CALC_loss(&(MotorController::instance()->quad->RADA_pack_loss));

    PWM_value_correction_roll=1*(int)MotorController::instance()->quad->RADA_pack_loss.Yo[0];
    PWM_value_correction_pitch=1*(int)MotorController::instance()->quad->RADA_pack_loss.Yo[1];

    if(MotorController::instance()->eris->x_vel_set>0){
        ux=MotorController::instance()->quad->RADA_pack_loss.Yo[3]+WHEEL_DEAD_ZONE;
    }else if(MotorController::instance()->eris->x_vel_set<0){
        ux=MotorController::instance()->quad->RADA_pack_loss.Yo[3]-WHEEL_DEAD_ZONE;
    }else{
        ux=MotorController::instance()->quad->RADA_pack_loss.Yo[3];
    }
    if(MotorController::instance()->eris->y_vel_set>0){
        uy=MotorController::instance()->quad->RADA_pack_loss.Yo[4]+WHEEL_DEAD_ZONE;
    }else if(MotorController::instance()->eris->y_vel_set<0){
        uy=MotorController::instance()->quad->RADA_pack_loss.Yo[4]-WHEEL_DEAD_ZONE;
    }else{
        uy=MotorController::instance()->quad->RADA_pack_loss.Yo[4];
    }

    uxb=ux*cos(vrpnDataEris->yaw)+uy*sin(vrpnDataEris->yaw);
    uyb=-ux*sin(vrpnDataEris->yaw)+uy*cos(vrpnDataEris->yaw);

    motors_left_correct=1*(int)(-uxb-MotorController::instance()->quad->RADA_pack_loss.Yo[2]);
    motors_right_correct=1*(int)(uxb-MotorController::instance()->quad->RADA_pack_loss.Yo[2]);
    motors_front_correct=1*(int)(-uyb-MotorController::instance()->quad->RADA_pack_loss.Yo[2]);
    motors_back_correct=1*(int)(uyb-MotorController::instance()->quad->RADA_pack_loss.Yo[2]);


    //checks to make sure the command is within the valid range of PWM commands
    if(motors_left_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_left_correct>MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_right_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_right_correct>MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_front_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_front_correct>MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_back_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_back_correct>MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=MAX_ERIS_DUTY_CYCLE;
    }

    //sets PWM command for each motor
    groundRobotMotors.motor_speed_left = motors_left_correct;
    groundRobotMotors.motor_speed_right = motors_right_correct;
    groundRobotMotors.motor_speed_front = motors_front_correct;
    groundRobotMotors.motor_speed_rear = motors_back_correct;
    ///*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;  
    // */
    /*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;
    // */
    
    //makes sure that the PWM commands are within the valid range for the ESCs
    if (MotorController::instance()->quad->motor_1_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_1_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_2_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_2_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MAX;
    }   
    if (MotorController::instance()->quad->motor_3_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_3_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_4_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_4_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MAX;
    }   
    
    //sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);
    MotorController::instance()->setMotors(&groundRobotMotors);
    MotorController::instance()->setMotorSpeeds();
    
    toc();
    deltT();
}

void RADA_lqi(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris){
    int motors_left_correct, motors_right_correct, motors_front_correct, motors_back_correct;
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;

    double ux, uy, uxb, uyb, ref_xb, ref_yb, dt;

    
    dt=.01;
    MotorController::instance()->quad->vrpnLast=MotorController::instance()->quad->vrpnNow;
    
    //sets vervious x and y locations to currentx,y location
    

    //MotorController::instance()->quad->RADA.x_dot=MotorController::instance()->dataFilter(&(MotorController::instance()->eris->xb_vel_filter), MotorController::instance()->eris->x_vel);
    //MotorController::instance()->quad->RADA.y_dot= MotorController::instance()->dataFilter(&(MotorController::instance()->eris->yb_vel_filter), MotorController::instance()->eris->y_vel);
    MotorController::instance()->quad->RADA.x_dot=MotorController::instance()->eris->x_vel;
    MotorController::instance()->quad->RADA.y_dot=MotorController::instance()->eris->y_vel;

    MotorController::instance()->quad->RADA.x+=(MotorController::instance()->quad->RADA.ref_x-MotorController::instance()->quad->RADA.x_dot)*dt;
    MotorController::instance()->quad->RADA.y+=(MotorController::instance()->quad->RADA.ref_y-MotorController::instance()->quad->RADA.y_dot)*dt;
    
    MotorController::instance()->quad->RADA.phi=vrpnDataPen->roll;
    MotorController::instance()->quad->RADA.theta=vrpnDataPen->pitch;
    MotorController::instance()->quad->RADA.psi=vrpnDataEris->yaw;
    
    MotorController::instance()->quad->RADA.phiI+=(MotorController::instance()->quad->RADA.ref_phi-vrpnDataPen->roll)*dt;
    MotorController::instance()->quad->RADA.thetaI+=(MotorController::instance()->quad->RADA.ref_theta-vrpnDataPen->pitch)*dt;
    MotorController::instance()->quad->RADA.psiI+=(MotorController::instance()->quad->RADA.ref_psi-vrpnDataEris->yaw)*dt;

    MotorController::instance()->RADA_LQI_dt(&(MotorController::instance()->quad->RADA), dt);

    PWM_value_correction_roll=1*(int)MotorController::instance()->quad->RADA.U[0];
    PWM_value_correction_pitch=1*(int)MotorController::instance()->quad->RADA.U[1];

    ux=MotorController::instance()->quad->RADA.U[3];
    uy=MotorController::instance()->quad->RADA.U[4];

    ref_xb= MotorController::instance()->quad->RADA.ref_x;//MotorController::instance()->quad->RADA.ref_x*cos(vrpnDataEris->yaw)+MotorController::instance()->quad->RADA.ref_y*sin(vrpnDataEris->yaw);
    ref_yb=MotorController::instance()->quad->RADA.ref_y;//-MotorController::instance()->quad->RADA.ref_x*sin(vrpnDataEris->yaw)+MotorController::instance()->quad->RADA.ref_y*cos(vrpnDataEris->yaw);
    
    if(ref_xb>0){
        ux+=WHEEL_DEAD_ZONE;
    }else if(ref_xb<0){
        ux-=WHEEL_DEAD_ZONE;
    }
    if(ref_yb>0){
        uy+=WHEEL_DEAD_ZONE;
    }else if(ref_yb<0){
        uy-=WHEEL_DEAD_ZONE;
    }
    uxb=ux*cos(vrpnDataEris->yaw)+uy*sin(vrpnDataEris->yaw);
    uyb=-ux*sin(vrpnDataEris->yaw)+uy*cos(vrpnDataEris->yaw);

    

    motors_left_correct=1*(int)(-uxb-MotorController::instance()->quad->RADA.U[2]);
    motors_right_correct=1*(int)(uxb-MotorController::instance()->quad->RADA.U[2]);
    motors_front_correct=1*(int)(-uyb-MotorController::instance()->quad->RADA.U[2]);
    motors_back_correct=1*(int)(uyb-MotorController::instance()->quad->RADA.U[2]);

    //checks to make sure the command is within the valid range of PWM commands
    if(motors_left_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_left_correct>MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_right_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_right_correct>MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_front_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_front_correct>MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_back_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_back_correct>MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=MAX_ERIS_DUTY_CYCLE;
    }

    //sets PWM command for each motor
    groundRobotMotors.motor_speed_left = motors_left_correct;
    groundRobotMotors.motor_speed_right = motors_right_correct;
    groundRobotMotors.motor_speed_front = motors_front_correct;
    groundRobotMotors.motor_speed_rear = motors_back_correct;

    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;  

    /*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;
    // */
    
    //makes sure that the PWM commands are within the valid range for the ESCs
    if (MotorController::instance()->quad->motor_1_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_1_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_2_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_2_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MAX;
    }   
    if (MotorController::instance()->quad->motor_3_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_3_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_4_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_4_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MAX;
    }   
    
    //sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);
    MotorController::instance()->setMotors(&groundRobotMotors);
    MotorController::instance()->setMotorSpeeds(); 

}

void RADA_lqi_est(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris){
    int motors_left_correct, motors_right_correct, motors_front_correct, motors_back_correct;
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;

    double ux, uy, uxb, uyb, ref_xb, ref_yb, dt;

    
    dt=.01;
    MotorController::instance()->quad->vrpnLast=MotorController::instance()->quad->vrpnNow;
    
    //sets vervious x and y locations to currentx,y location
    MotorController::instance()->quad->RADA.x_dot=MotorController::instance()->eris->x_vel;
    MotorController::instance()->quad->RADA.y_dot=MotorController::instance()->eris->y_vel;
    
    MotorController::instance()->quad->RADA.phi=vrpnDataPen->roll;
    MotorController::instance()->quad->RADA.theta=vrpnDataPen->pitch;
    MotorController::instance()->quad->RADA.psi=vrpnDataEris->yaw;

    //MotorController::instance()->quad->RADA.x_dot=MotorController::instance()->dataFilter(&(MotorController::instance()->eris->xb_vel_filter), MotorController::instance()->eris->x_vel);
    //MotorController::instance()->quad->RADA.y_dot= MotorController::instance()->dataFilter(&(MotorController::instance()->eris->yb_vel_filter), MotorController::instance()->eris->y_vel);
    MotorController::instance()->quad->RADA.y_in[3]=MotorController::instance()->eris->x_vel;
    MotorController::instance()->quad->RADA.y_in[4]=MotorController::instance()->eris->y_vel;

    MotorController::instance()->quad->RADA.x+=(MotorController::instance()->quad->RADA.ref_x-MotorController::instance()->quad->RADA.y_in[3])*dt;
    MotorController::instance()->quad->RADA.y+=(MotorController::instance()->quad->RADA.ref_y-MotorController::instance()->quad->RADA.y_in[4])*dt;
    
    MotorController::instance()->quad->RADA.y_in[0]=vrpnDataPen->roll;
    MotorController::instance()->quad->RADA.y_in[1]=vrpnDataPen->pitch;
    MotorController::instance()->quad->RADA.y_in[2]=vrpnDataEris->yaw;
    
    MotorController::instance()->quad->RADA.phiI+=(MotorController::instance()->quad->RADA.ref_phi-vrpnDataPen->roll)*dt;
    MotorController::instance()->quad->RADA.thetaI+=(MotorController::instance()->quad->RADA.ref_theta-vrpnDataPen->pitch)*dt;
    MotorController::instance()->quad->RADA.psiI+=(MotorController::instance()->quad->RADA.ref_psi-vrpnDataEris->yaw)*dt;

    MotorController::instance()->RADA_EST_LQI(&(MotorController::instance()->quad->RADA));

    PWM_value_correction_roll=1*(int)MotorController::instance()->quad->RADA.U[0];
    PWM_value_correction_pitch=1*(int)MotorController::instance()->quad->RADA.U[1];

    ux=MotorController::instance()->quad->RADA.U[3];
    uy=MotorController::instance()->quad->RADA.U[4];

    ref_xb= MotorController::instance()->quad->RADA.ref_x;//MotorController::instance()->quad->RADA.ref_x*cos(vrpnDataEris->yaw)+MotorController::instance()->quad->RADA.ref_y*sin(vrpnDataEris->yaw);
    ref_yb=MotorController::instance()->quad->RADA.ref_y;//-MotorController::instance()->quad->RADA.ref_x*sin(vrpnDataEris->yaw)+MotorController::instance()->quad->RADA.ref_y*cos(vrpnDataEris->yaw);
    
    if(ref_xb>0){
        ux+=WHEEL_DEAD_ZONE;
    }else if(ref_xb<0){
        ux-=WHEEL_DEAD_ZONE;
    }
    if(ref_yb>0){
        uy+=WHEEL_DEAD_ZONE;
    }else if(ref_yb<0){
        uy-=WHEEL_DEAD_ZONE;
    }
    uxb=ux*cos(vrpnDataEris->yaw)+uy*sin(vrpnDataEris->yaw);
    uyb=-ux*sin(vrpnDataEris->yaw)+uy*cos(vrpnDataEris->yaw);

    

    motors_left_correct=1*(int)(-uxb-MotorController::instance()->quad->RADA.U[2]);
    motors_right_correct=1*(int)(uxb-MotorController::instance()->quad->RADA.U[2]);
    motors_front_correct=1*(int)(-uyb-MotorController::instance()->quad->RADA.U[2]);
    motors_back_correct=1*(int)(uyb-MotorController::instance()->quad->RADA.U[2]);

    //checks to make sure the command is within the valid range of PWM commands
    if(motors_left_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_left_correct>MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_right_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_right_correct>MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_front_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_front_correct>MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_back_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_back_correct>MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=MAX_ERIS_DUTY_CYCLE;
    }

    //sets PWM command for each motor
    groundRobotMotors.motor_speed_left = motors_left_correct;
    groundRobotMotors.motor_speed_right = motors_right_correct;
    groundRobotMotors.motor_speed_front = motors_front_correct;
    groundRobotMotors.motor_speed_rear = motors_back_correct;

    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;  

    /*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;
    // */
    
    //makes sure that the PWM commands are within the valid range for the ESCs
    if (MotorController::instance()->quad->motor_1_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_1_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_2_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_2_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MAX;
    }   
    if (MotorController::instance()->quad->motor_3_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_3_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_4_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_4_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MAX;
    }   
    
    //sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);
    MotorController::instance()->setMotors(&groundRobotMotors);
    MotorController::instance()->setMotorSpeeds(); 

}

void RADA_lqg(vrpn_data_t *vrpnDataPen, vrpn_data_t *vrpnDataEris){
    int motors_left_correct, motors_right_correct, motors_front_correct, motors_back_correct;
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;

    double ux, uy, uxb, uyb, ref_xb, ref_yb, dt;

    
    dt=.01;
    MotorController::instance()->quad->vrpnLast=MotorController::instance()->quad->vrpnNow;
    

    //MotorController::instance()->quad->RADA.x_dot=MotorController::instance()->dataFilter(&(MotorController::instance()->eris->xb_vel_filter), MotorController::instance()->eris->x_vel);
    //MotorController::instance()->quad->RADA.y_dot= MotorController::instance()->dataFilter(&(MotorController::instance()->eris->yb_vel_filter), MotorController::instance()->eris->y_vel);
    MotorController::instance()->quad->RADAg.ry[0]=MotorController::instance()->quad->RADA.ref_phi;
    MotorController::instance()->quad->RADAg.ry[1]=MotorController::instance()->quad->RADA.ref_theta;
    MotorController::instance()->quad->RADAg.ry[2]=MotorController::instance()->eris->yaw_set;
    MotorController::instance()->quad->RADAg.ry[3]=MotorController::instance()->eris->x_vel_set*.01;
    MotorController::instance()->quad->RADAg.ry[4]=MotorController::instance()->eris->y_vel_set*.01;
    MotorController::instance()->quad->RADAg.ry[8]=MotorController::instance()->eris->x_vel;
    MotorController::instance()->quad->RADAg.ry[9]=MotorController::instance()->eris->y_vel;
    
    MotorController::instance()->quad->RADAg.ry[5]=vrpnDataPen->roll;
    MotorController::instance()->quad->RADAg.ry[6]=vrpnDataPen->pitch;
    MotorController::instance()->quad->RADAg.ry[7]=vrpnDataEris->yaw;
    

    MotorController::instance()->LQG_CALC(&(MotorController::instance()->quad->RADAg));

    PWM_value_correction_roll=1*(int)MotorController::instance()->quad->RADAg.U[0];
    PWM_value_correction_pitch=1*(int)MotorController::instance()->quad->RADAg.U[1];

    ux=MotorController::instance()->quad->RADAg.U[3];
    uy=MotorController::instance()->quad->RADAg.U[4];

    ref_xb= MotorController::instance()->quad->RADAg.ry[3];//MotorController::instance()->quad->RADA.ref_x*cos(vrpnDataEris->yaw)+MotorController::instance()->quad->RADA.ref_y*sin(vrpnDataEris->yaw);
    ref_yb=MotorController::instance()->quad->RADAg.ry[4];//-MotorController::instance()->quad->RADA.ref_x*sin(vrpnDataEris->yaw)+MotorController::instance()->quad->RADA.ref_y*cos(vrpnDataEris->yaw);
    
    if(ref_xb>0){
        ux+=WHEEL_DEAD_ZONE;
    }else if(ref_xb<0){
        ux-=WHEEL_DEAD_ZONE;
    }
    if(ref_yb>0){
        uy+=WHEEL_DEAD_ZONE;
    }else if(ref_yb<0){
        uy-=WHEEL_DEAD_ZONE;
    }
    uxb=ux*cos(vrpnDataEris->yaw)+uy*sin(vrpnDataEris->yaw);
    uyb=-ux*sin(vrpnDataEris->yaw)+uy*cos(vrpnDataEris->yaw);

    
    #ifdef yaw_off
    motors_left_correct=1*(int)(-uxb);
    motors_right_correct=1*(int)(uxb);
    motors_front_correct=1*(int)(-uyb);
    motors_back_correct=1*(int)(uyb);
    #else
    motors_left_correct=1*(int)(-uxb-MotorController::instance()->quad->RADAg.U[2]);
    motors_right_correct=1*(int)(uxb-MotorController::instance()->quad->RADAg.U[2]);
    motors_front_correct=1*(int)(-uyb-MotorController::instance()->quad->RADAg.U[2]);
    motors_back_correct=1*(int)(uyb-MotorController::instance()->quad->RADAg.U[2]);
    #endif

    //checks to make sure the command is within the valid range of PWM commands
    if(motors_left_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_left_correct>MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_right_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_right_correct>MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_front_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_front_correct>MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_back_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_back_correct>MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=MAX_ERIS_DUTY_CYCLE;
    }

    //sets PWM command for each motor
    groundRobotMotors.motor_speed_left = motors_left_correct;
    groundRobotMotors.motor_speed_right = motors_right_correct;
    groundRobotMotors.motor_speed_front = motors_front_correct;
    groundRobotMotors.motor_speed_rear = motors_back_correct;

    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;  

    /*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;
    // */
    
    //makes sure that the PWM commands are within the valid range for the ESCs
    if (MotorController::instance()->quad->motor_1_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_1_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_2_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_2_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MAX;
    }   
    if (MotorController::instance()->quad->motor_3_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_3_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_4_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_4_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MAX;
    }   
    
    //sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);
    MotorController::instance()->setMotors(&groundRobotMotors);
    MotorController::instance()->setMotorSpeeds(); 

}

void quad_lqg(vrpn_data_t *vrpnDataPen){
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;

    
    MotorController::instance()->quad->vrpnLast=MotorController::instance()->quad->vrpnNow;
    
    MotorController::instance()->quad->quad_split.ry[0]=MotorController::instance()->quad->RADA.ref_phi;
    MotorController::instance()->quad->quad_split.ry[1]=MotorController::instance()->quad->RADA.ref_theta;
    
    MotorController::instance()->quad->quad_split.ry[2]=vrpnDataPen->roll;
    MotorController::instance()->quad->quad_split.ry[3]=vrpnDataPen->pitch;
    

    MotorController::instance()->quad_LQG_CALC(&(MotorController::instance()->quad->quad_split));

    PWM_value_correction_roll=1*(int)MotorController::instance()->quad->quad_split.U[0];
    PWM_value_correction_pitch=1*(int)MotorController::instance()->quad->quad_split.U[1];

    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;  

    /*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;
    // */
    
    //makes sure that the PWM commands are within the valid range for the ESCs
    if (MotorController::instance()->quad->motor_1_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_1_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_2_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_2_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MAX;
    }   
    if (MotorController::instance()->quad->motor_3_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_3_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MAX;
    }
    if (MotorController::instance()->quad->motor_4_pwm<DUTY_CYCLE_MIN){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MIN;
    } else if (MotorController::instance()->quad->motor_4_pwm>DUTY_CYCLE_MAX){
        MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MAX;
    }   
    
    //sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);

}

void eris_lqg(vrpn_data_t *vrpnDataEris){
    int motors_left_correct, motors_right_correct, motors_front_correct, motors_back_correct;


    double ux, uy, uxb, uyb, ref_xb, ref_yb, dt;

    
    dt=.01;
    MotorController::instance()->quad->vrpnLast=MotorController::instance()->quad->vrpnNow;
    

    //MotorController::instance()->quad->RADA.x_dot=MotorController::instance()->dataFilter(&(MotorController::instance()->eris->xb_vel_filter), MotorController::instance()->eris->x_vel);
    //MotorController::instance()->quad->RADA.y_dot= MotorController::instance()->dataFilter(&(MotorController::instance()->eris->yb_vel_filter), MotorController::instance()->eris->y_vel);
    MotorController::instance()->eris->eris_split.ry[0]=MotorController::instance()->eris->yaw_set;
    MotorController::instance()->eris->eris_split.ry[1]=MotorController::instance()->eris->x_vel_set*.01;
    MotorController::instance()->eris->eris_split.ry[2]=MotorController::instance()->eris->y_vel_set*.01;
    MotorController::instance()->eris->eris_split.ry[4]=MotorController::instance()->eris->x_vel;
    MotorController::instance()->eris->eris_split.ry[5]=MotorController::instance()->eris->y_vel;
    
    MotorController::instance()->eris->eris_split.ry[3]=vrpnDataEris->yaw;
    

    MotorController::instance()->eris_LQG_CALC(&(MotorController::instance()->eris->eris_split));


    ux=MotorController::instance()->eris->eris_split.U[1];
    uy=MotorController::instance()->eris->eris_split.U[2];

    ref_xb= MotorController::instance()->eris->eris_split.ry[1];//MotorController::instance()->quad->RADA.ref_x*cos(vrpnDataEris->yaw)+MotorController::instance()->quad->RADA.ref_y*sin(vrpnDataEris->yaw);
    ref_yb=MotorController::instance()->eris->eris_split.ry[2];//-MotorController::instance()->quad->RADA.ref_x*sin(vrpnDataEris->yaw)+MotorController::instance()->quad->RADA.ref_y*cos(vrpnDataEris->yaw);
    
    if(ref_xb>0){
        ux+=WHEEL_DEAD_ZONE;
    }else if(ref_xb<0){
        ux-=WHEEL_DEAD_ZONE;
    }
    if(ref_yb>0){
        uy+=WHEEL_DEAD_ZONE;
    }else if(ref_yb<0){
        uy-=WHEEL_DEAD_ZONE;
    }
    uxb=ux*cos(vrpnDataEris->yaw)+uy*sin(vrpnDataEris->yaw);
    uyb=-ux*sin(vrpnDataEris->yaw)+uy*cos(vrpnDataEris->yaw);

    
    #ifdef yaw_off
    motors_left_correct=1*(int)(-uxb);
    motors_right_correct=1*(int)(uxb);
    motors_front_correct=1*(int)(-uyb);
    motors_back_correct=1*(int)(uyb);
    #else
    motors_left_correct=1*(int)(-uxb-MotorController::instance()->eris->eris_split.U[0]);
    motors_right_correct=1*(int)(uxb-MotorController::instance()->eris->eris_split.U[0]);
    motors_front_correct=1*(int)(-uyb-MotorController::instance()->eris->eris_split.U[0]);
    motors_back_correct=1*(int)(uyb-MotorController::instance()->eris->eris_split.U[0]);
    #endif
    //checks to make sure the command is within the valid range of PWM commands
    if(motors_left_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_left_correct>MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_right_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_right_correct>MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_front_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_front_correct>MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_back_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_back_correct>MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=MAX_ERIS_DUTY_CYCLE;
    }

    //sets PWM command for each motor
    groundRobotMotors.motor_speed_left = motors_left_correct;
    groundRobotMotors.motor_speed_right = motors_right_correct;
    groundRobotMotors.motor_speed_front = motors_front_correct;
    groundRobotMotors.motor_speed_rear = motors_back_correct;

   
    MotorController::instance()->setMotors(&groundRobotMotors);
    MotorController::instance()->setMotorSpeeds(); 

}
void PID_eris(vrpn_data_t *vrpnData){
    int motors_left_correct, motors_right_correct, motors_front_correct, motors_back_correct;
    #ifdef posRamp
        if(MotorController::instance()->eris->x_pre==-10000){
            MotorController::instance()->eris->x_vel=0;
            MotorController::instance()->eris->y_vel=0;
            MotorController::instance()->eris->vrpnLast = MotorController::instance()->eris->vrpnNow;
            MotorController::instance()->eris->dt=.01;
            MotorController::instance()->motors.x_vel_ctrl.desired=vrpnData->x*100;
            MotorController::instance()->motors.y_vel_ctrl.desired=vrpnData->y*100;
        }else{
            //calculates (estimates) x and y velocities in camera reference frame in meters/s
            MotorController::instance()->eris->dt=MotorController::instance()->eris->vrpnNow-MotorController::instance()->eris->vrpnLast;
            MotorController::instance()->eris->y_vel=(vrpnData->y-MotorController::instance()->eris->y_pre)/(MotorController::instance()->eris->dt);
            MotorController::instance()->motors.x_vel_ctrl.desired+=MotorController::instance()->eris->x_vel_set*(MotorController::instance()->eris->dt);
            MotorController::instance()->motors.y_vel_ctrl.desired+=MotorController::instance()->eris->y_vel_set*(MotorController::instance()->eris->dt;
           
            MotorController::instance()->eris->vrpnLast = MotorController::instance()->eris->vrpnNow; 
        }   
        MotorController::instance()->eris->xb_vel=MotorController::instance()->eris->x_vel*cos(vrpnData->yaw)+MotorController::instance()->eris->y_vel*sin(vrpnData->yaw);
        MotorController::instance()->eris->yb_vel=MotorController::instance()->eris->y_vel*cos(vrpnData->yaw)-MotorController::instance()->eris->x_vel*sin(vrpnData->yaw);
        MotorController::instance()->eris->xb_vel_filt=MotorController::instance()->eris->xb_vel;
        MotorController::instance()->eris->yb_vel_filt=MotorController::instance()->eris->yb_vel;


        MotorController::instance()->motors.x_vel_ctrl.current=vrpnData->x*100;
        MotorController::instance()->motors.y_vel_ctrl.current=vrpnData->y*100;
        MotorController::instance()->motors.yaw_ctrl.current=vrpnData->yaw;
        
        MotorController::instance()->eris->x_pre=vrpnData->x;
        MotorController::instance()->eris->y_pre=vrpnData->y;

        MotorController::instance()->pidControl_dt(&(MotorController::instance()->motors.x_vel_ctrl), MotorController::instance()->eris->dt);
        MotorController::instance()->pidControl_dt(&(MotorController::instance()->motors.y_vel_ctrl), MotorController::instance()->eris->dt);
        #ifndef yaw_off
        MotorController::instance()->pidControlFilt_dt(&(MotorController::instance()->motors.yaw_ctrl), MotorController::instance()->eris->dt);
        #endif
    #else
    /*if(MotorController::instance()->eris->x_pre==-10000){
        MotorController::instance()->eris->x_vel=0;
        MotorController::instance()->eris->y_vel=0;
        MotorController::instance()->eris->dt=.01;
        MotorController::instance()->eris->vrpnLast = MotorController::instance()->eris->vrpnNow;
    }else{
        //calculates (estimates) x and y velocities in camera reference frame in meters/s
        MotorController::instance()->eris->dt=MotorController::instance()->eris->vrpnNow-MotorController::instance()->eris->vrpnLast;
        MotorController::instance()->eris->x_vel=(vrpnData->x-MotorController::instance()->eris->x_pre)/(MotorController::instance()->eris->dt);
        MotorController::instance()->eris->y_vel=(vrpnData->y-MotorController::instance()->eris->y_pre)/(MotorController::instance()->eris->dt);
        MotorController::instance()->eris->vrpnLast = MotorController::instance()->eris->vrpnNow;
    }*/   
    #ifdef accR
    /*if(MotorController::instance()->motors.x_vel_ctrl.desiredSet==0){
        MotorController::instance()->motors.x_vel_ctrl.desired=0;
    }else */if(MotorController::instance()->motors.x_vel_ctrl.desiredSet>MotorController::instance()->motors.x_vel_ctrl.desired){
        MotorController::instance()->motors.x_vel_ctrl.desired+=MotorController::instance()->eris->vel_slope*MotorController::instance()->eris->dt;
        if(MotorController::instance()->motors.x_vel_ctrl.desired>MotorController::instance()->motors.x_vel_ctrl.desiredSet){
            MotorController::instance()->motors.x_vel_ctrl.desired=MotorController::instance()->motors.x_vel_ctrl.desiredSet;
        }
    }else if(MotorController::instance()->motors.x_vel_ctrl.desiredSet<MotorController::instance()->motors.x_vel_ctrl.desired){
        MotorController::instance()->motors.x_vel_ctrl.desired-=MotorController::instance()->eris->vel_slope*MotorController::instance()->eris->dt;
        if(MotorController::instance()->motors.x_vel_ctrl.desired<MotorController::instance()->motors.x_vel_ctrl.desiredSet){
            MotorController::instance()->motors.x_vel_ctrl.desired=MotorController::instance()->motors.x_vel_ctrl.desiredSet;
        }
    }
    /*if(MotorController::instance()->motors.y_vel_ctrl.desiredSet==0){
        MotorController::instance()->motors.y_vel_ctrl.desired=0;
    }else */if(MotorController::instance()->motors.y_vel_ctrl.desiredSet>MotorController::instance()->motors.y_vel_ctrl.desired){
        MotorController::instance()->motors.y_vel_ctrl.desired+=MotorController::instance()->eris->vel_slope*MotorController::instance()->eris->dt;
        if(MotorController::instance()->motors.y_vel_ctrl.desired>MotorController::instance()->motors.y_vel_ctrl.desiredSet){
            MotorController::instance()->motors.y_vel_ctrl.desired=MotorController::instance()->motors.y_vel_ctrl.desiredSet;
        }
    }else if(MotorController::instance()->motors.y_vel_ctrl.desiredSet<MotorController::instance()->motors.y_vel_ctrl.desired){
        MotorController::instance()->motors.y_vel_ctrl.desired-=MotorController::instance()->eris->vel_slope*MotorController::instance()->eris->dt;
        if(MotorController::instance()->motors.y_vel_ctrl.desired<MotorController::instance()->motors.y_vel_ctrl.desiredSet){
            MotorController::instance()->motors.y_vel_ctrl.desired=MotorController::instance()->motors.y_vel_ctrl.desiredSet;
        }
    }
    #endif
    //sets vervious x and y locations to currentx,y location
    //MotorController::instance()->eris->x_pre=vrpnData->x;
    //MotorController::instance()->eris->y_pre=vrpnData->y;

    //calcualtes the x and y component body velocites as this is realted to each wheel (in meters/s)
    MotorController::instance()->eris->xb_vel=MotorController::instance()->eris->x_vel*cos(vrpnData->yaw)+MotorController::instance()->eris->y_vel*sin(vrpnData->yaw);
    MotorController::instance()->eris->yb_vel=MotorController::instance()->eris->y_vel*cos(vrpnData->yaw)-MotorController::instance()->eris->x_vel*sin(vrpnData->yaw);
    
    MotorController::instance()->eris->xb_vel_filt= MotorController::instance()->dataFilter(&(MotorController::instance()->eris->xb_vel_filter), MotorController::instance()->eris->xb_vel);
    MotorController::instance()->eris->yb_vel_filt= MotorController::instance()->dataFilter(&(MotorController::instance()->eris->yb_vel_filter), MotorController::instance()->eris->yb_vel);

    MotorController::instance()->motors.x_vel_ctrl.current=MotorController::instance()->eris->xb_vel_filt*100;
    MotorController::instance()->motors.y_vel_ctrl.current=MotorController::instance()->eris->yb_vel_filt*100;
    MotorController::instance()->motors.yaw_ctrl.current=vrpnData->yaw;

    MotorController::instance()->pidControl(&(MotorController::instance()->motors.x_vel_ctrl));
    MotorController::instance()->pidControl(&(MotorController::instance()->motors.y_vel_ctrl));
    #ifndef yaw_off
    MotorController::instance()->pidControlFilt(&(MotorController::instance()->motors.yaw_ctrl));
    #endif
    #endif
    //writes current wheel velocity to the PID struct for each motor in cm/s
    /*MotorController::instance()->motors.left.openFPGA.current=-100*MotorController::instance()->eris->xb_vel_filt;
    MotorController::instance()->motors.right.openFPGA.current=100*MotorController::instance()->eris->xb_vel_filt;
    MotorController::instance()->motors.front.openFPGA.current=-100*MotorController::instance()->eris->yb_vel_filt;
    MotorController::instance()->motors.rear.openFPGA.current=100*MotorController::instance()->eris->yb_vel_filt;
    */

    //calcuates the PWM command for each motor
    /*MotorController::instance()->pidControl(&(MotorController::instance()->motors.left.openFPGA));
    MotorController::instance()->pidControl(&(MotorController::instance()->motors.right.openFPGA));
    MotorController::instance()->pidControl(&(MotorController::instance()->motors.front.openFPGA));
    MotorController::instance()->pidControl(&(MotorController::instance()->motors.rear.openFPGA));
    */
    
    motors_left_correct=(int)(-MotorController::instance()->motors.x_vel_ctrl.correction-MotorController::instance()->motors.yaw_ctrl.correction);
    motors_right_correct=(int)(MotorController::instance()->motors.x_vel_ctrl.correction-MotorController::instance()->motors.yaw_ctrl.correction);
    motors_front_correct=(int)(-MotorController::instance()->motors.y_vel_ctrl.correction-MotorController::instance()->motors.yaw_ctrl.correction);
    motors_back_correct=(int)(MotorController::instance()->motors.y_vel_ctrl.correction-MotorController::instance()->motors.yaw_ctrl.correction);
    //adds value to correcti to offset dead zone in H-Bridge
    #ifdef posRamp
    if((-MotorController::instance()->eris->x_vel_set)<0){
        motors_left_correct-=WHEEL_DEAD_ZONE;
    }else if((-MotorController::instance()->eris->x_vel_set)>0){
        motors_left_correct+=WHEEL_DEAD_ZONE;
    }
    if((MotorController::instance()->eris->x_vel_set)<0){
        motors_right_correct-=WHEEL_DEAD_ZONE;
    }else if((MotorController::instance()->eris->x_vel_set)>0){
        motors_right_correct+=WHEEL_DEAD_ZONE;
    }
    if((-MotorController::instance()->eris->y_vel_set)<0){
        motors_front_correct-=WHEEL_DEAD_ZONE;
    }else if((-MotorController::instance()->eris->y_vel_set)>0){
        motors_front_correct+=WHEEL_DEAD_ZONE;
    }
    if((MotorController::instance()->eris->y_vel_set-0*MotorController::instance()->motors.yaw_ctrl.err)<0){
        motors_back_correct-=WHEEL_DEAD_ZONE;
    }else if((MotorController::instance()->eris->y_vel_set)>0){
        motors_back_correct+=WHEEL_DEAD_ZONE;
    }
    #else
    if((-MotorController::instance()->motors.x_vel_ctrl.desired-0*MotorController::instance()->motors.yaw_ctrl.err)<0){
        motors_left_correct-=WHEEL_DEAD_ZONE;
    }else if((-MotorController::instance()->motors.x_vel_ctrl.desired-0*MotorController::instance()->motors.yaw_ctrl.err)>0){
        motors_left_correct+=WHEEL_DEAD_ZONE;
    }
    if((MotorController::instance()->motors.x_vel_ctrl.desired-0*MotorController::instance()->motors.yaw_ctrl.err)<0){
        motors_right_correct-=WHEEL_DEAD_ZONE;
    }else if((MotorController::instance()->motors.x_vel_ctrl.desired-0*MotorController::instance()->motors.yaw_ctrl.err)>0){
        motors_right_correct+=WHEEL_DEAD_ZONE;
    }
    if((-MotorController::instance()->motors.y_vel_ctrl.desired-0*MotorController::instance()->motors.yaw_ctrl.err)<0){
        motors_front_correct-=WHEEL_DEAD_ZONE;
    }else if((-MotorController::instance()->motors.y_vel_ctrl.desired-0*MotorController::instance()->motors.yaw_ctrl.err)>0){
        motors_front_correct+=WHEEL_DEAD_ZONE;
    }
    if((MotorController::instance()->motors.y_vel_ctrl.desired-0*MotorController::instance()->motors.yaw_ctrl.err)<0){
        motors_back_correct-=WHEEL_DEAD_ZONE;
    }else if((MotorController::instance()->motors.y_vel_ctrl.desired-0*MotorController::instance()->motors.yaw_ctrl.err)>0){
        motors_back_correct+=WHEEL_DEAD_ZONE;
    }
    #endif

    //checks to make sure the command is within the valid range of PWM commands
    if(motors_left_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_left_correct>MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_right_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_right_correct>MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_front_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_front_correct>MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_back_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_back_correct>MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=MAX_ERIS_DUTY_CYCLE;
    }

    //sets PWM command for each motor
    groundRobotMotors.motor_speed_left = motors_left_correct;
    groundRobotMotors.motor_speed_right = motors_right_correct;
    groundRobotMotors.motor_speed_front = motors_front_correct;
    groundRobotMotors.motor_speed_rear = motors_back_correct;  
    

    //for using Eris' wheels in open loop
    /*groundRobotMotors.motor_speed_left=MotorController::instance()->motors.left.openFPGA.desired;
    groundRobotMotors.motor_speed_right=MotorController::instance()->motors.right.openFPGA.desired;
    groundRobotMotors.motor_speed_front=MotorController::instance()->motors.front.openFPGA.desired;
    groundRobotMotors.motor_speed_rear=MotorController::instance()->motors.rear.openFPGA.desired;*/
    MotorController::instance()->setMotors(&groundRobotMotors);
    MotorController::instance()->setMotorSpeeds();  
}

void PID_eris_ol(vrpn_data_t *vrpnData){
    int motors_left_correct, motors_right_correct, motors_front_correct, motors_back_correct;

    //calcualtes the x and y component body velocites as this is realted to each wheel (in meters/s)
    MotorController::instance()->eris->xb_vel=MotorController::instance()->eris->x_vel*cos(vrpnData->yaw)+MotorController::instance()->eris->y_vel*sin(vrpnData->yaw);
    MotorController::instance()->eris->yb_vel=MotorController::instance()->eris->y_vel*cos(vrpnData->yaw)-MotorController::instance()->eris->x_vel*sin(vrpnData->yaw);
    
    MotorController::instance()->eris->xb_vel_filt= MotorController::instance()->dataFilter(&(MotorController::instance()->eris->xb_vel_filter), MotorController::instance()->eris->xb_vel);
    MotorController::instance()->eris->yb_vel_filt= MotorController::instance()->dataFilter(&(MotorController::instance()->eris->yb_vel_filter), MotorController::instance()->eris->yb_vel);

    MotorController::instance()->motors.x_vel_ctrl.current=MotorController::instance()->eris->xb_vel_filt*100;
    MotorController::instance()->motors.y_vel_ctrl.current=MotorController::instance()->eris->yb_vel_filt*100;
    MotorController::instance()->motors.yaw_ctrl.current=vrpnData->yaw;

    #ifndef yaw_off
    MotorController::instance()->pidControlFilt(&(MotorController::instance()->motors.yaw_ctrl));
    #endif
    
    motors_left_correct=(int)(-MotorController::instance()->motors.yaw_ctrl.correction);
    motors_right_correct=(int)(-MotorController::instance()->motors.yaw_ctrl.correction);
    motors_front_correct=(int)(-MotorController::instance()->motors.yaw_ctrl.correction);
    motors_back_correct=(int)(-MotorController::instance()->motors.yaw_ctrl.correction);
    
    if((-MotorController::instance()->motors.x_vel_ctrl.desired)<0){
        motors_left_correct-=WHEEL_OL_SPEED;
    }else if((-MotorController::instance()->motors.x_vel_ctrl.desired)>0){
        motors_left_correct+=WHEEL_OL_SPEED;
    }
    if((MotorController::instance()->motors.x_vel_ctrl.desired)<0){
        motors_right_correct-=WHEEL_OL_SPEED;
    }else if((MotorController::instance()->motors.x_vel_ctrl.desired)>0){
        motors_right_correct+=WHEEL_OL_SPEED;
    }
    if((-MotorController::instance()->motors.y_vel_ctrl.desired)<0){
        motors_front_correct-=WHEEL_OL_SPEED;
    }else if((-MotorController::instance()->motors.y_vel_ctrl.desired)>0){
        motors_front_correct+=WHEEL_OL_SPEED;
    }
    if((MotorController::instance()->motors.y_vel_ctrl.desired)<0){
        motors_back_correct-=WHEEL_OL_SPEED;
    }else if((MotorController::instance()->motors.y_vel_ctrl.desired)>0){
        motors_back_correct+=WHEEL_OL_SPEED;
    }

    //checks to make sure the command is within the valid range of PWM commands
    if(motors_left_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_left_correct>MAX_ERIS_DUTY_CYCLE){
        motors_left_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_right_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_right_correct>MAX_ERIS_DUTY_CYCLE){
        motors_right_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_front_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_front_correct>MAX_ERIS_DUTY_CYCLE){
        motors_front_correct=MAX_ERIS_DUTY_CYCLE;
    }
    if(motors_back_correct<-MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=-MAX_ERIS_DUTY_CYCLE;
    }else if(motors_back_correct>MAX_ERIS_DUTY_CYCLE){
        motors_back_correct=MAX_ERIS_DUTY_CYCLE;
    }

    //sets PWM command for each motor
    groundRobotMotors.motor_speed_left = motors_left_correct;
    groundRobotMotors.motor_speed_right = motors_right_correct;
    groundRobotMotors.motor_speed_front = motors_front_correct;
    groundRobotMotors.motor_speed_rear = motors_back_correct;  
    

    //for using Eris' wheels in open loop
    /*groundRobotMotors.motor_speed_left=MotorController::instance()->motors.left.openFPGA.desired;
    groundRobotMotors.motor_speed_right=MotorController::instance()->motors.right.openFPGA.desired;
    groundRobotMotors.motor_speed_front=MotorController::instance()->motors.front.openFPGA.desired;
    groundRobotMotors.motor_speed_rear=MotorController::instance()->motors.rear.openFPGA.desired;*/
    MotorController::instance()->setMotors(&groundRobotMotors);
    MotorController::instance()->setMotorSpeeds();  
}

void PID_controller(vrpn_data_t *vrpnData)
{
    int PWM_value_correction_pitch;
    int PWM_value_correction_roll;
    int PWM_value_correction_x_vel;
    float PWM_value_correction_pitch_inter;

    if(MotorController::instance()->quad->vrpnNow==MotorController::instance()->quad->vrpnLast){
        MotorController::instance()->quad->dt=.01;
    }else{
        MotorController::instance()->quad->dt=MotorController::instance()->quad->vrpnNow-MotorController::instance()->quad->vrpnLast;
        MotorController::instance()->quad->vrpnLast=MotorController::instance()->quad->vrpnNow;
    }
    // PID control
    //MotorController::instance()->quad->pitch.current = vrpnData->pitch;
    MotorController::instance()->quad->roll.current = vrpnData->roll;
    MotorController::instance()->quad->yaw.current = vrpnData->yaw;

    MotorController::instance()->pidControlFilt_dt(&MotorController::instance()->quad->roll, MotorController::instance()->quad->dt);
    PWM_value_correction_roll=1*MotorController::instance()->quad->roll.correction;
    
    //MotorController::instance()->pidControl(&MotorController::instance()->quad->roll);
    //MotorController::instance()->pidControl(&MotorController::instance()->quad->yaw);
    ///*
    //MotorController::instance()->pidControl(&MotorController::instance()->quad->pitch);
    MotorController::instance()->quad->pitch.current = vrpnData->pitch;
	MotorController::instance()->pidControlFilt_dt(&MotorController::instance()->quad->pitch, MotorController::instance()->quad->dt);
    
	//converts the PID controller output to the right magnitude 
    PWM_value_correction_pitch=1*MotorController::instance()->quad->pitch.correction;
	//*/
    
    //SF controller Pitch LQR
    /*
    MotorController::instance()->quad->theta.position = vrpnData->pitch;
    PWM_value_correction_pitch=1*(int)MotorController::instance()->stateFeedback(&MotorController::instance()->quad->theta);
    //*/

    //SF controller Pitch LQI
    /*
    MotorController::instance()->quad->thetaI.position = vrpnData->pitch;
    PWM_value_correction_pitch=1*(int)MotorController::instance()->stateFeedbackI(&MotorController::instance()->quad->thetaI);
    //*/

    //calculates the desired motor outputs based on the PWM correction value
	// /*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID + PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID + PWM_value_correction_roll;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID - PWM_value_correction_pitch;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID - PWM_value_correction_roll;
    // */
    
    /*
    MotorController::instance()->quad->motor_1_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_2_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_3_pwm = DUTY_CYCLE_MID;
    MotorController::instance()->quad->motor_4_pwm = DUTY_CYCLE_MID;
    // */
	
	//makes sure that the PWM commands are within the valid range for the ESCs
	if (MotorController::instance()->quad->motor_1_pwm<DUTY_CYCLE_MIN){
		MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MIN;
	} else if (MotorController::instance()->quad->motor_1_pwm>DUTY_CYCLE_MAX){
		MotorController::instance()->quad->motor_1_pwm=DUTY_CYCLE_MAX;
	}
	if (MotorController::instance()->quad->motor_2_pwm<DUTY_CYCLE_MIN){
		MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MIN;
	} else if (MotorController::instance()->quad->motor_2_pwm>DUTY_CYCLE_MAX){
		MotorController::instance()->quad->motor_2_pwm=DUTY_CYCLE_MAX;
	}	
	if (MotorController::instance()->quad->motor_3_pwm<DUTY_CYCLE_MIN){
		MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MIN;
	} else if (MotorController::instance()->quad->motor_3_pwm>DUTY_CYCLE_MAX){
		MotorController::instance()->quad->motor_3_pwm=DUTY_CYCLE_MAX;
	}
	if (MotorController::instance()->quad->motor_4_pwm<DUTY_CYCLE_MIN){
		MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MIN;
	} else if (MotorController::instance()->quad->motor_4_pwm>DUTY_CYCLE_MAX){
		MotorController::instance()->quad->motor_4_pwm=DUTY_CYCLE_MAX;
	}	
	
	//sets the motor outputs
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_1_pwm, 1);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_2_pwm, 2);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_3_pwm, 3);
    MotorController::instance()->setDutyCycle(MotorController::instance()->quad->motor_4_pwm, 4);

}

void tuning_UI(vrpn_data_t *vrpnData)
{
    // Clear the UI
    cout << CLEAR;

    // Display the current mode
    cout << "Current mode: \t" << ((currentMode == TUNING) ? "Tuning mode" : "Box step mode");
    cout << endl << endl;

    cout << "Change running mode:" << endl;
    cout << KeyBindings::TUNING_MODE    << ": Change to tuning mode" << endl;
    cout << KeyBindings::ROBOT_CTRL_MODE<< ": Change to robot control mode" << endl;
    cout << KeyBindings::BOX_STEP_MODE  << ": Change to box step mode" << endl;
    cout << endl;

    // Print out the tuning commands
    cout << "Tuning commands:" << endl;
    cout << KeyBindings::TUNE_PITCH     << ": Tune pitch axis       | ";
    cout << KeyBindings::TUNE_ROLL      << ": Tune roll axis" << endl;
    cout << endl;

    cout << KeyBindings::TUNE_P_UP      << ": Increase P            | ";
    cout << KeyBindings::TUNE_P_DN      << ": Decrease P" << endl;
    cout << KeyBindings::TUNE_I_UP      << ": Increase I            | ";
    cout << KeyBindings::TUNE_I_DN      << ": Decrease I" << endl;
    cout << KeyBindings::TUNE_D_UP      << ": Increase D            | ";
    cout << KeyBindings::TUNE_D_DN      << ": Decrease D" << endl;
    cout << KeyBindings::TUNE_SP_UP     << ": Increase setpoint     | ";
    cout << KeyBindings::TUNE_SP_DN     << ": Decrease setpoint" << endl;
    cout << KeyBindings::TUNE_TAW_UP    << ": Increase Taw          | ";
    cout << KeyBindings::TUNE_TAW_DN    << ": Decrease Taw"  << endl;
    cout << KeyBindings::RESET_I_COMP   << ": Reset cumulative I" << endl;
    cout << endl;

    cout << KeyBindings::ADD_LOG_MARKER << ": Add marker to logfile" << endl;
    cout << endl;

    cout << "ESC"                       << ": QUIT system"  << endl;
    cout << endl << endl;

    // Print current mode
    if (cur_axis == PITCH)
    {
        cout << "EDITING PITCH" << endl << endl;
    }
    else if (cur_axis == ROLL)
    {
        cout << "EDITING ROLL" << endl << endl;
    }

    // Display current data
    cout << "Current data:" << endl;
    cout << setprecision(3) << fixed << setw(6);
    cout << "         | Pitch:       | Roll:" << endl;
    cout << "Current  | " << vrpnData->pitch << "\t| "
                          << vrpnData->roll << endl;
    cout << "P        | " << MotorController::instance()->quad->pitch.p << "\t| "
                          << MotorController::instance()->quad->roll.p << endl;
    cout << "I        | " << MotorController::instance()->quad->pitch.i << "\t| "
                          << MotorController::instance()->quad->roll.i << endl;
    cout << "D        | " << MotorController::instance()->quad->pitch.d << "\t| "
                          << MotorController::instance()->quad->roll.d << endl;
    cout << "Setpoint | " << MotorController::instance()->quad->pitch.desired << "\t| "
                          << MotorController::instance()->quad->roll.desired << endl;
    cout << "Taw      | " << MotorController::instance()->quad->pitch.Taw << "\t| "
                          << MotorController::instance()->quad->roll.Taw << endl;
    deltT();
}

void robotControl_UI(vrpn_data_t *vrpnData)
{
    // Clear the UI
    cout << CLEAR;

    cout << "Current mode: \tRobot control" << endl;
    cout << endl;

    cout << "Change running mode:" << endl;
    cout << KeyBindings::TUNING_MODE    << ": Change to tuning mode" << endl;
    cout << KeyBindings::ROBOT_CTRL_MODE<< ": Change to robot control mode" << endl;
    cout << KeyBindings::BOX_STEP_MODE  << ": Change to box step mode" << endl;
    cout << endl;

    // Print out the keybindings
    cout << "Movement:" << endl;
    cout << KeyBindings::FORWARD        << ": forward               | ";
    cout << KeyBindings::FORWARD_RIGHT  << ": forward right" << endl;
    cout << KeyBindings::LEFT           << ": left                  | ";
    cout << KeyBindings::FORWARD_LEFT   << ": forward left" << endl;
    cout << KeyBindings::REVERSE        << ": reverse               | ";
    cout << KeyBindings::REVERSE_RIGHT  << ": reverse right" << endl;
    cout << KeyBindings::RIGHT          << ": right                 | ";
    cout << KeyBindings::REVERSE_LEFT   << ": reverse left" << endl;
    cout << KeyBindings::ROTATE_CW      << ": rotate clockwise      | ";
    cout << KeyBindings::ROTATE_CCW     << ": reverse counter-clockwise" << endl;
    cout << endl;

    cout << KeyBindings::STOP_ROBOT     << ": STOP ground robot motors" << endl;
    cout << "ESC"                       << ": QUIT system" << endl;
    cout << endl;

    cout << "Setpoint:" << endl;
    cout << KeyBindings::PITCH_SP_UP    << ": Pitch setpoint up     | ";
    cout << KeyBindings::PITCH_SP_DN    << ": Pitch setpoint down" << endl;
    cout << KeyBindings::ROLL_SP_UP     << ": Roll setpoint up      | ";
    cout << KeyBindings::ROLL_SP_DN     << ": Roll setpoint down" << endl;
    cout << endl;

    cout << KeyBindings::RESET_I_COMP   << ": Reset cumulative I" << endl;
    cout << endl;

    cout << KeyBindings::ADD_LOG_MARKER << ": Add marker to logfile" << endl;
    cout << endl;

    // Display current data
    cout << "Current data:" << endl;
    cout << setprecision(3) << fixed << setw(6);
    cout << "         | Pitch:       | Roll:" << endl;
    cout << "Current  | " << vrpnData->pitch << "\t| "
                          << vrpnData->roll << endl;
    cout << "P        | " << MotorController::instance()->quad->pitch.p << "\t| "
                          << MotorController::instance()->quad->roll.p << endl;
    cout << "I        | " << MotorController::instance()->quad->pitch.i << "\t| "
                          << MotorController::instance()->quad->roll.i << endl;
    cout << "D        | " << MotorController::instance()->quad->pitch.d << "\t| "
                          << MotorController::instance()->quad->roll.d << endl;
    cout << "Setpoint | " << MotorController::instance()->quad->pitch.desired << "\t| "
                          << MotorController::instance()->quad->roll.desired << endl;
    cout << "Taw      | " << MotorController::instance()->quad->pitch.Taw << "\t| "
                          << MotorController::instance()->quad->roll.Taw << endl;
     deltT();
}

void tuningMode()
{
    switch (cmd)
    {
        /***** Tune P *****/
        case KeyBindings::TUNE_P_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.p += 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.p += 1;
            break;
        case KeyBindings::TUNE_P_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.p -= 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.p -= 1;
            break;

        /***** Tune I *****/
        case KeyBindings::TUNE_I_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.i += 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.i += 1;
            break;
        case KeyBindings::TUNE_I_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.i -= 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.i -= 1;
            break;

        /***** Tune D *****/
        case KeyBindings::TUNE_D_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.d += 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.d += 1;
            break;
        case KeyBindings::TUNE_D_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.d -= 1;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.d -= 1;
            break;

        /***** Tune setpoint *****/
        case KeyBindings::TUNE_SP_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.desired += 0.01;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.desired += 0.01;
            break;
        case KeyBindings::TUNE_SP_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.desired -= 0.01;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.desired -= 0.01;
            break;

        /***** Tune taw *****/
        case KeyBindings::TUNE_TAW_UP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.Taw += 0.01;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.Taw += 0.01;
            break;
        case KeyBindings::TUNE_TAW_DN:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.Taw -= 0.01;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.Taw -= 0.01;
            break;

        /***** Switch tuning axis *****/
        case KeyBindings::TUNE_PITCH:
            cur_axis = PITCH;
            break;
        case KeyBindings::TUNE_ROLL:
            cur_axis = ROLL;
            break;

        /***** Reset cumulative I *****/
        case KeyBindings::RESET_I_COMP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.PID_icomp = 0;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.PID_icomp = 0;
            break;

        /***** Change mode *****/
        case KeyBindings::ROBOT_CTRL_MODE:
            currentMode = ROBOT_CONTROL;
            break;
        case KeyBindings::TUNING_MODE:
            currentMode = TUNING;
            break;
        case KeyBindings::BOX_STEP_MODE:
            currentMode = BOX_STEP;
            break;

        /***** Logfile *****/
        case KeyBindings::ADD_LOG_MARKER:
            MotorController::instance()->quad->marker++;
            break;

        /***** Quit *****/
        case KeyBindings::QUIT:
            // Stop the motors and logger
            MotorController::instance()->stop();
            Logger::instance()->stop();
            exit(0);
            break;
    }

    // Set the cmd back to 0
    pthread_mutex_lock(&mutex);
    cmd = 0;
    pthread_mutex_unlock(&mutex);
}

void robotControlMode()
{

    switch (cmd)
    {
        /***** Reset cumulative I *****/
        case KeyBindings::RESET_I_COMP:
            if (cur_axis == PITCH) MotorController::instance()->quad->pitch.PID_icomp = 0;
            else if (cur_axis == ROLL) MotorController::instance()->quad->roll.PID_icomp = 0;
            break;

        /***** Pendulum setpoint *****/
        case KeyBindings::PITCH_SP_UP:
            MotorController::instance()->quad->pitch.desired += 0.01;
            break;
        case KeyBindings::PITCH_SP_DN:
            MotorController::instance()->quad->pitch.desired -= 0.01;
            break;
        case KeyBindings::ROLL_SP_UP:
            MotorController::instance()->quad->roll.desired += 0.01;
            break;
        case KeyBindings::ROLL_SP_DN:
            MotorController::instance()->quad->roll.desired -= 0.01;
            break;

        /***** Logfile *****/
        case KeyBindings::ADD_LOG_MARKER:
            MotorController::instance()->quad->marker++;
            break;

        /***** Change mode *****/
        case KeyBindings::TUNING_MODE:
            currentMode = TUNING;
            break;
        case KeyBindings::BOX_STEP_MODE:
            currentMode = BOX_STEP;
            break;

        /***** Ground Robot *****/
        case KeyBindings::STOP_ROBOT:
            //MotorController::instance()->stopGroundRobot();
            MotorController::instance()->eris->x_vel_set=0;
            MotorController::instance()->eris->y_vel_set=0;
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=0;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=0;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=0;
            MotorController::instance()->motors.y_vel_ctrl.desired=0;
            #endif
            MotorController::instance()->quad->RADA.ref_x=0;
            MotorController::instance()->quad->RADA.ref_y=0;
            MotorController::instance()->quad->RADA_pack.slope_x=0;
            MotorController::instance()->quad->RADA_pack.slope_y=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=0;
            break;
        case KeyBindings::FORWARD:
            MotorController::instance()->eris->x_vel_set=MAX_SPEED;
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=0;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desired=0;
            #endif
            MotorController::instance()->eris->y_vel_set=0;
            MotorController::instance()->quad->RADA.ref_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA.ref_y=0;
            MotorController::instance()->quad->RADA_pack.slope_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_y=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=0;
            break;
        case KeyBindings::RIGHT:   // Move right
            MotorController::instance()->eris->x_vel_set=0;
            MotorController::instance()->eris->y_vel_set=MAX_SPEED;
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=MAX_SPEED;
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=0;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=0;
            MotorController::instance()->motors.y_vel_ctrl.desired=MAX_SPEED;
            #endif
            MotorController::instance()->quad->RADA.ref_x=0;
            MotorController::instance()->quad->RADA.ref_y=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_x=0;
            MotorController::instance()->quad->RADA_pack.slope_y=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=-((double)MAX_SPEED)*.01;
            break;
        case KeyBindings::REVERSE:   // Move backwards
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=-MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=0;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=-MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desired=0;
            #endif
            MotorController::instance()->eris->x_vel_set=-MAX_SPEED;
            MotorController::instance()->eris->y_vel_set=0;
            MotorController::instance()->quad->RADA.ref_y=0;
             MotorController::instance()->quad->RADA.ref_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_y=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=0;
            break;
        case KeyBindings::LEFT:
            MotorController::instance()->eris->x_vel_set=0;
            MotorController::instance()->eris->y_vel_set=-MAX_SPEED;
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=0;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=-MAX_SPEED;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=0;
            MotorController::instance()->motors.y_vel_ctrl.desired=-MAX_SPEED;
            #endif
            MotorController::instance()->quad->RADA.ref_x=0;
            MotorController::instance()->quad->RADA.ref_y=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_x=0;
            MotorController::instance()->quad->RADA_pack.slope_y=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=((double)MAX_SPEED)*.01;
            break;
        case KeyBindings::FORWARD_RIGHT:
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=MAX_SPEED;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desired=MAX_SPEED;
            #endif
            MotorController::instance()->eris->x_vel_set=MAX_SPEED;
            MotorController::instance()->eris->y_vel_set=MAX_SPEED;
            MotorController::instance()->quad->RADA.ref_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA.ref_y=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_y=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=-((double)MAX_SPEED)*.01;
            break;
        case KeyBindings::FORWARD_LEFT:
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=-MAX_SPEED;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desired=-MAX_SPEED;
            #endif
            MotorController::instance()->eris->x_vel_set=MAX_SPEED;
            MotorController::instance()->eris->y_vel_set=-MAX_SPEED;
            MotorController::instance()->quad->RADA.ref_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA.ref_y=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_y=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=((double)MAX_SPEED)*.01;
            break;
        case KeyBindings::REVERSE_RIGHT:
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=-MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=MAX_SPEED;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=-MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desired=MAX_SPEED;
            #endif
            MotorController::instance()->eris->y_vel_set=MAX_SPEED;
            MotorController::instance()->eris->x_vel_set=-MAX_SPEED;
            MotorController::instance()->quad->RADA.ref_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA.ref_y=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_y=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=-((double)MAX_SPEED)*.01;
            break;
        case KeyBindings::REVERSE_LEFT:
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=-MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=-MAX_SPEED;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=-MAX_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desired=-MAX_SPEED;
            #endif
            MotorController::instance()->eris->x_vel_set=-MAX_SPEED;
            MotorController::instance()->eris->y_vel_set=-MAX_SPEED;
            MotorController::instance()->quad->RADA.ref_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA.ref_y=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_x=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack.slope_y=-((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=((double)MAX_SPEED)*.01;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=((double)MAX_SPEED)*.01;
            break;
        case KeyBindings::ROTATE_CW:
            MotorController::instance()->eris->x_vel_set= -MAX_ROT_SPEED;
            MotorController::instance()->eris->y_vel_set=-MAX_ROT_SPEED;
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=-MAX_ROT_SPEED;
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=-MAX_ROT_SPEED;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=-MAX_ROT_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desired=-MAX_ROT_SPEED;
            #endif
            MotorController::instance()->quad->RADA.ref_x=0;
            MotorController::instance()->quad->RADA.ref_y=0;
            MotorController::instance()->quad->RADA_pack.slope_x=0;
            MotorController::instance()->quad->RADA_pack.slope_y=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=0;
            break;
        case KeyBindings::ROTATE_CCW:
            MotorController::instance()->eris->x_vel_set= MAX_ROT_SPEED;
            MotorController::instance()->eris->y_vel_set= MAX_ROT_SPEED;
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=MAX_ROT_SPEED;
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=MAX_ROT_SPEED;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=MAX_ROT_SPEED;
            MotorController::instance()->motors.y_vel_ctrl.desired=MAX_ROT_SPEED;
            #endif
            MotorController::instance()->quad->RADA.ref_x=0;
            MotorController::instance()->quad->RADA.ref_y=0;
            MotorController::instance()->quad->RADA_pack.slope_x=0;
            MotorController::instance()->quad->RADA_pack.slope_y=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=0;
            break;
        case KeyBindings::QUIT:
            // Stop the motors and logger
            MotorController::instance()->stop();
            Logger::instance()->stop();
            #if defined(accR) || defined(posRamp)
            MotorController::instance()->motors.x_vel_ctrl.desiredSet=0;
            MotorController::instance()->motors.y_vel_ctrl.desiredSet=0;
            #else
            MotorController::instance()->motors.x_vel_ctrl.desired=0;
            MotorController::instance()->motors.y_vel_ctrl.desired=0;
            #endif
            MotorController::instance()->eris->x_vel_set=0;
            MotorController::instance()->eris->y_vel_set=0;
            MotorController::instance()->quad->RADA.ref_x=0;
            MotorController::instance()->quad->RADA.ref_y=0;
            MotorController::instance()->quad->RADA_pack.slope_x=0;
            MotorController::instance()->quad->RADA_pack.slope_y=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_x=0;
            MotorController::instance()->quad->RADA_pack_loss.slope_y=0;
            exit(0);
            break;
        case '<':
            MotorController::instance()->motors.yaw_ctrl.desired-=yawInc;
            MotorController::instance()->eris->yaw_set-=yawInc;
            MotorController::instance()->quad->RADA.ref_psi-=yawInc;
            MotorController::instance()->quad->RADA_pack.ref_psi-=yawInc;
            MotorController::instance()->quad->RADA_pack_loss.ref_psi+=yawInc;
            break;
        case '>':
            MotorController::instance()->motors.yaw_ctrl.desired+=yawInc;
            MotorController::instance()->eris->yaw_set+=yawInc;
            MotorController::instance()->quad->RADA.ref_psi+=yawInc;
            MotorController::instance()->quad->RADA_pack.ref_psi+=yawInc;
            MotorController::instance()->quad->RADA_pack_loss.ref_psi-=yawInc;
            break;
    };

    // Set the cmd back to 0
    pthread_mutex_lock(&mutex);
    cmd = 0;
    pthread_mutex_unlock(&mutex);
}

/**
 * This function performs a demo by moving in a box formation (first moves to a corner from center,
 * then steps around in a square). It takes 9 steps to be centered again.
 *
 * This function uses the following global variables"
 *  nextChange  - Time in seconds of the next state change
 *  state       - The current state - the position in the box
 *  boxSize     - Angle size in radians of the box
 *  changeRate  - Time in seconds between each state change
 *
 * @param curTime  is the current vrpn time
 */
void boxStepMode(double curTime)
{
    // Initialize if it is the first time here
    if (nextChange == 0)
    {
        nextChange = curTime;
    }
    else if (nextChange <= curTime)
    {
        nextChange = curTime += changeRate;
        switch (state)
        {
            //1D roll
            /*case 0:
                state = 1;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 1:
                state = 2;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
            case 2:
                state = 3;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 3:
                state = 0;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = boxSize;
                break;
                */
            //1D pitch
            case 0:
                state = 1;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 1:
                state = 2;
                MotorController::instance()->quad->roll.desired = 0.0;
                MotorController::instance()->quad->pitch.desired  = boxSize;
                break;
            case 2:
                state = 3;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 3:
                state = 0;
                MotorController::instance()->quad->roll.desired = 0.0;
                MotorController::instance()->quad->pitch.desired  = -boxSize;
                break;
            //2D box
            /*case 0:
                state = 1;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 1:
                state = 2;
                MotorController::instance()->quad->pitch.desired = -boxSize;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
            case 2:
                state = 3;
                MotorController::instance()->quad->pitch.desired = -boxSize;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 3:
                state = 4;
                MotorController::instance()->quad->pitch.desired = -boxSize;
                MotorController::instance()->quad->roll.desired  = boxSize;
                break;
            case 4:
                state = 5;
                MotorController::instance()->quad->pitch.desired = 0;
                MotorController::instance()->quad->roll.desired  = boxSize;
                break;
            case 5:
                state = 6;
                MotorController::instance()->quad->pitch.desired = boxSize;
                MotorController::instance()->quad->roll.desired  = boxSize;
                break;
            case 6:
                state = 7;
                MotorController::instance()->quad->pitch.desired = boxSize;
                MotorController::instance()->quad->roll.desired  = 0.0;
                break;
            case 7:
                state = 8;
                MotorController::instance()->quad->pitch.desired = boxSize;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
            case 8:
                state = 9;
                MotorController::instance()->quad->pitch.desired = 0.0;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
            case 9:
                state = 0;
                MotorController::instance()->quad->pitch.desired = -boxSize;
                MotorController::instance()->quad->roll.desired  = -boxSize;
                break;
                */
                
        }
    }
}

/**
 * Lods the configuration file in the Configuration class
 *
 * @return bool value that for success or failure
 */
bool loadConfigFile()
{
    char config_filename[64];

    // Handle Ctrl+c so we we can shutdown gracefully
    signal(SIGINT, &ctrlc_handler);

    // Attempt to load the config file
    FILE *fp = fopen("/root/config_file", "r");
    if (fp != NULL)
    {
        fgets(config_filename, 64, fp);
        fclose(fp);
        while (strlen(config_filename) > 0 &&
                (config_filename[strlen(config_filename) - 1] == ' ' ||
                 config_filename[strlen(config_filename) - 1] == '\t' ||
                 config_filename[strlen(config_filename) - 1] == '\n' ||
                 config_filename[strlen(config_filename) - 1] == '\r'))
        {
            config_filename[strlen(config_filename) - 1] = 0;
        }
    }

    printf("Loading config \"%s\"...\n", config_filename);
    if (!Configuration::instance()->load(config_filename))
    {
        fprintf(stderr, "Fatal Error: Could not load Korebot config file.\n");
        return false;
    }

    return true;
}

/**
 * This thread just waits for user input (in the form of a single keypress). Once input is received,
 * a global variable for the key is set so that the main thread can access the key that was pressed.
 *
 * @param threadID is the thread ID
 * @return NULL since it shouldn't end in our case
 */
void *UIThread(void *threadID)
{
    char temp;

    // Loop forever while getting input
    while (1)
    {
        temp = getch();
        pthread_mutex_lock(&mutex);
        cmd = temp;
        pthread_mutex_unlock(&mutex);
        usleep(1000);
    }
    return NULL;
}
void *controlLoop(void *threadID){
    
    while (1)
    {
        //pthread_mutex_lock(&mutex_vrpn);
        randomData = fopen("/dev/urandom", "r");
        randomDataLen = 0;
        
        while (randomDataLen < sizeof myRandomData)
        {
            result = fread( myRandomData+randomDataLen, 1,  (sizeof myRandomData) - randomDataLen, randomData);
            if (result < 0)
            {
                // error, unable to read /dev/random 
            }
            randomDataLen += result;
        }
        //pthread_mutex_unlock(&mutex_vrpn);
        fclose(randomData);
      /* pthread_mutex_lock(&mutex);
            if(controlMode!=STOPPED){
                pthread_mutex_lock(&mutex_vrpn);
                RADA_lqi(&MotorController::instance()->quad->vrpnData, &(MotorController::instance()->eris->vrpnData));
                //PID_controller(&(MotorController::instance()->quad->vrpnData));
                //PID_eris(&(MotorController::instance()->eris->vrpnData));
                Logger::instance()->logData();
                pthread_mutex_unlock(&mutex_vrpn);
            }
        pthread_mutex_unlock(&mutex);*/
        
        usleep(10000);
    }
    return NULL;
}
/**
 * This is a ctrl+c handler. It sets app_shutdown to 1, which should shut down the program.
 * I am not sure of the specifics because this was written before the May15-27 team
 *
 * @param sig the signal that it receives when the key is pressed
 */
static void ctrlc_handler(int sig)
{
    app_shutdown = 1;
}
