#ifndef _KOREBOT__MOTOR_CONTROLLER_H_
#define _KOREBOT__MOTOR_CONTROLLER_H_

#include <fstream>
#include <sstream>

#include "motor_controller_common.h"
#include "Network/packets/motor_control.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
//#include "rtwtypes.h"
//#include "stateMod_types.h"

extern void stateMod(const double u[13], double y[13]);
extern void stateMod_initialize(void);
extern void stateMod_terminate(void);

/****************************************** New Code ******************************************/
#define SAMPLE_PERIOD       0.01 // seconds
#define PID_SATURATION_MIN  -500
#define PID_SATURATION_MAX  500
/****************************************** New Code ******************************************/

class MotorController
{
private:
    // This is a singleton object, so here's the instance to the object
    static MotorController *s_instance;

    /**
     * Creates the MotorController object. It is private to prevent someone from destroying the
     * current instance.
     * Nothing is requested of the motor controller board at this ponint.
     */
    MotorController();

    /**
     * Frees any resources used by the motor controller.
     */
    ~MotorController();

public:
    /**
     * Returns a single instance of the MotorController.
     *
     * @returns A single instance of the MotorController
     */
    static MotorController *instance() {
        if (s_instance == NULL) {
            s_instance = new MotorController();
        }
        return s_instance;
    }

private:
    // The current speeds of the ground robot motors
    struct motor_control_manual_t motor_speeds;
    struct motor_control_manual_t current_motor_speeds;

    // The current mode we are controlling the motors
    char current_mode;

    // 1, if the motors should stop spinning, 0 otherwise
    char e_stop;

    short min_x;
    short min_y;
    short max_x;
    short max_y;
    int use_bounds_limits;
    int use_status_logging;

    // Structures to hold the settings for the different
    // control methods

    struct motor_control_speed_t current_speed_control_settings;
    struct motor_control_manual_t current_manual_control_settings;

public:
    // Contains configuration information about the motors
    struct motortrain_t motors;

    /****************************************** New Code ******************************************/
    QUAD* quad;
    ERIS* eris;
    void initAllQuadMotors();
    void initQuadMotor(int motorNum);
    void setDutyCycle(int pwmgen_value, int motor_num);
    double pidControl(QUADPID *pidptr);
    double pidControlFilt(QUADPID *pidptr);
    int loadConfigurationFile();
    void armQuadMotors();
    float dataFilter(struct filter *filter, float data);
    double stateFeedback(QUADSS *ssptr);
    double stateFeedbackI(QUADSSI *ssptr);
    int RADA_LQI(RADALQI *lqiptr);
    int SS_CTRL_CALC(CTRLSS *ctrlss);
    int SSctrl_config();
    int SSctrl_config_noTop();
    int SS_CTRL_CALC_loss(CTRLSS_loss *ctrlss);
    int SSctrl_config_loss();
    double pidControlFilt_dt(QUADPID *pidptr, double dt);
    int RADA_LQI_dt(RADALQI *lqiptr, double dt);
    int RADA_EST_LQI(RADALQI *lqiptr);
    int LQG_CALC(RADALQG *ctrlss);
    int eris_LQG_CALC(ERIS_LQG *ctrlss);
    int quad_LQG_CALC(QUAD_LQG *ctrlss);
    /****************************************** New Code ******************************************/

    /**
     * Initializes the MotorController.  If the mode is anything other
     * than free, then the controller will attempt to acquire the motor
     * device descriptors as defined in the Configuration object.
     *
     * @returns 1 on success, 0 otherwise
     */
    int init();

    /**
     * Shuts the motor controller down, destroying any resources & releasing
     * any devices the controller is holding.
     */
    void shutdown();

    /*
     * This will stop all quad and ground robot motors
     */
    void stop();

    /*
     * This will stop all ground robot motors.  This is different from setMotors() because
     * the controller will still maintain the connection to the motor
     * device descriptors, but the motors will be stopped.
     */
    void stopGroundRobot();

    /**
     * This will set the motors to free mode.  In free mode, the
     * association with the motor device descriptors is still maintained,
     * but the speeds of the motors are never set, allowing other programs
     * to set the motor speeds, and this program acts as a sensor for the motor
     * encoders
     */
    void setMotors();

    /**
     * This will run the motors in speed-profile mode, moving with the
     * rotational speed & in the direction given.
     *
     * @param command The parameters to control the motors
     */
    void setMotors(const struct motor_control_speed_t *command);

    /**
     * This will control the motors in a manual way, such that
     * each motor can be controlled individually.
     *
     * @param command The parameters for the movement command.
     */
    void setMotors(const struct motor_control_manual_t *command);

    /**
     * Runs one iteration of the motor controller; Sets
     * the speeds of the motors according to how the user / AI
     * wants them to go.
     *
     * @param dt The time difference since the last call to this method
     */
    void run(float dt);

    /**
     * Processes a packet received over the network
     *
     * @param packet The packet received from the network
     */
    void processPacket(MotorControl *packet);

    /**
     * Sends status information about the sensor manager to the control server.
     *
     * This includes the mode & speeds of the motors.
     */
    void sendConfig();

    /**
     * Prints the status of various modules on the Korebot
     */
    void printStatus();

    /**
     * Gets if the motor controller is using bounds limiting
     *
     * @returns 1, if the korebot stops the motors when it leaves the bounds of the playing field, 0 otherwise
     */
    int getUseBoundsLimit();

    /**
     * Gets if the motor controller is logging status
     *
     * @returns 1, if the motor controller is logging, 0 otherwise
     */
    int getUseStatusLogging();

    /**
     * Sets if the motor controller should stop the motors if the korebot leaves the bounds of the playing field
     *
     * @param useBoundsLimit 1, if the korebot should stay in-bounds, 0 otherwise
     */
    void setUseBoundsLimit(int useBoundsLimit);

    /**
     * Sets if the motor controller should log the statuse
     *
     * @param useStatusLogging 1, if the motor controller should log, 0 otherwise
     */
    void setUseStatusLogging(int useStatusLogging);

private:
    /**
     * Initializes one motor
     *
     * @param motor The motor structure to initialize
     */
    void initMotor(struct motor_t &motor);

    /**
     * Initializes all the motors
     */
    void initAllMotors();

public:
    /**
     * Passes the motor speeds onto the motor board.  After
     * this command has been executed, the robot will begin to
     * move.
     */
     // TODO: made this public
    void setMotorSpeeds();

private:
    /**
     * Prints the status of the given motor to console
     *
     * @param name  A human-readable description of the motor
     * @param motor The motor details
     */
    void printMotorStatus(const char *name, const motor_t &motor);

    /**
     * Computes the output of sin(degree)*256 using a lookup table
     *
     * @returns (int)(sin(degree)*256
     */
    static int sin_256(unsigned int degree) {
        static int sin_values[360] = {0, 4, 9, 13, 18, 22, 27, 31, 36, 40, 44, 49, 53, 58, 62, 66, 71, 75, 79, 83, 88, 92, 96, 100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 139, 143, 147, 150, 154, 158, 161, 165, 168, 171, 175, 178, 181, 184, 187, 190, 193, 196, 199, 202, 204, 207, 210, 212, 215, 217, 219, 222, 224, 226, 228, 230, 232, 234, 236, 237, 239, 241, 242, 243, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 254, 255, 255, 255, 256, 256, 256, 256, 256, 256, 256, 255, 255, 255, 254, 254, 253, 252, 251, 250, 249, 248, 247, 246, 245, 243, 242, 241, 239, 237, 236, 234, 232, 230, 228, 226, 224, 222, 219, 217, 215, 212, 210, 207, 204, 202, 199, 196, 193, 190, 187, 184, 181, 178, 175, 171, 168, 165, 161, 158, 154, 150, 147, 143, 139, 136, 132, 128, 124, 120, 116, 112, 108, 104, 100, 96, 92, 88, 83, 79, 75, 71, 66, 62, 58, 53, 49, 44, 40, 36, 31, 27, 22, 18, 13, 9, 4, 0, -4, -9, -13, -18, -22, -27, -31, -36, -40, -44, -49, -53, -58, -62, -66, -71, -75, -79, -83, -88, -92, -96, -100, -104, -108, -112, -116, -120, -124, -128, -132, -136, -139, -143, -147, -150, -154, -158, -161, -165, -168, -171, -175, -178, -181, -184, -187, -190, -193, -196, -199, -202, -204, -207, -210, -212, -215, -217, -219, -222, -224, -226, -228, -230, -232, -234, -236, -237, -239, -241, -242, -243, -245, -246, -247, -248, -249, -250, -251, -252, -253, -254, -254, -255, -255, -255, -256, -256, -256, -256, -256, -256, -256, -255, -255, -255, -254, -254, -253, -252, -251, -250, -249, -248, -247, -246, -245, -243, -242, -241, -239, -237, -236, -234, -232, -230, -228, -226, -224, -222, -219, -217, -215, -212, -210, -207, -204, -202, -199, -196, -193, -190, -187, -184, -181, -178, -175, -171, -168, -165, -161, -158, -154, -150, -147, -143, -139, -136, -132, -128, -124, -120, -116, -112, -108, -104, -100, -96, -92, -88, -83, -79, -75, -71, -66, -62, -58, -53, -49, -44, -40, -36, -31, -27, -22, -18, -13, -9, -4};
        return sin_values[degree % 360];
    };

    /**
     * Computes the output of cos(degree)*256 using a lookup table
     *
     * @returns (int)(cos(degree)*256
     */
    static int cos_256(unsigned int degree) {
        static int cos_values[360] = {256, 256, 256, 256, 255, 255, 255, 254, 254, 253, 252, 251, 250, 249, 248, 247, 246, 245, 243, 242, 241, 239, 237, 236, 234, 232, 230, 228, 226, 224, 222, 219, 217, 215, 212, 210, 207, 204, 202, 199, 196, 193, 190, 187, 184, 181, 178, 175, 171, 168, 165, 161, 158, 154, 150, 147, 143, 139, 136, 132, 128, 124, 120, 116, 112, 108, 104, 100, 96, 92, 88, 83, 79, 75, 71, 66, 62, 58, 53, 49, 44, 40, 36, 31, 27, 22, 18, 13, 9, 4, 0, -4, -9, -13, -18, -22, -27, -31, -36, -40, -44, -49, -53, -58, -62, -66, -71, -75, -79, -83, -88, -92, -96, -100, -104, -108, -112, -116, -120, -124, -128, -132, -136, -139, -143, -147, -150, -154, -158, -161, -165, -168, -171, -175, -178, -181, -184, -187, -190, -193, -196, -199, -202, -204, -207, -210, -212, -215, -217, -219, -222, -224, -226, -228, -230, -232, -234, -236, -237, -239, -241, -242, -243, -245, -246, -247, -248, -249, -250, -251, -252, -253, -254, -254, -255, -255, -255, -256, -256, -256, -256, -256, -256, -256, -255, -255, -255, -254, -254, -253, -252, -251, -250, -249, -248, -247, -246, -245, -243, -242, -241, -239, -237, -236, -234, -232, -230, -228, -226, -224, -222, -219, -217, -215, -212, -210, -207, -204, -202, -199, -196, -193, -190, -187, -184, -181, -178, -175, -171, -168, -165, -161, -158, -154, -150, -147, -143, -139, -136, -132, -128, -124, -120, -116, -112, -108, -104, -100, -96, -92, -88, -83, -79, -75, -71, -66, -62, -58, -53, -49, -44, -40, -36, -31, -27, -22, -18, -13, -9, -4, 0, 4, 9, 13, 18, 22, 27, 31, 36, 40, 44, 49, 53, 58, 62, 66, 71, 75, 79, 83, 88, 92, 96, 100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 139, 143, 147, 150, 154, 158, 161, 165, 168, 171, 175, 178, 181, 184, 187, 190, 193, 196, 199, 202, 204, 207, 210, 212, 215, 217, 219, 222, 224, 226, 228, 230, 232, 234, 236, 237, 239, 241, 242, 243, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 254, 255, 255, 255, 256, 256, 256};
        return cos_values[degree % 360];
    };

    friend class KorebotAI;
    friend class SensorManager;
};

#endif  // _KOREBOT__MOTOR_CONTROLLER_H_
