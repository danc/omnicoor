
#include "vrpn.h"
#include <unistd.h>

vrpn_Connection *connection;
vrpn_Tracker_Remote *tracker;
vrpn_Tracker_Remote *tracker1;

void vrpn_init(std::string connectionName, void (*callback)(void*, const vrpn_TRACKERCB), void (*callback1)(void*, const vrpn_TRACKERCB))
{
    connection = vrpn_get_connection_by_name(connectionName.c_str());
    tracker = new vrpn_Tracker_Remote("UAV", connection);
	tracker1 = new vrpn_Tracker_Remote("ERIS", connection);

    tracker->register_change_handler(0, callback);
	tracker1->register_change_handler(0, callback1);
    usleep(2000);
}

void vrpn_go()
{
    while (1)
    {
        connection->mainloop();
        tracker1->mainloop();
        tracker->mainloop();
        usleep(1000);
    }
}

