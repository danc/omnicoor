#include "korebot_ai.h"

#include "configuration.h"
#include "sensor_manager.h"
#include "localization_system.h"
#include "motor_controller.h"
#include "external_sensors.h"
#include "korebot.h"

virtual int KorebotAI::init(Configuration *configuration, SensorManager *sensors, LocalizationSystem *localization_system,
                 MotorController *motor_controller, ExternalSensors *external_sensors, Korebot *korebot) {

    // Set the singleton instances of all the global objects
    SensorManager::s_instance = sensors;
    LocalizationSystem::s_instance = localization_system;
    MotorController::s_instance = motor_controller;
    ExternalSensors::s_instance = external_sensors;
    Korebot::s_instance = korebot;
    return 1;
}