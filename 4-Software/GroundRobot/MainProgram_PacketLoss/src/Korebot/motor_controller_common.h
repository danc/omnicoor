#ifndef _KOREBOT__MOTOR_CONTROLLER_COMMON_H_
#define _KOREBOT__MOTOR_CONTROLLER_COMMON_H_
#include "vrpn.h"
#include "RADA_est.h"
const int MOTOR_CONFIG_NAME_LEN = 40;
//#define LQI_strong
#include "RADA_split_LQG1.h"

#ifdef LQI_strong
static const double K[5][13] = {{137250,    -2208,  0,  118,    7325,   37593,  -605,    0, -131832,    2120,   0,  27, 1655},
{2208,  137250, 0,  -7325,  118,    605,    37593,  0,  -2120,  -131832,    0,  -1655,  27},
{0, 0,  48746,  0,  0,  0,  0,  2440,   0,  0,  -135130,    0,  0},
{0, 11037,  0,  146235, 0,  0,  -1625,  0,  0,  4006,   0,  -793095,    0},
{-11037,    0,  0,  0,  146235, 1625,   0,  0,  -4006,  0,  0,  0,  -793095}};
#else
static const double K[5][13] = {{89246, -1435,  0,  33, 2065,   26120,  -420,   0,  -78669, 1265,   0,  532,    33092},
{1435,  89246,  0,  -2065,  33, 420,    26120,  0, -1265,  -78669, 0,  -33092, 532},
{0, 0,  34251,  0,  0,  0,  0,  1848,   0,  0,  -43274, 0,  0},
{0, 10700,  0,  133545, 0,  0,  -1685,  0,  0,  4740,   0,  -802320,    0},
{-10700,    0,  0,  0,  133545, 1685,   0,  0,  -4740,  0,  0,  0,  -802320}};
/*static const double K[5][13] = {{72591,    -1168,  0,  19, 1211,   25144,  -404,   0,  -27027, 435,    0,  587,    36488},
{1168,  72591,  0,  -1211,  19, 404,    25144,  0,  -435,   -27027, 0,  -36488, 587},
{0, 0,  34251,  0,  0,  0,  0,  1848,   0,  0,  -43274, 0,  0},
{0, 11989,  0,  133491, 0,  0,  -1561,  0,  0,  1737,   0,  -802071,    0},
{-11989,    0,  0,  0,  133491, 1561,   0,  0,  -1737,  0,  0,  0,  -802071}};*//*{{74399,    -1197,  0,  40, 2517,   27352,  -440,   0,  -26914, 433,    0,  471,    29296},
{1197,  74399,  0,  -2517,  40, 440,    27352,  0,  -433,   -26914, 0,  -29296, 471},
{0, 0,  34251,  0,  0,  0,  0,  1848,   0,  0,  -43274, 0,  0},
{0, 12297,  0,  133566, 0,  0,  -1518,  0,  0,  1535,   0,  -802403,    0},
{-12297,    0,  0,  0,  133566, 1518,   0,  0,  -1535,  0,  0,  0,  -802403}};/*{{60240,  -969,    0,   106, 6599,    24423,   -393,    0,   -17141,  276, 0,   -21, -1297},
{969,    60240,   0,   -6599,   106, 393, 24423,   0,   -276,    -17141,  0,   1297,    -21},
{0,  0,   34251,   0,   0,   0,   0,   1848,    0,   0,   -43274,  0,   0},
{0,  14656,   0,   17274,   0,   0,   2530,    0,   0,   -62, 0,   -76739,  0},
{-14656, 0,   0,   0,   17274,   -2530,   0,   0,   62,  0,   0,   0,   -76739}};*/
#endif
#ifdef ssMotors
    #define STATES_C 14
    #define INPUTS_C 5
    #define OUTPUTS_C 5
#else
    #ifdef ssTracking
        #define STATES_C 13
        #define INPUTS_C 8
        #define OUTPUTS_C 5
    #else
        #define STATES_C 10
        #define INPUTS_C 5
        #define OUTPUTS_C 5
    #endif
#endif

#include "RADA_LQG_s2.h"

#define STATES_C_LOSS 13 
#define INPUTS_C_LOSS 13
#define OUTPUTS_C_LOSS 13
#define OUT1_C_LOSS 5
#define OUT2_C_Loss 8
#define INPUT1_C_LOSS 5
#define INPUT2_C_LOSS 8

#ifdef drop10
#include "packLoss10.h"
#else
#ifdef drop30
#include "packLoss30.h"
#else
#ifdef drop50
#include "packLoss50.h"
#else
#include "packLoss0.h"
#endif
#endif
#endif

/************************* Ground Robot *************************/

struct motor_control_speed_t {
    int heading;
    int linear_speed;
    int rotational_velocity;
};

struct motor_control_manual_t {
    int motor_speed_front;
    int motor_speed_left;
    int motor_speed_right;
    int motor_speed_rear;
    
};

struct filter{
    float filterPre;
    float inputPre;
    float inputGain;
    float outputGain;
    float inputPreGain;
};
typedef struct
{
    double x[STATES_C];
    double x_next[STATES_C];
    double A[STATES_C][STATES_C];
    double B[STATES_C][INPUTS_C];
    double C[OUTPUTS_C][STATES_C];
    double D[OUTPUTS_C][INPUTS_C];
    double Y[OUTPUTS_C];
    double u[INPUTS_C];
    double slope_x;
    double slope_y;
    double ref_x;
    double ref_y;
    double ref_phi;
    double ref_theta;
    double ref_psi;
}CTRLSS;

typedef struct
{
    double x[STATES_C_LOSS];
    double x_next[STATES_C_LOSS];
    double Y[OUTPUTS_C_LOSS];
    double u[INPUT2_C_LOSS];
    double Xi_a[OUT1_C_LOSS];
    double Xi_s[INPUT2_C_LOSS];
    double V[INPUTS_C_LOSS];
    double Yo[OUT1_C_LOSS];
    double slope_x;
    double slope_y;
    double ref_x;
    double ref_y;
    double ref_phi;
    double ref_theta;
    double ref_psi;
}CTRLSS_loss;

typedef struct 
{
    double x[6];
    double x_next[6];
    double ry[4];
    double U[2];
    double ref_theta;
    double ref_phi;
}QUAD_LQG;

typedef struct 
{
    double x[7];
    double x_next[7];
    double ry[6];
    double U[3];
    double ref_psi;
    double ref_x;
    double ref_y; 
}ERIS_LQG;
    

typedef struct
{
    double err;
    double preverr;
    double prevI;
    double prevD;
    double prevSatDiff;     // Previous difference to show saturation (used for anti-windup)
    double correction;
    double PID_pcomp;
    double PID_icomp;
    double PID_dcomp;
    double current;
    double desired;
    double desiredSet;
    double filtError;
    double filtErrorPre;
    double timeSum;
    // These are kp, ki, and kd. They are not named such because there are #defines in some of
    // the MESA board includes with those names.
    double p;
    double i;
    double d;
    double Taw;
    double filtGain;
    double errorGain;
} QUADPID;
typedef struct 
{
    double position;
    double positionGain;
    double velGain;
    double errorGain;
    double filtError;
    double filtErrorPre;  
    double filtGain;
    double correction;

}QUADSS;

typedef struct 
{
    double position;
    double positionGain;
    double velGain;
    double errorGain;
    double filtError;
    double filtErrorPre;  
    double filtGain;
    double correction;
    double reference;
    double preI;
    double Igain;

}QUADSSI;

typedef struct{
    //states of full system LQI
    double phi;
    double theta;
    double psi;
    double x_dot;
    double y_dot;
    double phi_dot;
    double theta_dot;
    double psi_dot;
    double phiI;
    double thetaI;
    double psiI;
    double x;
    double y;

    double x_vec[8];
    double x_next[8];
    double y_in[5];

    double K[5][13];

    double U[5];

    double ref_phi;
    double ref_theta;
    double ref_psi;
    double ref_x;
    double ref_y;

    double filtGain_pitch;
    double errorGain_pitch;
    double filtError_pitch;
    double filtErrorPre_pitch;

    double filtGain_roll;
    double errorGain_roll;
    double filtError_roll;
    double filtErrorPre_roll;

    double filtGain_yaw;
    double errorGain_yaw;
    double filtError_yaw;
    double filtErrorPre_yaw;

}RADALQI;

typedef struct{
    //states of full system LQI
    double x[13];
    double x_next[13];
    double ry[10];

    double U[5];

}RADALQG;
struct motor_t
{
    int dev_desc;   // KNET device descriptor that represents the motor

    char name[MOTOR_CONFIG_NAME_LEN];   // The name of the motor (Used when using KNET)

    int pid_kk;
    int pid_kff;
    int pid_kp;
    int pid_kd;
    int pid_ka;
    int pid_ki;
    int pid_kih;
    int pid_kil;
    int pid_kf0;
    int pid_kf1;
    int pid_kf2;
    int pid_kf3;
    int pid_kdfil;
    int pid_driveplus;
    int pid_driveminus;
    int pid_accelf;
    int pid_accel;
    int pid_slewlimit;
    int pid_maxpwm;

    /*
    int Kp;
    int Ki;
    int Kd;
    */
    // int sample_time;         // The sample time to use with the motors; Only used in closed-loop mode
    int speed_multiplier;       // how much the desired speed must be multipled by to acheive this speed when commanding the motors
    // int current_limit;       // differential constant used in the internal PID controller of the motors
    int margin;                 // The motor control near target margin
    // int current_limit;       // maximum current that a motor can be given (0 = 0 amps, 512 = 2 amps)
    // int encoder_resolution;  // encoder ticks per centimeter traveled.
    int max_motor_speed;        // The maximum speed (Absolute value) of the motor
    QUADPID openFPGA;
};

struct motortrain_t
{
    struct motor_t left;
    struct motor_t right;
    struct motor_t front;
    struct motor_t rear;
    QUADPID x_vel_ctrl;
    QUADPID y_vel_ctrl;
    QUADPID yaw_ctrl;
};

/************************* Pendulum (propellor system) *************************/


typedef struct
{
    double  vrpnNow; 
    double  vrpnLast;        // units: seconds
    double  vrpnPrev;
    double  vrpnTime0;
    double  dt;
    vrpn_data_t vrpnData;
    vrpn_data_t vrpnData_null;
    double  usec;
    int     motor_1_pwm;
    int     motor_2_pwm;
    int     motor_3_pwm;
    int     motor_4_pwm;
    double  desired_pitch;
    double  desired_yaw;
    double  desired_roll;
    QUADPID pitch;
    QUADPID roll;
    QUADPID yaw;
    QUADSS  theta;
    QUADSSI thetaI;
    RADALQI RADA;
    RADALQG RADAg;
    QUAD_LQG quad_split;
    double controlTime;
    struct filter pitch_ctrl;
    struct filter roll_filt;
    struct filter yaw_filt;
    CTRLSS RADA_pack;
    CTRLSS_loss RADA_pack_loss;
    
    // TODO: move to logger.h
    int     marker;       // to keep track of markers
    int     packetDrop;
} QUAD;

typedef struct
{
    vrpn_data_t vrpnData;
    vrpn_data_t vrpnData_null;
	ERIS_LQG eris_split;
    double vrpnNow;		    // units: seconds
	double vrpnPrev;
    double vrpnTime0;
	double usec;
    double vrpnLast;
    double dt;
    float  x;
    float  x_pre;
    float  y_pre;
    float  x_vel;
    float  y_vel;
    float  xb_vel;
    float  yb_vel;
    float  xb_vel_filt;
    float  yb_vel_filt;
    float  vel_slope;
    float  yaw_vel;
    float  y;
    float  z;
    float  yaw;
    float  pitch;
    float  roll;
    float  x_vel_set;
    float  y_vel_set;
    float  yaw_set;
    short motor_vel_front;
    long motor_pos_front;
    float motor_pwm_front;
    short motor_vel_left;
    long motor_pos_left;
    float motor_pwm_left;
    short motor_vel_right;
    long motor_pos_right;
    float motor_pwm_right;
    short motor_vel_rear;
    long motor_pos_rear;
    float motor_pwm_rear;
    int counter1;
    int packetDrop;

    struct filter xb_vel_filter;
    struct filter yb_vel_filter;
} ERIS;
#endif  // _KOREBOT__MOTOR_CONTROLLER_COMMON_H_
