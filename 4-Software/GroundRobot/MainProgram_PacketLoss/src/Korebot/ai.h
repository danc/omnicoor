#ifndef _KOREBOT__AI_H_
#define _KOREBOT__AI_H_

#include <stddef.h>

#include <sys/queue.h>

class BasePacket;
class KorebotAI;
typedef KorebotAI *create_ai_t();
typedef void destroy_ai_t(KorebotAI *);

#define MAX_AI_PATH_LEN 128

class AI {
	private:
		// This is a singleton object, so here's the instance to the object
		static AI *s_instance;

		//typedef
		struct ai_entry_t {
			SLIST_ENTRY(ai_entry_t) next;

			void *lib_handle;

			// The methods to create & destroy the AI object
			create_ai_t *create_ai;
			destroy_ai_t *destroy_ai;

			KorebotAI *korebot_ai;
		};

		SLIST_HEAD(listhead, ai_entry_t) known_ai;

		unsigned char num_ai;

		KorebotAI *active_ai;

	public:
		/*
		 * Returns a single instance of the AI.
		 *
		 * @returns A single instance of the AI
		 */
		static AI *instance() {
			if (s_instance == NULL) {
				s_instance = new AI();
			}
			return s_instance;
		}


		/*
		 * Frees any resources used by the localization system.
		 */
		~AI();

		/*
		 * Initializes the AI. 
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int init();

		/*
		 * Shuts the localization system down, destroying any resources & closing
		 * the network connection down in the process
		 */
		void shutdown();

		/*
		 * Draws the menu to select the active AI
		 */
		void drawMenu();

		/*
		 * Starts the AI with the given ID
		 *
		 * @param ID The ID of the AI to start
		 *
		 * @returns 1, if the AI with the given ID could be found & is started, 0 otherwise
		 */
		int start(int ID);

		/*
		 * Stops the AI
		 */
		void stop();

		/*
		 * Runs one iteration of the localization system; Checks if
		 * there are any localization packets from the network.
		 * 
		 * @param dt The time difference since the last call to this method
		 */
		void run(float dt);

		/*
		 * Processes a packet received over the network
		 *
		 * @param packet The packet received from the network
		 */
		void processPacket(BasePacket *packet);

		/*
		 * Sends status information about the sensor manager to the control server.
		 *
		 * This includes the currently active AI
		 */
		void sendConfig();

		/*
		 * Prints the status of various modules on the Korebot
		 */
		void printStatus();

		/********************************************************
		 * 		GETTERS / SETTERS		                      	*
		 *******************************************************/

		/*
		 * Returns if the AI is currently running an AI
		 *
		 * @returns 1, if the AI is running, 0 otherwise
		 */
		int isRunning();

	private:
		/*
		 * Creates the AI object.
		 * No parsing of network packets happens yet, and the network
		 * will not be configured yet either.
		 */
		AI();
};

#endif	// _KOREBOT__AI_H_
