#include "motor_controller.h"

#include <iostream>
#include <cstring>

#include "ai.h"
#include "configuration.h"
#include "korebot.h"
#include "localization_system.h"
#include "Network/tcp_connection.h"
#include "Network/network_bitstream.h"
#include "Network/packets/motor_control_speed.h"
#include "Network/packets/motor_control_manual.h"
#include "Network/packets/configuration_packet.h"

#include "DMCCom.h"
#include "INCLUDE5.H"
#include "IO5.H"

// Initialize the instance
MotorController *MotorController::s_instance = NULL;
#ifndef ssDrop
#define ssDrop
#endif
/*
#ifndef posRamp
#define posRamp
#endif
//*/
/**
 * Creates the MotorController object.
 * Nothing is requested of the motor controller board at this ponint.
 */
MotorController::MotorController()
{
    /********** Pendulum motor code **********/
    quad = (QUAD *)calloc(1, sizeof(QUAD));
	eris = (ERIS *)calloc(1, sizeof(ERIS));
    /********** Ground robot code **********/
    memset(&motors, 0, sizeof(motortrain_t));
    memset(&motor_speeds, 0, sizeof(motor_control_manual_t));
    memset(&current_motor_speeds, 0, sizeof(motor_control_manual_t));
    memset(&current_speed_control_settings, 0, sizeof(motor_control_speed_t));
    memset(&current_manual_control_settings, 0, sizeof(motor_control_manual_t));
	
    current_mode = MOTOR_CONTROL_MODE_RELEASE;

    use_bounds_limits = 0;
    use_status_logging = 0;
    e_stop = 0;
}

/**
 * Frees any resources used by the motor controller.
 */
MotorController::~MotorController()
{
    stop();
}

int MotorController::loadConfigurationFile()
{
    /********** Load pendulum PID values **********/
    quad->pitch.desired = Configuration::instance()->getValue("pendulum.pitch.setpoint", (float)0);
    quad->pitch.p       = /*39450/30;*/(Configuration::instance()->getValue("pendulum.pitch.pid_p",    (int)0))*30;
    quad->pitch.i       = /*0;*/(Configuration::instance()->getValue("pendulum.pitch.pid_i",    (int)0))*30;
    quad->pitch.d       = /*14551/30;*/(Configuration::instance()->getValue("pendulum.pitch.pid_d",    (int)0))*30;
    quad->pitch.Taw     = Configuration::instance()->getValue("pendulum.pitch.pid_taw",  (float)0);
    quad->pitch.filtErrorPre=0;
    quad->pitch.filtGain= 0.84189;
    quad->pitch.errorGain=0.1583; 
    quad->roll.desired  = Configuration::instance()->getValue("pendulum.roll.setpoint",  (float)0);
    quad->roll.p        = (Configuration::instance()->getValue("pendulum.roll.pid_p",     (int)0))*30;
    quad->roll.i        = (Configuration::instance()->getValue("pendulum.roll.pid_i",     (int)0))*30;
    quad->roll.d        = (Configuration::instance()->getValue("pendulum.roll.pid_d",     (int)0))*30;
    quad->roll.Taw      = Configuration::instance()->getValue("pendulum.roll.pid_taw",   (float)0);
    quad->roll.filtErrorPre=0;
    quad->roll.filtGain=0.84189;
    quad->roll.errorGain=0.1583;   
    quad->controlTime=0; 
    quad->roll.desiredSet=0.0;
    quad->pitch.desiredSet=0.0;
    quad->roll.timeSum=0.0;

    quad->pitch_ctrl.filterPre=0;
    quad->pitch_ctrl.inputPre=0;
    quad->pitch_ctrl.inputGain=0.16;
    quad->pitch_ctrl.outputGain=0.84;
    quad->pitch_ctrl.inputPreGain=0;

    quad->roll_filt.filterPre=0;
    quad->roll_filt.inputPre=0;
    quad->roll_filt.inputGain=0.16;
    quad->roll_filt.outputGain=0.84;
    quad->roll_filt.inputPreGain=0;

    quad->yaw_filt.filterPre=0;
    quad->yaw_filt.inputPre=0;
    quad->yaw_filt.inputGain=0.16;
    quad->yaw_filt.outputGain=0.84;
    quad->yaw_filt.inputPreGain=0;


    quad->theta.positionGain=72884;//39450.0;
    quad->theta.velGain=26929;//14551.0;
    quad->theta.filtGain=0.84189;
    quad->theta.errorGain=0.1583;
    quad->theta.filtErrorPre=0;

    quad->thetaI.positionGain=83898;//39450.0;
    quad->thetaI.velGain=27964;//14551.0;
    quad->thetaI.Igain=-23061;
    quad->thetaI.filtGain=0.7;
    quad->thetaI.errorGain=0.3;
    quad->thetaI.filtErrorPre=0;
    quad->thetaI.preI=0;
    quad->thetaI.reference=0;

    /********** Load ground robot PID values **********/
    // Left Motor
    Configuration::instance()->getValue("motortrain.left.knet_dev_name", motors.left.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.left.sample_time         = Configuration::instance()->getValue("motortrain.left.sample_time",        (int)0);
    motors.left.speed_multiplier    = Configuration::instance()->getValue("motortrain.left.speed_multiplier",   (int)0);
    motors.left.pid_kk              = Configuration::instance()->getValue("motortrain.left.pid_kk",             (int)0);
    motors.left.pid_kff             = Configuration::instance()->getValue("motortrain.left.pid_kff",            (int)0);
    motors.left.pid_kp              = Configuration::instance()->getValue("motortrain.left.pid_kp",             (int)0);
    motors.left.pid_kd              = Configuration::instance()->getValue("motortrain.left.pid_kd",             (int)0);
    motors.left.pid_ka              = Configuration::instance()->getValue("motortrain.left.pid_ka",             (int)0);
    motors.left.pid_ki              = Configuration::instance()->getValue("motortrain.left.pid_ki",             (int)0);
    motors.left.pid_kih             = Configuration::instance()->getValue("motortrain.left.pid_kih",            (int)0);
    motors.left.pid_kil             = Configuration::instance()->getValue("motortrain.left.pid_kil",            (int)0);
    motors.left.pid_kf0             = Configuration::instance()->getValue("motortrain.left.pid_kf0",            (int)0);
    motors.left.pid_kf1             = Configuration::instance()->getValue("motortrain.left.pid_kf1",            (int)0);
    motors.left.pid_kf2             = Configuration::instance()->getValue("motortrain.left.pid_kf2",            (int)0);
    motors.left.pid_kf3             = Configuration::instance()->getValue("motortrain.left.pid_kf3",            (int)0);
    motors.left.pid_kdfil           = Configuration::instance()->getValue("motortrain.left.pid_kdfil",          (int)0);
    motors.left.pid_driveplus       = Configuration::instance()->getValue("motortrain.left.pid_driveplus",      (int)0);
    motors.left.pid_driveminus      = Configuration::instance()->getValue("motortrain.left.pid_minus",          (int)0);
    motors.left.margin              = Configuration::instance()->getValue("motortrain.left.margin",             (int)0);
    //motors.left.current_limit       = Configuration::instance()->getValue("motortrain.left.current_limit",      (int)0);
    motors.left.pid_accel           = Configuration::instance()->getValue("motortrain.left.pid_accel",          (int)0);
    motors.left.pid_accelf          = Configuration::instance()->getValue("motortrain.left.pid_accelf",         (int)0);
    motors.left.pid_slewlimit       = Configuration::instance()->getValue("motortrain.left.pid_slewlimit",      (int)0);
    motors.left.pid_maxpwm          = Configuration::instance()->getValue("motortrain.left.pid_maxpwm",         (int)0);
    //motors.left.encoder_resolution  = Configuration::instance()->getValue("motortrain.left.encoder_resolution", (int)0);
    motors.left.max_motor_speed     = Configuration::instance()->getValue("motortrain.left.max_motor_speed",    (int)0);
   
    motors.left.openFPGA.desired=0;
    motors.left.openFPGA.p = 500;
    motors.left.openFPGA.i = 1625;
    motors.left.openFPGA.d = 25;
    motors.left.openFPGA.preverr=0;
    motors.left.openFPGA.filtErrorPre=0;
    motors.left.openFPGA.filtGain=0.84189;;
    motors.left.openFPGA.errorGain=0.1583;
   
    // Right Motor
    Configuration::instance()->getValue("motortrain.right.knet_dev_name", motors.right.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.right.sample_time        = Configuration::instance()->getValue("motortrain.right.sample_time",       (int)0);
    motors.right.speed_multiplier   = Configuration::instance()->getValue("motortrain.right.speed_multiplier",  (int)0);
    motors.right.pid_kk             = Configuration::instance()->getValue("motortrain.right.pid_kk",            (int)0);
    motors.right.pid_kff            = Configuration::instance()->getValue("motortrain.right.pid_kff",           (int)0);
    motors.right.pid_kp             = Configuration::instance()->getValue("motortrain.right.pid_kp",            (int)0);
    motors.right.pid_kd             = Configuration::instance()->getValue("motortrain.right.pid_kd",            (int)0);
    motors.right.pid_ka             = Configuration::instance()->getValue("motortrain.right.pid_ka",            (int)0);
    motors.right.pid_ki             = Configuration::instance()->getValue("motortrain.right.pid_ki",            (int)0);
    motors.right.pid_kih            = Configuration::instance()->getValue("motortrain.right.pid_kih",           (int)0);
    motors.right.pid_kil            = Configuration::instance()->getValue("motortrain.right.pid_kil",           (int)0);
    motors.right.pid_kf0            = Configuration::instance()->getValue("motortrain.right.pid_kf0",           (int)0);
    motors.right.pid_kf1            = Configuration::instance()->getValue("motortrain.right.pid_kf1",           (int)0);
    motors.right.pid_kf2            = Configuration::instance()->getValue("motortrain.right.pid_kf2",           (int)0);
    motors.right.pid_kf3            = Configuration::instance()->getValue("motortrain.right.pid_kf3",           (int)0);
    motors.right.pid_kdfil          = Configuration::instance()->getValue("motortrain.right.pid_kdfil",         (int)0);
    motors.right.pid_driveplus      = Configuration::instance()->getValue("motortrain.right.pid_driveplus",     (int)0);
    motors.right.pid_driveminus     = Configuration::instance()->getValue("motortrain.right.pid_minus",         (int)0);
    motors.right.margin             = Configuration::instance()->getValue("motortrain.right.margin",            (int)0);
    //motors.right.current_limit      = Configuration::instance()->getValue("motortrain.right.current_limit",     (int)0);
    motors.right.pid_accel          = Configuration::instance()->getValue("motortrain.right.pid_accel",         (int)0);
    motors.right.pid_accelf         = Configuration::instance()->getValue("motortrain.right.accelf",            (int)0);
    motors.right.pid_slewlimit      = Configuration::instance()->getValue("motortrain.right.pid_slewlimit",     (int)0);
    motors.right.pid_maxpwm         = Configuration::instance()->getValue("motortrain.right.pid_maxpwm",        (int)0);
    //motors.right.encoder_resolution = Configuration::instance()->getValue("motortrain.right.encoder_resolution",(int)0);
    motors.right.max_motor_speed    = Configuration::instance()->getValue("motortrain.right.max_motor_speed",   (int)0);

    motors.right.openFPGA.desired=0;
    motors.right.openFPGA.p = 500;
    motors.right.openFPGA.i = 1625;
    motors.right.openFPGA.d = 25;
    motors.right.openFPGA.preverr=0;
    motors.right.openFPGA.filtErrorPre=0;
    motors.right.openFPGA.filtGain=0.84189;;
    motors.right.openFPGA.errorGain=0.1583;

    // Front Motor
    Configuration::instance()->getValue("motortrain.front.knet_dev_name", motors.front.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.front.sample_time        = Configuration::instance()->getValue("motortrain.front.sample_time",       (int)0);
    motors.front.speed_multiplier   = Configuration::instance()->getValue("motortrain.front.speed_multiplier",  (int)0);
    motors.front.pid_kk             = Configuration::instance()->getValue("motortrain.front.pid_kk",            (int)0);
    motors.front.pid_kff            = Configuration::instance()->getValue("motortrain.front.pid_kff",           (int)0);
    motors.front.pid_kp             = Configuration::instance()->getValue("motortrain.front.pid_kp",            (int)0);
    motors.front.pid_kd             = Configuration::instance()->getValue("motortrain.front.pid_kd",            (int)0);
    motors.front.pid_ka             = Configuration::instance()->getValue("motortrain.front.pid_ka",            (int)0);
    motors.front.pid_ki             = Configuration::instance()->getValue("motortrain.front.pid_ki",            (int)0);
    motors.front.pid_kih            = Configuration::instance()->getValue("motortrain.front.pid_kih",           (int)0);
    motors.front.pid_kil            = Configuration::instance()->getValue("motortrain.front.pid_kil",           (int)0);
    motors.front.pid_kf0            = Configuration::instance()->getValue("motortrain.front.pid_kf0",           (int)0);
    motors.front.pid_kf1            = Configuration::instance()->getValue("motortrain.front.pid_kf1",           (int)0);
    motors.front.pid_kf2            = Configuration::instance()->getValue("motortrain.front.pid_kf2",           (int)0);
    motors.front.pid_kf3            = Configuration::instance()->getValue("motortrain.front.pid_kf3",           (int)0);
    motors.front.pid_kdfil          = Configuration::instance()->getValue("motortrain.front.pid_kdfil",         (int)0);
    motors.front.pid_driveplus      = Configuration::instance()->getValue("motortrain.front.pid_driveplus",     (int)0);
    motors.front.pid_driveminus     = Configuration::instance()->getValue("motortrain.front.pid_minus",         (int)0);
    motors.front.margin             = Configuration::instance()->getValue("motortrain.front.margin",            (int)0);
    //motors.front.current_limit      = Configuration::instance()->getValue("motortrain.front.current_limit",     (int)0);
    motors.front.pid_accel          = Configuration::instance()->getValue("motortrain.front.pid_accel",         (int)0);
    motors.front.pid_accelf         = Configuration::instance()->getValue("motortrain.front.pid_accelf",        (int)0);
    motors.front.pid_slewlimit      = Configuration::instance()->getValue("motortrain.front.pid_slewlimit",     (int)0);
    motors.front.pid_maxpwm         = Configuration::instance()->getValue("motortrain.front.pid_maxpwm",        (int)0);
    //motors.front.encoder_resolution = Configuration::instance()->getValue("motortrain.front.encoder_resolution",(int)0);
    motors.front.max_motor_speed    = Configuration::instance()->getValue("motortrain.front.max_motor_speed",   (int)0);

    motors.front.openFPGA.desired=0;
    motors.front.openFPGA.p = 500*0;
    motors.front.openFPGA.i = 1625*0;
    motors.front.openFPGA.d = 25*0;
    motors.front.openFPGA.preverr=0;
    motors.front.openFPGA.filtErrorPre=0;
    motors.front.openFPGA.filtGain=0;
    motors.front.openFPGA.errorGain=0;

    // Rear Motor
    Configuration::instance()->getValue("motortrain.rear.knet_dev_name", motors.rear.name, MOTOR_CONFIG_NAME_LEN, "/dev/null");
    //motors.rear.sample_time         = Configuration::instance()->getValue("motortrain.rear.sample_time",        (int)0);
    motors.rear.speed_multiplier    = Configuration::instance()->getValue("motortrain.rear.speed_multiplier",   (int)0);
    motors.rear.pid_kk              = Configuration::instance()->getValue("motortrain.rear.pid_kk",             (int)0);
    motors.rear.pid_kff             = Configuration::instance()->getValue("motortrain.rear.pid_kff",            (int)0);
    motors.rear.pid_kp              = Configuration::instance()->getValue("motortrain.rear.pid_kp",             (int)0);
    motors.rear.pid_kd              = Configuration::instance()->getValue("motortrain.rear.pid_kd",             (int)0);
    motors.rear.pid_ka              = Configuration::instance()->getValue("motortrain.rear.pid_ka",             (int)0);
    motors.rear.pid_ki              = Configuration::instance()->getValue("motortrain.rear.pid_ki",             (int)0);
    motors.rear.pid_kih             = Configuration::instance()->getValue("motortrain.rear.pid_kih",            (int)0);
    motors.rear.pid_kil             = Configuration::instance()->getValue("motortrain.rear.pid_kil",            (int)0);
    motors.rear.pid_kf0             = Configuration::instance()->getValue("motortrain.rear.pid_kf0",            (int)0);
    motors.rear.pid_kf1             = Configuration::instance()->getValue("motortrain.rear.pid_kf1",            (int)0);
    motors.rear.pid_kf2             = Configuration::instance()->getValue("motortrain.rear.pid_kf2",            (int)0);
    motors.rear.pid_kf3             = Configuration::instance()->getValue("motortrain.rear.pid_kf3",            (int)0);
    motors.rear.pid_kdfil           = Configuration::instance()->getValue("motortrain.rear.pid_kdfil",          (int)0);
    motors.rear.pid_driveplus       = Configuration::instance()->getValue("motortrain.rear.pid_driveplus",      (int)0);
    motors.rear.pid_driveminus      = Configuration::instance()->getValue("motortrain.rear.pid_minus",          (int)0);
    motors.rear.margin              = Configuration::instance()->getValue("motortrain.rear.margin",             (int)0);
    //motors.rear.current_limit       = Configuration::instance()->getValue("motortrain.rear.current_limit",      (int)0);
    motors.rear.pid_accel           = Configuration::instance()->getValue("motortrain.rear.pid_accel",          (int)0);
    motors.rear.pid_accelf          = Configuration::instance()->getValue("motortrain.rear.pid_accelf",         (int)0);
    motors.rear.pid_slewlimit       = Configuration::instance()->getValue("motortrain.rear.pid_slewlimit",      (int)0);
    motors.rear.pid_maxpwm          = Configuration::instance()->getValue("motortrain.rear.pid_maxpwm",         (int)0);
    //motors.rear.encoder_resolution  = Configuration::instance()->getValue("motortrain.rear.encoder_resolution", (int)0);
    motors.rear.max_motor_speed     = Configuration::instance()->getValue("motortrain.rear.max_motor_speed",    (int)0);

    motors.rear.openFPGA.desired=0;
    motors.rear.openFPGA.p = 500*0;
    motors.rear.openFPGA.i = 1625*0;
    motors.rear.openFPGA.d = 25*0;
    motors.rear.openFPGA.preverr=0;
    motors.rear.openFPGA.filtErrorPre=0;
    motors.rear.openFPGA.filtGain=0;
    motors.rear.openFPGA.errorGain=0;
    #ifdef posRamp
    motors.x_vel_ctrl.p = 3800; //1500;//3000;
    motors.x_vel_ctrl.i = 0;//11375; //4875; //6500;
    motors.x_vel_ctrl.d = 1000;//25; //50;//100;
    motors.x_vel_ctrl.errorGain = 1;//.16;
    motors.x_vel_ctrl.filtGain = 0;//.84;
    motors.x_vel_ctrl.preverr=0;
    motors.x_vel_ctrl.filtErrorPre=0;
    motors.x_vel_ctrl.desired=0;
    motors.x_vel_ctrl.correction=0;

    motors.y_vel_ctrl.p = 3800;//2000;
    motors.y_vel_ctrl.i = 0;//11375;
    motors.y_vel_ctrl.d = 1000;//25;
    motors.y_vel_ctrl.errorGain = 1;
    motors.y_vel_ctrl.filtGain = 0;
    motors.y_vel_ctrl.preverr=0;
    motors.y_vel_ctrl.filtErrorPre=0;
    motors.y_vel_ctrl.desired=0;
    motors.y_vel_ctrl.correction=0;
    printf("postion to ctrl velocity\n\r");
    #else
    motors.x_vel_ctrl.p = 1000; //1500;//3000;
    motors.x_vel_ctrl.i = 3800;//11375; //4875; //6500;
    motors.x_vel_ctrl.d = 0;//25; //50;//100;
    motors.x_vel_ctrl.errorGain = 1;//.16;
    motors.x_vel_ctrl.filtGain = 0;//.84;
    motors.x_vel_ctrl.preverr=0;
    motors.x_vel_ctrl.filtErrorPre=0;
    motors.x_vel_ctrl.desired=0;
    motors.x_vel_ctrl.correction=0;

    motors.y_vel_ctrl.p = 1000;//2000;
    motors.y_vel_ctrl.i = 3800;//11375;
    motors.y_vel_ctrl.d = 0;//25;
    motors.y_vel_ctrl.errorGain = .16;
    motors.y_vel_ctrl.filtGain = .84;
    motors.y_vel_ctrl.preverr=0;
    motors.y_vel_ctrl.filtErrorPre=0;
    motors.y_vel_ctrl.desired=0;
    motors.y_vel_ctrl.correction=0;
    #endif
    motors.yaw_ctrl.p = 40000;//14500;
    motors.yaw_ctrl.i = 21800;//2000;
    motors.yaw_ctrl.d = 14500;//550;
    motors.yaw_ctrl.errorGain = .16;
    motors.yaw_ctrl.filtGain = .84;
    motors.yaw_ctrl.preverr=0;
    motors.yaw_ctrl.filtErrorPre=0;
    motors.yaw_ctrl.desired=0*3.14159/180;
    motors.yaw_ctrl.correction=0;

    /********** Load camera system boundaries **********/
    // TODO: these do not reflect the current system, they should be updated
    min_x = Configuration::instance()->getValue("localization.min_x", (int)0);
    min_y = Configuration::instance()->getValue("localization.min_y", (int)0);
    max_x = Configuration::instance()->getValue("localization.max_x", (int)0);
    max_y = Configuration::instance()->getValue("localization.max_y", (int)0);
    eris->x_vel_set=0;
    //dummy initialization to make sure the intial readins are assinged aprroprately
    eris->x_pre=-10000;
    eris->y_pre=-10000;

    //intatiazes paraperts for Eris wheel controller
    eris->vel_slope=17.5;
    eris->x_vel=0;
    eris->y_vel=0;
    eris->xb_vel_filter.filterPre=0;
    eris->xb_vel_filter.inputGain=1;//0.16;
    eris->xb_vel_filter.outputGain=0;//0.84;
    eris->xb_vel_filter.inputPre=0;
    eris->xb_vel_filter.inputPreGain=0;
    eris->yb_vel_filter.filterPre=0;
    eris->yb_vel_filter.inputGain=1;//0.16;
    eris->yb_vel_filter.outputGain=0;//0.84;
    eris->yb_vel_filter.inputPre=0;
    eris->yb_vel_filter.inputPreGain=0;

    eris->yaw_set=0;
    eris->y_vel_set=0;

    quad->RADA.phi=0;
    quad->RADA.theta=0;
    quad->RADA.psi=0;
    quad->RADA.x_dot=0;
    quad->RADA.y_dot=0;
    quad->RADA.phi_dot=0;
    quad->RADA.theta_dot=0;
    quad->RADA.psi_dot=0;
    quad->RADA.phiI=0;
    quad->RADA.thetaI=0;
    quad->RADA.psiI=0;
    quad->RADA.x=0;
    quad->RADA.y=0;

    quad->RADA.K[0][0]=70350;//90608;
    quad->RADA.K[0][1]=-1132;//-1457;
    quad->RADA.K[0][2]=0;
    quad->RADA.K[0][3]=56;//-317;
    quad->RADA.K[0][4]=3498;//-19687;
    quad->RADA.K[0][5]=22305;//24561;
    quad->RADA.K[0][6]=-359;// -395;
    quad->RADA.K[0][7]=0;
    quad->RADA.K[0][8]=-23873;//-74690;
    quad->RADA.K[0][9]=384;//1201;
    quad->RADA.K[0][10]=0;
    quad->RADA.K[0][11]=30;//1203;
    quad->RADA.K[0][12]=1870;//74822;

    quad->RADA.K[1][0]=1132;//1457;
    quad->RADA.K[1][1]=70350;//90608;
    quad->RADA.K[1][2]=0;
    quad->RADA.K[1][3]=-3498;//19687;
    quad->RADA.K[1][4]=56;//-317;
    quad->RADA.K[1][5]=359;//395;
    quad->RADA.K[1][6]=22305;//24561;
    quad->RADA.K[1][7]=0;
    quad->RADA.K[1][8]=-384;//-1201;
    quad->RADA.K[1][9]=-238873;//-74690;
    quad->RADA.K[1][10]=0;
    quad->RADA.K[1][11]=-1870;//-74822;
    quad->RADA.K[1][12]=30;//1203;

    quad->RADA.K[2][0]=0;
    quad->RADA.K[2][1]=0;
    quad->RADA.K[2][2]=45115;
    quad->RADA.K[2][3]=0;
    quad->RADA.K[2][4]=0;
    quad->RADA.K[2][5]=0;
    quad->RADA.K[2][6]=0;
    quad->RADA.K[2][7]=3432;//25042;
    quad->RADA.K[2][8]=0;
    quad->RADA.K[2][9]=0;
    quad->RADA.K[2][10]=-59207;//-812892;
    quad->RADA.K[2][11]=0;
    quad->RADA.K[2][12]=0;

    quad->RADA.K[3][0]=0;
    quad->RADA.K[3][1]=16729;//11413;
    quad->RADA.K[3][2]=0;
    quad->RADA.K[3][3]=21742;//244217;
    quad->RADA.K[3][4]=0;
    quad->RADA.K[3][5]=0;
    quad->RADA.K[3][6]=1875;//-2987;
    quad->RADA.K[3][7]=0;
    quad->RADA.K[3][8]=0;
    quad->RADA.K[3][9]=1067;//9207;
    quad->RADA.K[3][10]=0;
    quad->RADA.K[3][11]=-61517;//-746643;
    quad->RADA.K[3][12]=0;  

    quad->RADA.K[4][0]=-16729;//-11413;
    quad->RADA.K[4][1]=0;
    quad->RADA.K[4][2]=0;
    quad->RADA.K[4][3]=0;
    quad->RADA.K[4][4]=21742;//244271;
    quad->RADA.K[4][5]=-1875;//2987;
    quad->RADA.K[4][6]=0;
    quad->RADA.K[4][7]=0;
    quad->RADA.K[4][8]=-1067;//-9207;
    quad->RADA.K[4][9]=0;
    quad->RADA.K[4][10]=0;
    quad->RADA.K[4][11]=0;
    quad->RADA.K[4][12]=-61517;//-746643;

    for(int i = 0; i<5; i++){
        for(int j = 0; j<13; j++){
          quad->RADA.K[i][j]=K[i][j];  
        }
    }

    for(int i = 0; i<8; i++){
        quad->RADA.x_vec[i]=0;
        quad->RADA.x_next[i]=0;
    }

    for(int i = 0; i<6; i++){
        quad->quad_split.x[i]=0;
        quad->quad_split.x_next[i]=0;
    }

    for(int i = 0; i<7; i++){
        eris->eris_split.x[i]=0;
        eris->eris_split.x_next[i]=0;
    }

    quad->quad_split.ry[0]=0;
    quad->quad_split.ry[1]=0;
    quad->quad_split.ry[2]=0;
    quad->quad_split.ry[3]=0;

    quad->quad_split.U[0]=0;
    quad->quad_split.U[1]=0;

    eris->eris_split.ry[0]=0;
    eris->eris_split.ry[1]=0;
    eris->eris_split.ry[2]=0;
    eris->eris_split.ry[3]=0;
    eris->eris_split.ry[4]=0;
    eris->eris_split.ry[5]=0;

    eris->eris_split.U[0]=0;
    eris->eris_split.U[1]=0;
    eris->eris_split.U[2]=0;

    for(int i = 0; i<5; i++){
        quad->RADA.y_in[i]=0;
    }

    for(int i = 0; i<13; i++){
        quad->RADAg.x[i]=0;
        quad->RADAg.x_next[i]=0;
    }

    for(int i = 0; i<10; i++){
        quad->RADAg.ry[i]=0;
    }
    for(int i = 0; i<5; i++){
        quad-> RADAg.U[i]=0;
    }



    quad->RADA.U[0]=0;
    quad->RADA.U[1]=0;
    quad->RADA.U[2]=0;
    quad->RADA.U[3]=0;
    quad->RADA.U[4]=0;

    quad->RADA.ref_theta=0;
    quad->RADA.ref_phi=0;
    quad->RADA.ref_psi=0;
    quad->RADA.ref_x=0;
    quad->RADA.ref_y=0;

    #ifdef LQI_strong
    quad->RADA.filtGain_pitch=.8;
    quad->RADA.errorGain_pitch=.2;
    quad->RADA.filtError_pitch=0;
    quad->RADA.filtErrorPre_pitch=0;

    quad->RADA.filtGain_roll=.8;
    quad->RADA.errorGain_roll=.2;
    quad->RADA.filtError_roll=0;
    quad->RADA.filtErrorPre_roll=0;
    #else
    quad->RADA.filtGain_pitch=.8;//.84;
    quad->RADA.errorGain_pitch=.2;//.16;
    quad->RADA.filtError_pitch=0;
    quad->RADA.filtErrorPre_pitch=0;

    quad->RADA.filtGain_roll=.8;//.84;
    quad->RADA.errorGain_roll=.2;//.16;
    quad->RADA.filtError_roll=0;
    quad->RADA.filtErrorPre_roll=0;
    #endif

    quad->RADA.filtGain_yaw=.84;
    quad->RADA.errorGain_yaw=.16;
    quad->RADA.filtError_yaw=0;
    quad->RADA.filtErrorPre_yaw=0;

    SSctrl_config();

    #ifdef ssDrop
    stateMod_initialize();
    SSctrl_config_loss();
    #endif
}

/**
 * Initializes the MotorController.  If the mode is anything other
 * than free, then the controller will attempt to acquire the motor
 * device descriptors as defined in the Configuration object.
 *
 * @returns 1 on success, 0 otherwise
 */
int MotorController::init()
{
    // fpga initialization (see DMCCOM.cpp)
    init_mcont();

    /********** Load configuration values from the config file **********/
    loadConfigurationFile();

    // Sets the motors to 0
    setMotors();
    return 1;
}

/****************************************** New Code ******************************************/

// TODO: Half of this is completely meaningless for what we want to do
//       it is initializing things for the ground robot motors
void MotorController::initQuadMotor(int motorNum)
{
    write_word(motorNum, openloop, 0xFFFF);   // in open loop mode
    write_word(motorNum, profile, 1);         // Set to Trapezoidal profile
    //write_word(0, prescale, 100);      // PWM Rate
    //write_word(0, postscale, 200);     // Sample Rate
}

void MotorController::armQuadMotors()
{
    int pwm_duty_cycle = 20000;
    int increment_amount = 1000;

    while (pwm_duty_cycle < 40000)
    {
        pwm_duty_cycle += increment_amount;
        setDutyCycle(pwm_duty_cycle, 1);
        setDutyCycle(pwm_duty_cycle, 2);
        setDutyCycle(pwm_duty_cycle, 3);
        setDutyCycle(pwm_duty_cycle, 4);
    }
    sleep(1);
    setDutyCycle(28000, 1);//actually 28000
    setDutyCycle(28000, 2);
    setDutyCycle(22800, 3);
    setDutyCycle(28000, 4);
}

/******************** pid controller methods ********************/
void MotorController::setDutyCycle(int pwmgen_value, int motor_num)
{ 
    if (motor_num == 1)
    {
        write_word(0, pwmgenc, pwmgen_value); // Duty cycle
    }
    if (motor_num == 2)
    {
        write_word(1, pwmgenc, pwmgen_value); // Duty cycle
    }
    if (motor_num == 3)
    {
        write_word(3, pwmgenc, pwmgen_value); // Duty cycle
    }
    if (motor_num == 4)
    {
        write_word(2, pwmgenc, pwmgen_value); // Duty cycle
    }
}

double MotorController::stateFeedback(QUADSS *ssptr){

   ssptr->filtError = ssptr->filtErrorPre*ssptr->filtGain+ssptr->position*ssptr->errorGain;
   ssptr->correction=-ssptr->positionGain*ssptr->position-ssptr->velGain*(ssptr->filtError - ssptr->filtErrorPre) / SAMPLE_PERIOD;
   ssptr->filtErrorPre=ssptr->filtError;
   return ssptr->correction;
}

double MotorController::stateFeedbackI(QUADSSI *ssptr){
   ssptr->preI+=(ssptr->reference-ssptr->position)*SAMPLE_PERIOD;
   ssptr->filtError = ssptr->filtErrorPre*ssptr->filtGain+ssptr->position*ssptr->errorGain;
   ssptr->correction=-ssptr->positionGain*ssptr->position-ssptr->velGain*(ssptr->filtError - ssptr->filtErrorPre) / SAMPLE_PERIOD-ssptr->Igain*ssptr->preI;
   ssptr->filtErrorPre=ssptr->filtError;
   return ssptr->correction;
}

int MotorController::RADA_LQI(RADALQI *lqiptr){
    lqiptr->filtError_roll = lqiptr->filtErrorPre_roll*lqiptr->filtGain_roll+lqiptr->phi*lqiptr->errorGain_roll;
    lqiptr->filtError_pitch = lqiptr->filtErrorPre_pitch*lqiptr->filtGain_pitch+lqiptr->theta*lqiptr->errorGain_pitch;
    lqiptr->filtError_yaw = lqiptr->filtErrorPre_yaw*lqiptr->filtGain_yaw+lqiptr->psi*lqiptr->errorGain_yaw;

    lqiptr->phi_dot=(lqiptr->filtError_roll - lqiptr->filtErrorPre_roll) / SAMPLE_PERIOD;
    lqiptr->theta_dot=(lqiptr->filtError_pitch - lqiptr->filtErrorPre_pitch) / SAMPLE_PERIOD;
    lqiptr->psi_dot=(lqiptr->filtError_yaw - lqiptr->filtErrorPre_yaw) / SAMPLE_PERIOD;

    lqiptr->filtErrorPre_roll=lqiptr->filtError_roll;
    lqiptr->filtErrorPre_pitch=lqiptr->filtError_pitch;
    lqiptr->filtErrorPre_yaw=lqiptr->filtError_yaw;

    lqiptr->U[0]=-lqiptr->K[0][0]*lqiptr->phi-lqiptr->K[0][1]*lqiptr->theta-lqiptr->K[0][2]*lqiptr->psi-lqiptr->K[0][3]*lqiptr->x_dot-lqiptr->K[0][4]*lqiptr->y_dot-lqiptr->K[0][5]*lqiptr->phi_dot-lqiptr->K[0][6]*lqiptr->theta_dot-lqiptr->K[0][7]*lqiptr->psi_dot-lqiptr->K[0][8]*lqiptr->phiI-lqiptr->K[0][9]*lqiptr->thetaI-lqiptr->K[0][10]*lqiptr->psiI-lqiptr->K[0][11]*lqiptr->x-lqiptr->K[0][12]*lqiptr->y;
    lqiptr->U[1]=-lqiptr->K[1][0]*lqiptr->phi-lqiptr->K[1][1]*lqiptr->theta-lqiptr->K[1][2]*lqiptr->psi-lqiptr->K[1][3]*lqiptr->x_dot-lqiptr->K[1][4]*lqiptr->y_dot-lqiptr->K[1][5]*lqiptr->phi_dot-lqiptr->K[1][6]*lqiptr->theta_dot-lqiptr->K[1][7]*lqiptr->psi_dot-lqiptr->K[1][8]*lqiptr->phiI-lqiptr->K[1][9]*lqiptr->thetaI-lqiptr->K[1][10]*lqiptr->psiI-lqiptr->K[1][11]*lqiptr->x-lqiptr->K[1][12]*lqiptr->y;
    lqiptr->U[2]=-lqiptr->K[2][0]*lqiptr->phi-lqiptr->K[2][1]*lqiptr->theta-lqiptr->K[2][2]*lqiptr->psi-lqiptr->K[2][3]*lqiptr->x_dot-lqiptr->K[2][4]*lqiptr->y_dot-lqiptr->K[2][5]*lqiptr->phi_dot-lqiptr->K[2][6]*lqiptr->theta_dot-lqiptr->K[2][7]*lqiptr->psi_dot-lqiptr->K[2][8]*lqiptr->phiI-lqiptr->K[2][9]*lqiptr->thetaI-lqiptr->K[2][10]*lqiptr->psiI-lqiptr->K[2][11]*lqiptr->x-lqiptr->K[2][12]*lqiptr->y;
    lqiptr->U[3]=-lqiptr->K[3][0]*lqiptr->phi-lqiptr->K[3][1]*lqiptr->theta-lqiptr->K[3][2]*lqiptr->psi-lqiptr->K[3][3]*lqiptr->x_dot-lqiptr->K[3][4]*lqiptr->y_dot-lqiptr->K[3][5]*lqiptr->phi_dot-lqiptr->K[3][6]*lqiptr->theta_dot-lqiptr->K[3][7]*lqiptr->psi_dot-lqiptr->K[3][8]*lqiptr->phiI-lqiptr->K[3][9]*lqiptr->thetaI-lqiptr->K[3][10]*lqiptr->psiI-lqiptr->K[3][11]*lqiptr->x-lqiptr->K[3][12]*lqiptr->y;
    lqiptr->U[4]=-lqiptr->K[4][0]*lqiptr->phi-lqiptr->K[4][1]*lqiptr->theta-lqiptr->K[4][2]*lqiptr->psi-lqiptr->K[4][3]*lqiptr->x_dot-lqiptr->K[4][4]*lqiptr->y_dot-lqiptr->K[4][5]*lqiptr->phi_dot-lqiptr->K[4][6]*lqiptr->theta_dot-lqiptr->K[4][7]*lqiptr->psi_dot-lqiptr->K[4][8]*lqiptr->phiI-lqiptr->K[4][9]*lqiptr->thetaI-lqiptr->K[4][10]*lqiptr->psiI-lqiptr->K[4][11]*lqiptr->x-lqiptr->K[4][12]*lqiptr->y;

}

int MotorController::RADA_EST_LQI(RADALQI *lqiptr){

    lqiptr->U[0]=-lqiptr->K[0][0]*lqiptr->x_vec[0]-lqiptr->K[0][1]*lqiptr->x_vec[1]-lqiptr->K[0][2]*lqiptr->x_vec[2]-lqiptr->K[0][3]*lqiptr->x_vec[3]-lqiptr->K[0][4]*lqiptr->x_vec[4]-lqiptr->K[0][5]*lqiptr->x_vec[5]-lqiptr->K[0][6]*lqiptr->x_vec[6]-lqiptr->K[0][7]*lqiptr->x_vec[7]-lqiptr->K[0][8]*lqiptr->phiI-lqiptr->K[0][9]*lqiptr->thetaI-lqiptr->K[0][10]*lqiptr->psiI-lqiptr->K[0][11]*lqiptr->x-lqiptr->K[0][12]*lqiptr->y;
    lqiptr->U[1]=-lqiptr->K[1][0]*lqiptr->x_vec[0]-lqiptr->K[1][1]*lqiptr->x_vec[1]-lqiptr->K[1][2]*lqiptr->x_vec[2]-lqiptr->K[1][3]*lqiptr->x_vec[3]-lqiptr->K[1][4]*lqiptr->x_vec[4]-lqiptr->K[1][5]*lqiptr->x_vec[5]-lqiptr->K[1][6]*lqiptr->x_vec[6]-lqiptr->K[1][7]*lqiptr->x_vec[7]-lqiptr->K[1][8]*lqiptr->phiI-lqiptr->K[1][9]*lqiptr->thetaI-lqiptr->K[1][10]*lqiptr->psiI-lqiptr->K[1][11]*lqiptr->x-lqiptr->K[1][12]*lqiptr->y;
    lqiptr->U[2]=-lqiptr->K[2][0]*lqiptr->x_vec[0]-lqiptr->K[2][1]*lqiptr->x_vec[1]-lqiptr->K[2][2]*lqiptr->x_vec[2]-lqiptr->K[2][3]*lqiptr->x_vec[3]-lqiptr->K[2][4]*lqiptr->x_vec[4]-lqiptr->K[2][5]*lqiptr->x_vec[5]-lqiptr->K[2][6]*lqiptr->x_vec[6]-lqiptr->K[2][7]*lqiptr->x_vec[7]-lqiptr->K[2][8]*lqiptr->phiI-lqiptr->K[2][9]*lqiptr->thetaI-lqiptr->K[2][10]*lqiptr->psiI-lqiptr->K[2][11]*lqiptr->x-lqiptr->K[2][12]*lqiptr->y;
    lqiptr->U[3]=-lqiptr->K[3][0]*lqiptr->x_vec[0]-lqiptr->K[3][1]*lqiptr->x_vec[1]-lqiptr->K[3][2]*lqiptr->x_vec[2]-lqiptr->K[3][3]*lqiptr->x_vec[3]-lqiptr->K[3][4]*lqiptr->x_vec[4]-lqiptr->K[3][5]*lqiptr->x_vec[5]-lqiptr->K[3][6]*lqiptr->x_vec[6]-lqiptr->K[3][7]*lqiptr->x_vec[7]-lqiptr->K[3][8]*lqiptr->phiI-lqiptr->K[3][9]*lqiptr->thetaI-lqiptr->K[3][10]*lqiptr->psiI-lqiptr->K[3][11]*lqiptr->x-lqiptr->K[3][12]*lqiptr->y;
    lqiptr->U[4]=-lqiptr->K[4][0]*lqiptr->x_vec[0]-lqiptr->K[4][1]*lqiptr->x_vec[1]-lqiptr->K[4][2]*lqiptr->x_vec[2]-lqiptr->K[4][3]*lqiptr->x_vec[3]-lqiptr->K[4][4]*lqiptr->x_vec[4]-lqiptr->K[4][5]*lqiptr->x_vec[5]-lqiptr->K[4][6]*lqiptr->x_vec[6]-lqiptr->K[4][7]*lqiptr->x_vec[7]-lqiptr->K[4][8]*lqiptr->phiI-lqiptr->K[4][9]*lqiptr->thetaI-lqiptr->K[4][10]*lqiptr->psiI-lqiptr->K[4][11]*lqiptr->x-lqiptr->K[4][12]*lqiptr->y;

    for(int i=0; i<8; i++){
        lqiptr->x_next[i]=0;
        for(int j=0; j<8; j++){
            lqiptr->x_next[i]+=Ad[i][j]*lqiptr->x_vec[j];
        }
        for(int k=0; k<5; k++){
            lqiptr->x_next[i]+=Bd[i][k]*lqiptr->U[k]+L[i][k]*(lqiptr->y_in[k]-lqiptr->x_vec[k]);
            //lqiptr->x_next[i]+=L[i][k]*lqiptr->y_in[k];
        }
    }

    for(int i=0; i<8; i++){
        lqiptr->x_vec[i]=lqiptr->x_next[i];
    }

}

int MotorController::RADA_LQI_dt(RADALQI *lqiptr, double dt){
    lqiptr->filtError_roll = lqiptr->filtErrorPre_roll*lqiptr->filtGain_roll+lqiptr->phi*lqiptr->errorGain_roll;
    lqiptr->filtError_pitch = lqiptr->filtErrorPre_pitch*lqiptr->filtGain_pitch+lqiptr->theta*lqiptr->errorGain_pitch;
    lqiptr->filtError_yaw = lqiptr->filtErrorPre_yaw*lqiptr->filtGain_yaw+lqiptr->psi*lqiptr->errorGain_yaw;

    lqiptr->phi_dot=(lqiptr->filtError_roll - lqiptr->filtErrorPre_roll) / dt;
    lqiptr->theta_dot=(lqiptr->filtError_pitch - lqiptr->filtErrorPre_pitch) / dt;
    lqiptr->psi_dot=(lqiptr->filtError_yaw - lqiptr->filtErrorPre_yaw) / dt;

    lqiptr->filtErrorPre_roll=lqiptr->filtError_roll;
    lqiptr->filtErrorPre_pitch=lqiptr->filtError_pitch;
    lqiptr->filtErrorPre_yaw=lqiptr->filtError_yaw;

    lqiptr->U[0]=-lqiptr->K[0][0]*lqiptr->phi-lqiptr->K[0][1]*lqiptr->theta-lqiptr->K[0][2]*lqiptr->psi-lqiptr->K[0][3]*lqiptr->x_dot-lqiptr->K[0][4]*lqiptr->y_dot-lqiptr->K[0][5]*lqiptr->phi_dot-lqiptr->K[0][6]*lqiptr->theta_dot-lqiptr->K[0][7]*lqiptr->psi_dot-lqiptr->K[0][8]*lqiptr->phiI-lqiptr->K[0][9]*lqiptr->thetaI-lqiptr->K[0][10]*lqiptr->psiI-lqiptr->K[0][11]*lqiptr->x-lqiptr->K[0][12]*lqiptr->y;
    lqiptr->U[1]=-lqiptr->K[1][0]*lqiptr->phi-lqiptr->K[1][1]*lqiptr->theta-lqiptr->K[1][2]*lqiptr->psi-lqiptr->K[1][3]*lqiptr->x_dot-lqiptr->K[1][4]*lqiptr->y_dot-lqiptr->K[1][5]*lqiptr->phi_dot-lqiptr->K[1][6]*lqiptr->theta_dot-lqiptr->K[1][7]*lqiptr->psi_dot-lqiptr->K[1][8]*lqiptr->phiI-lqiptr->K[1][9]*lqiptr->thetaI-lqiptr->K[1][10]*lqiptr->psiI-lqiptr->K[1][11]*lqiptr->x-lqiptr->K[1][12]*lqiptr->y;
    lqiptr->U[2]=-lqiptr->K[2][0]*lqiptr->phi-lqiptr->K[2][1]*lqiptr->theta-lqiptr->K[2][2]*lqiptr->psi-lqiptr->K[2][3]*lqiptr->x_dot-lqiptr->K[2][4]*lqiptr->y_dot-lqiptr->K[2][5]*lqiptr->phi_dot-lqiptr->K[2][6]*lqiptr->theta_dot-lqiptr->K[2][7]*lqiptr->psi_dot-lqiptr->K[2][8]*lqiptr->phiI-lqiptr->K[2][9]*lqiptr->thetaI-lqiptr->K[2][10]*lqiptr->psiI-lqiptr->K[2][11]*lqiptr->x-lqiptr->K[2][12]*lqiptr->y;
    lqiptr->U[3]=-lqiptr->K[3][0]*lqiptr->phi-lqiptr->K[3][1]*lqiptr->theta-lqiptr->K[3][2]*lqiptr->psi-lqiptr->K[3][3]*lqiptr->x_dot-lqiptr->K[3][4]*lqiptr->y_dot-lqiptr->K[3][5]*lqiptr->phi_dot-lqiptr->K[3][6]*lqiptr->theta_dot-lqiptr->K[3][7]*lqiptr->psi_dot-lqiptr->K[3][8]*lqiptr->phiI-lqiptr->K[3][9]*lqiptr->thetaI-lqiptr->K[3][10]*lqiptr->psiI-lqiptr->K[3][11]*lqiptr->x-lqiptr->K[3][12]*lqiptr->y;
    lqiptr->U[4]=-lqiptr->K[4][0]*lqiptr->phi-lqiptr->K[4][1]*lqiptr->theta-lqiptr->K[4][2]*lqiptr->psi-lqiptr->K[4][3]*lqiptr->x_dot-lqiptr->K[4][4]*lqiptr->y_dot-lqiptr->K[4][5]*lqiptr->phi_dot-lqiptr->K[4][6]*lqiptr->theta_dot-lqiptr->K[4][7]*lqiptr->psi_dot-lqiptr->K[4][8]*lqiptr->phiI-lqiptr->K[4][9]*lqiptr->thetaI-lqiptr->K[4][10]*lqiptr->psiI-lqiptr->K[4][11]*lqiptr->x-lqiptr->K[4][12]*lqiptr->y;

}
/*
int MotorController::RADA_LQI(RADALQI *lqiptr){
    lqiptr->filtError_roll = lqiptr->filtErrorPre_roll*lqiptr->filtGain_roll+lqiptr->phi*lqiptr->errorGain_roll;
    lqiptr->filtError_pitch = lqiptr->filtErrorPre_pitch*lqiptr->filtGain_pitch+lqiptr->theta*lqiptr->errorGain_pitch;
    lqiptr->filtError_yaw = lqiptr->filtErrorPre_yaw*lqiptr->filtGain_yaw+lqiptr->psi*lqiptr->errorGain_yaw;

    lqiptr->phi_dot=(lqiptr->filtError_roll - lqiptr->filtErrorPre_roll) / SAMPLE_PERIOD;
    lqiptr->theta_dot=(lqiptr->filtError_pitch - lqiptr->filtErrorPre_pitch) / SAMPLE_PERIOD;
    lqiptr->psi_dot=(lqiptr->filtError_yaw - lqiptr->filtErrorPre_yaw) / SAMPLE_PERIOD;

    lqiptr->filtErrorPre_roll=lqiptr->filtError_roll;
    lqiptr->filtErrorPre_pitch=lqiptr->filtError_pitch;
    lqiptr->filtErrorPre_yaw=lqiptr->filtError_yaw;

    lqiptr->U[0]=-K[0][0]*lqiptr->phi-K[0][1]*lqiptr->theta-K[0][2]*lqiptr->psi-K[0][3]*lqiptr->x_dot-K[0][4]*lqiptr->y_dot-K[0][5]*lqiptr->phi_dot-K[0][6]*lqiptr->theta_dot-K[0][7]*lqiptr->psi_dot-lqiptr->K[0][8]*lqiptr->phiI-K[0][9]*lqiptr->thetaI-K[0][10]*lqiptr->psiI-K[0][11]*lqiptr->x-K[0][12]*lqiptr->y;
    lqiptr->U[1]=-K[1][0]*lqiptr->phi-K[1][1]*lqiptr->theta-K[1][2]*lqiptr->psi-K[1][3]*lqiptr->x_dot-K[1][4]*lqiptr->y_dot-K[1][5]*lqiptr->phi_dot-K[1][6]*lqiptr->theta_dot-K[1][7]*lqiptr->psi_dot-K[1][8]*lqiptr->phiI-K[1][9]*lqiptr->thetaI-K[1][10]*lqiptr->psiI-K[1][11]*lqiptr->x-K[1][12]*lqiptr->y;
    lqiptr->U[2]=-K[2][0]*lqiptr->phi-K[2][1]*lqiptr->theta-K[2][2]*lqiptr->psi-K[2][3]*lqiptr->x_dot-K[2][4]*lqiptr->y_dot-K[2][5]*lqiptr->phi_dot-K[2][6]*lqiptr->theta_dot-K[2][7]*lqiptr->psi_dot-K[2][8]*lqiptr->phiI-K[2][9]*lqiptr->thetaI-K[2][10]*lqiptr->psiI-K[2][11]*lqiptr->x-K[2][12]*lqiptr->y;
    lqiptr->U[3]=-K[3][0]*lqiptr->phi-K[3][1]*lqiptr->theta-K[3][2]*lqiptr->psi-K[3][3]*lqiptr->x_dot-K[3][4]*lqiptr->y_dot-K[3][5]*lqiptr->phi_dot-K[3][6]*lqiptr->theta_dot-K[3][7]*lqiptr->psi_dot-K[3][8]*lqiptr->phiI-K[3][9]*lqiptr->thetaI-K[3][10]*lqiptr->psiI-K[3][11]*lqiptr->x-K[3][12]*lqiptr->y;
    lqiptr->U[4]=-K[4][0]*lqiptr->phi-K[4][1]*lqiptr->theta-K[4][2]*lqiptr->psi-K[4][3]*lqiptr->x_dot-K[4][4]*lqiptr->y_dot-K[4][5]*lqiptr->phi_dot-K[4][6]*lqiptr->theta_dot-K[4][7]*lqiptr->psi_dot-K[4][8]*lqiptr->phiI-K[4][9]*lqiptr->thetaI-K[4][10]*lqiptr->psiI-K[4][11]*lqiptr->x-K[4][12]*lqiptr->y;

}//*/
int MotorController::SS_CTRL_CALC(CTRLSS *ctrlss){
    
    for(int i=0; i<OUTPUTS_C; i++){
        ctrlss->Y[i]=0;
        for(int j=0; j<STATES_C; j++){
            ctrlss->Y[i]+=ctrlss->C[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<INPUTS_C; k++){
            ctrlss->Y[i]+=ctrlss->D[i][k]*ctrlss->u[k];
        }
    }
    for(int i=0; i<STATES_C; i++){
        ctrlss->x_next[i]=0;
        for(int j=0; j<STATES_C; j++){
            ctrlss->x_next[i]+=ctrlss->A[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<INPUTS_C; k++){
            ctrlss->x_next[i]+=ctrlss->B[i][k]*ctrlss->u[k];
        }
    }
    for(int i=0; i<STATES_C; i++){
        ctrlss->x[i]=ctrlss->x_next[i];
    }
    return 1;
}

int MotorController::LQG_CALC(RADALQG *ctrlss){
    
    for(int i=0; i<5; i++){
        ctrlss->U[i]=0;
        for(int j=0; j<13; j++){
            ctrlss->U[i]+=C[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<10; k++){
            ctrlss->U[i]+=D[i][k]*ctrlss->ry[k];
        }
    }
    for(int i=0; i<13; i++){
        ctrlss->x_next[i]=0;
        for(int j=0; j<13; j++){
            ctrlss->x_next[i]+=A[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<10; k++){
            ctrlss->x_next[i]+=B[i][k]*ctrlss->ry[k];
        }
    }
    for(int i=0; i<13; i++){
        ctrlss->x[i]=ctrlss->x_next[i];
    }
    return 1;
}

int MotorController::quad_LQG_CALC(QUAD_LQG *ctrlss){
    
    for(int i=0; i<2; i++){
        ctrlss->U[i]=0;
        for(int j=0; j<6; j++){
            ctrlss->U[i]+=C_quad[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<4; k++){
            ctrlss->U[i]+=D_quad[i][k]*ctrlss->ry[k];
        }
    }
    for(int i=0; i<6; i++){
        ctrlss->x_next[i]=0;
        for(int j=0; j<6; j++){
            ctrlss->x_next[i]+=A_quad[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<4; k++){
            ctrlss->x_next[i]+=B_quad[i][k]*ctrlss->ry[k];
        }
    }
    for(int i=0; i<6; i++){
        ctrlss->x[i]=ctrlss->x_next[i];
    }
    return 1;
}

int MotorController::eris_LQG_CALC(ERIS_LQG *ctrlss){
    
    for(int i=0; i<3; i++){
        ctrlss->U[i]=0;
        for(int j=0; j<7; j++){
            ctrlss->U[i]+=C_eris[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<6; k++){
            ctrlss->U[i]+=D_eris[i][k]*ctrlss->ry[k];
        }
    }
    for(int i=0; i<7; i++){
        ctrlss->x_next[i]=0;
        for(int j=0; j<7; j++){
            ctrlss->x_next[i]+=A_eris[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<6; k++){
            ctrlss->x_next[i]+=B_eris[i][k]*ctrlss->ry[k];
        }
    }
    for(int i=0; i<7; i++){
        ctrlss->x[i]=ctrlss->x_next[i];
    }
    return 1;
}

int MotorController::SS_CTRL_CALC_loss(CTRLSS_loss *ctrlss){
    /*for(int i=INPUT1_C_LOSS; i<INPUTS_C_LOSS; i++){
            ctrlss->V[i]=ctrlss->Xi_s[i]*ctrlss->Y[i]+ctrlss->u[i-INPUT1_C_LOSS];
    }
    for(int i=0; i<INPUT1_C_LOSS; i++){
       ctrlss->V[i]=ctrlss->Xi_a[i]*ctrlss->Y[i]; 
    }
    stateMod(ctrlss->V, ctrlss->Y);

    //calculates Yo aka scalued output u1. this is the command sent to the plant.
    for(int i=0; i<OUT1_C_LOSS; i++){
        quad->RADA_pack_loss.Yo[i]=scale[i]*quad->RADA_pack_loss.Y[i];
    }*/
    //calculates v2 (V[INPUT1_C_LOSS:INPUTS_C_LOSS-1])
    for(int i=INPUT1_C_LOSS; i<INPUTS_C_LOSS; i++){
            ctrlss->V[i]=ctrlss->Xi_s[i-INPUT1_C_LOSS]*ctrlss->Y[i]+ctrlss->u[i-INPUT1_C_LOSS];
    }
    
    
    //calculate u2 (Y2; Y[OUT1_C_LOSS:OUTPUTS_C_LOSS-1])
    for(int i=OUT1_C_LOSS; i<OUTPUTS_C_LOSS; i++){
        ctrlss->Y[i]=0;
        for(int j=0; j<STATES_C_LOSS; j++){
            ctrlss->Y[i]+=C_loss[i][j]*ctrlss->x[j];
        }
        
    }

    //calculates v1_bar (V[0:INPUT1_C_LOSS-1])
    for(int i=0; i<INPUT1_C_LOSS; i++){
       ctrlss->V[i]=ctrlss->Xi_a[i]*ctrlss->Y[i]; 
    }

    //calcuates u1 ((Y1; Y[0:OUT1_C_LOSS-1])
    for(int i=0; i<OUT1_C_LOSS; i++){
        ctrlss->Y[i]=0;
        for(int j=0; j<STATES_C_LOSS; j++){
            ctrlss->Y[i]+=C_loss[i][j]*ctrlss->x[j];
        }
        for(int k=INPUT1_C_LOSS; k<INPUTS_C_LOSS; k++){
            ctrlss->x_next[i]+=D_loss[i][k]*ctrlss->V[k];
        }
    }

    //calculates Yo aka scalued output u1. this is the command sent to the plant.
    for(int i=0; i<OUT1_C_LOSS; i++){
        quad->RADA_pack_loss.Yo[i]=scale[i]*quad->RADA_pack_loss.Y[i];
    }

    //calculate the next states x+
    for(int i=0; i<STATES_C_LOSS; i++){
        ctrlss->x_next[i]=0;
        for(int j=0; j<STATES_C_LOSS; j++){
            ctrlss->x_next[i]+=A_loss[i][j]*ctrlss->x[j];
        }
        for(int k=0; k<INPUTS_C_LOSS; k++){
            ctrlss->x_next[i]+=B_loss[i][k]*ctrlss->V[k];
        }
    }
    //sets x to x+
    for(int i=0; i<STATES_C_LOSS; i++){
        ctrlss->x[i]=ctrlss->x_next[i];
    }
    return 1;
}


/**
 * Runs a PID control algorithm on the QUADPID struct.
 * Doesn't saturate the PID correction.
 *
 * @param pidptr A pointer to a QUADPID struct
 *
 */
double MotorController::pidControl(QUADPID *pidptr)
{
    // Used to determine the saturation affect on the signal
    double tempCorrection;

    pidptr->err = pidptr->desired - pidptr->current;
    if (pidptr->err == pidptr->preverr)
    {
        return pidptr->correction;
    }

    // Compute the P and D terms
    pidptr->PID_pcomp = pidptr->p * pidptr->err;
    pidptr->PID_dcomp = pidptr->d * (pidptr->err - pidptr->preverr) / SAMPLE_PERIOD;

    // Compute the I term taking into account the anti-windup protection
    pidptr->PID_icomp += pidptr->i * pidptr->err*SAMPLE_PERIOD;
    /*if (pidptr->Taw != 0)
    {
        // Only use the anti-windup protection if Taw is not zero
        pidptr->PID_icomp += (1 / pidptr->Taw) * pidptr->prevSatDiff;
    }*/

    pidptr->preverr = pidptr->err;
    pidptr->correction = pidptr->PID_pcomp + pidptr->PID_icomp + pidptr->PID_dcomp;

    // Store the un-saturated correction for windup implementation
    tempCorrection = pidptr->correction;

   
    // Compute how much saturation is affecting the signal
    pidptr->prevSatDiff = pidptr->correction - tempCorrection;

    return pidptr->correction;
}

float MotorController::dataFilter(struct filter *filter, float data){
    float calc;
    calc=data*filter->inputGain+filter->filterPre*filter->outputGain+filter->inputPreGain*filter->inputPre;
    filter->filterPre=calc;
    filter->inputPre=data;
    return calc;
}
/**
 * Runs a PID control algorithm on the QUADPID struct.
 * Filters the derivative of the error with a first order
 * filter.
 * 
 * @param pidptr A pointer to a QUADPID struct
 *
 */
double MotorController::pidControlFilt(QUADPID *pidptr)
{
    pidptr->err = pidptr->desired - pidptr->current;
    pidptr->filtError = pidptr->filtErrorPre*pidptr->filtGain+pidptr->err*pidptr->errorGain;
    if (pidptr->err == pidptr->preverr)
    {
        return pidptr->correction;
    }
    pidptr->PID_pcomp = pidptr->p * pidptr->err;
    pidptr->PID_icomp += pidptr->i * pidptr->err*SAMPLE_PERIOD;
    pidptr->PID_dcomp = pidptr->d * (pidptr->filtError - pidptr->filtErrorPre) / SAMPLE_PERIOD;
    pidptr->preverr = pidptr->err;
    pidptr->filtErrorPre = pidptr->filtError;
    pidptr->correction = pidptr->PID_pcomp + pidptr->PID_icomp + pidptr->PID_dcomp;
  
    return pidptr->correction;
}

double MotorController::pidControlFilt_dt(QUADPID *pidptr, double dt)
{
    pidptr->err = pidptr->desired - pidptr->current;
    pidptr->filtError = pidptr->filtErrorPre*pidptr->filtGain+pidptr->err*pidptr->errorGain;
    if (pidptr->err == pidptr->preverr)
    {
        return pidptr->correction;
    }
    pidptr->PID_pcomp = pidptr->p * pidptr->err;
    pidptr->PID_icomp += pidptr->i * pidptr->err*dt;
    pidptr->PID_dcomp = pidptr->d * (pidptr->filtError - pidptr->filtErrorPre) / dt;
    pidptr->preverr = pidptr->err;
    pidptr->filtErrorPre = pidptr->filtError;
    pidptr->correction = pidptr->PID_pcomp + pidptr->PID_icomp + pidptr->PID_dcomp;
  
    return pidptr->correction;
}
/******************** pid controller methods ********************/

// TODO: Dylan, remove this completely. we can create a better init function instead of this crap
void MotorController::initAllQuadMotors()
{
    initQuadMotor(0);
    initQuadMotor(1);
    initQuadMotor(2);
    initQuadMotor(3);
}
/****************************************** New Code ******************************************/

/**
 * Shuts the motor controller down, destroying any resources & releasing
 * any devices the controller is holding.
 */
void MotorController::shutdown()
{
    if (s_instance != NULL)
    {
        delete s_instance;
        s_instance = NULL;
    }
}

/*
 * This will stop all quad and ground robot motors
 */
void MotorController::stop()
{
    // Turn off the quad motors
    setDutyCycle(28000, 1);
    setDutyCycle(28000, 2);
    setDutyCycle(28000, 3);
    setDutyCycle(28000, 4);

    stopGroundRobot();
}

/*
 * This will stop all ground robot motors.  This is different from setMotors() because
 * the controller will still maintain the connection to the motor
 * device descriptors, but the motors will be stopped.
 */
void MotorController::stopGroundRobot()
{
    // Set the ground robot speeds to 0
    memset(&motor_speeds, 0, sizeof(motor_control_manual_t));
    motor_speeds.motor_speed_front = 0;
    motor_speeds.motor_speed_rear  = 0;
    motor_speeds.motor_speed_left  = 0;
    motor_speeds.motor_speed_right = 0;
    // This stops the motors immedietly, as opposed to other methods
    // which wait for run() to be called to set the speeds
    setMotorSpeeds();

    current_mode = MOTOR_CONTROL_MODE_FREE;
}

/**
 * This will set the motors to free mode.  In free mode, the
 * association with the motor device descriptors is still maintained,
 * but the speeds of the motors are never set, allowing other programs
 * to set the motor speeds, and this program acts as a sensor for the motor
 * encoders
 */
void MotorController::setMotors()
{
    if (current_mode == MOTOR_CONTROL_MODE_RELEASE)
    {
        initAllMotors();
        initAllQuadMotors();
    }

    stop();
    current_mode = MOTOR_CONTROL_MODE_FREE;
}

/*
 * This will run the motors in speed-profile mode, moving with the
 * rotational speed & in the direction given.
 *
 * @param command The parameters to control the motors
 */
void MotorController::setMotors(const struct motor_control_speed_t *command)
{
    if (current_mode == MOTOR_CONTROL_MODE_RELEASE)
    {
        initAllMotors();
    }

    memcpy(&current_speed_control_settings, command, sizeof(motor_control_speed_t));
    current_mode = MOTOR_CONTROL_MODE_SPEED;
}

/*
 * This will control the motors in a manual way, such that
 * each motor can be controlled individually.
 *
 * @param command The parameters for the movement command.
 */
void MotorController::setMotors(const struct motor_control_manual_t *command)
{
    if (current_mode == MOTOR_CONTROL_MODE_RELEASE)
    {
        initAllMotors();
    }
    current_mode = MOTOR_CONTROL_MODE_MANUAL;
    memcpy(&current_manual_control_settings, command, sizeof(motor_control_manual_t));
   /* motor_speeds.motor_speed_front = motors.front.speed_multiplier * command->motor_speed_front;
    motor_speeds.motor_speed_rear  = motors.rear.speed_multiplier * command->motor_speed_rear;
    motor_speeds.motor_speed_left  = motors.left.speed_multiplier  * command->motor_speed_left;
    motor_speeds.motor_speed_right = motors.right.speed_multiplier * command->motor_speed_right;*/
    motor_speeds.motor_speed_front = command->motor_speed_front;
    motor_speeds.motor_speed_rear  = command->motor_speed_rear;
    motor_speeds.motor_speed_left  = command->motor_speed_left;
    motor_speeds.motor_speed_right = command->motor_speed_right;

    //printf("(f, r, l, r) = (%d, %d, %d, %d)\n", motor_speeds.motor_speed_front, motor_speeds.motor_speed_rear, motor_speeds.motor_speed_left, motor_speeds.motor_speed_right);
}

/**
 * Runs one iteration of the motor controller; Sets
 * the speeds of the motors according to how the user / AI
 * wants them to go.
 *
 * @param dt The time difference since the last call to this method
 */
void MotorController::run(float dt)
{
    PROFILE("MotorController::run");
    e_stop = 0;

    if (use_bounds_limits)
    {
        const struct localization_robot_t *location_history;
        struct localization_robot_t our_location;

        if ((location_history = LocalizationSystem::instance()->getRobotData(Korebot::instance()->getRobotID())) == NULL)
        {
            // Unrecognized KorebotID.  Stop
            e_stop = 1;
        }
        else
        {
            double dx = location_history[SENSOR_HISTORY - 2].x - location_history[SENSOR_HISTORY - 1].x;
            double dy = location_history[SENSOR_HISTORY - 2].y - location_history[SENSOR_HISTORY - 1].y;
            double dt = location_history[SENSOR_HISTORY - 2].timestamp - location_history[SENSOR_HISTORY - 1].timestamp;

            double vx = dx / dt;
            double vy = dy / dt;

            // Attempt to predict where the ball is with the 200ms localization system delay
            our_location.x = location_history[SENSOR_HISTORY - 1].x + vx * 200;
            our_location.y = location_history[SENSOR_HISTORY - 1].y + vy * 200;
            our_location.orientation = location_history[SENSOR_HISTORY - 1].orientation;

            short localization_heading = (current_speed_control_settings.heading + our_location.orientation) % 360;
            if (current_speed_control_settings.linear_speed < 0)
            {
                localization_heading = (localization_heading + 180) % 360;
            }

            // Position is unknown, stop
            if (our_location.x < 0 || our_location.y < 0)
            {
                e_stop = 1;
            }

            if (our_location.x < min_x)
            {
                if (current_mode == MOTOR_CONTROL_MODE_SPEED)
                {
                    if (localization_heading > 90 && localization_heading < 270)
                    {
                        e_stop = 1;
                    }
                }
                else
                {
                    e_stop = 1;
                }
            }
            else if (our_location.x > max_x)
            {
                if (current_mode == MOTOR_CONTROL_MODE_SPEED)
                {
                    if (localization_heading < 90 || localization_heading > 270)
                    {
                        e_stop = 1;
                    }
                }
                else
                {
                    e_stop = 1;
                }
            }

            if (our_location.y < min_y)
            {
                if (current_mode == MOTOR_CONTROL_MODE_SPEED)
                {
                    if (localization_heading > 180)
                    {
                        e_stop = 1;
                    }
                }
                else
                {
                    e_stop = 1;
                }
            }
            else if (our_location.y > max_y)
            {
                if (current_mode == MOTOR_CONTROL_MODE_SPEED)
                {
                    if (localization_heading < 180)
                    {
                        e_stop = 1;
                    }
                }
                else
                {
                    e_stop = 1;
                }
            }

            /*printf("%d::%d (%d,%d)::(%d,%d)-(%d,%d)\n",
                    e_stop,
                    localization_heading,
                    our_location.x, our_location.y,
                    min_x,min_y,max_x,max_y);*/
        }
    }

    switch (current_mode)
    {
        case MOTOR_CONTROL_MODE_SPEED:
            motor_speeds.motor_speed_front = motors.front.speed_multiplier * ( (sin_256(current_speed_control_settings.heading) * current_speed_control_settings.linear_speed) / 256 - current_speed_control_settings.rotational_velocity);
            motor_speeds.motor_speed_rear  = motors.rear.speed_multiplier  * (-(sin_256(current_speed_control_settings.heading) * current_speed_control_settings.linear_speed) / 256 - current_speed_control_settings.rotational_velocity);
            motor_speeds.motor_speed_right = motors.right.speed_multiplier * ( (cos_256(current_speed_control_settings.heading) * current_speed_control_settings.linear_speed) / 256 - current_speed_control_settings.rotational_velocity);
            motor_speeds.motor_speed_left  = motors.left.speed_multiplier  * (-(cos_256(current_speed_control_settings.heading) * current_speed_control_settings.linear_speed) / 256 - current_speed_control_settings.rotational_velocity);
        case MOTOR_CONTROL_MODE_MANUAL:
            setMotorSpeeds();
            break;
    };
}

/**
 * Processes a packet received over the network
 *
 * @param packet The packet received from the network
 */
void MotorController::processPacket(MotorControl *packet)
{
    PROFILE("MotorController::processPacket");
    switch (packet->getCommand())
    {
        case MOTOR_CONTROL_MODE_RELEASE:
            // releaseMotors function was removed, does nothing
            break;
        case MOTOR_CONTROL_MODE_FREE:
            setMotors();
            break;
        case MOTOR_CONTROL_MODE_MANUAL:
            setMotors(((MotorControlManual *)packet)->getMotorSpeeds());
            break;
        case MOTOR_CONTROL_MODE_SPEED:
            setMotors(((MotorControlSpeed *)packet)->getMotorSpeeds());
            break;
        case MOTOR_CONTROL_COMMAND_STOP:
            AI::instance()->stop();
            stop();
            break;

        default:
            fprintf(stderr, "MotorController::processPacket:Error: Unknown motor command (%d)\n", packet->getCommand());
            break;
    }
}

/**
 * Sends status information about the sensor manager to the control server.
 *
 * This includes the mode & speeds of the motors.
 */
void MotorController::sendConfig()
{
    if (current_mode == MOTOR_CONTROL_MODE_RELEASE)
    {
        MotorControl control_method((char)MOTOR_CONTROL_MODE_RELEASE);
        Korebot::instance()->getControlServerConnection()->transmit(&control_method);
    }
    else if (current_mode == MOTOR_CONTROL_MODE_FREE)
    {
        MotorControl control_method((char)MOTOR_CONTROL_MODE_FREE);
        Korebot::instance()->getControlServerConnection()->transmit(&control_method);
    }
    else if (current_mode == MOTOR_CONTROL_MODE_MANUAL)
    {
        MotorControlManual control_method(current_manual_control_settings);
        Korebot::instance()->getControlServerConnection()->transmit(&control_method);
    }
    else if (current_mode == MOTOR_CONTROL_MODE_SPEED)
    {
        MotorControlSpeed control_method(current_speed_control_settings);
        Korebot::instance()->getControlServerConnection()->transmit(&control_method);
    }

    ConfigurationPacket config(CONFIGURATION_OPTION_MOTOR_BOUNDS_LIMIT, use_bounds_limits);
    Korebot::instance()->getControlServerConnection()->transmit(&config);
}

/**
 * Prints the status of various modules on the Korebot
 */
void MotorController::printStatus()
{
    FILE *PrintFile;
    PrintFile = fopen ("Motor_Status.txt", "a");

    fprintf(PrintFile, "MOTOR CONTROLLER STATUS:\n");
    fprintf(PrintFile, "Current Control Mode:\t\t\t\t");
    switch (current_mode)
    {
        case MOTOR_CONTROL_MODE_RELEASE:
            fprintf(PrintFile, "Released\n");
            break;
        case MOTOR_CONTROL_MODE_FREE:
            fprintf(PrintFile, "Free\n");
            break;
        case MOTOR_CONTROL_MODE_MANUAL:
            fprintf(PrintFile, "Manual\n");
            break;
        case MOTOR_CONTROL_MODE_SPEED:
            fprintf(PrintFile, "Speed\n");
            break;
        default:
            fprintf(PrintFile, "Unknown\n");
            break;
    }

    fprintf(PrintFile, "Bounds Limitation:\t\t\t\t%s\n", e_stop ? "Robot is out of bounds, motors are stopped." : "Motors are Running");
    printMotorStatus("Left", motors.left);
    printMotorStatus("Right", motors.right);
    printMotorStatus("Front", motors.front);
    printMotorStatus("Rear", motors.rear);

    fprintf(PrintFile, "\n");
    fclose(PrintFile);
}

/******************** Get and Set methods ********************/
/**
 * Gets if the motor controller is using bounds limiting
 *
 * @returns 1, if the korebot stops the motors when it leaves the bounds of the playing field, 0 otherwise
 */
int MotorController::getUseBoundsLimit()
{
    return use_bounds_limits;
}

/**
 * Gets if the motor controller is logging status
 *
 * @returns 1, if the motor controller is logging, 0 otherwise
 */
int MotorController::getUseStatusLogging()
{
    return use_status_logging;
}

/**
 * Sets if the motor controller should stop the motors if the korebot leaves the bounds of the playing field
 *
 * @param useBoundsLimit 1, if the korebot should stay in-bounds, 0 otherwise
 */
void MotorController::setUseBoundsLimit(int useBoundsLimit)
{
    use_bounds_limits = useBoundsLimit;
}

/**
 * Sets if the motor controller should log the status information to a file
 *
 * @param useStatusLogging 1, if the motor controller should log, 0 otherwise
 */
void MotorController::setUseStatusLogging(int useStatusLogging)
{
    use_status_logging = useStatusLogging;
}


/******************** Private methods ********************/
/**
 * Initializes one motor
 *
 * @param motor The motor structure to initialize
 */
void MotorController::initMotor(struct motor_t &motor)
{
    if (strcmp(motor.name, "Erismotor:PriMotor0") == 0)
    {
        motor.dev_desc = 0;
    }
    else if (strcmp(motor.name, "Erismotor:PriMotor1") == 0)
    {
        motor.dev_desc = 1;
    }
    else if (strcmp(motor.name, "Erismotor:PriMotor2") == 0)
    {
        motor.dev_desc = 2;
    }
    else if (strcmp(motor.name, "Erismotor:PriMotor3") == 0)
    {
        motor.dev_desc = 3;
    }
    else
    {
        motor.dev_desc = -1;
    }

    // If the device exists.
    if (motor.dev_desc != -1)
    {
        // Write PID values
        write_word(motor.dev_desc, kk, (uword)motor.pid_kk);
        write_word(motor.dev_desc, kff, (uword)motor.pid_kff);
        write_word(motor.dev_desc, kp, (uword)motor.pid_kp);
        write_word(motor.dev_desc, kd, (uword)motor.pid_kd);
        write_word(motor.dev_desc, ka, (uword)motor.pid_ka);
        write_long(motor.dev_desc, ki, (udword)motor.pid_ki);
        write_word(motor.dev_desc, kih, (uword)motor.pid_kih);
        write_word(motor.dev_desc, kil, (uword)motor.pid_kil);
        write_word(motor.dev_desc, kf0, (uword)motor.pid_kf0);
        write_word(motor.dev_desc, kf0, (uword)motor.pid_kf0);
        write_word(motor.dev_desc, kf1, (uword)motor.pid_kf1);
        write_word(motor.dev_desc, kf2, (uword)motor.pid_kf2);
        write_word(motor.dev_desc, kf3, (uword)motor.pid_kf3);
        write_word(motor.dev_desc, kdfil, (uword)motor.pid_kdfil);
        write_word(motor.dev_desc, driveplus, (uword)motor.pid_driveplus);
        write_word(motor.dev_desc, driveminus, (uword)motor.pid_driveminus);
        write_long(motor.dev_desc, accel, (udword)motor.pid_accel);
        write_long(motor.dev_desc, slewlimit, (udword)motor.pid_slewlimit);
        write_word(motor.dev_desc, maxpwm, motor.pid_maxpwm);

        //Hardcoded values
        write_word(0, prescale, 4660/*2671*/);  // PWM Rate 6711 28.8kHz  15kHz==3495
        write_word(0, postscale, 20/*229*/);  // Sample Rate //was 200=144Hz 576==50Hz 72=400Hz
        write_long(motor.dev_desc, desvel, 100); //ensure target velocity 0
        write_word(motor.dev_desc, profile, 1);
        write_word(motor.dev_desc, pid, 0x0000);  // Sets PID usage to true
        write_word(motor.dev_desc, openloop, 0xFFFF);
    }
    else
    {
        fprintf(stderr, "Error: Could not initialize motor.\n");
    }
}

/**
 * Initializes all the motors
 */
void MotorController::initAllMotors()
{
    initMotor(motors.front);
    initMotor(motors.left);
    initMotor(motors.right);
    initMotor(motors.rear);
}

/**
 * Passes the motor speeds onto the motor board.  After
 * this command has been executed, the robot will begin to
 * move.
 */
void MotorController::setMotorSpeeds()
{
    // set the motor specified by its descriptor (dev_desc), to the speed we desire
    PROFILE("MotorController::setMotorSpeeds");

    //ERIS CODE ADDED HERE
    //printf("**setMotorSpeeds() **\n");
    if ((motors.front.dev_desc != -1) && (motors.rear.dev_desc != -1) && (motors.right.dev_desc != -1) && (motors.left.dev_desc != -1))
    {
            if(motor_speeds.motor_speed_right<0){
                write_word(motors.right.dev_desc, dira, 0xFFFF);
                write_word(motors.right.dev_desc, pwmgena, -motor_speeds.motor_speed_right);
            }else{
                write_word(motors.right.dev_desc, dira, 0x0000);
                write_word(motors.right.dev_desc, pwmgena, motor_speeds.motor_speed_right);
            }
            if(motor_speeds.motor_speed_left<0){
                write_word(motors.left.dev_desc, dira, 0xFFFF);
                write_word(motors.left.dev_desc, pwmgena, -motor_speeds.motor_speed_left);
            }else{
                write_word(motors.left.dev_desc, pwmgena, motor_speeds.motor_speed_left);
                write_word(motors.left.dev_desc, dira, 0x0000);
            }
            if(motor_speeds.motor_speed_front<0){
                write_word(motors.front.dev_desc, dira, 0xFFFF);
                write_word(motors.front.dev_desc, pwmgena, -motor_speeds.motor_speed_front);
            }else{
                write_word(motors.front.dev_desc, dira, 0x0000);
                write_word(motors.front.dev_desc, pwmgena, motor_speeds.motor_speed_front);
            }if(motor_speeds.motor_speed_rear<0){
                write_word(motors.rear.dev_desc, dira, 0xFFFF);
                write_word(motors.rear.dev_desc, pwmgena, -motor_speeds.motor_speed_rear);
            }else{
                write_word(motors.rear.dev_desc, dira, 0x0000);
                write_word(motors.rear.dev_desc, pwmgena, motor_speeds.motor_speed_rear);
            }
       /*if (e_stop)
        {
            printf("e_stop?!?!\n");
            if (current_motor_speeds.motor_speed_front != 0)
            {
                write_long(motors.front.dev_desc, desvel, 0);
            }
            if (current_motor_speeds.motor_speed_rear != 0)
            {
                write_long(motors.rear.dev_desc, desvel, 0);
            }
            if (current_motor_speeds.motor_speed_right != 0)
            {
                write_long(motors.right.dev_desc, desvel, 0);
            }
            if (current_motor_speeds.motor_speed_left != 0)
            {
                write_long(motors.left.dev_desc, desvel, 0);
            }

            memset(&current_motor_speeds, 0, sizeof(motor_control_manual_t));
        }
        else
        {
            if (use_status_logging)
            {
                printStatus();
            }
            //printf("(current_motor_speeds.motor_speed_front) %d != %d (motor_speeds.motor_speed_front)\n", current_motor_speeds.motor_speed_front, motor_speeds.motor_speed_front);
            if (current_motor_speeds.motor_speed_front != motor_speeds.motor_speed_front)
            {
                if (motor_speeds.motor_speed_front > motors.front.max_motor_speed)
                {
                    motor_speeds.motor_speed_front = motors.front.max_motor_speed;
                }
                else if (motor_speeds.motor_speed_front < -motors.front.max_motor_speed)
                {
                    motor_speeds.motor_speed_front = -motors.front.max_motor_speed;
                }

                write_long(motors.front.dev_desc, desvel, motor_speeds.motor_speed_front);
                
                //printf("front speed = %d\n", motor_speeds.motor_speed_front);

            }
            //printf("motor speed rear: %d = %d\n", current_motor_speeds.motor_speed_rear, motor_speeds.motor_speed_rear);
            if (current_motor_speeds.motor_speed_rear != motor_speeds.motor_speed_rear)
            {
                if (motor_speeds.motor_speed_rear > motors.rear.max_motor_speed)
                {
                    motor_speeds.motor_speed_rear = motors.rear.max_motor_speed;
                }
                else if (motor_speeds.motor_speed_rear < -motors.rear.max_motor_speed)
                {
                    motor_speeds.motor_speed_rear = -motors.rear.max_motor_speed;
                }

                write_long(motors.rear.dev_desc, desvel, motor_speeds.motor_speed_rear);
                //printf("front speed = %d\n", motor_speeds.motor_speed_rear);


            }
            //printf("motor speed right: %d = %d\n", current_motor_speeds.motor_speed_right, motor_speeds.motor_speed_right);
            if (current_motor_speeds.motor_speed_right != motor_speeds.motor_speed_right)
            {
                if (motor_speeds.motor_speed_right > motors.right.max_motor_speed)
                {
                    motor_speeds.motor_speed_right = motors.right.max_motor_speed;
                }
                else if (motor_speeds.motor_speed_right < -motors.right.max_motor_speed)
                {
                    motor_speeds.motor_speed_right = -motors.right.max_motor_speed;
                }

                write_long(motors.right.dev_desc, desvel, motor_speeds.motor_speed_right);
                //printf("front speed = %d\n", motor_speeds.motor_speed_right);


            }
            //printf("motor speed left: %d = %d\n", current_motor_speeds.motor_speed_left, motor_speeds.motor_speed_left);
            if (current_motor_speeds.motor_speed_left != motor_speeds.motor_speed_left)
            {
                if (motor_speeds.motor_speed_left > motors.left.max_motor_speed)
                {
                    motor_speeds.motor_speed_left = motors.left.max_motor_speed;
                }
                else if (motor_speeds.motor_speed_left < -motors.left.max_motor_speed)
                {
                    motor_speeds.motor_speed_left = -motors.left.max_motor_speed;
                }

                write_long(motors.left.dev_desc, desvel, motor_speeds.motor_speed_left);
                //printf("front speed = %d\n", motor_speeds.motor_speed_left);
            }
            memcpy(&current_motor_speeds, &motor_speeds, sizeof(motor_control_manual_t));
        }*/
    }
}

/**
 * Prints the status of the given motor to console
 *
 * @param motor A human-readable description of the motor
 * @param motor The motor details
 */
void MotorController::printMotorStatus(const char *name, const motor_t &motor)
{
    // Software & Hardware Options
    unsigned char software , hardware;

    // Error & Status Regs
    unsigned char error , status;

    FILE *PrintFile;
    PrintFile = fopen ("Motor_Status.txt", "a");

    if (motor.dev_desc != NULL)
    {
        fprintf(PrintFile, "%s Motor Status:\n", name);

        software = 0;
        hardware = 0;
        error    = 0;
        status   = 0;
        fprintf(PrintFile, "\tNot meaningful when compiled for x86 architectures\n");
        fclose(PrintFile);
    }
}
#ifdef ssDrop
int MotorController::SSctrl_config_loss(){
    printf("\tHello drop\n\r");
    for(int i=0; i<STATES_C_LOSS; i++){
        quad->RADA_pack_loss.x[i]=0;
        quad->RADA_pack_loss.x_next[i]=0;
    }
    for(int i=0; i<INPUT2_C_LOSS; i++){
        quad->RADA_pack_loss.u[i]=0;
    }
    for(int i=0; i<INPUTS_C_LOSS; i++){
        quad->RADA_pack_loss.V[i]=0;
    }
    for(int i=0; i<OUTPUTS_C_LOSS; i++){
        quad->RADA_pack_loss.Y[i]=0;
    }
    for(int i=0; i<OUT1_C_LOSS; i++){
        quad->RADA_pack_loss.Yo[i]=0;
    }
/*
//0.989992208469593 0   0   0   0.000191865330849025    -0.00116412109518594    0   0   0   0   0   0   0
    quad->RADA_pack_loss.A[0][0]=0.989992208469593;
    quad->RADA_pack_loss.A[0][1]=0;
    quad->RADA_pack_loss.A[0][2]=0;
    quad->RADA_pack_loss.A[0][3]=0;
    quad->RADA_pack_loss.A[0][4]=0.000191865330849025;
    quad->RADA_pack_loss.A[0][5]=-0.00116412109518594;
    quad->RADA_pack_loss.A[0][6]=0;
    quad->RADA_pack_loss.A[0][7]=0;
    quad->RADA_pack_loss.A[0][8]=0;
    quad->RADA_pack_loss.A[0][9]=0;
    quad->RADA_pack_loss.A[0][10]=0;
    quad->RADA_pack_loss.A[0][11]=0;
    quad->RADA_pack_loss.A[0][12]=0;

//0 0.989992208469592   0   -0.000191865330849319   0   0   -0.00116412109518641    0   0   0   0   0   0
    quad->RADA_pack_loss.A[1][0]=0;
    quad->RADA_pack_loss.A[1][1]=0.989992208469592;
    quad->RADA_pack_loss.A[1][2]=0;
    quad->RADA_pack_loss.A[1][3]=-0.000191865330849319;
    quad->RADA_pack_loss.A[1][4]=0;
    quad->RADA_pack_loss.A[1][5]=0;
    quad->RADA_pack_loss.A[1][6]=-0.00116412109518641;
    quad->RADA_pack_loss.A[1][7]=0;
    quad->RADA_pack_loss.A[1][8]=0;
    quad->RADA_pack_loss.A[1][9]=0;
    quad->RADA_pack_loss.A[1][10]=0;
    quad->RADA_pack_loss.A[1][11]=0;
    quad->RADA_pack_loss.A[1][12]=0;

//0 0   0.989988134923058   0   0   0   0   0.000284569383728215    0   0   0   0   0
    quad->RADA_pack_loss.A[2][0]=0;
    quad->RADA_pack_loss.A[2][1]=0;
    quad->RADA_pack_loss.A[2][2]=0.989988134923058;
    quad->RADA_pack_loss.A[2][3]=0;
    quad->RADA_pack_loss.A[2][4]=0;
    quad->RADA_pack_loss.A[2][5]=0;
    quad->RADA_pack_loss.A[2][6]=0;
    quad->RADA_pack_loss.A[2][7]=0.000284569383728215;
    quad->RADA_pack_loss.A[2][8]=0;
    quad->RADA_pack_loss.A[2][9]=0;
    quad->RADA_pack_loss.A[2][10]=0;
    quad->RADA_pack_loss.A[2][11]=0;
    quad->RADA_pack_loss.A[2][12]=0;


//0 -0.000552092703578626   0   0.876984862268608   0   0   -0.0166000167523665 0   0.00574298512921447 
//0   0   -0.000266965285180296   0
    quad->RADA_pack_loss.A[3][0]=0;
    quad->RADA_pack_loss.A[3][1]=-0.000552092703578626;
    quad->RADA_pack_loss.A[3][2]=0;
    quad->RADA_pack_loss.A[3][3]=0.876984862268608;
    quad->RADA_pack_loss.A[3][4]=0;
    quad->RADA_pack_loss.A[3][5]=0;
    quad->RADA_pack_loss.A[3][6]=-0.0166000167523665;
    quad->RADA_pack_loss.A[3][7]=0;
    quad->RADA_pack_loss.A[3][8]=0.00574298512921447;
    quad->RADA_pack_loss.A[3][9]=0;
    quad->RADA_pack_loss.A[3][10]=0;
    quad->RADA_pack_loss.A[3][11]=-0.000266965285180296;
    quad->RADA_pack_loss.A[3][12]=0;

//0.000552092703578332  0   0   0   0.876984862268609   0.0166000167523659  0   0   0   
//0.00574298512921447 0.000266965285180296    0   0
    quad->RADA_pack_loss.A[4][0]=0.000552092703578332;
    quad->RADA_pack_loss.A[4][1]=0;
    quad->RADA_pack_loss.A[4][2]=0;
    quad->RADA_pack_loss.A[4][3]=0;
    quad->RADA_pack_loss.A[4][4]=0.876984862268609;
    quad->RADA_pack_loss.A[4][5]=0.0166000167523659;
    quad->RADA_pack_loss.A[4][6]=0;
    quad->RADA_pack_loss.A[4][7]=0;
    quad->RADA_pack_loss.A[4][8]=0;
    quad->RADA_pack_loss.A[4][9]=0.00574298512921447;
    quad->RADA_pack_loss.A[4][10]=0.000266965285180296;
    quad->RADA_pack_loss.A[4][11]=0;
    quad->RADA_pack_loss.A[4][12]=0;

//-0.0167963985918934   0   0   0   0.0173369322096912  0.842475401018034   0   0   0   
//0.000667545102546464    0.00620922595435761 0   0
    quad->RADA_pack_loss.A[5][0]=-0.0167963985918934;
    quad->RADA_pack_loss.A[5][1]=0;
    quad->RADA_pack_loss.A[5][2]=0;
    quad->RADA_pack_loss.A[5][3]=0;
    quad->RADA_pack_loss.A[5][4]=0.0173369322096912;
    quad->RADA_pack_loss.A[5][5]=0.842475401018034;
    quad->RADA_pack_loss.A[5][6]=0;
    quad->RADA_pack_loss.A[5][7]=0;
    quad->RADA_pack_loss.A[5][8]=0;
    quad->RADA_pack_loss.A[5][9]=0.000667545102546464;
    quad->RADA_pack_loss.A[5][10]=0.00620922595435761;
    quad->RADA_pack_loss.A[5][11]=0;
    quad->RADA_pack_loss.A[5][12]=0;

//0 -0.0167963985918938 0   -0.0173369322096921 0   0   0.842475401018035   0   -0.000667545102546468   
//0   0   0.00620922595435761 0
    quad->RADA_pack_loss.A[6][0]=0;
    quad->RADA_pack_loss.A[6][1]=-0.0167963985918938;
    quad->RADA_pack_loss.A[6][2]=0;
    quad->RADA_pack_loss.A[6][3]=-0.0173369322096921;
    quad->RADA_pack_loss.A[6][4]=0;
    quad->RADA_pack_loss.A[6][5]=0;
    quad->RADA_pack_loss.A[6][6]=0.842475401018035;
    quad->RADA_pack_loss.A[6][7]=0;
    quad->RADA_pack_loss.A[6][8]=-0.000667545102546468;
    quad->RADA_pack_loss.A[6][9]=0;
    quad->RADA_pack_loss.A[6][10]=0;
    quad->RADA_pack_loss.A[6][11]=0.00620922595435761;
    quad->RADA_pack_loss.A[6][12]=0;


//0 0   -0.0212890355242736 0   0   0   0   0.898033804443649   0   0   0   0   0.00853041746011101
    quad->RADA_pack_loss.A[7][0]=0;
    quad->RADA_pack_loss.A[7][1]=0;
    quad->RADA_pack_loss.A[7][2]=-0.0212890355242736;
    quad->RADA_pack_loss.A[7][3]=0;
    quad->RADA_pack_loss.A[7][4]=0;
    quad->RADA_pack_loss.A[7][5]=0;
    quad->RADA_pack_loss.A[7][6]=0;
    quad->RADA_pack_loss.A[7][7]=0.89803380444364;
    quad->RADA_pack_loss.A[7][8]=0;
    quad->RADA_pack_loss.A[7][9]=0;
    quad->RADA_pack_loss.A[7][10]=0;
    quad->RADA_pack_loss.A[7][11]=0;
    quad->RADA_pack_loss.A[7][12]=0.00853041746011101;


//0 -0.102720023961029  0   -2.38710021944403   0   0   -0.100412510326261  0   0.152422569425388   
//0   0   -0.0531247673738933 0
    quad->RADA_pack_loss.A[8][0]=0;
    quad->RADA_pack_loss.A[8][1]=-0.102720023961029;
    quad->RADA_pack_loss.A[8][2]=0;
    quad->RADA_pack_loss.A[8][3]=-2.38710021944403;
    quad->RADA_pack_loss.A[8][4]=0;
    quad->RADA_pack_loss.A[8][5]=0;
    quad->RADA_pack_loss.A[8][6]=-0.100412510326261;
    quad->RADA_pack_loss.A[8][7]=0;
    quad->RADA_pack_loss.A[8][8]=0.152422569425388;
    quad->RADA_pack_loss.A[8][9]=0;
    quad->RADA_pack_loss.A[8][10]=0;
    quad->RADA_pack_loss.A[8][11]=-0.0531247673738933;
    quad->RADA_pack_loss.A[8][12]=0;

//0.102720023961027 0   0   0   -2.38710021944387   0.100412510326262   0   0   0   0.152422569425390   
//0.0531247673738932  0   0
    quad->RADA_pack_loss.A[9][0]=0.102720023961027;
    quad->RADA_pack_loss.A[9][1]=0;
    quad->RADA_pack_loss.A[9][2]=0;
    quad->RADA_pack_loss.A[9][3]=0;
    quad->RADA_pack_loss.A[9][4]=-2.38710021944387;
    quad->RADA_pack_loss.A[9][5]=0.100412510326262;
    quad->RADA_pack_loss.A[9][6]=0;
    quad->RADA_pack_loss.A[9][7]=0;
    quad->RADA_pack_loss.A[9][8]=0;
    quad->RADA_pack_loss.A[9][9]=0.152422569425390;
    quad->RADA_pack_loss.A[9][10]=0.0531247673738932;
    quad->RADA_pack_loss.A[9][11]=0;
    quad->RADA_pack_loss.A[9][12]=0;


//-1.40940134139100 0   0   0   0.292247143416554   -2.43484171711265   0   0   0   0.131181138780295   
//0.242070868144933   0   0
    quad->RADA_pack_loss.A[10][0]=-1.40940134139100;
    quad->RADA_pack_loss.A[10][1]=0;
    quad->RADA_pack_loss.A[10][2]=0;
    quad->RADA_pack_loss.A[10][3]=0;
    quad->RADA_pack_loss.A[10][4]=0.292247143416554;
    quad->RADA_pack_loss.A[10][5]=-2.43484171711265;
    quad->RADA_pack_loss.A[10][6]=0;
    quad->RADA_pack_loss.A[10][7]=0;
    quad->RADA_pack_loss.A[10][8]=0;
    quad->RADA_pack_loss.A[10][9]=0.131181138780295;
    quad->RADA_pack_loss.A[10][10]=0.242070868144933;
    quad->RADA_pack_loss.A[10][11]=0;
    quad->RADA_pack_loss.A[10][12]=0;

//0 -1.40940134139099   0   -0.292247143416601  0   0   -2.43484171711260   0   -0.131181138780296  
//0   0   0.242070868144933   0
    quad->RADA_pack_loss.A[11][0]=0;
    quad->RADA_pack_loss.A[11][1]=-1.40940134139099;
    quad->RADA_pack_loss.A[11][2]=0;
    quad->RADA_pack_loss.A[11][3]=-0.292247143416601;
    quad->RADA_pack_loss.A[11][4]=0;
    quad->RADA_pack_loss.A[11][5]=0;
    quad->RADA_pack_loss.A[11][6]=-2.43484171711260;
    quad->RADA_pack_loss.A[11][7]=0;
    quad->RADA_pack_loss.A[11][8]=-0.131181138780296;
    quad->RADA_pack_loss.A[11][9]=0;
    quad->RADA_pack_loss.A[11][10]=0;
    quad->RADA_pack_loss.A[11][11]=0.242070868144933;
    quad->RADA_pack_loss.A[11][12]=0;

//0 0   -2.45695524124574   0   0   0   0   -3.88605172207263   0   0   0   0   0.709993479397112
    quad->RADA_pack_loss.A[12][0]=0;
    quad->RADA_pack_loss.A[12][1]=0;
    quad->RADA_pack_loss.A[12][2]=-2.45695524124574;
    quad->RADA_pack_loss.A[12][3]=0;
    quad->RADA_pack_loss.A[12][4]=0;
    quad->RADA_pack_loss.A[12][5]=0;
    quad->RADA_pack_loss.A[12][6]=0;
    quad->RADA_pack_loss.A[12][7]=-3.88605172207263;
    quad->RADA_pack_loss.A[12][8]=0;
    quad->RADA_pack_loss.A[12][9]=0;
    quad->RADA_pack_loss.A[12][10]=0;
    quad->RADA_pack_loss.A[12][11]=0;
    quad->RADA_pack_loss.A[12][12]=0.709993479397112;


//-0.00998468672533316  0   0   0   0.000184775373459410    -0.0111209978368681 0   0
    quad->RADA_pack_loss.B[0][0]=-0.00998468672533316;
    quad->RADA_pack_loss.B[0][1]=0;
    quad->RADA_pack_loss.B[0][2]=0;
    quad->RADA_pack_loss.B[0][3]=0;
    quad->RADA_pack_loss.B[0][4]=0.000184775373459410;
    quad->RADA_pack_loss.B[0][5]=-0.0111209978368681;
    quad->RADA_pack_loss.B[0][6]=0;
    quad->RADA_pack_loss.B[0][7]=0;

//0 -0.00998468672533402    0   -0.000184775373459704   0   0   -0.0111209978368685 0
    quad->RADA_pack_loss.B[1][0]=0;
    quad->RADA_pack_loss.B[1][1]=-0.00998468672533402;
    quad->RADA_pack_loss.B[1][2]=0;
    quad->RADA_pack_loss.B[1][3]=-0.000184775373459704;
    quad->RADA_pack_loss.B[1][4]=0;
    quad->RADA_pack_loss.B[1][5]=0;
    quad->RADA_pack_loss.B[1][6]=-0.0111209978368685;
    quad->RADA_pack_loss.B[1][7]=0;

//0 0   -0.00997007968898963    0   0   0   0   -0.00964765723865512
    quad->RADA_pack_loss.B[2][0]=0;
    quad->RADA_pack_loss.B[2][1]=0;
    quad->RADA_pack_loss.B[2][2]=-0.00997007968898963 ;
    quad->RADA_pack_loss.B[2][3]=0;
    quad->RADA_pack_loss.B[2][4]=0;
    quad->RADA_pack_loss.B[2][5]=0;
    quad->RADA_pack_loss.B[2][6]=0;
    quad->RADA_pack_loss.B[2][7]=-0.00964765723865512;
 
 //0    0   0   -0.110045939928387  0   0   -0.0156533582874075 0  
    quad->RADA_pack_loss.B[3][0]=0;
    quad->RADA_pack_loss.B[3][1]=0;
    quad->RADA_pack_loss.B[3][2]=0;
    quad->RADA_pack_loss.B[3][3]=-0.110045939928387;
    quad->RADA_pack_loss.B[3][4]=0;
    quad->RADA_pack_loss.B[3][5]=0;
    quad->RADA_pack_loss.B[3][6]=-0.0156533582874075;
    quad->RADA_pack_loss.B[3][7]=0;

//0 0   0   0   -0.110045939928387  0.0156533582874070  0   0
    quad->RADA_pack_loss.B[4][0]=0;
    quad->RADA_pack_loss.B[4][1]=0;
    quad->RADA_pack_loss.B[4][2]=0;
    quad->RADA_pack_loss.B[4][3]=0;
    quad->RADA_pack_loss.B[4][4]=-0.110045939928387;
    quad->RADA_pack_loss.B[4][5]=0.0156533582874070;
    quad->RADA_pack_loss.B[4][6]=0;
    quad->RADA_pack_loss.B[4][7]=0;

//-0.00956550760005890  0   0   0   0.0152277345478759  -0.144587096977999  0   0
    quad->RADA_pack_loss.B[5][0]=-0.00956550760005890;
    quad->RADA_pack_loss.B[5][1]=0;
    quad->RADA_pack_loss.B[5][2]=0;
    quad->RADA_pack_loss.B[5][3]=0;
    quad->RADA_pack_loss.B[5][4]=0.0152277345478759;
    quad->RADA_pack_loss.B[5][5]=-0.144587096977999;
    quad->RADA_pack_loss.B[5][6]=0;
    quad->RADA_pack_loss.B[5][7]=0;

//0 -0.00956550760005937    0   -0.0152277345478765 0   0   -0.144587096977998  0
    quad->RADA_pack_loss.B[6][0]=0;
    quad->RADA_pack_loss.B[6][1]=-0.00956550760005937;
    quad->RADA_pack_loss.B[6][2]=0;
    quad->RADA_pack_loss.B[6][3]=-0.0152277345478765;
    quad->RADA_pack_loss.B[6][4]=0;
    quad->RADA_pack_loss.B[6][5]=0;
    quad->RADA_pack_loss.B[6][6]=-0.144587096977998;
    quad->RADA_pack_loss.B[6][7]=0;

//0 0   -0.00853891606907932    0   0   0   0   -0.0817681222802766  
    quad->RADA_pack_loss.B[7][0]=0;
    quad->RADA_pack_loss.B[7][1]=0;
    quad->RADA_pack_loss.B[7][2]=-0.00853891606907932;
    quad->RADA_pack_loss.B[7][3]=0;
    quad->RADA_pack_loss.B[7][4]=0;
    quad->RADA_pack_loss.B[7][5]=0;
    quad->RADA_pack_loss.B[7][6]=0;
    quad->RADA_pack_loss.B[7][7]=-0.0817681222802766;

//0 0.00344769605246655 0   0.195084984360599   0   0   0.0881266251151363  0
    quad->RADA_pack_loss.B[8][0]=0;
    quad->RADA_pack_loss.B[8][1]=0.00344769605246655;
    quad->RADA_pack_loss.B[8][2]=0;
    quad->RADA_pack_loss.B[8][3]=0.195084984360599;
    quad->RADA_pack_loss.B[8][4]=0;
    quad->RADA_pack_loss.B[8][5]=0;
    quad->RADA_pack_loss.B[8][6]=0.0881266251151363;
    quad->RADA_pack_loss.B[8][7]=0;

//-0.00344769605246546  0   0   0   0.195084984360579   -0.0881266251151323 0   0
    quad->RADA_pack_loss.B[9][0]=-0.00344769605246546;
    quad->RADA_pack_loss.B[9][1]=0;
    quad->RADA_pack_loss.B[9][2]=0;
    quad->RADA_pack_loss.B[9][3]=0;
    quad->RADA_pack_loss.B[9][4]=0.195084984360579;
    quad->RADA_pack_loss.B[9][5]=-0.0881266251151323;
    quad->RADA_pack_loss.B[9][6]=0;
    quad->RADA_pack_loss.B[9][7]=0;

// 0.0365765007059505   0   0   0   -0.122501349477499  0.152304109846906   0   0 
    quad->RADA_pack_loss.B[10][0]=0.0365765007059505;
    quad->RADA_pack_loss.B[10][1]=0;
    quad->RADA_pack_loss.B[10][2]=0;
    quad->RADA_pack_loss.B[10][3]=0;
    quad->RADA_pack_loss.B[10][4]=-0.122501349477499;
    quad->RADA_pack_loss.B[10][5]=0.152304109846906;
    quad->RADA_pack_loss.B[10][6]=0;
    quad->RADA_pack_loss.B[10][7]=0;

//0 0.0365765007059523  0   0.122501349477506   0   0   0.152304109846903   0  
    quad->RADA_pack_loss.B[11][0]=0;
    quad->RADA_pack_loss.B[11][1]=0.0365765007059523;
    quad->RADA_pack_loss.B[11][2]=0;
    quad->RADA_pack_loss.B[11][3]=0.122501349477506;
    quad->RADA_pack_loss.B[11][4]=0;
    quad->RADA_pack_loss.B[11][5]=0;
    quad->RADA_pack_loss.B[11][6]=0.152304109846903;
    quad->RADA_pack_loss.B[11][7]=0;

//0 0   0.0591455425532973  0   0   0   0   0.0998237185263470
    quad->RADA_pack_loss.B[12][0]=0;
    quad->RADA_pack_loss.B[12][1]=0;
    quad->RADA_pack_loss.B[12][2]=0.0591455425532973;
    quad->RADA_pack_loss.B[12][3]=0;
    quad->RADA_pack_loss.B[12][4]=0;
    quad->RADA_pack_loss.B[12][5]=0;
    quad->RADA_pack_loss.B[12][6]=0;
    quad->RADA_pack_loss.B[12][7]=0.0998237185263470;

//-360339.645898939 5795.88518202630    0   -4174.88308962975   -259559.298872039   -402309.671260679   
//6470.95230508925    0   -1676.75598911058   -104246.657827136   -195961.217632940   3151.93937290818    
//0
    quad->RADA_pack_loss.C[0][0]=-360339.645898939;
    quad->RADA_pack_loss.C[0][1]=5795.88518202630;
    quad->RADA_pack_loss.C[0][2]=0;
    quad->RADA_pack_loss.C[0][3]=-4174.88308962975 ;
    quad->RADA_pack_loss.C[0][4]=-259559.298872039;
    quad->RADA_pack_loss.C[0][5]=-402309.671260679;
    quad->RADA_pack_loss.C[0][6]=6470.95230508925;
    quad->RADA_pack_loss.C[0][7]=0;
    quad->RADA_pack_loss.C[0][8]=-1676.75598911058;
    quad->RADA_pack_loss.C[0][9]=-104246.657827136;
    quad->RADA_pack_loss.C[0][10]=-195961.217632940;
    quad->RADA_pack_loss.C[0][11]=3151.93937290818;
    quad->RADA_pack_loss.C[0][12]=0;

//-5795.88518200600 -360339.645898935   0   259559.298872052    -4174.88308961463   -6470.95230507362   
//-402309.671260673   0   104246.657827136    -1676.75598911043   -3151.93937290803   -195961.217632939   
//0
    quad->RADA_pack_loss.C[1][0]=-5795.88518200600;
    quad->RADA_pack_loss.C[1][1]=-360339.645898935;
    quad->RADA_pack_loss.C[1][2]=0;
    quad->RADA_pack_loss.C[1][3]=259559.298872052;
    quad->RADA_pack_loss.C[1][4]=-4174.88308961463;
    quad->RADA_pack_loss.C[1][5]=-6470.95230507362;
    quad->RADA_pack_loss.C[1][6]=-402309.671260673;
    quad->RADA_pack_loss.C[1][7]=0;
    quad->RADA_pack_loss.C[1][8]=104246.657827136;
    quad->RADA_pack_loss.C[1][9]=-1676.75598911043;
    quad->RADA_pack_loss.C[1][10]=-3151.93937290803;
    quad->RADA_pack_loss.C[1][11]=-195961.217632939;
    quad->RADA_pack_loss.C[1][12]=0;

//0 0   -86351.2870513368   0   0   0   0   -125518.348188018   0   0   0   0   -7464.69704623055
    quad->RADA_pack_loss.C[2][0]=0;
    quad->RADA_pack_loss.C[2][1]=0;
    quad->RADA_pack_loss.C[2][2]=-86351.2870513368;
    quad->RADA_pack_loss.C[2][3]=0;
    quad->RADA_pack_loss.C[2][4]=0;
    quad->RADA_pack_loss.C[2][5]=0;
    quad->RADA_pack_loss.C[2][6]=0;
    quad->RADA_pack_loss.C[2][7]=-125518.348188018;
    quad->RADA_pack_loss.C[2][8]=0;
    quad->RADA_pack_loss.C[2][9]=0;
    quad->RADA_pack_loss.C[2][10]=0;
    quad->RADA_pack_loss.C[2][11]=0;
    quad->RADA_pack_loss.C[2][12]=-7464.69704623055;

//0 -24053.2972661013   0   -1634561.01978430   0   0   163869.129775183    0   -753220.908936544   
//0   0   -11356.7669814611   0
    quad->RADA_pack_loss.C[3][0]=0;
    quad->RADA_pack_loss.C[3][1]=-24053.2972661013;
    quad->RADA_pack_loss.C[3][2]=0;
    quad->RADA_pack_loss.C[3][3]=-1634561.01978430;
    quad->RADA_pack_loss.C[3][4]=0;
    quad->RADA_pack_loss.C[3][5]=0;
    quad->RADA_pack_loss.C[3][6]=163869.129775183;
    quad->RADA_pack_loss.C[3][7]=0;
    quad->RADA_pack_loss.C[3][8]=-753220.908936544;
    quad->RADA_pack_loss.C[3][9]=0;
    quad->RADA_pack_loss.C[3][10]=0;
    quad->RADA_pack_loss.C[3][11]=-11356.7669814611;
    quad->RADA_pack_loss.C[3][12]=0;

//24053.2972660986  0   0   0   -1634561.01978417   -163869.129775185   0   0   0   -753220.908936542   
//11356.7669814611    0   0
    quad->RADA_pack_loss.C[4][0]=24053.2972660986;
    quad->RADA_pack_loss.C[4][1]=0;
    quad->RADA_pack_loss.C[4][2]=0;
    quad->RADA_pack_loss.C[4][3]=0;
    quad->RADA_pack_loss.C[4][4]=-1634561.01978417;
    quad->RADA_pack_loss.C[4][5]=-163869.129775185;
    quad->RADA_pack_loss.C[4][6]=0;
    quad->RADA_pack_loss.C[4][7]=0;
    quad->RADA_pack_loss.C[4][8]=0;
    quad->RADA_pack_loss.C[4][9]=-753220.908936542;
    quad->RADA_pack_loss.C[4][10]=11356.7669814611;
    quad->RADA_pack_loss.C[4][11]=0;
    quad->RADA_pack_loss.C[4][12]=0;

//12567.5195399013  -202.142342939525   0   867.864717299047    53956.5666155281    281294.776183980    
//-4524.48750399445   0
    quad->RADA_pack_loss.D[0][0]=12567.5195399013;
    quad->RADA_pack_loss.D[0][1]=-202.142342939525;
    quad->RADA_pack_loss.D[0][2]=0;
    quad->RADA_pack_loss.D[0][3]=867.864717299047;
    quad->RADA_pack_loss.D[0][4]=53956.5666155281;
    quad->RADA_pack_loss.D[0][5]=281294.776183980;
    quad->RADA_pack_loss.D[0][6]=-4524.48750399445;
    quad->RADA_pack_loss.D[0][7]=0;

 //202.142342938264 12567.5195399019    0   -53956.5666155288   867.864717297963    4524.48750399384    
 //281294.776183974    0     
    quad->RADA_pack_loss.D[1][0]=202.142342938264;
    quad->RADA_pack_loss.D[1][1]=12567.5195399019;
    quad->RADA_pack_loss.D[1][2]=0;
    quad->RADA_pack_loss.D[1][3]=-53956.5666155288;
    quad->RADA_pack_loss.D[1][4]=867.864717297963;
    quad->RADA_pack_loss.D[1][5]=4524.48750399384;
    quad->RADA_pack_loss.D[1][6]=281294.776183974;
    quad->RADA_pack_loss.D[1][7]=0;

//0 0   2121.95004491107    0   0   0   0   14636.3328815422
    quad->RADA_pack_loss.D[2][0]=0;
    quad->RADA_pack_loss.D[2][1]=0;
    quad->RADA_pack_loss.D[2][2]=2121.95004491107;
    quad->RADA_pack_loss.D[2][3]=0;
    quad->RADA_pack_loss.D[2][4]=0;
    quad->RADA_pack_loss.D[2][5]=0;
    quad->RADA_pack_loss.D[2][6]=0;
    quad->RADA_pack_loss.D[2][7]=14636.3328815422;

//0 1863.40912226270    0   732246.848842441    0   0   230094.795112572    0
    quad->RADA_pack_loss.D[3][0]=0;
    quad->RADA_pack_loss.D[3][1]=1863.40912226270;
    quad->RADA_pack_loss.D[3][2]=0;
    quad->RADA_pack_loss.D[3][3]=732246.848842441;
    quad->RADA_pack_loss.D[3][4]=0;
    quad->RADA_pack_loss.D[3][5]=0;
    quad->RADA_pack_loss.D[3][6]=230094.795112572;
    quad->RADA_pack_loss.D[3][7]=0;

//-1863.40912226216 0   0   0   732246.848842419    -230094.795112568   0   0
    quad->RADA_pack_loss.D[4][0]=-1863.40912226216;
    quad->RADA_pack_loss.D[4][1]=0;
    quad->RADA_pack_loss.D[4][2]=0;
    quad->RADA_pack_loss.D[4][3]=0;
    quad->RADA_pack_loss.D[4][4]=732246.848842419;
    quad->RADA_pack_loss.D[4][5]=-230094.795112568;
    quad->RADA_pack_loss.D[4][6]=0;
    quad->RADA_pack_loss.D[4][7]=0;


    quad->RADA_pack.slope_x=0;
    quad->RADA_pack.slope_y=0;
    quad->RADA_pack.ref_x=0;
    quad->RADA_pack.ref_y=0;
    quad->RADA_pack.ref_phi=0;
    quad->RADA_pack.ref_theta=0;
    quad->RADA_pack.ref_psi=0;
    */
    return 1;

}
#endif

#ifndef ssMotors
#ifdef ssTracking
int MotorController::SSctrl_config(){
    printf("\tHello\n\r");
    for(int i=0; i<13; i++){
        quad->RADA_pack.x[i]=0;
        quad->RADA_pack.x_next[i]=0;
    }
    for(int i=0; i<8; i++){
        quad->RADA_pack.u[i]=0;
    }
    for(int i=0; i<5; i++){
        quad->RADA_pack.Y[i]=0;
    }

//0.989992208469593 0   0   0   0.000191865330849025    -0.00116412109518594    0   0   0   0   0   0   0
    quad->RADA_pack.A[0][0]=0.989992208469593;
    quad->RADA_pack.A[0][1]=0;
    quad->RADA_pack.A[0][2]=0;
    quad->RADA_pack.A[0][3]=0;
    quad->RADA_pack.A[0][4]=0.000191865330849025;
    quad->RADA_pack.A[0][5]=-0.00116412109518594;
    quad->RADA_pack.A[0][6]=0;
    quad->RADA_pack.A[0][7]=0;
    quad->RADA_pack.A[0][8]=0;
    quad->RADA_pack.A[0][9]=0;
    quad->RADA_pack.A[0][10]=0;
    quad->RADA_pack.A[0][11]=0;
    quad->RADA_pack.A[0][12]=0;

//0 0.989992208469592   0   -0.000191865330849319   0   0   -0.00116412109518641    0   0   0   0   0   0
    quad->RADA_pack.A[1][0]=0;
    quad->RADA_pack.A[1][1]=0.989992208469592;
    quad->RADA_pack.A[1][2]=0;
    quad->RADA_pack.A[1][3]=-0.000191865330849319;
    quad->RADA_pack.A[1][4]=0;
    quad->RADA_pack.A[1][5]=0;
    quad->RADA_pack.A[1][6]=-0.00116412109518641;
    quad->RADA_pack.A[1][7]=0;
    quad->RADA_pack.A[1][8]=0;
    quad->RADA_pack.A[1][9]=0;
    quad->RADA_pack.A[1][10]=0;
    quad->RADA_pack.A[1][11]=0;
    quad->RADA_pack.A[1][12]=0;

//0 0   0.989988134923058   0   0   0   0   0.000284569383728215    0   0   0   0   0
    quad->RADA_pack.A[2][0]=0;
    quad->RADA_pack.A[2][1]=0;
    quad->RADA_pack.A[2][2]=0.989988134923058;
    quad->RADA_pack.A[2][3]=0;
    quad->RADA_pack.A[2][4]=0;
    quad->RADA_pack.A[2][5]=0;
    quad->RADA_pack.A[2][6]=0;
    quad->RADA_pack.A[2][7]=0.000284569383728215;
    quad->RADA_pack.A[2][8]=0;
    quad->RADA_pack.A[2][9]=0;
    quad->RADA_pack.A[2][10]=0;
    quad->RADA_pack.A[2][11]=0;
    quad->RADA_pack.A[2][12]=0;


//0 -0.000552092703578626   0   0.876984862268608   0   0   -0.0166000167523665 0   0.00574298512921447 
//0   0   -0.000266965285180296   0
    quad->RADA_pack.A[3][0]=0;
    quad->RADA_pack.A[3][1]=-0.000552092703578626;
    quad->RADA_pack.A[3][2]=0;
    quad->RADA_pack.A[3][3]=0.876984862268608;
    quad->RADA_pack.A[3][4]=0;
    quad->RADA_pack.A[3][5]=0;
    quad->RADA_pack.A[3][6]=-0.0166000167523665;
    quad->RADA_pack.A[3][7]=0;
    quad->RADA_pack.A[3][8]=0.00574298512921447;
    quad->RADA_pack.A[3][9]=0;
    quad->RADA_pack.A[3][10]=0;
    quad->RADA_pack.A[3][11]=-0.000266965285180296;
    quad->RADA_pack.A[3][12]=0;

//0.000552092703578332  0   0   0   0.876984862268609   0.0166000167523659  0   0   0   
//0.00574298512921447 0.000266965285180296    0   0
    quad->RADA_pack.A[4][0]=0.000552092703578332;
    quad->RADA_pack.A[4][1]=0;
    quad->RADA_pack.A[4][2]=0;
    quad->RADA_pack.A[4][3]=0;
    quad->RADA_pack.A[4][4]=0.876984862268609;
    quad->RADA_pack.A[4][5]=0.0166000167523659;
    quad->RADA_pack.A[4][6]=0;
    quad->RADA_pack.A[4][7]=0;
    quad->RADA_pack.A[4][8]=0;
    quad->RADA_pack.A[4][9]=0.00574298512921447;
    quad->RADA_pack.A[4][10]=0.000266965285180296;
    quad->RADA_pack.A[4][11]=0;
    quad->RADA_pack.A[4][12]=0;

//-0.0167963985918934   0   0   0   0.0173369322096912  0.842475401018034   0   0   0   
//0.000667545102546464    0.00620922595435761 0   0
    quad->RADA_pack.A[5][0]=-0.0167963985918934;
    quad->RADA_pack.A[5][1]=0;
    quad->RADA_pack.A[5][2]=0;
    quad->RADA_pack.A[5][3]=0;
    quad->RADA_pack.A[5][4]=0.0173369322096912;
    quad->RADA_pack.A[5][5]=0.842475401018034;
    quad->RADA_pack.A[5][6]=0;
    quad->RADA_pack.A[5][7]=0;
    quad->RADA_pack.A[5][8]=0;
    quad->RADA_pack.A[5][9]=0.000667545102546464;
    quad->RADA_pack.A[5][10]=0.00620922595435761;
    quad->RADA_pack.A[5][11]=0;
    quad->RADA_pack.A[5][12]=0;

//0 -0.0167963985918938 0   -0.0173369322096921 0   0   0.842475401018035   0   -0.000667545102546468   
//0   0   0.00620922595435761 0
    quad->RADA_pack.A[6][0]=0;
    quad->RADA_pack.A[6][1]=-0.0167963985918938;
    quad->RADA_pack.A[6][2]=0;
    quad->RADA_pack.A[6][3]=-0.0173369322096921;
    quad->RADA_pack.A[6][4]=0;
    quad->RADA_pack.A[6][5]=0;
    quad->RADA_pack.A[6][6]=0.842475401018035;
    quad->RADA_pack.A[6][7]=0;
    quad->RADA_pack.A[6][8]=-0.000667545102546468;
    quad->RADA_pack.A[6][9]=0;
    quad->RADA_pack.A[6][10]=0;
    quad->RADA_pack.A[6][11]=0.00620922595435761;
    quad->RADA_pack.A[6][12]=0;


//0 0   -0.0212890355242736 0   0   0   0   0.898033804443649   0   0   0   0   0.00853041746011101
    quad->RADA_pack.A[7][0]=0;
    quad->RADA_pack.A[7][1]=0;
    quad->RADA_pack.A[7][2]=-0.0212890355242736;
    quad->RADA_pack.A[7][3]=0;
    quad->RADA_pack.A[7][4]=0;
    quad->RADA_pack.A[7][5]=0;
    quad->RADA_pack.A[7][6]=0;
    quad->RADA_pack.A[7][7]=0.89803380444364;
    quad->RADA_pack.A[7][8]=0;
    quad->RADA_pack.A[7][9]=0;
    quad->RADA_pack.A[7][10]=0;
    quad->RADA_pack.A[7][11]=0;
    quad->RADA_pack.A[7][12]=0.00853041746011101;


//0 -0.102720023961029  0   -2.38710021944403   0   0   -0.100412510326261  0   0.152422569425388   
//0   0   -0.0531247673738933 0
    quad->RADA_pack.A[8][0]=0;
    quad->RADA_pack.A[8][1]=-0.102720023961029;
    quad->RADA_pack.A[8][2]=0;
    quad->RADA_pack.A[8][3]=-2.38710021944403;
    quad->RADA_pack.A[8][4]=0;
    quad->RADA_pack.A[8][5]=0;
    quad->RADA_pack.A[8][6]=-0.100412510326261;
    quad->RADA_pack.A[8][7]=0;
    quad->RADA_pack.A[8][8]=0.152422569425388;
    quad->RADA_pack.A[8][9]=0;
    quad->RADA_pack.A[8][10]=0;
    quad->RADA_pack.A[8][11]=-0.0531247673738933;
    quad->RADA_pack.A[8][12]=0;

//0.102720023961027 0   0   0   -2.38710021944387   0.100412510326262   0   0   0   0.152422569425390   
//0.0531247673738932  0   0
    quad->RADA_pack.A[9][0]=0.102720023961027;
    quad->RADA_pack.A[9][1]=0;
    quad->RADA_pack.A[9][2]=0;
    quad->RADA_pack.A[9][3]=0;
    quad->RADA_pack.A[9][4]=-2.38710021944387;
    quad->RADA_pack.A[9][5]=0.100412510326262;
    quad->RADA_pack.A[9][6]=0;
    quad->RADA_pack.A[9][7]=0;
    quad->RADA_pack.A[9][8]=0;
    quad->RADA_pack.A[9][9]=0.152422569425390;
    quad->RADA_pack.A[9][10]=0.0531247673738932;
    quad->RADA_pack.A[9][11]=0;
    quad->RADA_pack.A[9][12]=0;


//-1.40940134139100 0   0   0   0.292247143416554   -2.43484171711265   0   0   0   0.131181138780295   
//0.242070868144933   0   0
    quad->RADA_pack.A[10][0]=-1.40940134139100;
    quad->RADA_pack.A[10][1]=0;
    quad->RADA_pack.A[10][2]=0;
    quad->RADA_pack.A[10][3]=0;
    quad->RADA_pack.A[10][4]=0.292247143416554;
    quad->RADA_pack.A[10][5]=-2.43484171711265;
    quad->RADA_pack.A[10][6]=0;
    quad->RADA_pack.A[10][7]=0;
    quad->RADA_pack.A[10][8]=0;
    quad->RADA_pack.A[10][9]=0.131181138780295;
    quad->RADA_pack.A[10][10]=0.242070868144933;
    quad->RADA_pack.A[10][11]=0;
    quad->RADA_pack.A[10][12]=0;

//0 -1.40940134139099   0   -0.292247143416601  0   0   -2.43484171711260   0   -0.131181138780296  
//0   0   0.242070868144933   0
    quad->RADA_pack.A[11][0]=0;
    quad->RADA_pack.A[11][1]=-1.40940134139099;
    quad->RADA_pack.A[11][2]=0;
    quad->RADA_pack.A[11][3]=-0.292247143416601;
    quad->RADA_pack.A[11][4]=0;
    quad->RADA_pack.A[11][5]=0;
    quad->RADA_pack.A[11][6]=-2.43484171711260;
    quad->RADA_pack.A[11][7]=0;
    quad->RADA_pack.A[11][8]=-0.131181138780296;
    quad->RADA_pack.A[11][9]=0;
    quad->RADA_pack.A[11][10]=0;
    quad->RADA_pack.A[11][11]=0.242070868144933;
    quad->RADA_pack.A[11][12]=0;

//0 0   -2.45695524124574   0   0   0   0   -3.88605172207263   0   0   0   0   0.709993479397112
    quad->RADA_pack.A[12][0]=0;
    quad->RADA_pack.A[12][1]=0;
    quad->RADA_pack.A[12][2]=-2.45695524124574;
    quad->RADA_pack.A[12][3]=0;
    quad->RADA_pack.A[12][4]=0;
    quad->RADA_pack.A[12][5]=0;
    quad->RADA_pack.A[12][6]=0;
    quad->RADA_pack.A[12][7]=-3.88605172207263;
    quad->RADA_pack.A[12][8]=0;
    quad->RADA_pack.A[12][9]=0;
    quad->RADA_pack.A[12][10]=0;
    quad->RADA_pack.A[12][11]=0;
    quad->RADA_pack.A[12][12]=0.709993479397112;


//-0.00998468672533316  0   0   0   0.000184775373459410    -0.0111209978368681 0   0
    quad->RADA_pack.B[0][0]=-0.00998468672533316;
    quad->RADA_pack.B[0][1]=0;
    quad->RADA_pack.B[0][2]=0;
    quad->RADA_pack.B[0][3]=0;
    quad->RADA_pack.B[0][4]=0.000184775373459410;
    quad->RADA_pack.B[0][5]=-0.0111209978368681;
    quad->RADA_pack.B[0][6]=0;
    quad->RADA_pack.B[0][7]=0;

//0 -0.00998468672533402    0   -0.000184775373459704   0   0   -0.0111209978368685 0
    quad->RADA_pack.B[1][0]=0;
    quad->RADA_pack.B[1][1]=-0.00998468672533402;
    quad->RADA_pack.B[1][2]=0;
    quad->RADA_pack.B[1][3]=-0.000184775373459704;
    quad->RADA_pack.B[1][4]=0;
    quad->RADA_pack.B[1][5]=0;
    quad->RADA_pack.B[1][6]=-0.0111209978368685;
    quad->RADA_pack.B[1][7]=0;

//0 0   -0.00997007968898963    0   0   0   0   -0.00964765723865512
    quad->RADA_pack.B[2][0]=0;
    quad->RADA_pack.B[2][1]=0;
    quad->RADA_pack.B[2][2]=-0.00997007968898963 ;
    quad->RADA_pack.B[2][3]=0;
    quad->RADA_pack.B[2][4]=0;
    quad->RADA_pack.B[2][5]=0;
    quad->RADA_pack.B[2][6]=0;
    quad->RADA_pack.B[2][7]=-0.00964765723865512;
 
 //0    0   0   -0.110045939928387  0   0   -0.0156533582874075 0  
    quad->RADA_pack.B[3][0]=0;
    quad->RADA_pack.B[3][1]=0;
    quad->RADA_pack.B[3][2]=0;
    quad->RADA_pack.B[3][3]=-0.110045939928387;
    quad->RADA_pack.B[3][4]=0;
    quad->RADA_pack.B[3][5]=0;
    quad->RADA_pack.B[3][6]=-0.0156533582874075;
    quad->RADA_pack.B[3][7]=0;

//0 0   0   0   -0.110045939928387  0.0156533582874070  0   0
    quad->RADA_pack.B[4][0]=0;
    quad->RADA_pack.B[4][1]=0;
    quad->RADA_pack.B[4][2]=0;
    quad->RADA_pack.B[4][3]=0;
    quad->RADA_pack.B[4][4]=-0.110045939928387;
    quad->RADA_pack.B[4][5]=0.0156533582874070;
    quad->RADA_pack.B[4][6]=0;
    quad->RADA_pack.B[4][7]=0;

//-0.00956550760005890  0   0   0   0.0152277345478759  -0.144587096977999  0   0
    quad->RADA_pack.B[5][0]=-0.00956550760005890;
    quad->RADA_pack.B[5][1]=0;
    quad->RADA_pack.B[5][2]=0;
    quad->RADA_pack.B[5][3]=0;
    quad->RADA_pack.B[5][4]=0.0152277345478759;
    quad->RADA_pack.B[5][5]=-0.144587096977999;
    quad->RADA_pack.B[5][6]=0;
    quad->RADA_pack.B[5][7]=0;

//0 -0.00956550760005937    0   -0.0152277345478765 0   0   -0.144587096977998  0
    quad->RADA_pack.B[6][0]=0;
    quad->RADA_pack.B[6][1]=-0.00956550760005937;
    quad->RADA_pack.B[6][2]=0;
    quad->RADA_pack.B[6][3]=-0.0152277345478765;
    quad->RADA_pack.B[6][4]=0;
    quad->RADA_pack.B[6][5]=0;
    quad->RADA_pack.B[6][6]=-0.144587096977998;
    quad->RADA_pack.B[6][7]=0;

//0 0   -0.00853891606907932    0   0   0   0   -0.0817681222802766  
    quad->RADA_pack.B[7][0]=0;
    quad->RADA_pack.B[7][1]=0;
    quad->RADA_pack.B[7][2]=-0.00853891606907932;
    quad->RADA_pack.B[7][3]=0;
    quad->RADA_pack.B[7][4]=0;
    quad->RADA_pack.B[7][5]=0;
    quad->RADA_pack.B[7][6]=0;
    quad->RADA_pack.B[7][7]=-0.0817681222802766;

//0 0.00344769605246655 0   0.195084984360599   0   0   0.0881266251151363  0
    quad->RADA_pack.B[8][0]=0;
    quad->RADA_pack.B[8][1]=0.00344769605246655;
    quad->RADA_pack.B[8][2]=0;
    quad->RADA_pack.B[8][3]=0.195084984360599;
    quad->RADA_pack.B[8][4]=0;
    quad->RADA_pack.B[8][5]=0;
    quad->RADA_pack.B[8][6]=0.0881266251151363;
    quad->RADA_pack.B[8][7]=0;

//-0.00344769605246546  0   0   0   0.195084984360579   -0.0881266251151323 0   0
    quad->RADA_pack.B[9][0]=-0.00344769605246546;
    quad->RADA_pack.B[9][1]=0;
    quad->RADA_pack.B[9][2]=0;
    quad->RADA_pack.B[9][3]=0;
    quad->RADA_pack.B[9][4]=0.195084984360579;
    quad->RADA_pack.B[9][5]=-0.0881266251151323;
    quad->RADA_pack.B[9][6]=0;
    quad->RADA_pack.B[9][7]=0;

// 0.0365765007059505   0   0   0   -0.122501349477499  0.152304109846906   0   0 
    quad->RADA_pack.B[10][0]=0.0365765007059505;
    quad->RADA_pack.B[10][1]=0;
    quad->RADA_pack.B[10][2]=0;
    quad->RADA_pack.B[10][3]=0;
    quad->RADA_pack.B[10][4]=-0.122501349477499;
    quad->RADA_pack.B[10][5]=0.152304109846906;
    quad->RADA_pack.B[10][6]=0;
    quad->RADA_pack.B[10][7]=0;

//0 0.0365765007059523  0   0.122501349477506   0   0   0.152304109846903   0  
    quad->RADA_pack.B[11][0]=0;
    quad->RADA_pack.B[11][1]=0.0365765007059523;
    quad->RADA_pack.B[11][2]=0;
    quad->RADA_pack.B[11][3]=0.122501349477506;
    quad->RADA_pack.B[11][4]=0;
    quad->RADA_pack.B[11][5]=0;
    quad->RADA_pack.B[11][6]=0.152304109846903;
    quad->RADA_pack.B[11][7]=0;

//0 0   0.0591455425532973  0   0   0   0   0.0998237185263470
    quad->RADA_pack.B[12][0]=0;
    quad->RADA_pack.B[12][1]=0;
    quad->RADA_pack.B[12][2]=0.0591455425532973;
    quad->RADA_pack.B[12][3]=0;
    quad->RADA_pack.B[12][4]=0;
    quad->RADA_pack.B[12][5]=0;
    quad->RADA_pack.B[12][6]=0;
    quad->RADA_pack.B[12][7]=0.0998237185263470;

//-360339.645898939 5795.88518202630    0   -4174.88308962975   -259559.298872039   -402309.671260679   
//6470.95230508925    0   -1676.75598911058   -104246.657827136   -195961.217632940   3151.93937290818    
//0
    quad->RADA_pack.C[0][0]=-360339.645898939;
    quad->RADA_pack.C[0][1]=5795.88518202630;
    quad->RADA_pack.C[0][2]=0;
    quad->RADA_pack.C[0][3]=-4174.88308962975 ;
    quad->RADA_pack.C[0][4]=-259559.298872039;
    quad->RADA_pack.C[0][5]=-402309.671260679;
    quad->RADA_pack.C[0][6]=6470.95230508925;
    quad->RADA_pack.C[0][7]=0;
    quad->RADA_pack.C[0][8]=-1676.75598911058;
    quad->RADA_pack.C[0][9]=-104246.657827136;
    quad->RADA_pack.C[0][10]=-195961.217632940;
    quad->RADA_pack.C[0][11]=3151.93937290818;
    quad->RADA_pack.C[0][12]=0;

//-5795.88518200600 -360339.645898935   0   259559.298872052    -4174.88308961463   -6470.95230507362   
//-402309.671260673   0   104246.657827136    -1676.75598911043   -3151.93937290803   -195961.217632939   
//0
    quad->RADA_pack.C[1][0]=-5795.88518200600;
    quad->RADA_pack.C[1][1]=-360339.645898935;
    quad->RADA_pack.C[1][2]=0;
    quad->RADA_pack.C[1][3]=259559.298872052;
    quad->RADA_pack.C[1][4]=-4174.88308961463;
    quad->RADA_pack.C[1][5]=-6470.95230507362;
    quad->RADA_pack.C[1][6]=-402309.671260673;
    quad->RADA_pack.C[1][7]=0;
    quad->RADA_pack.C[1][8]=104246.657827136;
    quad->RADA_pack.C[1][9]=-1676.75598911043;
    quad->RADA_pack.C[1][10]=-3151.93937290803;
    quad->RADA_pack.C[1][11]=-195961.217632939;
    quad->RADA_pack.C[1][12]=0;

//0 0   -86351.2870513368   0   0   0   0   -125518.348188018   0   0   0   0   -7464.69704623055
    quad->RADA_pack.C[2][0]=0;
    quad->RADA_pack.C[2][1]=0;
    quad->RADA_pack.C[2][2]=-86351.2870513368;
    quad->RADA_pack.C[2][3]=0;
    quad->RADA_pack.C[2][4]=0;
    quad->RADA_pack.C[2][5]=0;
    quad->RADA_pack.C[2][6]=0;
    quad->RADA_pack.C[2][7]=-125518.348188018;
    quad->RADA_pack.C[2][8]=0;
    quad->RADA_pack.C[2][9]=0;
    quad->RADA_pack.C[2][10]=0;
    quad->RADA_pack.C[2][11]=0;
    quad->RADA_pack.C[2][12]=-7464.69704623055;

//0 -24053.2972661013   0   -1634561.01978430   0   0   163869.129775183    0   -753220.908936544   
//0   0   -11356.7669814611   0
    quad->RADA_pack.C[3][0]=0;
    quad->RADA_pack.C[3][1]=-24053.2972661013;
    quad->RADA_pack.C[3][2]=0;
    quad->RADA_pack.C[3][3]=-1634561.01978430;
    quad->RADA_pack.C[3][4]=0;
    quad->RADA_pack.C[3][5]=0;
    quad->RADA_pack.C[3][6]=163869.129775183;
    quad->RADA_pack.C[3][7]=0;
    quad->RADA_pack.C[3][8]=-753220.908936544;
    quad->RADA_pack.C[3][9]=0;
    quad->RADA_pack.C[3][10]=0;
    quad->RADA_pack.C[3][11]=-11356.7669814611;
    quad->RADA_pack.C[3][12]=0;

//24053.2972660986  0   0   0   -1634561.01978417   -163869.129775185   0   0   0   -753220.908936542   
//11356.7669814611    0   0
    quad->RADA_pack.C[4][0]=24053.2972660986;
    quad->RADA_pack.C[4][1]=0;
    quad->RADA_pack.C[4][2]=0;
    quad->RADA_pack.C[4][3]=0;
    quad->RADA_pack.C[4][4]=-1634561.01978417;
    quad->RADA_pack.C[4][5]=-163869.129775185;
    quad->RADA_pack.C[4][6]=0;
    quad->RADA_pack.C[4][7]=0;
    quad->RADA_pack.C[4][8]=0;
    quad->RADA_pack.C[4][9]=-753220.908936542;
    quad->RADA_pack.C[4][10]=11356.7669814611;
    quad->RADA_pack.C[4][11]=0;
    quad->RADA_pack.C[4][12]=0;

//12567.5195399013  -202.142342939525   0   867.864717299047    53956.5666155281    281294.776183980    
//-4524.48750399445   0
    quad->RADA_pack.D[0][0]=12567.5195399013;
    quad->RADA_pack.D[0][1]=-202.142342939525;
    quad->RADA_pack.D[0][2]=0;
    quad->RADA_pack.D[0][3]=867.864717299047;
    quad->RADA_pack.D[0][4]=53956.5666155281;
    quad->RADA_pack.D[0][5]=281294.776183980;
    quad->RADA_pack.D[0][6]=-4524.48750399445;
    quad->RADA_pack.D[0][7]=0;

 //202.142342938264 12567.5195399019    0   -53956.5666155288   867.864717297963    4524.48750399384    
 //281294.776183974    0     
    quad->RADA_pack.D[1][0]=202.142342938264;
    quad->RADA_pack.D[1][1]=12567.5195399019;
    quad->RADA_pack.D[1][2]=0;
    quad->RADA_pack.D[1][3]=-53956.5666155288;
    quad->RADA_pack.D[1][4]=867.864717297963;
    quad->RADA_pack.D[1][5]=4524.48750399384;
    quad->RADA_pack.D[1][6]=281294.776183974;
    quad->RADA_pack.D[1][7]=0;

//0 0   2121.95004491107    0   0   0   0   14636.3328815422
    quad->RADA_pack.D[2][0]=0;
    quad->RADA_pack.D[2][1]=0;
    quad->RADA_pack.D[2][2]=2121.95004491107;
    quad->RADA_pack.D[2][3]=0;
    quad->RADA_pack.D[2][4]=0;
    quad->RADA_pack.D[2][5]=0;
    quad->RADA_pack.D[2][6]=0;
    quad->RADA_pack.D[2][7]=14636.3328815422;

//0 1863.40912226270    0   732246.848842441    0   0   230094.795112572    0
    quad->RADA_pack.D[3][0]=0;
    quad->RADA_pack.D[3][1]=1863.40912226270;
    quad->RADA_pack.D[3][2]=0;
    quad->RADA_pack.D[3][3]=732246.848842441;
    quad->RADA_pack.D[3][4]=0;
    quad->RADA_pack.D[3][5]=0;
    quad->RADA_pack.D[3][6]=230094.795112572;
    quad->RADA_pack.D[3][7]=0;

//-1863.40912226216 0   0   0   732246.848842419    -230094.795112568   0   0
    quad->RADA_pack.D[4][0]=-1863.40912226216;
    quad->RADA_pack.D[4][1]=0;
    quad->RADA_pack.D[4][2]=0;
    quad->RADA_pack.D[4][3]=0;
    quad->RADA_pack.D[4][4]=732246.848842419;
    quad->RADA_pack.D[4][5]=-230094.795112568;
    quad->RADA_pack.D[4][6]=0;
    quad->RADA_pack.D[4][7]=0;


    quad->RADA_pack.slope_x=0;
    quad->RADA_pack.slope_y=0;
    quad->RADA_pack.ref_x=0;
    quad->RADA_pack.ref_y=0;
    quad->RADA_pack.ref_phi=0;
    quad->RADA_pack.ref_theta=0;
    quad->RADA_pack.ref_psi=0;

    return 1;

}
#else
int MotorController::SSctrl_config(){
    printf("\tHello_motor stuff\n\r");

    for(int i=0; i<10; i++){
        quad->RADA_pack.x[i]=0;
        
    }
    for(int i=0; i<5; i++){
        quad->RADA_pack.u[i]=0;
        quad->RADA_pack.Y[i]=0;
    }

//0.876974537148524 0   0   -0.0164431561647720 0   0.00574261910980799 0   0   
 // -0.000265525743742696   0
    quad->RADA_pack.A[0][0]=0.876974537148524;
    quad->RADA_pack.A[0][1]=0;
    quad->RADA_pack.A[0][2]=0;
    quad->RADA_pack.A[0][3]=-0.0164431561647720;
    quad->RADA_pack.A[0][4]=0;
    quad->RADA_pack.A[0][5]=0.00574261910980799;
    quad->RADA_pack.A[0][6]=0;
    quad->RADA_pack.A[0][7]=0;
    quad->RADA_pack.A[0][8]=-0.000265525743742696 ;
    quad->RADA_pack.A[0][9]=0;

//0 0.876974537148525   0.0164431561647720  0   0   0   0.00574261910980799 0.000265525743742698    0   0
    quad->RADA_pack.A[1][0]=0;
    quad->RADA_pack.A[1][1]=0.876974537148525;
    quad->RADA_pack.A[1][2]=0.0164431561647720;
    quad->RADA_pack.A[1][3]=0;
    quad->RADA_pack.A[1][4]=0;
    quad->RADA_pack.A[1][5]=0;
    quad->RADA_pack.A[1][6]=0.00574261910980799;
    quad->RADA_pack.A[1][7]=0.000265525743742698;
    quad->RADA_pack.A[1][8]=0;
    quad->RADA_pack.A[1][9]=0;

//0 0.0173703203544094  0.843995091201138   0   0   0   0.000672530380461292    0.00622883088216277 0   0
    quad->RADA_pack.A[2][0]=0;
    quad->RADA_pack.A[2][1]=0.0173703203544094;
    quad->RADA_pack.A[2][2]=0.843995091201138;
    quad->RADA_pack.A[2][3]=0;
    quad->RADA_pack.A[2][4]=0;
    quad->RADA_pack.A[2][5]=0;
    quad->RADA_pack.A[2][6]=0.000672530380461292;
    quad->RADA_pack.A[2][7]=0.00622883088216277;
    quad->RADA_pack.A[2][8]=0;
    quad->RADA_pack.A[2][9]=0;


//-0.0173703203544096   0   0   0.843995091201140   0   -0.000672530380461296   0   0   
//0.00622883088216278 0
    quad->RADA_pack.A[3][0]=-0.0173703203544096 ;
    quad->RADA_pack.A[3][1]=0;
    quad->RADA_pack.A[3][2]=0;
    quad->RADA_pack.A[3][3]=0.843995091201140;
    quad->RADA_pack.A[3][4]=0;
    quad->RADA_pack.A[3][5]=-0.000672530380461296;
    quad->RADA_pack.A[3][6]=0;
    quad->RADA_pack.A[3][7]=0;
    quad->RADA_pack.A[3][8]=0.00622883088216278;
    quad->RADA_pack.A[3][9]=0;

//0 0   0   0   0.898299851933441   0   0   0   0   0.00855885488434011
    quad->RADA_pack.A[4][0]=0;
    quad->RADA_pack.A[4][1]=0;
    quad->RADA_pack.A[4][2]=0;
    quad->RADA_pack.A[4][3]=0;
    quad->RADA_pack.A[4][4]=0.898299851933441;
    quad->RADA_pack.A[4][5]=0;
    quad->RADA_pack.A[4][6]=0;
    quad->RADA_pack.A[4][7]=0;
    quad->RADA_pack.A[4][8]=0;
    quad->RADA_pack.A[4][9]=0.00855885488434011;

//-2.38779305059660 0   0   -0.0728044468966566 0   0.152349672058326   0   0   -0.0528380646051280 0
    quad->RADA_pack.A[5][0]=-2.38779305059660;
    quad->RADA_pack.A[5][1]=0;
    quad->RADA_pack.A[5][2]=0;
    quad->RADA_pack.A[5][3]=-0.0728044468966566;
    quad->RADA_pack.A[5][4]=0;
    quad->RADA_pack.A[5][5]=0.152349672058326;
    quad->RADA_pack.A[5][6]=0;
    quad->RADA_pack.A[5][7]=0;
    quad->RADA_pack.A[5][8]=-0.0528380646051280;
    quad->RADA_pack.A[5][9]=0;

//0 -2.38779305059644   0.0728044468966749  0   0   0   0.152349672058327   0.0528380646051283  0   0
    quad->RADA_pack.A[6][0]=0;
    quad->RADA_pack.A[6][1]=-2.38779305059644;
    quad->RADA_pack.A[6][2]=0.0728044468966749;
    quad->RADA_pack.A[6][3]=0;
    quad->RADA_pack.A[6][4]=0;
    quad->RADA_pack.A[6][5]=0;
    quad->RADA_pack.A[6][6]=0.152349672058327;
    quad->RADA_pack.A[6][7]=0.0528380646051283;
    quad->RADA_pack.A[6][8]=0;
    quad->RADA_pack.A[6][9]=0;


//0 0.302095434848247   -2.05712312336713   0   0   0   0.132178057053746   0.245991313647041   0   0
    quad->RADA_pack.A[7][0]=0;
    quad->RADA_pack.A[7][1]=0.302095434848247;
    quad->RADA_pack.A[7][2]=-2.05712312336713;
    quad->RADA_pack.A[7][3]=0;
    quad->RADA_pack.A[7][4]=0;
    quad->RADA_pack.A[7][5]=0;
    quad->RADA_pack.A[7][6]=0.132178057053746;
    quad->RADA_pack.A[7][7]=0.245991313647041;
    quad->RADA_pack.A[7][8]=0;
    quad->RADA_pack.A[7][9]=0;

//-0.302095434848286    0   0   -2.05712312336687   0   -0.132178057053747  0   0   0.245991313647044   0
    quad->RADA_pack.A[8][0]=-0.302095434848286;
    quad->RADA_pack.A[8][1]=0;
    quad->RADA_pack.A[8][2]=0;
    quad->RADA_pack.A[8][3]=-2.05712312336687;
    quad->RADA_pack.A[8][4]=0;
    quad->RADA_pack.A[8][5]=-0.132178057053747;
    quad->RADA_pack.A[8][6]=0;
    quad->RADA_pack.A[8][7]=0;
    quad->RADA_pack.A[8][8]=0.245991313647044;
    quad->RADA_pack.A[8][9]=0;

//0 0   0   0   -3.74712728558680   0   0   0   0   0.715605303321076
    quad->RADA_pack.A[9][0]=0;
    quad->RADA_pack.A[9][1]=0;
    quad->RADA_pack.A[9][2]=0;
    quad->RADA_pack.A[9][3]=0;
    quad->RADA_pack.A[9][4]=-3.74712728558680;
    quad->RADA_pack.A[9][5]=0;
    quad->RADA_pack.A[9][6]=0;
    quad->RADA_pack.A[9][7]=0;
    quad->RADA_pack.A[9][8]=0;
    quad->RADA_pack.A[9][9]=0.715605303321076;

//-0.110055126157101    0   0   -0.0156651190847230 0
    quad->RADA_pack.B[0][0]=-0.110055126157101;
    quad->RADA_pack.B[0][1]=0;
    quad->RADA_pack.B[0][2]=0;
    quad->RADA_pack.B[0][3]=-0.015665119084723;
    quad->RADA_pack.B[0][4]=0;

//0 -0.110055126157102  0.0156651190847229  0   0
    quad->RADA_pack.B[1][0]=0;
    quad->RADA_pack.B[1][1]=-0.110055126157102;
    quad->RADA_pack.B[1][2]=0.0156651190847229;
    quad->RADA_pack.B[1][3]=0;
    quad->RADA_pack.B[1][4]=0;

//0 0.0152456112247319  -0.145363608809093  0   0
    quad->RADA_pack.B[2][0]=0;
    quad->RADA_pack.B[2][1]=0.0152456112247319;
    quad->RADA_pack.B[2][2]=-0.145363608809093;
    quad->RADA_pack.B[2][3]=0;
    quad->RADA_pack.B[2][4]=0;
 
//-0.0152456112247319   0   0   -0.145363608809093  0   
    quad->RADA_pack.B[3][0]=-0.0152456112247319;
    quad->RADA_pack.B[3][1]=0;
    quad->RADA_pack.B[3][2]=0;
    quad->RADA_pack.B[3][3]=-0.145363608809093;
    quad->RADA_pack.B[3][4]=0;

//0 0   0   0   -0.0823890200934984 
    quad->RADA_pack.B[4][0]=0;
    quad->RADA_pack.B[4][1]=0;
    quad->RADA_pack.B[4][2]=0;
    quad->RADA_pack.B[4][3]=0;
    quad->RADA_pack.B[4][4]=-0.0823890200934984;

//0.194618977742138 0   0   0.0821516216362027  0
    quad->RADA_pack.B[5][0]=0.194618977742138;
    quad->RADA_pack.B[5][1]=0;
    quad->RADA_pack.B[5][2]=0;
    quad->RADA_pack.B[5][3]=0.0821516216362027;
    quad->RADA_pack.B[5][4]=0;

//0 0.194618977742118   -0.0821516216362034 0   0
    quad->RADA_pack.B[6][0]=0;
    quad->RADA_pack.B[6][1]=0.194618977742118;
    quad->RADA_pack.B[6][2]=-0.0821516216362034;
    quad->RADA_pack.B[6][3]=0;
    quad->RADA_pack.B[6][4]=0;

//0 -0.115754924364747  0.0708455637588841  0   0  
    quad->RADA_pack.B[7][0]=0;
    quad->RADA_pack.B[7][1]=-0.115754924364747;
    quad->RADA_pack.B[7][2]=0.0708455637588841;
    quad->RADA_pack.B[7][3]=0;
    quad->RADA_pack.B[7][4]=0;

//0.115754924364747 0   0   0.0708455637588414  0
    quad->RADA_pack.B[8][0]=0.115754924364747;
    quad->RADA_pack.B[8][1]=0;
    quad->RADA_pack.B[8][2]=0;
    quad->RADA_pack.B[8][3]=0.0708455637588414;
    quad->RADA_pack.B[8][4]=0;

//0 0   0   0   0.0637189107809936
    quad->RADA_pack.B[9][0]=0;
    quad->RADA_pack.B[9][1]=0;
    quad->RADA_pack.B[9][2]=0;
    quad->RADA_pack.B[9][3]=0;
    quad->RADA_pack.B[9][4]=0.0637189107809936;

//-4134.96323197541 -257077.416136449   -304851.255004439   4903.38182797309    0   -1672.61990241447   
//-103989.510563403   -194949.974214672   3135.67402212016    0
    quad->RADA_pack.C[0][0]=-4134.96323197541;
    quad->RADA_pack.C[0][1]=-257077.416136449;
    quad->RADA_pack.C[0][2]=-304851.255004439;
    quad->RADA_pack.C[0][3]=4903.38182797309 ;
    quad->RADA_pack.C[0][4]=0;
    quad->RADA_pack.C[0][5]=-1672.61990241447;
    quad->RADA_pack.C[0][6]=-103989.510563403;
    quad->RADA_pack.C[0][7]=-194949.974214672;
    quad->RADA_pack.C[0][8]=3135.67402212016;
    quad->RADA_pack.C[0][9]=0;

//257077.416136466  -4134.96323197683   -4903.38182797298   -304851.255004375   0   103989.510563403    
//-1672.61990241448   -3135.67402212016   -194949.974214672   0
    quad->RADA_pack.C[1][0]=257077.416136466;
    quad->RADA_pack.C[1][1]=-4134.96323197683;
    quad->RADA_pack.C[1][2]=-4903.38182797298;
    quad->RADA_pack.C[1][3]=-304851.255004375;
    quad->RADA_pack.C[1][4]=0;
    quad->RADA_pack.C[1][5]=103989.510563403;
    quad->RADA_pack.C[1][6]=-1672.61990241448;
    quad->RADA_pack.C[1][7]=-3135.67402212016;
    quad->RADA_pack.C[1][8]=-194949.974214672;
    quad->RADA_pack.C[1][9]=0;

//0 0   0   0   -120654.499869441   0   0   0   0   -7267.36940666850
    quad->RADA_pack.C[2][0]=0;
    quad->RADA_pack.C[2][1]=0;
    quad->RADA_pack.C[2][2]=0;
    quad->RADA_pack.C[2][3]=0;
    quad->RADA_pack.C[2][4]=-120654.499869441;
    quad->RADA_pack.C[2][5]=0;
    quad->RADA_pack.C[2][6]=0;
    quad->RADA_pack.C[2][7]=0;
    quad->RADA_pack.C[2][8]=0;
    quad->RADA_pack.C[2][9]=-7267.36940666850;

//-1634677.75905627 0   0   170342.478694559    0   -753238.500412007   0   0   -11287.5604021720   0
    quad->RADA_pack.C[3][0]=-1634677.75905627;
    quad->RADA_pack.C[3][1]=0;
    quad->RADA_pack.C[3][2]=0;
    quad->RADA_pack.C[3][3]=170342.478694559;
    quad->RADA_pack.C[3][4]=0;
    quad->RADA_pack.C[3][5]=-753238.500412007;
    quad->RADA_pack.C[3][6]=0;
    quad->RADA_pack.C[3][7]=0;
    quad->RADA_pack.C[3][8]=-11287.5604021720;
    quad->RADA_pack.C[3][9]=0;

//0 -1634677.75905612   -170342.478694552   0   0   0   -753238.500412006   11287.5604021721    0   0
    quad->RADA_pack.C[4][0]=0;
    quad->RADA_pack.C[4][1]=-1634677.75905612;
    quad->RADA_pack.C[4][2]=-170342.478694552;
    quad->RADA_pack.C[4][3]=0;
    quad->RADA_pack.C[4][4]=0;
    quad->RADA_pack.C[4][5]=0;
    quad->RADA_pack.C[4][6]=-753238.500412006;
    quad->RADA_pack.C[4][7]=11287.5604021721;
    quad->RADA_pack.C[4][8]=0;
    quad->RADA_pack.C[4][9]=0;

//894.915347732716  55638.3484807905    260313.162909890    -4187.00862024035   0    
    quad->RADA_pack.D[0][0]=894.915347732716;
    quad->RADA_pack.D[0][1]=55638.3484807905;
    quad->RADA_pack.D[0][2]=260313.162909890;
    quad->RADA_pack.D[0][3]=-4187.00862024035;
    quad->RADA_pack.D[0][4]=0;

 //-55638.3484807939    894.915347732895    4187.00862024033    260313.162909876    0      
    quad->RADA_pack.D[1][0]=-55638.3484807939;
    quad->RADA_pack.D[1][1]=894.915347732895;
    quad->RADA_pack.D[1][2]=4187.00862024033;
    quad->RADA_pack.D[1][3]=260313.162909876;
    quad->RADA_pack.D[1][4]=0;

//0 0   0   0   13345.6567377641
    quad->RADA_pack.D[2][0]=0;
    quad->RADA_pack.D[2][1]=0;
    quad->RADA_pack.D[2][2]=0;
    quad->RADA_pack.D[2][3]=0;
    quad->RADA_pack.D[2][4]=13345.6567377641;

//732184.851735822    0   0   228459.293050908    0
    quad->RADA_pack.D[3][0]=732184.851735822 ;
    quad->RADA_pack.D[3][1]=0;
    quad->RADA_pack.D[3][2]=0;
    quad->RADA_pack.D[3][3]=228459.293050908;
    quad->RADA_pack.D[3][4]=0;

//0 732184.851735805    -228459.293050904   0   0
    quad->RADA_pack.D[4][0]=0;
    quad->RADA_pack.D[4][1]=732184.851735805;
    quad->RADA_pack.D[4][2]=-228459.293050904;
    quad->RADA_pack.D[4][3]=0;
    quad->RADA_pack.D[4][4]=0;

    quad->RADA_pack.slope_x=0;
    quad->RADA_pack.slope_y=0;
    quad->RADA_pack.ref_x=0;
    quad->RADA_pack.ref_y=0;
    quad->RADA_pack.ref_phi=0;
    quad->RADA_pack.ref_theta=0;
    quad->RADA_pack.ref_psi=0;

    return 1;

}
#endif
#else

int MotorController::SSctrl_config(){
    for(int i=0; i<14; i++){
        quad->RADA_pack.x[i]=0;
        
    }
    for(int i=0; i<5; i++){
        quad->RADA_pack.u[i]=0;
        quad->RADA_pack.Y[i]=0;
    }
//0.767414022353609 0   0.000626960686625319    0   2.33454875720146    -0.0375500633953950 
//-0.102917821616014  -6.39856902554265   0   1.76927482337766    -0.0284579114385294 
//-0.0469597676516022 -2.91956543603410   0
    quad->RADA_pack.A[0][0]=0.767414022353609;
    quad->RADA_pack.A[0][1]=0;
    quad->RADA_pack.A[0][2]=0.000626960686625319;
    quad->RADA_pack.A[0][3]=0;
    quad->RADA_pack.A[0][4]=2.33454875720146;
    quad->RADA_pack.A[0][5]=-0.0375500633953950;
    quad->RADA_pack.A[0][6]=-0.102917821616014;
    quad->RADA_pack.A[0][7]=-6.39856902554265;
    quad->RADA_pack.A[0][8]=0;
    quad->RADA_pack.A[0][9]=1.76927482337766;
    quad->RADA_pack.A[0][10]=-0.0284579114385294;
    quad->RADA_pack.A[0][11]=-0.0469597676516022;
    quad->RADA_pack.A[0][12]=-2.91956543603410;
    quad->RADA_pack.A[0][13]=0;

//0  0.767414022353613 0   0.000626960686620954    0.0375500634019271  2.33454875718585    
//6.39856902549797    -0.102917821634425  0   0.0284579114435735  1.76927482336527    
//2.91956543601365    -0.0469597676599287 0
    quad->RADA_pack.A[1][0]=0;
    quad->RADA_pack.A[1][1]=0.767414022353613;
    quad->RADA_pack.A[1][2]=0;
    quad->RADA_pack.A[1][3]=0.000626960686620954;
    quad->RADA_pack.A[1][4]=0.0375500634019271;
    quad->RADA_pack.A[1][5]=2.33454875718585;
    quad->RADA_pack.A[1][6]=6.39856902549797;
    quad->RADA_pack.A[1][7]=-0.102917821634425;
    quad->RADA_pack.A[1][8]=0;
    quad->RADA_pack.A[1][9]=0.0284579114435735;
    quad->RADA_pack.A[1][10]=1.76927482336527;
    quad->RADA_pack.A[1][11]=2.91956543601365;
    quad->RADA_pack.A[1][12]=-0.0469597676599287;
    quad->RADA_pack.A[1][13]=0;

//0.000626960686625318  0   0.767414022353609   0   -2.33454875720145   0.0375500633953841  
//0.102917821616018   6.39856902554266    0   -1.76927482337766   0.0284579114385294  
//0.0469597676516022  2.91956543603410    0
    quad->RADA_pack.A[2][0]=0.000626960686625318;
    quad->RADA_pack.A[2][1]=0;
    quad->RADA_pack.A[2][2]=0.767414022353609;
    quad->RADA_pack.A[2][3]=0;
    quad->RADA_pack.A[2][4]=-2.33454875720145;
    quad->RADA_pack.A[2][5]=0.0375500633953841;
    quad->RADA_pack.A[2][6]=0.102917821616018;
    quad->RADA_pack.A[2][7]=6.39856902554266;
    quad->RADA_pack.A[2][8]=0;
    quad->RADA_pack.A[2][9]=-1.76927482337766;
    quad->RADA_pack.A[2][10]=0.0284579114385294;
    quad->RADA_pack.A[2][11]=0.0469597676516022;
    quad->RADA_pack.A[2][12]=2.91956543603410;
    quad->RADA_pack.A[2][13]=0;


//0 0.000626960686620949    0   0.767414022353613   -0.0375500634019264 -2.33454875718585   
//-6.39856902549797   0.102917821634421   0   -0.0284579114435735 -1.76927482336527   
//-2.91956543601365   0.0469597676599287  0
    quad->RADA_pack.A[3][0]=0;
    quad->RADA_pack.A[3][1]=0.000626960686620949;
    quad->RADA_pack.A[3][2]=0;
    quad->RADA_pack.A[3][3]=0.767414022353613;
    quad->RADA_pack.A[3][4]=-0.0375500634019264;
    quad->RADA_pack.A[3][5]=-2.33454875718585;
    quad->RADA_pack.A[3][6]=-6.39856902549797;
    quad->RADA_pack.A[3][7]=0.102917821634421;
    quad->RADA_pack.A[3][8]=0;
    quad->RADA_pack.A[3][9]=-0.0284579114435735;
    quad->RADA_pack.A[3][10]=-1.76927482336527;
    quad->RADA_pack.A[3][11]=-2.91956543601365;
    quad->RADA_pack.A[3][12]=0.0469597676599287;
    quad->RADA_pack.A[3][13]=0;

//0 0   0   0   0.910822590345383   0   0   -0.105395301230193  0   0.0238354469969729  0   0   
//-0.0301912694244206 0
    quad->RADA_pack.A[4][0]=0;
    quad->RADA_pack.A[4][1]=0;
    quad->RADA_pack.A[4][2]=0;
    quad->RADA_pack.A[4][3]=0;
    quad->RADA_pack.A[4][4]=0.910822590345383;
    quad->RADA_pack.A[4][5]=0;
    quad->RADA_pack.A[4][6]=0;
    quad->RADA_pack.A[4][7]=-0.105395301230193;
    quad->RADA_pack.A[4][8]=0;
    quad->RADA_pack.A[4][9]=0.0238354469969729;
    quad->RADA_pack.A[4][10]=0;
    quad->RADA_pack.A[4][11]=0;
    quad->RADA_pack.A[4][12]=-0.0301912694244206;
    quad->RADA_pack.A[4][13]=0;

//0 0 0 0   0   0.910822590345210   0.105395301229709   0   0   0   0.0238354469968382  0.0301912694241983  
//0   0
    quad->RADA_pack.A[5][0]=0;
    quad->RADA_pack.A[5][1]=0;
    quad->RADA_pack.A[5][2]=0;
    quad->RADA_pack.A[5][3]=0;
    quad->RADA_pack.A[5][4]=0;
    quad->RADA_pack.A[5][5]=0.910822590345210;
    quad->RADA_pack.A[5][6]=0.105395301229709;
    quad->RADA_pack.A[5][7]=0;
    quad->RADA_pack.A[5][8]=0;
    quad->RADA_pack.A[5][9]=0;
    quad->RADA_pack.A[5][10]=0.0238354469968382;
    quad->RADA_pack.A[5][11]=0.0301912694241983;
    quad->RADA_pack.A[5][12]=0;
    quad->RADA_pack.A[5][13]=0;

//0 0   0   0   0   0.0315468342183540  0.890522116712665   0   0   0   -0.00844399195496245   
// -0.00842500462960749    0   0
    quad->RADA_pack.A[6][0]=0;
    quad->RADA_pack.A[6][1]=0;
    quad->RADA_pack.A[6][2]=0;
    quad->RADA_pack.A[6][3]=0;
    quad->RADA_pack.A[6][4]=0;
    quad->RADA_pack.A[6][5]=0.0315468342183540;
    quad->RADA_pack.A[6][6]=0.890522116712665;
    quad->RADA_pack.A[6][7]=0;
    quad->RADA_pack.A[6][8]=0;
    quad->RADA_pack.A[6][9]=0;
    quad->RADA_pack.A[6][10]=-0.00844399195496245;
    quad->RADA_pack.A[6][11]=-0.00842500462960749 ;
    quad->RADA_pack.A[6][12]=0;
    quad->RADA_pack.A[6][13]=0;


//0 0   0   0   -0.0315468342182518 0   0   0.890522116712371   0   0.00844399195504464 0   0   
//-0.00842500462974315    0
    quad->RADA_pack.A[7][0]=0;
    quad->RADA_pack.A[7][1]=0;
    quad->RADA_pack.A[7][2]=0;
    quad->RADA_pack.A[7][3]=0;
    quad->RADA_pack.A[7][4]=-0.0315468342182518;
    quad->RADA_pack.A[7][5]=0;
    quad->RADA_pack.A[7][6]=0;
    quad->RADA_pack.A[7][7]=0.890522116712371;
    quad->RADA_pack.A[7][8]=0;
    quad->RADA_pack.A[7][9]=0.00844399195504464;
    quad->RADA_pack.A[7][10]=0;
    quad->RADA_pack.A[7][11]=0;
    quad->RADA_pack.A[7][12]=-0.00842500462974315;
    quad->RADA_pack.A[7][13]=0;


//0 0   0   0   0   0   0   0   0.898299851933440   0   0   0   0   0.00855885488434011
    quad->RADA_pack.A[8][0]=0;
    quad->RADA_pack.A[8][1]=0;
    quad->RADA_pack.A[8][2]=0;
    quad->RADA_pack.A[8][3]=0;
    quad->RADA_pack.A[8][4]=0;
    quad->RADA_pack.A[8][5]=0;
    quad->RADA_pack.A[8][6]=0;
    quad->RADA_pack.A[8][7]=0;
    quad->RADA_pack.A[8][8]=0.898299851933440 ;
    quad->RADA_pack.A[8][9]=0;
    quad->RADA_pack.A[8][10]=0;
    quad->RADA_pack.A[8][11]=0;
    quad->RADA_pack.A[8][12]=0;
    quad->RADA_pack.A[8][13]=0.00855885488434011;

//-0.00126197717672568  0   0.00126197717672569 0   2.44389506229822    0   0   -13.1933016943533  
// 0   3.75468682637633    0   0   -6.01114286344417   0
    quad->RADA_pack.A[9][0]=-0.00126197717672568;
    quad->RADA_pack.A[9][1]=0;
    quad->RADA_pack.A[9][2]=0.00126197717672569;
    quad->RADA_pack.A[9][3]=0;
    quad->RADA_pack.A[9][4]=2.44389506229822 ;
    quad->RADA_pack.A[9][5]=0;
    quad->RADA_pack.A[9][6]=0;
    quad->RADA_pack.A[9][7]=-13.1933016943533;
    quad->RADA_pack.A[9][8]=0;
    quad->RADA_pack.A[9][9]=3.75468682637633 ;
    quad->RADA_pack.A[9][10]=0;
    quad->RADA_pack.A[9][11]=0;
    quad->RADA_pack.A[9][12]=-6.01114286344417;
    quad->RADA_pack.A[9][13]=0;


//0   -0.00126197717671620    0   0.00126197717671622 0   2.44389506226436    13.1933016942571    
//0   0   0   3.75468682634952    6.01114286339991    0   0
    quad->RADA_pack.A[10][0]=0;
    quad->RADA_pack.A[10][1]=-0.00126197717671620;
    quad->RADA_pack.A[10][2]=0;
    quad->RADA_pack.A[10][3]=0.00126197717671622;
    quad->RADA_pack.A[10][4]=0;
    quad->RADA_pack.A[10][5]=2.44389506226436 ;
    quad->RADA_pack.A[10][6]=13.1933016942571;
    quad->RADA_pack.A[10][7]=0;
    quad->RADA_pack.A[10][8]=0;
    quad->RADA_pack.A[10][9]=0;
    quad->RADA_pack.A[10][10]=3.75468682634952;
    quad->RADA_pack.A[10][11]=6.01114286339991 ;
    quad->RADA_pack.A[10][12]=0;
    quad->RADA_pack.A[10][13]=0;

//0 0.000716033783941021    0   -0.000716033783941034   0   -1.51482281781616   -8.10783635306200   
//0   0   0   -1.68135029880312   -2.66860042427704   0   0
    quad->RADA_pack.A[11][0]=0;
    quad->RADA_pack.A[11][1]=0.000716033783941021;
    quad->RADA_pack.A[11][2]=0;
    quad->RADA_pack.A[11][3]=-0.000716033783941034;
    quad->RADA_pack.A[11][4]=0;
    quad->RADA_pack.A[11][5]=-1.51482281781616;
    quad->RADA_pack.A[11][6]=-8.10783635306200;
    quad->RADA_pack.A[11][7]=0;
    quad->RADA_pack.A[11][8]=0;
    quad->RADA_pack.A[11][9]=0;
    quad->RADA_pack.A[11][10]=-1.68135029880312;
    quad->RADA_pack.A[11][11]=-2.66860042427704;
    quad->RADA_pack.A[11][12]=0;
    quad->RADA_pack.A[11][13]=0;

//-0.000716033783946804 0   0.000716033783946811    0   1.51482281783679    0   0   -8.10783635312072   
//0   1.68135029881948    0   0   -2.66860042430406   0
    quad->RADA_pack.A[12][0]=-0.000716033783946804;
    quad->RADA_pack.A[12][1]=0;
    quad->RADA_pack.A[12][2]=0.000716033783946811;
    quad->RADA_pack.A[12][3]=0;
    quad->RADA_pack.A[12][4]=1.51482281783679;
    quad->RADA_pack.A[12][5]=0;
    quad->RADA_pack.A[12][6]=0;
    quad->RADA_pack.A[12][7]=-8.10783635312072;
    quad->RADA_pack.A[12][8]=0;
    quad->RADA_pack.A[12][9]=1.68135029881948;
    quad->RADA_pack.A[12][10]=0;
    quad->RADA_pack.A[12][11]=0;
    quad->RADA_pack.A[12][12]=-2.66860042430406;
    quad->RADA_pack.A[12][13]=0;

//0 0   0   0   0   0   0   0   -3.74712728558677   0   0   0   0   0.715605303321076
    quad->RADA_pack.A[13][0]=0;
    quad->RADA_pack.A[13][1]=0;
    quad->RADA_pack.A[13][2]=0;
    quad->RADA_pack.A[13][3]=0;
    quad->RADA_pack.A[13][4]=0;
    quad->RADA_pack.A[13][5]=0;
    quad->RADA_pack.A[13][6]=0;
    quad->RADA_pack.A[13][7]=0;
    quad->RADA_pack.A[13][8]=-3.74712728558677;
    quad->RADA_pack.A[13][9]=0;
    quad->RADA_pack.A[13][10]=0;
    quad->RADA_pack.A[13][11]=0;
    quad->RADA_pack.A[13][12]=0;
    quad->RADA_pack.A[13][13]=0.715605303321076;

//-0.0333962173204363 0.000537161656520478    0.0115463621450765  0.717856188743506   0
    quad->RADA_pack.B[0][0]=-0.0333962173204363;
    quad->RADA_pack.B[0][1]=0.000537161656520478 ;
    quad->RADA_pack.B[0][2]=0.0115463621450765;
    quad->RADA_pack.B[0][3]=0.717856188743506;
    quad->RADA_pack.B[0][4]=0;

//-0.000537161656551005 -0.0333962173203673 -0.717856188738484  0.0115463621471011  0
    quad->RADA_pack.B[1][0]=-0.000537161656551005;
    quad->RADA_pack.B[1][1]=-0.0333962173203673;
    quad->RADA_pack.B[1][2]=-0.717856188738484;
    quad->RADA_pack.B[1][3]=0.0115463621471011;
    quad->RADA_pack.B[1][4]=0;

//0.0333962173204502    -0.000537161656531396   -0.0115463621450719 -0.717856188743491  0
    quad->RADA_pack.B[2][0]=0.0333962173204502;
    quad->RADA_pack.B[2][1]=-0.000537161656531396 ;
    quad->RADA_pack.B[2][2]=-0.0115463621450719;
    quad->RADA_pack.B[2][3]=-0.717856188743491;
    quad->RADA_pack.B[2][4]=0;
 
 //0.000537161656551626 0.0333962173203717  0.717856188738481   -0.0115463621471052 0     
    quad->RADA_pack.B[3][0]=0.000537161656551626 ;
    quad->RADA_pack.B[3][1]=0.0333962173203717;
    quad->RADA_pack.B[3][2]=0.717856188738481;
    quad->RADA_pack.B[3][3]=-0.0115463621471052;
    quad->RADA_pack.B[3][4]=0;

//-0.100851869974954    0   0   -0.0313857724850808 0  
    quad->RADA_pack.B[4][0]=-0.100851869974954;
    quad->RADA_pack.B[4][1]=0;
    quad->RADA_pack.B[4][2]=0;
    quad->RADA_pack.B[4][3]=-0.0313857724850808;
    quad->RADA_pack.B[4][4]=0;

//0   -0.100851869974956  0.0313857724851341  0   0
    quad->RADA_pack.B[5][0]=0;
    quad->RADA_pack.B[5][1]=-0.100851869974956;
    quad->RADA_pack.B[5][2]=0.0313857724851341;
    quad->RADA_pack.B[5][3]=0;
    quad->RADA_pack.B[5][4]=0;

//0 0.0386721337601585  -0.0646085592915025 0   0
    quad->RADA_pack.B[6][0]=0;
    quad->RADA_pack.B[6][1]=0.0386721337601585;
    quad->RADA_pack.B[6][2]=-0.0646085592915025;
    quad->RADA_pack.B[6][3]=0;
    quad->RADA_pack.B[6][4]=0;

//-0.0386721337601606 0   0   -0.0646085592914673 0   
    quad->RADA_pack.B[7][0]=-0.0386721337601606;
    quad->RADA_pack.B[7][1]=0;
    quad->RADA_pack.B[7][2]=0;
    quad->RADA_pack.B[7][3]=-0.0646085592914673;
    quad->RADA_pack.B[7][4]=0;

//0   0   0   0   -0.0823890200934994
    quad->RADA_pack.B[8][0]=0;
    quad->RADA_pack.B[8][1]=0;
    quad->RADA_pack.B[8][2]=0;
    quad->RADA_pack.B[8][3]=0;
    quad->RADA_pack.B[8][4]=-0.0823890200934994;

//0.119467374724271 0   0   1.54225527415074    0 
    quad->RADA_pack.B[9][0]=0.119467374724271 ;
    quad->RADA_pack.B[9][1]=0;
    quad->RADA_pack.B[9][2]=0;
    quad->RADA_pack.B[9][3]=1.54225527415074;
    quad->RADA_pack.B[9][4]=0;

//0   0.119467374724409   -1.54225527413987   0   0   
    quad->RADA_pack.B[10][0]=0;
    quad->RADA_pack.B[10][1]=0.119467374724409;
    quad->RADA_pack.B[10][2]=-1.54225527413987;
    quad->RADA_pack.B[10][3]=0;
    quad->RADA_pack.B[10][4]=0;

//0   -0.0960290757389626 0.826132889025490   0   0    
    quad->RADA_pack.B[11][0]=0;
    quad->RADA_pack.B[11][1]=-0.0960290757389626;
    quad->RADA_pack.B[11][2]=0.826132889025490;
    quad->RADA_pack.B[11][3]=0;
    quad->RADA_pack.B[11][4]=0;

//0.0960290757388487  0   0   0.826132889032128   0
    quad->RADA_pack.B[12][0]=0.0960290757388487;
    quad->RADA_pack.B[12][1]=0;
    quad->RADA_pack.B[12][2]=0;
    quad->RADA_pack.B[12][3]=0.826132889032128;
    quad->RADA_pack.B[12][4]=0;

//0   0   0   0   0.0637189107809976    
    quad->RADA_pack.B[13][0]=0;
    quad->RADA_pack.B[13][1]=0;
    quad->RADA_pack.B[13][2]=0;
    quad->RADA_pack.B[13][3]=0;
    quad->RADA_pack.B[13][4]=0.0637189107809976;

//0   0.0900498170461407  0   -0.0900498170461415 -5.39327626397770   -335.308792010797   
//-919.017149018657   14.7819368119292    0   -4.08738502061516   -254.119401021886   
//-419.334639280524   6.74479050528167    0
    quad->RADA_pack.C[0][0]=0;
    quad->RADA_pack.C[0][1]=0.0900498170461407;
    quad->RADA_pack.C[0][2]=0;
    quad->RADA_pack.C[0][3]=-0.0900498170461415;
    quad->RADA_pack.C[0][4]=-5.39327626397770;
    quad->RADA_pack.C[0][5]=-335.308792010797;
    quad->RADA_pack.C[0][6]=-919.017149018657;
    quad->RADA_pack.C[0][7]=14.7819368119292;
    quad->RADA_pack.C[0][8]=0;
    quad->RADA_pack.C[0][9]=-4.08738502061516;
    quad->RADA_pack.C[0][10]=-254.119401021886;
    quad->RADA_pack.C[0][11]=-419.334639280524;
    quad->RADA_pack.C[0][12]=6.74479050528167;
    quad->RADA_pack.C[0][13]=0;

//-0.0900498170467682   0   0.0900498170467683  0   335.308792013042    -5.39327626304157   
//-14.7819368092853   -919.017149025071   0   254.119401023665    -4.08738501989069   
//-6.74479050408575   -419.334639283461   0
    quad->RADA_pack.C[1][0]=-0.0900498170467682;
    quad->RADA_pack.C[1][1]=0;
    quad->RADA_pack.C[1][2]=0.0900498170467683;
    quad->RADA_pack.C[1][3]=0;
    quad->RADA_pack.C[1][4]=335.308792013042;
    quad->RADA_pack.C[1][5]=-5.39327626304157;
    quad->RADA_pack.C[1][6]=-14.7819368092853 ;
    quad->RADA_pack.C[1][7]=-919.017149025071;
    quad->RADA_pack.C[1][8]=0;
    quad->RADA_pack.C[1][9]=254.119401023665;
    quad->RADA_pack.C[1][10]=-4.08738501989069;
    quad->RADA_pack.C[1][11]=-6.74479050408575;
    quad->RADA_pack.C[1][12]=-419.334639283461;
    quad->RADA_pack.C[1][13]=0;

//0 0   0   0   0   0   0   0   -120654.499869440   0   0   0   0   -7267.36940666848
    quad->RADA_pack.C[2][0]=0;
    quad->RADA_pack.C[2][1]=0;
    quad->RADA_pack.C[2][2]=0;
    quad->RADA_pack.C[2][3]=0;
    quad->RADA_pack.C[2][4]=0;
    quad->RADA_pack.C[2][5]=0;
    quad->RADA_pack.C[2][6]=0;
    quad->RADA_pack.C[2][7]=0;
    quad->RADA_pack.C[2][8]=-120654.499869440;
    quad->RADA_pack.C[2][9]=0;
    quad->RADA_pack.C[2][10]=0;
    quad->RADA_pack.C[2][11]=0;
    quad->RADA_pack.C[2][12]=0;
    quad->RADA_pack.C[2][13]=-7267.36940666848;

//-1131.06688054232 -18.1926519825560   1131.06688054233    18.1926519825535    2700795.09825737    
//0   0   -11530892.7171476   0   2486929.02630012    0   0   -5374807.04384213   0
    quad->RADA_pack.C[3][0]=-1131.06688054232;
    quad->RADA_pack.C[3][1]=-18.1926519825560;
    quad->RADA_pack.C[3][2]=1131.06688054233;
    quad->RADA_pack.C[3][3]=18.1926519825535;
    quad->RADA_pack.C[3][4]=2700795.09825737;
    quad->RADA_pack.C[3][5]=0;
    quad->RADA_pack.C[3][6]=0;
    quad->RADA_pack.C[3][7]=-11530892.7171476;
    quad->RADA_pack.C[3][8]=0;
    quad->RADA_pack.C[3][9]=2486929.02630012;
    quad->RADA_pack.C[3][10]=0;
    quad->RADA_pack.C[3][11]=0;
    quad->RADA_pack.C[3][12]=-5374807.04384213 ;
    quad->RADA_pack.C[3][13]=0;

//18.1926519791509  -1131.06688053385   -18.1926519791521   1131.06688053386    0   2700795.09822711    
//11530892.7170616    0   0   0   2486929.02627615    5374807.04380256    0   0
    quad->RADA_pack.C[4][0]=18.1926519791509 ;
    quad->RADA_pack.C[4][1]=-1131.06688053385;
    quad->RADA_pack.C[4][2]=-18.1926519791521;
    quad->RADA_pack.C[4][3]=1131.06688053386;
    quad->RADA_pack.C[4][4]=0;
    quad->RADA_pack.C[4][5]=2700795.09822711;
    quad->RADA_pack.C[4][6]=11530892.7170616;
    quad->RADA_pack.C[4][7]=0;
    quad->RADA_pack.C[4][8]=0;
    quad->RADA_pack.C[4][9]=0;
    quad->RADA_pack.C[4][10]=2486929.02627615;
    quad->RADA_pack.C[4][11]=5374807.04380256;
    quad->RADA_pack.C[4][12]=0;
    quad->RADA_pack.C[4][13]=0;

//0.0771586795528392    4.79708110040088    103.108802798754    -1.65845415327648   0      
    quad->RADA_pack.D[0][0]=0.0771586795528392;
    quad->RADA_pack.D[0][1]=4.79708110040088;
    quad->RADA_pack.D[0][2]=103.108802798754;
    quad->RADA_pack.D[0][3]=-1.65845415327648;
    quad->RADA_pack.D[0][4]=0;

//-4.79708110040732   0.0771586795463826  1.65845415298524    103.108802799478    0        
    quad->RADA_pack.D[1][0]=-4.79708110040732;
    quad->RADA_pack.D[1][1]=0.0771586795463826;
    quad->RADA_pack.D[1][2]=1.65845415298524;
    quad->RADA_pack.D[1][3]=103.108802799478;
    quad->RADA_pack.D[1][4]=0;

//0 0   0   0   13345.6567377641
    quad->RADA_pack.D[2][0]=0;
    quad->RADA_pack.D[2][1]=0;
    quad->RADA_pack.D[2][2]=0;
    quad->RADA_pack.D[2][3]=0;
    quad->RADA_pack.D[2][4]=13345.6567377641;

//622480.786016967    0   0   1668626.64021248    0
    quad->RADA_pack.D[3][0]=622480.786016967;
    quad->RADA_pack.D[3][1]=0;
    quad->RADA_pack.D[3][2]=0;
    quad->RADA_pack.D[3][3]=1668626.64021248;
    quad->RADA_pack.D[3][4]=0;

//0 622480.786017104    -1668626.64020276   0   0
    quad->RADA_pack.D[4][0]=0;
    quad->RADA_pack.D[4][1]=622480.786017104;
    quad->RADA_pack.D[4][2]=-1668626.64020276;
    quad->RADA_pack.D[4][3]=0;
    quad->RADA_pack.D[4][4]=0;

    quad->RADA_pack.slope_x=0;
    quad->RADA_pack.slope_y=0;
    quad->RADA_pack.ref_x=0;
    quad->RADA_pack.ref_y=0;
    quad->RADA_pack.ref_phi=0;
    quad->RADA_pack.ref_theta=0;
    quad->RADA_pack.ref_psi=0;

    return 1;

}
#endif


static double x_old[13];
static void stateMod_init(void);
static void stateMod_init(void)
{
  memset(&x_old[0], 0, 13U * sizeof(double));
}

void stateMod(const double u[13], double y[13])
{
  double e_a[13];
  double f_a[13];
  double g_a[13];
  double h_a[13];
  int i0;
  int i1;
  static const double i_a[169] = { -1.4279179450875197, -0.022967354697875947,
    0.0, 2.2449838303530978E-18, 0.028985648496578163, -1.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, -0.0, -0.0, 0.022967354697875933, -1.42791794508752, 0.0,
    -0.028985648496578066, -2.2310317431928235E-18, -0.0, -1.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, -0.0, 0.0, 0.0, -6.2598291907754948, 0.0, 0.0, -0.0, -0.0, -1.0,
    -0.0, -0.0, -0.0, -0.0, -0.0, -0.019309444165495907, 1.2004996742671046, 0.0,
    -2.6470748229636696, 8.3193802004714915E-19, -0.0, -0.0, -0.0, -1.0, -0.0,
    -0.0, -0.0, -0.0, -1.2004996742671046, -0.019309444165495914, 0.0,
    4.4121537313624544E-19, -2.6470748229636705, -0.0, -0.0, -0.0, -0.0, -1.0,
    -0.0, -0.0, -0.0, -2.6176248373351139, -0.042103202296656143, 0.0,
    1.9434060874553083E-18, 0.074067816682326926, -0.0, -0.0, -0.0, -0.0, -0.0,
    -1.0, -0.0, -0.0, 0.042103202296656123, -2.6176248373351148, 0.0,
    -0.074067816682326842, -5.3885830447220229E-18, -0.0, -0.0, -0.0, -0.0, -0.0,
    -0.0, -1.0, -0.0, 0.0, 0.0, -4.2454378056927222, 0.0, 0.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, -0.0, -0.0, -1.0, -0.0064205523239540393, 0.39917622213563969,
    0.0, -0.84241400859150051, 2.2036056963041833E-19, -0.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, -0.0, -0.0, -0.39917622213563969, -0.0064205523239540419, 0.0,
    1.7383312474111755E-19, -0.84241400859150062, -0.0, -0.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, -0.0, -0.75036514522507147, -0.012069252650404297, 0.0,
    6.25806511503281E-19, 0.012701585263962704, -0.0, -0.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, -0.0, 0.012069252650404295, -0.75036514522507147, 0.0,
    -0.012701585263962679, -7.0069909351569826E-19, -0.0, -0.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, -0.0, 0.0, 0.0, -0.22041808310922623, 0.0, 0.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, -0.0, -0.0, -0.0 };

  static const double j_a[169] = { 0.0, 0.0, 0.0, 0.0, 0.0, -0.0, 0.0, 0.0, 0.0,
    0.0, -0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.0, -0.0, 0.0, -0.0, 0.0,
    -0.0, -0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.0, 0.0, 0.0, 0.0, 0.0,
    -0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.0, 0.0, -0.0, 0.0, 0.0, -0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.0, 0.0, 0.0, 0.0,
    -0.048123473020783736, -0.00077404228860941985, 5.266214691683848E-36,
    6.9065176969290466E-20, 0.0020840740232880572, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.00077404228860941931, -0.048123473020783757,
    1.2325951644078309E-32, -0.0020840740232880546, -1.0185993312935393E-19, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.2325951644078309E-32,
    -1.9721522630525295E-31, -0.099649125601396549, -4.9303806576313238E-32,
    -4.997788205684877E-32, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    -0.0033231852889034769, 0.20660785585877581, 1.2325951644078309E-32,
    -0.81895629275496262, 9.2832577956843032E-20, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, -0.20660785585877589, -0.00332318528890348,
    -3.0814879110195774E-33, 3.6635280906191677E-20, -0.81895629275496284, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0771202425890596,
    -0.017324946961352759, -1.1555579666323415E-33, 3.7217083609412597E-19,
    0.25734160633413022, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.017324946961352759, -1.0771202425890596, 0.0, -0.25734160633413028,
    -1.3078488327632132E-18, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    3.8518598887744717E-33, 7.3955709864469857E-32, -0.47301445883032173,
    -4.9303806576313238E-32, 8.6666847497425613E-34, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0 };

  static const double k_a[169] = { 1.0000010000005, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0000010000005, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0000010000005, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.010001279436315999, 0.0, 0.0, 0.0, -0.00013568645763879282,
    1.0003821538750328, 0.0, 0.0, 0.0, -0.027016998531766721,
    0.076361179052481673, 0.0, 0.0, 0.0, 0.010001279436315999, 0.0,
    0.00013568645763879282, 0.0, 0.0, 1.0003821538750328, 0.0,
    0.027016998531766721, 0.0, 0.0, 0.076361179052481673, 0.0, 0.0, 0.0,
    0.010000005000001666, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    -2.7381628157929352E-7, 0.0, 0.0098656991146513166, 0.0, 0.0,
    -8.1962186134280419E-5, 0.0, 0.973260234776685, 0.0, 0.0,
    -0.016319773549878236, 0.0, 2.7381628157929352E-7, 0.0, 0.0, 0.0,
    0.0098656991146513166, 8.1962186134280419E-5, 0.0, 0.0, 0.0,
    0.973260234776685, 0.016319773549878236, 0.0, 0.0, 5.0003203651704775E-5,
    0.0, 0.0, 0.0, -4.5329625456454103E-7, 0.010001274435995633, 0.0, 0.0, 0.0,
    -0.00013568645763879279, 1.0003821538750328, 0.0, 0.0, 0.0,
    5.0003203651704775E-5, 0.0, 4.53296254564541E-7, 0.0, 0.0,
    0.010001274435995633, 0.0, 0.00013568645763879279, 0.0, 0.0,
    1.0003821538750328, 0.0, 0.0, 0.0, 4.8678464932495495E-5, 0.0, 0.0, 0.0, 0.0,
    0.0096061753068905457, 0.0, 0.0, 0.0, 0.0, 0.92228287566290712 };

  static const double l_a[169] = { 1.6668908477026121E-5, -2.6811115774220838E-7,
    0.0, -4.3636823581440507E-6, -0.00027129725768688397, 0.0050004256480365838,
    -8.0429376137417489E-5, 0.0, -0.00086917767389844709, -0.054038195271299633,
    1.0, -0.016084505959807253, 0.0, 2.6811115774220833E-7,
    1.6668908477026124E-5, 0.0, 0.00027129725768688397, -4.3636823581440507E-6,
    8.0429376137417489E-5, 0.0050004256480365838, 0.0, 0.054038195271299633,
    -0.00086917767389844709, 0.016084505959807253, 1.0, 0.0, 0.0, 0.0,
    1.7004640167116862E-5, 0.0, 0.0, 0.0, 0.0, 0.0050674120596802784, 0.0, 0.0,
    0.0, 0.0, 0.99999999999999989, 0.0, 1.0240040602172026E-5, 0.0,
    0.0050225155018034425, 0.0, 0.0, 0.0030651797220274642, 0.0, 1.0, 0.0, 0.0,
    0.6103185055510002, 0.0, -1.0240040602172026E-5, 0.0, 0.0, 0.0,
    0.0050225155018034425, -0.0030651797220274642, 0.0, 0.0, 0.0, 1.0,
    -0.6103185055510002, 0.0, 0.0, 0.009985766798586846, -5.0252034181891E-25,
    -0.0, 6.5720191066294127E-24, -4.254747123757786E-5, 0.0098125923021980463,
    -5.5522573639898291E-23, -0.0, 4.4945143843840125E-23,
    -0.0012375170409293112, 0.012831067422080886, -1.1740238540503592E-21, -0.0,
    4.9598969927717308E-25, 0.009985766798586846, -0.0, 4.2547471237577888E-5,
    8.2633174212128852E-24, 5.6081960492561632E-23, 0.0098125923021980446, -0.0,
    0.0012375170409293112, 6.3599313783714674E-23, 1.1799969602291048E-21,
    0.012831067422080886, -0.0, -0.0, -0.0, 0.0099713492510500358, -0.0, -0.0,
    -0.0, -0.0, 0.0088446976021852274, -0.0, -0.0, -0.0, -0.0,
    0.0012009260486285753, 1.3279967991173147E-23, 0.00018971655189886855, -0.0,
    0.11410309395822119, 9.94771348554951E-24, 7.8209486355561068E-22,
    0.016704588268445016, -0.0, 0.61270370431956322, 9.3550012748390268E-22,
    1.3029955950773522E-20, 0.17066152375645427, -0.0, -0.00018971655189886852,
    1.4780690388763832E-23, -0.0, -9.2360983536660059E-24, 0.11410309395822119,
    -0.016704588268445016, 7.8195329535114286E-22, -0.0, -9.36864989194961E-22,
    0.61270370431956322, -0.17066152375645427, 1.3029850839756369E-20, -0.0,
    0.011141588940962475, 4.8459518423950066E-23, -0.0, 7.0739985213108587E-22,
    -0.017238155867450897, 0.15076334841588973, 8.3036352661498084E-22, -0.0,
    5.4573359159419844E-21, -0.22743567416253266, 1.0821551443305792,
    8.3015020401437922E-20, -0.0, -4.79586976801861E-23, 0.011141588940962475,
    -0.0, 0.017238155867450894, 7.0725382122852982E-22, -8.3042410158793042E-22,
    0.15076334841588973, -0.0, 0.22743567416253266, 5.456786901033988E-21,
    -8.3015153126936416E-20, 1.0821551443305792, -0.0, -0.0, -0.0,
    0.0096547160115937527, -0.0, -0.0, -0.0, -0.0, 0.083877405589500742, -0.0,
    -0.0, -0.0, -0.0, 0.31642066529312662 };

  for (i0 = 0; i0 < 13; i0++) {
    e_a[i0] = 0.0;
    for (i1 = 0; i1 < 13; i1++) {
      e_a[i0] += i_a[i0 + 13 * i1] * x_old[i1];
    }

    f_a[i0] = 0.0;
    for (i1 = 0; i1 < 13; i1++) {
      f_a[i0] += j_a[i0 + 13 * i1] * u[i1];
    }

    y[i0] = e_a[i0] + f_a[i0];
    g_a[i0] = 0.0;
    for (i1 = 0; i1 < 13; i1++) {
      g_a[i0] += k_a[i0 + 13 * i1] * x_old[i1];
    }

    h_a[i0] = 0.0;
    for (i1 = 0; i1 < 13; i1++) {
      h_a[i0] += l_a[i0 + 13 * i1] * u[i1];
    }
  }

  for (i0 = 0; i0 < 13; i0++) {
    x_old[i0] = g_a[i0] + h_a[i0];
  }
}

void stateMod_initialize(void)
{
  stateMod_init();
}

void stateMod_terminate(void)
{
}
