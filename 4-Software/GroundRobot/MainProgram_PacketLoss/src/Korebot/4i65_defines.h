#ifndef _KOREBOT__4I65_DEFINES_H_
#define _KOREBOT__4I65_DEFINES_H_

/* Assorted #defines.
*/
#define PROGNAME      "SC5I20L"
#define SIGNONMESSAGE "\n"PROGNAME" version "PROGVERSION"\n"
#define PROGVERSION   "0.2"

/* I/O registers.
*/
#define R_5I20CONTROLNSTATUS 0x0054 /* 32-bit control/status register. */

/* PCI base register indices.
*/
#define BRNUM_5I20LCLCFGIO 1 /* Local configuration I/O base address register number. */
#define BRNUM_5I20IO16     2 /* 16-bit I/O window to FPGA. (Base address register 2.) */
#define BRNUM_5I20IO32     3 /* 32-bit I/O window to FPGA. (Base address register 3.) */
#define BRNUM_5I20CFGMEM   4 /* Configuration memory window. */

/* Masks for R_5I20CONTROLNSTATUS.
*/
#define P_5I20CFGPRGM         P_FPGAIOControlnStatus32
#define B_5I20CFGPRGM         26 /* Program enable. (Output.) */
#define M_5I20CFGPRGM         (0x1 << B_5I20CFGPRGM)
#define M_5I20CFGPRGMDEASSERT (0x0 << B_5I20CFGPRGM) /* Reset the FPGA. */
#define M_5I20CFGPRGMASSERT   (0x1 << B_5I20CFGPRGM) /* Begin programming. */

#define P_5I20CFGRW           P_FPGAIOControlnStatus32
#define B_5I20CFGRW           23 /* Data direction control. (Output.) */
#define M_5I20CFGRW           (0x1 << B_5I20CFGRW)
#define M_5I20CFGWRITEENABLE  (0x0 << B_5I20CFGRW) /* From CPU. */
#define M_5I20CFGWRITEDISABLE (0x1 << B_5I20CFGRW) /* To CPU. */

#define P_5I20LEDONOFF        P_FPGAIOControlnStatus32
#define B_5I20LEDONOFF        17 /* Red LED control. (Output.) */
#define M_5I20LEDONOFF        (0x1 << B_5I20LEDONOFF)
#define M_5I20LEDON           (0x0 << B_5I20LEDONOFF)
#define M_5I20LEDOFF          (0x1 << B_5I20LEDONOFF)

#define P_5I20PRGMDUN         P_FPGAIOControlnStatus32
#define B_5I20PRGMDUN         11 /* Programming-done flag. (Input.) */
#define M_5I20PRGMDUN         (0x1 << B_5I20PRGMDUN)
#define M_5I20PRGMDUNNOW      (0x1 << B_5I20PRGMDUN)

/* Paths.
*/
#define PATH_PROC_BUS_PCI "/proc/bus/pci"

/* Card I.D.
*/
#define VENDORID5I20 0x10B5 /* PCI vendor I.D. */
#define DEVICEID5I20 0x9054 /* PCI device I.D. */

/* Exit codes.
*/
#define EC_OK    0   /* Exit OK. */
#define EC_BADCL 100 /* Bad command line. */
#define EC_USER  101 /* Invalid input. */
#define EC_HDW   102 /* Some sort of hardware failure on the 5I20. */
#define EC_FILE  103 /* File error of some sort. */
#define EC_SYS   104 /* Beyond our scope. */
#define EC_NTRNL 105 /* Internal error. */


#endif  // _KOREBOT__4I65_DEFINES_H_
