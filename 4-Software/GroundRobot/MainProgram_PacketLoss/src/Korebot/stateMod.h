#ifndef __STATEMOD_H__
#define __STATEMOD_H__
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
//#include "rtwtypes.h"
//#include "stateMod_types.h"

extern void stateMod(const double u[13], double y[13]);
extern void stateMod_initialize(void);
extern void stateMod_terminate(void);

#endif
