#include "sensor_list.h"

#include <cstring>

#include "Network/network_bitstream.h"

/*
 * Creates a sensor list packet with no sensors
 */
SensorList::SensorList() : SensorControl(SENSOR_CONTROL_SENSOR_LIST_DATA) {
	num_sensors = 0;
}

/*
 * Creates a sensor list with the given data
 *
 * @param num_sensors The number of sensors in the packet
 * @param sensors Information about the sensors
 */
SensorList::SensorList(unsigned char num_sensors, sensor_info_t *sensors) : SensorControl(SENSOR_CONTROL_SENSOR_LIST_DATA) {

	if (num_sensors > MAX_SENSORS_IN_PACKET) {
		num_sensors = MAX_SENSORS_IN_PACKET;
	}

	this->num_sensors = num_sensors;
	memcpy(this->sensors, sensors, sizeof(struct sensor_info_t)*num_sensors);
}

/*
 * Creates a copy of a SensorList packet
 *
 * @param copy The packet to copy
 */
SensorList::SensorList(SensorList *copy) : SensorControl(SENSOR_CONTROL_SENSOR_LIST_DATA) {
	this->num_sensors = copy->num_sensors;
	memcpy(this->sensors, copy->sensors, sizeof(struct sensor_info_t) * MAX_SENSORS_IN_PACKET);
}

/*
 * Frees any resources used by this packet
 */
SensorList::~SensorList() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *SensorList::read(NetworkBitstream &in) {
	unsigned char i;

	in>>num_sensors;

	for (i=0; i<num_sensors; i++) {
		in>>sensors[i].id;
		in>>sensors[i].name;
		in>>sensors[i].port;
	}

	return new SensorList(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void SensorList::write(NetworkBitstream &out) const {
	unsigned char i;
	SensorControl::write(out);
	out<<num_sensors;

	for (i=0; i<num_sensors; i++) {
		out<<sensors[i].id;
		out<<((const char *)sensors[i].name);
		out<<sensors[i].port;
	}
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the number of available sensors.
 *
 * @returns The number of sensors on the korebot
 */
unsigned char SensorList::getNumSensors() {
	return num_sensors;
}

/*
 * Gets the sensor information for the given sensor
 * 
 * @parm index The index of the sensor
 * @param out Where to store the data
 *
 * @retuns 1 on success, 0 otherwise
 */
bool SensorList::getSensorData(unsigned int index, struct sensor_info_t *out) {
	if (index < 0 || index >= num_sensors) {
		return 0;
	}

	memcpy(out, &sensors[index], sizeof(struct sensor_info_t));

	return 1;
}
