#ifndef _NETWORK__PACKETS__CONFIGURATION_PACKET_H_
#define _NETWORK__PACKETS__CONFIGURATION_PACKET_H_

#include "base_packet.h"

class ConfigurationPacket : public BasePacket {
	private:
		char option;
		int value;

	public:
		/*
		 * Creates a configuration packet by default
		 */
		ConfigurationPacket();

		/*
		 * Creates a configuration packet by default
		 */
		ConfigurationPacket(char option, int value);

		/*
		 * Creates a copy of a ConfigurationPacket packet
		 *
		 * @param copy The packet to copy
		 */
		ConfigurationPacket(ConfigurationPacket *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~ConfigurationPacket();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 *
		 * @returns A pointer to a base packet filled with
		 * 			the information read from the bitstream.
		 * 			The base packet should be freed by the
		 * 			calling program.
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the option of the configuration packet
		 */
		char getOption() const;

		/*
		 * Sets the option of configuration packet
		 */
		void setOption(char newOption);

		/*
		 * Gets the value of the configuration packet
		 */
		int getValue() const;

		/*
		 * Sets the value of configuration packet
		 */
		void setValue(int newValue);
};

#endif	// _NETWORK__PACKETS__CONFIGURATION_PACKET_H_
