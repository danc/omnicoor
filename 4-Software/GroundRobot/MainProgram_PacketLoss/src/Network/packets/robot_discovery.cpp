#include "robot_discovery.h"

#include "Network/network_bitstream.h"

/*
 * Creates an invalid robot discovery packet
 */
RobotDiscovery::RobotDiscovery() {
	this->robot_id = 0xFE;
}

/*
 * Creates a robot discovery packet with the given parameters
 */
RobotDiscovery::RobotDiscovery(unsigned char robot_id, struct sockaddr_in address) : BasePacket(OPCODE_ROBOT_DISCOVERY) {
	this->robot_id = robot_id;
	addr = address;
}

/*
 * Creates a copy of the given robot identification
 *
 * @param copy The packet to copy
 */
RobotDiscovery::RobotDiscovery(RobotDiscovery *copy) : BasePacket(OPCODE_ROBOT_DISCOVERY) {
	this->robot_id = copy->robot_id;
	this->addr = copy->addr;
}

/*
 * Frees any resources used by this packet
 */
RobotDiscovery::~RobotDiscovery() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 */
BasePacket *RobotDiscovery::read(NetworkBitstream &in) { 
	in>>robot_id;
	in>>addr.sin_family;
	in>>addr.sin_port;
	in>>addr.sin_addr.s_addr;

	return new RobotDiscovery(this);

}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void RobotDiscovery::write(NetworkBitstream &out) const {
	BasePacket::write(out);
	out<<robot_id;
	out<<addr.sin_family;
	out<<addr.sin_port;
	out<<addr.sin_addr.s_addr;
}

/**********************************************
 * 			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the robot ID
 */
unsigned char RobotDiscovery::getRobotID() const {
	return robot_id;
}

/*
 * Gets the robot address
 */
struct sockaddr_in RobotDiscovery::getRobotAddr() const {
	return addr;
}

/*
 * Sets the robot ID
 */
void RobotDiscovery::setRobotID(unsigned char newRobotID) {
	robot_id = newRobotID;
}
