#include "udp_connection.h"

// Standard Library Files
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <errno.h>

// Networking files
#include <unistd.h>
//#include <sys/socket.h>
//#include <sys/types.h>
//#include <fcntl.h>

#include "packet_handler.h"
#include "packets/base_packet.h"
#include "packets/localization_packet.h"
#include "packets/robot_discovery.h"

void *udpListenThreadProxy(void *data);

/*
 * Creates a new udp connection and initializes the values
 * in the structure to default values.
 */
UDP_Connection::UDP_Connection() {
	init(-1, 1, NULL);
}

/*
 * Creates a new object that represents a UDP connection,
 * which passes received packets to the given packet handler.
 *
 * @param usePacketHandlerThread 1 if we should use a thread to
 *			handle incoming packets, or 0 if the creator will
 *			call run() to handle packets themelves
 * @param handler The object to handle received packets
 */
UDP_Connection::UDP_Connection(int usePacketHandlerThread, PacketHandler *handler) {
	init(-1, usePacketHandlerThread, handler);
}

/*
 * Creates a new object that represents a UDP connection,
 * which passes received packets to the given packet handler.
 *
 * @param robot_id The ID of the robot associated with this connection, if available
 * @param usePacketHandlerThread 1 if we should use a thread to
 *			handle incoming packets, or 0 if the creator will
 *			call run() to handle packets themelves
 * @param handler The object to handle received packets
 */
UDP_Connection::UDP_Connection(unsigned char robot_id, int usePacketHandlerThread, PacketHandler *handler) {
	init(robot_id, usePacketHandlerThread, handler);
}

/*
 * Destroys the UDP connection
 */
UDP_Connection::~UDP_Connection() {
	struct known_korebot_list *connection_entry;
	struct known_korebot_list *temp;

	stop();

	for (connection_entry = SLIST_FIRST(&connections); connection_entry != NULL; ) {
			temp = connection_entry;
			connection_entry = SLIST_NEXT(connection_entry, known_korebots);

			SLIST_REMOVE(&connections, temp, known_korebot_list, known_korebots);

			free(temp);
	}
}

/*
 * Initializes the values for the class to default values.
 *
 * @param robotID The ID of the robot associated with this connection, if available
 * @param usePacketHandlerThread 1 if we should use a thread to
 *			handle incoming packets, or 0 if the creator will
 *			call run() to handle packets themelves
 * @param handler The object that will handle received packets
 */
void UDP_Connection::init(unsigned char robotID, int usePacketHandlerThread, PacketHandler *handler) {
	struct known_korebot_list *broadcast;

	// We are not listening by default
	we_are_listening = 0;

	// Non-Valid socket
	sockfd = -1;

	// By default listen on localhost
	strcpy(broadcast_config.ip_addr, "127.0.0.1");

	// Just choose a random port in the upper ranges
	broadcast_config.port = 54321;

	// Shouldn't try to shutdown
	shutdown_ready = 0;

	// Connection handler thread is not running
	connection_handler_running = 0;

	// Invalid handle
	connection_thread = 0;

	pthread_mutex_init(&bitstreamMutex, NULL);

	// This must be specified by someone else
	packetHandler = handler;

	use_packet_handler_thread = usePacketHandlerThread;

	connection_handler_is_starting = 0;

	robot_id = robotID;

	SLIST_INIT(&connections);

	broadcast = (struct known_korebot_list *)malloc(sizeof(struct known_korebot_list));
	memset(broadcast, 0, sizeof(struct known_korebot_list));
	broadcast->robot_id                = KOREBOT_ID_BROADCAST;
	broadcast->address.sin_family      = AF_INET;
	broadcast->address.sin_port        = htons(broadcast_config.port);
	broadcast->address.sin_addr.s_addr = INADDR_BROADCAST;
	SLIST_INSERT_HEAD(&connections, broadcast, known_korebots);


}

/*
 * Starts the connection.  The connection attempts to listen on the given address and port
 *					 specified in the config parameter.
 *
 * @param config The network configuration
 *
 * @returns 1 on success, <= 0 otherwise
 */
int UDP_Connection::start(const network_config_t &broadcast_config) {
	struct known_korebot_list *connection_entry;
	struct sockaddr_in host;
	int broadcast = 1;

	memset(&host, 0, sizeof(struct sockaddr_in));
	memcpy(&this->broadcast_config, &broadcast_config, sizeof(network_config_t));

	// Set the appropriate port for our connection
	for (connection_entry = SLIST_FIRST(&connections); connection_entry != NULL; ) {
		if (connection_entry->robot_id == KOREBOT_ID_BROADCAST) {
			connection_entry->address.sin_port = htons(broadcast_config.port);
			inet_aton(broadcast_config.ip_addr, (struct in_addr *) &(connection_entry->address.sin_addr.s_addr));
			break;
		}

		connection_entry = SLIST_NEXT(connection_entry, known_korebots);
	}

	// Setup the connection to listen for UDP packets
	host.sin_family = AF_INET;
	host.sin_addr.s_addr = INADDR_ANY;
	host.sin_port = htons(broadcast_config.port);

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("UDP_Connection::start:Error: Could not initialize socket");
		return -2;
	}

	if(setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) == -1) {
        perror( "UDP_Connection::start:Error: Could not set broadcast mode");
		destroySocket();
        return -3;
    }

	if (bind(sockfd, (struct sockaddr *)&host, sizeof(host)) != 0) {
		perror("UDP_Connection::start:Error: Could not bind to port");
		destroySocket();
		return -3;
	}

	we_are_listening = 1;
	shutdown_ready = 0;

	if (use_packet_handler_thread) {
		if (!launchListenThread()) {
			return -4;
		}
	}


	return 1;
}

/*
 * Attempts to stop an active connection.  Closes any active
 * sockets and stops listening on the port.
 *
 * @returns void
 */
void UDP_Connection::stop() {
	shutdown_ready = 1;

	if (connection_thread != 0) {
		// Wait for the socket thread to finish
		if (pthread_join(connection_thread, NULL) != 0) {
			perror("UDP_Connection::stop:Error: Could not re-join connection handler thread");
		}

		connection_thread = 0;
	}

	destroySocket();
	we_are_listening = 0;
}

/*
 * Destroys the socket
 */
void UDP_Connection::destroySocket() {
	if (sockfd != -1) {
#		ifdef DEBUG
			printf("Closing connection listening on %s:%d\n", config.ip_addr, config.port);
#		endif
		close(sockfd);
		sockfd = -1;
	}
}

/*
 * Listens for incoming packets, and passes them to the packet
 * processor
 */
void UDP_Connection::listenThread() {

	connection_handler_running = 1;
	connection_handler_is_starting = 0;

	// If the caller decides not to use a seperate thread for the packet handler
	// after the thread has been created, we test for that here and exit the thread
	// if they want to.
	while (!shutdown_ready && use_packet_handler_thread) {
		if (run() < 0) {
			break;
		}
	}

	connection_handler_running = 0;

}

/*
 * Checks for any incoming packets, and sends them to
 * the packet handler.  This should not be called
 * if use_packet_handler_thread is true.
 *
 * @returns The number of packets received, or -1 if there is a problem with the socket.
 * 			If this function returns -1, the connection is no longer
 * 			valid and the connection should be stopped.
 */
int UDP_Connection::run() {
	BasePacket base;
	BasePacket *base_data = NULL;
	RobotDiscovery *discovery;
	char recv_buffer[MAX_NETWORK_PACKET_SIZE];
	struct known_korebot_list *connection_entry;
	unsigned short bytes = 0;
	int received = 0;
	struct sockaddr_in source;
	socklen_t source_len;
	int num_packets = 0;

	unsigned short *size;

	// Point the header to the start of the recv buffer
	size = (unsigned short *)&recv_buffer[0];

	if (isListening()) {
		// Loop over all pending packets
		while (1) {
			source_len = sizeof(struct sockaddr_in);
			// Get the header from the network, but don't block
			// Only peek at the data, since this is a UDP socket & we need to grab the entire packet in one
			// shot
			received = recvfrom(sockfd, recv_buffer, sizeof(unsigned short), MSG_PEEK | MSG_DONTWAIT, (struct sockaddr *)&source, &source_len);

			if (received == -1) {
				// errno is defined in errno.h
				if (errno != EAGAIN) {
					// Error occured, shut the application down.
					perror("UDP_Connection::run:Error: Bad data");
					return -1;
				}

				// No more packets, return success
				return num_packets;
			} else if (received == 0) {
				// Connection closed gracefully
				return -1;
			} else {
#			ifdef DEBUG
					printf("UDP_Connection::Received Packet...\n");
#			endif
				num_packets++;

				// +2 => backwards compatability for the localization system
				bytes = ntohs(*size)+2;				

				// Get the rest of the packet
				received = recvfrom(sockfd, recv_buffer, bytes, MSG_DONTWAIT, NULL, 0); 					

				if (pthread_mutex_lock(&bitstreamMutex) != 0) {
					perror("UDP_Connection::run:Error: Could not lock mutex");
					break;
				}

					if (received == bytes) {
						// This is backwards compatability for the localization system
						// I feel like I will be sodomized by the end of the semester
						// by doing this, but it's better to remain backwards compatible
						// with the rest of the (badly-designed) localization system then
						// to have to go through every program and change it
						LocalizationPacket packet;

						// We have a full packet..
						bitstream.setData(recv_buffer, bytes);

						try {
							base_data = packet.read(bitstream);
						} catch (const char *) {
							fprintf(stderr, "UDP_Connection::run: Premature end of stream while reading packet information\n");

							if (pthread_mutex_unlock(&bitstreamMutex) != 0) {
								perror("UDP_Connection::run:Error: Could not unlock mutex");
								break;
							}
							return num_packets;
						}
					} else if (received == bytes - 2) { 									
						// We have a full packet..
						bitstream.setData(recv_buffer, bytes-2);							

						// Data has been read from the stream, now form
						// a packet from it
						try {
							base_data = base.read(bitstream);
						} catch (const char *) {
							fprintf(stderr, "UDP_Connection::run: Premature end of stream while reading packet information\n");

							if (pthread_mutex_unlock(&bitstreamMutex) != 0) {
								perror("UDP_Connection::run:Error: Could not unlock mutex");
								break;
							}
							return num_packets;
						}
					}

				if (pthread_mutex_unlock(&bitstreamMutex) != 0) {
					perror("UDP_Connection::run:Error: Could not unlock mutex");
					break;
				}

				// This is intentionally done outside of the mutex; If it were
				// done inside the lock, and the packet handler wanted to send
				// a reply packet, then it would cause a deadlock in the transmit
				// method
				if (received == bytes - 2) {											
					if (base_data->getOpcode() == OPCODE_ROBOT_DISCOVERY) {
						discovery = (RobotDiscovery *)base_data;
						
						/*
						printf("\n\n**********************  UDP  *********************\n\n");
						printf("size = %u\n", base_data->getSize());
						printf("opcode = %u\n", base_data->getOpcode());
						printf("robot_id = %u\n", discovery->getRobotID());
						printf("sin_family = %d\n", discovery->getRobotAddr().sin_family);
						printf("sin_port = %d\n", discovery->getRobotAddr().sin_port);
						printf("s_addr = %s\n", inet_ntoa(*(struct in_addr *)&discovery->getRobotAddr().sin_addr.s_addr));
						printf("\n\n**************************************************\n\n");
						*/

						if (discovery->getRobotID() != robot_id) {
							for (connection_entry = SLIST_FIRST(&connections); connection_entry != NULL; ) {
								if (connection_entry->robot_id == discovery->getRobotID()) {
									break;
								}

								connection_entry = SLIST_NEXT(connection_entry, known_korebots);
							}

							if (connection_entry == NULL) {
								printf("Discovered new Korebot: (%d)\n", discovery->getRobotID());
								connection_entry = (struct known_korebot_list *)malloc(sizeof(struct known_korebot_list));
								memset(connection_entry, 0, sizeof(struct known_korebot_list));
								//memcpy(&connection_entry->address, &source, sizeof(source_len));

								connection_entry->robot_id = discovery->getRobotID();
								connection_entry->address = discovery->getRobotAddr();

								//printf("UDP::Run discovery = %s\n",inet_ntoa(*(struct in_addr *)&discovery->getRobotAddr().sin_addr.s_addr) );
								//printf("UDP::Run\n\tip: %s\n\n", inet_ntoa(*(struct in_addr *)&connection_entry->address.sin_addr.s_addr) );

								SLIST_INSERT_HEAD(&connections, connection_entry, known_korebots);

								// Let the new Korebot know about us
								sendRobotDiscovery();
							}
						}

					} else if (packetHandler != NULL) {
						packetHandler->processPacket(this, base_data);
					}

					delete base_data;
					base_data = NULL;
				} else if (received == bytes) {
					if (packetHandler != NULL) {
						packetHandler->processPacket(this, base_data);
					}

					delete base_data;
					base_data = NULL;
				}
			}
		}
		return num_packets;
	}

	return -1;
}

/*
 * Sends a packet to the given korebot
 *
 * @param robot_id The robot to send to, or KOREBOT_ID_BROADCAST for broadcast
 * @param data     The data to send across the network
 *
 * @returns 1 on success, 0 otherwise
 */
int UDP_Connection::transmit(unsigned char robot_id, BasePacket *data) {
	struct known_korebot_list *robot_info;

	// Make sure we're actually connected to something
	if (isListening()) {
		// Look for the IP address of the korebot
		for (robot_info = SLIST_FIRST(&connections); robot_info != NULL; ) {
			if (robot_info->robot_id == robot_id) {
				break;
			}

			robot_info = SLIST_NEXT(robot_info, known_korebots);
		}

		// Could not find the requested robot
		if (robot_info == NULL) {
			return 0;
		}

		if (pthread_mutex_lock(&bitstreamMutex) != 0) {
			perror("UDP_Connection:transmit:Error: Could not lock mutex");
			return -1;
		}
			bitstream.reset();
			data->write(bitstream);
			data->finalize(bitstream);

			sendto(sockfd, bitstream.getData(), bitstream.getLength(), 0, (struct sockaddr *)&robot_info->address, sizeof(robot_info->address));


		/* OUR CRAP 

		printf("TRANSMIT\n");
		
		for (robot_info = SLIST_FIRST(&connections); robot_info != NULL; ) {
			
			printf("\tid : %d\n" , robot_info->robot_id);
			printf("\tip: %s\n\n", inet_ntoa(*(struct in_addr *)&robot_info->address.sin_addr.s_addr) );

			robot_info = SLIST_NEXT(robot_info, known_korebots);
		}

		printf("\n\n");

		 ENDS HERE */



		if (pthread_mutex_unlock(&bitstreamMutex) != 0) {
			perror("UDP_Connection:transmite:Error: Could not unlock mutex");
			return -1;
		}
	}

	return 1;
}

/*
 * Broadcasts a packet to all Korebots
 *
 * @param data     The data to send across the network
 *
 * @returns 1 on success, 0 otherwise
 */
int UDP_Connection::broadcast(BasePacket *data) {
	return transmit(KOREBOT_ID_BROADCAST, data);
}


/*
 * Sends a robot discovery packet across the network
 */
void UDP_Connection::sendRobotDiscovery() {	
	struct sockaddr_in my_addr;
	memset(&my_addr, 0, sizeof(struct sockaddr_in));

	my_addr.sin_family      = AF_INET;
	my_addr.sin_port = htons(peer_config.port);
	inet_aton(peer_config.ip_addr, (struct in_addr *) &(my_addr.sin_addr.s_addr));

	//printf("UDP::sendRobotDiscovery\n\tip: %s\n" , peer_config.ip_addr);
	//printf("\tid: %d\n" , robot_id);

	RobotDiscovery discover(robot_id , my_addr);
	broadcast(&discover);
}

/*
 * Launches the packet listener thread
 */
int UDP_Connection::launchListenThread() {
	if (connection_thread == 0) {
		connection_handler_is_starting = 1;
		if (pthread_create(&connection_thread, NULL, &udpListenThreadProxy, this) != 0) {
			perror("UDP_Connection:launchConnectionThread:Error: Could not create UDP connection thread");
			return 0;
		}

		while (connection_handler_is_starting == 1) { 
			sleep(1);
		}
	}

	return 1;
}

/*
 * Returns 1 if the connection is active and listening for incoming
 *			connections.  Returns 0 otherwise
 *
 * @returns (See description)
 */
int UDP_Connection::isListening() {
	return we_are_listening;
}

/*
 * Gets the currently active network configuration.
 *
 * @param config The network configuration will be copied into
 *			this parameter
 *
 * @returns void
 */
void UDP_Connection::getConfiguration(network_config_t &config) {
	memcpy(&peer_config, &config, sizeof(network_config_t));
}

/*
 * Gets the ID of the robot this connection is connected to
 *
 * @returns The ID of the robot
 */
unsigned char UDP_Connection::getRobotID() {
	return robot_id;
}

/*
 * Sets the ID of the robot this connection is connected to
 *
 * @param newRobotID The new robot ID
 */
void UDP_Connection::setRobotID(unsigned char newRobotID) {
	robot_id = newRobotID;
}

/*
 * Gets the object that handles received packets
 *
 * @returns The object that handles received packets
 */
PacketHandler *UDP_Connection::getPacketHandler() {
	return packetHandler;
}

/*
 * Sets the object that handles received packets
 *
 * @param newHandler The new object to handle received packets
 */
void UDP_Connection::setPacketHandler(PacketHandler *newHandler) {
	packetHandler = newHandler;
}

/*
 * Gets if this connection uses a separate thread for processing
 * incoming packets, or if the creator calls a method to
 * do the processing.
 *
 * @returns 1, if the connection uses a thread, 0 otherwise
 */
int UDP_Connection::getUsePacketHandlerThread() {
	return use_packet_handler_thread;
}

/*
 * Sets if this connection should use a packet handler thread or
 * if some other part of the program will tell the connection when
 * to listen for packets.  If the connection was already using a
 * thread to process packets, and this method is called with a value
 * of 0, the thread will be stopped before this method returns.
 *
 * If the thread had not been created, it will be created.
 *
 * @param usePacketHandlerThread 1 if we should use a thread to
 *			handle incoming packets, or 0 if the creator will
 *			call run() to handle packets themelves
 */
void UDP_Connection::setUsePacketHandlerThread(int usePacketHandlerThread) {
	if (use_packet_handler_thread && !usePacketHandlerThread) {
		// We were using a thread, and we shouldn't.  Set the new value, 
		// and join the thread with this one if it exists.
		use_packet_handler_thread = usePacketHandlerThread;

		if (connection_thread != 0) {
			// Wait for the socket thread to finish
			if (pthread_join(connection_thread, NULL) != 0) {
				perror("UDP_Connection::setUsePacketHandlerThread:Error: Could not re-join connection handler thread");
			}

			connection_thread = 0;
		}

	} else if (!use_packet_handler_thread && usePacketHandlerThread) {
		// We were not using a thread, and we want to now.  Start a new
		// thread if a connection is active.

		use_packet_handler_thread = usePacketHandlerThread;

		if (we_are_listening) {
			launchListenThread();
		}
	}
}

void *udpListenThreadProxy(void *data) {
	if (data != NULL) {
		((UDP_Connection *)data)->listenThread();
	}

	return NULL;
}
