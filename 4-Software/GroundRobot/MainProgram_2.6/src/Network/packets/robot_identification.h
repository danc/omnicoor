#ifndef _NETWORK__PACKETS__ROBOT_IDENTIFICATION_H_
#define _NETWORK__PACKETS__ROBOT_IDENTIFICATION_H_

#include "base_packet.h"

#define MAX_ROBOT_NAME_LEN	16

class RobotIdentification : public BasePacket {
	private:
		unsigned char robot_id;

		char robot_name[MAX_ROBOT_NAME_LEN];

	public:
		/*
		 * Creates a manual motor control by default
		 */
		RobotIdentification();

		/*
		 * Creates a manual motor control by default
		 *
		 * @param robot_id   The ID of the robot
		 * @param robot_name The name of the robot
		 */
		RobotIdentification(unsigned char robot_id, const char *robot_name);

		/*
		 * Creates a copy of the given robot identification
		 *
		 * @param copy The packet to copy
		 */
		RobotIdentification(RobotIdentification *copy);

		/*
		 * Frees any resources used by this packet
		 */
		virtual ~RobotIdentification();

		/*
		 * Reads information for this packet in
		 * from the network
		 *
		 * @param in The bitstream to read from
		 */
		virtual BasePacket *read(NetworkBitstream &in);

		/*
		 * Writes the information for this packet
		 * to the network bitstream
		 *
		 * @param out The bitstream to write to
		 */
		virtual void write(NetworkBitstream &out) const;

		/**********************************************
		 * 			GETTERS / SETTERS				  *
		 *********************************************/

		/*
		 * Gets the ID of the robot
		 */
		unsigned char getRobotID() const;

		/*
		 * Sets the ID of the robot
		 */
		void setRobotID(unsigned char newRobotID);

		/*
		 * Gets the name of the robot
		 */
		const char *getRobotName() const;

		/*
		 * Sets the name of the robot
		 * 
		 * @param newRobotName The new name of the robot
		 */
		void setRobotName(const char *newRobotName);
};

#endif	// _NETWORK__PACKETS__ROBOT_IDENTIFICATION_H_
