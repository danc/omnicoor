#include "sensor_control.h"

#include <cstdio>

#include "Network/network_bitstream.h"
#include "sensor_data.h"
#include "sensor_list.h"
#include "sensor_request.h"

/*
 * Creates a invalid sensor control by default
 */
SensorControl::SensorControl() : BasePacket(OPCODE_SENSOR) {
	this->command = SENSOR_CONTROL_COMMAND_INVALID;
}

/*
 * Creates a manual sensor control by default
 */
SensorControl::SensorControl(char command) : BasePacket(OPCODE_SENSOR) {
	this->command = command;
}

/*
 * Creates a copy of a SensorControl packet
 *
 * @param copy The packet to copy
 */
SensorControl::SensorControl(SensorControl *copy) : BasePacket(OPCODE_SENSOR) {
	this->command = copy->command;
}

/*
 * Frees any resources used by this packet
 */
SensorControl::~SensorControl() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *SensorControl::read(NetworkBitstream &in) {
	SensorList    list;
	SensorRequest req;
	SensorData    data;

	in>>command;

	switch(command) {
		case SENSOR_CONTROL_SENSOR_LIST_DATA:
			return list.read(in);
		case SENSOR_CONTROL_SENSOR_REQUEST:
			return req.read(in);
		case SENSOR_CONTROL_SENSOR_DATA:
			return data.read(in);
		case SENSOR_CONTROL_SENSOR_LIST_REQUEST:
			return new SensorControl(this);
		default:
			fprintf(stderr, "SensorControl::read: Unknown command (%d)\n", command);
			return new SensorControl((char)SENSOR_CONTROL_COMMAND_INVALID);
	}
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void SensorControl::write(NetworkBitstream &out) const {
	BasePacket::write(out);
	out<<command;
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the command of the sensor control
 */
char SensorControl::getCommand() const {
	return command;
}

/*
 * Sets the command of sensor control
 */
void SensorControl::setCommand(char newCommand) {
	command = newCommand;
}

