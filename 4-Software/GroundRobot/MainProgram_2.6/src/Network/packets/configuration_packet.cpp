#include "configuration_packet.h"

#include "Network/network_bitstream.h"

/**
 * Creates a manual motor control by default
 */
ConfigurationPacket::ConfigurationPacket() : BasePacket(OPCODE_CONFIGURATION) {
	this->option = CONFIGURATION_OPTION_INVALID;
	this->value = 0;
}

/*
 * Creates a manual motor control by default
 */
ConfigurationPacket::ConfigurationPacket(char option, int value) : BasePacket(OPCODE_CONFIGURATION) {
	this->option = option;
	this->value = value;
}

/*
 * Creates a copy of a ConfigurationPacket packet
 *
 * @param copy The packet to copy
 */
ConfigurationPacket::ConfigurationPacket(ConfigurationPacket *copy) : BasePacket(OPCODE_CONFIGURATION) {
	this->option = copy->option;
	this->value = copy->value;
}

/*
 * Frees any resources used by this packet
 */
ConfigurationPacket::~ConfigurationPacket() {
}

/*
 * Reads information for this packet in
 * from the network
 *
 * @param in The bitstream to read from
 *
 * @returns A pointer to a base packet filled with
 *			the information read from the bitstream.
 *			The base packet should be freed by the
 *			calling program.
 */
BasePacket *ConfigurationPacket::read(NetworkBitstream &in) {
	in>>option;
	in>>value;

	return new ConfigurationPacket(this);
}

/*
 * Writes the information for this packet
 * to the network bitstream
 *
 * @param out The bitstream to write to
 */
void ConfigurationPacket::write(NetworkBitstream &out) const {
	BasePacket::write(out);
	out<<option;
	out<<value;
}

/**********************************************
 *			GETTERS / SETTERS				  *
 *********************************************/

/*
 * Gets the option of the motor control
 */
char ConfigurationPacket::getOption() const {
	return option;
}

/*
 * Sets the option of motor control
 */
void ConfigurationPacket::setOption(char newOption) {
	option = newOption;
}

/*
 * Gets the value of the configuration packet
 */
int ConfigurationPacket::getValue() const {
	return value;
}

/*
 * Sets the value of configuration packet
 */
void ConfigurationPacket::setValue(int newValue) {
	value = newValue;
}
