#include "tcp_connection.h"

// Standard Library Files
#include <cstdio>
#include <cstring>
#include <errno.h>

// Networking files
#include <unistd.h>
//#include <sys/socket.h>
//#include <sys/types.h>
#include <arpa/inet.h>
//#include <fcntl.h>

#include "packet_handler.h"
#include "packets/base_packet.h"

/*
 * Proxy function to launch the connection handler thread
 */
void *tcpConnectionThreadProxy(void *data);

/*
 * Creates a new object that represents a TCP connection.
 */
TCP_Connection::TCP_Connection() {
	init(1, NULL);
}

/*
 * Creates a new object that represents a TCP connection,
 * which passes received packets to the given packet handler.
 *
 * @param usePacketHandlerThread 1 if we should use a thread to
 * 			handle incoming packets, or 0 if the creator will
 * 			call run() to handle packets themelves
 * @param handler The object to handler received packets
 */
TCP_Connection::TCP_Connection(int usePacketHandlerThread, PacketHandler *handler) {
	init(usePacketHandlerThread, handler);
}

/*
 * Initializes the object with default values.
 *
 * @param usePacketHandlerThread 1 if we should use a thread to
 * 			handle incoming packets, or 0 if the creator will
 * 			call run() to handle packets themelves
 * @param handler The object that will handle received packets
 */
void TCP_Connection::init(int usePacketHandlerThread, PacketHandler *handler) {
	// Non-Valid socket
	sockfd = -1;

	// We aren't connected to the server yet
	we_are_connected = 0;

	// By default listen on localhost
	strcpy(config.ip_addr, "127.0.0.1");

	// Just choose a random port in the upper ranges
	config.port = 54321;

	// Shouldn't try to shutdown
	shutdown_ready = 0;

	// Connection handler thread is not running
	connection_handler_running = 0;

	// Invalid handle
	connection_thread = 0;

	pthread_mutex_init(&bitstreamMutex, NULL);

	// This must be specified by someone else
	packetHandler = handler;

	robot_id = KOREBOT_ID_BROADCAST;

	use_packet_handler_thread = usePacketHandlerThread;

	connection_handler_is_starting = 0;
}
/*
 * Stops the active connection and frees any memory this
 * object is using.
 */
TCP_Connection::~TCP_Connection() {
	stop();

	pthread_mutex_destroy(&bitstreamMutex);
}

/*
 * Attempts to connect to the server on the port given in the config
 * structure passed in.  If successful, a thread is spawned to handle
 * the connection.
 *
 * @param config The network configuration
 *
 * @returns 1 on success, <= 0 otherwise
 */
int TCP_Connection::start(const network_config_t &config) {
	struct sockaddr_in host;

	if (we_are_connected) {
		// If we are currently listening for connections, stop listening first
		stop();
	}

	memset(&host, 0, sizeof(struct sockaddr_in));
	memcpy(&this->config, &config, sizeof(network_config_t));

	// Setup parameters for socket
	host.sin_family = AF_INET;
	host.sin_port = htons(config.port);

	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
		perror("TCP_Connection::start:Error: Could not initialize socket");
		return -1;
	}

	// Set the server IP address
	if (inet_aton(config.ip_addr, (struct in_addr *) &(host.sin_addr.s_addr)) == 0) {
		fprintf(stderr, "TCP_Connection::start:Error: Could not initialize socket: Invalid Server Address\n");
		destroySocket();
		return -2;
	}

	// Try to connect
	if (connect(sockfd, (struct sockaddr *)&host, sizeof(struct sockaddr_in)) == -1) {
		perror("TCP_Connection:start:Error: Could not connect to server");
		destroySocket();
		return -3;
	}

	we_are_connected = 1;
	shutdown_ready = 0;

	if (use_packet_handler_thread) {
		if (!launchConnectionThread()) {
			return -4;
		}
	}

	return 1;
}

/*
 * Attempts to close an active connection
 *
 * @returns void
 */
void TCP_Connection::stop() {
	shutdown_ready = 1;

	if (connection_thread != 0) {
		// Wait for the socket thread to finish
		if (pthread_join(connection_thread, NULL) != 0) {
			perror("TCP_Connection::stop:Error: Could not re-join connection handler thread");
		}

		connection_thread = 0;
	}

	destroySocket();
	we_are_connected = 0;
}

/*
 * Destroys the socket
 */
void TCP_Connection::destroySocket() {
	if (sockfd != -1) {
#		ifdef DEBUG
			printf("Closing connection to %s:%d\n", config.ip_addr, config.port);
#		endif
		// shutdown(sockfd, SHUT_RDWR);
		close(sockfd);
		sockfd = -1;
	}
}

/*
 * Manages a TCP connection with the server.
 * Receives packets & passes them to the packet handler.
 */
void TCP_Connection::connectionThread() {

	connection_handler_running = 1;
	connection_handler_is_starting = 0;

	// If the caller decides not to use a seperate thread for the packet handler
	// after the thread has been created, we test for that here and exit the thread
	// if they want to.
	while (!shutdown_ready && use_packet_handler_thread) {
		if (run() < 0) {
			break;
		}
	}

	connection_handler_running = 0;

}

/*
 * Checks for any incoming packets, and sends them to
 * the packet handler.  This should not be called
 * if use_use_packet_handler_thread is true.
 *
 * @returns The number of packets received, or -1 if there is a problem with the socket.
 * 			If this function returns -1, the connection is no longer
 * 			valid and the connection should be stopped.
 */
int TCP_Connection::run() {
	BasePacket base;
	BasePacket *base_data = NULL;
	char recv_buffer[MAX_NETWORK_PACKET_SIZE];
	unsigned short bytes = 0;
	int received = 0;
	int num_packets = 0;

	unsigned short *size;

	// Point the header to the start of the recv buffer
	size = (unsigned short *)&recv_buffer[0];

	// Loop over all pending packets
	while (1) {
		// Get the header from the network, but don't block
		received = recv(sockfd, recv_buffer, sizeof(unsigned short), MSG_DONTWAIT);

		if (received == -1) {
			// errno is defined in errno.h
			if (errno != EAGAIN) {
				// Error occured, shut the application down.
				perror("TCP_Connection::run:Error: Bad data");
				return -1;
			}

			// No more packets, return success
			return num_packets;
		} else if (received == 0) {
			// Connection closed gracefully
			return -1;
		} else {
#			ifdef DEBUG
				printf("TCP_Connection::Received Packet...\n");
#			endif

			num_packets++;

			bytes = ntohs(*size);

			// Get the rest of the packet
			while (received < bytes) {
				received += recv(sockfd, (char *)(recv_buffer + received), bytes - received, 0);
			}

			if (pthread_mutex_lock(&bitstreamMutex) != 0) {
				perror("TCP_Connection::run:Error: Could not lock mutex");
				break;
			}

				if (received == bytes && packetHandler != NULL) {
					// We have a full packet..
					bitstream.setData(recv_buffer, bytes);

					// Data has been read from the stream, now form
					// a packet from it
					try {
						base_data = base.read(bitstream);
					} catch (const char *) {
						fprintf(stderr, "TCP_Connection::run: Premature end of stream while reading packet information\n");

						if (pthread_mutex_unlock(&bitstreamMutex) != 0) {
							perror("TCP_Connection::run:Error: Could not unlock mutex");
							break;
						}
						return num_packets;
					}
				}

			if (pthread_mutex_unlock(&bitstreamMutex) != 0) {
				perror("TCP_Connection::run:Error: Could not unlock mutex");
				break;
			}

			// This is intentionally done outside of the mutex; If it were
			// done inside the lock, and the packet handler wanted to send
			// a reply packet, then it would cause a deadlock in the transmit
			// method
			if (received == bytes && packetHandler != NULL) {
				packetHandler->processPacket(this, base_data);
				delete base_data;
				base_data = NULL;
			}
		}
	}

	return num_packets;
}

/*
 * Sends a packet to the connected client
 *
 * @param packet The packet to send
 *
 * @returns 1 on success, 0 otherwise
 */
int TCP_Connection::transmit(BasePacket *packet) {
	int result = 0;

	// Make sure we're actually connected to something
	if (isConnected()) {
		if (pthread_mutex_lock(&bitstreamMutex) != 0) {
			perror("TCP_Connection:transmit:Error: Could not lock mutex");
			return -1;
		}
			bitstream.reset();
			packet->write(bitstream);
			packet->finalize(bitstream);

			result = send(sockfd, bitstream.getData(), bitstream.getLength(), 0);

		if (pthread_mutex_unlock(&bitstreamMutex) != 0) {
			perror("TCP_Connection:transmite:Error: Could not unlock mutex");
			return -1;
		}
	}

	return result;
}

/*
 * Launches the connection thread.  Used by the tcp_server
 * when a client connects
 */
int TCP_Connection::launchConnectionThread() {
	if (connection_thread == 0) {
		connection_handler_is_starting = 1;
		if (pthread_create(&connection_thread, NULL, &tcpConnectionThreadProxy, this) != 0) {
			perror("TCP_Connection:launchConnectionThread:Error: Could not create TCP connection thread");
			return 0;
		}

		while (connection_handler_is_starting == 1) { 
			sleep(1);
		}
	}

	return 1;
}

/*
 * Returns 1 if the client is connected to the server and processing
 *				packets.  Returns 0 otherwise
 *
 * @returns (See description)
 */
int TCP_Connection::isConnected() {
	return we_are_connected && (!use_packet_handler_thread || connection_handler_running);
}

/*
 * Gets the object that handles received packets
 *
 * @returns The object that handles received packets
 */
PacketHandler *TCP_Connection::getPacketHandler() {
	return packetHandler;
}

/*
 * Sets the object that handles received packets
 *
 * @param newHandler The new object to handle received packets
 */
void TCP_Connection::setPacketHandler(PacketHandler *newHandler) {
	packetHandler = newHandler;
}

/*
 * Gets the ID of the robot this connection is connected to
 *
 * @returns The ID of the robot
 */
unsigned char TCP_Connection::getRobotID() {
	return robot_id;
}

/*
 * Sets the ID of the robot this connection is connected to
 *
 * @param newRobotID The new robot ID
 */
void TCP_Connection::setRobotID(unsigned char newRobotID) {
	robot_id = newRobotID;
}

/*
 * Gets the address of the machine we are connected to.
 *
 * @param config The network configuration will be copied into
 *			this parameter
 *
 * @returns void
 */
void TCP_Connection::getConfiguration(network_config_t &config) {
	memcpy(&config, &this->config, sizeof(network_config_t));
}

/*
 * Gets if this connection uses a separate thread for processing
 * incoming packets, or if the creator calls a method to
 * do the processing.
 *
 * @returns 1, if the connection uses a thread, 0 otherwise
 */
int TCP_Connection::getUsePacketHandlerThread() {
	return use_packet_handler_thread;
}

/*
 * Sets if this connection should use a packet handler thread or
 * if some other part of the program will tell the connection when
 * to listen for packets.  If the connection was already using a
 * thread to process packets, and this method is called with a value
 * of 0, the thread will be stopped before this method returns.
 *
 * If the thread had not been created, it will be created.
 *
 * @param usePacketHandlerThread 1 if we should use a thread to
 * 			handle incoming packets, or 0 if the creator will
 * 			call run() to handle packets themelves
 */
void TCP_Connection::setUsePacketHandlerThread(int usePacketHandlerThread) {
	if (use_packet_handler_thread && !usePacketHandlerThread) {
		// We were using a thread, and we shouldn't.  Set the new value, 
		// and join the thread with this one if it exists.
		use_packet_handler_thread = usePacketHandlerThread;

		if (connection_thread != 0) {
			// Wait for the socket thread to finish
			if (pthread_join(connection_thread, NULL) != 0) {
				fprintf(stderr, "TCP_Connection::setUsePacketHandlerThread:Error: Could not re-join connection handler thread");
			}

			connection_thread = 0;
		}

	} else if (!use_packet_handler_thread && usePacketHandlerThread) {
		// We were not using a thread, and we want to now.  Start a new
		// thread if a connection is active.

		use_packet_handler_thread = usePacketHandlerThread;

		if (we_are_connected) {
			launchConnectionThread();
		}
	}
}


/*
 * Proxy function to launch the connection handler thread
 */
void *tcpConnectionThreadProxy(void *data) {
	if (data != NULL) {
		((TCP_Connection *)data)->connectionThread();
	}

	return NULL;
}
