#ifndef _NETWORK__UDP_SERVER_H_
#define _NETWORK__UDP_SERVER_H_

#include <pthread.h>
#include <sys/queue.h>
#include <arpa/inet.h>

#include "network.h"
#include "network_bitstream.h"

class BasePacket;
class PacketHandler;

class UDP_Connection {
	private:
		typedef struct known_korebot_list {
			SLIST_ENTRY(known_korebot_list) known_korebots;

			unsigned char robot_id;

			struct sockaddr_in address;
		} HI;

		// The configuration of the network
		network_config_t peer_config;
		network_config_t broadcast_config;

		// 1 if we are listening on the network, 0 otherwise
		int we_are_listening;

		// The file descriptor of the socket we create
		int sockfd;

		// 1 when the connection should shut itself down.  0 otherwise.
		int shutdown_ready;

		// 1 if the connection handler thread is running, 0 otherwise
		int connection_handler_running;

		// Store the UDP connection thread handler somewhere
		pthread_t connection_thread;

		// Handles storing of network data for the program to read in
		NetworkBitstream bitstream;

		// Mutex to control a lock on the bitstream
		pthread_mutex_t bitstreamMutex;

		// The object that handles incoming packets
		PacketHandler *packetHandler;

		// 1 if we should use a thread to handle incoming packets, 0 if
		// the creator will call a method to handle packets themselves
		int use_packet_handler_thread;

		// 1 when we are first launching the connection handler; Once
		// the handler thread has started, this becomes false
		int connection_handler_is_starting;

		unsigned char robot_id;

		// A list of known korebots & their address
		SLIST_HEAD(listhead, known_korebot_list) connections;

	public:
		/*
		 * Creates a new udp connection and initializes the values
		 * in the structure to default values.
		 */
		UDP_Connection();

		/*
		 * Creates a new object that represents a UDP connection,
		 * which passes received packets to the given packet handler.
		 *
		 * @param usePacketHandlerThread 1 if we should use a thread to
		 * 			handle incoming packets, or 0 if the creator will
		 * 			call run() to handle packets themelves
		 * @param handler The object to handle received packets
		 */
		UDP_Connection(int usePacketHandlerThread, PacketHandler *handler);

		/*
		 * Creates a new object that represents a UDP connection,
		 * which passes received packets to the given packet handler.
		 *
		 * @param robot_id The ID of the robot associated with this connection, if available
		 * @param usePacketHandlerThread 1 if we should use a thread to
		 * 			handle incoming packets, or 0 if the creator will
		 * 			call run() to handle packets themelves
		 * @param handler The object to handle received packets
		 */
		UDP_Connection(unsigned char robot_id, int usePacketHandlerThread, PacketHandler *handler);

		/*
		 * Destroys the UDP connection
		 */
		~UDP_Connection();

		/*
		 * Starts the connection.  The connection attempts to listen on the given address and port
		 *					 specified in the config parameter.
		 *
		 * @param config The network configuration
		 *
		 * @returns 1 on success, <= 0 otherwise
		 */
		int start(const network_config_t &config);

		/*
		 * Attempts to stop an active connection.  Closes any active
		 * sockets and stops listening on the port.
		 *
		 * @returns void
		 */
		void stop();

		/*
		 * Sends a packet to the given korebot
		 *
		 * @param robot_id The robot to send to, or KOREBOT_ID_BROADCAST for broadcast
		 * @param data     The data to send across the network
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int transmit(unsigned char robot_id, BasePacket *data);

		/*
		 * Broadcasts a packet to all Korebots
		 *
		 * @param data     The data to send across the network
		 *
		 * @returns 1 on success, 0 otherwise
		 */
		int broadcast(BasePacket *data);

		/*
		 * Sends a robot discovery packet across the network
		 */
		void sendRobotDiscovery();

		/*
		 * Checks for any incoming packets, and sends them to
		 * the packet handler.  This should not be called
		 * if use_packet_handler_thread is true.
		 *
		 * @returns The number of packets received, or -1 if there is a problem with the socket.
		 * 			If this function returns -1, the connection is no longer
		 * 			valid and the connection should be stopped.
		 */
		int run();

		/////////////////////////////////////////////////////
		//                GETTERS / SETTERS                //
		/////////////////////////////////////////////////////

		/*
		 * Returns 1 if the connection is active and listening for incoming
		 *			connections.  Returns 0 otherwise
		 *
		 * @returns (See description)
		 */
		int isListening();

		/*
		 * Gets the currently active network configuration.
		 *
		 * @param config The network configuration will be copied into
		 *			this parameter
		 *
		 * @returns void
		 */
		void getConfiguration(network_config_t &config);

		/*
		 * Gets the ID of the robot this connection is connected to
		 *
		 * @returns The ID of the robot
		 */
		unsigned char getRobotID();

		/*
		 * Sets the ID of the robot this connection is connected to
		 *
		 * @param newRobotID The new robot ID
		 */
		void setRobotID(unsigned char newRobotID);

		/*
		 * Gets the object that handles received packets
		 *
		 * @returns The object that handles received packets
		 */
		PacketHandler *getPacketHandler();

		/*
		 * Sets the object that handles received packets
		 *
		 * @param newHandler The new object to handle received packets
		 */
		void setPacketHandler(PacketHandler *newHandler);

		/*
		 * Gets if this connection uses a separate thread for processing
		 * incoming packets, or if the creator calls a method to
		 * do the processing.
		 *
		 * @returns 1, if the connection uses a thread, 0 otherwise
		 */
		int getUsePacketHandlerThread();

		/*
		 * Sets if this connection should use a packet handler thread or
		 * if some other part of the program will tell the connection when
		 * to listen for packets.  If the connection was already using a
		 * thread to process packets, and this method is called with a value
		 * of 0, the thread will be stopped before this method returns.
		 *
		 * If the thread had not been created, it will be created.
		 *
		 * @param usePacketHandlerThread 1 if we should use a thread to
		 * 			handle incoming packets, or 0 if the creator will
		 * 			call run() to handle packets themelves
		 */
		void setUsePacketHandlerThread(int usePacketHandlerThread);

	private:
		/*
		 * Initializes the values for the class to default values.
		 *
		 * @param robotID The ID of the robot associated with this connection, if available
		 * @param usePacketHandlerThread 1 if we should use a thread to
		 * 			handle incoming packets, or 0 if the creator will
		 * 			call run() to handle packets themelves
		 * @param handler The object that will handle received packets
		 */
		void init(unsigned char robotID, int usePacketHandlerThread, PacketHandler *handler);

		/*
		 * Destroys the socket
		 */
		void destroySocket();

		/*
		 * Listens for incoming packets, and passes them to the packet
		 * processor
		 */
		void listenThread();

		/*
		 * Launches the packet listener thread
		 */
		int launchListenThread();

	friend void *udpListenThreadProxy(void *data);
};

#endif	// _NETWORK__UDP_SERVER_H_
