#ifndef _NETWORK__PACKET_HANDLER_H_
#define _NETWORK__PACKET_HANDLER_H_

class BasePacket;
class TCP_Connection;
class UDP_Connection;

class PacketHandler {
	public:
		virtual ~PacketHandler() {}

		/*
		 * When a packet is received on the network, this method is called to
		 * process the packet.
		 *
		 * @param con    The connection the packet was received on
		 * @param packet The packet that was received from the network
		 */
		virtual void processPacket(TCP_Connection *con, BasePacket *packet)=0;

		/*
		 * When a packet is received on the network, this method is called to
		 * process the packet.
		 *
		 * @param con    The connection the packet was received on
		 * @param packet The packet that was received from the network
		 */
		virtual void processPacket(UDP_Connection *con, BasePacket *packet)=0;
};

#endif	// _NETWORK__PACKET_HANDLER_H_
