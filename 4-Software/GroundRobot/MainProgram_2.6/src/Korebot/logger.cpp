
#include "logger.h"

#include <iomanip>
#include <iostream>
#include <string>
#include <sys/stat.h>

#include "utils.h"

using namespace std;
extern struct motor_control_manual_t groundRobotMotors;
// TODO: this needs to move somewhere better
// This is used to clear the terminal
const string CLEAR = "\033[H\033[J";

/* TODO: list
         Clean up the initialize function
         Add getter/setter functions for booleans
*/


// Default value for singleton pointer
Logger *Logger::_instance = 0;

Logger::Logger()
{
    isInitialized = false;
    logPitch = true;
    logRoll = true;
	logEris = true;
    logLocal = true;
    logMotors = true;
    logVelCtrl = false;
    logPID = true;
    logSSI = false;
    logSS = false;
    logSSIF = false;
    logSSIw = false;
    quad = MotorController::instance()->quad;
	eris = MotorController::instance()->eris;
    motors =&MotorController::instance()->motors;

}

Logger::~Logger()
{

}

bool Logger::initialize()
{
    time_t curTime;             // Time
    struct tm *ts;              // Time struct
    char dirName[50];          // Directory name
    char fName[50];            // File name
    stringstream logFilename;   // Log file path

    // Get the current time
    curTime = time(0);

    // Put the time into a tm struct
    ts = localtime(&curTime);

    // Format the dirName to look like: "2011-4-15_DAY" based on current date
    strftime(dirName, sizeof(dirName), "%Y-%m-%d_%a/", ts);

    // Format the fName to look like: "12.34.10" based on current time
    strftime(fName, sizeof(fName), "log(%H.%M.%S).txt", ts);

    // Create the default log file directory
    //logFilename << "./Logs/";
    logFilename << "/root/Logs/";
    mkdir(logFilename.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    // Create the log file directory for this day
    logFilename << dirName;
    mkdir(logFilename.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    // Create and open the log file for this run
    logFilename << fName;
    logFile.open(logFilename.str().c_str(), fstream::out);

    // Print the header information for this log file
    writeHeader();

    // Set the width of the data being printed out
    logFile.width(1);

    // return value primarily used for debugging purposes
    if (logFile.is_open())
    {
        isInitialized = true;
        return true;
    }

    cerr << "Failure to create and open logfile" << endl;
    return false;
}

void Logger::writeHeader()
{
    /*
     * The first thing to be printed in the log file is relevant information regarding the
     * PID constants being used for this run.
     */
    logFile << "#Constants\t\tpitch_P\t\tpitch_I\t\tpitch_D" << endl;
    logFile << "#PIDValues\t\t" << quad->pitch.p
            << "\t\t" << quad->pitch.i
            << "\t\t" << quad->pitch.d << endl;
    logFile << "#Constants\t\troll_P\t\troll_I\t\troll_D" << endl;
    logFile << "#PIDValues\t\t" << quad->roll.p
            << "\t\t" << quad->roll.i
            << "\t\t" << quad->roll.d << endl;
    logFile << "#Battery\t\tVquad\t\tVwheels" << endl;
    logFile << "#Voltage\t\t\t\t" << endl;

    /*
     * Log file column titles.
     * Always log the time, marker, and motor thrust of all 4 motors
     * Based on user settings, log the pitch and roll values
     */
    logFile << "%Time\t\tMarker\t\t";
    logFile << "Motor_1\t\tMotor_2\t\tMotor_3\t\tMotor_4\t\t";

    if (logPitch)
    {
        if(logPID)
        {
            logFile << "Pitch\t\tPitch_pid_p\t\tPitch_pid_i\t\tPitch_pid_d\t\tPitch_desired\t\t";
        }else if (logSS){
            logFile << "Pitch\t\tPitch_corr\t\tPitch_desired\t\t";
        }else if(logSSI){
            logFile << "Pitch\t\tPitch_corr\t\tPitch_desired\t\t";
        }else if(logSSIF){
            logFile << "Pitch\t\tPitch_corr\t\tPitch_desired\t\t";
        }
        // TODO: don't do now
        // logFile << "Pitch_p\t\tPitch_d\t\tPitch_i\t\t";
        // logFile << "Pitch_setPoint\t\t";
    }
    if (logRoll)
    {
        logFile << "Roll\t\t Roll_pid_p\t\tRoll_pid_i\t\tRoll_pid_d\t\tRoll_desired";

        // TODO: don't do now
        // logFile << "Roll_p\t\tRoll_d\t\tRoll_i\t\t";
        // logFile << "Roll_setPoint\t\t";
    }
	if (logEris)
	{
		//Eris time, eris x, eris y, eris z, eris roll, eris pitch, eris yaw, dersired x velosity
		logFile <<"\t\tEris_time\t\tEris_x\t\tEris_y\t\tEris_z\t\tEris_roll\t\tEris_pitch\t\tEris_yaw\t\tx_vel_set\t\ty_vel_set\t\tyaw_set";
	}
    if (logLocal)
    {
        logFile <<"\t\txb_vel\t\tyb_vel";
    }
    if(logMotors)
    {
        if(logSSIF){
            logFile << "\t\tXv_c\t\tYv_P\t\tYv_i\t\tYv_d\t\tYaw_p\t\tYaw_i\t\tYaw_d\t\tLeft_PWM\t\tRight_PWM\t\tFront_PWM\t\tBack_PWM\t\txb_vel_filt\t\tyb_vel_filt";
        }else{
            logFile << "\t\tXv_P\t\tXv_i\t\tXv_d\t\tYv_P\t\tYv_i\t\tYv_d\t\tYaw_p\t\tYaw_i\t\tYaw_d\t\tLeft_PWM\t\tRight_PWM\t\tFront_PWM\t\tBack_PWM\t\txb_vel_filt\t\tyb_vel_filt";
        }
    }
    logFile << endl;

    /*
     * Log file colum units.
     * Always log the time, marker, and motor thrust of all 4 motors
     * Based on user settings, log the pitch and roll values
     */
    // Time
    // logFile << "%Time\t\tMarker\t\t";
    //logFile << "Motor_1\t\tMotor_2\t\tMotor_3\t\tMotor_4\t\t";
    logFile << "&sec\t\tMarker\t\t";
    // Motor thrust (1-4)
    logFile << "%thrust\t\t%thrust\t\t%thrust\t\t%thrust\t\t";

    if (logPitch)
    {
        //logFile << "Pitch\t\tPitch_pid_p\t\tPitch_pid_i\t\tPitch_pid_d\t\tPitch_desired\t\t";
        // pitch, pitch_pid_p, pitch_pid_i, pitch_pid_d roll_desired
        if(logPID)
        {
        logFile << "degrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\t";
        }else if (logSS){
        logFile << "degrees\t\tdegrees\t\tdegrees\t\t";
        }else if(logSSI){
        logFile << "degrees\t\tdegrees\t\tdegrees\t\t";
        }else if(logSSIF){
        logFile << "degrees\t\tdegrees\t\tdegrees\t\t";
        }  
        // TODO: don't do now
        // pitch p constant, pitch d constant
        //logFile << "degrees\t\tdegrees\t\tdegrees\t\t";

        // pitch set point
        //logFile << "degrees\t\t";
    }
    if (logRoll)
    {
        // roll, roll error, roll_pid_p, roll_pid_d, roll_pid_i roll_desired
        logFile << "degrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees";

        // TODO: don't do now
        // roll p constant, roll d constant, roll i constant
        // logFile << "degrees\t\tdegrees\t\tdegrees\t\t";
        // roll set point
        // logFile << "degrees\t\t";
    }
	if (logEris)
	{
        //logFile <<"\t\tEris_time\t\tEris_x\t\tEris_y\t\tEris_z\t\tEris_roll\t\tEris_pitch\t\tEris_yaw\t\tx_vel_set\t\ty_vel_set\t\tyaw_set";
		//Eris time, eris x, eris y, eris z, eris roll, eris pitch, eris yaw, x velocity setpoint, y velocity setpoint, yaw setpoint
		logFile << "\t\tsec\t\tm\t\tm\t\tm\t\tdegrees\t\tdegrees\t\tdegrees\t\tcm/s\t\tcm/s\t\tdeggrees";
	}
    if(logLocal)
    {
        logFile << "\t\tm/s\t\tm/s";
    }
    if(logMotors)
    {
        //Xv_P, Xv_i, Xv_d, Yv_P, Yv_i, Yv_d, Yaw_p, Yaw_i, Yaw_d, Left_PWM, Right_PWM, Front_PWM, Back_PWM, xb_vel_filt, yb_vel_filt
        if(logSSIF){
            logFile << "\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tm/s\t\tm/s";
        }else if(logSSIw){
            logFile << "\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tm/s\t\tm/s";
        }
        else{
            logFile << "\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tPWM\t\tm/s\t\tm/s";
        }
    }
   
    quad->marker = 0;    // initializing the value of the marker to 0
    logFile << endl;

}

void Logger::stop()
{
    logFile.close();
}

void Logger::logData()
{
    /*
     * Log file data.
     * Always log the time and motor thrust of all 4 motors
     * Based on user settings, log the pitch and roll values
     */

    //Formatting settings (fixed point output for decimal values)
    logFile << fixed;

    // Time
    logFile << quad->vrpnNow     << "\t\t";
    logFile << quad->marker      << "\t\t";

    // Motor thrust for motors 1-4
    logFile << quad->motor_1_pwm << "\t\t"
            << quad->motor_2_pwm << "\t\t"
            << quad->motor_3_pwm << "\t\t"
            << quad->motor_4_pwm << "\t\t";

    // Log the pitch data values
    if (logPitch)
    {
        if(logPID){
            logFile << RAD_TO_DEG(quad->pitch.current)   << "\t\t"
                    << quad->pitch.PID_pcomp             << "\t\t"
                    << quad->pitch.PID_icomp             << "\t\t"
                    << quad->pitch.PID_dcomp             << "\t\t"
                    << RAD_TO_DEG(quad->pitch.desired)   << "\t\t";
        }else if (logSS){
            logFile << RAD_TO_DEG(quad->theta.position)   << "\t\t"
                    << quad->theta.correction             << "\t\t"
                    << 0   << "\t\t";    
        }else if (logSSI){
            logFile << RAD_TO_DEG(quad->thetaI.position)   << "\t\t"
                    << quad->thetaI.correction             << "\t\t"
                    << RAD_TO_DEG(quad->thetaI.reference)   << "\t\t";    
        }else if (logSSIF){
            logFile << RAD_TO_DEG(quad->RADA1D.position_theta)   << "\t\t"
                    << quad->RADA1D.correction1             << "\t\t"
                    << RAD_TO_DEG(quad->RADA1D.reference_theta)   << "\t\t";    
        }
        // TODO: don't do now
        //logFile << quad->pitch.p << "\t"
        //        << quad->pitch.d << "\t"
        //        << quad->pitch.i << "\t";
        //logFile << quad->pitch.desired << "\t";
    }

    // Log the roll data values
    if (logRoll)
    {
        logFile << RAD_TO_DEG(quad->roll.current)    << "\t\t"
                << quad->roll.PID_pcomp              << "\t\t"
                << quad->roll.PID_icomp              << "\t\t"
                << quad->roll.PID_dcomp              << "\t\t"
                << RAD_TO_DEG(quad->roll.desired);

        // TODO: don't do now
        //logFile << quad->roll.p << "\t"
        //        << quad->roll.d << "\t"
        //        << quad->roll.i << "\t";
        //logFile << quad->roll.desired << "\t";
    }
	//logs the data collected about Eris's time postion and orientation
	if (logEris)
	{
		logFile << "\t\t" << eris->vrpnNow              << "\t\t"
						  << eris->x                    << "\t\t"
				          << eris->y                    << "\t\t"
				          << eris->z                    << "\t\t"
				          << RAD_TO_DEG(eris->roll)     << "\t\t"
				          << RAD_TO_DEG(eris->pitch)    << "\t\t"
				          << RAD_TO_DEG(eris->yaw)      << "\t\t"
                          << eris->x_vel_set            << "\t\t"
                          << eris->y_vel_set            << "\t\t"
                          << RAD_TO_DEG(eris->yaw_set);
	}
    if (logLocal)
    {
        logFile << "\t\t" << eris->xb_vel       << "\t\t"
                          << eris->yb_vel;
    }
    if(logMotors){
        if(logSSIF){
            logFile << "\t\t" << quad->RADA1D.correction2            << "\t\t"
                              << motors->y_vel_ctrl.PID_pcomp        << "\t\t"
                              << motors->y_vel_ctrl.PID_icomp        << "\t\t"
                              << motors->y_vel_ctrl.PID_dcomp        << "\t\t"
                              << motors->yaw_ctrl.PID_pcomp          << "\t\t"
                              << motors->yaw_ctrl.PID_icomp          << "\t\t"
                              << motors->yaw_ctrl.PID_dcomp          << "\t\t"
                              << groundRobotMotors.motor_speed_left  << "\t\t"
                              << groundRobotMotors.motor_speed_right << "\t\t"
                              << groundRobotMotors.motor_speed_front << "\t\t"
                              << groundRobotMotors.motor_speed_rear  << "\t\t"
                              << eris->xb_vel_filt                   << "\t\t" 
                              << eris->yb_vel_filt;
        }else if(logSSIw){
            logFile << "\t\t" << quad->xI.correction            << "\t\t"
                              << motors->y_vel_ctrl.PID_pcomp        << "\t\t"
                              << motors->y_vel_ctrl.PID_icomp        << "\t\t"
                              << motors->y_vel_ctrl.PID_dcomp        << "\t\t"
                              << motors->yaw_ctrl.PID_pcomp          << "\t\t"
                              << motors->yaw_ctrl.PID_icomp          << "\t\t"
                              << motors->yaw_ctrl.PID_dcomp          << "\t\t"
                              << groundRobotMotors.motor_speed_left  << "\t\t"
                              << groundRobotMotors.motor_speed_right << "\t\t"
                              << groundRobotMotors.motor_speed_front << "\t\t"
                              << groundRobotMotors.motor_speed_rear  << "\t\t"
                              << eris->xb_vel_filt                   << "\t\t" 
                              << eris->yb_vel_filt;
        }
        else{
            logFile << "\t\t" << motors->x_vel_ctrl.PID_pcomp        << "\t\t"
                              << motors->x_vel_ctrl.PID_icomp        << "\t\t"
                              << motors->x_vel_ctrl.PID_dcomp        << "\t\t"
                              << motors->y_vel_ctrl.PID_pcomp        << "\t\t"
                              << motors->y_vel_ctrl.PID_icomp        << "\t\t"
                              << motors->y_vel_ctrl.PID_dcomp        << "\t\t"
                              << motors->yaw_ctrl.PID_pcomp          << "\t\t"
                              << motors->yaw_ctrl.PID_icomp          << "\t\t"
                              << motors->yaw_ctrl.PID_dcomp          << "\t\t"
                              << groundRobotMotors.motor_speed_left  << "\t\t"
                              << groundRobotMotors.motor_speed_right << "\t\t"
                              << groundRobotMotors.motor_speed_front << "\t\t"
                              << groundRobotMotors.motor_speed_rear  << "\t\t"
                              << eris->xb_vel_filt                   << "\t\t" 
                              << eris->yb_vel_filt;
        }

    }
    logFile << endl;
}
