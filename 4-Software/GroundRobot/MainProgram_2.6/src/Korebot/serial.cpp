/* serial.c
 * Mostly private functions used by IMU.c to access the serial interface.
	Author: Seth Beinhart
	Beinhart@iastate.edu
*/
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include "serial.h"
#define BAUDRATE B115200


int initport(int fd) {




	struct termios options;
	// Get the current options for the port...
	tcgetattr(fd, &options);
	// Set the baud rates to 19200...
	cfsetispeed(&options, BAUDRATE);
	cfsetospeed(&options, BAUDRATE);
	// Enable the receiver and set local mode...
	options.c_cflag |= (CLOCAL | CREAD);

	//options.c_cflag &= ~PARENB;
	//options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;
	//no softwar control flow
	options.c_iflag&=~(IXON|IXOFF|IXANY);
	options.c_oflag&=~OPOST;
        /* set input mode (non-canonical, no echo,...) */
        options.c_lflag = 0;
  
        options.c_cc[VTIME]    = 0;   /* inter-character timer unused */
        options.c_cc[VMIN]     = 1;   /* blocking read until 1 chars received */
	
	// Set the new options for the port...
	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW, &options);

	
	return 1;
}



// ALL methods sent to the function must be null terminated
// and have ss1 empty space extra for the addition of a <CR> //no longer needed
int writeport(int fd,BYTE *chars,int bytesToWrite) {
	//int len = strlen(chars);
	//chars[bytes] = 0x0d; // stick a <CR> after the command at the /n spot
	//chars[len+1] = 0x00; // terminate the string properly not 
	int n = write(fd, chars, bytesToWrite);
	if (n < 0) {
	//	fputs("write failed!\n", stderr);
		return 0;
	}
	usleep(5000);	//allow device to write packet
	return 1;                                                                       	                                
}
// Buffers must be the size of 256 to allow for max packet plus a null terminator
int readport(int fd,BYTE *result, int bytesToRead) {
	if(bytesToRead==0)
	{return 0;}
	int iIn = read(fd, result, bytesToRead);	// max size of 1 packet
	//result[iIn-1] = 0x00;		//puts it in 
	if (iIn < 0) {
		if (errno == EAGAIN) {
			//printf("\nSERIAL EAGAIN ERROR\n");
			return 0;
		} else {
			//printf("SERIAL read error %d %s\n", errno, strerror(errno));
			return 0;
		}
	}    
	//tcflush(fd,TCIFLUSH);            
	return iIn;
}

int getbaud(int fd) {
	struct termios termAttr;
	int inputSpeed = -1;
	speed_t baudRate;
	tcgetattr(fd, &termAttr);
	/* Get the input speed.                              */
	baudRate = cfgetispeed(&termAttr);
	switch (baudRate) {
		case B0:      inputSpeed = 0; break;
		case B50:     inputSpeed = 50; break;
		case B110:    inputSpeed = 110; break;
		case B134:    inputSpeed = 134; break;
		case B150:    inputSpeed = 150; break;
		case B200:    inputSpeed = 200; break;
		case B300:    inputSpeed = 300; break;
		case B600:    inputSpeed = 600; break;
		case B1200:   inputSpeed = 1200; break;
		case B1800:   inputSpeed = 1800; break;
		case B2400:   inputSpeed = 2400; break;
		case B4800:   inputSpeed = 4800; break;
		case B9600:   inputSpeed = 9600; break;
		case B19200:  inputSpeed = 19200; break;
		case B38400:  inputSpeed = 38400; break;
		case B115200:  inputSpeed = 115200; break;
	}
	return inputSpeed;
}
