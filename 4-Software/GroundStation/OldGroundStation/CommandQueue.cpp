#include "CommandQueue.h"
#include <QTimer>

CommandQueue::CommandQueue()
{
    command_interval_ms = 100;
    state = STATE_IDLE;
}

CommandQueue::~CommandQueue()
{

}

bool CommandQueue::clear()
{
    position_queue.resize(0);
    state = STATE_IDLE;
    return true;
}

bool CommandQueue::start()
{
    if (position_queue.size() > 0)
        state = STATE_STARTED;
    return true;
}

STATE CommandQueue::getQueueState()
{
    return state;
}

bool CommandQueue::addPosition(Position position)
{
    position_queue.push_back(position);
    if (state != STATE_STARTED)
    {
        state = STATE_STARTED;
        sendPosition();
    }
    return true;
}

bool CommandQueue::addPositions(std::vector<Position> positions)
{
    position_queue.insert(position_queue.end(), positions.begin(), positions.end());
    if (state != STATE_STARTED)
    {
        state = STATE_STARTED;
        sendPosition();
    }
    return true;
}

bool CommandQueue::sendPosition()
{
    if (state == STATE_IDLE)
        return false;

    if (position_queue.size() == 0)
    {
        state = STATE_IDLE;
        return false;
    }

    control->adjustTarget(position_queue[0]);
    position_queue.erase(position_queue.begin());

    QTimer::singleShot(command_interval_ms, this, SLOT(sendPosition()));

    return true;
}
