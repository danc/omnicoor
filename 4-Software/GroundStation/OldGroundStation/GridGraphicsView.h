#ifndef GRIDGRAPHICSVIEW_H
#define GRIDGRAPHICSVIEW_H

#include "MCTStructures.h"
#include <QGraphicsView>
#include <vector>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <vector>
#include <cmath>

/** @brief QGraphicsView widget for drawing a path.
  * @details The drawn path is just a series of (x,y,z,heading) positoins.
  * The quadcopter can fly around following the path.
  * @todo Add add public booleans to allow easy mirroring of the horizontal and verticle axis.
  */
class GridGraphicsView : public QGraphicsView
{
public:
    /** @brief Initializes the gridgraphicsview widget.
      * @details This is automatically called by the contstructors.
      * Subsequent calls will probably lead to memory leaks.
      */
    void init();

    /** @brief Default constructor for GridGraphicsView
      */
    GridGraphicsView(QFrame*&);

    /** @brief Default constructor for GridGraphicsView
      */
    GridGraphicsView(QWidget*&);

    /** @brief Default destructor for GridGraphicsView
      */
    ~GridGraphicsView();

    //! The minumum x coordinate of the display view.
    double x_min;

    //! The maximum x coordinate of the display view.
    double x_max;

    //! The minimum y coordinate of the display view.
    double y_min;

    //! The maximum y coordinate of the diaplay view.
    double y_max;

    /** @brief The current altitude value.
      * @details This widget isn't able to determine heading values on its own so
      *          they have to be input from an external source.
      */
    double z;

    /** @brief The current heading value.
      * @details This widget isn't able to determine heading values so
      *          they have to be input from an external source.
      */
    double heading;

    //! The minimum distance the mouse has to move in meters during a move event before another segment is drawn.
    double minimum_segment_distance;

    /** @brief Gets the entire path as absolute meters from the origin.
      * @return Vector of Positions.
      */
    std::vector<Position> getPathAM();

    /** @brief Gets the entire path as relative meters from the previous location.
      * @details the value of current_position is used as the inital location of the path.
      * The first command will be an offset from that.
      * @return Vector of Positions.
      */
    std::vector<Position> getPathRM();

    /** @brief Sets the path using absolute meters from the origin.
      * @param[in] new_path The new path to replace the old path with.
      * @return True if successful.
      */
    bool setPathAM(std::vector<Position> new_path);

    /** @brief Sets the path using relative meters from the previous location.
      * @param[in] new_path The new path to replace the old path with.
      * @return True if successful.
      */
    bool setPathRM(std::vector<Position> new_path);

    /** @brief Clears the path
      * @return True if successful.
      */
    bool clearPath();

    /** @brief Smooths the path using an algorithm.
      * @return True if the path was smoothed.
      */
    bool smoothPath();

    /** @brief Redraws the entire path.
      * @return True if successful.
      */
    bool redrawPath();

    /** @brief Sets the current location of the quadcopter to be drawn on screen.
      * @details The location wilpathl update immediately after calling this function.
      * @param[in] position Position structure containing x,y,z,heading.
      * @return True if successful.
      */
    bool setCurrentLocation(Position position);

    /** @brief Sets the target location of the quadcopter to be drawn on screen.
      * @details The location will update immediately after calling this function.
      * @param[in] position Position structure containing x,y,z,heading.
      * @return True if successful.
      */
    bool setTargetLocation(Position position);

private:
    //! The current width of this widget in pixels.
    double width;

    //! The current height of this widget in pixels.
    double height;

    /** @brief Adds a vertex to the end of the path and draws the segment it creates.
      * @details If the vertex would create a segment that is longer than 2*minimum_segment_distance,
      * the segment is split into smaller segments.
      * @param[in] vertex Position structure containing x,y,z,heading.
      * @return True if successful.
      */
    bool addVertex(Position vertex);

    /** @brief Draws a line segment between the two specified vertices.
      * @param[in] vertex1 Position structure containing x,y,z,heading.
      * @param[in] vertex2 Position structure containing x,y,z,heading.
      * @return True if successful.
      */
    bool drawSegment(Position vertex1, Position vertex2);

    /** @brief Checks if a vertex is at least a minimum distance from the vertex at the end of the path.
      * @param[in] v1 The first position.
      * @param[in] v2 The seconds position.
      * @param[in] dist The distance to check against.
      * @return True if the vector is above the minimum distance.
      */
    bool isAboveDist(Position v1, Position v2, double dist);

    /** @brief Converts from a meter coordiante system to a pixel coordinate system.
      * @param[in] vertex Position structure containing x,y,z,heading.
      * @return Position structure containing x,y,z,heading.
      */
    Position alignMeterToPixel(Position vertex);

    /** @brief Converts from a pixel coordinate system to a meter coordinate system.
      * @param[in] vertex Position structure containing x,y,z,heading.
      * @return Position structure containing x,y,z,heading.
      */
    Position alignPixelToMeter(Position vertex);

protected:
    /** @brief Add another vertex to the path when the user clicks the mouse.
      */
    void mousePressEvent( QMouseEvent *event );

    /** @brief Draws a continuous path when the user clicks and drags the mouse around the grid.
      */
    void mouseMoveEvent( QMouseEvent *event );

    /** @brief When the view is resized, recalcuate the alignment and redraw the path with the new alignment.
      * @details This seems to get called upon first creation so we should be able to use this to set up the initial alignment.
      */
    void resizeEvent(QResizeEvent *event);

    //! Used to draw the path on the screen.
    QGraphicsScene* grid_graphics_scene;

    //! Stores the current location of the quadcopter
    Position current_location;

    //! Stores the current target location of the quadcopter
    //! For the current implementation of this class, this should always be (0,0,0,0);
    Position target_location;

    //! Stores the path using an abolute position system from the origin.
    std::vector<Position> path;

};

#endif // GRIDGRAPHICSVIEW_H
