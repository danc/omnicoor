#ifndef FOURCHANTRAIN_H
#define FOURCHANTRAIN_H

#include "Module.h"
#include "Trainer.h"
#include <vector>

/** @deprecated Use the six channel module.
  * @brief Module for communicating with the quadcopter via a 4 channel controller.
  */
class FourChanTrain : public Module
{
protected:
    Trainer *train;
    std::string usb_id;

    double MAX_THROTTLE;
    double CHANGE_THROTTLE;
    double MIN_THROTTLE;
    double MID_PITCH;
    double CHANGE_PITCH;
    double MID_ROLL;
    double CHANGE_ROLL;
    double MID_YAW;
    double CHANGE_YAW;


    double thrtVal;
    double rollVal;
    double yawVal;
    double pitchVal;

    double thrtTrim;
    double rollTrim;
    double pitchTrim;
    double yawTrim;

public:
    FourChanTrain();
    ~FourChanTrain();
    bool isCommMod() {return true;}
    bool setModValues(double *data, int size);
    bool getModValues(double *data, int size);
    bool start();
    bool update();
    bool stop();
};

#endif // FOURCHANTRAIN_H
