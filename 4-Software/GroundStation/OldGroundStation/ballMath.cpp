/*---------------------------------------------------------
 File: ballMath.cpp

 --------------------------------------------------------*/

/*---------------------------------------------------------
INCLUDES
---------------------------------------------------------*/
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "ballMath.h"

/*---------------------------------------------------------
DEFINITIONS
---------------------------------------------------------*/
#define PI 3.14159265

/*---------------------------------------------------------
GLOBAL VARIABLES
---------------------------------------------------------*/
/*---------------------------------------------------------
MACROS
---------------------------------------------------------*/


/*********************************************************
 * Title:               ballSortFrame
 *
 * Description:
 * 		Takes in one frame of data from the camera and 
 *		sorts the balls in a clockwise fashion. The
 *		sorted front ball is ball number 1.
 *		Returns the same frame of data with the balls sorted.
 *
 ********************************************************/

ball_frame_data_t ballSortFrame(ball_frame_data_t unsorted )
{
    ball_frame_data_t sorted;
    float dist_1_2; // distances between unsorted balls 1, 2, and 3
    float dist_2_3;
    float dist_3_1;
    float center_x; // The center point of the back two balls (2 and 3)
    float center_y;
    float dx;
    float dy;

    dist_1_2 = ballgetDistance( unsorted.ball1, unsorted.ball2 );
    dist_2_3 = ballgetDistance( unsorted.ball2, unsorted.ball3 );
    dist_3_1 = ballgetDistance( unsorted.ball3, unsorted.ball1 );

    /* The shortest distance is opposite the front ball, ball 1 */
    if ( dist_1_2 <= dist_2_3 && dist_1_2 <= dist_3_1 )
    {	
        sorted.ball1 = unsorted.ball3;
        sorted.ball2 = unsorted.ball1;
        sorted.ball3 = unsorted.ball2;
    }
    else if ( dist_2_3 <= dist_1_2 && dist_2_3 <= dist_3_1 )
    {
        sorted.ball1 = unsorted.ball1;
        sorted.ball2 = unsorted.ball2;
        sorted.ball3 = unsorted.ball3;
    }
    else
    {
        sorted.ball1 = unsorted.ball2;
        sorted.ball2 = unsorted.ball3;
        sorted.ball3 = unsorted.ball1;
    }

    center_x = (sorted.ball2.x + sorted.ball3.x) / 2;
    center_y = (sorted.ball2.y + sorted.ball3.y) / 2;

    /*
 * Slope between center and front ball
 * Note: This will be considered the slope of the direction the 
 * helicopter is facing, using this logic, if the slope is postive
 * the ball on the left should have a larger y value.  If the slope
 * is negative, the ball on the right should have a higher y value.
 */
    dx = sorted.ball1.x - center_x;
    dy = sorted.ball1.y - center_y;
    float posdx, posdy;
    if (dx<0)
    {
        posdx=-dx;
    }
    else
    {
        posdx=dx;
    }
    if (dy<0)
    {
        posdy=-dy;
    }
    else
    {
        posdy=dy;
    }
    if ( posdy<10*posdx && posdx<10*posdy)
    {
        // if slope negative, resort so ball2 y is larger
        if ( dy > 0 && dx < 0 )
        {
            if ( sorted.ball2.y < sorted.ball3.y )
            {
                swapBalls( &sorted.ball2, &sorted.ball3 );
            }
        }
        else if ( dy < 0 && dx > 0 )
        {
            if ( sorted.ball2.y > sorted.ball3.y )
            {
                swapBalls( &sorted.ball2, &sorted.ball3 );
            }
        }
        // if slope is positive, ball 2 y is smaller
        else if ( dy < 0 && dx < 0 )
        {
            if ( sorted.ball2.x > sorted.ball3.x )
            {
                swapBalls( &sorted.ball2, &sorted.ball3 );
            }
        }
        else // dy > 0 && dx > 0
        {
            if ( sorted.ball2.x < sorted.ball3.x )
            {
                swapBalls( &sorted.ball2, &sorted.ball3 );
            }
        }
    }
    else if ( posdy>=10*posdx ) // helicopter is vertical
    {
        // If helicopter facing up, ball2 x should be larger
        if ( center_y < sorted.ball1.y )
        {
            if ( sorted.ball2.x < sorted.ball3.x )
            {
                swapBalls( &sorted.ball2, &sorted.ball3 );
            }
        }	
        else
        {
            if ( sorted.ball2.x > sorted.ball3.x )
            {
                swapBalls( &sorted.ball2, &sorted.ball3 );
            }
        }
    }
    else if (posdx>=10*posdy)//if helicopter is horizontal
    {
        if ( center_x < sorted.ball1.x ) // heli is facing right
        {
            if ( sorted.ball2.y > sorted.ball3.y )
            {
                swapBalls( &sorted.ball2, &sorted.ball3 );
            }
        }
        else // heli is facing left
        {
            if ( sorted.ball2.y < sorted.ball3.y )
            {
                swapBalls( &sorted.ball2, &sorted.ball3 );
            }
        }
    }

    sorted.timestamp = unsorted.timestamp;

    return sorted;
}

/*********************************************************
 * Title:               getLocation
 *
 * Description:
 *
 * 		To take in the sorted data from the motion sensor camear and return 
 *      the global X,Y,Z position of the helicopter.  The X and Y coordinates
 *      can be used as a psudo GPS while the Z can be used for the throttle PID
 *      controller.  fps is the frame rate of the camera per second.
 *
 ********************************************************/
ball_Location ballgetLocation( ball_frame_data_t sorted )
{
    ball_Location location;
    float center_x;
    float center_y;
    float back_center_x;
    float back_center_y;
    float dist_1_2; // distances between unsorted balls 1, 2, and 3
    float dist_2_3;
    float dist_3_1;
    float ppm;
    float X,Y,Z;

    dist_1_2 = ballgetDistance( sorted.ball1, sorted.ball2 );
    dist_2_3 = ballgetDistance( sorted.ball2, sorted.ball3 );
    dist_3_1 = ballgetDistance( sorted.ball3, sorted.ball1 );

    ppm=1.0/3.0*(dist_1_2/.43+dist_2_3/.35+dist_3_1/.42);

    location.Z=(ppm-241)/80;

    back_center_x = ( sorted.ball2.x + sorted.ball3.x ) / 2;
    back_center_y = ( sorted.ball2.y + sorted.ball3.y ) / 2;

    center_x=(sorted.ball1.x+back_center_x)/2;
    center_y=(sorted.ball1.y+back_center_y)/2;

    location.X=center_x/ppm+.5*(2.6556-640/ppm);

    location.Y=center_y/ppm+.5*(1.99-480/ppm);

    return location;
}

/*********************************************************
 * Title:               getDirection
 *
 * Description:
 * 		Takes in sorted ball data from the camera and 
 *      returns the angle of direction the helicopter 
 *      is facing. The positive X axis of the camera
 *      is aligned with North, which is considered
 *      0 radians and changes from 0 to 2*PI when turned
 *      counterclockwise: West is PI/2, South is PI, East
 *      is 3*PI/2.
 *
 ********************************************************/
float ballgetDirection(ball_frame_data_t sorted )
{
    float center_x;
    float center_y;
    float dx;
    float dy;
    float theta;
    float x;
    center_x = ( sorted.ball2.x + sorted.ball3.x ) / 2;
    center_y = ( sorted.ball2.y + sorted.ball3.y ) / 2;
    dx = sorted.ball1.x - center_x;
    dy = sorted.ball1.y - center_y;

    theta = -atan2( dy, dx );

    if ( theta+PI/2 > PI )
    {
        x=theta-PI/2;
        theta=-PI+x;
    }
    else
    {
        theta=theta+PI/2;
    }
    theta=theta*180/PI;
    return theta;
}

/*********************************************************
 * Title:               getPitchRoll
 *
 * Description:
 * 		Takes in set location, current location, and heading and splits the 
 *      distance moved into pitch and roll movement
 *
 ********************************************************/
PitchRoll ballgetPitchRoll(float set_X, float set_Y, float current_X, float current_Y, float heading)
{
    ball_info_t prevLocation;
    ball_info_t currentLocation;
    PitchRoll pitchroll;
    //prevLocation.x=set_X;
    //prevLocation.y=set_Y;
    //currentLocation.x=current_X;
    //currentLocation.y=current_Y;
    float x;
    
    float distanceMoved=ballgetDistance(currentLocation,prevLocation);

    float dx = prevLocation.x - currentLocation.x;
    float dy = prevLocation.y - currentLocation.y;

    float theta = -atan2( dy, dx );

    if ( theta+PI/2 > PI )
    {
        x=theta-PI/2;
        theta=-PI+x;
    }
    else
    {
        theta=theta+PI/2;
    }
    heading=heading*PI/180.0;
    
    float dTheta=heading-theta;
    float pitch=distanceMoved*cos(dTheta);
    float roll=distanceMoved*sin(dTheta);
    
    pitchroll.Pitch=-pitch;
    pitchroll.Roll=roll;
    
    return pitchroll;
}

/*********************************************************
 * Title:               swapBalls
 *
 * Description:
 *      Swaps the data for two balls
 *
 ********************************************************/
void swapBalls(ball_info_t* one, ball_info_t* two )
{
    ball_info_t	*copy;
    *copy = *one;
    *one = *two;
    *two = *copy;
}



/*********************************************************
 * Title:               getDistance
 *
 * Description:
 *      Gets the distance between two balls
 *
 ********************************************************/
float ballgetDistance( ball_info_t one, ball_info_t two )
{
    float diffX;
    float diffY;
    float sqrt;

    diffX = two.x - one.x;
    diffY = two.y - one.y;

    sqrt = sqrtf(diffX*diffX + diffY*diffY);
    return sqrt;
}




