#ifndef IMU_H
#define IMU_H

#include <sys/types.h>
#include <vector>

#include "Module.h"
#include "MCTStructures.h"

class IMU : public Module
{
private:
    int imu_pID;
    int xbee_fd;
    int KALMAN;
    std::string usb_id;
public:
    IMU();
    ~IMU();
    bool isDirMod()  {return true;}
    bool isPosMod()  {return true;}
    bool isOriMod()  {return true;}
    bool start();
    bool update();
    bool stop();
    bool check_xbee(int p);
    bool read_uart(char *payload, int length);
    void set_uart_options();
};

#endif // IMU_H
