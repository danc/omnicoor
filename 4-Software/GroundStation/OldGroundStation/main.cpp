#include <QtGui/QApplication>

#include "mainwindow.h"
#include "MCTController.h"
#include "FourchanTrainMod.h"
#include "SixchanTrainMod.h"
#include "ImuMod.h"
#include "SingleCameraMod.h"
#include "USBRemoteMod.h"
#include "OptitrackMod.h"
#include "OmnibotTracker.h"
#include "usb2train.h"

using namespace std;


int main(int argc, char *argv[])
{
    //Create main controller
    MCTController *cont = new MCTController();
    MCTController *secondary = new MCTController();
    //Create each module
    SixChanTrain *six = new SixChanTrain();
    FourChanTrain *four = new FourChanTrain();
    IMU *imu = new IMU();
    //SingleCamera *single = new SingleCamera();
    OptiTrack *opti = new OptiTrack();
    USBRemote *usbr = new USBRemote();

    OmnibotTracker *omni = new OmnibotTracker();

    //add modules to controller
    cont->addModule(six);
    cont->addModule(four);
    cont->addModule(opti);
    cont->addModule(imu);

    cont->addModule(omni);

    //cont->addModule(single);
    cont->addModule(usbr);

    //add modules to controller
    secondary->addModule(six);
    secondary->addModule(four);
    secondary->addModule(opti);
    secondary->addModule(imu);

    secondary->addModule(omni);

    //secondary->addModule(single);
    secondary->addModule(usbr);

    QApplication a(argc, argv);
    MainWindow *w = new MainWindow(cont, secondary);
    w->show();
    
    return a.exec();
}
