#ifndef SSBEHAVIOR_H
#define SSBEHAVIOR_H

#define MAX_MATRIX_SIZE 24
#include "BehaviorModule.h"
#include "MCTStructures.h"
#include "Datalogger.h"
#include "DataloggerAlt.h"
#include <string>
#include <vector>

/** @brief Module for specific behavior of the quadcopter and how it reacts to inputs from the camera system.
  * @details This module takes the quadcopters orientation and position.
  *          The module determines an action to take based on the inputs.
  *          The module outputs values that relate to the control the quadcopter's pitch, yaw, roll, and throttle.
  */
class SSBehavior : public BehaviorModule
{
protected:
    double throttle[MAX_MATRIX_SIZE];
    double yaw[MAX_MATRIX_SIZE];
    double pitch[MAX_MATRIX_SIZE];
    double roll[MAX_MATRIX_SIZE];
    double errors[MAX_MATRIX_SIZE];
    int size;

    double thrtCor;
    double pitCor;
    double rolCor;
    double yawCor;
    timespec init, mark;
    double prevDir;
    MCTorientation prevOri;
    MCTposition prevPos;

public:
    /** @brief Default constructor for SSBehavior.
      */
    SSBehavior();

    /** @brief Default destructor for SSBehavior.
      */
    ~SSBehavior();

    /** @brief Gets the correction for the controller based on values of the FPGA interface
      * @param[out] *sigCor Reference to an array to write output values to.
      *                     The values might correspond to [throttle, elevator, aileron, rudder] But I'm not sure.
      * @param[in]  curOri Values for the quadcopter's current orientation.  Contains latitude, longitude, altitude.
      * @param[in]  currDir The quadcopters current direction.
      * @param[in]  curPos Values for the quadcopter's current position.  Contains roll, pitch, yaw.
      * @param[in]  targetPos Values for the quacopter's target position.  Contains latitude, longitude, altitude.
      * @param[in]  targetDir The quadcopter's target direction.
      *
      * @return True is successful.
     **/
    bool getCorrections(double *sigCor, MCTorientation curOri, double currDir, MCTposition curPos, MCTposition targetPos, double targetDir);

    /** @brief Loads the profile associated with tis behavior and sets internal constants.
      * @details Loads the currently active profile.
      *          Gets the file associated with that profile.
      *          Attemps to load constants from that file.
      * @return True if successful.
      */
    bool populateConstants();

    /** @brief Initializes the module.
      * @details Records the current time and sets the corrections to 0.
      * @return True is successful.
      */
    bool initialize();

    /** @brief Writes information to the log file.
      * @details Takes a supplied reference to the datalogger object and log information.
      *          Currently this doesn't log anything.
      * @param[out] log Reference to a datalogger object.
      * @return True if successful.
      */
    bool write_log_header(Datalogger *log);

    //PID specific methods
    //double get_SS_correction(double current, double target, MCTPIDERR err, MCTPID pid);
    //double get_SS_correction_verbose(double current, double target, MCTPIDERR err, MCTPID pid);
    //void calc_orientation(MCTposition cpos, double cdir, MCTposition tpos, double tdir);
    //PID read write methods
    //void read_PID_model(std::string filename);
    //void write_PID_model(std::string filename, MCTUAV model);


};

#endif // SSBEHAVIOR_H
