#ifndef OPTITRACKMOD_H
#define OPTITRACKMOD_H

#include <sys/types.h>
#include "Module.h"
#include "MCTStructures.h"
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"


/** @brief Module for tracking the quadcopter using the VRPN camera setup.
 **/
class OptiTrack : public Module
{
private:
    vrpn_Connection *connection;
    vrpn_Tracker_Remote *tracker;

public:
    OptiTrack();
    ~OptiTrack();
    bool isDirMod()  {return true;}
    bool isPosMod()  {return true;}
    bool isOriMod()  {return true;}

    bool start();
    bool update();
    bool stop();
};



#endif
