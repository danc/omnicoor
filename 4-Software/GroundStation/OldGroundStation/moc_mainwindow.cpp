/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Sun Nov 30 14:44:02 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      41,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   12,   11,   11, 0x08,
      62,   12,   11,   11, 0x08,
     107,   12,   11,   11, 0x08,
     152,   12,   11,   11, 0x08,
     198,   12,   11,   11, 0x08,
     244,   12,   11,   11, 0x08,
     286,   12,   11,   11, 0x08,
     333,   11,   11,   11, 0x08,
     368,   11,   11,   11, 0x08,
     403,   11,   11,   11, 0x08,
     432,   11,   11,   11, 0x08,
     464,   11,   11,   11, 0x08,
     495,   11,   11,   11, 0x08,
     525,   11,   11,   11, 0x08,
     551,   11,   11,   11, 0x08,
     564,   11,   11,   11, 0x08,
     579,   11,   11,   11, 0x08,
     613,   11,   11,   11, 0x08,
     646,   11,   11,   11, 0x08,
     680,   11,   11,   11, 0x08,
     713,   11,   11,   11, 0x08,
     748,   11,   11,   11, 0x08,
     782,   11,   11,   11, 0x08,
     816,   11,   11,   11, 0x08,
     849,   11,   11,   11, 0x08,
     861,   11,   11,   11, 0x08,
     898,   11,   11,   11, 0x08,
     935,   11,   11,   11, 0x08,
     972,   11,   11,   11, 0x08,
    1022,   11,   11,   11, 0x08,
    1036,   11,   11,   11, 0x08,
    1068,   11,   11,   11, 0x08,
    1099,   11,   11,   11, 0x08,
    1131,   11,   11,   11, 0x08,
    1164,   11,   11,   11, 0x08,
    1182,   11,   11,   11, 0x08,
    1213,   11,   11,   11, 0x08,
    1244,   11,   11,   11, 0x08,
    1276,   11,   11,   11, 0x08,
    1289,   11,   11,   11, 0x08,
    1314, 1308,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0arg1\0"
    "on_cBox_Mod_Ori_currentIndexChanged(QString)\0"
    "on_cBox_Mod_Pos_currentIndexChanged(QString)\0"
    "on_cBox_Mod_Dir_currentIndexChanged(QString)\0"
    "on_cBox_Mod_Comm_currentIndexChanged(QString)\0"
    "on_cBox_Mod_Cont_currentIndexChanged(QString)\0"
    "on_cBox_Bhvr_currentIndexChanged(QString)\0"
    "on_cBox_BhvrModel_currentIndexChanged(QString)\0"
    "on_pButton_IO_CancelCmds_clicked()\0"
    "on_pButton_IO_ImportFile_clicked()\0"
    "on_pButton_IO_Send_clicked()\0"
    "on_pButton_Main_Start_clicked()\0"
    "on_pButton_Main_Stop_clicked()\0"
    "on_pButton_Take_Off_clicked()\0"
    "on_pButton_Land_clicked()\0throttleUp()\0"
    "throttleDown()\0on_rButton_Sel_Thr_Auto_clicked()\0"
    "on_rButton_Sel_Thr_Man_clicked()\0"
    "on_rButton_Sel_Pit_Auto_clicked()\0"
    "on_rButton_Sel_Pit_Man_clicked()\0"
    "on_rButton_Sel_Roll_Auto_clicked()\0"
    "on_rButton_Sel_Roll_Man_clicked()\0"
    "on_rButton_Sel_Yaw_Auto_clicked()\0"
    "on_rButton_Sel_Yaw_Man_clicked()\0"
    "gridSetup()\0on_Settings_Grid_X_editingFinished()\0"
    "on_Settings_Grid_Y_editingFinished()\0"
    "on_Settings_Grid_Z_editingFinished()\0"
    "on_Settings_Grid_Queue_Interval_editingFinished()\0"
    "sendCommand()\0on_Grid_Toolbar_Clear_clicked()\0"
    "on_Grid_Toolbar_Save_clicked()\0"
    "on_cBox_Mod_TrackOmni_clicked()\0"
    "on_cBox_Mod_FollowOmni_clicked()\0"
    "setOmniToTarget()\0on_Grid_Toolbar_Load_clicked()\0"
    "on_Grid_Toolbar_Send_clicked()\0"
    "on_Grid_Toolbar_Reset_clicked()\0"
    "updateGrid()\0updateController()\0event\0"
    "keyPressEvent(QKeyEvent*)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_cBox_Mod_Ori_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_cBox_Mod_Pos_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->on_cBox_Mod_Dir_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->on_cBox_Mod_Comm_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->on_cBox_Mod_Cont_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_cBox_Bhvr_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_cBox_BhvrModel_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_pButton_IO_CancelCmds_clicked(); break;
        case 8: _t->on_pButton_IO_ImportFile_clicked(); break;
        case 9: _t->on_pButton_IO_Send_clicked(); break;
        case 10: _t->on_pButton_Main_Start_clicked(); break;
        case 11: _t->on_pButton_Main_Stop_clicked(); break;
        case 12: _t->on_pButton_Take_Off_clicked(); break;
        case 13: _t->on_pButton_Land_clicked(); break;
        case 14: _t->throttleUp(); break;
        case 15: _t->throttleDown(); break;
        case 16: _t->on_rButton_Sel_Thr_Auto_clicked(); break;
        case 17: _t->on_rButton_Sel_Thr_Man_clicked(); break;
        case 18: _t->on_rButton_Sel_Pit_Auto_clicked(); break;
        case 19: _t->on_rButton_Sel_Pit_Man_clicked(); break;
        case 20: _t->on_rButton_Sel_Roll_Auto_clicked(); break;
        case 21: _t->on_rButton_Sel_Roll_Man_clicked(); break;
        case 22: _t->on_rButton_Sel_Yaw_Auto_clicked(); break;
        case 23: _t->on_rButton_Sel_Yaw_Man_clicked(); break;
        case 24: _t->gridSetup(); break;
        case 25: _t->on_Settings_Grid_X_editingFinished(); break;
        case 26: _t->on_Settings_Grid_Y_editingFinished(); break;
        case 27: _t->on_Settings_Grid_Z_editingFinished(); break;
        case 28: _t->on_Settings_Grid_Queue_Interval_editingFinished(); break;
        case 29: _t->sendCommand(); break;
        case 30: _t->on_Grid_Toolbar_Clear_clicked(); break;
        case 31: _t->on_Grid_Toolbar_Save_clicked(); break;
        case 32: _t->on_cBox_Mod_TrackOmni_clicked(); break;
        case 33: _t->on_cBox_Mod_FollowOmni_clicked(); break;
        case 34: _t->setOmniToTarget(); break;
        case 35: _t->on_Grid_Toolbar_Load_clicked(); break;
        case 36: _t->on_Grid_Toolbar_Send_clicked(); break;
        case 37: _t->on_Grid_Toolbar_Reset_clicked(); break;
        case 38: _t->updateGrid(); break;
        case 39: _t->updateController(); break;
        case 40: _t->keyPressEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 41)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 41;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
