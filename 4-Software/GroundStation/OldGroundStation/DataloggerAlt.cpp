#include "DataloggerAlt.h"

#include <string>
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <ctime>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>


DataloggerAlt::DataloggerAlt(std::string fileName)
{
    directory = "";
    filename = "_1_D_Test_" + fileName;
}

/**
  * The start method for the data logger creates a folder of the day and then places the log
  * for that session (session being from successfully starting the controller to stopping the controller)
  * folders are marked year - month - date day and individual file is marked by the time of start
  * @author Jeff Wick MCT 2012
 **/
bool DataloggerAlt::start()
{
    /**
      * From main_dev.c
      * @author MCT 2010
     **/
    time_t now;
    struct tm *ts;
    char buf[300],buf2[300];

    now = time(0);
    ts = localtime(&now);
    //Directory should look like "2011-4-15_DAY" and specific log "12:34:10_1_D_Test"
    strftime(buf, sizeof(buf),"%Y-%m-%d_%a",ts);
   // filename = "_1_D_Test_" + filename;
    const char* fname = filename.c_str();
    strftime(buf2, sizeof(buf2), "(%H:%M:%S)", ts);
    strcat(buf2,fname);
    directory = "Logs/";
    directory += buf;
    /*---------------------------------------------
    Start up development logfile
    Create new directory for development logfile
    ---------------------------------------------*/
    mkdir(directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    /*---------------------------------------------
    Open log file to dump data
    ---------------------------------------------*/
    char logFilename[300];
    sprintf( logFilename, "%s/log%s.txt", directory.c_str(), buf2);
    //sprintf( logFilename, "log%s.txt", buf2);
    logFile.open(logFilename, std::fstream::out);

    if(logFile.is_open()) {
        return true;
    } else {
        return false;
    }
}

//closes the log file
bool DataloggerAlt::stop()
{
    logFile.close();
    return true;
}

//writes a line to the log file
void DataloggerAlt::logMessage(std::string line)
{
    logFile << line << std::endl;
}
