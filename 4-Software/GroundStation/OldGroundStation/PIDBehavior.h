#ifndef PIDBEHAVIOR_H
#define PIDBEHAVIOR_H

#include "Datalogger.h"
//#include "DataloggerAlt.h"
#include <string>
#include <BehaviorModule.h>
#include <MCTStructures.h>

class PIDBehavior : public BehaviorModule
{
protected:
    MCTUAV currUAV;
    MCTPIDERR errLat;
    MCTPIDERR errLong;
    MCTPIDERR errAlt;
    MCTPIDERR errPitch;
    MCTPIDERR errRoll;
    MCTPIDERR errYaw;
    std::string modelName;

    MCTorientation targetOrientation;
    double targetDirection;

public:
    PIDBehavior();
    ~PIDBehavior();

    //extended methods
    bool getCorrections(double *sigCor, MCTorientation curOri, double currDir, MCTposition curPos, MCTposition targetPos, double targetDir);
    bool populateConstants();
    bool initialize();

    //PID specific methods
    double get_PID_correction(double current, double target, MCTPIDERR *err, MCTPID pid);
    double get_PID_correction_verbose(double current, double target, MCTPIDERR *err, MCTPID pid);
    void calc_orientation(MCTposition cpos, double cdir, MCTposition tpos, double tdir);
    //PID read write methods
    void read_PID_model(std::string filename);
    void write_PID_model(std::string filename, MCTUAV model);

    bool write_log_header(Datalogger *log);

};

#endif // PIDBEHAVIOR_H
