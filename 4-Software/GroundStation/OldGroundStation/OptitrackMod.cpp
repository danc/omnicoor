#include "OptitrackMod.h"
#include "Module.h"
#include "MCTUtilities.h"
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"
#include "quat.h"

#include <iostream>

static void VRPN_CALLBACK handle_pos1 (void *ori, const vrpn_TRACKERCB t)
{
    MCTorientation *temp = (MCTorientation *) ori;
    char stats[100];

    //q_vec_type pitchYawRoll;
    //q_to_euler(pitchYawRoll, t.quat);
    q_vec_type yawPitchRoll;
    q_to_euler(yawPitchRoll, t.quat);
    temp->pitch = yawPitchRoll[1];
    temp->roll = yawPitchRoll[2];
//    if( pitchYawRoll[0] < DEG_TO_RAD(25) &&
//        pitchYawRoll[0] > -DEG_TO_RAD(25)) {
//            temp->pitch = pitchYawRoll[0];
//    }
//    if( pitchYawRoll[2] < DEG_TO_RAD(25) &&
//        pitchYawRoll[2] > -DEG_TO_RAD(25)) {
//            temp->roll = pitchYawRoll[2];
//    }
/*    if( yawPitchRoll[1] < DEG_TO_RAD(25) &&
        yawPitchRoll[1] > -DEG_TO_RAD(25)) {
            temp->pitch = yawPitchRoll[1];
    }
    if( yawPitchRoll[2] < DEG_TO_RAD(25) &&
        yawPitchRoll[2] > -DEG_TO_RAD(25)) {
            temp->roll = yawPitchRoll[2];
    }
*/
}
static void VRPN_CALLBACK handle_pos2 (void *pos, const vrpn_TRACKERCB t)
{
    MCTposition *temp = (MCTposition *) pos;
    //temp->altitude = (double)  t.pos[1];
    //temp->longitude = (double) t.pos[2];
    //temp->latitude = (double) t.pos[0];
    temp->altitude = (double)  -t.pos[2];
    temp->longitude = (double) t.pos[1];
    temp->latitude = (double) t.pos[0];
}
static void VRPN_CALLBACK handle_pos3 (void *dir, const vrpn_TRACKERCB t)
{
    double *temp = (double *) dir;

    //q_vec_type pitchYawRoll;
    //q_to_euler(pitchYawRoll, t.quat);
    q_vec_type yawPitchRoll;
    q_to_euler(yawPitchRoll, t.quat);
    //*temp = -pitchYawRoll[1];
    *temp = yawPitchRoll[0];

    //std::cout<< pitchYawRoll[1] << " to " << -(RAD_TO_DEG(pitchYawRoll[1])) <<std::endl;

}

OptiTrack::OptiTrack() : Module("OptiTrack-VRPN")
{
    modPosition.GPSalt = -1000.0;
    modPosition.GPSlat = -1000.0;
    modPosition.GPSlong = -1000.0;
}

OptiTrack::~OptiTrack()
{

}

bool OptiTrack::start()
{
    //open vrpn connection
    char connectionName[128];
    //sprintf(connectionName,"co3050-07.ece.iastate.edu:3883");


    //sprintf(connectionName,"192.168.1.100:3883");
    sprintf(connectionName,"192.168.0.120:3883");
    //sprintf(connectionName,"192.168.0.121:3883");

    //sprintf(connectionName,"129.186.158.48:3883");
    connection = vrpn_get_connection_by_name(connectionName);
    /*
    if(!connection->connected()) {
        std::cout << "No connection" << std::endl;
        return false;
    }
    */
    char constName[128];
    sprintf(constName,"UAV");//change this string to name of constellation
    tracker = new vrpn_Tracker_Remote(constName,connection);  

    tracker->register_change_handler((void*)&modOrientation, handle_pos1);
    tracker->register_change_handler((void*)&modPosition, handle_pos2);
    tracker->register_change_handler((void*)&modDirection, handle_pos3);

    //small wait for the preliminary values to be read and written locally
    usleep(10000);

    //get initial values
    connection->mainloop();
    tracker->mainloop();
    return true;
}

bool OptiTrack::update()
{
    connection->mainloop();
    tracker->mainloop();
    normalize_Orientation(&modOrientation);
}

bool OptiTrack::stop()
{

}
