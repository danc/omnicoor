/*---------------------------------------------------------
 File: ballMath.h

 --------------------------------------------------------*/
#ifndef BALL_MATH_H_
#define BALL_MATH_H_

/*---------------------------------------------------------
INCLUDES
---------------------------------------------------------*/
/*---------------------------------------------------------
DEFINITIONS
---------------------------------------------------------*/

/*---------------------------------------------------------
TYPES
---------------------------------------------------------*/
//! Structure used with the single camera module.
typedef struct {
	float radius;
	float x;
	float y;
}ball_info_t;

//! Structure used with the single camera module.
typedef struct {
	float X;
	float Y;
	float Z;
}ball_Location;

//! Structure used with the single camera module.
typedef struct {
	float timestamp;
        ball_info_t ball1;
        ball_info_t ball2;
        ball_info_t ball3;
}ball_frame_data_t;

//! Structure used with the single camera module.
typedef struct {
    float Pitch;
    float Roll;
}PitchRoll;
/*---------------------------------------------------------
GLOBAL VARIABLES
---------------------------------------------------------*/
/*---------------------------------------------------------
MACROS
---------------------------------------------------------*/
/*---------------------------------------------------------
PROCEDURES
---------------------------------------------------------*/

//! Used with the single camera module.
ball_frame_data_t	ballSortFrame( ball_frame_data_t unsorted );

//! Used with the single camera module.
void			swapBalls( ball_info_t* one, ball_info_t* two );

//! Used with the single camera module.
float                   ballgetDistance( ball_info_t one, ball_info_t two );

//! Used with the single camera module.
float                   ballgetDirection( ball_frame_data_t sorted );

//! Used with the single camera module.
ball_Location           ballgetLocation( ball_frame_data_t sorted );

//! Used with the single camera module.
PitchRoll               ballgetPitchRoll( float set_X, float set_Y, float current_X, float current_Y, float heading);
#endif

