#include "FourchanTrainMod.h"
#include "MCTUtilities.h"
#include "Trainer.h"

FourChanTrain::FourChanTrain() : Module("4-Channel Trainer")
{
    //create trainer interface to ground FPGA board with min and max signal values
    train = new Trainer(0,1312);

    //Parameters for sending integer value to ground FPGA
    // MID_?? signifies the nuetral value sent by transmitter for that signal
    // CHANGE_?? signifies the change of the value sent in respect to % change of signal in controller
    // MIN and MAX of throttle are defined as well

    // The MID_? values were found by measuring the ppm of the transmitter and converting to the integer
    MAX_THROTTLE = 800;
    MAX_THROTTLE = 600;
    CHANGE_THROTTLE = 2;
    MIN_THROTTLE = 170;
    MID_PITCH = 656;
    CHANGE_PITCH = 2;
    MID_ROLL = 656;
    CHANGE_ROLL= 2;
    MID_YAW = 656;
    CHANGE_YAW = 2;


    thrtVal=0;
    rollVal=0;
    yawVal=0;
    pitchVal=0;

    thrtTrim = 0;
    rollTrim = 0;
    pitchTrim = 0;
    yawTrim = 0;
}

FourChanTrain::~FourChanTrain()
{
    delete train;
}

bool FourChanTrain::setModValues(double *data, int size)
{
    if (size == 8) {
        thrtVal = data[0];
        pitchVal = data[1];
        rollVal = data[2];
        yawVal = data[3];
        thrtTrim = data[4];
        rollTrim = data[5];
        pitchTrim = data[6];
        yawTrim = data[7];
        return true;
    } else {
        return false;
    }
}
bool FourChanTrain::getModValues(double *data, int size)
{
    if(size == 4) {
        data[0] = CHANGE_THROTTLE;
        data[1] = CHANGE_PITCH;
        data[2] = CHANGE_ROLL;
        data[3] = CHANGE_YAW;
        return true;
    } else {
        return false;
    }
}

bool FourChanTrain::start()
{
    return train->initFPGA();
}

bool FourChanTrain::stop()
{
    train->setAllChannel(0, (unsigned short) MID_ROLL, (unsigned short) MID_PITCH,(unsigned short) MID_YAW);
    train->closeFPGA();
    return true;
}

bool FourChanTrain::update() //Insert matrix here ****
{
    unsigned short freq_throttle,freq_roll,freq_pitch,freq_yaw;
    freq_throttle = (unsigned short) ((thrtVal*CHANGE_THROTTLE) + MIN_THROTTLE);
    if(freq_throttle > MAX_THROTTLE)
        freq_throttle = (unsigned short) (MAX_THROTTLE);

    freq_roll = (unsigned short) ( ((rollVal-50)*CHANGE_ROLL)+MID_ROLL);
    freq_pitch = (unsigned short) ( ((pitchVal-50)*CHANGE_PITCH)+MID_PITCH);
    freq_yaw = (unsigned short) ( ((yawVal-50)*CHANGE_YAW)+MID_YAW);

    train->setAllChannel(freq_throttle,freq_roll,freq_pitch,freq_yaw);
    return true;
}

