#ifndef TRAINER_H
#define TRAINER_H




#include "MCTStructures.h"

class Trainer
{
protected:
    int fpga_fd;
    unsigned short MAX_CHANNEL_VAL;
    unsigned short MIN_CHANNEL_VAL;


public:
    Trainer(int, int);
    ~Trainer();
    bool initFPGA();
    void setChannel(char, unsigned short);
    void setAllChannel(unsigned short,unsigned short,unsigned short,unsigned short);
    void closeFPGA();
};

#endif // TRAINER_H
