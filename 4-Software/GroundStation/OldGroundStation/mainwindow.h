#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QGraphicsView>
#include "MCTController.h"
#include "CommandQueue.h"

namespace Ui {
class MainWindow;
}

/** @brief Class containing the user interface logic.
  * @todo Make this code follow SOLID principles better.
  * http://en.wikipedia.org/wiki/SOLID_(object-oriented_design)
  * @todo Move the auto take off and landing to a new class called subroutines.
  * This way we don't start bloating this code when we add more exotic
  * behaviors. (single responsibility)
  * @todo Move the other control related code somewhere else.  It probably
  * doesn't belong in the ui class either.
  * @bug Sometime the manually set throttle value doesn't reset when the stop
  * button is pressed.  That happens is that the next time the start button
  * is pressed the quadcopter will start flight with a higher than expected
  * throttle causing it to jump up and crash.
  */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(MCTController *cont, MCTController *secondary, QWidget *parent = 0);
    ~MainWindow();
    void printMessage(QString line);

private slots:
    void on_cBox_Mod_Ori_currentIndexChanged(const QString &arg1);
    void on_cBox_Mod_Pos_currentIndexChanged(const QString &arg1);
    void on_cBox_Mod_Dir_currentIndexChanged(const QString &arg1);
    void on_cBox_Mod_Comm_currentIndexChanged(const QString &arg1);
    void on_cBox_Mod_Cont_currentIndexChanged(const QString &arg1);
    void on_cBox_Bhvr_currentIndexChanged(const QString &arg1);
    void on_cBox_BhvrModel_currentIndexChanged(const QString &arg1);

    void on_pButton_IO_CancelCmds_clicked();
    void on_pButton_IO_ImportFile_clicked();
    void on_pButton_IO_Send_clicked();

    void on_pButton_Main_Start_clicked();
    void on_pButton_Main_Stop_clicked();

    void on_pButton_Take_Off_clicked();
    void on_pButton_Land_clicked();

    void throttleUp();
    void throttleDown();

    void on_rButton_Sel_Thr_Auto_clicked();
    void on_rButton_Sel_Thr_Man_clicked();
    void on_rButton_Sel_Pit_Auto_clicked();
    void on_rButton_Sel_Pit_Man_clicked();
    void on_rButton_Sel_Roll_Auto_clicked();
    void on_rButton_Sel_Roll_Man_clicked();
    void on_rButton_Sel_Yaw_Auto_clicked();
    void on_rButton_Sel_Yaw_Man_clicked();

    void gridSetup();
    //Buttons for the Settings tab
    void on_Settings_Grid_X_editingFinished();
    void on_Settings_Grid_Y_editingFinished();
    void on_Settings_Grid_Z_editingFinished();
    void on_Settings_Grid_Queue_Interval_editingFinished();
    void sendCommand();

    //Buttons for the Grid tab
    void on_Grid_Toolbar_Clear_clicked();

    /** @bug The save dialog doesn't load if the MCTController is running.*/
    void on_Grid_Toolbar_Save_clicked();

    void on_cBox_Mod_TrackOmni_clicked();
    void on_cBox_Mod_FollowOmni_clicked();
    void setOmniToTarget();

    /** @bug The load dialog doesn't load if the MCTController is running. */
    void on_Grid_Toolbar_Load_clicked();
    void on_Grid_Toolbar_Send_clicked();
    void on_Grid_Toolbar_Reset_clicked();
    void updateGrid();

    void updateController();

    void keyPressEvent(QKeyEvent* event);

private:
    Ui::MainWindow *ui;
    MCTController *control;
    MCTController *second;
    QTimer *timer;
    QGraphicsScene *grid_graphics_scene;
    CommandQueue *Command_Queue;
    double avgTime;
    QStringList cmdQueue;
    int currentCmd;
    double groundLevel;
    int groundFlag;
    void syncController();
    void updateFS();
    void updateManSelect();
    void updateStatus();
};

#endif // MAINWINDOW_H
