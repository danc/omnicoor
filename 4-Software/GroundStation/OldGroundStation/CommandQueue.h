#ifndef COMMANDQUEUE_H
#define COMMANDQUEUE_H

#include "MCTController.h"
#include "MCTStructures.h"
#include <QObject>
#include <vector>
#include <string>

/** @brief Contains and automatically sends queued commands.
  * @details This command queue exists as an object within the mainwindow class
  * and stores a pointer to the MCTController class.  You can give it a list of
  * positions and it will send those positions to the MCTController one at a
  * time at specified intervals.  The queue currently runs separate from the
  * controller and will attempt to send commands even when the controller is not
  * running. Currently, the queue only serves the GridGraphicsView class and
  * nothing else. In the future, it may be better to integrate a queuing
  * functionality directly into the MCTController class.
  * @todo Allow the queue to be paused and resumed
  * @todo Allow non-movement commands to be queued (eg: takeoff and land)
  * @todo Find a way to make this work without a QTimer.  Using QTimer
  * ties us to QT and I don't think this function should rely on the
  * QT framework.  Also, everyone loves interrupts.
  */
class CommandQueue : public QObject
{
    Q_OBJECT //Run qmake if you are getting unusual errors.
public:
    //! The microcart controller that controls the quadcopter.
    MCTController *control;

    /** @brief The time to wait in between sending commands.
      * @details If this is updates while the que has commands queued up,
      * This interval timing will take affect starting after the next command
      * is send to the MCTController
      */
    int command_interval_ms;

    /** @brief Default constructor for CommandQueue.
      * @details The command_interval is 100 ms by default and the state is
      * initially STATE_IDLE.
      */
    CommandQueue();

    /** @brief Default destructor for CommandQueue.
      */
    ~CommandQueue();

    /** @brief Stops execution of the queue and clears all contained commands.
      * @details After execution, the queue's state will be STATE_IDLE and the
      * queue will contain no Position elements.
      * @return True if successful.
      */
    bool clear();

    /** @brief Starts a paused queue.
      * @return True if successful.
      */
    bool start();

    /** @brief Checks the state of the queue.
      * @return Enumerator of the state of the queue.  Either STATE_IDLE or
      * STATE_STARTED.
      */
    STATE getQueueState();

    /** @brief Adds a single position to the end of the queue.
      * @details If the queue has not yet been started, start the queue.
      * @param[in] position Position structure containing x,y,z,heading values;
      * @return True if the position was added successfully.
      */
    bool addPosition(Position position);

    /** @brief Adds a vector of positions to the end of the queue.
      * @details If the queue has not yet been started, start the queue.
      * @param[in] positions Vector of Position structures containing x,y,z,
      * heading values;
      * @return True if the vector of positions was added successfully.
      */
    bool addPositions(std::vector<Position> positions);

private:
    //! Structure storing positions
    std::vector<Position> position_queue;

    //! State of the command queue.
    STATE state;

private slots:
    /** @brief Pops a command and sends it to MCTController at specified
     *  intervals.
      * @details If the queue is STATE_IDLE, no command will be sent.
      * If there are no items in the queue, no command will be sent.
      * @return False if no command was sent.  True otherwise.
      */
    bool sendPosition();
};

#endif // COMMANDQUEUE_H
