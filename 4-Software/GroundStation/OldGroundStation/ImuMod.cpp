#include "ImuMod.h"
#include "MCTUtilities.h"

#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <termios.h>


IMU::IMU() : Module("On-board Sensors")
{
    usb_id = "0403:6001";
    imu_pID = 0;
    KALMAN = 1;
}

IMU::~IMU()
{
    
}

bool IMU::start()
{
    int err, port;
    //check to see that the xbee wireless is connected
    if( get_USB_port(usb_id) == -1 ) {
        return false;
    }
    //find the file number of the xbee wireless USB
    for( port = 0; port < 7; port++) {
        if(check_xbee(port) == true) {
            break;
        }
    }
    if( port == 7 )
        return false;

    //for threaded imu calling
    //start the imu reader process with the detected file number

    imu_pID = fork();
    if( imu_pID == 0) {
        execl("ImuReader/imuReader","ImuReader/imuReader&", port, (char *)0);
        exit(0);
    }
    if( imu_pID < 0) {
        return false;
    }
    //small wait for the preliminary imu values to be read and written locally
    usleep(10000);

    /*
    //for direct IMU calling
    char device[30];
    sprintf( device, "/dev/ttyUSB%d", port );
    xbee_fd = open(device, O_RDWR | O_NDELAY);
    fcntl(xbee_fd, F_SETFL, 0);
    set_uart_options();
    */
    return true;
}

bool IMU::stop()
{
    //close the xbee file descriptor and kill the started imu reader process
    close(xbee_fd);
    if(imu_pID > 0) {
        kill(imu_pID,SIGTERM);
    }
    return true;
}

bool IMU::update()
{


    //for threaded imu calling
    float kal_roll,kal_pitch;
    int sensor[9];

    FILE *imuFile = fopen ("single_imu_data.txt" , "r");

    if (imuFile == NULL) {
        std::cout << "IMU: no file" << std::endl;
        return false;
    } else {
        int scanfRet = fscanf(imuFile, "%f %f %d %d %d %d %d %d %d %d %d",
                              &kal_roll, &kal_pitch,
                              &sensor[0], &sensor[1], &sensor[2],
                              &sensor[3], &sensor[4], &sensor[5],
                              &sensor[6], &sensor[7], &sensor[8] );
        if(scanfRet == EOF) {
            std::cout << "IMU:end of file" << std::endl;
            return false;
        }
    }
    std::cout  << kal_pitch << " " << kal_roll << std::endl;
    modOrientation.pitch = DEG_TO_RAD( (double) kal_pitch);
    modOrientation.roll = DEG_TO_RAD( (double) kal_roll );

    return true;
}

bool IMU::check_xbee(int p)
{
    int n;
    char device[30];
    sprintf( device, "/dev/ttyUSB%d", p );
    xbee_fd = open(device, O_RDWR | O_NDELAY);
    //fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
    if (xbee_fd == -1) {
        return false;
    } else {
        fcntl(xbee_fd, F_SETFL, 0);
        set_uart_options();
    }
    char payload[100];
    short int *sensor = (short int*)payload;
    n = write(xbee_fd, "b", 1);
    if (n < 1) {
        return false;
    }
    read_uart(payload,26);
    close(xbee_fd);
    if (sensor[9] == 6 ) {
        return true;
    } else {
        return false;
    }
}

bool IMU::read_uart(char *payload, int length)
{
    int i;

    char *buf = payload;

    // Read from UART one byte at a time
    for(i=0; i<length; i++){
        if(read(xbee_fd, (void *)buf, 1) == -1)
            return false;
        buf = buf++;
    }
}

void IMU::set_uart_options(){
    struct termios options; // UART port options data structure

    // Get the current options for the port...
    tcgetattr(xbee_fd, &options);

    // Set the baud rates to 9600...
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);

    // Enable the receiver and set local mode...
    options.c_cflag |= (CLOCAL | CREAD);

    // Set charater size
    options.c_cflag &= ~CSIZE; // Mask the character size bits
    options.c_cflag |= CS8;    // Select 8 data bits

    // Set no parity 8N1
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;

    // Disable Hardware flow control
    options.c_cflag &= ~CRTSCTS;

    // Use raw input
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

    // Disable SW flow control
    options.c_iflag &= ~(IXON | IXOFF | IXANY);

    // Use raw output
    options.c_oflag &= ~OPOST;

    // Set new options for the port...
    tcsetattr(xbee_fd, TCSANOW, &options);
}
