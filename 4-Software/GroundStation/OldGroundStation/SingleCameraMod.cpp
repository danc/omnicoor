  #include <sys/types.h>

#include "SingleCameraMod.h"
#include "Module.h"
#include "ballMath.h"

#include <cstdlib>
#include <cstdio>
#include <fcntl.h>

SingleCamera::SingleCamera() : Module("Single Camera")
{

}

SingleCamera::~SingleCamera()
{
    
}

bool SingleCamera::start()
{
    /*---------------------------------------------
       Create ballReader process
       ---------------------------------------------*/
    int cam_pID = fork();
    if( cam_pID < 0) {
        return false;
    } else if( cam_pID == 0) {
        execl("BallReader/ballReader","BallReader/ballReader&", (char *)0);
    }
    return true;
}

bool SingleCamera::update()
{
    //get current values from camera file
    int ball_Size[3], ball_X[3], ball_Y[3];
    ball_frame_data_t unsorted, sorted;
    float ballHeading;
    ball_Location location;

    FILE *ballFile = fopen ("single_ball_data" , "r");
    if (ballFile == NULL) {
        return false;
    } else {
        int scanfRet = fscanf(ballFile, "%i %i %i %i %i %i %i %i %i",
                                    &ball_Size[0], &ball_X[0], &ball_Y[0],
                                    &ball_Size[1], &ball_X[1], &ball_Y[1],
                                    &ball_Size[2], &ball_X[2], &ball_Y[2] );
        if ( scanfRet == EOF ) {
            return false;
        }
        fclose(ballFile);
    }


    unsorted.timestamp=1;
    unsorted.ball1.radius=(float)ball_Size[0];
    unsorted.ball1.x=(float)ball_X[0];
    unsorted.ball1.y=(float)ball_Y[0];
    unsorted.ball2.radius=(float)ball_Size[1];
    unsorted.ball2.x=(float)ball_X[1];
    unsorted.ball2.y=(float)ball_Y[1];
    unsorted.ball3.radius=(float)ball_Size[2];
    unsorted.ball3.x=(float)ball_X[2];
    unsorted.ball3.y=(float)ball_Y[2];

    sorted=ballSortFrame(unsorted);
    ballHeading=ballgetDirection(sorted);
    location=ballgetLocation(sorted);
    //take this information and save it in new MCTstructure format
    modDirection = (double) ballHeading;

}

bool SingleCamera::stop()
{
    //kill external program
    //return kill(cam_pID, SIGTERM);
}
