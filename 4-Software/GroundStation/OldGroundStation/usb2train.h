#ifndef USB2TRAIN_H
#define USB2TRAIN_H


#include "Module.h"
#include <string>

class USB2Train : public Module
{
protected:
    std::string usb_id;
    int rc_fd;
    int axis[5];
    char button[2];
    unsigned char axes;
    unsigned char buttons;

    double thrtVal;
    double rollVal;
    double yawVal;
    double pitchVal;

    //Ranges for joystick movement of this USB device
    double IL_ROLL_NEUTRAL;
    double IL_ROLL_RANGE;
    double IL_PITCH_NEUTRAL;
    double IL_PITCH_RANGE;
    double IL_THROTTLE_NEUTRAL;
    double IL_THROTTLE_RANGE;
    double IL_YAW_NEUTRAL;
    double IL_YAW_RANGE;


public:
    USB2Train();
    ~USB2Train();
    bool isContMod() {return true;}
    bool getModValues(double *data, int size);
    bool start();
    bool update();
    bool stop();
};

#endif // USB2TRAIN_H
