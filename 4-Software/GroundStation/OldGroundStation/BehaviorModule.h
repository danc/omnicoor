#ifndef BEHAVIORMODULE_H
#define BEHAVIORMODULE_H

#include <string>
#include <vector>
#include "MCTStructures.h"
#include "Datalogger.h"
#include "DataloggerAlt.h"


/** @brief Module for the specific behavior of the quadcopter and how it reacts to inputs from the camera system.
  *
  * @details This module takes the quadcopters orientation and position.
  * The module determines an action to take based on the inputs.
  * The module outputs values that relate to the control the quadcopter's pitch, yaw, roll, and throttle.
  */
class BehaviorModule
{
protected:
    //! The name of the module.
    std::string modName;

    //! A vector of profile names
    std::vector<std::string> profiles;

    //! A vector of profile directories
    std::vector<std::string> profiledir;

    //! The index in profiles and profiledir list of the currently active profile.
    unsigned int currIndex;

public:
    /**@brief Default constructor for BehaviorModule.
      */
    BehaviorModule();

    /**@brief Another constructor for BehaviorModule.
      */
    BehaviorModule(std::string name);

    /**@brief Desconstructor for BehaviorModule.
      */
    virtual ~BehaviorModule() {}

    /** @brief Gets the name of the module.
      * @return The name of the module.
      */
    std::string getName()   {return modName;}

    /** @brief Adds a profile to an internal vector of profiles.
      * @details Gets a name of a profile and a directory path.
      *          Checks if the profile or directory already exists within
      *          the profiles and profiledir vectors respectively.
      * @param[in] profileName String representation of the profile's name.
      * @param[in] fileName String representation of a file path.
      * @return True if the profile was added successfully.
      *         False if the profile has already exists within the vector
      */
    bool addProfile(std::string profileName, std::string fileName);

    /** @brief Gets a vector of all profile names
      *
      * @return Vector containing profile names.
      */
    std::vector<std::string> getModels() {return profiles;}

    /** @brief Sets a profile active.
      * @details Iterates throught the vector of profiles and compares strings.
      *          If a match is found, update currIndex to reflect the index of that profile.
      * @param[in] profileName The name of the profile to set to active.
      * @return True if successful.
      *         False if the profile could not be found
      */
    bool setProfile(std::string profileName);

    /** @brief Gets the correction for the controller based on values of the FPGA interface
      * @param[out] *sigCor Reference to an array to write output values to.
      *                     The values might correspond to [throttle, elevator, aileron, rudder] But I'm not sure.
      * @param[in]  curOri Values for the quadcopter's current orientation.  Contains latitude, longitude, altitude.
      * @param[in]  currDir The quadcopters current direction.
      * @param[in]  curPos Values for the quadcopter's current position.  Contains roll, pitch, yaw.
      * @param[in]  targetPos Values for the quacopter's target position.  Contains latitude, longitude, altitude.
      * @param[in]  targetDir The quadcopter's target direction.
      *
      * @return True is successful.
     **/
    virtual bool getCorrections(double *sigCor, MCTorientation curOri, double currDir, MCTposition curPos, MCTposition targetPos, double targetDir) {return false;}

    /** @brief Loads the currently active profile.
      * @return True if successful.
      */
    virtual bool populateConstants() {return false;}

    /** @brief Initializes the module.
      * @return True if successful.
      */
    virtual bool initialize() {return false;}

    /** @brief Writes information to a log file.
      * @param[out] *log Datalogger object
      * @return True if successful.
      */
    virtual bool write_log_header(Datalogger *log) {return false;}

    /** @brief Writes information to a log file for the 1-D testing system.
      * @param[out] *log DataloggerAlt object
      * @return True if successful.
      */
    
    virtual bool write_log_header(DataloggerAlt *log) {return false;}
    


};

#endif // BEHAVIORMODULE_H
