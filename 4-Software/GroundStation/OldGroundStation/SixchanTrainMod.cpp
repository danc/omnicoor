#include "SixchanTrainMod.h"
#include "Module.h"
#include "MCTUtilities.h"

 //Motors
  unsigned int Motor_1,Motor_2,Motor_3,Motor_4;
SixChanTrain::SixChanTrain() : Module("6-Channel Trainer")
{
     //create trainer interface to ground FPGA board with min and max signal values
    train = new Trainer(0,1312);

    //Parameters for sending integer value to ground FPGA
    // MID_?? signifies the nuetral value sent by transmitter for that signal
    // CHANGE_?? signifies the change of the value sent in respect to % change of signal in controller
    // MIN and MAX of throttle are defined as well

    // The MID_? values were found by measuring the ppm of the transmitter and converting to the integer
    MAX_THROTTLE = 1500;
    MIN_THROTTLE = 200;
    CHANGE_THROTTLE = 7;
    //MID_PITCH = 700;
    MID_PITCH = 656;
    CHANGE_PITCH = 6;
    //MID_ROLL = 640;
    MID_ROLL = 656; //900 used for expanded quad
    CHANGE_ROLL= 6;
    //MID_YAW = 756;
    MID_YAW = 656;
    CHANGE_YAW = 6;

    thrtVal=0;
    rollVal=50;
    yawVal=50;
    pitchVal=50;
    thrtTrim = 0;
    rollTrim = 0;
    pitchTrim = 0;
    yawTrim = 0;
    
   

    //variables for testing trainer glitch
    newRoll = 0;
    newPitch = 0;
    origRoll = 0;
    origPitch = 0;
    prevRoll = 0;
    prev2Roll = 0;
    prevNRoll = 0;
    prev2NRoll = 0;
    prevPitch = 0;
    prev2Pitch = 0;
    prevNPitch = 0;
    prev2NPitch = 0;
}

SixChanTrain::~SixChanTrain()
{
    delete train;
}

bool SixChanTrain::setModValues(double *data, int size)
{
    
    /*
    if(size == 8) {
        thrtVal = data[0];
        pitchVal = data[1];
        rollVal = data[2];
        yawVal = data[3];
        thrtTrim = data[4];
        pitchTrim = data[5];
        rollTrim = data[6];
        yawTrim = data[7];
        return true;
    } else {
        return false;
    }
    
    */
     
     if(size == 8) {
        Motor_1 = data[0];
        Motor_2 = data[1];
        Motor_3 = data[2];
        Motor_4= data[3];
        
        std::cout << "M1_before:" << Motor_1  << " M3_before:" << Motor_3 << std::endl;
        
        
        
        thrtTrim = 0;
        pitchTrim = 0;
        rollTrim = 0;
        yawTrim = 0;
        
        /********** Check to avoid saturation *******************/
       
    
        /********** End of Check *******************/
        
        /***********Scale the motors value ********/ 
        
        
        
        Motor_1 = Motor_1 *13; 
        Motor_3 = Motor_3 *13; 
        
        return true;
        
    } else {
        return false;
    }
    
    
    
}

// This is vitual in module, but only handled in sixchantrain (the most modern control)
bool SixChanTrain::getAllModValues(double *data, int size)
{
    if(size == 8) {
        data[0] = thrtVal;//0
        data[1] = pitchVal;//50
        data[2] = rollVal;//50
        data[3] = yawVal;//50
        data[4] = thrtTrim;//0
        data[5] = pitchTrim;//0
        data[6] = rollTrim;//0
        data[7] = yawTrim;//0
        return true;
    } else {
        return false;
    }
}


bool SixChanTrain::getModValues(double *data, int size)
{
    if(size == 4) {
        data[0] = CHANGE_THROTTLE; ///value = 7
        data[1] = CHANGE_PITCH;///value = 6
        data[2] = CHANGE_ROLL;///value = 6
        data[3] = CHANGE_YAW;///value = 6
        return true;
    } else {
        return false;
    }
}

bool SixChanTrain::start()
{
    newRoll = 0;
    newPitch = 0;
    origRoll = 0;
    origPitch = 0;
    prevRoll = 0;
    prev2Roll = 0;
    prevNRoll = 0;
    prev2NRoll = 0;
    prevPitch = 0;
    prev2Pitch = 0;
    prevNPitch = 0;
    prev2NPitch = 0;
    //must be set for specific computer
    return train->initFPGA();
}

bool SixChanTrain::stop()
{
    train->setAllChannel(0, (unsigned short) MID_ROLL, (unsigned short) MID_PITCH,(unsigned short) MID_YAW);
    train->closeFPGA();
    return true;
}

bool SixChanTrain::update()
{
    //double temp;
     /*
    unsigned short freq_throttle,freq_roll,freq_pitch,freq_yaw;
    freq_throttle = (unsigned short) ((thrtVal*CHANGE_THROTTLE) + MIN_THROTTLE) + thrtTrim;
    if(freq_throttle > MAX_THROTTLE)
        freq_throttle = (unsigned short) (MAX_THROTTLE);
    
   
    origRoll = ( ( (MID_ROLL + ((rollVal-50.0)*CHANGE_ROLL))) + rollTrim);

    temp = ( ( origRoll - (1.968*prevRoll) + (0.9695*prev2Roll) + (2.021*prevNRoll) - (0.9531*prev2NRoll) ) / 1.069);
    std::cout << "Orig: " << origRoll << "   New: " << temp << std::endl;

    prev2Roll = prevRoll;
    prevRoll = origRoll;
    prev2NRoll = prevNRoll;
    prevNRoll = temp;

    freq_roll = (unsigned short) temp;

    origPitch = ( ( (MID_PITCH - ((pitchVal-50.0)*CHANGE_PITCH))) + pitchTrim);

    temp = ( ( origPitch - (1.968*prevPitch) + (0.9695*prev2Pitch) + (2.021*prevNPitch) - (0.9531*prev2NPitch) ) / 1.069 );
    prev2Pitch = prevPitch;
    prevPitch = origPitch;
    prev2NPitch = prevNPitch;
    prevNPitch = temp;

    freq_pitch = (unsigned short) temp;
    //
     
    freq_roll = (unsigned short)   (((MID_ROLL + ((rollVal-50.0)*CHANGE_ROLL))) + rollTrim);
    freq_pitch = (unsigned short) (((MID_PITCH - ((pitchVal-50.0)*CHANGE_PITCH))) + pitchTrim);
    freq_yaw = (unsigned short)   (((MID_YAW + ((yawVal-50.0)*CHANGE_YAW))) + yawTrim);
   
    unsigned short Motor_1 = freq_throttle - freq_pitch; 
    unsigned short Motor_2 = freq_throttle - freq_roll - freq_pitch - freq_yaw; //This excuation gives a negative value, becuase it is unsigned TODO
    unsigned short Motor_3 = freq_throttle + freq_roll - freq_pitch + freq_yaw; 
    unsigned short Motor_4 = freq_throttle + freq_roll + freq_pitch - freq_yaw; 
   */
  
   
   
    //std::cout << "Cam Pitch:" << freq_throttle << " " << freq_pitch << " " << freq_roll << " " << freq_yaw << std::endl;
    //std::cout << freq_throttle << " " << freq_pitch << " " << freq_roll << " " << freq_yaw << std::endl;
    //std::cout << "M1:" << Motor_1 << " M2:" << Motor_2 << " M3:" << Motor_3 << " M4:" << Motor_4 << std::endl;
    //train->setAllChannel(thrtVal,rollVal,pitchVal,yawVal); //Send the ppm vlaues to the FPGA


   //Motor_1 = Motor_1 * 0.85; 
   //Motor_3 = Motor_3 * 0.80;
    
	//Motor_1 = 0;
   //Motor_2 = 0;
   //Motor_3 = 400;
   //Motor_4 = 0;
   
	 std::cout << "M1:" << Motor_1 << " M2:" << Motor_2 << " M3:" << Motor_3 << " M4:" << Motor_4 << std::endl;
    
   
  
    train->setAllChannel(Motor_1,Motor_2,Motor_3,Motor_4); //Send the ppm vlaues to the FPGA



    return true;
}
