#ifndef SIXCHANTRAIN_H
#define SIXCHANTRAIN_H

#include "Module.h"
#include "Trainer.h"

/** @brief Module for communicating via a 6 channel trainer controller.
  */
class SixChanTrain : public Module
{
private:
    Trainer *train;

    double MAX_THROTTLE;
    double CHANGE_THROTTLE;
    double MIN_THROTTLE;
    double MID_PITCH;
    double CHANGE_PITCH;
    double MID_ROLL;
    double CHANGE_ROLL;
    double MID_YAW;
    double CHANGE_YAW;

    double thrtVal;
    double rollVal;
    double yawVal;
    double pitchVal;

    double thrtTrim;
    double rollTrim;
    double pitchTrim;
    double yawTrim;

    //variables for testing trainer glitch
    double newRoll;
    double newPitch;
    double origRoll;
    double origPitch;
    double prevRoll;
    double prev2Roll;
    double prevNRoll;
    double prev2NRoll;
    double prevPitch;
    double prev2Pitch;
    double prevNPitch;
    double prev2NPitch;


public:
    /** @brief default constructor for SixchanTrainMod
      */
    SixChanTrain();

    /** @brief default destructor for SixchanTrainMod
      */
    ~SixChanTrain();

    /** @brief Checks if this module is responsible for communicating with the quadcopter.
      * @return True
      */
    bool isCommMod() {return true;}

    /** @brief Sets specific values to the module.
      * @param[in] data Pointer to an array of doubles corresponding to
      *                 throttle,
      *                 pitch,
      *                 roll,
      *                 yaw,
      *                 pitchtrim,
      *                 rolltrim,
      *                 yawtrim.
      *                 Throttle etc values are in %, trim values are an offset.
      * @param[in] size The size of the data array.  This value must be 8.
      * @return False if the size is not 8.  True otherwise.
      */
    bool setModValues(double *data, int size);

    bool getAllModValues(double *data, int size);
    /** @brief Gets specific values to the module.
      * @param[out] data Pointer to an array of double to write data to.
      * @param[in]  size The size of the data array.  This value must be 8.
      * @return False if the size is not 8.  True otherwise
      */
    bool getModValues(double *data, int size);

    /** @brief Starts the module.
      * @details Sets internal pitch, roll, yaw values to 0.
      * @return True if successful.
      */
    bool start();

    /** @brief Starts the module.
      * @details Sets internal pitch, roll, yaw values to 0.
      * @return True if successful.
      */
    bool stop();
    bool update();
    
    
};

#endif // SIXCHANTRAIN_H
