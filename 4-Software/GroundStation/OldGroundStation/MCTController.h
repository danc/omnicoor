#ifndef MCTCONTROLLER_H
#define MCTCONTROLLER_H

#include "Module.h"
#include "MCTStructures.h"
#include "MCTUtilities.h"
#include "Datalogger.h"
#include "DataloggerAlt.h"
#include "BehaviorModule.h"
#include "PIDBehavior.h"

#include <vector>
#include <string>


/** @deprecated Use STATE.
  * @brief Enumerator for the state of the MCTController class.
  */
enum CONTROLLER_STATUS {RUNNING,IDLE};


/** @brief Configures and controls the flight of the quadcopter.
  * @details This class is the effective brains of the quadcopter.  Each
  * instance when correctly configured should be able to control one quadcopter.
  * The modules used in this class are for getting, manipulating, and sending
  * data to the quadcopter.
  * @todo Make this code follow SOLID principles better.
  * http://en.wikipedia.org/wiki/SOLID_(object-oriented_design)
  * @todo Move command parsing to a different class. (single responsibility)
  * @todo Move whatever organizes and keeps track of all modules to a different
  * class. (single responsibility)
  * @bug The target postion structure does not accurately represent the actual
  * target position when using the PID controller. For example, when using the
  * PID controller the behavior of the quadcopter will not be to reduce the
  * distance between the current position and the target position.  The
  * quadcopter will appear to reduce the distance between the current position
  * and a predictable offset from the target position.  The offset is very
  * roughly the offset from the origin to the location at which the quadcopter
  * attains stable flight when taking off.  I haven't tested if this bug exists
  * with the SSBehavior module.
  */
class MCTController
{
public:
    /** @brief Default constructor for MCTController class.
      */
    MCTController();

    /** @brief Decault destructor for MCTController class.
      */
    ~MCTController();

    /** @brief Adds a module to the controller.  This allows the controller to
      * access and use that module.
      * @details Modules should be added in the main function.
      * @param[in] *module Pointer to a module class.
      * @return True if the module was successfully added.
      */
    bool addModule(Module *module);

    /** @brief Sets a module to active.  This module will be used by the
      * MCTController during operation.
      * @details Only one module of each enum type may be active at a time.
      * @param[in] type The type of module that is being set to active.
      * @param[in] name The name of the module that is being set.
      * @return True if the module was successfully set as active.
      */
    bool setModule(enum MODULE_TYPE type, std::string name);

    /** @brief Gets a vector of all module names of a specified type.
      * @param[in] type Enumerator of a module type.
      * @return Vector of strings representing all module names of a specified
      * type.
      */
    std::vector<std::string> getAllModName(enum MODULE_TYPE type);

    /** @brief Sets the current behavior to use.
      * @param[in] behaviorName The name of the behavior to set.
      * @return True if the behavior was successfully set.
      */
    bool setBehavior(std::string behaviorName);

    /** @brief Sets the current behavior model to use.
      * @param[in] behaviorName The name of the behavior model to set.
      * @return True if the behavior model was successfully set.
      */
    bool setBehaviorModel(std::string behaviorName);

    /** @brief Gets a list of all behaviors.
      * @return Vector of strigs representing all behaviors.
      */
    std::vector<std::string> getBehaviors();

    /** @brief Gets a list of all behavior models.
      * @return Vector of strigs representing all behavior models.
      */
    std::vector<std::string> getBehaviorModels();

    /** @brief Sets the current behavior to use.
      * @return False if the controller is already running.
      *        False if an active module was not successfully started.
      *        True otherwise.
      *
      * @todo Change to int return and have it return error number.
      *      Add an error code method to controller to return a string
      *      corresponding to error number.
      */
    bool start();

    /** @brief Updates the internal state of the controller using the active
      * modules.
      * @details This function is the one that ties everything together and
      * makes the quadcopter fly.
      * @return Time to complete the update in milliseconds.
      */
    double update();

    /** @deprecated
      * @brief Sets the home point to the quadcopter's current position.
      * @note Does setting the home point actually do anything functional?
      *       As far as I can see, it is only used when displaying the
      *       quadcopter's location in non-absolute coordinates.
      *       It seems kind of useless.
      */
    void setHome();

    /** @brief Sets the current position of the omnibot to the target position
      * @param latitude : latitude of the omni-bot to use as target position of
      * the UAV
      * @param longitude : longitude of the omni-bot to use as target position
      * of the UAV
      * @return True if valid lat/lon values are provided as parameters
     **/
    bool setToOmni(double longitude, double latitude);

    /** @brief Stops the controller and all active moduels.
      */
    void stop();

    /** @brief Writes as string to the log file.
      * @param[in] line String to write to the log file.
      */
    void log(std::string line);

    //--------------------------------------//
    // functions for getting current status //
    //--------------------------------------//

    /** @brief Gets the current position of the quadcopter in absolute
      * coordinates.
      * @return Position structure containing x,y,z,heading,pitch,yaw,roll
      * values.
      */
    Position getCurrentPosition();

    /** @brief Gets the target position of the quadcopter in absolute
      * coordinates.
      * @return Position structure containin x,y,z,heading,pitch,yaw,roll
      * values.
      */
    Position getTargetPosition();

    /** @deprecated Use getCurrentPosition.
      * @brief Gets the current position of the quadcopter as a string.
      * @param[in] type String representation of the requested position.
      * @param[in] abso Absolute ot relative coordinates.
      * @return String representation of the command corresponding to type.
      */
    std::string getPosition(std::string type, bool abso);

    /** @deprecated Use getTargetPosition.
      * @brief Gets the target position of the quadcopter as a string.
      * @param[in] type String representation of the requested position.
      * @param[in] abso Absolute ot relative coordinates.
      * @return String representation of the command corresponding to type.
      */
    std::string getTarget(std::string type, bool abso);

    /** @deprecated Use getCurrentPosition.
      * @brief Gets the orientation of the quadcopter.
      * @param[in] type String representation of the requested orientation.
      * @return Orientation value corresponding to type in degrees.
      */
    double getOrientation(std::string type);

    /** @deprecated Use getCurrentPosition.
      * @brief Gets the heading of the quadcopter.
      * @return Heading in degrees.
      */
    double getDirection();

    /** @brief Parses a command and immediately executes it.
      * @param[in] command String command as it typed into a console.
      * @return True if the command was recognized and executed successfully.
      * @todo Change this function to use the functions in the fileParser class.
      *       Get rid of all the unnecessary string comparissons.
      *       This may marginally improve performance.
      */
    bool parseCommand(std::string command);

    /** @brief Changes the quadcopter's target position.
      * @param[in] new_target Position structure containing changes to x,y,z,
      * heading (meter, meter, meter, degree).
      * @return True if the new target was changed successfully.
      */
    bool adjustTarget(Position new_target);

    /** @brief Sets the quadcopter's target position.
      * @param[in] new_target Position structure containing changes to x,y,z,
      * heading (meter, meter, meter, degree).
      * @return True if the new target was set successfully.
      */
    bool setTarget(Position new_target);

    //-----------------------------------------------------------//
    // Getters and setters for manual or auto Quadcopter control //
    //-----------------------------------------------------------//

    void setYaw(bool autonom)       {autoYaw = autonom;}
    bool getYaw()                   {return autoYaw;}
    void setThrottle(bool autonom)  {autoThrottle = autonom;}
    bool getThrottle()              {return autoThrottle;}
    void setPitch(bool autonom)     {autoPitch = autonom;}
    bool getPitch()                 {return autoPitch;}
    void setRoll(bool autonom)      {autoRoll = autonom;}
    bool getRoll()                  {return autoRoll;}

    /** @brief Returns the current throttle value
      * @return Most recent throttle value for this controller
      */
    double getThrottleVal();

    /** @brief When manual on throttle, allows for internal control over the
      * throttle
      */
    void setThrottleVal(double throttleChange);

    /** @brief Gets an enumerator of the controller's status.
      * @return A CONTROLLER_STATUS enumerator.
      * @todo Make this return a STATE enumerator instead.
      */
    CONTROLLER_STATUS getStatus()   {return status;}

private:
    //! Keeps track of the controller's status.
    CONTROLLER_STATUS status;

    //! Keeps track of when the module started operation
    timespec run_start;

    //! Vector of all available modules.
    std::vector<Module*> *listMod;

    //! Pointer to the active orientation module.
    Module *currOriMod;

    //! Pointer to the active direction module.
    Module *currDirMod;

    //! Pointer to the active position module.
    Module *currPosMod;

    //! Pointer to the active communication module.
    Module *currCommMod;

    //! Pointer to the active control module.
    Module *currContMod;

    //! Pointer to the class used to write log files.
    Datalogger *logger;

	 //! Pointer to the class used to write log files for the 1_D testing system.
    DataloggerAlt *loggerAlt;
    
    //! Vector of all available behavior modules.
    std::vector<BehaviorModule*> *listBehavior;

    //! Pointer to the active behavior module.
    BehaviorModule *currBehavior;

    //! Is pitch control automatically handled by the controller.
    bool autoPitch;

    //! Is throttle control automatically handled by the controller.
    bool autoThrottle;

    //! Is roll controll automatically handled by the controller.
    bool autoRoll;

    //! Is yaw control automatically handled by the controller.
    bool autoYaw;

    //! tracks the last manual throttle signal
    double lastThrottle;

    /** @deprecated Setting a home point is currently useless.
      * @brief True if the home point has been set.
      */
    bool home;

    /** @deprecated Setting a home point is currently useless.
      * @brief Stores the home position of the quadcopter.
      */
    MCTposition homePosition;

    /** @brief Stores the target orientation of the quadcopter.
      * @todo Switch this to a Position.
      */
    MCTorientation targetOrientation;

    /** @brief Stores the target position of the quadcopter.
      * @todo Switch this to a Position.
      */
    MCTposition targetPosition;

    /** @brief Stores the target direction of the quadcopter.
      * @todo Switch this to a Position.
      */
    double targetDirection;

    //-------------------//
    // Control Functions //
    //-------------------//

    /** @deprecated Use adjustTarget.
      * @brief Changes the quadcopter's target position.
      * @param[in] longitude Change in the x direction in meters.
      * @param[in] latitude Change in the y direction in meters.
      * @param[in] altitude Change in the z direction in meters.
      * @param[in] direction Change in heading in degrees.
      * @return True if the new target was set successfully.
      */
    bool adjust_target(double longitude, double latitude, double altitude, double direction);

    /** @deprecated Use FileParser.h.
      * @brief Checks if the command was to change latitude.
      * @param[in] str String representation of the command to check.
      * @return True if the command was latitude.
      */
    bool check_latitude(std::string str);

    /** @deprecated Use FileParser.h.
      * @brief Checks if the command was to change longitude.
      * @param[in] str String representation of the command to check.
      * @return True if the command was longitude.
      */
    bool check_longitude(std::string str);

    /** @deprecated Use FileParser.h.
      * @brief Checks if the command was to change altitude.
      * @param[in] str String representation of the command to check.
      * @return True if the command was altitude.
      */
    bool check_altitude(std::string str);

    /** @deprecated Use FileParser.h.
      * @brief Checks if the command was to change direction.
      * @param[in] str String representation of the command to check.
      * @return True if the command was direction.
      */
    bool check_direction(std::string str);

    /** @deprecated Use FileParser.h.
      * @brief Checks if the command was to change position.
      * @param[in] str String representation of the command to check.
      * @return True if the command was position.
      */
    bool check_position(std::string str);

    /** @deprecated Use FileParser.h.
      * @brief Checks if the command was to set home.
      * @param[in] str String representation of the command to check.
      * @return True if the command was to set home.
      */
    bool check_sethome(std::string str);

    /** @brief Checks to see if the command is for tracking omnibot
      * @details This is an internal command to update the target location based
      * on the omnibot
      */
    bool check_omni(std::string str);
};

#endif // MCTCONTROLLER_H
