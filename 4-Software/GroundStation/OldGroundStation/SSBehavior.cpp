#include "SSBehavior.h"
#include <cmath>
#include <cstdlib>


SSBehavior::SSBehavior() : BehaviorModule("SF Controller")
{
    thrtCor;
     pitCor;
    rolCor;
    yawCor;

}

SSBehavior::~SSBehavior()
{

}

bool SSBehavior::initialize()
{
    clock_gettime(CLOCK_REALTIME, &init);
    mark = init;
    thrtCor = 0;
    pitCor = 0;
    rolCor = 0;
    yawCor = 0;
}

bool SSBehavior::getCorrections(double *sigCor, MCTorientation curOri, double curDir, MCTposition curPos, MCTposition targetPos, double targetDir)
{
    static double state[2] = {0,0};


    if(! (curOri.pitch == prevOri.pitch && curOri.roll == prevOri.roll &&
          curPos.altitude == prevPos.altitude && curPos.longitude == prevPos.longitude && curPos.latitude == prevPos.latitude) ) {

        state[0]++;

        int loop;
        timespec now;
        double timeslice;
        double dLat,dLong,dAlt,dRoll,dPitch,dYaw;
        //calculate derivative values of angles and position
        clock_gettime(CLOCK_REALTIME, &now);
        timeslice = ((double) (now.tv_sec - mark.tv_sec)) + ((double) (now.tv_nsec - mark.tv_nsec)/1000000000);
        dLat = (prevPos.latitude - curPos.latitude)/timeslice ;
        dLong = (prevPos.longitude - curPos.longitude)/timeslice;
        dAlt = -(prevPos.altitude - curPos.altitude)/timeslice;
        dRoll = (prevOri.roll - curOri.roll)/timeslice;
        dPitch = (prevOri.pitch - curOri.pitch)/timeslice;
        dYaw = -(prevDir - curDir)/timeslice;

        //std::cout << "time: " << timeslice << std::endl;
        //body position velocity states
        errors[0] = (cos(curOri.pitch) * cos(curDir) * dLat)
                + (cos(curOri.pitch) * sin(curDir) * dLong)
                - (sin(curOri.pitch) * dAlt);
        errors[1] = ( ( (sin(curOri.roll) * sin(curOri.pitch) * cos(curDir) ) - (cos(curOri.roll) * sin(curDir)) ) * dLat )
                + ( ( (sin(curOri.roll) * sin(curOri.pitch) * sin(curDir) )  + (cos(curOri.roll) * cos(curDir)) ) * dLong )
                + (sin(curOri.roll) * cos(curOri.pitch) * dAlt);
        errors[2] = ( ( (cos(curOri.roll) * sin(curOri.pitch) * cos(curDir) ) + (sin(curOri.roll) * sin(curDir)) ) * dLat )
                + ( ( (cos(curOri.roll) * sin(curOri.pitch) * sin(curDir) )  + (sin(curOri.roll) * cos(curDir)) ) * dLong )
                + (cos(curOri.roll) * cos(curOri.pitch) * dAlt);
        //body angle rate states
        errors[3] = dRoll - (sin(curOri.roll) * dYaw);
        errors[4] = (cos(curOri.roll) * dPitch) + (sin(curOri.roll) * cos(curOri.pitch) * dYaw);
        errors[5] = (-sin(curOri.roll) * dRoll) + (cos(curOri.roll) * cos(curOri.pitch) * dYaw);
        //position
        //errors[6] = (targetPos.latitude - curPos.latitude);
        //errors[7] = (targetPos.longitude - curPos.longitude);
        //errors[8] = -(targetPos.altitude - curPos.altitude);
        //
        errors[6] = cos(curDir) * (targetPos.latitude - curPos.latitude) + sin(curDir) * (targetPos.longitude - curPos.longitude);
        errors[7] = -sin(curDir) * (targetPos.latitude - curPos.latitude) + cos(curDir) * (targetPos.longitude - curPos.longitude);
        errors[8] = -(targetPos.altitude - curPos.altitude);
        //
        //errors[6] = (cos(curOri.pitch) * cos(curDir) * (targetPos.latitude - curPos.latitude))
          //      + (cos(curOri.pitch) * sin(curDir) * (targetPos.longitude - curPos.longitude))
            //    - (sin(curOri.pitch) * (-targetPos.altitude + curPos.altitude));
        //errors[7] = ( ( (sin(curOri.roll) * sin(curOri.pitch) * cos(curDir) ) - (cos(curOri.roll) * sin(curDir)) ) * (targetPos.latitude - curPos.latitude) )
          //      + ( ( (sin(curOri.roll) * sin(curOri.pitch) * sin(curDir) )  + (cos(curOri.roll) * cos(curDir)) ) * (targetPos.longitude - curPos.longitude) )
            //    + (sin(curOri.roll) * cos(curOri.pitch) * (-targetPos.altitude + curPos.altitude));
        //errors[8] = ( ( (cos(curOri.roll) * sin(curOri.pitch) * cos(curDir) ) + (sin(curOri.roll) * sin(curDir)) ) * (targetPos.latitude - curPos.latitude) )
          //      + ( ( (cos(curOri.roll) * sin(curOri.pitch) * sin(curDir) )  + (sin(curOri.roll) * cos(curDir)) ) * (targetPos.longitude - curPos.longitude) )
            //    + (cos(curOri.roll) * cos(curOri.pitch) * (-targetPos.altitude + curPos.altitude));
        //angles
        errors[9] = (0 - curOri.roll);
        errors[10] = (0 - curOri.pitch);
        errors[11] = (targetDir - curDir);
        //
        //errors[9] = (cos(curOri.pitch) * cos(-curDir) * (0 - curOri.roll))
        //        + (cos(curOri.pitch) * sin(-curDir) * (0 - curOri.pitch))
         //       - (sin(curOri.pitch) * (targetDir - curDir));
        //errors[10] = ( ( (sin(curOri.roll) * sin(curOri.pitch) * cos(-curDir) ) - (cos(curOri.roll) * sin(-curDir)) ) * (0 - curOri.roll) )
        //        + ( ( (sin(curOri.roll) * sin(curOri.pitch) * sin(-curDir) )  + (cos(curOri.roll) * cos(-curDir)) ) * (0 - curOri.pitch) )
        //        + (sin(curOri.roll) * cos(curOri.pitch) * (targetDir - curDir));
        //errors[11] = ( ( (cos(curOri.roll) * sin(curOri.pitch) * cos(-curDir) ) + (sin(curOri.roll) * sin(-curDir)) ) * (0 - curOri.roll) )
         //       + ( ( (cos(curOri.roll) * sin(curOri.pitch) * sin(-curDir) )  + (sin(curOri.roll) * cos(-curDir)) ) * (0 - curOri.pitch) )
         //       + (cos(curOri.roll) * cos(curOri.pitch) * (targetDir - curDir));

        switch(size) {
        case 15:
            errors[14] = 0;
        case 14:
            errors[13] = 0;
        case 13:
            errors[12] = 0;
        }


        prevOri = curOri;
        prevDir = curDir;
        prevPos = curPos;
        mark = now;
        thrtCor = 0;
        pitCor = 0;
        rolCor = 0;
        yawCor = 0;
        for(loop=0;loop<size;loop++) {
            thrtCor += throttle[loop] * errors[loop];
            pitCor -= pitch[loop] * errors[loop];
            rolCor += roll[loop] * errors[loop];
            yawCor += yaw[loop] * errors[loop];
        }


    }


    sigCor[0] = thrtCor;
    sigCor[1] = pitCor;
    sigCor[2] = rolCor;
    sigCor[3] = yawCor;
    return true;
}

bool SSBehavior::populateConstants()
{
    std::fstream profile (profiledir.at(currIndex).c_str(), std::fstream::in);
    std::string line;
    int i,first;
    getline(profile, line);
    size = atof(line.c_str());
    if(size < 12 || size > MAX_MATRIX_SIZE)
        return false;

    getline(profile, line);
    for(i=0;i<size;i++) {
        first = line.find_first_of(' ',0);
        throttle[i] = atof(line.substr(0,first).c_str());
        line = line.substr(first+1, line.size());
        std::cout << i << " = " << throttle[i] << std::endl;
    }

    getline(profile, line);
    for(i=0;i<size;i++) {
        first = line.find_first_of(' ',0);
        roll[i] = atof(line.substr(0,first).c_str());
        line = line.substr(first+1, line.size());
    }
    getline(profile, line);
    for(i=0;i<size;i++) {
        first = line.find_first_of(' ',0);
        pitch[i] = atof(line.substr(0,first).c_str());
        line = line.substr(first+1, line.size());
    }
    getline(profile, line);
    for(i=0;i<size;i++) {
        first = line.find_first_of(' ',0);
        yaw[i] = atof(line.substr(0,first).c_str());
        line = line.substr(first+1, line.size());
    }

    return true;
}

bool SSBehavior::write_log_header(Datalogger *log)
{
    int i;
    std::string logline;
    char line[100];

    logline = "#Profile = " + profiles.at(currIndex);
    log->logMessage(logline);
    logline = "Throttle :";
    for(i = 0; i < size; i++) {
        sprintf(line,"%g ",throttle[i]);
        logline.append(line);
    }
    log->logMessage(logline);
    logline = "Roll     :";
    for(i = 0; i < size; i++) {
        sprintf(line,"%g ",roll[i]);
        logline.append(line);
    }
    log->logMessage(logline);
    logline = "Pitch    :";
    for(i = 0; i < size; i++) {
        sprintf(line,"%g ",pitch[i]);
        logline.append(line);
    }

    log->logMessage(logline);
    logline = "Yaw      :";
    for(i = 0; i < size; i++) {
        sprintf(line,"%g ",yaw[i]);
        logline.append(line);
    }
    log->logMessage(logline);
    return true;
}
