#include "PIDBehavior.h"
#include "MCTUtilities.h"
#include "MCTStructures.h"

#include <math.h>
#include <list>
#include <vector>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <ctime>


PIDBehavior::PIDBehavior() : BehaviorModule("PID PENDULUM")
{
    initialize();
}

PIDBehavior::~PIDBehavior()
{

}
bool PIDBehavior::initialize()
{
    clock_gettime(CLOCK_REALTIME, &run_start);
    setZero(&errAlt);
    setZero(&errLat);
    setZero(&errLong);
    setZero(&errPitch);
    setZero(&errYaw);
    setZero(&errRoll);
    std::string fname = "PID";
    loggerAlt = new DataloggerAlt(fname);
    loggerAlt->start();
    std::string logline =  "#Time\tPitch\tdeltaT\tP\tD\tprevError\tdeltaError";
    loggerAlt->logMessage(logline);
    return true;
}

bool PIDBehavior::getCorrections(double *sigCor, MCTorientation curOri, double currDir, MCTposition curPos, MCTposition targetPos, double targetDir)
{
    calc_orientation(curPos, currDir, targetPos, targetDir);
    //sigCor[0] = get_PID_correction(curPos.altitude, targetPos.altitude , &errAlt, currUAV.altitude);
    
    //Ian, this is the value we care for the PID on the pitch channel 
    sigCor[1] = get_PID_correction(curOri.pitch, targetOrientation.pitch , &errPitch, currUAV.pitch); 
   
    //sigCor[2] = get_PID_correction(curOri.roll, targetOrientation.roll , &errRoll, currUAV.roll);
    //sigCor[3] = get_PID_correction(currDir, targetDir , &errYaw, currUAV.yaw);
    return true;
}

bool PIDBehavior::populateConstants()
{
   // read_PID_model("./Profiles/GAUIquadPID.profile");
    read_PID_model(profiledir.at(currIndex));
    return true;
}


//This is the function we need you to understand to implement the controls theory
//Function seems not to be bounded by returning a value between 0 and 100.. 
/**double PIDBehavior::get_PID_correction(double current, double target, MCTPIDERR *err, MCTPID pid)
{
    // check ifthere is new data
    if( (target - current) == err->err) {
    	  char lineAlt[300];
    	  timespec now;
    	  clock_gettime(CLOCK_REALTIME, &now);
        // jus changed the err->err to target and err->preverr to current for debugging purposes
        sprintf(lineAlt,"%8.0f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f",( ((double) (now.tv_sec - run_start.tv_sec)*1000) + ((double) (now.tv_nsec - run_start.tv_nsec)/1000000) ),0,0,target,current,0,err->correction);
        loggerAlt->logMessage(lineAlt);
        return err->correction;
    } else {
        double timeslice;
        timespec now;
        double p,i,d;
        //long seconds;
        //int nseconds;
        clock_gettime(CLOCK_REALTIME, &now);
        //calculate timeslice in seconds
        timeslice = ((double) (now.tv_sec - err->mark.tv_sec)) + ((double) (now.tv_nsec - err->mark.tv_nsec)/1000000000);
       //move all current to previous
        err->mark = now;
        err->preverr = err->err;
        //caluclate new error and filtered error
        err->err = target - current;

        //calculate p term
        p = pid.Kp * err->err;
        //calculate i term
        i =  0;//( pid.Ki * timeslice * err->err ) + err->prevI;
        err->prevI = i;
        //calculate d term
        //d = ( ( pid.Kd * ( err->err - err->preverr ) ) + (pid.Kf * err->prevD) ) / (pid.Kf + timeslice);
        d= ( pid.Kd * ( err->err - err->preverr ) ) / (timeslice);
        err->prevD = d;
			
        err->correction = ( p + i + d);
        
        char lineAlt[300];
        // jus changed the err->err to target and err->preverr to current for debugging purposes
        sprintf(lineAlt,"%8.0f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f",( ((double) (now.tv_sec - run_start.tv_sec)*1000) + ((double) (now.tv_nsec - run_start.tv_nsec)/1000000) ),p,d,target,current,(p+d),err->correction);
        loggerAlt->logMessage(lineAlt);
        
        return err->correction;

    }
}**/



double PIDBehavior::get_PID_correction(double current, double target, MCTPIDERR *err, MCTPID pid)
{
    // check ifthere is new data
    if( (- current) == err->err) {
    	  char lineAlt[300];
    	  timespec now;
    	  clock_gettime(CLOCK_REALTIME, &now);
        // jus changed the err->err to target and err->preverr to current for debugging purposes
      //  sprintf(lineAlt,"%8.0f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f",( ((double) (now.tv_sec - run_start.tv_sec)*1000) + ((double) (now.tv_nsec - run_start.tv_nsec)/1000000) ),0,0,target,current,0,err->correction);
 //sprintf(lineAlt,"%8.0f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f",( ((double) (now.tv_sec - run_start.tv_sec)*1000) + ((double) (now.tv_nsec - run_start.tv_nsec)/1000000) ),current,target,0,0,0,0);
                
        loggerAlt->logMessage(lineAlt);
        return err->correction;
    } else {
        double timeslice;
        timespec now;
        double p,i,d;
        //long seconds;
        //int nseconds;
        clock_gettime(CLOCK_REALTIME, &now);
        //calculate timeslice in seconds
        timeslice = 0.0100;//** To avoid Noise, we decided to hardcore the value to: 10 ms((double) (now.tv_sec - err->mark.tv_sec)) + ((double) (now.tv_nsec - err->mark.tv_nsec)/1000000000);
       //move all current to previous
        err->mark = now;
        err->preverr = err->err;
        //caluclate new error and filtered error
        err->err = target - current;

        //calculate p term
        p = pid.Kp * err->err;
        //calculate i term
        i =  0;//( pid.Ki * timeslice * err->err ) + err->prevI;
        err->prevI = i;
        //calculate d term
        //d = ( ( pid.Kd * ( err->err - err->preverr ) ) + (pid.Kf * err->prevD) ) / (pid.Kf + timeslice);
        d= ( pid.Kd * ( err->err - err->preverr ) ) / (timeslice);
        err->prevD = d;
			
        err->correction = ( p + i + d);
        
        char lineAlt[300];
        // jus changed the err->err to target and err->preverr to current for debugging purposes
     //   sprintf(lineAlt,"%8.0f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f",( ((double) (now.tv_sec - run_start.tv_sec)*1000) + ((double) (now.tv_nsec - run_start.tv_nsec)/1000000) ),p,d,target,current,0,err->correction);
        sprintf(lineAlt,"%8.0f\t%3.6f\t%8.6f\t%4.6f\t%4.6f\t%8.6f\t%8.6f",( ((double) (now.tv_sec - run_start.tv_sec)*1000) + ((double) (now.tv_nsec - run_start.tv_nsec)/1000000) ),current,timeslice,p,d,err->preverr,err->err - err->preverr );
  
        loggerAlt->logMessage(lineAlt);
        
        return err->correction;

    }
}


double PIDBehavior::get_PID_correction_verbose(double current, double target, MCTPIDERR *err, MCTPID pid)
{
    // check if there is new data
    
    if( (target - current) == err->err) {
        return err->correction;
    } else {
        double timeslice;
        timespec now;
        double p,i,d;
        clock_gettime(CLOCK_REALTIME, &now);
        //calculate timeslice in seconds
        timeslice = ((double) (now.tv_sec - err->mark.tv_sec)) + ((double) (now.tv_nsec - err->mark.tv_nsec)/1000000000);

        //move all current to previous
        err->mark = now;
        err->preverr = err->err;
        //caluclate new error and filtered error
        err->err = target - current;

        //calculate p term
        p = pid.Kp * err->err;
        //calculate i term
        i = ( pid.Ki * timeslice * err->err ) + err->prevI;
        err->prevI = i;
        //calculate d term
        d = ( ( pid.Kd * ( err->err - err->preverr ) ) + (pid.Kf * err->prevD) ) / (pid.Kf + timeslice);
        err->prevD = d;


        err->correction = ( p + i + d);

        std::cout << "KP " << pid.Kp << " KI " << pid.Ki << " KD " << pid.Kd << " KF " << pid.Kf << std::endl;
        std::cout << "Current " << current <<  " Target " << target << std::endl;
        std::cout << "Slice " << timeslice << " Error" << err->err << " P " << p << " I " << i << " D " << d << std::endl;
        std::cout <<"Correction " << err->correction << std::endl;

        
       
       return err->correction;
    }
}

void PIDBehavior::calc_orientation(MCTposition cpos, double cdir, MCTposition tpos, double tdir)
{
    //targetOrientation.pitch = -DEG_TO_RAD(1);
    //targetOrientation.roll = DEG_TO_RAD(-1.75);
    //targetOrientation.pitch = 0 + get_PID_correction(cpos.latitude, targetPosition.latitude, &errLat, &(currUAV.latitude));
    //targetOrientation.roll =  0 + get_PID_correction_verbose(cpos.longitude, targetPosition.longitude, &errLong, &(currUAV.longitude));


    //change absolute positions to distance needed forward and side from
    //the current vehicle depending on its current direction
    
    //double front = cos(cdir)*(tpos.latitude-cpos.latitude) + sin(cdir)*(tpos.longitude - cpos.longitude);
    //double side = -sin(cdir)*(tpos.latitude-cpos.latitude) + cos(cdir)*(tpos.longitude - cpos.longitude);
    //targetOrientation.pitch = 0 + get_PID_correction( 0.0, front, &errLat, currUAV.latitude);
    //targetOrientation.roll = 0 + get_PID_correction(0.0, side, &errLong, currUAV.longitude);
    
    targetOrientation.pitch = 0;
    targetOrientation.roll = 0;
    

    //std::cout << "lat: " << front << "  pitch : " << RAD_TO_DEG(targetOrientation.pitch) << std::endl;
    //std::cout << "long: " << side << "  roll : " << RAD_TO_DEG(targetOrientation.roll) << std::endl;
}


void PIDBehavior::read_PID_model(std::string filename)
{

    std::fstream profile (filename.c_str(), std::fstream::in);
    int fpos = 14;
    modelName = filename;
    std::string line;

    getline(profile, line);
    currUAV.roll.Kp = atof(line.substr(fpos).c_str());
    std::cout<< line << std::endl;
    getline(profile, line);
    currUAV.roll.Ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.roll.Kd = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.roll.Kf = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.roll.Kt = atof(line.substr(fpos).c_str());

    getline(profile, line);
    currUAV.pitch.Kp = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.pitch.Ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.pitch.Kd = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.pitch.Kf = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.pitch.Kt = atof(line.substr(fpos).c_str());

    getline(profile, line);
    currUAV.yaw.Kp = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.yaw.Ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.yaw.Kd = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.yaw.Kf = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.yaw.Kt = atof(line.substr(fpos).c_str());

    getline(profile, line);
    currUAV.altitude.Kp = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.altitude.Ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.altitude.Kd = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.altitude.Kf = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.altitude.Kt = atof(line.substr(fpos).c_str());

    getline(profile, line);
    currUAV.longitude.Kp = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.longitude.Ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.longitude.Kd = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.longitude.Kf = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.longitude.Kt = atof(line.substr(fpos).c_str());

    getline(profile, line);
    currUAV.latitude.Kp = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.latitude.Ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.latitude.Kd = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.latitude.Kf = atof(line.substr(fpos).c_str());
    getline(profile, line);
    currUAV.latitude.Kt = atof(line.substr(fpos).c_str());

    profile.close();
}
/*
  * Writes the current constants to the pid UAV model file
  * @author Jeff Wick MCT 2011
  * @param pointer to the file
 **/
void PIDBehavior::write_PID_model(std::string filename, MCTUAV model)
{
    std::fstream profile (filename.c_str(), std::fstream::out);

    profile << "Roll KP:      " << model.roll.Kp << std::endl;
    profile << "Roll KI:      " << model.roll.Ki << std::endl;
    profile << "Roll KD:      " << model.roll.Kd << std::endl;
    profile << "Roll Kf:      " << model.roll.Kf << std::endl;
    profile << "Roll KT:      " << model.roll.Kt << std::endl;

    profile << "Pitch KP:     " << model.pitch.Kp << std::endl;
    profile << "Pitch KI:     " << model.pitch.Ki << std::endl;
    profile << "Pitch KD:     " << model.pitch.Kd << std::endl;
    profile << "Pitch Kf:     " << model.pitch.Kf << std::endl;
    profile << "Pitch KT:     " << model.pitch.Kt << std::endl;

    profile << "Yaw KP:       " << model.yaw.Kp << std::endl;
    profile << "Yaw KI:       " << model.yaw.Ki << std::endl;
    profile << "Yaw KD:       " << model.yaw.Kd << std::endl;
    profile << "Yaw Kf:       " << model.yaw.Kf << std::endl;
    profile << "Yaw KT:       " << model.yaw.Kt << std::endl;

    profile << "Altitude KP:  " << model.altitude.Kp << std::endl;
    profile << "Altitude KI:  " << model.altitude.Ki << std::endl;
    profile << "Altitude KD:  " << model.altitude.Kd << std::endl;
    profile << "Altitude Kf:  " << model.altitude.Kf << std::endl;
    profile << "Altitude KT:  " << model.altitude.Kt << std::endl;

    profile << "Longitude KP: " << model.longitude.Kp << std::endl;
    profile << "Longitude KI: " << model.longitude.Ki << std::endl;
    profile << "Longitude KD: " << model.longitude.Kd << std::endl;
    profile << "Longitude Kf: " << model.longitude.Kf << std::endl;
    profile << "Longitude KT: " << model.longitude.Kt << std::endl;

    profile << "Latitude KP:  " << model.latitude.Kp << std::endl;
    profile << "Latitude KI:  " << model.latitude.Ki << std::endl;
    profile << "Latitude KD:  " << model.latitude.Kd << std::endl;
    profile << "Latitude Kf:  " << model.latitude.Kf << std::endl;
    profile << "Latitude KT:  " << model.latitude.Kt << std::endl;

    profile.close();
}

bool PIDBehavior::write_log_header(Datalogger *log)
{
    std::string logline;
    char line[100];
    logline = "#Profile = " + modelName;
    log->logMessage(logline);
    logline = "#UAV Constants\t\tP\t\tI\t\tD\t\tTrim";
    log->logMessage(logline);
    sprintf(line,"#Pitch\t\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.pitch.Kp, currUAV.pitch.Ki, currUAV.pitch.Kd, currUAV.pitch.Kt);
    log->logMessage(line);
    sprintf(line,"#Roll\t\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.roll.Kp, currUAV.roll.Ki, currUAV.roll.Kd, currUAV.roll.Kt);
    log->logMessage(line);
    sprintf(line,"#Yaw\t\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.yaw.Kp, currUAV.yaw.Ki, currUAV.yaw.Kd, currUAV.yaw.Kt);
    log->logMessage(line);
    sprintf(line,"#Altitude\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.altitude.Kp, currUAV.altitude.Ki, currUAV.altitude.Kd, currUAV.altitude.Kt);
    log->logMessage(line);
    sprintf(line,"#Longitude\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.longitude.Kp, currUAV.longitude.Ki, currUAV.longitude.Kd, currUAV.longitude.Kt);
    log->logMessage(line);
    sprintf(line,"#Lattitude\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.latitude.Kp, currUAV.latitude.Ki, currUAV.latitude.Kd, currUAV.latitude.Kt);
    log->logMessage(line);
    return true;
}

bool PIDBehavior::write_log_header(DataloggerAlt *log)
{
    std::string logline;
    char line[100];
    logline = "#Profile = " + modelName;
    log->logMessage(logline);
    logline = "#UAV Constants\t\tP\t\tI\t\tD\t\tTrim";
    log->logMessage(logline);
    sprintf(line,"#Pitch\t\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.pitch.Kp, currUAV.pitch.Ki, currUAV.pitch.Kd, currUAV.pitch.Kt);
    log->logMessage(line);
    sprintf(line,"#Roll\t\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.roll.Kp, currUAV.roll.Ki, currUAV.roll.Kd, currUAV.roll.Kt);
    log->logMessage(line);
    sprintf(line,"#Yaw\t\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.yaw.Kp, currUAV.yaw.Ki, currUAV.yaw.Kd, currUAV.yaw.Kt);
    log->logMessage(line);
    sprintf(line,"#Altitude\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.altitude.Kp, currUAV.altitude.Ki, currUAV.altitude.Kd, currUAV.altitude.Kt);
    log->logMessage(line);
    sprintf(line,"#Longitude\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.longitude.Kp, currUAV.longitude.Ki, currUAV.longitude.Kd, currUAV.longitude.Kt);
    log->logMessage(line);
    sprintf(line,"#Lattitude\t\t\t%g\t\t%g\t\t%g\t\t%g",currUAV.latitude.Kp, currUAV.latitude.Ki, currUAV.latitude.Kd, currUAV.latitude.Kt);
    log->logMessage(line);
    return true;
}

