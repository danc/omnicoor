#ifndef OMNIBOTTRACKER_H
#define OMNIBOTTRACKER_H

#include <sys/types.h>
#include "Module.h"
#include "MCTStructures.h"
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"

/**
  * Optitrack VRPN camera setup Module for tracking Omnibot
 **/
class OmnibotTracker : public Module
{
private:
    vrpn_Connection *connection;
    vrpn_Tracker_Remote *tracker;

public:
    OmnibotTracker();
    ~OmnibotTracker();
    bool isDirMod()  {return true;}
    bool isPosMod()  {return true;}
    bool isOriMod()  {return true;}

    bool start();
    bool update();
    bool stop();
};

#endif // OMNIBOTTRACKER_H
