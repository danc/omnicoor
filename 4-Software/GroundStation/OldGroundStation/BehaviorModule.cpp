#include "BehaviorModule.h"

#include <string>

#include "MCTStructures.h"

/**
  * Behavior Module Interace is used to define various controller such as PID etc
  * @author Jeff Wick MCT 2012
 **/
BehaviorModule::BehaviorModule()
{
    modName="";
}

BehaviorModule::BehaviorModule(std::string name)
{
   modName = name;
}

bool BehaviorModule::addProfile(std::string profileName, std::string fileName)
{
    for(unsigned int i = 0; i < profiles.size(); i++) {
        if(profiles.at(i).compare(profileName) == true ||
                profiledir.at(1).compare(fileName) == true)
            return false;
    }
    profiles.push_back(profileName);
    profiledir.push_back(fileName);
    return true;
}

  bool BehaviorModule::setProfile(std::string profileName)
  {
      for(unsigned int i = 0; i < profiles.size(); i++) {
          if(profiles.at(i).compare(profileName) == 0) {
              currIndex = i;
              return true;
          }
      }
      return false;
  }
