#include "Trainer.h"

#include <cstdlib>
#include <cstdio>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <string>
#include <iostream>

Trainer::Trainer(int minimum, int maximum)
{
    MAX_CHANNEL_VAL = maximum;
    MIN_CHANNEL_VAL = minimum;
}

Trainer::~Trainer()
{

}

/**
  * Initializes and sets the options of the fpga used
  * to send the signal to the trainer
 **/
bool Trainer::initFPGA()
{
    char dir[20];
    sprintf(dir, "/dev/ttyS0");
    fpga_fd = open(dir, O_RDWR | O_NDELAY);
    //fd = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY);
    if (fpga_fd == -1)
        {
        std::cout << "could not open " << dir << std::endl;
        //Could not open the port.
        return false;
        }
    else
        {
        fcntl(fpga_fd, F_SETFL, 0);
        //set_fpga_options( fpga_fd );
        struct termios options; // UART port options data structure

        // Get the current options for the port...
        tcgetattr(fpga_fd, &options);

        // Set the baud rates to 38400...
        cfsetispeed(&options, B38400);
        cfsetospeed(&options, B38400);

        // Enable the receiver and set local mode...
        options.c_cflag |= (CLOCAL | CREAD);

        // Set charater size
        options.c_cflag &= ~CSIZE; // Mask the character size bits
        options.c_cflag |= CS8;    // Select 8 data bits

        // Set no parity 8N1
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;

        // Disable Hardware flow control
        options.c_cflag &= ~CRTSCTS;

        // (unsigned short)Use raw input
        options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

        // Disable SW flow control
        options.c_iflag &= ~(IXON | IXOFF | IXANY);

        // Use raw output
        options.c_oflag &= ~OPOST;

        // Set new options for the port...
        tcsetattr(fpga_fd, TCSANOW, &options);
        }
    return true;
}

void Trainer::setChannel(char command, unsigned short value)
{
    char high, low;
    int n; // number of bytes written out of the UART port

    if ( value > MAX_CHANNEL_VAL )
        value = MAX_CHANNEL_VAL;
    if ( value < MIN_CHANNEL_VAL)
        value = MIN_CHANNEL_VAL;

    high = value >> 8;
    low  = value % 256;

    // Command hex value
    n = write(fpga_fd, &command, sizeof(command));

    // High byte
    n = write(fpga_fd, &high, sizeof(high));

    // Low byte
    n = write(fpga_fd, &low, sizeof(low));
}

void Trainer::setAllChannel(unsigned short one, unsigned short two, unsigned short three, unsigned short four)
{
 

    //std::cout << "Set " << one << " " << two <<" " << three << " " << four << std::endl;
    this->setChannel(0xC1, one); /****one ***/
    this->setChannel(0xC2, two); /****two ***/
    this->setChannel(0xC3, three);     /****three ***/
    this->setChannel(0xC4, four);    /****four ***/
}

void Trainer::closeFPGA()
{
    close(fpga_fd);
}
