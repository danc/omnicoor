#ifndef DATALOGGER_H
#define DATALOGGER_H

#include <string>
#include <iostream>
#include <fstream>

/** @brief Writes information to a log file.
  *
  * @details Log files are created in "./Logs/(data)/(time)"
  */
class Datalogger
{
private:
    //! File name of the log file
    std::fstream logFile;

    //! Directory to create the log file in.
    std::string directory;
public:
    /** @brief Defualt constructor for Datalogger.
      */
    Datalogger();

    /** @brief Starts the data logger.
      * @details Creates a directory and a log file.
      *          Directories are named by the current date "year - month - date".
      *          Log files are named by the time of start.
      *
      * @return True if successful.
      */
    bool start();

    /** @brief Closes the log file stream.
      *
      * @return True if successful.
      */
    bool stop();

    /** @brief Writes a message to the log file.
      *
      * @param[in] line String of what to write to the log file.
      */
    void logMessage(std::string);
};

#endif // DATALOGGER_H
