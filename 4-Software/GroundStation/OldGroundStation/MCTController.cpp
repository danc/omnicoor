#include "MCTController.h"
#include "MCTUtilities.h"
#include "BehaviorModule.h"
#include "PIDBehavior.h"
#include "SSBehavior.h"

#include <math.h>
#include <list>
#include <vector>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <ctime>

MCTController::MCTController()
{
    listMod = new std::vector<Module*>();
    logger = new Datalogger();
    std::string fname = "mct";
    loggerAlt = new DataloggerAlt(fname);
    listBehavior = new std::vector<BehaviorModule*>();

    currBehavior = new BehaviorModule();

    currOriMod = new Module();
    currDirMod = new Module();
    currPosMod = new Module();
    currCommMod = new Module();
    currContMod = new Module();
    listBehavior=  new std::vector<BehaviorModule*>;
    home = false;
    targetDirection = 0.0;

    PIDBehavior *pid1 = new PIDBehavior();
    pid1->addProfile("GAUI","./Profiles/GAUIquadPID.profile");
    SSBehavior *ss1 = new SSBehavior();
    ss1->addProfile("TEST","./Profiles/TESTSF.profile");

    listBehavior->push_back(pid1);
    listBehavior->push_back(ss1);

    autoPitch = true;
    autoRoll = true;
    autoThrottle = true;
    autoYaw = true;
    lastThrottle = 0;
    status = IDLE;

}

MCTController::~MCTController()
{
    if(status == RUNNING)
        stop();
    delete listMod;
    delete logger;
    delete currOriMod;
    delete currDirMod;
    delete currPosMod;
    delete currCommMod;
    delete currContMod;
    delete loggerAlt;
}

bool MCTController::addModule(Module *module) {
    if(!listMod->empty()) {
        // return false if the module is already added
        for(unsigned int i = 0; i<listMod->size(); i++) {
            if(listMod->at(i)->getName().compare(module->getName()) == 0) {
                return false;
            }
        }

    }
    listMod->push_back(module);
    return true;
}

bool MCTController::setModule(enum MODULE_TYPE type, std::string name) {
    if(status == RUNNING)
        return false;
    if(!listMod->empty()) {
        for(unsigned int i = 0;i < listMod->size(); i++) {
            //Special Cases
            if(name.compare("None") == 0 && type == CONTROL) {
                currContMod = NULL;
                autoYaw = true;
                autoPitch = true;
                autoRoll = true;
                autoThrottle = true;
                return true;
            }
            if(name.compare("Automatic") == 0 && type == CONTROL){
                currContMod = NULL;
                autoYaw = true;
                autoPitch = true;
                autoRoll = true;
                autoThrottle = false;
                return true;
            }
            if(listMod->at(i)->getName().compare(name) == 0) {
                switch(type) {
                case(ORIENTATION):
                    currOriMod = listMod->at(i);
                    return true;
                case(POSITION):
                    currPosMod = listMod->at(i);
                    return true;
                case (DIRECTION):
                    currDirMod = listMod->at(i);
                    return true;
                case(COMMUNICATION):
                    currCommMod = listMod->at(i);
                    return true;
                case(CONTROL):
                    currContMod = listMod->at(i);
                    autoYaw = false;
                    autoPitch = false;
                    autoRoll = false;
                    autoThrottle = false;
                    return true;
                }
            }
        }
    }
    return false;
}

bool MCTController::setBehavior(std::string behaviorName)
{
    for(unsigned int i = 0; i < listBehavior->size(); i++) {
        if(listBehavior->at(i)->getName().compare(behaviorName) == 0) {
            currBehavior = listBehavior->at(i);
            return true;
        }
    }
    return false;
}

bool MCTController::setBehaviorModel(std::string modelName)
{
    return currBehavior->setProfile(modelName);
}

std::vector<std::string> MCTController::getBehaviors()
{
    std::vector<std::string> *temp = new std::vector<std::string>;
    for(unsigned int i = 0; i<listBehavior->size(); i++) {
        temp->push_back(listBehavior->at(i)->getName());
    }
    return *temp;
}

std::vector<std::string> MCTController::getBehaviorModels()
{
    return currBehavior->getModels();
}

bool MCTController::start()
{

    if(status == RUNNING)
        return false;
    //currUAV = read_UAV_model(modelFilename);
    bool tempStatus = true;
    home = false;
    lastThrottle = 0;
    unsigned int loop;
    Module *tempMod;
    for(loop = 0; loop< listMod->size(); loop++) {
        tempMod = listMod->at(loop);
        if( currContMod != NULL ) {
            if( currContMod == tempMod) {
                if(listMod->at(loop)->start() == false)
                    tempStatus = false;
            }
        }
        if(tempMod == currPosMod || tempMod == currOriMod ||
                tempMod == currDirMod || tempMod == currCommMod) {
            if(listMod->at(loop)->start() == false)
                tempStatus = false;
        }

    }

    if(tempStatus == false) {
        stop();
    } else {

        home = false;

        currBehavior->initialize();
        currBehavior->populateConstants();
        //TODO BEHAVIOR MODULE NEED TO WRITE ITS INFORMATION TO LOGS
        status = RUNNING;
        //logger->start();
        loggerAlt->start();
        //currBehavior->write_log_header(logger);
        currBehavior->write_log_header(loggerAlt);
        std::string logline;
        logline = "#Orientation:   " + currOriMod->getName();
        log(logline);
        logline = "#Position:      " + currPosMod->getName();
        log(logline);
        logline = "#Direction:     " + currDirMod->getName();
        log(logline);
        logline = "#Communication: " + currCommMod->getName();
        log(logline);
        if(currContMod == NULL) {
            logline = "#Control:       None";
            log(logline);
        } else {
            logline = "#Control:       " + currContMod->getName();
            log(logline);
        }
        logline = "#Time\tPitch\tRoll\tDir\tsetPit\tsetRol\tsetDir\tAlt\tLat\tLon\tsetAlt\tsetLat\tsetLon\tSigThr\tSigPit\tSigRol\tSigYaw";
        log(logline);
        logline =  "#Time\tLat\tLon\tAlt\tRoll\tPitch\tYaw\tPIDpitchIn\tPIDpitchOut\tsetThrottle\tmatrixInPitch\tMotor_1\tMotor_3";
        loggerAlt->logMessage(logline);
        clock_gettime(CLOCK_REALTIME, &run_start);
    }
    return tempStatus;
}

void MCTController::stop() {
    std::cout << "Stopping things";
    unsigned int loop;
    Module *tempMod;
    home = false;
    status = IDLE;
    for(loop = 0; loop< listMod->size(); loop++) {
        tempMod = listMod->at(loop);
        if( currContMod != NULL ) {
            if( currContMod == tempMod) {
                listMod->at(loop)->stop();
            }
        }
        if(tempMod == currPosMod || tempMod == currOriMod ||
                tempMod == currDirMod ||tempMod == currCommMod) {
            listMod->at(loop)->stop();
        }

    }
    logger->stop();
    loggerAlt->stop();
}

double MCTController::update()  //HERE FENG
{
    
    //Conv[] get the CHANGE_XXXX (of every of the 4 directions, t,p,y,r)
    // ManSig[] get the 
    
    
    if(status == IDLE)
        return 0;
    timespec start, end;
    clock_gettime(CLOCK_REALTIME, &start);
    unsigned int loop;
    double manSig[4] = {0,0,0,0};
    double sendSig[8] = {0,0,0,0,0,0,0,0};
    double contSig[4] = {0,0,0,0};
    double pidIn[4] = {0,0,0,0};
    double conv[4] = {0,0,0,0};
    long total_nsec;
    int total_sec;
    Module *tempMod;
    for(loop = 0; loop< listMod->size(); loop++) {
        tempMod = listMod->at(loop);
        if(tempMod == currPosMod ||
           tempMod == currOriMod ||
           tempMod == currDirMod ) {
                listMod->at(loop)->update();
        }
    }
    // This isn't a PROBLEM without the omnibot running, but it's weird.
    // Results in multiple runs of the UAV having irregular starting points.
    /*if(home == false) {
        setHome();
    }*/
    manSig[0] = 0;
    manSig[1] = 50;
    manSig[2] = 50;
    manSig[3] = 50;
    //send proper signal to the communication MOD
    //get the values from the manual input if needed
    if(currContMod != NULL &&
       !(autoPitch && autoRoll && autoThrottle && autoYaw) ) {
            currContMod->getModValues(manSig, 4);
            std::cout << "manSig[0]: " << manSig[0] << std::endl;
    }
    //get comm mods conversion factors if needed
    currCommMod->getModValues(conv,4); //get CHANGE VALUES frp throttle, yaw, pitch , roll. 

    if( cus_nancheck(targetOrientation.pitch) || cus_nancheck(targetOrientation.roll) ) {
        std::cout << "Target orientation nan error" << std::endl;
        return -1;
    }
    //std::cout<< manSig[0] << " " << manSig[1] << " " << manSig[2] << " " << manSig[3] << std::endl;
    
    /*
      Increase throttle increases signal %
      Left yaw decreases signal %
      Left roll decreases signal %
      Forward pitch increases signal % */
      
      
    pidIn[0] = contSig[0];     
    pidIn[1] = contSig[1];     
    pidIn[2] = contSig[2];     
    pidIn[3] = contSig[3];     

	 //Function from PIDBehavior.cpp
	 //It calculates the error correction fotr the 4 directions (pitch, yaw, roll , roll). these are used only in auto mode
	 //this error correction values are stored in contSig[]
    currBehavior->getCorrections(contSig, currOriMod->getOrientation(), currDirMod->getDirection(),
                                 currPosMod->getPosition(), targetPosition, targetDirection);
                                 //*****look in PIDBehavior.cpp for computation of contSig (sigCor)*****
                                 
  	 //contSig[] is used when the system is set to auto mode 
    //std::cout <<"Cont Sig" << contSig[0] << " " << contSig[1] << " " << contSig[2] << " " << contSig[3] << "\n";
    
    
    
    
    
    //If Throttle is set as Auto 
    //conv[0] is equal to the constant  CHANGE_ROLL = 7; (from the SixchantrainMod) 
	 //sendSig[4] Represent Throttle trim val in SixchanTraiMod
    if(autoThrottle) {
        sendSig[0] = lastThrottle + (contSig[0] / conv[0] );
        sendSig[4] = 0;
       // lastThrottle = sendSig[0];
    } 
    
    //Manual throttle 
    //send [0] is the thrthVal we send to the freq_throttle for the throttle calculation 
    //mansig[0] is the numerical output from the Throttle channel comming from the USB controler 
    else 
    {
        if(currContMod != NULL){
            sendSig[0] = manSig[0];
            
           	if( sendSig[0] > 25)
           	{
           	sendSig[0] = 70;
           	}
           	else
           	{
           	sendSig[0] = 0;
           	}
            lastThrottle = manSig[0];
            targetPosition.altitude = currPosMod->getPosition().altitude;
        }
        else
        {
            double getAll[8];
            currCommMod->getAllModValues(getAll, 8);

            sendSig[0] = getAll[0];
            lastThrottle = getAll[0];
            targetPosition.altitude = currPosMod->getPosition().altitude;
        }

    }
		
		
    //When the system is set as auto pitch this block is executed.
    //sendsig[1] = the pitchVal we send to the SixchanTrainMod 
    //contSig[1] is the error change calculated in PIDBehavior for the pitch direction
    //conv[1] is the CHANGE_PITCH= 6; Constant from SixchanTrainMod
    if(autoPitch) 
    {
        sendSig[1] = contSig[1] / conv[1] -11;
        //sendSig[1] =  contSig[1];
        sendSig[5] = 0;
    } 
    
    //Manual Pitch 
    //send [1] is the ptichVal we send to the freq_pitch for the pitch calculation 
    //conv[1] is equal to the constant  CHANGE_PITCH = 6; (from the SixchantrainMod)  
    //mansig[1] is the numerical output from the pitch channel comming from the USB controler 
    //sendsig[5] represent pitchTrim in SixchanTrainMod. 
    else 
    { 
        sendSig[1] = 50.0 + ((3/conv[1])*(manSig[1]-50.0));
        sendSig[5] = 0; //was the trim value of the model TODO readd base values of a model??
    }

   
   
    if(autoRoll) {
        sendSig[2] = 50 + (contSig[2] / conv[2] );
        sendSig[6] = 0;
    } 
    
    //Manual Roll 
    //send [2] is the rollVal we send to the freq_roll for the roll calculation 
    //conv[2] is equal to the constant  CHANGE_ROLL = 6; (from the SixchantrainMod)  
    //mansig[2] is the numerical output from the ROLL channel comming from the USB controler 
    //sendsig[6] represent rollTrim in SixchanTrainMod. 
    else 
    {
        sendSig[2] = 50.0 + ((3/conv[2])*(manSig[2]-50.0));
        sendSig[6] = 0; //was the trim value of the model TODO readd base values of a model??

    }

    if(autoYaw) {
        sendSig[3] = 50 + (contSig[3] / conv[3] );
        sendSig[7] = 0;
    } 
    
    
    //Manual YAW 
    //send [3] is the yawVal we send to the freq_roll for the roll calculation 
    //conv[3] is equal to the constant  CHANGE_YAW = 6; (from the SixchantrainMod)  
    //mansig[3] is the numerical output from the YAW channel comming from the USB controler 
    //sendsig[7] represent yawTrim in SixchanTrainMod. 
    else 
    {
        sendSig[3] = 50.0 + ((4/conv[3])*(manSig[3]-50.0));
        sendSig[7] = 0; //was the trim value of the model TODO readd base values of a model??
    }
    
    
    //This loop avoid having send values below 0 or above 100 
    //to the throlle,yaw,pitch,and roll 
    int i;
    /*
    for(i=0; i<4; i++) {
        if(sendSig[i] < 0)
            sendSig[i] = 0;
        if(sendSig[i] > 100)
            sendSig[i] = 100;
    }
	*/
		
    // std::cout<< sendSig[0] << " " << sendSig[1] << " " << sendSig[2] << " " << sendSig[3] << std::endl;
    if(cus_nancheck(sendSig[0]) || cus_nancheck(sendSig[1]) || cus_nancheck(sendSig[2]) || cus_nancheck(sendSig[3])) {
        std::cout << "Sent signal nan error" << std::endl;
        return -1;
    }
    
  
   
   /*
   *Motors 2 and 4 are set to 0 because we are doing 1-D pendulum control 
   *Future we will use them when we go to 2-D control 
   *sendSig[0] coms from the USB controller 
   *sendSig[1] comes fro the output from the PID 
   */
    
    
    
   
     std::cout << "USB REMOTE: " << sendSig[0] << std::endl;
    long Motor_1 = sendSig[0]   -  sendSig[1];
    int Motor_2 = 0; 
    long Motor_3 = sendSig[0]  + sendSig[1];   
    int Motor_4 = 0;  
    
   
    std::cout << "PID pitch output " << sendSig[1] << std::endl;
    
   
    
    
     if(Motor_1 > 100)
        {
            Motor_1 = 100;
        }
     
        if(Motor_2 > 100)
        {
            Motor_2 = 100;
        }
       
        if(Motor_3 > 100)
        {
            Motor_3 = 100;
        }
        
        
        if(Motor_4 > 100)
        {
            Motor_4 = 100;
        }
   
   
    if(Motor_1 < 0)
    {
      Motor_1 = 0;
    }
    
    if(Motor_3 < 0)
    {
      Motor_3 = 0; 
    }
    
    
    std::cout << "Motor_1 Real Value:  " <<  Motor_1 << std::endl;
    std::cout << "Motor_3 Real Value: " <<  Motor_3 << std::endl;
    double All_Motors [8] = {Motor_1, Motor_2 ,Motor_3, Motor_4,0,0,0,0};
    
    
    currCommMod->setModValues(All_Motors,8);   //    currCommMod->setModValues(sendSig,8); 
    currCommMod->update();
    
    
    
    // Data being logged into the original logger
    
    char line[300];
    //log the data we want to keep
    clock_gettime(CLOCK_REALTIME, &end);
    total_nsec = end.tv_nsec - run_start.tv_nsec;
    total_sec = end.tv_sec - run_start.tv_sec;
    
    /*
    sprintf(line,"%8.0f\t%+3.2f\t%+3.2f\t%+3.2f\t%+3.2f\t%+3.2f\t%+3.2f\t%+3.4f\t%+3.4f\t%+3.4f\t%+3.4f\t%+3.4f\t%+3.4f\t%2.2f\t%2.2f\t%2.2f\t%2.2f",
                ( ((double) total_sec*1000) + ((double) total_nsec /1000000) ),
                RAD_TO_DEG(currOriMod->getOrientation().pitch), RAD_TO_DEG(currOriMod->getOrientation().roll), RAD_TO_DEG(currDirMod->getDirection()),
                RAD_TO_DEG(targetOrientation.pitch), RAD_TO_DEG(targetOrientation.roll), RAD_TO_DEG(targetDirection),
                currPosMod->getPosition().altitude,currPosMod->getPosition().latitude,currPosMod->getPosition().longitude,targetPosition.altitude,targetPosition.latitude,targetPosition.longitude,
                sendSig[0],sendSig[1],sendSig[2],sendSig[3]
                /*kp,ki,kd,kf*///);  
    //log(line);
    
    
    // Data being logged into the logger for the 1_D testing system

   char lineAlt[300];
   sprintf(lineAlt,"%8.0f\t%+3.4f\t%+3.4f\t%+3.4f\t%+3.2f\t%+3.2f\t%+3.2f\t%2.2f\t%2.2f\t%2.2f\t%2.2f\t%d\t%d",( ((double) total_sec*1000) + ((double)      total_nsec /1000000) ),currPosMod->getPosition().latitude,currPosMod->getPosition().longitude,currPosMod->getPosition().altitude,RAD_TO_DEG(currOriMod->getOrientation().roll),RAD_TO_DEG(currOriMod->getOrientation().pitch),RAD_TO_DEG(currDirMod->getDirection()),pidIn[1],contSig[1],sendSig[0],sendSig[1],(Motor_1*13),(Motor_3*13));
   //(int)((double)Motor_3*13.0*0.8)
    loggerAlt->logMessage(lineAlt);
    clock_gettime(CLOCK_REALTIME, &end);
    total_nsec = end.tv_nsec - start.tv_nsec;
    total_sec = end.tv_sec - start.tv_sec;
    return ( ((double) total_sec*1000) + ((double) total_nsec /1000000) ); 
    
  
    
}

void MCTController::log(std::string line)
{
    logger->logMessage(line);
}

std::vector<std::string> MCTController::getAllModName(enum MODULE_TYPE type)
{
    unsigned int i;
    std::vector<std::string> *temp = new std::vector<std::string>();
    if(type == CONTROL) {
        temp->push_back("None");
        temp->push_back("Automatic");
    }
    for( i = 0; i < listMod->size(); i++) {
        switch(type) {
        case(ORIENTATION):
           if( listMod->at(i)->isOriMod())
               temp->push_back(listMod->at(i)->getName());
           break;
        case(POSITION):
           if( listMod->at(i)->isPosMod() )
               temp->push_back(listMod->at(i)->getName());
           break;
        case(DIRECTION):
           if( listMod->at(i)->isDirMod() )
               temp->push_back(listMod->at(i)->getName());
            break;
        case(COMMUNICATION):
           if( listMod->at(i)->isCommMod())
               temp->push_back(listMod->at(i)->getName());
           break;
        case(CONTROL):
           if( listMod->at(i)->isContMod() )
               temp->push_back(listMod->at(i)->getName());
           break;
        }
    }
    return *temp;
}

void MCTController::setHome()
{
    homePosition = currPosMod->getPosition();
    targetPosition = homePosition;

    home = true;
}


bool MCTController::setToOmni(double latitude, double longitude){
    if(longitude == 0 && latitude == 0) {
        return false;
    }

    targetPosition.longitude = longitude;
    targetPosition.latitude = latitude;
    return true;
}


//Function that get the real time position of the system 
Position MCTController::getCurrentPosition()
{
    Position position;
    MCTposition current_position = currPosMod->getPosition();

    position.x = current_position.longitude;
    position.y = current_position.latitude;
    position.z = current_position.altitude;

    position.heading = RAD_TO_DEG(currDirMod->getDirection());
    position.pitch   = RAD_TO_DEG(currOriMod->getOrientation().pitch);
    position.roll    = RAD_TO_DEG(currOriMod->getOrientation().roll);

	std::cout << "Cam Pitch:" << position.pitch << std::endl;

    return position;
}

Position MCTController::getTargetPosition()
{
    Position position;

    position.x = targetPosition.longitude;
    position.y = targetPosition.latitude;
    position.z = targetPosition.altitude;

    position.heading = RAD_TO_DEG(currDirMod->getDirection());

    return position;
}

std::string MCTController::getPosition(std::string type, bool abso) {
    std::string str;
    MCTposition tempPos = currPosMod->getPosition();
    char temp[80];
    if(type == "altitude") {
        if(abso) {
            if(tempPos.GPSalt == -1000)
                sprintf(temp, "%.2fm", tempPos.altitude);
            else
                sprintf(temp,"%.2fm", tempPos.GPSalt);
        } else {
            sprintf(temp,"%.2fm", tempPos.altitude - homePosition.altitude);
        }
        str = temp;
    } else if(type == "latitude") {
        if(abso) {
            if(tempPos.GPSalt == -1000) {
                sprintf(temp, "%.2fm", tempPos.latitude);
            } else {
                str = GPS_TO_STRING(tempPos.GPSlat, true);
            }
            str = temp;
        } else {
            if(homePosition.latitude > tempPos.latitude) {
                sprintf(temp,"S%.2fm",homePosition.latitude - tempPos.latitude);
            }else {
                sprintf(temp,"N%.2fm",tempPos.latitude - homePosition.latitude);
            }
            str = temp;
        }
    } else if(type == "longitude") {
        if( abso) {
            if(tempPos.GPSalt == -1000) {
                sprintf(temp, "%.2fm", tempPos.longitude);
            } else {
                str = GPS_TO_STRING(tempPos.GPSlong, false);
            }
            str = temp;
        } else {
            if(homePosition.longitude > tempPos.longitude) {
                sprintf(temp,"W%.2fm",homePosition.longitude - tempPos.longitude);
            }else {
                sprintf(temp,"E%.2fm",tempPos.longitude - homePosition.longitude);
            }
            str = temp;
        }
    }
    return str;
}

std::string MCTController::getTarget(std::string type, bool abso) {
    std::string str;
    char temp[80];
    //abso = false; //forcing false
    MCTposition tempPos = currPosMod->getPosition();

    if(type == "altitude") {
        if(abso) {
            if(tempPos.GPSalt == -1000)
                sprintf(temp, "%.2fm", tempPos.altitude);
            else
                sprintf(temp,"%.2fm", tempPos.GPSalt);
        } else {
            sprintf(temp,"%.2fm", targetPosition.altitude - tempPos.altitude);
        }
        str = temp;
    } else if(type == "latitude") {
        if(abso) {          
            if(tempPos.GPSalt == -1000) {
                if(targetPosition.latitude == 0 && tempPos.latitude != 0){
                    sprintf(temp, "%.2fm", tempPos.latitude);
                }
                else{
                    sprintf(temp, "%.2fm", targetPosition.latitude);
                }
                str = temp;
            } else {
                str = GPS_TO_STRING(targetPosition.GPSlat, true);
            }
        } else {
            if(homePosition.latitude > tempPos.latitude) {
                sprintf(temp,"S%.2fm",targetPosition.latitude - tempPos.latitude);
            }else {
                sprintf(temp,"N%.2fm",tempPos.latitude - targetPosition.latitude);
            }
            str = temp;
        }
    } else if(type == "longitude") {
        if( abso) {
            if(tempPos.GPSalt == -1000) {
                if(targetPosition.longitude == 0 && tempPos.longitude != 0){
                    sprintf(temp, "%.2fm", tempPos.longitude);
                }
                else{
                    sprintf(temp, "%.2fm", targetPosition.longitude);
                }
                str = temp;
            } else {
                str = GPS_TO_STRING(targetPosition.GPSlong, false);
            }
        } else {
            if(targetPosition.longitude > tempPos.longitude) {
                sprintf(temp,"E%.2fm",targetPosition.longitude - tempPos.longitude);
            }else {
                sprintf(temp,"W%.2fm",tempPos.longitude - targetPosition.longitude);
            }
            str = temp;
        }
    }
    return str;
}

double MCTController::getOrientation(std::string type) {
    if(type == "pitch")
        return RAD_TO_DEG(currOriMod->getOrientation().pitch);
    else if(type == "roll")
        return RAD_TO_DEG(currOriMod->getOrientation().roll);
}

double MCTController::getDirection() {
    return RAD_TO_DEG(currDirMod->getDirection());
}

bool MCTController::parseCommand(std::string command) {
    // tokenize command into arguments
    std::string *args = new std::string[10]();
    int start = 0,end = 0, count = 0;
    unsigned int i;
    for(i = 0;i < command.length()+1; i++) {
        if(command[i] == ' ' || (command.length()) == i) {
            if(end > 0) {
                args[count] = command.substr(start, end);
                if( count++ > 9)
                    return false;
            }
            end = 0;
            start = i+1;
        } else {
            end++;
        }
    }

    if(check_latitude(args[0]) == true && count == 2) {
       return adjust_target(0.0, atof(args[1].c_str()), 0.0, 0.0);
    }
    if(check_longitude(args[0]) == true && count == 2) {
        return adjust_target(atof(args[1].c_str()), 0.0, 0.0, 0.0 );
    }
    if(check_altitude(args[0]) == true && count == 2) {
        return adjust_target(0.0, 0.0, atof(args[1].c_str()), 0.0);
    }
    if(check_direction(args[0]) == true && count == 2) {
        return adjust_target(0.0, 0.0, 0.0, atof(args[1].c_str()));
    }
    if(check_position(args[0]) == true && count == 4) {
        return adjust_target(atof(args[1].c_str()), atof(args[2].c_str()), atof(args[3].c_str()), 0.0 );
    }
    if(check_sethome(args[0]) == true && count == 1 ) {
        setHome();
        return true;
    }
    if(check_omni(args[0]) == true && count == 3){
        return setToOmni(atof(args[1].c_str()), atof(args[2].c_str()));
    }

    return false;
}

bool MCTController::adjustTarget(Position new_target)
{
    if(new_target.x == 0 && new_target.y == 0 && new_target.z == 0 && new_target.heading == 0)
    {
        return false;
    }

    targetPosition.altitude += new_target.z;
    targetPosition.longitude += new_target.y;
    targetPosition.latitude += new_target.x;
    targetDirection += DEG_TO_RAD(new_target.heading);
    while (targetDirection > PI || targetDirection < -PI)
    {
        if(targetDirection > PI)
            targetDirection -= PI;
        if(targetDirection <0 )
            targetDirection += PI;
    }
    return true;
}

bool MCTController::setTarget(Position new_target)
{
    targetPosition.altitude  = new_target.z;
    targetPosition.longitude = new_target.y;
    targetPosition.latitude  = new_target.x;
    targetDirection          = DEG_TO_RAD(new_target.heading);
    while (targetDirection > PI || targetDirection < -PI)
    {
        if(targetDirection > PI)
            targetDirection -= PI;
        if(targetDirection <0 )
            targetDirection += PI;
    }
    return true;
}

bool MCTController::check_altitude(std::string str)
{
    if(str == "alt" ||
       str == "z" ||
       str == "altitude" ) {
            return true;
    }
    return false;
}

bool MCTController::check_latitude(std::string str)
{
    if(str == "lat" ||
       str == "y" ||
       str == "latitude" ) {
            return true;
    }
    return false;
}

bool MCTController::check_longitude(std::string str)
{
    if(str == "long" ||
       str == "lon" ||
       str == "x" ||
       str == "longitude" ) {
            return true;
    }
    return false;
}

bool MCTController::check_direction(std::string str)
{
    if(str == "dir" ||
       str == "direction" ) {
            return true;
    }
    return false;
}

bool MCTController::check_position(std::string str)
{
    if(str == "xyz" ||
       str == "p" ||
       str == "pos" ||
       str == "position" ||
       str == "target" ) {
            return true;
    }
    return false;
}

bool MCTController::check_sethome(std::string str)
{
    if(str == "sh" ||
       str == "sethome" ) {
            return true;
    }
    return false;
}

bool MCTController::check_omni(std::string str){
    if(str == "omni"){
        return true;
    }
}

//altitude,longitude,latitude in meters direction in degress
bool MCTController::adjust_target(double longitude, double latitude, double altitude, double direction)
{
    if(longitude == 0 && latitude == 0 && altitude == 0 && direction == 0) {
        return false;
    }

    targetPosition.altitude += altitude;
    targetPosition.longitude += longitude;
    targetPosition.latitude += latitude;
    targetDirection += DEG_TO_RAD(direction);
    while (targetDirection > PI || targetDirection < -PI) {
        if(targetDirection > PI)
            targetDirection -= PI;
        if(targetDirection <0 )
            targetDirection += PI;
    }
    return true;
}

double MCTController::getThrottleVal(){
    return lastThrottle;
}

void MCTController::setThrottleVal(double throttleChange){
    double curVals[8];
    currCommMod->getAllModValues(curVals, 8);
    curVals[0] += throttleChange;
    currCommMod->setModValues(curVals, 8);
}
