function [logtype, headings, A] = parse_component_log(filename)
%PARSE_COMPONENT_LOG
%Inputs: filename to data log for a certain component (e.g. Optitrack.log,
%PID.log, SixChannelTrainer.log, Vehicle.log)

%Outputs: logtype - A string pertaining to the data log passed in.
%         headings - A cell array of strings descripting each row in the
%         data matrix
%         A - Data matrix 

if (~exist(filename,'file'))
    error(strcat(filename, ' does not exist'));
end

%Open file
FileID = fopen(filename, 'r');

%Obtain log type
logtype = fgetl(FileID);
logtype = strsplit(logtype);
logtype = logtype{3};

%Obtain data headers
headings = fgetl(FileID);
headings = strsplit(headings);

variable_string = '';

for i = 1:size(headings,2)
   strcat(variable_string, '%f '); 
end

%Get all data into cells; convert cells to a single matrix
A = textscan(FileID, variable_string);
A = cell2mat(A);

figureCount = 0;

%Plot data points
for i = 1:size(headings,2)
    
    
    %create a new subplot if 2x2 is filled or doesn't exist
   if(mod(i,9) == 1)
       figTitle = sprintf('%s Log Results %d', logtype, figureCount);
       figureCount = figureCount + 1;
       figure('name', figTitle);
   end
   
   subplot(3,3,mod(i-1,9)+1);
   plot(A(:,1), A(:,i));
   title(headings{i});
   xlabel(headings{1});
   ylabel(headings{i});
   
end

end %function


