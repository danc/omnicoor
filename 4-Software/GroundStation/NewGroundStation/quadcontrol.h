#ifndef QUADCONTROL_H
#define QUADCONTROL_H

#include <stdio.h>

#include "structs.h"
#include "rfutils.h"


#define THRO_CHANNEL 1
#define AILE_CHANNEL 2
#define ELEV_CHANNEL 3
#define RUDD_CHANNEL 4

#define THRO_IDLE 70
#define AILE_IDLE 70
#define ELEV_IDLE 70
#define RUDD_IDLE 70

#define SCALING .167  // 1/6 because this is the ratio used in old gr. station

#define KILL_VALUE 0

#define MIN_VALUE 0
#define MAX_VALUE 1300

void quadInit();

int clamp(int, int, int);

// expected to be in [throttle, roll, pitch, yaw]
void setValues(int* data);

void sendRUDD(int);

void sendELEV(int);

void sendAILE(int);

void sendTHRO(int);

void killSwitch();

#endif
