#ifndef C_PID_H
#define C_PID_H

#include <math.h>
#include "structs.h"

void pidInit();

double euler_PID(QUADPID* pidptr);

double euler_PD(QUADPID* pidptr);

double proportional(double, double);

double integral(double, double);

double derivative(double, double);

//Rad to Deg covertion function
double RAD_TO_DEG(double angle); 

void printTerminal();

bool loadKvalues(std::string fileName);

#endif
