/*************************************
quadcontrol.cpp - omniCOOR quad rotor control system

Contents
-Initializer for UART
-SetValues functions for all channels as well as individual channels
-clamp - clips a variable to be within a minimum and maximum value


**************************************/

#include "quadcontrol.h"

int currentData[NUM_CHANNELS];
extern QUAD quad;

/* Initialize serial port and pointers*/ 
void quadInit() {
    rfInit("/dev/ttyS0");
}

/*checks bounds of motors*/
int clamp(int n, int min, int max) {
    int m;
    if (n < min){
    m = min;
    }
    else if (n > max){
    m = max;
    }
    else 
    m = n;
    return m;
}


void setValues(int* data) {
    for(int i=0; i < NUM_CHANNELS; i++) {
        currentData[i] = clamp( data[i] * 13, MIN_VALUE, MAX_VALUE);
    }
    sendRFData(currentData);
    quad.motor1 =  currentData[THRO_CHANNEL-1];
    quad.motor2 =  currentData[AILE_CHANNEL-1];
    quad.motor3 =  currentData[ELEV_CHANNEL-1];
    quad.motor4 =  currentData[RUDD_CHANNEL-1];
}

void sendRUDD(int value) {
    currentData[RUDD_CHANNEL - 1] = clamp( value * 13, MIN_VALUE, MAX_VALUE);
    quad.motor4 =  currentData[RUDD_CHANNEL-1];
    sendRFData(currentData);
}

void sendELEV(int value) {
    currentData[ELEV_CHANNEL - 1] = clamp( value * 13, MIN_VALUE, MAX_VALUE);
    quad.motor3 =  currentData[ELEV_CHANNEL-1];
    sendRFData(currentData);
}

void sendAILE(int value) {
    currentData[AILE_CHANNEL - 1] = clamp( value * 13, MIN_VALUE, MAX_VALUE);
    quad.motor2 =  currentData[AILE_CHANNEL-1];
    sendRFData(currentData);
}

void sendTHRO(int value) {
    currentData[THRO_CHANNEL - 1] = clamp( value * 13, MIN_VALUE, MAX_VALUE);
    quad.motor1 =  currentData[THRO_CHANNEL-1];
    sendRFData(currentData);
}

void killSwitch() {
    currentData[THRO_CHANNEL - 1] = KILL_VALUE;
    sendRFData(currentData);
}



