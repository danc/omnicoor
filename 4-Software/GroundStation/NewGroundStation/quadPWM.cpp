/*************************************
quadPWM.cpp - omniCOOR quad rotor control system

Contents:
-Main function
-Implementation of PID controls
-Logger


**************************************/


#include "quadPWM.h"

#define TAB << "\t" <<
#define TAAB << "\t\t" <<
#define CLEAR "\033[H\033[J"

QUAD quad; //includes PID values, logger for the quad and motor values etc.
using namespace std;

/*init function - runs init functions for all other files*/
void init()
{
    pidInit();
    logStart();
    vrpnInit("192.168.0.120:3883", handle_pos);
    quadInit();    
}

/* MAIN LOOP */ 
int main() {    
    init();
    vrpnGo();
    logStop();
}



void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t){
    //sets previous times for using later
    quad.prev = quad.now;
    quad.vrpnPrev = quad.vrpnNow;
    
    //this caputres the initial time so logging begins at 0 for both vrpn and the system time logged this runs the first time the callback is run
    if(quad.vrpnNow == 0){ 
        quad.vrpnPrev = 0;
        quad.vrpnTime0 = t.msg_time.tv_sec + (double)t.msg_time.tv_usec/1000000.0;
        quad.vrpnNow = t.msg_time.tv_sec + (double)t.msg_time.tv_usec/1000000.0;
        clock_gettime(CLOCK_REALTIME, &quad.sys);
        quad.time0 = quad.sys.tv_sec + (double)quad.sys.tv_nsec / 1000000000.0 ;
        quad.now = quad.sys.tv_sec + (double)quad.sys.tv_nsec/1000000000.0 - quad.time0;
    }
    //this runs after the first time the callback function is run
    else{
    quad.vrpnNow = t.msg_time.tv_sec + (double)t.msg_time.tv_usec/1000000.0 - quad.vrpnTime0;
    clock_gettime(CLOCK_REALTIME, &quad.sys);
    quad.now = quad.sys.tv_sec + (double)quad.sys.tv_nsec/1000000000.0 - quad.time0;
    }
    
    //calculates time slice between current and previous vrpn time as well as system times.
    //vtimeslice = exact camera system sample time 
    //timeslice = exact time between system running PID computation
    quad.vtimeslice = quad.vrpnNow - quad.vrpnPrev;
    quad.timeslice = quad.now - quad.prev;
    
    q_vec_type euler;
    q_to_euler(euler, t.quat);

    /*********************************************
    ** PID Implementation                        *
    ** Here is the actual implementation of the  *
    ** Controller                                *    
    *********************************************/
    
    quad.pitch.desired = 0.01744; // The desired pitch is not exaclty 0... there needs to be an offset of -1 degree = -0.01744 radians
    quad.yaw.desired   = 0;
    quad.roll.desired  = 0;
    
    quad.yaw.current   = euler[0];
    quad.pitch.current = euler[1];
    quad.roll.current  = euler[2];
    
 
        
        
    // Runs the PID **all needed data is stored in the structures!**
    euler_PID(&quad.pitch);
    euler_PID(&quad.roll);
    euler_PID(&quad.yaw);


    //mixing matrix
        
    // We just use THRO and ELEV channels. They are the only being used on the 1-D setup 
    // IDLE constants are the speeds at which the motors will spin when a PID value of 0 is being calculated and returned.
    int data_send[NUM_CHANNELS] = {};
	data_send[THRO_CHANNEL-1]    = THRO_IDLE -  quad.pitch.correction * SCALING ;
    data_send[AILE_CHANNEL-1]    = AILE_IDLE +  quad.roll.correction * SCALING ;
    data_send[ELEV_CHANNEL-1]    = ELEV_IDLE +  quad.pitch.correction * SCALING ;   
    data_send[RUDD_CHANNEL-1]    = RUDD_IDLE -  quad.roll.correction * SCALING ;
    setValues(data_send);
    clock_gettime(CLOCK_REALTIME, &quad.sys);
    quad.tcompute = quad.sys.tv_sec + (double)quad.sys.tv_nsec / 1000000000.0 - quad.time0 - quad.now ;
    
    /* Terminal output - May be modified to meet user's needs..  quad.line can also be modified in any of the source files for debugging purposes*/
    quad.line << fixed << "Time(vrpn, sys): " << quad.vrpnNow TAB quad.now  << "\nTracker Orientation(Yaw, Pitch, Roll): (" << RAD_TO_DEG(quad.yaw.current) << ", " << RAD_TO_DEG(quad.pitch.current) << ", " << RAD_TO_DEG(quad.roll.current) << ")" << "\nERROR(Roll): " << RAD_TO_DEG(quad.roll.err) << "\nMotor 1: " << quad.motor1 << "\nMotor 2: " << quad.motor2 << "\nMotor 3: " << quad.motor3 << "\nMotor 4: " << quad.motor4 << "\nTimeslice(vrpn, sys): " << quad.vtimeslice TAB quad.timeslice;
    
    logStandard();      //wait till after sending to print and log to optimize speed
    printTerminal();    //prints quad.line to terminal

}
    

    /*********************************************
    **   Data logger                             *
    *********************************************/

bool logStart()
{
    time_t now;
    struct tm *ts;
    char buf[300],buf2[100];
    now = time(0);
    ts = localtime(&now);
    //Directory should look like "2011-4-15_DAY" and specific log "12:34:10"
    strftime(buf, sizeof(buf),"%Y-%m-%d_%a/",ts);
    strftime(buf2, sizeof(buf2), "log(%H:%M:%S).txt", ts);
    quad.logFilename << "./Logs/";
    mkdir(quad.logFilename.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // make the Logs directory
    quad.logFilename << buf;
    mkdir(quad.logFilename.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // make the directory with date to contain log file
    quad.logFilename << buf2;
    quad.logFile.open(quad.logFilename.str().c_str(), fstream::out);         // open the log file
    
    //quad.logFile.width(2);
    //This is the header for the logfile..
    quad.logFile << "#Constants\t\tpitch_P\t\tpitch_I\t\tpitch_D" << endl;
    quad.logFile << "#PIDValues\t\t" << quad.pitch.kp << "\t\t" << quad.pitch.ki << "\t\t" << quad.pitch.kd << endl;
    quad.logFile << "#Constants\t\troll_P\t\troll_I\t\troll_D" << endl;
    quad.logFile << "#PIDValues\t\t" << quad.roll.kp << "\t\t" << quad.roll.ki << "\t\t" << quad.roll.kd << endl;
    quad.logFile << "#TIME"TAAB"Motor_1"TAB"Motor_2"TAB"Motor_3"TAB"Motor_4"TAB"Pitch"TAAB"Pitch_err"TAAB"Pitch_pid_p"TAAB"Pitch_pid_d"TAAB"Roll"TAAB"Roll_err"TAAB"Roll_pid_p"TAAB"Roll_pid_d"TAAB"vrpn_slice"TAAB"sys_slice"TAAB"compute time" << endl;
    quad.logFile << "&sec"TAAB"%thrust"TAB"%thrust"TAB"%thrust"TAB"%thrust"TAB"degrees"TAAB"degrees"TAAB"degrees"TAAB"degrees"TAAB"degrees"TAAB"degrees"TAAB"degrees"TAAB"degrees"TAAB"sec"TAAB"sec"TAAB"sec"<< endl;
    
    quad.line.width(1);
    quad.logFile.width(1);
    quad.line << CLEAR;
    
    if(quad.logFile.is_open()) {
        return true;
    } else {
        return false;
    }
}
//closes the log file
bool logStop()
{
    quad.logFile.close();
    return true;
}

//writes a line to the log file
void logStandard()
{
    //all inputs must be doubles if you want the logfile to be human readable. you may leave these typecast out if you don't plan on looking at the logfiles manually
    quad.logFile << fixed << quad.vrpnNow TAB quad.motor1 TAB quad.motor2 TAB quad.motor3 TAB quad.motor4 TAB RAD_TO_DEG(quad.pitch.current) TAB RAD_TO_DEG(quad.pitch.err) TAB quad.pitch.PID_pcomp TAB quad.pitch.PID_dcomp TAB RAD_TO_DEG(quad.roll.current) TAB RAD_TO_DEG(quad.roll.err) TAB quad.roll.PID_pcomp TAB quad.roll.PID_dcomp TAB quad.vtimeslice TAB quad.timeslice TAB quad.tcompute <<  endl;
}

void printTerminal()
{    
    cout << quad.line.str() << endl;   //This can be used for debugging
    quad.line.str(string());
    quad.line << CLEAR;
}

