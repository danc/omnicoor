/*************************************
c_PID.cpp - omniCOOR quad rotor control system
- PID control functions

Contents:
-pid_init is currently not being used.
-euler_PID the full PID function
-euler_PD a PID function without the Integral component
-proportional, integral, derivitive - each individual PID component;
-RAD_TO_DEG - conversion from radians to degrees, mainly for logging.
-loadKvalues - function to parse PID contents from a file and load to memory

**************************************/

#include "PID.h"
#include <stdio.h>

extern QUAD quad;

void pidInit()
{
    loadKvalues(std::string("profile.txt"));
}
/**
    Returns a PID coefficient based on the current position and the desired position.
    This is for roll/pitch/yaw as they are all within the same range of -pi : +pi
*/

double euler_PID(QUADPID* pidptr){
   pidptr->err= pidptr->desired - pidptr->current;
   if(pidptr->err == pidptr->preverr)
    {
        return pidptr->correction; 
    }
    pidptr->prevD  = pidptr->PID_dcomp; 
    pidptr->PID_pcomp  = pidptr->kp * pidptr->err;
    pidptr->PID_icomp += pidptr->ki * pidptr->err;
    pidptr->PID_dcomp  = ( (pidptr->kd * (pidptr->err - pidptr->preverr) ) + (0.001 * pidptr->prevD) )/ ( 0.001 + quad.timeslice);
    pidptr->preverr = pidptr->err;
    pidptr->correction = pidptr->PID_pcomp + pidptr->PID_icomp + pidptr->PID_dcomp;
    return pidptr->correction; 
}

/* PD controller */ //This is the actual controller being used in the 1-D system 
double euler_PD(QUADPID* pidptr){
    pidptr->err= pidptr->desired - pidptr->current;
    if(pidptr->err == pidptr->preverr)
    {
        return pidptr->correction; 
    }
    pidptr->prevD  = pidptr->PID_dcomp; 
    pidptr->PID_pcomp  = pidptr->kp * pidptr->err;
    pidptr->PID_dcomp  = ( (pidptr->kd * (pidptr->err - pidptr->preverr) ) + (0.001 * pidptr->prevD) )/ ( 0.001 + quad.timeslice);
    pidptr->preverr = pidptr->err;
    pidptr->correction = pidptr->PID_pcomp + pidptr->PID_dcomp;
    return pidptr->correction; 
}

/*
  These three functions will be used to help demonstrate how PIDs work
*/
// max return value = pi * .025
double proportional(QUADPID* pidptr){
    pidptr->err = pidptr->desired - pidptr->current;
    if(pidptr->err == pidptr->preverr)
    {
        return pidptr->correction; 
    }
    pidptr->correction = pidptr->kp * pidptr->err;
    return pidptr->correction;
}

double integral(QUADPID* pidptr){
    pidptr->PID_icomp += pidptr->ki * (pidptr->desired - pidptr->current);
    return pidptr->PID_icomp;
}

double derivative(QUADPID* pidptr){
    pidptr->err = pidptr->desired - pidptr->current;
    pidptr->PID_dcomp = pidptr->kd * (pidptr->err - pidptr->preverr) / quad.timeslice;
    pidptr->preverr = pidptr->err;
    return pidptr->PID_dcomp;
}


/* Radians to degrees convertion utility function */ 
double RAD_TO_DEG(double angle) {
    double temp = angle/M_PI * 180.0;
    while( temp > 180.0 || temp < -180.0 ) {
        if( temp > 180) {
            temp -= 180;
        }
        if(temp < -180) {
            temp += 180;
        }
    }
    return temp;
}
/*
This is the command that parses the logfile to assign PID constants.
*/
bool loadKvalues(std::string fileName)
{
    QUADPID* pidptr;

    std::fstream profile (fileName.c_str(), std::fstream::in);
    int fpos = 14;
    std::string line;

    getline(profile, line);
    quad.roll.kp = atof(line.substr(fpos).c_str());
    getline(profile, line);
    quad.roll.ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    quad.roll.kd = atof(line.substr(fpos).c_str());

    getline(profile, line);
    quad.pitch.kp = atof(line.substr(fpos).c_str());
    getline(profile, line);
    quad.pitch.ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    quad.pitch.kd = atof(line.substr(fpos).c_str());

    getline(profile, line);
    quad.yaw.kp = atof(line.substr(fpos).c_str());
    getline(profile, line);
    quad.yaw.ki = atof(line.substr(fpos).c_str());
    getline(profile, line);
    quad.yaw.kd = atof(line.substr(fpos).c_str());

    profile.close();
    
    std::cout << "roll.kp: " << quad.roll.kp << "\nroll.ki: " << quad.roll.ki << "\nroll.kd: " << quad.roll.kd << "\npitch.kp: " << quad.pitch.kp << "\npitch.ki: " << quad.pitch.ki << "\npitch.kd: " << quad.pitch.kd << "\nyaw.kp: " << quad.yaw.kp << "\nyaw.ki: " << quad.yaw.ki << "\nyaw.kd: " << quad.yaw.kd << std::endl;

    return 0;

}


