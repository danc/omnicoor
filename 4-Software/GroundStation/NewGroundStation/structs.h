#include <stdio.h>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
#include <time.h>


//Struct to store field of prev Errors, and previous D and I 
#ifndef QUADPID_STR
#define QUADPID_STR
typedef struct {    
    double err;             //error from desired.. units: radians
    double preverr;         //previous error from desired.. units: radians
    double prevI;           //previous PID Integral component. units: ki*error*time
    double prevD;           //previous PID Derivative component. units: kd*delta_error/time
    double correction;      //sum of PID components. units: ki*error*time + kd*delta_error/time + kp*error
    double PID_pcomp;       //PID proportional component. units: kp*error
    double PID_icomp;       //PID Integral component. units: ki*error*time
    double PID_dcomp;       //PID derivative component. units: kd*delta_error/time
    double current;         //current angle. units: radians
    double desired;         //desired angle. units: radians
    double kp;              //proportional PID constant. units: k value
    double ki;              //integral PID constant. units: k value
    double kd;              //derivative PID constant. units: k value
    
}QUADPID;
#endif

#ifndef QUAD_STR
#define QUAD_STR
typedef struct {
    struct timespec sys;          //system time structure. used for logging and debugging purposes only.
    double now;                   //current system time. units: seconds
    double prev;                  //previous system time. units: seconds
    double time0;                 //first time obtained from system time
    double tcompute;              //time to compute PID and send after receiving VRPN packet.
    double vrpnNow;               //time obtained from vrpn packet. units: seconds
    double vrpnPrev;              //previous time obtained from vrpn packet
    double vrpnTime0;             //first time obtained from vrpn packet
    double vtimeslice;            //timeslice obtained from vrpn packet time.
    double timeslice;             //timeslice calculated from system time.
    int motor1;                   //value to be sent to fpga to generate PWM for motor1. units: %thrust
    int motor2;                   //value to be sent to fpga to generate PWM for motor2. units: %thrust
    int motor3;                   //value to be sent to fpga to generate PWM for motor3. units: %thrust
    int motor4;                   //value to be sent to fpga to generate PWM for motor4. units: %thrust
    double desired_pitch;         //desired pitch angle. units: radians
    double desired_yaw;           //desired yaw angle. units: radians
    double desired_roll;          //desired roll angle. units: radians
    std::stringstream line;       // line to be printed to terminal
    std::stringstream logFilename;// name of the LogFile contained in a string
    std::stringstream directory;  // name of the logFile directory contained in a string
    std::fstream logFile;         // log file object
    QUADPID pitch;                // PID data for the pitch component
    QUADPID roll;                 // PID data for the roll component
    QUADPID yaw;                  // PID data for the yaw component

}QUAD;
#endif


