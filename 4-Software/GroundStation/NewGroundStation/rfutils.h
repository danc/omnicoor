#ifndef RFUTILS_H
#define RFUTILS_H

#include <stdio.h> /* Standard input/output definitions */
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h> /* File control definitions */
#include <errno.h> /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#define NUM_CHANNELS 4


/* Initialize Serial port */ 
void rfInit(const char*);

/* Send all 4 channels data via the serial port */ 
void sendRFData(int* data);

#endif
