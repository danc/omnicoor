/*
 * @author: Robert Larsen
 * Date: 10/4/2014
 * Description: A simple test program to see if values are being read from an Xbox controller.
 *              Prints what button is being pressed.
 */

// Includes for open function
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
// Other Includes
#include <stdio.h>
#include <stdlib.h>

// Defines
#define CONTROLLER_PATH "/dev/input/js0"    // For now, hard coded path name

// Globals
typedef struct controller_event
{
    unsigned int time;      // Event timestamp
    short value;            // Value
    unsigned char type;     // Event type
    unsigned char number;   // Button number
} control_event_t;

// Opens controller file
int open_controller()
{
    int controller_fd = -1;     // file descriptor for Xbox controller
    
    controller_fd = open(CONTROLLER_PATH, O_RDONLY);    // Open controller file
    
    return controller_fd;
}


int read_controller(int file_descriptor, control_event_t *event)
{
    int num_bytes_read = -1;     // Value to examine read status
    
    num_bytes_read = read(file_descriptor, event, sizeof(*event));   // Read from device
    // Quick check on status of read
    if(num_bytes_read < 0)
    {
        // Failed to read device
        return -1;
    }
    else if(num_bytes_read == sizeof(*event))
    {
        // Read properly
        return 1;
    }
    // Otherwise, unexpected number of bytes read
    return 0;
}

int main()
{
    int file_descriptor = -1;
    int read_status = -1;
    control_event_t controller_event;
    
    file_descriptor = open_controller();
    // Print to user if file could not be opened
    if(file_descriptor < 0)
    {
        printf("Controller could not be opened.\n");
        exit(0);
    }
    // ctrl-C to exit for now
    while(1)
    {
        usleep(1000);
        read_status = read_controller(file_descriptor, &controller_event);
        if(read_status < 0)
        {
            printf("Device could not be read.\n");
            exit(0);    // Exit if device cannot be read (allows it to exit when controller is unplugged)
        }
        else if(read_status == 0)
        {
            printf("Unexpected number of bytes read.\n");
        }
        else
        {
            // Diagnose individual buttons
            // Type = 1: Corresponds to a button event
            // Type = 2: Triggers, analog sticks, and d-pad
            if(controller_event.number == 0 && controller_event.type == 1)
            {
                printf("This is button A!\n");
            }
            else if(controller_event.number == 1 && controller_event.type == 1)
            {
                printf("This is button B!\n");
            }
            else if(controller_event.number == 2 && controller_event.type == 1)
            {
                printf("This is the black button!\n");
            }
            else if(controller_event.number == 3 && controller_event.type == 1)
            {
                printf("This is button X!\n");
            }
            else if(controller_event.number == 4 && controller_event.type == 1)
            {
                printf("This is button Y!\n");
            }
            else if(controller_event.number == 5 && controller_event.type == 1)
            {
                printf("This is the white button!\n");
            }
            else if(controller_event.number == 6 && controller_event.type == 1)
            {
                printf("This is the start button!\n");
            }
            else if(controller_event.number == 7 && controller_event.type == 1)
            {
                printf("This is the left joystick button!\n");
            }
            else if(controller_event.number == 8 && controller_event.type == 1)
            {
                printf("This is the right joystick button!\n");
            }
            else if(controller_event.number == 9 && controller_event.type == 1)
            {
                printf("This is the select button!\n");
            }
            else if(controller_event.number == 5 && controller_event.type == 2)
            {
                printf("This is the right trigger!\n");
            }
            else if(controller_event.number == 2 && controller_event.type == 2)
            {
                printf("This is the left trigger\n");
            }
            // These joystick axis may be flipped around
            else if(controller_event.number == 0 && controller_event.type == 2)
            {
                printf("Left Joystick Vertical Axis?\n");
            }
            else if(controller_event.number == 1 && controller_event.type == 2)
            {
                printf("Left Joystick Horizontal Axis?\n");
            }
            else if(controller_event.number == 3 && controller_event.type == 2)
            {
                printf("Right Joystick Vertical Axis?\n");
            }
            else if(controller_event.number == 4 && controller_event.type == 2)
            {
                printf("Right Joystick Horizontal Axis?\n");
            }
            // D-pad buttons
            else if(controller_event.number == 6 && controller_event.type == 2 && controller_event.value < 0)
            {
                printf("Left on d-pad!\n");
            }
            else if(controller_event.number == 6 && controller_event.type == 2 && controller_event.value > 0)
            {
                printf("Right on d-pad!\n");
            }
            else if(controller_event.number == 7 && controller_event.type == 2 && controller_event.value < 0)
            {
                printf("Up on d-pad!\n");
            }
            else if(controller_event.number == 7 && controller_event.type == 2 && controller_event.value > 0)
            {
                printf("Down on d-pad!\n");
            }
        }
    }
}

