Date:   11/6/2014
Author: Rohit Zambre

Duration: 1 hour 
Members Present: All except Aaron 
Advisors Present: Paul Uhing, Matt Rich

------------
* Overview *
------------
Meeting minutes

11-6-2014:

Eris
-- Apparently not easy to expand out of the current setup.
	Refer to Robert's email

Feng
-- Went through Matt's thesis. Will get reference from Matt soon.
-- Worked with Ian to measure the moment of inertia.
-- Will work with the team on Friday to do generate constants for the inverted pendulum system

Moment of inertia measurements
-- Did 5 runs with 5 different voltage levels

Might want to look into OpenCV to work with the webcam
-- for demo and application purposes
-- Purpose: to detect objects without the IR sensor balls
-- talk to Dr. Jones for possible options and next steps

Motor test
-- Conduct the test that Dr. Elia mentioned
	Test the motor with constant PID and see if there is any jumping occuring or not

Use Matt's flow chart on the white board as a guide to characterize each part of the system
-- Purpose: to find the source of jumping error
-- While doing so, come up with own diagram of the board.

Gimbal joint
-- Need a brushless motor OR stepper motor for the gimbal joint (servocity.com)
-- look into what is available. Find out what is needed to drive them, etc.
-- don't think we need the mechanical universal joint as of now

Localization system
-- Partially works on the XP machine with the ground robot
-- The robot does follow the tennis ball. The motors do update only when the robot is rotated.
-- The camera is most likely not detecting the objects.