Date:   2/12/2015
Author: Rohit Zambre

Duration:  
Members Present: All
Advisors Present: Dr. Jones, Paul Uhing, Matt Rich

------------
* Overview *
------------
Meeting minutes

2-19-2015:

Aaron: Tested the rotatory potentiometers and they look pretttttty bad for the most part. Kinda, sorta reliable but not really. The hall effect sensor was better than the rotatory potentiometers but is still not the best. 
Be sure to record data in one place for each sensor. Will be useful for the IRP.
We should look at possibly geting some new sensors for better sensing capabilities.
Need to make spare mounts for the motors.
Will also work on incorporating options in the GUI.

Dylan: Removed the unecessary dependencies for the Korebot. Did a bunch of cleaning make files to improve compiling. Need to update the wiki to reflect changes. Was able to compile everything except for the GUI: some library issues with GTK. Has automake files.

Did the backup. ETG wants us to bring the computer down there. Ghost would take 3 hours at the max to backup the drive. Need to coordinate with ETG to get this task done.

Feng: Talked to Paul. Still working on modeling. Need to get subsytems working.

Ian: Installed the ESCs on to the pole. Will install two more to finish up the 4 propellers after flashing two more ESCs. Need to document the data about the characteristics of the ESCs. Will add this to the Wiki.

Robert: After throwing the ucf file, was having issues exporting designing.
The .pin file is just a text file that corresponds to the .ucf file.
Will send Dr. Jones a zipped file containing the project so that he will be able to check for errors.

Rohit: Modified the current tool to incorporate the new changes in the structure format. Update parse_log function. Will work with Dylan and Robert to log data on Eris. Also, need to work on quaternion to euler angle conversion. Test Z,Y, X option first (in MATLAB)

Ian got access to create a new wiki. Name of the wiki IS RADA (Robotic Agriculture Data Acquisition)

Need to look at getting some new IMUs. Everyone still needs to research sensors and actuators.
