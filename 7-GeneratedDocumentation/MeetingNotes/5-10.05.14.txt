Date:   10/05/2014
Author: Rohit Zambre

Duration: 1 hr
Members Present: All

------------
* Overview *
------------
Project plan turned out to be good. Good enough for 
Hosting of website: Need to use ece resources.

Updates from team members:
  * Robert and Dylan: Wrote a C program to read from the Xbox controller. Reads the buttons that are being pressed.
  * Aaron: Found the k-values for the PID. We know where the values should be put in
  * Ian: Was working on continuous time model of the pendulum. Now, has to make discrete time model first to see how it balances then.
  * Feng: Project plan help with controller stuff. Ian and Feng met with Matt and Paul Friday afternoon to understand the parameters of an inverted pendulum. Played around with Simulink.
  * Aaron: Looked through the test code. Made some progress with manipulating the matrix but not a lot of success. Still need to work on finding the right matrix to manipulate it. 


----------------
* Action Items *
----------------
  * Make system-level diagram for how the controller works.
  * Need to make discrete time model for the pendulum
  * Need to make a signal flow from the camera system to the qaudcopter to see where the values are being manipulated.
    - Need to write additional test scripts