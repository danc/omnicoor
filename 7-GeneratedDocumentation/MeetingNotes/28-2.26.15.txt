Date:   2/26/2015
Author: Rohit Zambre

Duration: 1 hour
Members Present: All
Advisors Present: Dr. Jones, Paul Uhing

------------
* Overview *
------------
Meeting minutes

2-26-2015:

Aaron: Finished up GUI and did sensor documentation

Robert has to do something for the PWM outputs. Try making things work on Eris if they are not working well on the base station.

For Aaron and Alberto: Install the passive joint, at least 1D motion. Find links to the Old SVN and send them to Aaron and Alberto. Need CAD files.

Dylan: Tested running the executable. Some run-time errors. Need to install something (as Ian said). If still having multiple errors, email Dr. Jones and get Dr. Zambreno's help.
Try possible ways to find where the error is occuring while compiling or during run-time.

Feng: Should send out power point slides to everyone. Found some mistakes and the new model seems to work fairly well. Minor unusual behviors here and there.
After working on some small errors, the model seems to behave wierdly while applying the PID.
Paul will look into the model and play around with the simulation to figure what might be wrong.
Feng should possibly look into regular potentiometers and alternate Hall effect sensors.

Ian: All ESCs now done. Put some ESC modification documentation on Wiki. Ian could help Robert and Dylan with their issues.

Robert: Did some ISE stuff with. Did correspond with Dr. Jones. Make sure that there is one UCF file. Get the control program set on Eris. And get logging on the system.

Rohit: Working on the quaternion to euler conversion. Will be meeting with Robert and Dylan to get logging on Eris.
Order that the group shoudl use: yaw (outer), pitch(inner), roll(inner-most)
The measured angle output of the camera system should be in the same order as that of the calculations.
Will help with debugging of Data Analysis GUI.

The lab should get a YouTube channel.