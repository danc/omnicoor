Date:   10/26/2014
Author: Rohit Zambre

Duration: 3 hrs (25 min of meeting time) 
Members Present: All except Robert
Advisors Present: Paul Uhing and Matt Rich

------------
* Overview *
------------
Meeting minutes

10-26-2014:

Data analysis tool done for 1-D pendulum system
  -- Need to make manual on how to use it.

Need to look into the issues of the localization system
  -- Need to contact CSG to resolve issues

A structure partially close to the final one is ready

One of the motors seems to be more powerful than the other one

Need to fine tune the 1-D system
  -- Try new PID constansts sent by Paul

Need to plan the timeline of tasks until dead week

Primarily work on Design Document to be submitted on Tuesday