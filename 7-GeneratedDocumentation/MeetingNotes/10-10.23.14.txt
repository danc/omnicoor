Date: 10/23/2014
Author: Rohit Zambre

Duration: 1 hour
Members present: All
Advisers present: All

------------
* Overview *
------------
Meeting minutes

10-19-2014:

Discussed about presentation next week
	-- Will discuss with the other team regarding day to present on

The PWM for omni
	-- found a couple files to play around with
	-- will ask MicroCART to reuse the PWM code to work with omniBot's FPGA

Emailed with mesa
	-- they said they have a linuxCNC program to work with certain packages and libraries

Vision system
	-- just needs to be hooked up to the windows 7 machine

Send email out with the git structure

Documentation is very important
	-- spend a week dedicated towards documentation

Design document
	-- more precise than the previous project plan
	-- will be tricky since the specifications haven't need to be 

Trouble with small omnibot
	-- build a light structure on the top of the omni
	-- first need to look into the gimbal joint before even building a structure
	-- need to complete gimbal joint research for sure

Cost of 3d printed materials
	-- $48 on the base

Need to have the 3d design of the gimbal joint 

Simulink model not cooperating
	-- Certain things don't make logical sense.
	-- Go back to the model and double check all the blocks
	-- Next week: get a linear model workin

Inverted 1-dimensional pendulum
	-- Worked sort of currectly on Monday
	-- Unstable on Wednesday
	-- 1 PID Controller in the GUI
	-- NEED TO BUY A NEW RECEIVER to separate ours from the microcart's receiver

