Date:   4/16/2015
Author: Rohit Zambre

Duration: 1 hour
Members Present: All
Advisors Present: Paul Uhing, Matt Rich, Dr. Jones

------------
* Overview *
------------
Meeting minutes

Aaron and Alberto:
Drilled holes, Assembled Eris.

Dylan and Robert:
Some software issues

Feng:
Should work with poster documentation

Ian:
Physically modified the ESC for the gimbal motor. Will flash and test the motor next.
Start working on documentation and report work.

Robert:
The new PWM outputs correctly but the old ones are a little erroneous with the new codebase. Haven't tested with the old code with the new bit file.
