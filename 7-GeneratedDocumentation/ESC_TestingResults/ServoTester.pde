#include <Servo.h>

// The number of characters the number contains
#define NUMBER_OF_CHARS 4

Servo serv;

uint8_t received = 0;
char number[NUMBER_OF_CHARS+1];	// The char array to hold the received number (including null termination)
uint16_t servoValue = 1000;

void setup() {
	// Start the serial port
	Serial.begin(9600);

	// Attach to the servo and zero it's speed
	serv.attach(9);
	serv.write(servoValue);
}

void loop() {
	if (Serial.available() ) {
		// Read in the serial port stuff if there is stuff
		number[received++] = Serial.read();

		if ( received == NUMBER_OF_CHARS ) {
			// Received all the numbers, process it
			number[4] = 0;				// Apply a null termination to the chars
			received = 0;				// Zero received for next round
			servoValue = atoi(number);	// Convert to a number from the char string

			// Write it to the servo
			serv.writeMicroseconds(servoValue);
			
			// Display the new set point to the user
			Serial.print("Set to: ");
			Serial.print(servoValue, DEC);
			Serial.print("\n");
		}
	}
	delay(100);
}
